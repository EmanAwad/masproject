﻿using MasProject.Domain.ViewModel.Persons;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using Radzen.Blazor;
using Microsoft.EntityFrameworkCore;
using Radzen;
using MasProject.Domain.ViewModel.TransVM;
using System.Reflection;
using System;
using System.ComponentModel;
using static MasProject.Domain.Enums.Enums;
using Microsoft.JSInterop;
namespace MasProject.Client.Pages.Reports.EmployeeBalance
{
    public partial class EmployeesBalance
    {
        private PageTitle PageTitle;
        //RadzenGrid<TransEmployeeVM> grid;
        //IEnumerable<TransEmployeeVM> TransEmployeeList;
        EmpolyeeBalanceReportVM EmployeeBalance = new EmpolyeeBalanceReportVM();
        List<EmployeeVM> EmployeeList = new List<EmployeeVM>();
        //TransEmployeeVM transEmployee = new TransEmployeeVM();
       // bool ShowReport = false;
        bool ShowConfirm = false;
        bool ShowCheckEmployee = false;
        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        protected async Task Report()
        {
            string DateFrom = EmployeeBalance.DateForm.HasValue ? EmployeeBalance.DateForm.Value.ToLocalTime().Date.ToString("yyyy- MM-dd") : null;
            string DateTo = EmployeeBalance.DateTo.HasValue ? EmployeeBalance.DateTo.Value.ToLocalTime().Date.ToString("yyyy- MM-dd") : null;
            await jsRuntime.InvokeAsync<object>("open", "/EmployeesBalanceReport?EmployeeId=" + EmployeeBalance.EmployeeId + "&DateFrom=" + DateFrom + "&DateTo=" + DateTo, "_blank");

        }
        async Task BindList()
        {
            var ListReturn = await Http.GetAsync("api/Employee/GetEmployees");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            EmployeeList = JsonConvert.DeserializeObject<List<EmployeeVM>>(vMs["data"].ToString());
            StateHasChanged();
        }
        async Task UpdateEmployeeBalance()
        {//
            if (EmployeeBalance.EmployeeId != 0)
            {
                string uri = "/api/EmployeeBalance/RecalualteBalance?EmployeeId=" + EmployeeBalance.EmployeeId;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    ShowConfirm = true;
                }
            }
            else
            {
                ShowCheckEmployee = true;
            }
        }
        void Confirm()
        {
            ShowConfirm = false;
        }
    }
}