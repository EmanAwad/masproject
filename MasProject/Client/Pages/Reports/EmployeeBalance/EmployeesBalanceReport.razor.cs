﻿using MasProject.Domain.ViewModel.Persons;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using Radzen.Blazor;
using Microsoft.EntityFrameworkCore;
using Radzen;
using MasProject.Domain.ViewModel.TransVM;
using System.Reflection;
using System;
using System.ComponentModel;
using static MasProject.Domain.Enums.Enums;
using Microsoft.JSInterop;
namespace MasProject.Client.Pages.Reports.EmployeeBalance
{
    public partial class EmployeesBalanceReport
    {
        private PageTitle PageTitle;
        RadzenGrid<TransEmployeeVM> grid;
        IEnumerable<TransEmployeeVM> TransEmployeeList;
        List<EmployeeVM> EmployeeList = new List<EmployeeVM>();
        TransEmployeeVM transEmployee = new TransEmployeeVM();
        EmpolyeeBalanceReportVM EmployeeBalance = new EmpolyeeBalanceReportVM();

        bool ShowReport = false;
        public void Export()
        {
            var query = new Query() { OrderBy = grid.Query.OrderBy, Filter = grid.Query.Filter };
            navigationManager.NavigateTo(query != null ? query.ToUrl($"/api/Report/EmployeeBalance/exportexcel") : $"/api/Report/EmployeeBalance/exportexcel", true);
        }
        void GeneratePDF()
        {
            var query = new Query() { OrderBy = grid.Query.OrderBy, Filter = grid.Query.Filter };
            navigationManager.NavigateTo(query != null ? query.ToUrl($"/api/Report/EmployeeBalance/exportPdf") : $"/api/Report/EmployeeBalance/exportPdf", true);
        }
        protected override async Task OnInitializedAsync()
        {
            var querystring = ExtensionMethods.QueryString(navigationManager);
            EmployeeBalance.EmployeeId = int.Parse(querystring["EmployeeId"]);
            EmployeeBalance.DateForm = querystring["DateFrom"] == "" ? DateTime.MinValue : Convert.ToDateTime(querystring["DateFrom"]);
            EmployeeBalance.DateTo = querystring["DateTo"] == "" ? DateTime.MinValue : Convert.ToDateTime(querystring["DateTo"]);
            string uri = "/api/EmployeeBalance/GetCurrentBalance?EmployeeId=" + EmployeeBalance.EmployeeId + "&DateFrom=" + EmployeeBalance.DateForm;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            var employee = JsonConvert.DeserializeObject<EmpolyeeBalanceReportVM>(vMs["data"].ToString());
            EmployeeBalance.EmployeeName = employee.EmployeeName;
            EmployeeBalance.Balance = employee.Balance;
            if (EmployeeBalance.DateForm != DateTime.MinValue)
                EmployeeBalance.DateForm = EmployeeBalance.DateForm.Value.ToLocalTime();
            if (EmployeeBalance.DateTo != DateTime.MinValue)
                EmployeeBalance.DateTo = EmployeeBalance.DateTo.Value.ToLocalTime();
            await Report(EmployeeBalance);
        }
        protected async Task Report(EmpolyeeBalanceReportVM EmployeeBalance)
        {
            var AddModel = JsonConvert.SerializeObject(EmployeeBalance);
            var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
            var result = await Http.PostAsync("api/EmployeeBalance/GetEmployeeBalance", content);
            var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
            TransEmployeeList = JsonConvert.DeserializeObject<IEnumerable<TransEmployeeVM>>(AddvMs["data"].ToString());
            ShowReport = true;
            StateHasChanged();
        }

        protected async Task OpenBill(TransEmployeeVM item)
        {
            var AddModel = JsonConvert.SerializeObject(item);
            var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
            var result = await Http.PostAsync("api/EmployeeBalance/GetBill", content);
            var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
            EmpolyeeBalanceReportVM NewModel = JsonConvert.DeserializeObject<EmpolyeeBalanceReportVM>(AddvMs["data"].ToString());
            await jsRuntime.InvokeAsync<object>("open", "/" + NewModel.PageName + "?ID=" + NewModel.EntryId + "&Type=View", "_blank");
        }
    }
}