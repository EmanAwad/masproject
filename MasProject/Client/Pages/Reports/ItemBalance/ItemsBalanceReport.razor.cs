﻿using MasProject.Domain.ViewModel.Items;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using Radzen.Blazor;
using Radzen;
using MasProject.Domain.ViewModel.TransVM;
using System;
using Microsoft.JSInterop;

namespace MasProject.Client.Pages.Reports.ItemBalance
{
    public partial class ItemsBalanceReport
    {
        private PageTitle PageTitle;
        RadzenGrid<TransItemVM> grid;
        IEnumerable<TransItemVM> TransItemList = new List<TransItemVM> { };
        TransItemVM transItem = new TransItemVM();
        ItemBalanceReportVM ItemBalance = new ItemBalanceReportVM();
        protected override async Task OnInitializedAsync()
        {
            var querystring = ExtensionMethods.QueryString(navigationManager);
            ItemBalance.ItemId = int.Parse(querystring["ItemId"]);
            ItemBalance.StoreId = int.Parse(querystring["StoreId"]);
            ItemBalance.DateForm= querystring["DateFrom"] == "" ? DateTime.MinValue : Convert.ToDateTime(querystring["DateFrom"]);
            ItemBalance.DateTo =  querystring["DateTo"] == "" ? DateTime.MinValue : Convert.ToDateTime(querystring["DateTo"]);
            string uri = "/api/ItemsBalance/GetCurrentBalance?ItemId=" + ItemBalance.ItemId + "&StoreId=" + ItemBalance.StoreId + "&DateFrom=" + ItemBalance.DateForm;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            var Item = JsonConvert.DeserializeObject<ItemBalanceReportVM>(vMs["data"].ToString());
            ItemBalance.ItemName = Item.ItemName;
            ItemBalance.StoreName = Item.StoreName;
            ItemBalance.Balance = Item.Balance;
            if (ItemBalance.DateForm != DateTime.MinValue)
                ItemBalance.DateForm = ItemBalance.DateForm.Value.ToLocalTime();
            if (ItemBalance.DateTo != DateTime.MinValue)
                ItemBalance.DateTo = ItemBalance.DateTo.Value.ToLocalTime();
            await Report(ItemBalance);
            StateHasChanged();
        }
        public void Export()
        {
            var query = new Query() { OrderBy = grid.Query.OrderBy, Filter = grid.Query.Filter };
            navigationManager.NavigateTo(query != null ? query.ToUrl($"/api/Report/ItemsBalance/exportexcel") : $"/api/Report/ItemsBalance/exportexcel", true);
        }
        void GeneratePDF()
        {
            var query = new Query() { OrderBy = grid.Query.OrderBy, Filter = grid.Query.Filter };
            navigationManager.NavigateTo(query != null ? query.ToUrl($"/api/Report/ItemsBalance/exportPdf") : $"/api/Report/ItemsBalance/exportPdf", true);
        }
        protected async Task Report(ItemBalanceReportVM ItemBalance)
        {
            var AddModel = JsonConvert.SerializeObject(ItemBalance);
            var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
            var result = await Http.PostAsync("api/ItemsBalance/GetItemBalance", content);
            var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
            TransItemList = JsonConvert.DeserializeObject<IEnumerable<TransItemVM>>(AddvMs["data"].ToString());
        }
        protected async Task OpenBill(TransItemVM item)
        {
            var AddModel = JsonConvert.SerializeObject(item);
            var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
            var result = await Http.PostAsync("api/ItemsBalance/GetBill", content);
            var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
            ItemBalanceReportVM NewModel = JsonConvert.DeserializeObject<ItemBalanceReportVM>(AddvMs["data"].ToString());
            await jsRuntime.InvokeAsync<object>("open", "/" + NewModel.PageName + "?ID=" + NewModel.BillId + "&Type=View", "_blank");
        }
    }
}