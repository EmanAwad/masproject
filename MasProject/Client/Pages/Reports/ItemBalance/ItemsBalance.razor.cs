﻿using MasProject.Domain.ViewModel.Items;
using MasProject.Domain.ViewModel.Hierarchy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using Radzen.Blazor;
using Radzen;
using MasProject.Domain.ViewModel.TransVM;
using Microsoft.JSInterop;
using System;

namespace MasProject.Client.Pages.Reports.ItemBalance
{
    public partial class ItemsBalance
    {
        private PageTitle PageTitle;
        ItemBalanceReportVM ItemBalance = new ItemBalanceReportVM();
        List<ItemVM> ItemList = new List<ItemVM>();
        List<StoreVM> StoreList = new List<StoreVM>();
        ItemVM Items = new ItemVM();
        bool ShowConfirm = false;
        bool ShowCheckItem = false;
        bool ShowCheckStore = false;
        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        protected async Task Report()
        {
            ItemBalance.ItemId = ItemList.Find(x => x.Name == Items.Name).ID;
            string DateFrom = ItemBalance.DateForm.HasValue ? ItemBalance.DateForm.Value.ToLocalTime().Date.ToString("yyyy- MM-dd") : null;
            string DateTo = ItemBalance.DateTo.HasValue ? ItemBalance.DateTo.Value.ToLocalTime().Date.ToString("yyyy- MM-dd") : null;
            await jsRuntime.InvokeAsync<object>("open", "/ItemBalanceReport?ItemId=" + ItemBalance.ItemId + "&StoreId=" + ItemBalance.StoreId + "&DateFrom=" + DateFrom + "&DateTo=" + DateTo, "_blank");
        }

        async Task UpdateItemBalance()
        {
            if (ItemBalance.StoreId != 0)
            {
                ItemBalance.ItemId = ItemList.Find(x => x.Name == Items.Name).ID;
                if (ItemBalance.ItemId != 0)
                {
                    string uri = "/api/ItemsBalance/RecalualteBalance?ItemId=" + ItemBalance.ItemId + "&StoreId=" + ItemBalance.StoreId;
                    var result = await Http.GetAsync(uri);
                    if (result.IsSuccessStatusCode)
                    {
                        ShowConfirm = true;
                    }
                }
                else
                {
                    ShowCheckItem = true;
                }
            }
            else
            {
                ShowCheckStore = true;
            }
        }
        void Confirm()
        {
            ShowConfirm = false;
        }
        async Task BindList()
        {
            //var ListReturn = await Http.GetAsync("api/Items/GetItems");
            //var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            //ItemList = JsonConvert.DeserializeObject<List<ItemVM>>(vMs["data"].ToString());

            //var List2Return = await Http.GetAsync("api/Store/GetStores");
            //var vMs2 = JObject.Parse(List2Return.Content.ReadAsStringAsync().Result);
            //StoreList = JsonConvert.DeserializeObject<List<StoreVM>>(vMs2["data"].ToString());

            var ListReturn = await Http.GetAsync("api/ItemsBalance/GetListDDL");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ItemBalance = JsonConvert.DeserializeObject<ItemBalanceReportVM>(vMs["data"].ToString());
            ItemList = ItemBalance.ItemList;
            StoreList = ItemBalance.StoreList;
            StateHasChanged();
        }
      

        async Task ReloadItemsList()
        {
            Items.Name = String.Empty;

            var ListReturn = await Http.GetAsync("api/Items/GetItems");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ItemList = JsonConvert.DeserializeObject<List<ItemVM>>(vMs["data"].ToString());

        }
    }
}