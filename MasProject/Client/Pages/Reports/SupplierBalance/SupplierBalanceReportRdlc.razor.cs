﻿using MasProject.Client.Shared;
using MasProject.Domain.ViewModel.TransVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MasProject.Client.Pages.Reports.SupplierBalance
{
    public partial class SupplierBalanceReportRdlc
    {
        SupplierBalanceReportVM BalanceModel = new SupplierBalanceReportVM();
        string ReportResult = "";
        string ReportResultExcel = "";
        protected override async Task OnInitializedAsync()
        {
            var querystring = ExtensionMethods.QueryString(navigationManager);
            BalanceModel.SupplierId = int.Parse(querystring["Id"]);
            bool IsPdf = bool.Parse(querystring["IsPdf"]);
            DateTime DateFrom = DateTime.Parse(querystring["DateFrom"]);
            DateTime DateTo = DateTime.Parse(querystring["DateTo"]);
            string uri = "/api/SupplierBalance/SupplierBalanceReport?Id=" + BalanceModel.SupplierId + "&IsPdf="+IsPdf+"&DateFrom=" + DateFrom + "&DateTo=" + DateTo + "";
            if (IsPdf)
            {
                ReportResult = uri;
            }
            else
            {
                ReportResultExcel = uri;
            }
        }
    }
}