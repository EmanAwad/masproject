﻿using MasProject.Domain.ViewModel.Persons;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using Radzen.Blazor;
using Microsoft.EntityFrameworkCore;
using Radzen;
using MasProject.Domain.ViewModel.TransVM;
using System.Reflection;
using System;
using System.ComponentModel;
using static MasProject.Domain.Enums.Enums;
using Microsoft.JSInterop;
namespace MasProject.Client.Pages.Reports.SupplierBalance
{
    public partial class SuppliersBalanceReport
    {
        private PageTitle PageTitle;
        RadzenGrid<TransSupplierVM> grid;
        IEnumerable<TransSupplierVM> TransSupplierList;
        TransSupplierVM transSupplier = new TransSupplierVM();
        SupplierBalanceReportVM SupplierBalance = new SupplierBalanceReportVM();
        bool ShowReport = false;
        protected async Task Export()
        {
            DateTime DateFrom = SupplierBalance.DateForm.HasValue ? SupplierBalance.DateForm.Value.ToLocalTime().Date : DateTime.Now;
            DateTime DateTo = SupplierBalance.DateTo.HasValue ? SupplierBalance.DateTo.Value.ToLocalTime().Date : DateTime.Now;
            await jsRuntime.InvokeAsync<object>("open", "/SupplierBalanceReportRdlc?Id=" + SupplierBalance.SupplierId + "&IsPdf=false&DateFrom=" + DateFrom + "&DateTo=" + DateTo + "", "_blank");
        }
        protected async Task GeneratePDF()
        {

            DateTime DateFrom = SupplierBalance.DateForm.HasValue ? SupplierBalance.DateForm.Value.ToLocalTime().Date : DateTime.Now;
            DateTime DateTo = SupplierBalance.DateTo.HasValue ? SupplierBalance.DateTo.Value.ToLocalTime().Date : DateTime.Now;
            await jsRuntime.InvokeAsync<object>("open", "/SupplierBalanceReportRdlc?Id=" + SupplierBalance.SupplierId + "&IsPdf=true&DateFrom=" + DateFrom + "&DateTo=" + DateTo+ "", "_blank");

        }
        protected override async Task OnInitializedAsync()
        {
            
            var querystring = ExtensionMethods.QueryString(navigationManager);
            SupplierBalance.SupplierId = int.Parse(querystring["Id"]);
            SupplierBalance.DateForm = querystring["DateFrom"] == "" ? DateTime.MinValue : Convert.ToDateTime(querystring["DateFrom"]);
            SupplierBalance.DateTo = querystring["DateTo"] == "" ? DateTime.MinValue : Convert.ToDateTime(querystring["DateTo"]);
            string uri = "/api/SupplierBalance/GetCurrentBalance?SupplierId=" + SupplierBalance.SupplierId + "&DateFrom=" + SupplierBalance.DateForm;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            var supplier = JsonConvert.DeserializeObject<SupplierBalanceReportVM>(vMs["data"].ToString());
            SupplierBalance.SupplierName = supplier.SupplierName;
            SupplierBalance.Balance = supplier.Balance;
            if (SupplierBalance.DateForm != DateTime.MinValue)
                SupplierBalance.DateForm = SupplierBalance.DateForm.Value.ToLocalTime();
            if (SupplierBalance.DateTo != DateTime.MinValue)
                SupplierBalance.DateTo = SupplierBalance.DateTo.Value.ToLocalTime();
            await Report(SupplierBalance);
        }
        protected async Task Report(SupplierBalanceReportVM SupplierBalance)
        {
            var AddModel = JsonConvert.SerializeObject(SupplierBalance);
            var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
            var result = await Http.PostAsync("api/SupplierBalance/GetSupplierBalance", content);
            var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
            TransSupplierList = JsonConvert.DeserializeObject<IEnumerable<TransSupplierVM>>(AddvMs["data"].ToString());
            ShowReport = true;
            StateHasChanged();
        }

        protected async Task OpenBill(TransSupplierVM item)
        {
            var AddModel = JsonConvert.SerializeObject(item);
            var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
            var result = await Http.PostAsync("api/SupplierBalance/GetBill", content);
            var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
            SupplierBalanceReportVM newModel = JsonConvert.DeserializeObject<SupplierBalanceReportVM>(AddvMs["data"].ToString());
            await jsRuntime.InvokeAsync<object>("open", "/" + newModel.PageName + "?ID=" + newModel.BillId + "", "_blank");
        }
    }
}