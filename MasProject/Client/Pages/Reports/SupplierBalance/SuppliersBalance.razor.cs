﻿using MasProject.Domain.ViewModel.Persons;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using Radzen.Blazor;
using Microsoft.EntityFrameworkCore;
using Radzen;
using MasProject.Domain.ViewModel.TransVM;
using System.Reflection;
using System;
using System.ComponentModel;
using static MasProject.Domain.Enums.Enums;
using Microsoft.JSInterop;
namespace MasProject.Client.Pages.Reports.SupplierBalance
{
    public partial class SuppliersBalance
    {
        private PageTitle PageTitle;
        SupplierBalanceReportVM SupplierBalance = new SupplierBalanceReportVM();
        List<SupplierVM> SupplierList = new List<SupplierVM>();
        bool ShowConfirm = false;
        bool ShowCheckSupplier = false;
        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        protected async Task Report()
        {
            DateTime DateFrom = SupplierBalance.DateForm.HasValue ? SupplierBalance.DateForm.Value.ToLocalTime().Date : DateTime.Now;
            DateTime DateTo = SupplierBalance.DateTo.HasValue ? SupplierBalance.DateTo.Value.ToLocalTime().Date : DateTime.Now;
            await jsRuntime.InvokeAsync<object>("open", "/SupplierBalanceReport?Id=" + SupplierBalance.SupplierId + "&DateFrom=" + DateFrom + "&DateTo=" + DateTo, "_blank");

        }
        async Task BindList()
        {
            var ListReturn = await Http.GetAsync("api/Supplier/GetSuppliers");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            SupplierList = JsonConvert.DeserializeObject<List<SupplierVM>>(vMs["data"].ToString());
            StateHasChanged();
        }

        async Task UpdateSupplierBalance()
        {//
            if (SupplierBalance.SupplierId != 0)
            {
                string uri = "/api/SupplierBalance/RecalualteBalance?SupplierId=" + SupplierBalance.SupplierId;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    ShowConfirm = true;
                }
            }
            else
            {
                ShowCheckSupplier = true;
            }
        }
        void Confirm()
        {
            ShowConfirm = false;
        }
    }
}