﻿using MasProject.Domain.ViewModel.Items;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using Radzen.Blazor;
using Radzen;
using MasProject.Domain.ViewModel.TransVM;
using System.Linq;
using Microsoft.JSInterop;
using MasProject.Domain.ViewModel.Hierarchy;
using static MasProject.Domain.Enums.Enums;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Persons;

namespace MasProject.Client.Pages.Reports.StoreReport
{
    public partial class StoreReport
    {
        private PageTitle PageTitle;
        ItemBalanceReportVM ItemBalance = new ItemBalanceReportVM();
        List<ItemCollectionVM> ItemCollectionList = new List<ItemCollectionVM>();
        List<StoreTypeEnum> TransactionTypeList = new List<StoreTypeEnum>();
        List<BranchVM> BranchList = new List<BranchVM>();
        List<StoreVM> StoreList = new List<StoreVM>();
        List<LookupKeyValueVM> ClientList = new List<LookupKeyValueVM>();
        bool ShowReport = false;
        RadzenGrid<StoreReportVM> grid;
        IEnumerable<StoreReportVM> TransItemList = new List<StoreReportVM> { };
        StoreReportVM transItem = new StoreReportVM();
        ClientsVM clients = new ClientsVM();

        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }

        protected async Task Report()
        {
            ItemBalance.PersionId = ClientList.Find(x => x.Name == clients.Name).ID;

            var AddModel = JsonConvert.SerializeObject(ItemBalance);
            var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
            var result = await Http.PostAsync("api/StoreReport/GetStoreReport", content);
            var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
            TransItemList = JsonConvert.DeserializeObject<IEnumerable<StoreReportVM>>(AddvMs["data"].ToString());
            List<StoreReportVM> asList = TransItemList.ToList();
            ItemBalance.BillsNumber = asList.Count;
            ItemBalance.BillsTotal = asList.Sum(x=>x.Total);
            StateHasChanged();
            ShowReport = true;
        }

        async Task BindList()
        {
            var ListReturn = await Http.GetAsync("api/StoreReport/GetListDDL");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ItemBalance = JsonConvert.DeserializeObject<ItemBalanceReportVM>(vMs["data"].ToString());
            TransactionTypeList = ItemBalance.TransactionTypeList;
            BranchList = ItemBalance.BranchList;
            ItemCollectionList = ItemBalance.ItemCollectionList;
            StoreList = ItemBalance.StoreList;
            ClientList = ItemBalance.PersionList;
            StateHasChanged();
        }


        public void Export()
        {
            var query = new Query() { OrderBy = grid.Query.OrderBy, Filter = grid.Query.Filter };
            navigationManager.NavigateTo(query != null ? query.ToUrl($"/api/Report/StoreReport/exportexcel") : $"/api/Report/StoreReport/exportexcel", true);
        }
        void GeneratePDF()
        {
            var query = new Query() { OrderBy = grid.Query.OrderBy, Filter = grid.Query.Filter };
            navigationManager.NavigateTo(query != null ? query.ToUrl($"/api/Report/StoreReport/exportPdf") : $"/api/Report/StoreReport/exportPdf", true);
        }
       
        protected async Task OpenBill(StoreReportVM item)
        {
            var AddModel = JsonConvert.SerializeObject(item);
            var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
            var result = await Http.PostAsync("api/StoreReport/GetBill", content);
            var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
            ItemBalanceReportVM NewModel = JsonConvert.DeserializeObject<ItemBalanceReportVM>(AddvMs["data"].ToString());
            await jsRuntime.InvokeAsync<object>("open", "/" + NewModel.PageName + "?ID=" + NewModel.BillId + "&Type=View", "_blank");
        }
    }
}