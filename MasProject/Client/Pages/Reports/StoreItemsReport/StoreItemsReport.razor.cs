﻿using MasProject.Domain.ViewModel.Items;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using Radzen.Blazor;
using Radzen;
using MasProject.Domain.ViewModel.TransVM;
using System;
using Microsoft.JSInterop;
using MasProject.Domain.ViewModel.Hierarchy;
using static MasProject.Domain.Enums.Enums;

namespace MasProject.Client.Pages.Reports.StoreItemsReport
{
    public partial class StoreItemsReport
    {
        private PageTitle PageTitle;
        ItemBalanceReportVM ItemBalance = new ItemBalanceReportVM();
        List<ItemVM> ItemList = new List<ItemVM>();
        List<ItemCollectionVM> ItemCollectionList = new List<ItemCollectionVM>();
        List<StoreTypeEnum> TransactionTypeList = new List<StoreTypeEnum>();
        List<BranchVM> BranchList = new List<BranchVM>();
        ItemVM Items = new ItemVM();
        bool ShowCheckItem = false;
        bool ShowReport = false;
        RadzenGrid<StoreReportVM> grid;
        IEnumerable<StoreReportVM> TransItemList = new List<StoreReportVM> { };
        StoreReportVM transItem = new StoreReportVM();

        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }

        protected async Task Report()
        {
            //if (Items.Name == "" && Items.Name == null)
            //{
            //    ShowCheckItem = true;
            //}
            //else
            //{
            try
            {
                ItemBalance.ItemId = ItemList.Find(x => x.Name == Items.Name).ID;
                ItemBalance.ItemName = Items.Name;
            }
            catch (Exception)
            {

            }
            //ItemBalance.Balance = 0;
            var AddModel = JsonConvert.SerializeObject(ItemBalance);
                var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/StoreItemsReport/GetStoreItemsReport", content);
                var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                TransItemList = JsonConvert.DeserializeObject<IEnumerable<StoreReportVM>>(AddvMs["data"].ToString());
                StateHasChanged();
                ShowReport = true;
           // }
        }

        async Task BindList()
        {
            var ListReturn = await Http.GetAsync("api/StoreItemsReport/GetListDDL");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ItemBalance = JsonConvert.DeserializeObject<ItemBalanceReportVM>(vMs["data"].ToString());
            ItemList = ItemBalance.ItemList;
            TransactionTypeList = ItemBalance.TransactionTypeList;
            BranchList = ItemBalance.BranchList;
            ItemCollectionList = ItemBalance.ItemCollectionList;
            StateHasChanged();
        }

        public void Export()
        {
            var query = new Query() { OrderBy = grid.Query.OrderBy, Filter = grid.Query.Filter };
            navigationManager.NavigateTo(query != null ? query.ToUrl($"/api/Report/StoreItemsReport/exportexcel") : $"/api/Report/StoreItemsReport/exportexcel", true);
        }
        void GeneratePDF()
        {
            var query = new Query() { OrderBy = grid.Query.OrderBy, Filter = grid.Query.Filter };
            navigationManager.NavigateTo(query != null ? query.ToUrl($"/api/Report/StoreItemsReport/exportPdf") : $"/api/Report/StoreItemsReport/exportPdf", true);
        }
        async Task ReloadItemsList()
        {
            Items.Name = String.Empty;

            var ListReturn = await Http.GetAsync("api/Items/GetItems");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ItemList = JsonConvert.DeserializeObject<List<ItemVM>>(vMs["data"].ToString());

        }
        protected async Task OpenBill(StoreReportVM item)
        {
            var AddModel = JsonConvert.SerializeObject(item);
            var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
            var result = await Http.PostAsync("api/StoreItemsReport/GetBill", content);
            var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
            ItemBalanceReportVM NewModel = JsonConvert.DeserializeObject<ItemBalanceReportVM>(AddvMs["data"].ToString());
            await jsRuntime.InvokeAsync<object>("open", "/" + NewModel.PageName + "?ID=" + NewModel.BillId + "&Type=View", "_blank");
        }
    }
}