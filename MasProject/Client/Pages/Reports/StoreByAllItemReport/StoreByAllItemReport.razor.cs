﻿using MasProject.Domain.ViewModel.Items;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using Radzen.Blazor;
using Radzen;
using MasProject.Domain.ViewModel.TransVM;
using System;
using Microsoft.JSInterop;
using MasProject.Domain.ViewModel.Hierarchy;

namespace MasProject.Client.Pages.Reports.StoreByAllItemReport
{
    public partial class StoreByAllItemReport
    {
        private PageTitle PageTitle;
        ItemBalanceReportVM ItemBalance = new ItemBalanceReportVM();
        List<StoreVM> StoreList = new List<StoreVM>();
       // bool ShowCheckItem = false;
        bool ShowReport = false;
        RadzenGrid<StoreReportVM> grid;
        IEnumerable<StoreReportVM> TransItemList = new List<StoreReportVM> { };
        StoreReportVM transItem = new StoreReportVM();
        List<ItemCollectionVM> ItemCollectionList = new List<ItemCollectionVM>();

        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }

        protected async Task Report()
        {
           
            var AddModel = JsonConvert.SerializeObject(ItemBalance);
            var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
            var result = await Http.PostAsync("api/StoreByAllItemReport/GetStoreByAllItemReport", content);
            var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
            TransItemList = JsonConvert.DeserializeObject<IEnumerable<StoreReportVM>>(AddvMs["data"].ToString());
            StateHasChanged();
            ShowReport = true;
        }

        async Task BindList()
        {
            var ListReturn = await Http.GetAsync("api/StoreByAllItemReport/GetListDDL");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ItemBalance = JsonConvert.DeserializeObject<ItemBalanceReportVM>(vMs["data"].ToString());
            ItemCollectionList = ItemBalance.ItemCollectionList;
            StoreList = ItemBalance.StoreList;
            StateHasChanged();
        }

        public void Export()
        {
            var query = new Query() { OrderBy = grid.Query.OrderBy, Filter = grid.Query.Filter };
            navigationManager.NavigateTo(query != null ? query.ToUrl($"/api/Report/StoreByAllItemReport/exportexcel") : $"/api/Report/StoreByAllItemReport/exportexcel", true);
        }
        void GeneratePDF()
        {
            var query = new Query() { OrderBy = grid.Query.OrderBy, Filter = grid.Query.Filter };
            navigationManager.NavigateTo(query != null ? query.ToUrl($"/api/Report/StoreByAllItemReport/exportPdf") : $"/api/Report/StoreByAllItemReport/exportPdf", true);
        }
       
        protected async Task OpenBill(StoreReportVM item)
        {
            var AddModel = JsonConvert.SerializeObject(item);
            var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
            var result = await Http.PostAsync("api/StoreByAllItemReport/GetBill", content);
            var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
            ItemBalanceReportVM NewModel = JsonConvert.DeserializeObject<ItemBalanceReportVM>(AddvMs["data"].ToString());
            await jsRuntime.InvokeAsync<object>("open", "/" + NewModel.PageName + "?ID=" + NewModel.BillId + "&Type=View", "_blank");
        }
    } 
}