﻿using MasProject.Domain.ViewModel.Persons;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using Radzen.Blazor;
using Radzen;
using MasProject.Domain.ViewModel.TransVM;
using System;
using Microsoft.JSInterop;
namespace MasProject.Client.Pages.Reports.ClientBalance
{
    public partial class ClientsBalanceReport
    {
        private PageTitle PageTitle;
        RadzenGrid<TransClientVM> grid;
        IEnumerable<TransClientVM> TransClientList= new List<TransClientVM> { };
        List<TransClientVM> TransClientList2 = new List<TransClientVM>();
        TransClientVM transClient = new TransClientVM();
        ClientBalanceReportVM clientBalance = new ClientBalanceReportVM();
        protected override async Task OnInitializedAsync()
        {
            var querystring = ExtensionMethods.QueryString(navigationManager);
            clientBalance.ClientId = int.Parse(querystring["ClientId"]);
            clientBalance.DateForm= querystring["DateFrom"] == "" ? DateTime.MinValue : Convert.ToDateTime(querystring["DateFrom"]);
            clientBalance.DateTo =  querystring["DateTo"] == "" ? DateTime.MinValue : Convert.ToDateTime(querystring["DateTo"]);
            string uri = "/api/ClientsBalance/GetCurrentBalance?ClientId=" + clientBalance.ClientId +"&DateFrom=" + clientBalance.DateForm;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            var client = JsonConvert.DeserializeObject<ClientBalanceReportVM>(vMs["data"].ToString());
            clientBalance.ClientName = client.ClientName;
            clientBalance.Balance = client.Balance;
            if (clientBalance.DateForm != DateTime.MinValue)
                clientBalance.DateForm = clientBalance.DateForm.Value.ToLocalTime();
            if (clientBalance.DateTo != DateTime.MinValue)
                clientBalance.DateTo = clientBalance.DateTo.Value.ToLocalTime();
            await Report(clientBalance);
            StateHasChanged();
        }
        
     protected async Task Export()
        {
            DateTime DateFrom = clientBalance.DateForm.HasValue ? clientBalance.DateForm.Value.ToLocalTime().Date : DateTime.Now;
            DateTime DateTo = clientBalance.DateTo.HasValue ? clientBalance.DateTo.Value.ToLocalTime().Date : DateTime.Now;
            await jsRuntime.InvokeAsync<object>("open", "/clientBalanceReportRdlc?Id=" + clientBalance.ClientId + "&IsPdf=false&DateFrom=" + DateFrom + "&DateTo=" + DateTo + "", "_blank");
        }
        protected async Task GeneratePDF()
        {

            DateTime DateFrom = clientBalance.DateForm.HasValue ? clientBalance.DateForm.Value.ToLocalTime().Date : DateTime.Now;
            DateTime DateTo = clientBalance.DateTo.HasValue ? clientBalance.DateTo.Value.ToLocalTime().Date : DateTime.Now;
            await jsRuntime.InvokeAsync<object>("open", "/clientBalanceReportRdlc?Id=" + clientBalance.ClientId + "&IsPdf=true&DateFrom=" + DateFrom + "&DateTo=" + DateTo + "", "_blank");

        }
        protected async Task Report(ClientBalanceReportVM clientBalance)
        {
            var AddModel = JsonConvert.SerializeObject(clientBalance);
            var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
            var result = await Http.PostAsync("api/ClientsBalance/GetclientBalance", content);
            var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
            TransClientList = JsonConvert.DeserializeObject<IEnumerable<TransClientVM>>(AddvMs["data"].ToString());
        }
        protected async Task OpenBill(TransClientVM item)
        {
            var AddModel = JsonConvert.SerializeObject(item);
            var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
            var result = await Http.PostAsync("api/ClientsBalance/GetBill", content);
            var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
            ClientBalanceReportVM NewModel = JsonConvert.DeserializeObject<ClientBalanceReportVM>(AddvMs["data"].ToString());
            await jsRuntime.InvokeAsync<object>("open", "/" + NewModel.PageName + "?ID=" + NewModel.BillId + "", "_blank");
        }
    }
}