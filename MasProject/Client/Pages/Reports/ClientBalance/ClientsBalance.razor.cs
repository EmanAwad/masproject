﻿using MasProject.Domain.ViewModel.Persons;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using Radzen.Blazor;
using Radzen;
using MasProject.Domain.ViewModel.TransVM;
using Microsoft.JSInterop;
using System;

namespace MasProject.Client.Pages.Reports.ClientBalance
{
    public partial class ClientsBalance
    {
        private PageTitle PageTitle;
        ClientBalanceReportVM clientBalance = new ClientBalanceReportVM();
        List<ClientsVM> ClientList = new List<ClientsVM>();
        ClientsVM clients = new ClientsVM();
        bool ShowConfirm = false;
        bool ShowCheckClient = false;
        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        protected async Task Report()
        {
            clientBalance.ClientId = ClientList.Find(x => x.Name == clients.Name).ID;
            string DateFrom = clientBalance.DateForm.HasValue ? clientBalance.DateForm.Value.ToLocalTime().Date.ToString("yyyy- MM-dd") : null;
            string DateTo = clientBalance.DateTo.HasValue ? clientBalance.DateTo.Value.ToLocalTime().Date.ToString("yyyy- MM-dd") :  null;
            await jsRuntime.InvokeAsync<object>("open", "/ClientBalanceReport?ClientId=" + clientBalance.ClientId + "&DateFrom=" + DateFrom + "&DateTo=" + DateTo, "_blank");

        }

        async Task UpdateClientBalance()
        {//
            if (clientBalance.ClientId != 0)
            {
                string uri = "/api/ClientsBalance/RecalualteBalance?ClientId=" + clientBalance.ClientId;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    //    var ListReturn = await Http.GetAsync(");
                    //var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
                    //string returnResult = JsonConvert.DeserializeObject<string>(vMs["data"].ToString());
                    //if (returnResult == "Done")
                    //{
                    ShowConfirm = true;
                }
            }
            else
            {
                ShowCheckClient = true;
            }
        }
        void Confirm()
        {
            ShowConfirm = false;
        }
        async Task BindList()
        {
            var ListReturn = await Http.GetAsync("api/Client/GetClients");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ClientList = JsonConvert.DeserializeObject<List<ClientsVM>>(vMs["data"].ToString());
            StateHasChanged();
        }
        //void EraseText()
        //{
        //    clients.Name = String.Empty;
        //}

        async Task ReloadClientsList()
        {
            clients.Name = String.Empty;

            var ListReturn = await Http.GetAsync("api/Client/GetClients");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ClientList = JsonConvert.DeserializeObject<List<ClientsVM>>(vMs["data"].ToString());

        }
    }
}