﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using System;
using MasProject.Domain.ViewModel;
using MasProject.Client.Shared;
using Microsoft.JSInterop;
using System.Threading.Tasks;

namespace MasProject.Client.Pages
{
    public partial class Logout
    {
        protected override async Task OnInitializedAsync()
        {
            HttpResponseMessage result = new HttpResponseMessage();
            try
            {
                result = await Http.GetAsync("api/Auth/Logout");
                //not redirect 
                //if (result.IsSuccessStatusCode)
                //{
                //    await jsRuntime.InvokeAsync<object>("open", "/", "_self");
                //}
            }
            catch (Exception ex)
            {
            }
        }
    }
}
