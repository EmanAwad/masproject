﻿using MasProject.Domain.ViewModel.Hierarchy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Domain.ViewModel.StoreTransaction.Quotation;
using MasProject.Domain.ViewModel.Items;
using MasProject.Client.Shared;
using MasProject.Domain.ViewModel;
using MasProject.Domain.Services;
using System;
using static MasProject.Domain.Enums.Enums;
using MasProject.Data.Models.Items;
using MasProject.Domain.Services.Items;
using MasProject.Data.DataAccess;

namespace MasProject.Client.Pages.StoreTransaction.Quotations
{
    public partial class QuotationAdd
    {
        private PageTitle PageTitle;
        bool ShowConfirm = false;
        bool FirstAdd = false;
        QuotationItemsVM QItemModel = new QuotationItemsVM();
        List<QuotationItemsVM> QItemList = new List<QuotationItemsVM>();
        QuotationVM QuotModel = new QuotationVM();
        IEnumerable<QuotationVM> QuotModelList;
        List<BranchVM> BranchList = new List<BranchVM>();
        List<ItemVM> ItemList2 = new List<ItemVM>();
        ItemVM ItemModel = new ItemVM();
        //List<ClientsVM> ClientList = new List<ClientsVM>();
        List<DealTypeVM> DealTypeList = new List<DealTypeVM>();
        List<LookupItem> ItemList = new List<LookupItem>();
        string TotalInWords = "";
        string TotalInWordsTax = "";
        bool ShowTaxPart = false;
        int FlagTypeID = 0;
        string ItemImg = "";
        //ItemVM ItemSrc = new ItemVM();
        bool ShowImg = false;
        bool SelectDealTypeChecker = false;
        bool IsDisabled = false;
        // List<Item> txtobj;

        //   ItemServices itemServices;
        protected override async Task OnInitializedAsync()
        {
            ItemImg = "";
            FirstAdd = false;
            QItemModel = new QuotationItemsVM { Identifer = 1 };
            QItemList = new List<QuotationItemsVM>();
            QItemList.Add(QItemModel);
            QuotModel = new QuotationVM();
            QuotModel.Date = DateTime.Now;
            QuotModel.DiscountPrecentage = 0;
            QuotModel.DiscountValue = 0;
            //var IdReturn = await Http.GetAsync("api/Quotation/GenerateSerial");
            //var IdvMs = JObject.Parse(IdReturn.Content.ReadAsStringAsync().Result);
            //QuotModel.SerialNumber = JsonConvert.DeserializeObject<int>(IdvMs["data"].ToString());
            await BindList();
            //txtobj = itemServices.ItemSearch();
        }
        async Task BindList()
        {
            //var BranchListReturn = await Http.GetAsync("api/Branch/GetBranchs");
            //var BranchvMs = JObject.Parse(BranchListReturn.Content.ReadAsStringAsync().Result);
            //BranchList = JsonConvert.DeserializeObject<List<BranchVM>>(BranchvMs["data"].ToString());

            ////var ClientListReturn = await Http.GetAsync("api/Client/GetClient");
            ////var ClientvMs = JObject.Parse(ClientListReturn.Content.ReadAsStringAsync().Result);
            ////ClientList = JsonConvert.DeserializeObject<List<ClientsVM>>(ClientvMs["data"].ToString());

            //var GetDealTypeListReturn = await Http.GetAsync("api/DealType/GetDealType");
            //var GetDealTypevMs = JObject.Parse(GetDealTypeListReturn.Content.ReadAsStringAsync().Result);
            //DealTypeList = JsonConvert.DeserializeObject<List<DealTypeVM>>(GetDealTypevMs["data"].ToString());

            //var ItemListReturn = await Http.GetAsync("api/Items/GetItemsList");
            //var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
            //ItemList2 = JsonConvert.DeserializeObject<List<ItemVM>>(ItemvMs["data"].ToString());
            var SalesListReturn = await Http.GetAsync("api/Quotation/GetListDDL");
            var SalesvMs = JObject.Parse(SalesListReturn.Content.ReadAsStringAsync().Result);
            QuotModel = JsonConvert.DeserializeObject<QuotationVM>(SalesvMs["data"].ToString());
           
            DealTypeList = QuotModel.DealTypeList;
            ItemList = QuotModel.ItemList;
            BranchList = QuotModel.BranchList;

        }
        async Task GetSerialNumber()
        {
            string serailuri = "/api/Quotation/GenerateSerial?BranchId=" + QuotModel.BranchId;
            HttpResponseMessage serialReturn = await Http.GetAsync(serailuri);
            var serialvms = JObject.Parse(serialReturn.Content.ReadAsStringAsync().Result);
            QuotModel.SerialNumber = JsonConvert.DeserializeObject<int>(serialvms["data"].ToString());
            StateHasChanged();

        }
        //async Task ClientRenew()
        //{
        //    var ClientListReturn = await Http.GetAsync("api/Client/GetClient");
        //    var ClientvMs = JObject.Parse(ClientListReturn.Content.ReadAsStringAsync().Result);
        //    ClientList = JsonConvert.DeserializeObject<List<ClientsVM>>(ClientvMs["data"].ToString());
        //}
        async Task ItemsRenew(QuotationItemsVM row)
        {
            var ItemListReturn = await Http.GetAsync("api/Items/GetItem");
            var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
            ItemList = JsonConvert.DeserializeObject<List<LookupItem>>(ItemvMs["data"].ToString());
            //get price for selected items
            //string priceuri = "/api/Items/GetItemPriceByDealId?DealId=" + QuotModel.DealTypeId + "&ItemId=" + row.ItemId;
            //HttpResponseMessage priceresponse = await Http.GetAsync(priceuri);
            //var pricevMs = JObject.Parse(priceresponse.Content.ReadAsStringAsync().Result);
            //row.Price = JsonConvert.DeserializeObject<decimal>(pricevMs["data"].ToString());
             LookupItem SelectedITem = ItemList.Find(x => (x.BoxCode == row.Box_ParCode || x.GuaranteeParCode == row.Box_ParCode || x.InterationalParCode == row.Box_ParCode || x.Code == row.Box_ParCode || x.ParCodeNote == row.Box_ParCode || x.SupplierParCode == row.Box_ParCode));
           // LookupItem SelectedITem = ItemList.Find(x => (x.Name == row.ItemName));
            ItemImg = SelectedITem.ItemImg ?? "";
            row.ItemId = SelectedITem.ID;
            if (QuotModel.DealTypeId != null && QuotModel.DealTypeId != 0)
            {
                row.Price = GetPriceFromDealType((int)QuotModel.DealTypeId, SelectedITem);
            }
            else
            {
                SelectDealTypeChecker = true;
            }
            RecalculateTotal();
            StateHasChanged();
        }
       
        void ChangeItemQuantity()
        {
            RecalculateTotal();
            AddNewRow();
        }
        //void Confirm()
        //{
        //    //await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        //    //changed 30-10-2020// protected async Task
        //    ShowConfirm = false;
        //    IsDisabled = false;
        //}
        protected async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        }
        void AddNewRow()
        {
            int IdentifierCount = QItemList.Count;
            QItemModel = new QuotationItemsVM { Identifer = IdentifierCount + 1 };
            QItemList.Add(QItemModel);
            StateHasChanged();
        }
        void RemoveRow(QuotationItemsVM row)
        {
            QItemList.Remove(row);
            StateHasChanged();
        }
        protected async Task EditQuotation()
        {
            IsDisabled = true;
            HttpResponseMessage result = new HttpResponseMessage();
            if (FirstAdd == false)
            {
              
                QuotModel.CreatedDate = QuotModel.CreatedDate.ToLocalTime();
                var AddModel = JsonConvert.SerializeObject(QuotModel);
                var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                result = await Http.PostAsync("api/Quotation/AddQuotation", content);
                var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                QuotModel.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                FirstAdd = true;
            }
            else

            {

                QuotModel.CreatedDate = DateTime.Now;
                QuotModel.QuotationItems = QItemList;
                var EditModel = JsonConvert.SerializeObject(QuotModel);
                var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                result = await Http.PostAsync("api/Quotation/EditQuotation", content);
            }
            if (result.IsSuccessStatusCode)
            {
                ShowConfirm = true;
            }
        }
        protected async Task GetItemDataByCode(QuotationItemsVM row)
        {
           //  LookupItem SelectedITem = ItemList.Find(x => (x.BoxCode == row.Box_ParCode || x.GuaranteeParCode == row.Box_ParCode || x.InterationalParCode == row.Box_ParCode || x.Code == row.Box_ParCode || x.ParCodeNote == row.Box_ParCode || x.SupplierParCode == row.Box_ParCode));
           LookupItem SelectedITem = ItemList.Find(x => x.Name == row.ItemName);
            ItemImg = SelectedITem.ItemImg ?? "";
               row.ItemId = SelectedITem.ID;
            row.ItemName = SelectedITem.Name;
            row.Box_ParCode = SelectedITem.BoxCode;
            
            if (QuotModel.DealTypeId != null && QuotModel.DealTypeId!=0)
            {
                row.Price = GetPriceFromDealType((int)QuotModel.DealTypeId, SelectedITem);
            }
            else
            {
                SelectDealTypeChecker = true;
            }
            row.Quantity = null;
            row.Total = null;
            HttpResponseMessage result = new HttpResponseMessage();
            //check for save
            if (QuotModel.SerialNumber != 0)
            {
                //string uri = "/api/Items/GetItemByAnyCode?AnyCode=" + row.Box_ParCode;
                //HttpResponseMessage response = await Http.GetAsync(uri);
                //var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                //ItemVM temp = JsonConvert.DeserializeObject<ItemVM>(vMs["data"].ToString());
                //row.Box_ParCode = temp.National_ParCode;
                //row.ItemId = temp.ID;
                //string priceuri = "/api/Items/GetItemPriceByDealId?DealId=" + QuotModel.DealTypeId + "&ItemId=" + row.ItemId;
                //HttpResponseMessage priceresponse = await Http.GetAsync(priceuri);
                //var pricevMs = JObject.Parse(priceresponse.Content.ReadAsStringAsync().Result);
                //row.Price = JsonConvert.DeserializeObject<decimal>(pricevMs["data"].ToString());
                //row.Quantity = null;
                //row.Total = null;
                ////new 09-11-2020
                //string uriImage = "/api/Items/GetItemImage?ItemId=" + row.ItemId;
                //HttpResponseMessage responseImage = await Http.GetAsync(uriImage);
                //var vMsImage = JObject.Parse(responseImage.Content.ReadAsStringAsync().Result);
                //ItemSrc = JsonConvert.DeserializeObject<ItemVM>(vMsImage["data"].ToString());
                //ItemImg = ItemSrc.ItemImg;
                if (FirstAdd == false)
                {
                    var AddModel = JsonConvert.SerializeObject(QuotModel);
                    var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/Quotation/AddQuotation", content);
                    var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    QuotModel.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                    FirstAdd = true;
                }
                else
                {
                    QuotModel.QuotationItems = QItemList;
                    var EditModel = JsonConvert.SerializeObject(QuotModel);
                    var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/Quotation/EditQuotation", content);
                }
                if (result.IsSuccessStatusCode)
                {
                    StateHasChanged();
                }
            }
        }
        private decimal GetPriceFromDealType(int DealTypeId, LookupItem SelectedITem)
        {
            float price = 0;
            switch (DealTypeId)
            {
                case (int)EnPriceTypes.التكلفة:
                    price = SelectedITem.CostPrice;
                    break;
                case (int)EnPriceTypes.الموزع:
                    price = SelectedITem.SupplierPrice;
                    break;
                case (int)EnPriceTypes.الجملة:
                    price = SelectedITem.WholesalePrice;
                    break;
                case (int)EnPriceTypes.البيع:
                    price = SelectedITem.SellingPrice;
                    break;
                case (int)EnPriceTypes.التنفيذ:
                    price = SelectedITem.ExecutionPrice;
                    break;
                case (int)EnPriceTypes.العرض:
                    price = SelectedITem.ShowPrice;
                    break;
                case (int)EnPriceTypes.التجزئة:
                    price = SelectedITem.SellingPrice;
                    break;
                case (int)EnPriceTypes.أخرى:
                    price = SelectedITem.other_Price;
                    break;
            }
            return (decimal)price;
        }

        protected async Task GetItemData(QuotationItemsVM row)
        {
          //   LookupItem SelectedITem = ItemList.Find(x => x.ID == row.ItemId);
          LookupItem SelectedITem = ItemList.Find(x => x.Name == row.ItemName);
            ItemImg = SelectedITem.ItemImg;
            row.Box_ParCode = SelectedITem.Code;
            row.ItemId = SelectedITem.ID;
            if (QuotModel.DealTypeId != null && QuotModel.DealTypeId != 0)
            {
                row.Price = GetPriceFromDealType((int)QuotModel.DealTypeId, SelectedITem);
            }
            else
            {
                SelectDealTypeChecker = true;
            }
            row.Quantity = null;
            row.Total = null;
            HttpResponseMessage result = new HttpResponseMessage();
            //check for save
            if (QuotModel.SerialNumber != 0)
            {
                if (FirstAdd == false)
                {
                    var AddModel = JsonConvert.SerializeObject(QuotModel);
                    var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/Quotation/AddQuotation", content);
                    var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    QuotModel.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                    FirstAdd = true;
                }
                else
                {
                    QuotModel.QuotationItems = QItemList;
                    var EditModel = JsonConvert.SerializeObject(QuotModel);
                    var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/Quotation/EditQuotation", content);
                }
            }
            if (result.IsSuccessStatusCode)
            {
                //string uri = "/api/Items/GetNationalCodeByID?Id=" + row.ItemId;
                //HttpResponseMessage response = await Http.GetAsync(uri);
                //var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                //row.Box_ParCode = JsonConvert.DeserializeObject<string>(vMs["data"].ToString());

                //string priceuri = "/api/Items/GetItemPriceByDealId?DealId=" + QuotModel.DealTypeId + "&ItemId=" + row.ItemId;
                //HttpResponseMessage priceresponse = await Http.GetAsync(priceuri);
                //var pricevMs = JObject.Parse(priceresponse.Content.ReadAsStringAsync().Result);
                //row.Price = JsonConvert.DeserializeObject<decimal>(pricevMs["data"].ToString());
                //row.Quantity = null;
                //row.Total = null;
                ////new 09-11-2020
                //string uriImage = "/api/Items/GetItemImage?ItemId=" + row.ItemId;
                //HttpResponseMessage responseImage = await Http.GetAsync(uriImage);
                //var vMsImage = JObject.Parse(responseImage.Content.ReadAsStringAsync().Result);
                //ItemSrc = JsonConvert.DeserializeObject<ItemVM>(vMsImage["data"].ToString());
                //ItemImg = ItemSrc.ItemImg;
                StateHasChanged();
            }
        }
        //protected async Task GetDealType()
        //{
        //    string uri = "/api/Client/GetDealType?Id=" + QuotModel.ClientId;
        //    HttpResponseMessage response = await Http.GetAsync(uri);
        //    var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
        //    QuotModel.DealTypeId = JsonConvert.DeserializeObject<int>(vMs["data"].ToString());
        //    StateHasChanged();
        //}
        //protected void GetItemTotal(QuotationItemsVM row)
        //{
        //    if (row.Quantity != null && row.Price != null)
        //    {
        //      //  row.Total = row.Quantity.Value * row.Price.Value;
        //        RecalculateTotal();
        //    }
        //}
        //protected void GetItemTotalEdit(QuotationItemsVM row)
        //{
        //   // decimal olditemtotal = row.Total.Value;
        //   // row.Total = row.Quantity.Value * row.Price.Value;
        //    //QuotModel.TotalPrice = (QuotModel.TotalPrice == null ? 0 : QuotModel.TotalPrice) + row.Total - olditemtotal;
        //    //decimal temp = 0;
        //    //if (QuotModel.DiscountPrecentage != null)
        //    //{
        //    //    temp = (QuotModel.TotalPrice.Value * QuotModel.DiscountPrecentage.Value) / 100;
        //    //}
        //    //QuotModel.DiscountValue = temp;
        //    RecalculateTotal();
        //}
        //void CalculateDiscountPrec()
        //{
        //    if (QuotModel.DiscountPrecentage != null)
        //    {
        //        decimal temp = (QuotModel.TotalPrice.Value * QuotModel.DiscountPrecentage.Value) / 100;
        //        QuotModel.DiscountValue = temp;
        //    }
        //    else
        //    {
        //        QuotModel.DiscountPrecentage = 0;
        //        QuotModel.DiscountValue =0;
        //    }
        //    RecalculateTotal();
        //}
        //void CalculateDiscountAmt()
        //{
        //    if ( QuotModel.DiscountValue != null)
        //    {
        //        QuotModel.TotalAfterDiscount = QuotModel.TotalPrice - QuotModel.DiscountValue;
        //        decimal temp = QuotModel.DiscountValue.Value / QuotModel.TotalPrice.Value;
        //        QuotModel.DiscountPrecentage = temp * 100;
        //    }
        //    else
        //    {
        //        QuotModel.DiscountValue = 0;
        //        QuotModel.DiscountValue = 0;
        //    }
        //    RecalculateTotal();
        //}
        protected async Task TaxPart()
        {
            if (FlagTypeID == 1)
            {
                QuotModel.FlagType = true;
                ShowTaxPart = true;
                //get 14% from otherconstant where id=1
                //calculate addtions amount
                if (QuotModel.AddtionTax == 0)
                {
                    string uri = "/api/OtherConstant/GetOtherConstantRatio?Id=1";
                    HttpResponseMessage response = await Http.GetAsync(uri);
                    var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                    QuotModel.AddtionTax = JsonConvert.DeserializeObject<decimal>(vMs["data"].ToString());
                }
                RecalculateTotal();
            }
            else
            {
                QuotModel.FlagType = false;
                ShowTaxPart = false;
            }
        }
        void ChangeDealType()
        {
            //loop on items for delete price and
            foreach (var row in QItemList)
            {
                if (row.ItemId != 0)
                {
                    LookupItem SelectedITem = ItemList.Find(x => x.ID == row.ItemId);
                    ItemImg = SelectedITem.ItemImg;
                    row.Box_ParCode = SelectedITem.Code;
                    row.Price = GetPriceFromDealType((int)QuotModel.DealTypeId, SelectedITem);
                }
                if(row.Quantity!=0 && row.Quantity != null)
                {
                    row.Total = row.Price* row.Quantity;
                }
            }
            RecalculateTotal();
        }
        void RecalculateTotal()
        {
            decimal CalculatedTotal = 0;
            foreach (var rowitem in QItemList)
            {
                if (rowitem.Quantity != null && rowitem.Price != null)
                {
                    rowitem.Total = rowitem.Quantity.Value * rowitem.Price.Value;
                    CalculatedTotal = (decimal)(CalculatedTotal + rowitem.Total);
                }
            }
            QuotModel.TotalPrice = CalculatedTotal;
            QuotModel.TotalAfterDiscount = QuotModel.TotalPrice;

            if (QuotModel.DiscountValue != null & QuotModel.DiscountValue != 0 & QuotModel.DiscountPrecentage == 0)
            {
                decimal temp = QuotModel.DiscountValue.Value / QuotModel.TotalPrice.Value;
                QuotModel.DiscountPrecentage = temp * 100;
                QuotModel.TotalAfterDiscount = (QuotModel.TotalPrice == null ? 0 : QuotModel.TotalPrice) - QuotModel.DiscountValue;
            }
            else if (QuotModel.DiscountPrecentage != null & QuotModel.DiscountPrecentage != 0 & QuotModel.DiscountValue == 0)
            {
                decimal temp = (QuotModel.TotalPrice.Value * QuotModel.DiscountPrecentage.Value) / 100;
                QuotModel.DiscountValue = temp;
                QuotModel.TotalAfterDiscount = (QuotModel.TotalPrice == null ? 0 : QuotModel.TotalPrice) - QuotModel.DiscountValue;
            }
            else
            {
                QuotModel.TotalAfterDiscount = (QuotModel.TotalPrice == null ? 0 : QuotModel.TotalPrice) - QuotModel.DiscountValue;
            }

            QuotModel.TotalAfterDiscount = Math.Round((decimal)QuotModel.TotalAfterDiscount, 0);
            NumberToAlphabetic Obj = new NumberToAlphabetic();
            Obj.ConvertNumbersToArabicAlphabet(QuotModel.TotalAfterDiscount.ToString());

            TotalInWords = Obj.GetNumberAr();
            decimal DeductionAmount = 0;
            decimal AddtionalAmount = 0;
            if (QuotModel.SourceDeduction != 0)
            {
                QuotModel.SourceDeductionAmount = (decimal)(((QuotModel.TotalAfterDiscount == null ? 0 : QuotModel.TotalAfterDiscount) * QuotModel.SourceDeduction) / 100);
                DeductionAmount = QuotModel.SourceDeductionAmount;
            }
            if (QuotModel.AddtionTax != 0)
            {
                QuotModel.AddtionTaxAmount = (decimal)(((QuotModel.TotalAfterDiscount == null ? 0 : QuotModel.TotalAfterDiscount) * QuotModel.AddtionTax) / 100);
                AddtionalAmount = QuotModel.AddtionTaxAmount;
            }
            QuotModel.TotalAfterTax = (decimal)(QuotModel.TotalAfterDiscount - DeductionAmount + AddtionalAmount);
            QuotModel.TotalAfterTax = Math.Round(QuotModel.TotalAfterTax, 0);

            Obj = new NumberToAlphabetic();
            Obj.ConvertNumbersToArabicAlphabet(QuotModel.TotalAfterTax.ToString());
            TotalInWordsTax = Obj.GetNumberAr();
            StateHasChanged();
        }

        void EraseText(QuotationItemsVM row)
        {
         
            row.ItemName = String.Empty;
            row.Box_ParCode = String.Empty;
            row.Price = 0 ;
        }

        async Task ReloadItemsList(QuotationItemsVM row)
        {
            row.ItemName = String.Empty;
            row.Box_ParCode = String.Empty;
            row.Price = 0;


            var ItemListReturn = await Http.GetAsync("api/Items/GetItem");
            var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
            ItemList = JsonConvert.DeserializeObject<List<LookupItem>>(ItemvMs["data"].ToString());


        }
    }
}