﻿using MasProject.Domain.ViewModel.Hierarchy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using MasProject.Domain.ViewModel.StoreTransaction.Quotation;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel.Items;
using MasProject.Client.Shared;
using Microsoft.JSInterop;
using System;

namespace MasProject.Client.Pages.StoreTransaction.Quotations
{
    public partial class Quotation
    {
        private PageTitle PageTitle;
        bool ShowDelete = false;
        List<QuotationVM> QuotList = new List<QuotationVM>();
        int QuotationID;
        //ClientsVM clients = new ClientsVM();
        //List<LookupKeyValueVM> ClientList = new List<LookupKeyValueVM>();
        List<BranchVM> BranchList = new List<BranchVM>();

        QuotationVM QuotationModel = new QuotationVM();
        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }

        async Task BindList()
        {
            var ListReturn = await Http.GetAsync("api/Quotation/GetQuotations");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            BillsTbl<QuotationVM> ReturnModel = JsonConvert.DeserializeObject<BillsTbl<QuotationVM>>(vMs["data"].ToString());
            QuotList = ReturnModel.BillsList;
            BranchList = ReturnModel.SearchItems.BranchList;
            //Fill Table
            //var ListReturn = await Http.GetAsync("api/Quotation/GetQuotations");
            //var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            //QuotList = JsonConvert.DeserializeObject<List<QuotationVM>>(vMs["data"].ToString());
            ////Fill Dropdowns
            //var SalesListReturn = await Http.GetAsync("api/Quotation/GetListDDL");
            //var SalesvMs = JObject.Parse(SalesListReturn.Content.ReadAsStringAsync().Result);
            //QuotationModel = JsonConvert.DeserializeObject<QuotationVM>(SalesvMs["data"].ToString());
        }

        protected async Task AddNewQuotation()
        {
            await jsRuntime.InvokeAsync<object>("open", "/QuotationAdd", "_blank");

        }

        protected async Task EditOldQuotation(int ID)//, string Type+ "&Type=" + Type
        {
            await jsRuntime.InvokeAsync<object>("open", "/QuotationEdit?ID=" + ID , "_blank");

        }

        protected async Task ViewOldQuotation(int ID)//, string Type//+"&Type="+Type
        {
            await jsRuntime.InvokeAsync<object>("open", "/QuotationView?ID=" + ID, "_blank");
        }
        protected void DeleteQuotation(int Id)
        {
            QuotationID = Id;
            ShowDelete = true;
        }
        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                string uri = "/api/Quotation/DeleteQuotation?ID=" + QuotationID;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
            ShowDelete = false;

        }  
        protected async Task GetAllByBranch()
        {


            string uri = "/api/Quotation/GetSalesByBranch?BranchId=" + QuotationModel.BranchId;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            QuotList = JsonConvert.DeserializeObject<List<QuotationVM>>(vMs["data"].ToString());


            StateHasChanged();
        }

    }
}
