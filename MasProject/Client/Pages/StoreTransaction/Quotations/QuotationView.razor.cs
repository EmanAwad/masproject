﻿using MasProject.Domain.ViewModel.Hierarchy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Domain.ViewModel.StoreTransaction.Quotation;
using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel.Items;
using MasProject.Client.Shared;
using MasProject.Domain.ViewModel;
using System;
using Microsoft.JSInterop;

using static MasProject.Domain.Enums.Enums;

namespace MasProject.Client.Pages.StoreTransaction.Quotations
{
    public partial class QuotationView
    {
        //private PageTitle PageTitle;
        //bool ShowConfirm = false;
        QuotationItemsVM QItemModel = new QuotationItemsVM();
        List<QuotationItemsVM> QItemList = new List<QuotationItemsVM>();
        QuotationVM QuotModel = new QuotationVM();
        List<BranchVM> BranchList = new List<BranchVM>();
        List<DealTypeVM> DealTypeList = new List<DealTypeVM>();
        List<LookupItem> ItemList = new List<LookupItem>();

       // bool FirstAdd = false;
        string TotalInWords = "";
        string TotalInWordsTax = "";
        bool ShowTaxPart = false;
        int FlagTypeID = 0;
        string ItemImg = "";
  //      bool IsDisabled = false;
        bool ShowImg = false;
      //  bool SelectDealTypeChecker = false;

        protected override async Task OnInitializedAsync()
        {
            var querystring = ExtensionMethods.QueryString(nav);
            var id = querystring["ID"];
            await EditOldQuotation(int.Parse(id));
            //var Type = querystring["Type"];
            //if (Type == "View")
            //{
            //    IsDisabled = true;
                //PageTitle.Title = "عرض عرض السعر";
            //}
            //else
            //{
            //    IsDisabled = false;
            //    PageTitle.Title = "تعديل عرض السعر";
            //}
           
        }
        protected async Task EditOldQuotation(int ID)
        {
            //FirstAdd = true;
            QuotModel = new QuotationVM();
            string uri = "/api/Quotation/GetSpecificQuotation?Id=" + ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            QuotModel = JsonConvert.DeserializeObject<QuotationVM>(vMs["data"].ToString());
            QuotModel.convert = "Select";
            BranchList = QuotModel.BranchList;
            DealTypeList = QuotModel.DealTypeList;
            ItemList = QuotModel.ItemList;
            QItemList = new List<QuotationItemsVM>();
            QItemList = QuotModel.QuotationItems;
            if (QuotModel.FlagType)
            {
                FlagTypeID = 1;
                await TaxPart();
            }
            RecalculateTotal();
            StateHasChanged();
        }
        private void ShowImage(QuotationItemsVM row)
        {
            ItemImg = row.ItemImg;
            ShowImg = true;
        }
        //async Task ItemsRenew(QuotationItemsVM row)
        //{
        //    var ItemListReturn = await Http.GetAsync("api/Items/GetItems");
        //    var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
        //    ItemList = JsonConvert.DeserializeObject<List<LookupItem>>(ItemvMs["data"].ToString());
        //    //get price for selected items
        //    //string priceuri = "/api/Items/GetItemPriceByDealId?DealId=" + QuotModel.DealTypeId + "&ItemId=" + row.ItemId;
        //    //HttpResponseMessage priceresponse = await Http.GetAsync(priceuri);
        //    //var pricevMs = JObject.Parse(priceresponse.Content.ReadAsStringAsync().Result);
        //    //row.Price = JsonConvert.DeserializeObject<decimal>(pricevMs["data"].ToString());
        //    //  LookupItem SelectedITem = ItemList.Find(x => (x.BoxCode == row.Box_ParCode || x.GuaranteeParCode == row.Box_ParCode || x.InterationalParCode == row.Box_ParCode || x.Code == row.Box_ParCode || x.ParCodeNote == row.Box_ParCode || x.SupplierParCode == row.Box_ParCode));
        //    LookupItem SelectedITem = ItemList.Find(x => x.Name == row.ItemName);
        //    ItemImg = SelectedITem.ItemImg ?? "";
        //    row.ItemId = SelectedITem.ID;
        //    if (QuotModel.DealTypeId != null && QuotModel.DealTypeId != 0)
        //    {
        //        row.Price = GetPriceFromDealType((int)QuotModel.DealTypeId, SelectedITem);
        //    }
        //    else
        //    {
        //        SelectDealTypeChecker = true;
        //    }
        //    RecalculateTotal();
        //    StateHasChanged();
        //}


        //protected async Task EditQuotation()
        //{
        //    IsDisabled = true;
        //    QuotModel.QuotationItems = QItemList;
        //    QuotModel.CreatedDate = QuotModel.CreatedDate.ToLocalTime();
        //    var EditModel = JsonConvert.SerializeObject(QuotModel);
        //    var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
        //    var result = await Http.PostAsync("api/Quotation/EditQuotation", content);
        //    if (result.IsSuccessStatusCode)
        //    {
        //        ShowConfirm = true;
        //    }
        //}
        //protected async Task Confirm()
        //{
        //    if (QuotModel.convert == "Select")
        //    {
        //        ShowConfirm = false;
        //        IsDisabled = false;
        //    }
        //    else
        //    {
        //        await jsRuntime.InvokeAsync<object>("open", "/" + QuotModel.convert + "?ID=" + QuotModel.ID, "_blank");
        //        ShowConfirm = false;
        //        IsDisabled = false;
        //    }
        //}
        //void AddNewRow()
        //{
        //    int IdentifierCount = QItemList.Count;
        //    QItemModel = new QuotationItemsVM { Identifer = IdentifierCount + 1};
        //    QItemList.Add(QItemModel);
        //    StateHasChanged();
        //}
        //void RemoveRow(QuotationItemsVM row)
        //{
        //    QItemList.Remove(row);
        //    StateHasChanged();
        //}
        //protected async Task GetItemDataByCode(QuotationItemsVM row)
        //{
        //    LookupItem SelectedITem = ItemList.Find(x => (x.BoxCode == row.Box_ParCode || x.GuaranteeParCode == row.Box_ParCode || x.InterationalParCode == row.Box_ParCode || x.Code == row.Box_ParCode || x.ParCodeNote == row.Box_ParCode || x.SupplierParCode == row.Box_ParCode));
        //    ItemImg = SelectedITem.ItemImg ?? "";
        //     row.ItemId = SelectedITem.ID;
        //    row.ItemName = SelectedITem.Name;
        //    if (QuotModel.DealTypeId != null && QuotModel.DealTypeId != 0)
        //    {
        //        row.Price = GetPriceFromDealType((int)QuotModel.DealTypeId, SelectedITem);
        //    }
        //    else
        //    {
        //        SelectDealTypeChecker = true;
        //    }
        //    row.Quantity = null;
        //    row.Total = null;
        //    StateHasChanged();
        //    HttpResponseMessage result = new HttpResponseMessage();

        //    if (QuotModel.SerialNumber != 0)
        //    {
        //        if (FirstAdd == false)
        //        {
        //            var AddModel = JsonConvert.SerializeObject(QuotModel);
        //            var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
        //            result = await Http.PostAsync("api/Quotation/AddQuotation", content);
        //            var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
        //            QuotModel.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
        //            FirstAdd = true;
        //        }
        //        else
        //        {
        //            QuotModel.QuotationItems = QItemList;
        //            var EditModel = JsonConvert.SerializeObject(QuotModel);
        //            var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
        //            result = await Http.PostAsync("api/Quotation/EditQuotation", content);
        //        }
        //    }
        //            if (result.IsSuccessStatusCode)
        //    {
        //        StateHasChanged();
        //    }
        //}
        //void ChangeItemQuantity()
        //{
        //    RecalculateTotal();
        //    AddNewRow();
        //}
        //protected async Task GetItemData(QuotationItemsVM row)
        //{
        //    // LookupItem SelectedITem = ItemList.Find(x => x.ID == row.ItemId);
        //    LookupItem SelectedITem = ItemList.Find(x => x.Name == row.ItemName);
        //    ItemImg = SelectedITem.ItemImg;
        //    row.Box_ParCode = SelectedITem.Code;
        //    row.ItemId = SelectedITem.ID;
        //    if (QuotModel.DealTypeId != null && QuotModel.DealTypeId != 0)
        //    {
        //        row.Price = GetPriceFromDealType((int)QuotModel.DealTypeId, SelectedITem);
        //    }
        //    else
        //    {
        //        SelectDealTypeChecker = true;
        //    }
        //    row.Quantity = null;
        //    row.Total = null;
        //    StateHasChanged();
        //    HttpResponseMessage result = new HttpResponseMessage();
        //    if (QuotModel.SerialNumber != 0)
        //    {
        //        if (FirstAdd == false)
        //        {
        //            var AddModel = JsonConvert.SerializeObject(QuotModel);
        //            var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
        //            result = await Http.PostAsync("api/Quotation/AddQuotation", content);
        //            var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
        //            QuotModel.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
        //            FirstAdd = true;
        //        }
        //        else
        //        {
        //            QuotModel.QuotationItems = QItemList;
        //            var EditModel = JsonConvert.SerializeObject(QuotModel);
        //            var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
        //            result = await Http.PostAsync("api/Quotation/EditQuotation", content);
        //        }
        //    }
        //    if (result.IsSuccessStatusCode)
        //    {
        //        StateHasChanged();
        //    }
        //}
        //private decimal GetPriceFromDealType(int DealTypeId, LookupItem SelectedITem)
        //{
        //    float price = 0;
        //    switch (DealTypeId)
        //    {
        //        case (int)EnPriceTypes.التكلفة:
        //            price = SelectedITem.CostPrice;
        //            break;
        //        case (int)EnPriceTypes.الموزع:
        //            price = SelectedITem.SupplierPrice;
        //            break;
        //        case (int)EnPriceTypes.الجملة:
        //            price = SelectedITem.WholesalePrice;
        //            break;
        //        case (int)EnPriceTypes.البيع:
        //            price = SelectedITem.SellingPrice;
        //            break;
        //        case (int)EnPriceTypes.التنفيذ:
        //            price = SelectedITem.ExecutionPrice;
        //            break;
        //        case (int)EnPriceTypes.العرض:
        //            price = SelectedITem.ShowPrice;
        //            break;
        //        case (int)EnPriceTypes.التجزئة:
        //            price = SelectedITem.SellingPrice;
        //            break;
        //        case (int)EnPriceTypes.أخرى:
        //            price = SelectedITem.other_Price;
        //            break;
        //    }
        //    return (decimal)price;
        //}
        protected async Task TaxPart()
        {
            if (FlagTypeID == 1)
            {
                QuotModel.FlagType = true;
                ShowTaxPart = true;
                //get 14% from otherconstant where id=1
                //calculate addtions amount
                if (QuotModel.AddtionTax == 0)
                {
                    string uri = "/api/OtherConstant/GetOtherConstantRatio?Id=1";
                    HttpResponseMessage response = await Http.GetAsync(uri);
                    var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                    QuotModel.AddtionTax = JsonConvert.DeserializeObject<decimal>(vMs["data"].ToString());
                }
                RecalculateTotal();
            }
            else
            {
                QuotModel.FlagType = false;
                ShowTaxPart = false;
            }
        }
        //void ChangeDealType()
        //{
        //    foreach (var row in QItemList)
        //    {
        //        if (row.ItemId != 0)
        //        {
        //            LookupItem SelectedITem = ItemList.Find(x => x.ID == row.ItemId);
        //            ItemImg = SelectedITem.ItemImg;
        //            row.Box_ParCode = SelectedITem.Code;
        //            row.Price = GetPriceFromDealType((int)QuotModel.DealTypeId, SelectedITem);
        //        }
        //        if (row.Quantity != 0 && row.Quantity != null)
        //        {
        //            row.Total = row.Price * row.Quantity;
        //        }
        //    }
        //    RecalculateTotal();
        //}

        void RecalculateTotal()
        {
            decimal CalculatedTotal = 0;
            foreach (var rowitem in QItemList)
            {
                if (rowitem.Quantity != null && rowitem.Price != null)
                {
                    rowitem.Total = rowitem.Quantity.Value * rowitem.Price.Value;
                    CalculatedTotal = (decimal)(CalculatedTotal + rowitem.Total);
                }
            }
            QuotModel.TotalPrice = CalculatedTotal;
            QuotModel.TotalAfterDiscount = QuotModel.TotalPrice;

            if (QuotModel.DiscountValue != null & QuotModel.DiscountValue != 0 & QuotModel.DiscountPrecentage == 0)
            {
                decimal temp = QuotModel.DiscountValue.Value / QuotModel.TotalPrice.Value;
                QuotModel.DiscountPrecentage = temp * 100;
                QuotModel.TotalAfterDiscount = (QuotModel.TotalPrice == null ? 0 : QuotModel.TotalPrice) - QuotModel.DiscountValue;
            }
            else if (QuotModel.DiscountPrecentage != null & QuotModel.DiscountPrecentage != 0 & QuotModel.DiscountValue == 0)
            {
                decimal temp = (QuotModel.TotalPrice.Value * QuotModel.DiscountPrecentage.Value) / 100;
                QuotModel.DiscountValue = temp;
                QuotModel.TotalAfterDiscount = (QuotModel.TotalPrice == null ? 0 : QuotModel.TotalPrice) - QuotModel.DiscountValue;
            }
            else
            {
                QuotModel.TotalAfterDiscount = (QuotModel.TotalPrice == null ? 0 : QuotModel.TotalPrice) - QuotModel.DiscountValue;
            }

            QuotModel.TotalAfterDiscount = Math.Round((decimal)QuotModel.TotalAfterDiscount, 0);
            NumberToAlphabetic Obj = new NumberToAlphabetic();
            Obj.ConvertNumbersToArabicAlphabet(QuotModel.TotalAfterDiscount.ToString());

            TotalInWords = Obj.GetNumberAr();
            decimal DeductionAmount = 0;
            decimal AddtionalAmount = 0;
            if (QuotModel.SourceDeduction != 0)
            {
                QuotModel.SourceDeductionAmount = (decimal)(((QuotModel.TotalAfterDiscount == null ? 0 : QuotModel.TotalAfterDiscount) * QuotModel.SourceDeduction) / 100);
                DeductionAmount = QuotModel.SourceDeductionAmount;
            }
            if (QuotModel.AddtionTax != 0)
            {
                QuotModel.AddtionTaxAmount = (decimal)(((QuotModel.TotalAfterDiscount == null ? 0 : QuotModel.TotalAfterDiscount) * QuotModel.AddtionTax) / 100);
                AddtionalAmount = QuotModel.AddtionTaxAmount;
            }
            QuotModel.TotalAfterTax = (decimal)(QuotModel.TotalAfterDiscount - DeductionAmount + AddtionalAmount);
           QuotModel.TotalAfterTax = Math.Round(QuotModel.TotalAfterTax, 0);

            Obj = new NumberToAlphabetic();
            Obj.ConvertNumbersToArabicAlphabet(QuotModel.TotalAfterTax.ToString());
            TotalInWordsTax = Obj.GetNumberAr();
            StateHasChanged();
        }
        //void EraseText(QuotationItemsVM row)
        //{
        //    //  LookupItem SelectedITem = ItemList.Find(x => x.Name == row.ItemName);
        //    row.ItemName = String.Empty;
        //}
        //async Task ReloadItemsList(QuotationItemsVM row)
        //{
        //    row.ItemName = String.Empty;
        //    row.Box_ParCode = String.Empty;
        //    row.Price = 0;
        //    row.Quantity = 0;
        //    var ItemListReturn = await Http.GetAsync("api/Items/GetItems");
        //    var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
        //    ItemList = JsonConvert.DeserializeObject<List<LookupItem>>(ItemvMs["data"].ToString());

        //}
    }
}
