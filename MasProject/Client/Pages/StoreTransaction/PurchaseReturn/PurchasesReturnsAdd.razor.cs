﻿using MasProject.Domain.ViewModel.Hierarchy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using MasProject.Domain.ViewModel.StoreTransaction.Purchase;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel.Items;
using MasProject.Client.Shared;
using System;
using static MasProject.Domain.Enums.Enums;

namespace MasProject.Client.Pages.StoreTransaction.PurchaseReturn
{
    public partial class PurchasesReturnsAdd
    {
        private PageTitle PageTitle;
        PurchasesReturns_ItemVM PItemModel = new PurchasesReturns_ItemVM();
        List<PurchasesReturns_ItemVM> PItemList = new List<PurchasesReturns_ItemVM>();
        PurchasesReturns_BillVM PurchaseModel = new PurchasesReturns_BillVM();
        List<StoreVM> StoreList = new List<StoreVM>();
        List<BranchVM> BranchList = new List<BranchVM>();
        List<LookupKeyValueVM> SupplierList = new List<LookupKeyValueVM>();
        //List<DealTypeVM> DealTypeList = new List<DealTypeVM>();
        List<LookupItem> ItemList = new List<LookupItem>();
        List<OtherIncomeVM> IncomeList = new List<OtherIncomeVM>();
        PurchaseReturns_OtherIncomeVM PIncomeModel = new PurchaseReturns_OtherIncomeVM();
        List<PurchaseReturns_OtherIncomeVM> PIncomeList = new List<PurchaseReturns_OtherIncomeVM>();
        bool FirstAdd = false;
        bool ShowConfirm = false;
        bool ShowTaxPart = false;
        string TotalInWords = "";
        string TotalInWordsTax = "";
        int FlagTypeID = 0;
        string ItemImg = "";
        bool ShowImg = false;
        bool SelectSupplierChecker = false;
        bool IsDisabled = false;
        protected override async Task OnInitializedAsync()
        {
            FirstAdd = false;
            PItemModel = new PurchasesReturns_ItemVM { Identifer = 0 };
            PItemList = new List<PurchasesReturns_ItemVM>();
            PItemList.Add(PItemModel);
            PurchaseModel = new PurchasesReturns_BillVM();
            PIncomeModel = new PurchaseReturns_OtherIncomeVM { Identifer = 1, };
            PIncomeList.Add(PIncomeModel);
            PurchaseModel.PurchaseItem = PItemList;
            PurchaseModel.PurchaseReturns_OtherIncome = PIncomeList;
            PurchaseModel.Date = DateTime.Now;
            PurchaseModel.DiscountPrecentage = 0;
            PurchaseModel.DiscountValue = 0;
            var IdReturn = await Http.GetAsync("api/PurchasesReturns/GenerateSerial");
            var IdvMs = JObject.Parse(IdReturn.Content.ReadAsStringAsync().Result);
            PurchaseModel.SerialNumber = JsonConvert.DeserializeObject<int>(IdvMs["data"].ToString());

            StateHasChanged();
            await BindList();
        }
        async Task BindList()
        {
            var ListReturn = await Http.GetAsync("api/PurchasesReturns/GetListDDL");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            PurchaseModel = JsonConvert.DeserializeObject<PurchasesReturns_BillVM>(vMs["data"].ToString());
            StoreList = PurchaseModel.StoreList;
            SupplierList = PurchaseModel.SupplierList;
            //DealTypeList = PurchaseModel.DealTypeList;
            ItemList = PurchaseModel.ItemList;
            IncomeList = PurchaseModel.OtherIncomeList;
            BranchList = PurchaseModel.BranchList;
        }
        async Task IncomeRenew()
        {
            var IncomesListReturn = await Http.GetAsync("api/OtherIncome/GetOtherIncome");
            var IncomesvMs = JObject.Parse(IncomesListReturn.Content.ReadAsStringAsync().Result);
            IncomeList = JsonConvert.DeserializeObject<List<OtherIncomeVM>>(IncomesvMs["data"].ToString());
        }
        async Task SuppliersRenew()
        {
            var SupplierListReturn = await Http.GetAsync("api/Supplier/GetSuppliers");
            var SuppliervMs = JObject.Parse(SupplierListReturn.Content.ReadAsStringAsync().Result);
            SupplierList = JsonConvert.DeserializeObject<List<LookupKeyValueVM>>(SuppliervMs["data"].ToString());

        }
        //async Task ItemsRenew()
        //{
        //    var ItemListReturn = await Http.GetAsync("api/Items/GetItems");
        //    var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
        //    ItemList = JsonConvert.DeserializeObject<List<ItemVM>>(ItemvMs["data"].ToString());

        //}
        async Task GetSerialNumber()
        {
            string serailuri = "/api/PurchasesReturns/GenerateSerial?BranchId=" + PurchaseModel.BranchId;
            HttpResponseMessage serialReturn = await Http.GetAsync(serailuri);
            var serialvms = JObject.Parse(serialReturn.Content.ReadAsStringAsync().Result);
            PurchaseModel.SerialNumber = JsonConvert.DeserializeObject<int>(serialvms["data"].ToString());
            StateHasChanged();

        }
        async Task ItemsRenew(PurchasesReturns_ItemVM row)
        {
            var ItemListReturn = await Http.GetAsync("api/Items/GetItems");
            var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
            ItemList = JsonConvert.DeserializeObject<List<LookupItem>>(ItemvMs["data"].ToString());

            var url = "/api/PurchasesReturns/GetLatestPrice?ItemId=" + row.ItemId;
            HttpResponseMessage PriceReturn = await Http.GetAsync(url);
            var PricevMs = JObject.Parse(PriceReturn.Content.ReadAsStringAsync().Result);
            row.Price = JsonConvert.DeserializeObject<decimal>(PricevMs["data"].ToString());
            
            RecalculateTotal();
            StateHasChanged();
        }
        protected async Task GetBalance()
        {
            string priceuri = "/api/SupplierBalance/GetBalance?SupplierId=" + PurchaseModel.SupplierId;
            HttpResponseMessage ClientListReturn = await Http.GetAsync(priceuri);
            var ClientvMs = JObject.Parse(ClientListReturn.Content.ReadAsStringAsync().Result);
            PurchaseModel.Balance = JsonConvert.DeserializeObject<decimal>(ClientvMs["data"].ToString());

            string Taxuri = "/api/Supplier/GetTaxFile?Id=" + PurchaseModel.SupplierId;
            HttpResponseMessage TaxReturn = await Http.GetAsync(Taxuri);
            var TaxvMs = JObject.Parse(TaxReturn.Content.ReadAsStringAsync().Result);
            SupplierVM TempFile = JsonConvert.DeserializeObject<SupplierVM>(TaxvMs["data"].ToString());
            PurchaseModel.TaxFileNumber = TempFile.Taxfile;

            StateHasChanged();
        }
        async Task BalancesRenew()
        {
            string priceuri = "/api/SupplierBalance/GetBalance?SupplierId=" + PurchaseModel.SupplierId;
            HttpResponseMessage ClientListReturn = await Http.GetAsync(priceuri);
            var ClientvMs = JObject.Parse(ClientListReturn.Content.ReadAsStringAsync().Result);
            PurchaseModel.Balance = JsonConvert.DeserializeObject<decimal>(ClientvMs["data"].ToString());
        }
        protected async Task TaxPart()
        {
            if (FlagTypeID == 1)
            {
                PurchaseModel.FlagType = true;
                ShowTaxPart = true;
                //get 14% from otherconstant where id=1
                //calculate addtions amount
                if (PurchaseModel.AddtionTax == 0)
                {
                    string uri = "/api/OtherConstant/GetOtherConstantRatio?Id=1";
                    HttpResponseMessage response = await Http.GetAsync(uri);
                    var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                    PurchaseModel.AddtionTax = JsonConvert.DeserializeObject<decimal>(vMs["data"].ToString());
                }
                RecalculateTotal();
            }
            else
            {
                PurchaseModel.FlagType = false;
                ShowTaxPart = false;
            }
        }
        void ChangeItemQuantity()
        {
            RecalculateTotal();
            AddNewRow();
        }
        void RecalculateTotal()
        {
            decimal CalculatedTotal = 0;

            foreach (var rowitem in PItemList)
            {
                if (rowitem.Quantity != null && rowitem.Price != null)
                {
                    rowitem.Total = rowitem.Quantity.Value * rowitem.Price.Value;
                    CalculatedTotal = (decimal)(CalculatedTotal + rowitem.Total);
                }
            }
            foreach (var rowExpense in PIncomeList)
            {
                if (rowExpense.Amount != null)
                {
                    CalculatedTotal = (decimal)(CalculatedTotal + rowExpense.Amount.Value);
                }
            }
            PurchaseModel.TotalPrice = CalculatedTotal;
            PurchaseModel.TotalAfterDiscount = PurchaseModel.TotalPrice;

            if (PurchaseModel.DiscountValue != null & PurchaseModel.DiscountValue != 0 & PurchaseModel.DiscountPrecentage == 0)
            {
                decimal temp = PurchaseModel.DiscountValue.Value / PurchaseModel.TotalPrice.Value;
                PurchaseModel.DiscountPrecentage = temp * 100;
                PurchaseModel.TotalAfterDiscount = (PurchaseModel.TotalPrice == null ? 0 : PurchaseModel.TotalPrice) - PurchaseModel.DiscountValue;
            }
            else if (PurchaseModel.DiscountPrecentage != null & PurchaseModel.DiscountPrecentage != 0 & PurchaseModel.DiscountValue == 0)
            {
                decimal temp = (PurchaseModel.TotalPrice.Value * PurchaseModel.DiscountPrecentage.Value) / 100;
                PurchaseModel.DiscountValue = temp;
                PurchaseModel.TotalAfterDiscount = (PurchaseModel.TotalPrice == null ? 0 : PurchaseModel.TotalPrice) - PurchaseModel.DiscountValue;
            }
            else
            {
                PurchaseModel.TotalAfterDiscount = (PurchaseModel.TotalPrice == null ? 0 : PurchaseModel.TotalPrice) - PurchaseModel.DiscountValue;
            }
            if (PurchaseModel.TotalAfterDiscount == null)
            {
                PurchaseModel.TotalAfterDiscount = PurchaseModel.TotalPrice;
            }
            PurchaseModel.TotalAfterDiscount = Math.Round((decimal)PurchaseModel.TotalAfterDiscount, 0);
            NumberToAlphabetic Obj = new NumberToAlphabetic();
            Obj.ConvertNumbersToArabicAlphabet(PurchaseModel.TotalAfterDiscount.ToString());

            TotalInWords = Obj.GetNumberAr();
            decimal DeductionAmount = 0;
            decimal AddtionalAmount = 0;
            if (PurchaseModel.SourceDeduction != 0)
            {
                PurchaseModel.SourceDeductionAmount = (decimal)(((PurchaseModel.TotalAfterDiscount == null ? 0 : PurchaseModel.TotalAfterDiscount) * PurchaseModel.SourceDeduction) / 100);
                DeductionAmount = PurchaseModel.SourceDeductionAmount;
            }
            if (PurchaseModel.AddtionTax != 0)
            {
                PurchaseModel.AddtionTaxAmount = (decimal)(((PurchaseModel.TotalAfterDiscount == null ? 0 : PurchaseModel.TotalAfterDiscount) * PurchaseModel.AddtionTax) / 100);
                AddtionalAmount = PurchaseModel.AddtionTaxAmount;
            }
            PurchaseModel.TotalAfterTax = (decimal)(PurchaseModel.TotalAfterDiscount - DeductionAmount + AddtionalAmount);
            PurchaseModel.TotalAfterTax = Math.Round(PurchaseModel.TotalAfterTax, 0);

            Obj = new NumberToAlphabetic();
            Obj.ConvertNumbersToArabicAlphabet(PurchaseModel.TotalAfterTax.ToString());
            TotalInWordsTax = Obj.GetNumberAr();
            StateHasChanged();
        }
        void AddNewIncomeRow()
        {
            int IdentifierCount = PIncomeList.Count;
            PIncomeModel = new PurchaseReturns_OtherIncomeVM { Identifer = IdentifierCount + 1, };
            PIncomeList.Add(PIncomeModel);
        }
        void RemoveIncomeRow(PurchaseReturns_OtherIncomeVM row)
        {
            PIncomeList.Remove(row);
            StateHasChanged();
        }
        void AddNewRow()
        {
            PItemModel = new PurchasesReturns_ItemVM { Identifer = 0 };
            PItemList.Add(PItemModel);
            StateHasChanged();
        }
        void RemoveRow(PurchasesReturns_ItemVM row)
        {
            PItemList.Remove(row);
            StateHasChanged();
        }
        protected async Task AddPurchasereturn()
        {
            HttpResponseMessage result = new HttpResponseMessage();
            //check for save
            if (PurchaseModel.SerialNumber != 0)
            {
                if (PurchaseModel.SupplierId != 0)
                {
                    if (FirstAdd == false)
                    {
                        var AddModel = JsonConvert.SerializeObject(PurchaseModel);
                        var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/PurchasesReturns/AddPurchasesReturns", content);
                        var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                        PurchaseModel.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                        FirstAdd = true;
                    }
                    else
                    {
                        PurchaseModel.PurchaseItem = PItemList;
                        PurchaseModel.PurchaseReturns_OtherIncome = PIncomeList;
                        var EditModel = JsonConvert.SerializeObject(PurchaseModel);
                        var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/PurchasesReturns/EditPurchasesReturns", content);
                    }
                }
                if (result.IsSuccessStatusCode)
                {
                    StateHasChanged();
                }
            }
            else
            {
                SelectSupplierChecker = true;
            }
        }
        protected async Task GetItemDataByCode(PurchasesReturns_ItemVM row)
        {
            LookupItem SelectedITem = ItemList.Find(x => (x.BoxCode == row.Box_ParCode || x.GuaranteeParCode == row.Box_ParCode || x.InterationalParCode == row.Box_ParCode || x.Code == row.Box_ParCode || x.ParCodeNote == row.Box_ParCode || x.SupplierParCode == row.Box_ParCode));
            ItemImg = SelectedITem.ItemImg ?? "";
            row.ItemId = SelectedITem.ID;
            row.ItemName = SelectedITem.Name;
            row.Quantity = null;
            row.Total = null;
            var url = "/api/PurchasesReturns/GetLatestPrice?ItemId=" + row.ItemId;
            HttpResponseMessage PriceReturn = await Http.GetAsync(url);
            var PricevMs = JObject.Parse(PriceReturn.Content.ReadAsStringAsync().Result);
            row.Price = JsonConvert.DeserializeObject<decimal>(PricevMs["data"].ToString());
            StateHasChanged();
            HttpResponseMessage result = new HttpResponseMessage();
            //check for save
            if (PurchaseModel.SerialNumber != 0)
            {
                if (PurchaseModel.SupplierId != 0)
                {
                    if (FirstAdd == false)
                    {
                        var AddModel = JsonConvert.SerializeObject(PurchaseModel);
                        var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/PurchasesReturns/AddPurchasesReturns", content);
                        var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                        PurchaseModel.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                        FirstAdd = true;
                    }
                    else
                    {
                        PurchaseModel.PurchaseItem = PItemList;
                        PurchaseModel.PurchaseReturns_OtherIncome = PIncomeList;
                        var EditModel = JsonConvert.SerializeObject(PurchaseModel);
                        var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/PurchasesReturns/EditPurchasesReturns", content);
                    }
                    if (result.IsSuccessStatusCode)
                    {

                        StateHasChanged();
                    }
                }
                else
                {
                    SelectSupplierChecker = true;
                }
            }
        }
        protected async Task GetItemData(PurchasesReturns_ItemVM row)
        {
           // LookupItem SelectedITem = ItemList.Find(x => x.ID == row.ItemId);
            LookupItem SelectedITem = ItemList.Find(x => x.Name == row.ItemName);
            ItemImg = SelectedITem.ItemImg;
            row.Box_ParCode = SelectedITem.Code;
            row.ItemId = SelectedITem.ID;
            row.Quantity = null;
            row.Total = null;
            var url = "/api/PurchasesReturns/GetLatestPrice?ItemId=" + row.ItemId;
            HttpResponseMessage PriceReturn = await Http.GetAsync(url);
            var PricevMs = JObject.Parse(PriceReturn.Content.ReadAsStringAsync().Result);
            row.Price = JsonConvert.DeserializeObject<decimal>(PricevMs["data"].ToString());
            StateHasChanged();
            HttpResponseMessage result = new HttpResponseMessage();
            //check for save
            if (PurchaseModel.SerialNumber != 0)
            {
                if (PurchaseModel.SupplierId != 0)
                {
                    if (FirstAdd == false)
                    {
                        var AddModel = JsonConvert.SerializeObject(PurchaseModel);
                        var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/PurchasesReturns/AddPurchasesReturns", content);
                        var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                        PurchaseModel.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                        FirstAdd = true;
                    }
                    else
                    {
                        PurchaseModel.PurchaseItem = PItemList;
                        PurchaseModel.PurchaseReturns_OtherIncome = PIncomeList;
                        var EditModel = JsonConvert.SerializeObject(PurchaseModel);
                        var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/PurchasesReturns/EditPurchasesReturns", content);
                    }
                }
                else
                {
                    SelectSupplierChecker = true;
                }

            }
            if (result.IsSuccessStatusCode)
            {

                StateHasChanged();
            }

        }
        protected async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        }
        //void Confirm()
        //{
        //    //await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        //    ShowConfirm = false;
        //    IsDisabled = false;
        //}
        protected async Task EditPurchasesReturnBill()
        {
            IsDisabled = true;
            HttpResponseMessage result = new HttpResponseMessage();
            //check for save
            if (PurchaseModel. SupplierId!= 0)
            {
                if (PurchaseModel.SerialNumber != 0)
                {
                    if (FirstAdd == false)
                    {
                        var AddModel = JsonConvert.SerializeObject(PurchaseModel);
                        var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/PurchasesReturns/AddPurchasesReturns", content);
                        var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                        PurchaseModel.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                        FirstAdd = true;
                    }
                    else
                    {
                        PurchaseModel.PurchaseItem = PItemList;
                        PurchaseModel.PurchaseReturns_OtherIncome = PIncomeList;
                        var EditModel = JsonConvert.SerializeObject(PurchaseModel);
                        var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/PurchasesReturns/EditPurchasesReturns", content);
                    }
                }
                if (result.IsSuccessStatusCode)
                {
                    StateHasChanged();
                    ShowConfirm = true;
                }
            }
            else
            {
                SelectSupplierChecker = true;
            }

        }
        protected async Task AddNewSupplier()
        {
            await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/SupplierAdd", "جارى التحميل", 1000);
        }
        async Task ReloadItemsList(PurchasesReturns_ItemVM row)
        {
            row.ItemName = String.Empty;
            row.Box_ParCode = String.Empty;
            row.Price = 0;


            var ItemListReturn = await Http.GetAsync("api/Items/GetItem");
            var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
            ItemList = JsonConvert.DeserializeObject<List<LookupItem>>(ItemvMs["data"].ToString());


        }
    }
}