﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using MasProject.Domain.ViewModel.StoreTransaction.Purchase;
using MasProject.Client.Shared;
using Microsoft.JSInterop;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Persons;
using System;
using System.Net.Http;

namespace MasProject.Client.Pages.StoreTransaction.PurchaseReturn
{
    public partial class PurchasesReturns
    {
        private PageTitle PageTitle;
        bool ShowDelete = false;
        bool ShowConfirm = false;
        List<PurchasesReturns_BillVM> PurchaseList = new List<PurchasesReturns_BillVM>();
        List<LookupKeyValueVM> SupplierList = new List<LookupKeyValueVM>();

        List<BranchVM> BranchList = new List<BranchVM>();

        PurchasesReturns_BillVM PurchaseReturnModel = new PurchasesReturns_BillVM();
        SupplierVM supplier = new SupplierVM();
        int PurchasesReturnBillID;
        int SupplierId;
        int SerialNumber;
        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        {
            //Fill Table
            var ListReturn = await Http.GetAsync("api/PurchasesReturns/GetPurchasesReturns");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            PurchaseList = JsonConvert.DeserializeObject<List<PurchasesReturns_BillVM>>(vMs["data"].ToString());
            //Fill Dropdowns
            var PurchaseListReturn = await Http.GetAsync("api/PurchasesReturns/GetListDDL");
            var vMMs = JObject.Parse(PurchaseListReturn.Content.ReadAsStringAsync().Result);
            PurchaseReturnModel = JsonConvert.DeserializeObject<PurchasesReturns_BillVM>(vMMs["data"].ToString());
            SupplierList = PurchaseReturnModel.SupplierList;
            BranchList = PurchaseReturnModel.BranchList;

        }
        protected async Task AddNewPurchaseBill()
        {
            //await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/PurchasesReturnsAdd", "جارى التحميل", 1200);
            await jsRuntime.InvokeAsync<object>("open", "/PurchasesReturnsAdd", "_blank");
        }
     
        protected void Confirm()
        {
            ShowConfirm = false;
         }

        protected void DeletePurchaseBill(int Id, int serialNumber, int Supplierid)
        {
            PurchasesReturnBillID = Id;
            SupplierId = Supplierid;
            SerialNumber = serialNumber;
            ShowDelete = true;
        }

        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                string uri = "/api/PurchasesReturns/DeletePurchasesReturns?ID=" + PurchasesReturnBillID + "&SerialNumber=" + SerialNumber + "&SupplierId=" + SupplierId;

                var result = await Http.GetAsync(uri);

                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
            ShowDelete = false;
        }

        protected async Task EditOldPurchaseBill(int ID, string Type)
        {
            //await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/PurchasesReturnsEdit?ID=" + ID+ "&Type=" + Type, "جارى التحميل", 1200);
            await jsRuntime.InvokeAsync<object>("open", "/PurchasesReturnsEdit?ID=" + ID + "&Type=" + Type, "_blank");
        }

        async Task ReloadSuppliersList()
        {
            supplier.Name = String.Empty;

            var ListReturn = await Http.GetAsync("api/Supplier/GetSuppliers");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            SupplierList = JsonConvert.DeserializeObject<List<LookupKeyValueVM>>(vMs["data"].ToString());

        }

        protected async Task GetAllBySupplier()
        {
            PurchaseReturnModel.SupplierId = SupplierList.Find(x => x.Name == supplier.Name).ID;
            if (PurchaseReturnModel.SupplierId != 0)
            {
                string uri = "/api/PurchasesReturns/GetAllBySupplier?SupplierId=" + PurchaseReturnModel.SupplierId;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                PurchaseList = JsonConvert.DeserializeObject<List<PurchasesReturns_BillVM>>(vMs["data"].ToString());
            }
            else
            {
                var ListReturn = await Http.GetAsync("api/PurchasesReturns/GetPurchasesReturns");
                var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
                PurchaseList = JsonConvert.DeserializeObject<List<PurchasesReturns_BillVM>>(vMs["data"].ToString());
            }

            StateHasChanged();
            //SalesModel.ClientId = ClientList.Find(x => x.Name == clients.Name).ID;
            //if (SalesModel.ClientId != 0)
            //{
            //    SalesList = SalesList.Where(x => x.ClientId == SalesModel.ClientId).ToList();
            //}
            //else
            //{
            //    SalesList = TempSalesList;
            //}
        }
        //protected async Task GetAllByBranch()
        //{
        //    //if (SalesModel.BranchId != 0)
        //    //{
        //    //    SalesList = SalesList.Where(x => x.BranchId == SalesModel.BranchId).ToList();
        //    //}
        //    //else
        //    //{
        //    //    SalesList = TempSalesList;
        //    //}
        //    //SalesList = SalesList.Where(x => x.BranchId == SalesModel.BranchId).ToList();
        //    string SalesBranch = "/api/SalesBills/GetSalesByBranch?BranchId=" + SalesModel.BranchId;
        //    HttpResponseMessage SalesBranchReturn = await Http.GetAsync(SalesBranch);
        //    var SalesBranchVMs = JObject.Parse(SalesBranchReturn.Content.ReadAsStringAsync().Result);
        //    SalesList = JsonConvert.DeserializeObject<List<SalesBillsVM>>(SalesBranchVMs["data"].ToString());
        //}

        protected async Task GetAllByBranch()
        {

            if (PurchaseReturnModel.BranchId != 0)
            {
                string uri = "/api/PurchasesReturns/GetAllByBranch?BranchId=" + PurchaseReturnModel.BranchId;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                PurchaseList = JsonConvert.DeserializeObject<List<PurchasesReturns_BillVM>>(vMs["data"].ToString());
            }
            else
            {
                var ListReturn = await Http.GetAsync("api/PurchasesReturns/GetPurchasesReturns");
                var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
                PurchaseList = JsonConvert.DeserializeObject<List<PurchasesReturns_BillVM>>(vMs["data"].ToString());
            }


            StateHasChanged();
        }

        protected async Task GetAllByDate()
        {
            if (PurchaseReturnModel.Date != null)
            {
                string uri = "/api/PurchasesReturns/GetAllByDate?date=" + PurchaseReturnModel.Date;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                PurchaseList = JsonConvert.DeserializeObject<List<PurchasesReturns_BillVM>>(vMs["data"].ToString());
            }
            else
            {
                var ListReturn = await Http.GetAsync("api/PurchasesReturns/GetPurchasesReturns");
                var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
                PurchaseList = JsonConvert.DeserializeObject<List<PurchasesReturns_BillVM>>(vMs["data"].ToString());
            }


            StateHasChanged();

        }
    }
}