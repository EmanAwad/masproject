﻿using MasProject.Domain.ViewModel.Hierarchy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Domain.ViewModel.StoreTransaction.Voucher;
using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel.Items;
using MasProject.Domain.ViewModel;
using MasProject.Client.Shared;
using System;

namespace MasProject.Client.Pages.StoreTransaction.PaymentVoucher
{
    public partial class PaymentVoucherEdit
    {
     //   private PageTitle PageTitle;
        bool ShowConfirm = false;
        PaymentVoucherItemVM sItemModel = new PaymentVoucherItemVM();
        List<PaymentVoucherItemVM> sItemList = new List<PaymentVoucherItemVM>();
        PaymentVoucherVM PaymentVoucherModel = new PaymentVoucherVM();
        List<StoreVM> StoreList = new List<StoreVM>();
        List<LookupKeyValueVM> Client_Supplier_List = new List<LookupKeyValueVM>();
        //List<DealTypeVM> DealTypeList = new List<DealTypeVM>();
        List<LookupItem> ItemList = new List<LookupItem>();
        List<Item_StoreVM> ItemStoreList = new List<Item_StoreVM>();
        ClientsVM clients = new ClientsVM();
        bool Type = false;
      //  bool FirstAdd = false;
        bool ShowImg = false;
        bool ShowWarn = false;
        bool ShowSucess = false;
        string ItemImg = "";
       // bool IsDisabled = false;
        bool SelectTypeChecker = false;
        bool SelectStoreChecker = false;
        protected override async Task OnInitializedAsync()
        {
            var querystring = ExtensionMethods.QueryString(navigationManager);
            var id = querystring["ID"];
            Type = querystring["Type"] == "temp" ? true : false;
           // await BindList();
            await EditOldPaymentVoucher(int.Parse(id));
            //var Show = querystring["Show"];
            //if (Show == "View")
            //{
            //    //disable controls
            //    IsDisabled = true;
            //    PageTitle.Title = "عرض سند صرف مخزن";
            //}
            //else
            //{
            //    IsDisabled = false;
            //    PageTitle.Title = "تعديل سند صرف مخزن";
            //}
            StateHasChanged();
        }
        //async Task BindList()
        //{
        //    var PaymentVoucherListReturn = await Http.GetAsync("api/PaymentVoucher/GetListsOfDDl");
        //    var PaymentVouchervMs = JObject.Parse(PaymentVoucherListReturn.Content.ReadAsStringAsync().Result);
        //    PaymentVoucherModel = JsonConvert.DeserializeObject<PaymentVoucherVM>(PaymentVouchervMs["data"].ToString());
        //    StoreList = PaymentVoucherModel.StoreList;
        //    ItemList = PaymentVoucherModel.ItemList;
        //    Client_Supplier_List = PaymentVoucherModel.Client_Supplier_List;
        //    StateHasChanged();
        //}
        async Task ClientsRenew()
        {
            var ClientListReturn = await Http.GetAsync("api/PaymentVoucher/GetListsOfSupplierClientDDl");
            var ClientvMs = JObject.Parse(ClientListReturn.Content.ReadAsStringAsync().Result);
            Client_Supplier_List = JsonConvert.DeserializeObject<List<LookupKeyValueVM>>(ClientvMs["data"].ToString());

        }
        async Task ItemsRenew()
        {
            var ItemListReturn = await Http.GetAsync("api/Items/GetItem");
            var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
            ItemList = JsonConvert.DeserializeObject<List<LookupItem>>(ItemvMs["data"].ToString());

        }
        protected async Task AddPaymentVoucher()
        {
            PaymentVoucherModel.PaymentVoucherItem = sItemList;
            var AddModel = JsonConvert.SerializeObject(PaymentVoucherModel);
            var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
            var result = await Http.PostAsync("api/PaymentVoucher/AddPaymentVoucher", content);
            if (result.IsSuccessStatusCode)
            {
                StateHasChanged();
            }
        }
        protected async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        }
        //void Confirm()
        //{
        //    ShowConfirm = false;
        //    IsDisabled = false;
        //}
        protected async Task EditOldPaymentVoucher(int ID)
        {
          //  FirstAdd = true;
            string uri = "/api/PaymentVoucher/GetSpecificPaymentVoucher?Id=" + ID + "&Type=" + Type;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            PaymentVoucherModel = JsonConvert.DeserializeObject<PaymentVoucherVM>(vMs["data"].ToString());
            sItemList = new List<PaymentVoucherItemVM>();
            sItemList = PaymentVoucherModel.PaymentVoucherItem;
            clients.Name = PaymentVoucherModel.ClientName;
            StoreList = PaymentVoucherModel.StoreList;
            ItemList = PaymentVoucherModel.ItemList;
            Client_Supplier_List = PaymentVoucherModel.Client_Supplier_List;
        }
        protected async Task EditPaymentVoucher()
        {
          //  IsDisabled = true;
            if (PaymentVoucherModel.StoreId != 0 && PaymentVoucherModel.StoreId != null)
            {
                if (clients.Name != "0" && clients.Name != "" && clients.Name != null)
                {
                    //PaymentVoucherModel.UserType = Client_Supplier_List.Find(x => x.Name == PaymentVoucherModel.Name).Type;
                    //PaymentVoucherModel.Client_Supplier_Id = Client_Supplier_List.Find(x => x.Name == PaymentVoucherModel.Name).ID;
                    PaymentVoucherModel.Client_Supplier_Id = Client_Supplier_List.Find(x => x.Name == clients.Name).ID;
                    PaymentVoucherModel.UserType = Client_Supplier_List.Find(x => x.Name == clients.Name).Type;

                    PaymentVoucherModel.PaymentVoucherItem = sItemList;
                    var EditModel = JsonConvert.SerializeObject(PaymentVoucherModel);
                    var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                    var result = await Http.PostAsync("api/PaymentVoucher/EditPaymentVoucher", content);
                    if (result.IsSuccessStatusCode)
                    {
                        ShowConfirm = true;
                    }
                }
                else
                {
                    SelectTypeChecker = true;
                }
            }
            else
            {
                SelectStoreChecker = true;
            }
        }
        void AddNewRow()
        {
            int IdentifierCount = sItemList.Count;
            sItemModel = new PaymentVoucherItemVM { Identifer = IdentifierCount + 1 };
            sItemList.Add(sItemModel);
            StateHasChanged();
        }
        void RemoveRow(PaymentVoucherItemVM row)
        {
            sItemList.Remove(row);
            StateHasChanged();
        }
        private void ShowImage(PaymentVoucherItemVM row)
        {
            ItemImg = row.ItemImg;
            ShowImg = true;
        }
        protected async Task GetItemStore()
        {
            string uri = "/api/Items/GetItemStore?StoreId=" + PaymentVoucherModel.StoreId;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            ItemStoreList = JsonConvert.DeserializeObject<List<Item_StoreVM>>(vMs["data"].ToString());
        }
        protected async Task GetItemDataByCode(PaymentVoucherItemVM row)
        {
            HttpResponseMessage result = new HttpResponseMessage();
            LookupItem SelectedITem = ItemList.Find(x => (x.BoxCode == row.Box_ParCode || x.GuaranteeParCode == row.Box_ParCode || x.InterationalParCode == row.Box_ParCode || x.Code == row.Box_ParCode || x.ParCodeNote == row.Box_ParCode || x.SupplierParCode == row.Box_ParCode));
            ItemImg = SelectedITem.ItemImg ?? "";
            row.ItemId = SelectedITem.ID;
            row.ItemName = SelectedITem.Name;
            row.Quantity = null;
            row.BoxCode = SelectedITem.BoxCode ?? null;
            row.SheilfNo = ItemStoreList?.Find(x => x.ItemId == row.ItemId)?.SheilfNo;
            StateHasChanged();
            //check for save
            if (PaymentVoucherModel.SerialNumber != 0)
            {
                if (PaymentVoucherModel.StoreId != 0 && PaymentVoucherModel.StoreId != null)
                {
                    if (clients.Name != "0" && clients.Name != "" && clients.Name != null)
                    {
                        //PaymentVoucherModel.UserType = Client_Supplier_List.Find(x => x.Name == PaymentVoucherModel.Name).Type;
                        //PaymentVoucherModel.Client_Supplier_Id = Client_Supplier_List.Find(x => x.Name == PaymentVoucherModel.Name).ID;
                        PaymentVoucherModel.Client_Supplier_Id = Client_Supplier_List.Find(x => x.Name == clients.Name).ID;
                        PaymentVoucherModel.UserType = Client_Supplier_List.Find(x => x.Name == clients.Name).Type;

                        PaymentVoucherModel.PaymentVoucherItem = sItemList;
                        var EditModel = JsonConvert.SerializeObject(PaymentVoucherModel);
                        var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/PaymentVoucher/EditPaymentVoucher", content);

                        StateHasChanged();
                    }
                    else
                    {
                        SelectTypeChecker = true;
                    }
                }
                else
                {
                    SelectStoreChecker = true;
                }
            }
        }
        protected async Task GetItemData(PaymentVoucherItemVM row)
        {
            //LookupItem SelectedITem = ItemList.Find(x => x.ID == row.ItemId);
            LookupItem SelectedITem = ItemList.Find(x => x.Name == row.ItemName);
            ItemImg = SelectedITem.ItemImg ?? "";
            row.Box_ParCode = SelectedITem.Code;
            row.ItemId = SelectedITem.ID;
            row.ItemName = SelectedITem.Name;
            row.Quantity = null;
            row.BoxCode = SelectedITem.BoxCode ?? null;
            row.SheilfNo = ItemStoreList?.Find(x => x.ItemId == row.ItemId)?.SheilfNo;
            StateHasChanged();
            HttpResponseMessage result = new HttpResponseMessage();
           // PaymentVoucherModel.UserType = Client_Supplier_List.Find(x => x.Name == PaymentVoucherModel.Name).Type;
            //PaymentVoucherModel.Client_Supplier_Id = Client_Supplier_List.Find(x => x.Name == PaymentVoucherModel.Name).ID;

            //check for save
            if (PaymentVoucherModel.SerialNumber != 0)
            {
                if (PaymentVoucherModel.StoreId != 0 && PaymentVoucherModel.StoreId != null)
                {
                    if (clients.Name != "0" && clients.Name != "" && clients.Name != null)
                    {
                        //PaymentVoucherModel.UserType = Client_Supplier_List.Find(x => x.Name == PaymentVoucherModel.Name).Type;
                        //PaymentVoucherModel.Client_Supplier_Id = Client_Supplier_List.Find(x => x.Name == PaymentVoucherModel.Name).ID;
                        PaymentVoucherModel.Client_Supplier_Id = Client_Supplier_List.Find(x => x.Name == clients.Name).ID;
                        PaymentVoucherModel.UserType = Client_Supplier_List.Find(x => x.Name == clients.Name).Type;

                        PaymentVoucherModel.PaymentVoucherItem = sItemList;
                        var EditModel = JsonConvert.SerializeObject(PaymentVoucherModel);
                        var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/PaymentVoucher/EditPaymentVoucher", content);

                        StateHasChanged();
                    }
                    else
                    {
                        SelectTypeChecker = true;
                    }
                }
                else
                {
                    SelectStoreChecker = true;
                }
            }
        }
        protected async Task ConvertToPayment()
        {
            PaymentVoucherModel.PaymentVoucherItem = sItemList;
            //PaymentVoucherModel.UserType = Client_Supplier_List.Find(x => x.Name == PaymentVoucherModel.Name).Type;
            // PaymentVoucherModel.Client_Supplier_Id = Client_Supplier_List.Find(x => x.Name == PaymentVoucherModel.Name).ID;
            PaymentVoucherModel.Client_Supplier_Id = Client_Supplier_List.Find(x => x.Name == clients.Name).ID;
            PaymentVoucherModel.UserType = Client_Supplier_List.Find(x => x.Name == clients.Name).Type;

            var EditModel = JsonConvert.SerializeObject(PaymentVoucherModel);
            var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
            var result = await Http.PostAsync("api/PaymentVoucher/ConvertToPayment", content);
            //var AddvMs = JObject.Parse(temp.Content.ReadAsStringAsync().Result);
            //var result = JsonConvert.DeserializeObject<int>(AddvMs["statusCode"].ToString());
            if (result.IsSuccessStatusCode)
            {
                ShowSucess = true;
                StateHasChanged();
            }
            else
            {
                ShowWarn = true;
                StateHasChanged();
            }
        }

        async Task ReloadItemsList(PaymentVoucherItemVM row)
        {
            row.ItemName = String.Empty;
            row.Box_ParCode = String.Empty;
            row.BoxCode = String.Empty;
            row.SheilfNo = String.Empty;


            var ItemListReturn = await Http.GetAsync("api/Items/GetItem");
            var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
            ItemList = JsonConvert.DeserializeObject<List<LookupItem>>(ItemvMs["data"].ToString());


        }
        async Task ReloadClientsList()
        {
            clients.Name = String.Empty;

            var ListReturn = await Http.GetAsync("api/Client/GetClients");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            Client_Supplier_List = JsonConvert.DeserializeObject<List<LookupKeyValueVM>>(vMs["data"].ToString());

        }
    }
}