﻿using MasProject.Domain.ViewModel.Hierarchy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using MasProject.Domain.ViewModel.StoreTransaction.Voucher;
using MasProject.Client.Shared;
using Microsoft.JSInterop;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Persons;
using System;

namespace MasProject.Client.Pages.StoreTransaction.PaymentVoucher
{
    public partial class PaymentVoucher
    {
        private PageTitle PageTitle;
        bool ShowDelete = false;
        List<PaymentVoucherVM> PaymentVoucherList = new List<PaymentVoucherVM>();
       
        List<LookupKeyValueVM> Client_Supplier_List = new List<LookupKeyValueVM>();
        PaymentVoucherVM PaymentVoucherModel = new PaymentVoucherVM();
        List<StoreVM> StoreList = new List<StoreVM>();
        ClientsVM clients = new ClientsVM();
        int PaymentVoucherID;
        string PaymentType = "";
        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        {
            var ListReturn = await Http.GetAsync("api/PaymentVoucher/GetPaymentVoucher");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            BillsTbl<PaymentVoucherVM> ReturnModel = JsonConvert.DeserializeObject<BillsTbl<PaymentVoucherVM>>(vMs["data"].ToString());
            PaymentVoucherList = ReturnModel.BillsList;
            StoreList = ReturnModel.SearchItems.StoreList;
            Client_Supplier_List = ReturnModel.SearchItems.Client_Supplier_List;

            //Fill Table
            //var ListReturn = await Http.GetAsync("api/PaymentVoucher/GetPaymentVoucher");
            //var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            //PaymentVoucherList = JsonConvert.DeserializeObject<List<PaymentVoucherVM>>(vMs["data"].ToString());
            ////FillDrop
            //PaymentVoucherModel = new PaymentVoucherVM { };
            //var PaymentVoucherListReturn = await Http.GetAsync("api/PaymentVoucher/GetListsOfDDl");
            //var PaymentVouchervMs = JObject.Parse(PaymentVoucherListReturn.Content.ReadAsStringAsync().Result);
            //PaymentVoucherModel = JsonConvert.DeserializeObject<PaymentVoucherVM>(PaymentVouchervMs["data"].ToString());
            //StoreList = PaymentVoucherModel.StoreList;

            //Client_Supplier_List = PaymentVoucherModel.Client_Supplier_List;
        }
        async Task ReloadClientsList()
        {
            clients.Name = String.Empty;

            var ListReturn = await Http.GetAsync("api/Client/GetClients");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            Client_Supplier_List = JsonConvert.DeserializeObject<List<LookupKeyValueVM>>(vMs["data"].ToString());

        }

        async Task ClientsRenew()
        {
            var ClientListReturn = await Http.GetAsync("api/AdditionVoucher/GetListsOfSupplierClientDDl");
            var ClientvMs = JObject.Parse(ClientListReturn.Content.ReadAsStringAsync().Result);
            Client_Supplier_List = JsonConvert.DeserializeObject<List<LookupKeyValueVM>>(ClientvMs["data"].ToString());

        }
        protected async Task AddNewPaymentVoucher()
        {
            await jsRuntime.InvokeAsync<object>("open", "/PaymentVoucherAdd", "_blank");

        }
        protected async Task EditOldPaymentVoucher(int ID, string Type)
        {
            await jsRuntime.InvokeAsync<object>("open", "/PaymentVoucherEdit?ID="+ID+"&Type="+Type, "_blank");

        }
        protected async Task ViewOldPaymentVoucher(int ID, string Type)
        {
            await jsRuntime.InvokeAsync<object>("open", "/PaymentVoucherView?ID=" + ID + "&Type=" + Type, "_blank");

        }

        protected void DeletePaymentVoucher(int Id, string Type)
        {
            PaymentVoucherID = Id;
            PaymentType = Type;
            ShowDelete = true;
        }
        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                string uri = "/api/PaymentVoucher/DeletePaymentVoucher?ID="+PaymentVoucherID+"&Type="+ PaymentType;

                var result = await Http.GetAsync(uri);

                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
            ShowDelete = false;
        }

        protected async Task GetAllByClientSupplierEmp()
        {
            PaymentVoucherModel.Client_Supplier_Id = Client_Supplier_List.Find(x => x.Name == clients.Name).ID;
            if (PaymentVoucherModel.Client_Supplier_Id != 0)
            {
                string uri = "/api/PaymentVoucher/GetPaymentVoucherByUserType?UserType=" + PaymentVoucherModel.Client_Supplier_Id;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                PaymentVoucherList = JsonConvert.DeserializeObject<List<PaymentVoucherVM>>(vMs["data"].ToString());
            }
            else
            {
                var ListReturn = await Http.GetAsync("api/PaymentVoucher/GetPaymentVoucher");
                var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
                PaymentVoucherList = JsonConvert.DeserializeObject<List<PaymentVoucherVM>>(vMs["data"].ToString());
            }

            StateHasChanged();

        }
        protected async Task GetAllByStore()
        {

            if (PaymentVoucherModel.StoreId != 0)
            {
                string uri = "/api/PaymentVoucher/GetPaymentVoucherByStore?StoreId=" + PaymentVoucherModel.StoreId;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                PaymentVoucherList = JsonConvert.DeserializeObject<List<PaymentVoucherVM>>(vMs["data"].ToString());
            }
            else
            {
                var ListReturn = await Http.GetAsync("api/PaymentVoucher/GetPaymentVoucher");
                var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
                PaymentVoucherList = JsonConvert.DeserializeObject<List<PaymentVoucherVM>>(vMs["data"].ToString());
            }

            StateHasChanged();

        }
    }
}