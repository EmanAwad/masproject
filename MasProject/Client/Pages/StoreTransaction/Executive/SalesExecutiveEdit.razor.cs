﻿using MasProject.Domain.ViewModel.Hierarchy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel.Items;
using MasProject.Client.Shared;
using System;
using static MasProject.Domain.Enums.Enums;
using Microsoft.JSInterop;
namespace MasProject.Client.Pages.StoreTransaction.Executive
{
    public partial class SalesExecutiveEdit
    {
        private PageTitle PageTitle;
        ExecutiveItemsVM sItemModel = new ExecutiveItemsVM();
        List<ExecutiveItemsVM> sItemList = new List<ExecutiveItemsVM>();
        ExecutiveSellExpensesVM sExpenseModel = new ExecutiveSellExpensesVM();
        List<ExecutiveSellExpensesVM> ExpenseList = new List<ExecutiveSellExpensesVM>();
        ExecutiveBillsVM ExecutiveModel = new ExecutiveBillsVM();
        List<ExecutiveBillsVM> ExecutiveList = new List<ExecutiveBillsVM>();
        ExecutiveOtherIncomeVM sIncomeModel = new ExecutiveOtherIncomeVM();
        List<ExecutiveOtherIncomeVM> IncomeList = new List<ExecutiveOtherIncomeVM>();
        List<OtherIncomeVM> OtherIncomeList = new List<OtherIncomeVM>();
        List<StoreVM> StoreList = new List<StoreVM>();
        List<LookupKeyValueVM> ClientList = new List<LookupKeyValueVM>();
        List<DealTypeVM> DealTypeList = new List<DealTypeVM>();
        List<LookupItem> ItemList = new List<LookupItem>();
        List<SellingExpensesVM> ExpensesList = new List<SellingExpensesVM>();
        List<BranchVM> BranchList = new List<BranchVM>();
        List<ExecutiveBillStoresVM> BillStoresList = new List<ExecutiveBillStoresVM>();
        ExecutiveBillStoresVM BillStoresModel = new ExecutiveBillStoresVM();
        ClientsVM clients = new ClientsVM();
        bool FirstAdd = false;
        bool ShowConfirm = false;
        bool ShowTaxPart = false;
        string TotalInWords = "";
        string TotalInWordsTax = "";
        int FlagTypeID = 0;
        string ItemImg = "";
        //ItemVM ItemSrc = new ItemVM();
        bool IsDisabled = false;
        bool ShowImg = false;
        bool SelectDealTypeChecker = false;
        bool SelectClientChecker = false;
        protected override async Task OnInitializedAsync()
        {
            var querystring = ExtensionMethods.QueryString(navigationManager);
            var id = querystring["ID"];
            await BindList();
            await EditOldExecutiveBill(int.Parse(id));
            var Type = querystring["Type"];
            if (Type == "View")
            {
                //disable controls
                IsDisabled = true;
                PageTitle.Title = "عرض فاتورة التنفيذ";
            }
            else
            {
                IsDisabled = false;
                PageTitle.Title = "تعديل فاتورة التنفيذ";
            }
        }
        async Task BindList()
        {
            var SalesListReturn = await Http.GetAsync("api/ExecutiveBills/GetListDDL");
            var SalesvMs = JObject.Parse(SalesListReturn.Content.ReadAsStringAsync().Result);
            ExecutiveModel = JsonConvert.DeserializeObject<ExecutiveBillsVM>(SalesvMs["data"].ToString());
            StoreList = ExecutiveModel.StoreList;
            DealTypeList = ExecutiveModel.DealTypeList;
            ClientList = ExecutiveModel.ClientList;
            ItemList = ExecutiveModel.ItemList;
            ExpensesList = ExecutiveModel.sellingExpensesList;
            BranchList = ExecutiveModel.BranchList;
            OtherIncomeList = ExecutiveModel.OtherIncomeList;
            //var StoreListReturn = await Http.GetAsync("api/Store/GetStores");
            //var StorevMs = JObject.Parse(StoreListReturn.Content.ReadAsStringAsync().Result);
            //StoreList = JsonConvert.DeserializeObject<List<StoreVM>>(StorevMs["data"].ToString());

            //var ClientListReturn = await Http.GetAsync("api/Client/GetClient");
            //var ClientvMs = JObject.Parse(ClientListReturn.Content.ReadAsStringAsync().Result);
            //ClientList = JsonConvert.DeserializeObject<List<ClientsVM>>(ClientvMs["data"].ToString());

            //var GetDealTypeListReturn = await Http.GetAsync("api/DealType/GetDealType");
            //var GetDealTypevMs = JObject.Parse(GetDealTypeListReturn.Content.ReadAsStringAsync().Result);
            //DealTypeList = JsonConvert.DeserializeObject<List<DealTypeVM>>(GetDealTypevMs["data"].ToString());

            //var ItemListReturn = await Http.GetAsync("api/Items/GetItems");
            //var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
            //ItemList = JsonConvert.DeserializeObject<List<ItemVM>>(ItemvMs["data"].ToString());

            //var ExpensesListReturn = await Http.GetAsync("api/SellingExpenses/GetSellingExpenses");
            //var ExpensesvMs = JObject.Parse(ExpensesListReturn.Content.ReadAsStringAsync().Result);
            //ExpensesList = JsonConvert.DeserializeObject<List<SellingExpensesVM>>(ExpensesvMs["data"].ToString());

            //var OtherIncomeListReturn = await Http.GetAsync("api/OtherIncome/GetOtherIncome");
            //var OtherIncomevMs = JObject.Parse(OtherIncomeListReturn.Content.ReadAsStringAsync().Result);
            //OtherIncomeList = JsonConvert.DeserializeObject<List<OtherIncomeVM>>(OtherIncomevMs["data"].ToString());
        }
        protected async Task TaxPart()
        {
            if (FlagTypeID == 1)
            {
                ExecutiveModel.FlagType = true;
                ShowTaxPart = true;
                //get 14% from otherconstant where id=1
                //calculate addtions amount
                if (ExecutiveModel.AddtionTax == 0)
                {
                    string uri = "/api/OtherConstant/GetOtherConstantRatio?Id=1";
                    HttpResponseMessage response = await Http.GetAsync(uri);
                    var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                    ExecutiveModel.AddtionTax = JsonConvert.DeserializeObject<decimal>(vMs["data"].ToString());
                }
                RecalculateTotal();
            }
            else
            {
                ExecutiveModel.FlagType = false;
                ShowTaxPart = false;
            }
        }
        //async Task GetSerialNumber()
        //{
        //    string serailuri = "/api/ExecutiveBills/GenerateSerial?BranchId=" + ExecutiveModel.BranchId;
        //    HttpResponseMessage serialReturn = await Http.GetAsync(serailuri);
        //    var serialvms = JObject.Parse(serialReturn.Content.ReadAsStringAsync().Result);
        //    ExecutiveModel.SerialNumber = JsonConvert.DeserializeObject<int>(serialvms["data"].ToString());
        //    StateHasChanged();

        //}
        async Task BalancesRenew()
        {
            string priceuri = "/api/ClientsBalance/GetBalance?ClientId=" + ExecutiveModel.ClientId;
            HttpResponseMessage ClientListReturn = await Http.GetAsync(priceuri);
            var ClientvMs = JObject.Parse(ClientListReturn.Content.ReadAsStringAsync().Result);
            ExecutiveModel.ClientBalance = JsonConvert.DeserializeObject<decimal>(ClientvMs["data"].ToString());
        }
        async Task ClientsRenew()
        {
            var ClientListReturn = await Http.GetAsync("api/Client/GetClient");
            var ClientvMs = JObject.Parse(ClientListReturn.Content.ReadAsStringAsync().Result);
            ClientList = JsonConvert.DeserializeObject<List<LookupKeyValueVM>>(ClientvMs["data"].ToString());

        }
        async Task ItemsRenew(ExecutiveItemsVM row)
        {
            var ItemListReturn = await Http.GetAsync("api/Items/GetItems");
            var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
            ItemList = JsonConvert.DeserializeObject<List<LookupItem>>(ItemvMs["data"].ToString());
            //get price for selected items
            //string priceuri = "/api/Items/GetItemPriceByDealId?DealId=" + ExecutiveModel.DealTypeId + "&ItemId=" + row.ItemId;
            //HttpResponseMessage priceresponse = await Http.GetAsync(priceuri);
            //var pricevMs = JObject.Parse(priceresponse.Content.ReadAsStringAsync().Result);
            //row.Price = JsonConvert.DeserializeObject<decimal>(pricevMs["data"].ToString());
            LookupItem SelectedITem = ItemList.Find(x => (x.BoxCode == row.Box_ParCode || x.GuaranteeParCode == row.Box_ParCode || x.InterationalParCode == row.Box_ParCode || x.Code == row.Box_ParCode || x.ParCodeNote == row.Box_ParCode || x.SupplierParCode == row.Box_ParCode));
            ItemImg = SelectedITem.ItemImg ?? "";
            row.ItemId = SelectedITem.ID;
            if (ExecutiveModel.DealTypeId != null && ExecutiveModel.DealTypeId != 0)
            {
                row.Price = GetPriceFromDealType((int)ExecutiveModel.DealTypeId, SelectedITem);
            }
            else
            {
                SelectDealTypeChecker = true;
            }
            row.Quantity = null;
            row.Total = null;
            RecalculateTotal();
            StateHasChanged();
        }
        //async Task ItemsRenew()
        //{
        //    var ItemListReturn = await Http.GetAsync("api/Items/GetItems");
        //    var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
        //    ItemList = JsonConvert.DeserializeObject<List<ItemVM>>(ItemvMs["data"].ToString());

        //}
        async Task ExpensesRenew()
        {
            var ExpensesListReturn = await Http.GetAsync("api/SellingExpenses/GetSellingExpenses");
            var ExpensesvMs = JObject.Parse(ExpensesListReturn.Content.ReadAsStringAsync().Result);
            ExpensesList = JsonConvert.DeserializeObject<List<SellingExpensesVM>>(ExpensesvMs["data"].ToString());
        }
        async Task OtherIncomeRenew()
        {
            var OtherIncomeListReturn = await Http.GetAsync("api/OtherIncome/GetOtherIncome");
            var OtherIncomevMs = JObject.Parse(OtherIncomeListReturn.Content.ReadAsStringAsync().Result);
            OtherIncomeList = JsonConvert.DeserializeObject<List<OtherIncomeVM>>(OtherIncomevMs["data"].ToString());

        }
        protected async Task AddNewExecutiveBill()
        {
            ExecutiveModel.ClientId = ClientList.Find(x => x.Name == clients.Name).ID;
            HttpResponseMessage result = new HttpResponseMessage();
            //check for save
            if (ExecutiveModel.SerialNumber != 0 & ExecutiveModel.SerialNumber != null)
            {
                if (ExecutiveModel.ClientId != 0)
                {
                    if (FirstAdd == false)
                {
                    var AddModel = JsonConvert.SerializeObject(ExecutiveModel);
                    var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/ExecutiveBills/AddExecutiveBills", content);
                    FirstAdd = true;
                }
                else
                {
                    //ExecutiveModel.ExecutiveItems = sItemList;
                    ExecutiveModel.BillStores = BillStoresList;
                    ExecutiveModel.ExecutiveSellExpenses = ExpenseList;
                    var EditModel = JsonConvert.SerializeObject(ExecutiveModel);
                    var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/ExecutiveBills/EditExecutiveBills", content);
                }
                if (result.IsSuccessStatusCode)
                {
                    StateHasChanged();
                }
            }
            else
            {
                SelectClientChecker = true;
            }
        }

        }
        protected async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        }
        //void Confirm()
        //{
        //    //await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        //    //changed 30-10-2020// protected async Task
        //    ShowConfirm = false;
        //    IsDisabled = false;
        //}
        protected async Task EditOldExecutiveBill(int ID)
        {
            FirstAdd = true;
            string uri = "/api/ExecutiveBills/GetSpecificExecutiveBills?Id=" + ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            ExecutiveModel = JsonConvert.DeserializeObject<ExecutiveBillsVM>(vMs["data"].ToString());
            ExecutiveModel.convert = 0;
            sItemList = new List<ExecutiveItemsVM>();
            // sItemList = ExecutiveModel.ExecutiveItems
            BillStoresList = new List<ExecutiveBillStoresVM>();
            BillStoresList = ExecutiveModel.BillStores;
            ExpenseList = new List<ExecutiveSellExpensesVM>();
            ExpenseList = ExecutiveModel.ExecutiveSellExpenses;
            IncomeList = new List<ExecutiveOtherIncomeVM>();
            IncomeList = ExecutiveModel.ExecutiveOtherIncome;
            clients.Name = ExecutiveModel.ClientName;
            if (ExecutiveModel.FlagType)
            {
                FlagTypeID = 1;
                await TaxPart();
            }
            RecalculateTotal();
            StateHasChanged();
        }
        protected async Task EditExecutiveBill()
        {
            IsDisabled = true;
            ExecutiveModel.ClientId = ClientList.Find(x => x.Name == clients.Name).ID;
            // ExecutiveModel.ExecutiveItems = sItemList;
            ExecutiveModel.BillStores = BillStoresList;
            ExecutiveModel.ExecutiveSellExpenses = ExpenseList;
            ExecutiveModel.ExecutiveOtherIncome = IncomeList;
            var EditModel = JsonConvert.SerializeObject(ExecutiveModel);
            var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
            var result = await Http.PostAsync("api/ExecutiveBills/EditExecutiveBills", content);
            if (result.IsSuccessStatusCode)
            {
                if(ExecutiveModel.convert == 1 || ExecutiveModel.IsConverted)
                {
                    var Convertresult = await Http.PostAsync("api/ExecutiveBills/ConvertExecutiveBills", content);

                }
                ShowConfirm = true;
            }
        }
        void ChangeDealType()
        {
            //loop on items for delete price and
            foreach (var BillStore in BillStoresList)
            {
                foreach (var row in BillStore.ExecutiveItems)
                {
                    if (row.ItemId != 0)
                    {
                        LookupItem SelectedITem = ItemList.Find(x => x.ID == row.ItemId);
                        ItemImg = SelectedITem.ItemImg;
                        row.Box_ParCode = SelectedITem.Code;
                        row.Price = GetPriceFromDealType((int)ExecutiveModel.DealTypeId, SelectedITem);
                    }
                    if (row.Quantity != 0 && row.Quantity != null)
                    {
                        row.Total = row.Price * row.Quantity;
                    }
                }
            }
            RecalculateTotal();
        }
        protected async Task GetDealType()
        {
            string uri = "/api/Client/GetDealType?Id=" + ExecutiveModel.ClientId;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            ExecutiveModel.DealTypeId = JsonConvert.DeserializeObject<int>(vMs["data"].ToString());

            string priceuri = "/api/ClientsBalance/GetBalance?ClientId=" + ExecutiveModel.ClientId;
            HttpResponseMessage ClientListReturn = await Http.GetAsync(priceuri);
            var ClientvMs = JObject.Parse(ClientListReturn.Content.ReadAsStringAsync().Result);
            ExecutiveModel.ClientBalance = JsonConvert.DeserializeObject<decimal>(ClientvMs["data"].ToString());

            string Taxuri = "/api/Client/GetTaxFile?Id=" + ExecutiveModel.ClientId;
            HttpResponseMessage TaxReturn = await Http.GetAsync(Taxuri);
            var TaxvMs = JObject.Parse(TaxReturn.Content.ReadAsStringAsync().Result);
            ClientsVM TempFile = JsonConvert.DeserializeObject<ClientsVM>(TaxvMs["data"].ToString());
            ExecutiveModel.TaxFileNumber = TempFile.Taxfile;

            StateHasChanged();
        }
        void AddNewRow(int? BillStoresIdentifer)
        {
            //int IdentifierCount = sItemList.Count;
            //sItemModel = new ExecutiveItemsVM { Identifer = IdentifierCount + 1, };
            //sItemList.Add(sItemModel);
            var tempList = BillStoresList.Find(x => x.Identifer == BillStoresIdentifer).ExecutiveItems;
            int IdentifierCount = tempList.Count;
            sItemModel = new ExecutiveItemsVM { Identifer = IdentifierCount + 1, };
            tempList.Add(sItemModel);
            StateHasChanged();
        }
        void RemoveRow(ExecutiveItemsVM row, int? BillStoresIdentifer)
        {
            //sItemList.Remove(row);
            var tempList = BillStoresList.Find(x => x.Identifer == BillStoresIdentifer).ExecutiveItems;
            tempList.Remove(row);
            StateHasChanged();
        }
        void AddNewExpenseRow()
        {
            int IdentifierCount = ExpenseList.Count;
            sExpenseModel = new ExecutiveSellExpensesVM { Identifer = IdentifierCount + 1, };
            ExpenseList.Add(sExpenseModel);
            StateHasChanged();
        }
        void RemoveExpenseRow(ExecutiveSellExpensesVM row)
        {
            ExpenseList.Remove(row);
            StateHasChanged();
        }
        void AddOtherIncomeRow()
        {
            int IdentifierCount = OtherIncomeList.Count;
            sIncomeModel = new ExecutiveOtherIncomeVM { Identifer = IdentifierCount + 1, };
            IncomeList.Add(sIncomeModel);
            StateHasChanged();
        }
        void RemoveOtherIncomeRow(ExecutiveOtherIncomeVM row)
        {
            IncomeList.Remove(row);
            StateHasChanged();
        }
        void ChangeItemQuantity(int BillStoresIdentifer)
        {
            RecalculateTotal();
            AddNewRow(BillStoresIdentifer);
        }
        //protected async Task AddExecutiveBill()
        //{
        //    HttpResponseMessage result = new HttpResponseMessage();
        //    //check for save
        //    if (ExecutiveModel.SerialNumber != 0 & ExecutiveModel.SerialNumber != null)
        //    {
        //        if (FirstAdd == false)
        //        {
        //            var AddModel = JsonConvert.SerializeObject(ExecutiveModel);
        //            var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
        //            result = await Http.PostAsync("api/ExecutiveBills/AddExecutiveBills", content);
        //            FirstAdd = true;
        //        }
        //        else
        //        {
        //            ExecutiveModel.ExecutiveItems = sItemList;
        //            ExecutiveModel.ExecutiveSellExpenses = ExpenseList;
        //            var EditModel = JsonConvert.SerializeObject(ExecutiveModel);
        //            var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
        //            result = await Http.PostAsync("api/ExecutiveBills/EditExecutiveBills", content);
        //        }
        //        if (result.IsSuccessStatusCode)
        //        {
        //            StateHasChanged();
        //        }
        //    }

        //}
        protected async Task GetItemDataByCode(ExecutiveItemsVM row)
        {
            HttpResponseMessage result = new HttpResponseMessage();
            LookupItem SelectedITem = ItemList.Find(x => (x.BoxCode == row.Box_ParCode || x.GuaranteeParCode == row.Box_ParCode || x.InterationalParCode == row.Box_ParCode || x.Code == row.Box_ParCode || x.ParCodeNote == row.Box_ParCode || x.SupplierParCode == row.Box_ParCode));
            ItemImg = SelectedITem.ItemImg ?? "";
            row.ItemId = SelectedITem.ID;
            row.ItemName = SelectedITem.Name;
            if (ExecutiveModel.DealTypeId != null && ExecutiveModel.DealTypeId !=0)
            {
                row.Price = GetPriceFromDealType((int)ExecutiveModel.DealTypeId, SelectedITem);
            }
            else
            {
                SelectDealTypeChecker = true;
            }
            row.Quantity = null;
            row.Total = null;
            //check for save
            if (ExecutiveModel.SerialNumber != 0)
            {
                //string uri = "/api/Items/GetItemByAnyCode?AnyCode=" + row.Box_ParCode;
                //HttpResponseMessage response = await Http.GetAsync(uri);
                //var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                //ItemVM temp = JsonConvert.DeserializeObject<ItemVM>(vMs["data"].ToString());
                //row.Box_ParCode = temp.National_ParCode;
                //row.ItemId = temp.ID;
                ////row.Price = null;
                //string priceuri = "/api/Items/GetItemPriceByDealId?DealId=" + ExecutiveModel.DealTypeId + "&ItemId=" + row.ItemId;
                //HttpResponseMessage priceresponse = await Http.GetAsync(priceuri);
                //var pricevMs = JObject.Parse(priceresponse.Content.ReadAsStringAsync().Result);
                //row.Price = JsonConvert.DeserializeObject<decimal>(pricevMs["data"].ToString());
                //row.Quantity = null;
                //row.Total = null;
                //string uriImage = "/api/Items/GetItemImage?ItemId=" + row.ItemId;
                //HttpResponseMessage responseImage = await Http.GetAsync(uriImage);
                //var vMsImage = JObject.Parse(responseImage.Content.ReadAsStringAsync().Result);
                //ItemSrc = JsonConvert.DeserializeObject<ItemVM>(vMsImage["data"].ToString());
                //ItemImg = ItemSrc.ItemImg;
                if (ExecutiveModel.ClientId != 0)
                {
                    if (FirstAdd == false)
                {
                    var AddModel = JsonConvert.SerializeObject(ExecutiveModel);
                    var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/ExecutiveBills/AddExecutiveBills", content);
                    var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    ExecutiveModel.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                    FirstAdd = true;
                }
                else
                {
                    // ExecutiveModel.ExecutiveItems = sItemList;
                    ExecutiveModel.BillStores = BillStoresList;
                    ExecutiveModel.ExecutiveSellExpenses = ExpenseList;
                    ExecutiveModel.ExecutiveOtherIncome = IncomeList;
                    var EditModel = JsonConvert.SerializeObject(ExecutiveModel);
                    var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/ExecutiveBills/EditExecutiveBills", content);
                }
                if (result.IsSuccessStatusCode)
                {

                    StateHasChanged();
                    }
                }
                else
                {
                    SelectClientChecker = true;
                }
            }
        }

        protected async Task GetItemData(ExecutiveItemsVM row)
        {
            // LookupItem SelectedITem = ItemList.Find(x => x.ID == row.ItemId);
            LookupItem SelectedITem = ItemList.Find(x => x.Name == row.ItemName);
            ItemImg = SelectedITem.ItemImg;
            row.Box_ParCode = SelectedITem.Code;
            row.ItemId = SelectedITem.ID;
            if (ExecutiveModel.DealTypeId != null && ExecutiveModel.DealTypeId != 0)
            {
                row.Price = GetPriceFromDealType((int)ExecutiveModel.DealTypeId, SelectedITem);
            }
            else
            {
                SelectDealTypeChecker = true;
            }
            row.Quantity = null;
            row.Total = null;
            HttpResponseMessage result = new HttpResponseMessage();
            //check for save
            if (ExecutiveModel.SerialNumber != 0 & ExecutiveModel.SerialNumber != null)
            {
                if (ExecutiveModel.ClientId != 0)
                {
                    if (FirstAdd == false)
                    {
                        var AddModel = JsonConvert.SerializeObject(ExecutiveModel);
                        var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/ExecutiveBills/AddExecutiveBills", content);
                        var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                        ExecutiveModel.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                        FirstAdd = true;
                    }
                    else
                    {
                        //  ExecutiveModel.ExecutiveItems = sItemList;
                        ExecutiveModel.BillStores = BillStoresList;
                        ExecutiveModel.ExecutiveSellExpenses = ExpenseList;
                        ExecutiveModel.ExecutiveOtherIncome = IncomeList;
                        var EditModel = JsonConvert.SerializeObject(ExecutiveModel);
                        var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/ExecutiveBills/EditExecutiveBills", content);
                    }
                    if (result.IsSuccessStatusCode)
                    {
                        //string uri = "/api/Items/GetNationalCodeByID?Id=" + row.ItemId;
                        //HttpResponseMessage response = await Http.GetAsync(uri);
                        //var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                        //row.Box_ParCode = JsonConvert.DeserializeObject<string>(vMs["data"].ToString());
                        ////10-10-2020 get last price for this client and item
                        ////uri = "/api/ExecutiveBills/GetPrice?ClientId=" + ExecutiveModel.ClientId + "&ItemId=" + row.ItemId + "&ThisSerial=" + ExecutiveModel.SerialNumber;
                        ////response = await Http.GetAsync(uri);
                        ////vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                        ////decimal? tempPrice = JsonConvert.DeserializeObject<decimal?>(vMs["data"].ToString());
                        ////row.Price = tempPrice.HasValue ? tempPrice : 0;
                        //string priceuri = "/api/Items/GetItemPriceByDealId?DealId=" + ExecutiveModel.DealTypeId + "&ItemId=" + row.ItemId;
                        //HttpResponseMessage priceresponse = await Http.GetAsync(priceuri);
                        //var pricevMs = JObject.Parse(priceresponse.Content.ReadAsStringAsync().Result);
                        //row.Price = JsonConvert.DeserializeObject<decimal>(pricevMs["data"].ToString());
                        //row.Quantity = null;
                        //row.Total = null;
                        //StateHasChanged();
                        //string uriImage = "/api/Items/GetItemImage?ItemId=" + row.ItemId;
                        //HttpResponseMessage responseImage = await Http.GetAsync(uriImage);
                        //var vMsImage = JObject.Parse(responseImage.Content.ReadAsStringAsync().Result);
                        //ItemSrc = JsonConvert.DeserializeObject<ItemVM>(vMsImage["data"].ToString());
                        //ItemImg = ItemSrc.ItemImg;
                        StateHasChanged();
                    }
                }
                else
                {
                    SelectClientChecker = true;
                }
            }
        }
        private decimal GetPriceFromDealType(int DealTypeId, LookupItem SelectedITem)
        {
            float price = 0;
            switch (DealTypeId)
            {
                case (int)EnPriceTypes.التكلفة:
                    price = SelectedITem.CostPrice;
                    break;
                case (int)EnPriceTypes.الموزع:
                    price = SelectedITem.SupplierPrice;
                    break;
                case (int)EnPriceTypes.الجملة:
                    price = SelectedITem.WholesalePrice;
                    break;
                case (int)EnPriceTypes.البيع:
                    price = SelectedITem.SellingPrice;
                    break;
                case (int)EnPriceTypes.التنفيذ:
                    price = SelectedITem.ExecutionPrice;
                    break;
                case (int)EnPriceTypes.العرض:
                    price = SelectedITem.ShowPrice;
                    break;
                case (int)EnPriceTypes.التجزئة:
                    price = SelectedITem.SellingPrice;
                    break;
                case (int)EnPriceTypes.أخرى:
                    price = SelectedITem.other_Price;
                    break;
            }
            return (decimal)price;
        }
        private void ShowImage(ExecutiveItemsVM row)
        {
            ItemImg = row.ItemImg;
            ShowImg = true;
        }
        void RecalculateTotal()
        {
            decimal CalculatedTotal = 0;
            foreach (var BillStore in BillStoresList)
            {
                foreach (var rowitem in BillStore.ExecutiveItems)
                {
                    if (rowitem.Quantity != null && rowitem.Price != null)
                    {
                        rowitem.Total = rowitem.Quantity.Value * rowitem.Price.Value;
                        CalculatedTotal = (decimal)(CalculatedTotal + rowitem.Total);
                    }
                }
            }
            foreach (var rowExpense in ExpenseList)
            {
                if (rowExpense.Amount != null)
                {
                    CalculatedTotal = (decimal)(CalculatedTotal + rowExpense.Amount.Value);
                }
            }
            ExecutiveModel.TotalPrice = CalculatedTotal;
            ExecutiveModel.TotalAfterDiscount = ExecutiveModel.TotalPrice;

            if (ExecutiveModel.DiscountValue != null & ExecutiveModel.DiscountValue != 0 & ExecutiveModel.DiscountPrecentage == 0)
            {
                decimal temp = ExecutiveModel.DiscountValue.Value / ExecutiveModel.TotalPrice.Value;
                ExecutiveModel.DiscountPrecentage = temp * 100;
                ExecutiveModel.TotalAfterDiscount = (ExecutiveModel.TotalPrice == null ? 0 : ExecutiveModel.TotalPrice) - ExecutiveModel.DiscountValue;
            }
            else if (ExecutiveModel.DiscountPrecentage != null & ExecutiveModel.DiscountPrecentage != 0 & ExecutiveModel.DiscountValue == 0)
            {
                decimal temp = (ExecutiveModel.TotalPrice.Value * ExecutiveModel.DiscountPrecentage.Value) / 100;
                ExecutiveModel.DiscountValue = temp;
                ExecutiveModel.TotalAfterDiscount = (ExecutiveModel.TotalPrice == null ? 0 : ExecutiveModel.TotalPrice) - ExecutiveModel.DiscountValue;
            }
            else
            {
                ExecutiveModel.TotalAfterDiscount = (ExecutiveModel.TotalPrice == null ? 0 : ExecutiveModel.TotalPrice) - ExecutiveModel.DiscountValue;
            }
            if (ExecutiveModel.TotalAfterDiscount == null)
            {
                ExecutiveModel.TotalAfterDiscount = ExecutiveModel.TotalPrice;
            }
            ExecutiveModel.TotalAfterDiscount = Math.Round((decimal)ExecutiveModel.TotalAfterDiscount, 0);
            try
            {
                NumberToAlphabetic Obj = new NumberToAlphabetic();
                Obj.ConvertNumbersToArabicAlphabet(ExecutiveModel.TotalAfterDiscount.ToString());
                TotalInWords = Obj.GetNumberAr();
            }
            catch (Exception)
            {

            }

            decimal DeductionAmount = 0;
            decimal AddtionalAmount = 0;
            if (ExecutiveModel.SourceDeduction != 0)
            {
                ExecutiveModel.SourceDeductionAmount = (decimal)(((ExecutiveModel.TotalAfterDiscount == null ? 0 : ExecutiveModel.TotalAfterDiscount) * ExecutiveModel.SourceDeduction) / 100);
                DeductionAmount = ExecutiveModel.SourceDeductionAmount;
            }
            if (ExecutiveModel.AddtionTax != 0)
            {
                ExecutiveModel.AddtionTaxAmount = (decimal)(((ExecutiveModel.TotalAfterDiscount == null ? 0 : ExecutiveModel.TotalAfterDiscount) * ExecutiveModel.AddtionTax) / 100);
                AddtionalAmount = ExecutiveModel.AddtionTaxAmount;
            }
            ExecutiveModel.TotalAfterTax = (decimal)(ExecutiveModel.TotalAfterDiscount - DeductionAmount + AddtionalAmount);
            ExecutiveModel.TotalAfterTax = Math.Round(ExecutiveModel.TotalAfterTax, 0);
            try
            {
                NumberToAlphabetic Obj = new NumberToAlphabetic();
                Obj.ConvertNumbersToArabicAlphabet(ExecutiveModel.TotalAfterTax.ToString());
                TotalInWordsTax = Obj.GetNumberAr();
            }
            catch (Exception)
            {
            }
            StateHasChanged();
        }

        //protected void GetItemTotal(ExecutiveItemsVM row)
        //{
        //    row.Total = row.Quantity.Value * row.Price.Value;
        //    ExecutiveModel.TotalPrice = (ExecutiveModel.TotalPrice == null ? 0 : ExecutiveModel.TotalPrice) + row.Total;
        //    if (ExecutiveModel.DiscountPrecentage != 0 && ExecutiveModel.DiscountPrecentage != null)
        //    {
        //        decimal temp = (ExecutiveModel.TotalPrice.Value * ExecutiveModel.DiscountPrecentage.Value) / 100;
        //        ExecutiveModel.TotalAfterDiscount = ExecutiveModel.TotalPrice - temp;
        //        ExecutiveModel.DiscountValue = temp;
        //        ExecutiveModel.TotalAfterDiscount = ExecutiveModel.TotalPrice - ExecutiveModel.DiscountValue;
        //    }
        //    StateHasChanged();
        //}
        //protected void GetItemTotalEdit(ExecutiveItemsVM row)
        //{
        //    decimal? olditemtotal = (row.Total == null ? 0 : row.Total);
        //    row.Total = row.Quantity.Value * row.Price;
        //    ExecutiveModel.TotalPrice = (ExecutiveModel.TotalPrice == null ? 0 : ExecutiveModel.TotalPrice) + row.Total - olditemtotal;
        //    if (ExecutiveModel.DiscountPrecentage != 0 && ExecutiveModel.DiscountPrecentage != null)
        //    {
        //        decimal temp = (ExecutiveModel.TotalPrice.Value * ExecutiveModel.DiscountPrecentage.Value) / 100;
        //        ExecutiveModel.TotalAfterDiscount = ExecutiveModel.TotalPrice - temp;
        //        ExecutiveModel.DiscountValue = temp;
        //        ExecutiveModel.TotalAfterDiscount = ExecutiveModel.TotalPrice - ExecutiveModel.DiscountValue;
        //    }
        //    StateHasChanged();
        //}
        //protected void GetExpenseTotal(ExecutiveSellExpensesVM row)
        //{
        //    ExecutiveModel.TotalPrice = (ExecutiveModel.TotalPrice == null ? 0 : ExecutiveModel.TotalPrice) + row.Amount.Value;
        //    if (ExecutiveModel.DiscountPrecentage != 0 && ExecutiveModel.DiscountPrecentage != null)
        //    {
        //        decimal temp = (ExecutiveModel.TotalPrice.Value * ExecutiveModel.DiscountPrecentage.Value) / 100;
        //        ExecutiveModel.TotalAfterDiscount = ExecutiveModel.TotalPrice - temp;
        //        ExecutiveModel.DiscountValue = temp;
        //        ExecutiveModel.TotalAfterDiscount = ExecutiveModel.TotalPrice - ExecutiveModel.DiscountValue;
        //    }
        //    StateHasChanged();
        //}
        //protected void GetExpenseTotalEdit(ExecutiveSellExpensesVM row)
        //{
        //    ExecutiveModel.TotalPrice = (ExecutiveModel.TotalPrice == null ? 0 : ExecutiveModel.TotalPrice) + row.Amount.Value;
        //    if (ExecutiveModel.DiscountPrecentage != 0 && ExecutiveModel.DiscountPrecentage != null)
        //    {
        //        decimal temp = (ExecutiveModel.TotalPrice.Value * ExecutiveModel.DiscountPrecentage.Value) / 100;
        //        ExecutiveModel.TotalAfterDiscount = ExecutiveModel.TotalPrice - temp;
        //        ExecutiveModel.DiscountValue = temp;
        //        ExecutiveModel.TotalAfterDiscount = ExecutiveModel.TotalPrice - ExecutiveModel.DiscountValue;
        //    } 
        //    StateHasChanged();
        //}
        //void CalculateDiscountPrec()
        //{
        //    if (ExecutiveModel.DiscountPrecentage != 0 && ExecutiveModel.DiscountPrecentage != null)
        //    {
        //        decimal temp = (ExecutiveModel.TotalPrice.Value * ExecutiveModel.DiscountPrecentage.Value) / 100;
        //        ExecutiveModel.TotalAfterDiscount = (ExecutiveModel.TotalPrice == null ? 0 : ExecutiveModel.TotalPrice) - temp;
        //        ExecutiveModel.DiscountValue = temp;
        //    }

        //}
        //void CalculateDiscountAmt()
        //{
        //    if (ExecutiveModel.DiscountValue != 0 && ExecutiveModel.DiscountValue != null)
        //    {
        //        ExecutiveModel.TotalAfterDiscount = (ExecutiveModel.TotalPrice == null ? 0 : ExecutiveModel.TotalPrice) - ExecutiveModel.DiscountValue;
        //        decimal temp = ExecutiveModel.DiscountValue.Value / ExecutiveModel.TotalPrice.Value;
        //        ExecutiveModel.DiscountPrecentage = temp * 100;
        //    }
        //}
        //protected async Task AddNewItem()
        //{
        //    await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/AddItems", "جارى التحميل", 1500);
        //}
        protected async Task AddNewClient()
        {
            await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/ClientAdd", "جارى التحميل", 1500);
        }
        protected async Task AddNewClientPayment()
        {
            decimal? Total = ShowTaxPart ? ExecutiveModel.TotalAfterTax : ExecutiveModel.TotalAfterDiscount;
            await jsRuntime.InvokeAsync<object>("open", "/ClientPaymentAdd?ClientId=" + ExecutiveModel.ClientId + "&SerialNumber=" + ExecutiveModel.SerialNumber + "&Total=" + Total + "&Branch=" + ExecutiveModel.BranchId, "_blank");

        }
        void AddNewStore()
        {
            int IdentifierCount = BillStoresList.Count;
            BillStoresModel = new ExecutiveBillStoresVM { Identifer = IdentifierCount + 1, };
            sItemModel = new ExecutiveItemsVM { Identifer = 1, };
            var TempsItemList = new List<ExecutiveItemsVM>();
            TempsItemList.Add(sItemModel);
            BillStoresModel.ExecutiveItems = TempsItemList;
            //SalesBillStoresModel.Store = StoreList;
            BillStoresList.Add(BillStoresModel);
            StateHasChanged();
        }
        void RemoveNewStore(ExecutiveBillStoresVM Store)
        {
            BillStoresList.Remove(Store);
            StateHasChanged();
        }
        async Task ReloadClientsList()
        {
            clients.Name = String.Empty;

            var ListReturn = await Http.GetAsync("api/Client/GetClients");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ClientList = JsonConvert.DeserializeObject<List<LookupKeyValueVM>>(vMs["data"].ToString());

        }

        async Task ReloadItemsList(ExecutiveItemsVM row)
        {
            row.ItemName = String.Empty;
            row.Box_ParCode = String.Empty;
            row.Price = 0;


            var ItemListReturn = await Http.GetAsync("api/Items/GetItem");
            var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
            ItemList = JsonConvert.DeserializeObject<List<LookupItem>>(ItemvMs["data"].ToString());


        }
    }
}