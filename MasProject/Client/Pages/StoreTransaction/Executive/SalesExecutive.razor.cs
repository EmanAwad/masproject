﻿using MasProject.Domain.ViewModel.Hierarchy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel.Items;
using MasProject.Client.Shared;
using Microsoft.JSInterop;
using System;

namespace MasProject.Client.Pages.StoreTransaction.Executive
{
    public partial class SalesExecutive
    {
        private PageTitle PageTitle;
        bool ShowDelete = false;
        List<ExecutiveBillsVM> ExecutiveList = new List<ExecutiveBillsVM>();
        int ExecutiveBillID;
        int ClientId;
        int SerialNumber;
        ClientsVM clients = new ClientsVM();
        List<LookupKeyValueVM> ClientList = new List<LookupKeyValueVM>();
        List<BranchVM> BranchList = new List<BranchVM>();

        ExecutiveBillsVM ExecutiveModel = new ExecutiveBillsVM();
        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        {
            //Fill Table
            var ListReturn = await Http.GetAsync("api/ExecutiveBills/GetExecutiveBills");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ExecutiveList = JsonConvert.DeserializeObject<List<ExecutiveBillsVM>>(vMs["data"].ToString());
            //Fill Dropdowns
            var SalesListReturn = await Http.GetAsync("api/ExecutiveBills/GetListDDL");
            var SalesvMs = JObject.Parse(SalesListReturn.Content.ReadAsStringAsync().Result);
            ExecutiveModel = JsonConvert.DeserializeObject<ExecutiveBillsVM>(SalesvMs["data"].ToString());
            ClientList = ExecutiveModel.ClientList;
            BranchList = ExecutiveModel.BranchList;
        }
        async Task TableRenew()
        {
            var ListReturn = await Http.GetAsync("api/ExecutiveBills/GetExecutiveBills");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ExecutiveList = JsonConvert.DeserializeObject<List<ExecutiveBillsVM>>(vMs["data"].ToString());
        }
        protected async Task AddNewExecutiveBill()
        {
            await jsRuntime.InvokeAsync<object>("open", "/SalesExecutiveAdd", "_blank");

        }
        protected async Task EditOldExecutiveBill(int ID, string Type)
        {
            await jsRuntime.InvokeAsync<object>("open", "/SalesExecutiveEdit?ID=" + ID + "&Type=" + Type, "_blank");
        }
        protected void DeleteExecutiveBill(int Id, int serialNumber, int Clientid)
        {
            ExecutiveBillID = Id;
            ClientId = Clientid;
            SerialNumber = serialNumber;
            ShowDelete = true;
        }
        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                string uri = "/api/ExecutiveBills/DeleteExecutiveBills?ID=" + ExecutiveBillID + "&SerialNumber=" + SerialNumber + "&ClientId=" + ClientId;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
            ShowDelete = false;

        }

        async Task ReloadClientsList()
        {
            clients.Name = String.Empty;

            var ListReturn = await Http.GetAsync("api/Client/GetClients");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ClientList = JsonConvert.DeserializeObject<List<LookupKeyValueVM>>(vMs["data"].ToString());

        }

        protected async Task GetAllByClient()
        {
            ExecutiveModel.ClientId = ClientList.Find(x => x.Name == clients.Name).ID;
            string uri = "/api/ExecutiveBills/GetSalesByClient?ClientId=" + ExecutiveModel.ClientId;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            ExecutiveList = JsonConvert.DeserializeObject<List<ExecutiveBillsVM>>(vMs["data"].ToString());
            StateHasChanged();
            //SalesModel.ClientId = ClientList.Find(x => x.Name == clients.Name).ID;
            //if (SalesModel.ClientId != 0)
            //{
            //    SalesList = SalesList.Where(x => x.ClientId == SalesModel.ClientId).ToList();
            //}
            //else
            //{
            //    SalesList = TempSalesList;
            //}
        }
        //protected async Task GetAllByBranch()
        //{
        //    //if (SalesModel.BranchId != 0)
        //    //{
        //    //    SalesList = SalesList.Where(x => x.BranchId == SalesModel.BranchId).ToList();
        //    //}
        //    //else
        //    //{
        //    //    SalesList = TempSalesList;
        //    //}
        //    //SalesList = SalesList.Where(x => x.BranchId == SalesModel.BranchId).ToList();
        //    string SalesBranch = "/api/SalesBills/GetSalesByBranch?BranchId=" + SalesModel.BranchId;
        //    HttpResponseMessage SalesBranchReturn = await Http.GetAsync(SalesBranch);
        //    var SalesBranchVMs = JObject.Parse(SalesBranchReturn.Content.ReadAsStringAsync().Result);
        //    SalesList = JsonConvert.DeserializeObject<List<SalesBillsVM>>(SalesBranchVMs["data"].ToString());
        //}

        protected async Task GetAllByBranch()
        {


            string uri = "/api/ExecutiveBills/GetSalesByBranch?BranchId=" + ExecutiveModel.BranchId;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            ExecutiveList = JsonConvert.DeserializeObject<List<ExecutiveBillsVM>>(vMs["data"].ToString());


            StateHasChanged();
        }
    }
}