﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using MasProject.Domain.ViewModel.StoreTransaction.Purchase;
using MasProject.Client.Shared;
using Microsoft.JSInterop;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Hierarchy;
using System;
using MasProject.Domain.ViewModel.Persons;
using System.Net.Http;


namespace MasProject.Client.Pages.StoreTransaction.Purchase
{
    public partial class PurchasesBill
    {
        private PageTitle PageTitle;
        bool ShowDelete = false;
        List<PurchaseBillVM> PurchaseList = new List<PurchaseBillVM>();
        List<LookupKeyValueVM> SupplierList = new List<LookupKeyValueVM>();
        
        List<BranchVM> BranchList = new List<BranchVM>();

        PurchaseBillVM PurchasesModel = new PurchaseBillVM();
        SupplierVM supplier = new SupplierVM();
        int PurchasesBillID;

        int SupplierId;
        int SerialNumber;
        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        {
            var PurchaseListReturn = await Http.GetAsync("api/Purchase/GetPurchases");
            var vMMs = JObject.Parse(PurchaseListReturn.Content.ReadAsStringAsync().Result);
            BillsTbl<PurchaseBillVM> ReturnModel = JsonConvert.DeserializeObject<BillsTbl<PurchaseBillVM>>(vMMs["data"].ToString());
            PurchaseList = ReturnModel.BillsList;
            SupplierList = ReturnModel.SearchItems.SupplierList;
            BranchList = ReturnModel.SearchItems.BranchList;

            //Fill Table
            //var ListReturn = await Http.GetAsync("api/Purchase/GetPurchases");
            //var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            //PurchaseList = JsonConvert.DeserializeObject<List<PurchaseBillVM>>(vMs["data"].ToString());
            ////Fill Dropdowns
            //var PurchaseListReturn = await Http.GetAsync("api/Purchase/GetListTbl");
            //var vMMs = JObject.Parse(PurchaseListReturn.Content.ReadAsStringAsync().Result);
            //PurchasesModel = JsonConvert.DeserializeObject<PurchaseBillVM>(vMMs["data"].ToString());
        }
        protected void DeletePurchaseBill(int Id, int serialNumber, int Supplierid)
        {
            PurchasesBillID = Id;
            SupplierId = Supplierid;
            SerialNumber = serialNumber;
            ShowDelete = true;
        }
        protected async Task AddNewPurchaseBill()
        {
            await jsRuntime.InvokeAsync<object>("open", "/PurchasesBillAdd", "_blank");
        }
        protected async Task EditOldPurchaseBill(int ID)
        {
            await jsRuntime.InvokeAsync<object>("open", "/PurchasesBillEdit?ID="+ID, "_blank");
        }
        protected async Task ViewOldPurchaseBill(int ID)
        {
            await jsRuntime.InvokeAsync<object>("open", "/PurchasesBillView?ID=" + ID, "_blank");
        }
        protected async Task ConfirmPurchaseBill(int ID)
        {
            await jsRuntime.InvokeAsync<object>("open", "/PurchasesBillConfirm?ID=" + ID, "_blank");
        }
        protected async Task RevirewPurchaseBill(int ID)
        {
            await jsRuntime.InvokeAsync<object>("open", "/PurchasesBillReview?ID=" + ID, "_blank");
        }

        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                string uri = "/api/Purchase/DeletePurchases?ID=" + PurchasesBillID + "&SerialNumber=" + SerialNumber + "&SupplierId=" + SupplierId;

                var result = await Http.GetAsync(uri);

                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
            ShowDelete = false;
        }
        async Task ReloadSuppliersList()
        {
            supplier.Name = String.Empty;

            var ListReturn = await Http.GetAsync("api/Supplier/GetSuppliers");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            SupplierList = JsonConvert.DeserializeObject<List<LookupKeyValueVM>>(vMs["data"].ToString());

        }

        protected async Task GetAllBySupplier()
        {
            PurchasesModel.SupplierId = SupplierList.Find(x => x.Name == supplier.Name).ID;
            if (PurchasesModel.SupplierId != 0)
            {
                string uri = "/api/Purchase/GetAllBySupplier?SupplierId=" + PurchasesModel.SupplierId;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                PurchaseList = JsonConvert.DeserializeObject<List<PurchaseBillVM>>(vMs["data"].ToString());
            }
            else
            {
                var ListReturn = await Http.GetAsync("api/Purchase/GetPurchases");
                var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
                PurchaseList = JsonConvert.DeserializeObject<List<PurchaseBillVM>>(vMs["data"].ToString());
            }

            StateHasChanged();
            //SalesModel.ClientId = ClientList.Find(x => x.Name == clients.Name).ID;
            //if (SalesModel.ClientId != 0)
            //{
            //    SalesList = SalesList.Where(x => x.ClientId == SalesModel.ClientId).ToList();
            //}
            //else
            //{
            //    SalesList = TempSalesList;
            //}
        }
        //protected async Task GetAllByBranch()
        //{
        //    //if (SalesModel.BranchId != 0)
        //    //{
        //    //    SalesList = SalesList.Where(x => x.BranchId == SalesModel.BranchId).ToList();
        //    //}
        //    //else
        //    //{
        //    //    SalesList = TempSalesList;
        //    //}
        //    //SalesList = SalesList.Where(x => x.BranchId == SalesModel.BranchId).ToList();
        //    string SalesBranch = "/api/SalesBills/GetSalesByBranch?BranchId=" + SalesModel.BranchId;
        //    HttpResponseMessage SalesBranchReturn = await Http.GetAsync(SalesBranch);
        //    var SalesBranchVMs = JObject.Parse(SalesBranchReturn.Content.ReadAsStringAsync().Result);
        //    SalesList = JsonConvert.DeserializeObject<List<SalesBillsVM>>(SalesBranchVMs["data"].ToString());
        //}

        protected async Task GetAllByBranch()
        {

            if (PurchasesModel.BranchId != 0)
            {
                string uri = "/api/Purchase/GetAllByBranch?BranchId=" + PurchasesModel.BranchId;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                PurchaseList = JsonConvert.DeserializeObject<List<PurchaseBillVM>>(vMs["data"].ToString());
            }
            else
            {
                var ListReturn = await Http.GetAsync("api/Purchase/GetPurchases");
                var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
                PurchaseList = JsonConvert.DeserializeObject<List<PurchaseBillVM>>(vMs["data"].ToString());
            }


            StateHasChanged();
        }

        protected async Task GetAllByDate()
        {
            if (PurchasesModel.Date != null)
            {
                string uri = "/api/Purchase/GetAllByDate?date=" + PurchasesModel.Date;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                PurchaseList = JsonConvert.DeserializeObject<List<PurchaseBillVM>>(vMs["data"].ToString());
            }
            else
            {
                var ListReturn = await Http.GetAsync("api/Purchase/GetPurchases");
                var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
                PurchaseList = JsonConvert.DeserializeObject<List<PurchaseBillVM>>(vMs["data"].ToString());
            }


            StateHasChanged();

        }
    }
}