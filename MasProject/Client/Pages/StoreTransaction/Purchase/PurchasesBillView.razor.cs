﻿using MasProject.Domain.ViewModel.Hierarchy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using MasProject.Domain.ViewModel.StoreTransaction.Purchase;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Items;
using MasProject.Client.Shared;
using System;

namespace MasProject.Client.Pages.StoreTransaction.Purchase
{
    public partial class PurchasesBillView
    {
        PurchaseBillVM PurchasesModel = new PurchaseBillVM();
        string ReportResult = "";
        protected override async Task OnInitializedAsync()
        {
            var querystring = ExtensionMethods.QueryString(navigationManager);
            PurchasesModel.ID = int.Parse(querystring["ID"]);

            string uri = "/api/Purchase/PurchaseBillsReport?Id=" + PurchasesModel.ID;
            ReportResult = uri;
        }

    }
}