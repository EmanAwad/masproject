﻿using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using MasProject.Domain.ViewModel.StoreTransaction.Purchase;
using Newtonsoft.Json;
using System.Text;

namespace MasProject.Client.Pages.StoreTransaction.Purchase
{
    public partial class PurchasesBillConfirm
    {
        PurchaseBillVM PurchasesModel = new PurchaseBillVM();
        string ReportResult="";
        bool ShowConfirm = false;

        protected override async Task OnInitializedAsync()
        {
            var querystring = ExtensionMethods.QueryString(navigationManager);
            PurchasesModel.ID = int.Parse(querystring["ID"]);

            string uri = "/api/Purchase/PurchaseBillsReport?Id=" + PurchasesModel.ID;
            ReportResult = uri;
        }
        protected async Task ConfirmPurchasesBill()
        {
            HttpResponseMessage result = new HttpResponseMessage();

            var AddModel = JsonConvert.SerializeObject(PurchasesModel);
            var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
            result = await Http.PostAsync("api/Purchase/ConfirmPurchasesBill", content);

            if (result.IsSuccessStatusCode)
            {
                StateHasChanged();
                ShowConfirm = true;
            }

        }
        protected async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        }
    }
}