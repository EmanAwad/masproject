﻿using MasProject.Domain.ViewModel.Hierarchy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Domain.ViewModel.StoreTransaction.Purchase;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Items;
using MasProject.Client.Shared;
using static MasProject.Domain.Enums.Enums;
using System;
using MasProject.Domain.ViewModel.Persons;

namespace MasProject.Client.Pages.StoreTransaction.Purchase
{
    public partial class PurchasesBillAdd
    {
        private PageTitle PageTitle;
        ItemBillVM ItemBill = new ItemBillVM();
        PurchaseItemVM PItemModel = new PurchaseItemVM();
        List<PurchaseItemVM> PItemList = new List<PurchaseItemVM>();
        PurchaseOtherIncomeVM PIncomeModel = new PurchaseOtherIncomeVM();
        List<PurchaseOtherIncomeVM> PIncomeList = new List<PurchaseOtherIncomeVM>();
        PurchaseBillVM PurchaseModel = new PurchaseBillVM();
        List<StoreVM> StoreList = new List<StoreVM>();
        List<BranchVM> BranchList = new List<BranchVM>();
        List<LookupKeyValueVM> SupplierList = new List<LookupKeyValueVM>();
        //List<DealTypeVM> DealTypeList = new List<DealTypeVM>();
        List<LookupItem> ItemList = new List<LookupItem>();
        List<OtherIncomeVM> IncomeList = new List<OtherIncomeVM>();
        bool FirstAdd = false;
        bool ShowConfirm = false;
        bool ShowTaxPart = false;
        string TotalInWords = "";
        string TotalInWordsTax = "";
        int FlagTypeID = 0;
        string ItemImg = "";
        bool ShowImg = false;
        bool SelectSupplierChecker = false;
        bool IsDisabled = false;
        protected override async Task OnInitializedAsync()
        {
            FirstAdd = false;
            await BindList();
            PItemModel = new PurchaseItemVM { Identifer = 1, };
            PItemList = new List<PurchaseItemVM>();
            PIncomeList = new List<PurchaseOtherIncomeVM>();
            PIncomeModel = new PurchaseOtherIncomeVM { Identifer = 1, };
            PIncomeList.Add(PIncomeModel);
            PItemList.Add(PItemModel);
            PurchaseModel.PurchaseItem = PItemList;
            PurchaseModel.PurchaseIncome = PIncomeList;

        }
        async Task BindList()
        {
            var ListReturn = await Http.GetAsync("api/Purchase/GetListDDL");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            PurchaseModel = JsonConvert.DeserializeObject<PurchaseBillVM>(vMs["data"].ToString());
            StoreList = PurchaseModel.StoreList;
            SupplierList = PurchaseModel.SupplierList;
            //DealTypeList = PurchaseModel.DealTypeList;
            ItemList = PurchaseModel.ItemList;
            IncomeList = PurchaseModel.OtherIncomeList;
            BranchList = PurchaseModel.BranchList;
        }
        async Task SuppliersRenew()
        {
            var SupplierListReturn = await Http.GetAsync("api/Supplier/GetSuppliers");
            var SuppliervMs = JObject.Parse(SupplierListReturn.Content.ReadAsStringAsync().Result);
            SupplierList = JsonConvert.DeserializeObject<List<LookupKeyValueVM>>(SuppliervMs["data"].ToString());

        }
        void ChangeItemQuantity()
        {
            RecalculateTotal();
            AddNewRow();
        }
        async Task ItemsRenew(PurchaseItemVM row)
        {
            var ItemListReturn = await Http.GetAsync("api/Items/GetItems");
            var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
            ItemList = JsonConvert.DeserializeObject<List<LookupItem>>(ItemvMs["data"].ToString());
            //get price for selected items
            //string priceuri = "/api/Items/GetItemPriceByDealId?DealId=" + PurchaseModel.DealTypeId + "&ItemId=" + row.ItemId;
            //HttpResponseMessage priceresponse = await Http.GetAsync(priceuri);
            //var pricevMs = JObject.Parse(priceresponse.Content.ReadAsStringAsync().Result);
            //row.Price = JsonConvert.DeserializeObject<decimal>(pricevMs["data"].ToString());
            var url = "/api/Purchase/GetLatestPrice?ItemId=" + row.ItemId;
            HttpResponseMessage PriceReturn = await Http.GetAsync(url);
            var PricevMs = JObject.Parse(PriceReturn.Content.ReadAsStringAsync().Result);
            row.Price = JsonConvert.DeserializeObject<decimal>(PricevMs["data"].ToString());
            row.Quantity = null;
            row.Total = null;
            RecalculateTotal();
            StateHasChanged();
        }
        async Task IncomeRenew()
        {
            var IncomesListReturn = await Http.GetAsync("api/OtherIncome/GetOtherIncome");
            var IncomesvMs = JObject.Parse(IncomesListReturn.Content.ReadAsStringAsync().Result);
            IncomeList = JsonConvert.DeserializeObject<List<OtherIncomeVM>>(IncomesvMs["data"].ToString());
        }
        async Task BalancesRenew()
        {
            string priceuri = "/api/SupplierBalance/GetBalance?SupplierId=" + PurchaseModel.SupplierId;
            HttpResponseMessage ClientListReturn = await Http.GetAsync(priceuri);
            var ClientvMs = JObject.Parse(ClientListReturn.Content.ReadAsStringAsync().Result);
            PurchaseModel.Balance = JsonConvert.DeserializeObject<decimal>(ClientvMs["data"].ToString());
        }
        async Task GetSerialNumber()
        {
            string serailuri = "/api/Purchase/GenerateSerial?BranchId=" + PurchaseModel.BranchId;
            HttpResponseMessage serialReturn = await Http.GetAsync(serailuri);
            var serialvms = JObject.Parse(serialReturn.Content.ReadAsStringAsync().Result);
            PurchaseModel.SerialNumber = JsonConvert.DeserializeObject<int>(serialvms["data"].ToString());
            StateHasChanged();

        }
        //void Confirm()
        //{
        //    ShowConfirm = false;
        //    IsDisabled = false;
        //}
        protected async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        }
        protected async Task AddPurchaseBill()
        {
            HttpResponseMessage result = new HttpResponseMessage();
            //check for save
            if (PurchaseModel.SupplierId != 0)
            {
                if (PurchaseModel.SerialNumber != 0)
                {
                    if (FirstAdd == false)
                    {
                        var AddModel = JsonConvert.SerializeObject(PurchaseModel);
                        var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/Purchase/AddPurchases", content);
                        var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                        PurchaseModel.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                        FirstAdd = true;
                    }
                    else
                    {
                        PurchaseModel.PurchaseItem = PItemList;
                        PurchaseModel.PurchaseIncome = PIncomeList;
                        var EditModel = JsonConvert.SerializeObject(PurchaseModel);
                        var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/Purchase/EditPurchases", content);
                    }
                }
                if (result.IsSuccessStatusCode)
                {

                    StateHasChanged();
                }
            }
            else
            {
                SelectSupplierChecker = true;
            }
        }
        protected async Task EditPurchasesBill()
        {
            IsDisabled = true;
            HttpResponseMessage result = new HttpResponseMessage();
            //check for save
            if (PurchaseModel.SupplierId != 0)
            {
                if (PurchaseModel.SerialNumber != 0)
                {
                    if (FirstAdd == false)
                    {
                        var AddModel = JsonConvert.SerializeObject(PurchaseModel);
                        var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/Purchase/AddPurchases", content);
                        var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                        PurchaseModel.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                        FirstAdd = true;
                    }
                    else
                    {
                        PurchaseModel.PurchaseItem = PItemList;
                        PurchaseModel.PurchaseIncome = PIncomeList;
                        var EditModel = JsonConvert.SerializeObject(PurchaseModel);
                        var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/Purchase/EditPurchases", content);
                    }
                }
                if (result.IsSuccessStatusCode)
                {
                    StateHasChanged();
                    ShowConfirm = true;
                }
            }
            else
            {
                SelectSupplierChecker = true;
            }
        }
        void AddNewRow()
        {
            int IdentifierCount = PItemList.Count;
            PItemModel = new PurchaseItemVM { Identifer = IdentifierCount + 1, };
            PItemList.Add(PItemModel);
        }
        void RemoveRow(PurchaseItemVM row)
        {
            PItemList.Remove(row);
            StateHasChanged();
        }
        void AddNewIncomeRow()
        {
            int IdentifierCount = PIncomeList.Count;
            PIncomeModel = new PurchaseOtherIncomeVM { Identifer = IdentifierCount + 1, };
            PIncomeList.Add(PIncomeModel);
        }
        void RemoveIncomeRow(PurchaseOtherIncomeVM row)
        {
            PIncomeList.Remove(row);
            StateHasChanged();
        }
        protected async Task GetBalance()
        {
            string priceuri = "/api/SupplierBalance/GetBalance?SupplierId=" + PurchaseModel.SupplierId;
            HttpResponseMessage ClientListReturn = await Http.GetAsync(priceuri);
            var ClientvMs = JObject.Parse(ClientListReturn.Content.ReadAsStringAsync().Result);
            PurchaseModel.Balance = JsonConvert.DeserializeObject<decimal>(ClientvMs["data"].ToString());

            string Taxuri = "/api/Supplier/GetTaxFile?Id=" + PurchaseModel.SupplierId;
            HttpResponseMessage TaxReturn = await Http.GetAsync(Taxuri);
            var TaxvMs = JObject.Parse(TaxReturn.Content.ReadAsStringAsync().Result);
            SupplierVM TempFile = JsonConvert.DeserializeObject<SupplierVM>(TaxvMs["data"].ToString());
            PurchaseModel.TaxFileNumber = TempFile.Taxfile;

            StateHasChanged();
        }
        protected async Task GetItemDataByCode(PurchaseItemVM row)
        {
            LookupItem SelectedITem = ItemList.Find(x => (x.BoxCode == row.Box_ParCode || x.GuaranteeParCode == row.Box_ParCode || x.InterationalParCode == row.Box_ParCode || x.Code == row.Box_ParCode || x.ParCodeNote == row.Box_ParCode || x.SupplierParCode == row.Box_ParCode));
            ItemImg = SelectedITem.ItemImg ?? "";
            row.ItemId = SelectedITem.ID;
            row.ItemName = SelectedITem.Name;
            //if (PurchaseModel.DealTypeId != null)
            //{
            //    row.Price = GetPriceFromDealType((int)PurchaseModel.DealTypeId, SelectedITem);
            //}
            var url = "/api/Purchase/GetLatestPrice?ItemId=" + row.ItemId;
            HttpResponseMessage PriceReturn = await Http.GetAsync(url);
            var PricevMs = JObject.Parse(PriceReturn.Content.ReadAsStringAsync().Result);
            row.Price = JsonConvert.DeserializeObject<decimal>(PricevMs["data"].ToString());
            row.Quantity = null;
            row.Total = null;
            StateHasChanged();
            HttpResponseMessage result = new HttpResponseMessage();
            //check for save
            if (PurchaseModel.SerialNumber != 0)
            {
                if (PurchaseModel.SupplierId != 0)
                {
                    if (FirstAdd == false)
                    {
                        var AddModel = JsonConvert.SerializeObject(PurchaseModel);
                        var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/Purchase/AddPurchases", content);
                        var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                        PurchaseModel.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                        FirstAdd = true;
                    }
                    else
                    {
                        PurchaseModel.PurchaseItem = PItemList;
                        PurchaseModel.PurchaseIncome = PIncomeList;
                        var EditModel = JsonConvert.SerializeObject(PurchaseModel);
                        var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/Purchase/EditPurchases", content);
                    }
                    if (result.IsSuccessStatusCode)
                    {

                        StateHasChanged();
                    }
                }
                else
                {
                    SelectSupplierChecker = true;
                }
            }
        }
        protected async Task GetItemData(PurchaseItemVM row)
        {
            //   LookupItem SelectedITem = ItemList.Find(x => x.ID == row.ItemId);
            LookupItem SelectedITem = ItemList.Find(x => x.Name == row.ItemName);
            ItemImg = SelectedITem.ItemImg;
            row.Box_ParCode = SelectedITem.Code;
            row.ItemId = SelectedITem.ID;
            //if (PurchaseModel.DealTypeId != null)
            //{
            //    row.Price = GetPriceFromDealType((int)PurchaseModel.DealTypeId, SelectedITem);
            //}
            var url = "/api/Purchase/GetLatestPrice?ItemId=" + row.ItemId;
            HttpResponseMessage PriceReturn = await Http.GetAsync(url);
            var PricevMs = JObject.Parse(PriceReturn.Content.ReadAsStringAsync().Result);
            row.Price = JsonConvert.DeserializeObject<decimal>(PricevMs["data"].ToString());
            row.Quantity = null;
            row.Total = null;
            StateHasChanged();
            HttpResponseMessage result = new HttpResponseMessage();
            //check for save
            if (PurchaseModel.SerialNumber != 0)
            {
                if (PurchaseModel.SupplierId != 0)
                {
                    if (FirstAdd == false)
                    {
                        var AddModel = JsonConvert.SerializeObject(PurchaseModel);
                        var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/Purchase/AddPurchases", content);
                        var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                        PurchaseModel.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());

                        FirstAdd = true;
                    }
                    else
                    {
                        PurchaseModel.PurchaseItem = PItemList;
                        PurchaseModel.PurchaseIncome = PIncomeList;
                        var EditModel = JsonConvert.SerializeObject(PurchaseModel);
                        var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/Purchase/EditPurchases", content);
                    }
                    if (result.IsSuccessStatusCode)
                    {
                        StateHasChanged();
                    }
                }
                else
                {
                    SelectSupplierChecker = true;
                }
            }
        }

        protected async Task TaxPart()
        {
            if (FlagTypeID == 1)
            {
                PurchaseModel.FlagType = true;
                ShowTaxPart = true;
                //get 14% from otherconstant where id=1
                //calculate addtions amount
                if (PurchaseModel.AddtionTax == 0)
                {
                    string uri = "/api/OtherConstant/GetOtherConstantRatio?Id=1";
                    HttpResponseMessage response = await Http.GetAsync(uri);
                    var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                    PurchaseModel.AddtionTax = JsonConvert.DeserializeObject<decimal>(vMs["data"].ToString());
                }
                RecalculateTotal();
            }
            else
            {
                PurchaseModel.FlagType = false;
                ShowTaxPart = false;
            }
        }
        void RecalculateTotal()
        {
            decimal CalculatedTotal = 0;
            foreach (var rowitem in PItemList)
            {
                if (rowitem.Quantity != null && rowitem.Price != null)
                {
                    rowitem.Total = rowitem.Quantity.Value * rowitem.Price.Value;
                    CalculatedTotal = (decimal)(CalculatedTotal + rowitem.Total);
                }
            }
            foreach (var rowIncome in PIncomeList)
            {
                if (rowIncome.Amount != null)
                {
                    CalculatedTotal = (decimal)(CalculatedTotal + rowIncome.Amount.Value);
                }
            }

            PurchaseModel.TotalPrice = CalculatedTotal;
            PurchaseModel.TotalAfterDiscount = PurchaseModel.TotalPrice;

            if (PurchaseModel.DiscountValue != null & PurchaseModel.DiscountValue != 0 & PurchaseModel.DiscountPrecentage == 0)
            {
                decimal temp = PurchaseModel.DiscountValue.Value / PurchaseModel.TotalPrice.Value;
                PurchaseModel.DiscountPrecentage = temp * 100;
                PurchaseModel.TotalAfterDiscount = (PurchaseModel.TotalPrice == null ? 0 : PurchaseModel.TotalPrice) - PurchaseModel.DiscountValue;
            }
            else if (PurchaseModel.DiscountPrecentage != null & PurchaseModel.DiscountPrecentage != 0 & PurchaseModel.DiscountValue == 0)
            {
                decimal temp = (PurchaseModel.TotalPrice.Value * PurchaseModel.DiscountPrecentage.Value) / 100;
                PurchaseModel.DiscountValue = temp;
                PurchaseModel.TotalAfterDiscount = (PurchaseModel.TotalPrice == null ? 0 : PurchaseModel.TotalPrice) - PurchaseModel.DiscountValue;
            }
            else
            {
                PurchaseModel.TotalAfterDiscount = (PurchaseModel.TotalPrice == null ? 0 : PurchaseModel.TotalPrice) - PurchaseModel.DiscountValue;
            }
            if (PurchaseModel.TotalAfterDiscount == null)
            {
                PurchaseModel.TotalAfterDiscount = PurchaseModel.TotalPrice;
            }
            PurchaseModel.TotalAfterDiscount = Math.Round((decimal)PurchaseModel.TotalAfterDiscount, 0);
            NumberToAlphabetic Obj = new NumberToAlphabetic();
            Obj.ConvertNumbersToArabicAlphabet(PurchaseModel.TotalAfterDiscount.ToString());

            TotalInWords = Obj.GetNumberAr();
            decimal DeductionAmount = 0;
            decimal AddtionalAmount = 0;
            if (PurchaseModel.SourceDeduction != 0)
            {
                PurchaseModel.SourceDeductionAmount = (decimal)(((PurchaseModel.TotalAfterDiscount == null ? 0 : PurchaseModel.TotalAfterDiscount) * PurchaseModel.SourceDeduction) / 100);
                DeductionAmount = PurchaseModel.SourceDeductionAmount;
            }
            if (PurchaseModel.AddtionTax != 0)
            {
                PurchaseModel.AddtionTaxAmount = (decimal)(((PurchaseModel.TotalAfterDiscount == null ? 0 : PurchaseModel.TotalAfterDiscount) * PurchaseModel.AddtionTax) / 100);
                AddtionalAmount = PurchaseModel.AddtionTaxAmount;
            }
            PurchaseModel.TotalAfterTax = (decimal)(PurchaseModel.TotalAfterDiscount - DeductionAmount + AddtionalAmount);
            PurchaseModel.TotalAfterTax = Math.Round(PurchaseModel.TotalAfterTax, 0);

            Obj = new NumberToAlphabetic();
            Obj.ConvertNumbersToArabicAlphabet(PurchaseModel.TotalAfterTax.ToString());
            TotalInWordsTax = Obj.GetNumberAr();
            StateHasChanged();
        }
        //protected async Task AddNewItem()
        //{
        //    await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/AddItems", "جارى التحميل", 1500);
        //}
        protected async Task AddNewSupplier()
        {
            await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/SupplierAdd", "جارى التحميل", 1000);
        }
        async Task ReloadItemsList(PurchaseItemVM row)
        {
            row.ItemName = String.Empty;
            row.Box_ParCode = String.Empty;
            row.Price = 0;


            var ItemListReturn = await Http.GetAsync("api/Items/GetItem");
            var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
            ItemList = JsonConvert.DeserializeObject<List<LookupItem>>(ItemvMs["data"].ToString());


        }
    }
}