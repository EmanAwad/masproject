﻿using MasProject.Domain.ViewModel.Hierarchy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel.StoreTransaction.Conversion;
using MasProject.Client.Shared;

namespace MasProject.Client.Pages.StoreTransaction.ItemsConversion
{
    public partial class ItemConversionAdd
    {
        private PageTitle PageTitle;
        bool ShowConfirm = false;
        ItemConversion_ItemVM CItemModelFrom = new ItemConversion_ItemVM();
        ItemConversion_ItemVM CItemModelTo = new ItemConversion_ItemVM();
        List<ItemConversion_ItemVM> CItemListFrom = new List<ItemConversion_ItemVM>();
        List<ItemConversion_ItemVM> CItemListTo = new List<ItemConversion_ItemVM>();
        ItemConversion_BillVM BillModel = new ItemConversion_BillVM();
        List<StoreVM> StoreList = new List<StoreVM>();
        List<LookupKeyValueVM> EmployeeList = new List<LookupKeyValueVM>();
        List<LookupItem> ItemList = new List<LookupItem>();
        List<BranchVM> BranchList = new List<BranchVM>();
        bool ShowImg = false;
        string ItemImg = "";
        bool IsDisabled = false;
        protected override async Task OnInitializedAsync()
        {
            await BindList();
            CItemModelFrom = new ItemConversion_ItemVM { Name = "test", IdentiferFrom = 1, QuantityFrom = 0, PriceFrom = 0, TotalFrom = 0 };
            CItemModelTo = new ItemConversion_ItemVM { Name = "test", IdentiferTo = 1, QuantityTo = 0, PriceTo = 0, TotalTo = 0 };
            CItemListFrom = new List<ItemConversion_ItemVM>();
            CItemListFrom.Add(CItemModelFrom);
            CItemListTo = new List<ItemConversion_ItemVM>();
            CItemListTo.Add(CItemModelTo);
            BillModel.Name = "Test";
            //BillModel.ConversionItemsFrom = CItemListFrom;
            //BillModel.ConversionItemsTo = CItemListTo;
        }
        async Task BindList()
        {
            //    var StoreListReturn = await Http.GetAsync("api/Store/GetStores");
            //    var StorevMs = JObject.Parse(StoreListReturn.Content.ReadAsStringAsync().Result);
            //    StoreList = JsonConvert.DeserializeObject<List<StoreVM>>(StorevMs["data"].ToString());
            //    var ClientListReturn = await Http.GetAsync("api/Employee/GetEmployees");
            //    var ClientvMs = JObject.Parse(ClientListReturn.Content.ReadAsStringAsync().Result);
            //    EmployeeList = JsonConvert.DeserializeObject<List<EmployeeVM>>(ClientvMs["data"].ToString());
            //    var ItemListReturn = await Http.GetAsync("api/Items/GetItems");
            //    var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
            //    ItemList = JsonConvert.DeserializeObject<List<ItemVM>>(ItemvMs["data"].ToString());
            var SalesListReturn = await Http.GetAsync("api/ItemConversion/GetListDDL");
            var SalesvMs = JObject.Parse(SalesListReturn.Content.ReadAsStringAsync().Result);
            BillModel = JsonConvert.DeserializeObject<ItemConversion_BillVM>(SalesvMs["data"].ToString());
            StoreList = BillModel.Store;
            EmployeeList = BillModel.Employee;
            ItemList = BillModel.ItemList;
            BranchList = BillModel.BranchList;
        }

        //async Task EmployeessRenew()
        //{
        //    var EmployeeListReturn = await Http.GetAsync("api/Employee/GetEmployees");
        //    var EmployeevMs = JObject.Parse(EmployeeListReturn.Content.ReadAsStringAsync().Result);
        //    EmployeeList = JsonConvert.DeserializeObject<List<EmployeeVM>>(EmployeevMs["data"].ToString());
        //}
        async Task GetSerialNumber()
        {
            string serailuri = "/api/ItemConversion/GenerateSerial?BranchId=" + BillModel.BranchId;
            HttpResponseMessage serialReturn = await Http.GetAsync(serailuri);
            var serialvms = JObject.Parse(serialReturn.Content.ReadAsStringAsync().Result);
            BillModel.ConversionRecordID = JsonConvert.DeserializeObject<int>(serialvms["data"].ToString());
            StateHasChanged();

        }
        async Task ItemsRenew()
        {
            var ItemListReturn = await Http.GetAsync("api/Items/GetItems");
            var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
            ItemList = JsonConvert.DeserializeObject<List<LookupItem>>(ItemvMs["data"].ToString());
        }

        void RecalculateTotalFrom()
        {
            decimal CalculatedTotal = 0;
            foreach (var rowitem in CItemListFrom)
            {
                if (rowitem.QuantityFrom != null && rowitem.PriceFrom != null)
                {
                    rowitem.TotalFrom = rowitem.QuantityFrom.Value * rowitem.PriceFrom.Value;
                    CalculatedTotal = (decimal)(CalculatedTotal + rowitem.TotalFrom);
                }
            }
            BillModel.TotalPriceFrom = CalculatedTotal;
            StateHasChanged();
        }
        void RecalculateTotalTo()
        {
            decimal CalculatedTotal = 0;
            foreach (var rowitem in CItemListTo)
            {
                if (rowitem.QuantityTo != null && rowitem.PriceTo != null)
                {
                    rowitem.TotalTo = rowitem.QuantityTo.Value * rowitem.PriceTo.Value;
                    CalculatedTotal = (decimal)(CalculatedTotal + rowitem.TotalTo);
                }
            }
            BillModel.TotalPriceTo = CalculatedTotal;
            StateHasChanged();
        }
        void AddNewFromRow()
        {
            int IdentifierCount = CItemListFrom.Count;
            CItemModelFrom = new ItemConversion_ItemVM { Name = "Test",IdentiferFrom = IdentifierCount + 1, QuantityFrom = 0, PriceFrom = 0, TotalTo = 0 };
            CItemListFrom.Add(CItemModelFrom);
            // to clear the inputs
            //isModel = new Item_Store();
            StateHasChanged();
        }
        void ChangeItemQuantityFrom()
        {
            RecalculateTotalFrom();
            AddNewFromRow();
        }
        void AddNewToRow()
        {
            int IdentifierCount = CItemListTo.Count;
            CItemModelTo = new ItemConversion_ItemVM { Name = "Test",IdentiferTo = IdentifierCount + 1, QuantityTo = 0, PriceTo = 0, TotalTo = 0 };
            CItemListTo.Add(CItemModelTo);
            // to clear the inputs
            //isModel = new Item_Store();
            StateHasChanged();
        }
        void ChangeItemQuantityTo()
        {
            RecalculateTotalTo();
            AddNewToRow();
        }
        protected void GetItemTotal(ItemConversion_ItemVM row)
        {
            row.TotalFrom = row.QuantityFrom * row.PriceFrom;
            BillModel.TotalPriceFrom = BillModel.TotalPriceFrom + row.TotalFrom;
            row.TotalTo = row.QuantityTo * row.PriceTo;
            BillModel.TotalPriceTo = BillModel.TotalPriceTo + row.TotalTo;
            StateHasChanged();
        }

        void RemoveFromRow(ItemConversion_ItemVM row)
        {
            CItemListFrom.Remove(row);
            StateHasChanged();
        }

        void RemoveToRow(ItemConversion_ItemVM row)
        {
            CItemListTo.Remove(row);
            StateHasChanged();
        }

        async Task GetItemFromDataByCode(ItemConversion_ItemVM row)
        {
            //string uri = "/api/Items/GetNationalCodeByID?Id=" + row.ItemIdFrom;
            //HttpResponseMessage response = await Http.GetAsync(uri);
            //var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            //row.ParCodeFrom = JsonConvert.DeserializeObject<string>(vMs["data"].ToString());
            LookupItem SelectedITem = ItemList.Find(x => (x.BoxCode == row.ParCodeFrom || x.GuaranteeParCode == row.ParCodeFrom || x.InterationalParCode == row.ParCodeFrom || x.Code == row.ParCodeFrom || x.ParCodeNote == row.ParCodeFrom || x.SupplierParCode == row.ParCodeFrom));
            ItemImg = SelectedITem.ItemImg ?? "";
            row.ItemIdFrom = SelectedITem.ID;
            row.ItemNameFrom = SelectedITem.Name;
            row.ParCodeFrom = SelectedITem.Code;
            row.QuantityFrom = null;
            row.TotalFrom = null;
            var url = "/api/PurchasesReturns/GetLatestPrice?ItemId=" + row.ItemIdFrom;
            HttpResponseMessage PriceReturn = await Http.GetAsync(url);
            var PricevMs = JObject.Parse(PriceReturn.Content.ReadAsStringAsync().Result);
            row.PriceFrom = JsonConvert.DeserializeObject<decimal>(PricevMs["data"].ToString());
            if (row.PriceFrom == 0)
            {
                row.PriceFrom = (decimal?)SelectedITem.CostPrice;
            }          
            StateHasChanged();
        }

        async Task GetItemToDataByCode(ItemConversion_ItemVM row)
        {
            //string uri = "/api/Items/GetNationalCodeByID?Id=" + row.ItemIdFrom;
            //HttpResponseMessage response = await Http.GetAsync(uri);
            //var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            //row.ParCodeFrom = JsonConvert.DeserializeObject<string>(vMs["data"].ToString());
            LookupItem SelectedITem = ItemList.Find(x => (x.BoxCode == row.ParCodeTo || x.GuaranteeParCode == row.ParCodeTo || x.InterationalParCode == row.ParCodeTo || x.Code == row.ParCodeTo || x.ParCodeNote == row.ParCodeTo || x.SupplierParCode == row.ParCodeTo));
            ItemImg = SelectedITem.ItemImg ?? "";
            row.ItemIdTo = SelectedITem.ID;
            row.ItemNameTo = SelectedITem.Name;
            //row.PriceTo = (decimal?)SelectedITem.CostPrice;
            row.ParCodeTo = SelectedITem.Code;
            row.QuantityTo = null;
            row.TotalTo = null;
            var url = "/api/PurchasesReturns/GetLatestPrice?ItemId=" + row.ItemIdTo;
            HttpResponseMessage PriceReturn = await Http.GetAsync(url);
            var PricevMs = JObject.Parse(PriceReturn.Content.ReadAsStringAsync().Result);
            row.PriceFrom = JsonConvert.DeserializeObject<decimal>(PricevMs["data"].ToString());
            if (row.PriceFrom == 0)
            {
                row.PriceTo = (decimal?)SelectedITem.CostPrice;
            }
            StateHasChanged();
        }
        async Task GetItemFromData(ItemConversion_ItemVM row)
        {
            //string uri = "/api/Items/GetNationalCodeByID?Id=" + row.ItemIdTo;
            //HttpResponseMessage response = await Http.GetAsync(uri);
            //var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            //row.ParCodeTo = JsonConvert.DeserializeObject<string>(vMs["data"].ToString());
            //   LookupItem SelectedITem = ItemList.Find(x => x.ID == row.ItemIdFrom);
            LookupItem SelectedITem = ItemList.Find(x => x.Name == row.ItemNameFrom);
            ItemImg = SelectedITem.ItemImg ?? "";
            row.ItemIdFrom = SelectedITem.ID;
            row.ItemNameFrom = SelectedITem.Name;
           // row.PriceFrom = (decimal?)SelectedITem.CostPrice;
            row.ParCodeFrom = SelectedITem.Code;
            row.QuantityFrom = null;
            row.TotalFrom = null;
            var url = "/api/PurchasesReturns/GetLatestPrice?ItemId=" + row.ItemIdFrom;
            HttpResponseMessage PriceReturn = await Http.GetAsync(url);
            var PricevMs = JObject.Parse(PriceReturn.Content.ReadAsStringAsync().Result);
            row.PriceFrom = JsonConvert.DeserializeObject<decimal>(PricevMs["data"].ToString());
            if (row.PriceFrom == 0)
            {
                row.PriceFrom = (decimal?)SelectedITem.CostPrice;
            }
            StateHasChanged();
        }
        async Task GetItemToData(ItemConversion_ItemVM row)
        {
            //string uri = "/api/Items/GetNationalCodeByID?Id=" + row.ItemIdTo;
            //HttpResponseMessage response = await Http.GetAsync(uri);
            //var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            //row.ParCodeTo = JsonConvert.DeserializeObject<string>(vMs["data"].ToString());
            //    LookupItem SelectedITem = ItemList.Find(x => x.ID == row.ItemIdTo); 
            LookupItem SelectedITem = ItemList.Find(x => x.Name == row.ItemNameTo);
            ItemImg = SelectedITem.ItemImg ?? "";
            row.ItemIdTo = SelectedITem.ID;
            row.ItemNameTo = SelectedITem.Name;
            //row.PriceTo = (decimal?)SelectedITem.CostPrice;
            row.ParCodeTo = SelectedITem.Code;
            row.QuantityTo = null;
            row.TotalTo = null;
            var url = "/api/PurchasesReturns/GetLatestPrice?ItemId=" + row.ItemIdTo;
            HttpResponseMessage PriceReturn = await Http.GetAsync(url);
            var PricevMs = JObject.Parse(PriceReturn.Content.ReadAsStringAsync().Result);
            row.PriceFrom = JsonConvert.DeserializeObject<decimal>(PricevMs["data"].ToString());
            if (row.PriceFrom == 0)
            {
                row.PriceTo = (decimal?)SelectedITem.CostPrice;
            }
            StateHasChanged();
        }

        protected async Task AddConversionBill()
        {
            IsDisabled = true;
            BillModel.ConversionItemsFrom = CItemListFrom;
            BillModel.ConversionItemsTo = CItemListTo;
            var AddModel = JsonConvert.SerializeObject(BillModel);

            var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
            var result = await Http.PostAsync("api/ItemConversion/AddItemConversion", content);
            var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
            BillModel.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());

            if (result.IsSuccessStatusCode)
            {
                StateHasChanged();
                ShowConfirm = true;
                AddModel = JsonConvert.SerializeObject(BillModel);
                content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                var Convertresult = await Http.PostAsync("api/ItemConversion/ConversionItemToTemp", content);
            }
        }

        //void Confirm()
        //{
        //    //await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        //    ShowConfirm = false;
        //    IsDisabled = false;
        //}
        protected async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        }
        protected void GetItemFromTotalEdit(ItemConversion_ItemVM row)
        {
            decimal olditemtotal = (decimal)row.TotalFrom;
            row.TotalFrom = row.QuantityFrom * row.PriceFrom;
            BillModel.TotalPriceFrom = BillModel.TotalPriceTo + row.TotalFrom - olditemtotal;

            StateHasChanged();
        }

        protected void GetItemTo_TotalEdit(ItemConversion_ItemVM row)
        {
            decimal olditemtotal = (decimal)row.TotalTo;
            row.TotalTo = row.QuantityTo * row.PriceTo;
            BillModel.TotalPriceTo = BillModel.TotalPriceTo + row.TotalTo - olditemtotal;

            StateHasChanged();
        }

        async Task ReloadItemsListFrom(ItemConversion_ItemVM row)
        {
            row.ItemNameFrom = string.Empty;
            row.ParCodeFrom = string.Empty;
            row.PriceFrom = 0;


            var ItemListReturn = await Http.GetAsync("api/Items/GetItem");
            var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
            ItemList = JsonConvert.DeserializeObject<List<LookupItem>>(ItemvMs["data"].ToString());


        }

        async Task ReloadItemsListTo(ItemConversion_ItemVM row)
        {
            row.ItemNameTo = string.Empty;
            row.ParCodeTo = string.Empty;
            row.PriceTo = 0;


            var ItemListReturn = await Http.GetAsync("api/Items/GetItem");
            var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
            ItemList = JsonConvert.DeserializeObject<List<LookupItem>>(ItemvMs["data"].ToString());


        }
    }
}