﻿ using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using MasProject.Domain.ViewModel.StoreTransaction.Conversion;
using MasProject.Client.Shared;
using Microsoft.JSInterop;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel;
using System.Net.Http;

namespace MasProject.Client.Pages.StoreTransaction.ItemsConversion
{
    public partial class ItemConversion
    {
        private PageTitle PageTitle;
        bool ShowDelete = false;
        bool ShowConfirm = false;
        List<ItemConversion_BillVM> BillList = new List<ItemConversion_BillVM>();
        ItemConversion_BillVM BillModel = new ItemConversion_BillVM();
        List<StoreVM> StoreList = new List<StoreVM>();
        ItemConversion_ItemVM CItemModelFrom = new ItemConversion_ItemVM();
        ItemConversion_ItemVM CItemModelTo = new ItemConversion_ItemVM();

        //List<LookupItem> ItemList = new List<LookupItem>();
        int ConversionBillID;

        protected async Task GetAllByStore()
        {
            if (BillModel.StoreId != 0)
            {
                string uri = "/api/ItemConversion/GetAllByStore?StoreId=" + BillModel.StoreId;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                BillList = JsonConvert.DeserializeObject<List<ItemConversion_BillVM>>(vMs["data"].ToString());
            }
            else
            {
                var ListReturn = await Http.GetAsync("api/AdditionVoucher/GetAdditionVoucher");
                var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
                BillList = JsonConvert.DeserializeObject<List<ItemConversion_BillVM>>(vMs["data"].ToString());
            }
            StateHasChanged();
        }
        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        {
            var PurchaseListReturn = await Http.GetAsync("api/ItemConversion/GetItemConversion");
            var vMMs = JObject.Parse(PurchaseListReturn.Content.ReadAsStringAsync().Result);
            BillsTbl<ItemConversion_BillVM> ReturnModel = JsonConvert.DeserializeObject<BillsTbl<ItemConversion_BillVM>>(vMMs["data"].ToString());
            BillList = ReturnModel.BillsList;
            StoreList = ReturnModel.SearchItems.Store;
            //ItemList = ReturnModel.SearchItems.ItemList;

            //Fill Table
            //var ListReturn = await Http.GetAsync("api/ItemConversion/GetItemConversion");
            //var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            //BillList = JsonConvert.DeserializeObject<List<ItemConversion_BillVM>>(vMs["data"].ToString());
            ////FillDrop
            //var SalesListReturn = await Http.GetAsync("api/ItemConversion/GetListDDL");
            //var SalesvMs = JObject.Parse(SalesListReturn.Content.ReadAsStringAsync().Result);
            //BillModel = JsonConvert.DeserializeObject<ItemConversion_BillVM>(SalesvMs["data"].ToString());
            //StoreList = BillModel.Store;

            //ItemList = BillModel.ItemList;

        }
       
        protected async Task AddNewConversionBill()
        {
            //  await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/ItemConversionAdd", "جارى التحميل", 1200);
            await jsRuntime.InvokeAsync<object>("open", "/ItemConversionAdd", "_blank");
        }

        protected void Confirm()
        {
            ShowConfirm = false;
        }

        protected void DeleteConversionBill(int? ConversionID)
        {
            ConversionBillID = (int)ConversionID;
            ShowDelete = true;
        }
        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                string uri = "/api/ItemConversion/DeleteItemConversion?ConversionRecordId=" + ConversionBillID;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
            ShowDelete = false;
        }
        protected async Task EditOldConversionBill(int ID)//, string Type
        {
            //  await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/ItemConversionEdit?ID=" + ID, "جارى التحميل", 1200);
            // + "&Type=" + Type
            await jsRuntime.InvokeAsync<object>("open", "/ItemConversionEdit?ID=" + ID, "_blank");
        }
        protected async Task ViewOldConversionBill(int ID)//, string Type
        {
            //  await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/ItemConversionEdit?ID=" + ID, "جارى التحميل", 1200);
            //+ "&Type=" + Type
            await jsRuntime.InvokeAsync<object>("open", "/ItemConversionView?ID=" + ID, "_blank");
        }
    }
}