﻿using MasProject.Domain.ViewModel.StoreTransaction.Transfer;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Items;
using MatBlazor;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using MasProject.Domain.ViewModel;
namespace MasProject.Client.Pages.StoreTransaction.TransferOrders
{
    public partial class TransferOrderEdit
    {
        private PageTitle PageTitle;
        TransferOrderVM TransOrder = new TransferOrderVM();
        TransferOrderDetailsVM TransOrderItem = new TransferOrderDetailsVM();
        List<TransferOrderVM> TransferOrderList = new List<TransferOrderVM>();
        List<TransferOrderDetailsVM> TransferOrderDetailsList = new List<TransferOrderDetailsVM>();
        List<TransferRequestAndOrder> TransferList;
        List<BranchVM> BranchList;
        List<LookupItem> ItemList = new List<LookupItem>();
        bool ShowConfirm = false;
        bool FirstAdd = false;
        bool ShowImg = false;
        string ItemImg = "";
        bool IsDisabled = false;
        bool ShowBranchChecker = false;
        protected override async Task OnInitializedAsync()
        {
            var querystring = ExtensionMethods.QueryString(navigationManager);
            var TransOrderIdStr = querystring["TransOrderId"];
            var TransRequestIDStr = querystring["TransRequestID"];
            int TransOrderId = 0;
            if(int.TryParse(TransOrderIdStr,out TransOrderId))
            {
                TransOrderId = int.Parse(TransOrderIdStr);
            }
            int TransRequestID = 0;
            if (int.TryParse(TransRequestIDStr, out TransRequestID))
            {
                TransRequestID = int.Parse(TransRequestIDStr);
            }
            await EditOldTransferOrder(TransOrderId, TransRequestID);
            await BindList();
            var Type = querystring["Type"];
            if (Type == "View")
            {
                //disable controls
                IsDisabled = true;
                PageTitle.Title = "عرض امر التحويل";
            }
            else
            {
                IsDisabled = false;
                PageTitle.Title = "تعديل امر التحويل";
            }
        }
        async Task BindList()
        {

            var ListReturndrp = await Http.GetAsync("api/TransferOrder/GetListsOfDDl");
            var vMMs = JObject.Parse(ListReturndrp.Content.ReadAsStringAsync().Result);
            var tempmodel = JsonConvert.DeserializeObject<TransferOrderVM>(vMMs["data"].ToString());
            BranchList = tempmodel.BranchList;
            ItemList = tempmodel.ItemList;
            TransferList = tempmodel.TransferList;
        }

        protected async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
            IsDisabled = false;

        }
        private void ShowImage(TransferOrderDetailsVM row)
        {
            ItemImg = row.ItemImg;
            ShowImg = true;
        }
        protected async Task EditOldTransferOrder(int TransOrderId, int TransRequestID)
        {
            if(TransOrderId != 0)
            {
                string uri = "/api/TransferOrder/GetSpecificTransferOrder?Id=" + TransOrderId;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                TransOrder = JsonConvert.DeserializeObject<TransferOrderVM>(vMs["data"].ToString());
                TransferOrderDetailsList = new List<TransferOrderDetailsVM>();
                TransferOrderDetailsList = TransOrder.TransferOrderDetailsList;
                TransOrder.Convert = 0;
            }
            else if(TransRequestID != 0)
            {
                string uri = "/api/TransferRequest/GetTransferRequestAsOrder?Id=" + TransRequestID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                TransOrder = JsonConvert.DeserializeObject<TransferOrderVM>(vMs["data"].ToString());
                TransOrder.TransferRequestId = TransRequestID;
                TransferOrderDetailsList = new List<TransferOrderDetailsVM>();
                TransferOrderDetailsList = TransOrder.TransferOrderDetailsList;
                TransOrder.Convert = 0;
            }
           
        }
        protected async Task EditTransferOrder(int? TransOrderId, int? TransRequestID)
        {
            IsDisabled = true;
            HttpResponseMessage result = new HttpResponseMessage();
            if (TransRequestID != null)
            {
                TransOrder.TransferRequestId = TransRequestID;
                var EditModel = JsonConvert.SerializeObject(TransOrder);
                var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                result = await Http.PostAsync("/api/TransferOrder/AddTransferOrderFromRequest", content);
            }
            else
            {
                TransOrder.TransferOrderDetailsList = TransferOrderDetailsList;
                var EditModel = JsonConvert.SerializeObject(TransOrder);
                var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                result = await Http.PostAsync("/api/TransferOrder/EditTransferOrder", content);
            }
            if (result.IsSuccessStatusCode)
            {
                if (TransOrder.Convert > 0)
                {
                    var ConvertModel = JsonConvert.SerializeObject(TransOrder);
                    var ConvertContent = new StringContent(ConvertModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("/api/TransferOrder/ConvertTransferOrder", ConvertContent);
                }
                ShowConfirm = true;
            }
        }
       
        void AddRow(TransferOrderDetailsVM row)
        {
            TransferOrderDetailsList.FindAll(x => x.Index > row.Index).ForEach(x => x.Index = x.Index + 1);
            TransOrderItem = new TransferOrderDetailsVM {
                Index = row.Index + 1,
                ItemId = row.ItemId,
                ItemName = row.ItemName,
                Code = row.Code,
                BranchFromID = row.BranchFromID,
                BranchFromName = row.BranchFromName,
                Quantity = row.Quantity,
                SelectedBranchQuantity = row.SelectedBranchQuantity,
                MainStoreQuantity = row.MainStoreQuantity,
                BranchToQuantity = row.BranchToQuantity,
            };
            TransferOrderDetailsList.Add(TransOrderItem);
           
            StateHasChanged();
        }
        void AddNewRow()
        {
            TransOrderItem = new TransferOrderDetailsVM { Note = "" };
            TransferOrderDetailsList.Add(TransOrderItem);
            StateHasChanged();
        }
        void RemoveRow(TransferOrderDetailsVM row)
        {
            TransferOrderDetailsList.Remove(row);
            StateHasChanged();
        }
        protected async Task GetItemDataByCode(TransferOrderDetailsVM row)
        {
            if (TransOrder.BranchToID != 0 && TransOrder.BranchToID != null)
            {
                HttpResponseMessage result = new HttpResponseMessage();
            LookupItem SelectedITem = ItemList.Find(x => (x.BoxCode == row.Code || x.GuaranteeParCode == row.Code || x.InterationalParCode == row.Code || x.Code == row.Code || x.ParCodeNote == row.Code || x.SupplierParCode == row.Code));
            ItemImg = SelectedITem.ItemImg ?? "";
            row.ItemId = SelectedITem.ID;
            string Storeuri = "/api/Items/GetBranchAndMianStoreQuantity?ItemId=" + row.ItemId + "&BranchId=" + TransOrder.BranchToID;
            HttpResponseMessage Storeresponse = await Http.GetAsync(Storeuri);
            var StorevMs = JObject.Parse(Storeresponse.Content.ReadAsStringAsync().Result);
            var itemModel = JsonConvert.DeserializeObject<LookupItem>(StorevMs["data"].ToString());
            row.BranchToQuantity = itemModel.BranchToQuantity;
            row.MainStoreQuantity = itemModel.MainStoreQuantity;
            StateHasChanged();
            //check for save
            if (TransOrder.BranchToID != 0)
            {
                if (FirstAdd == false)
                {
                    var AddModel = JsonConvert.SerializeObject(TransOrder);
                    var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/TransferOrder/AddTransferOrder", content);
                    var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    TransOrder.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                    FirstAdd = true;
                }
                else
                {
                    TransOrder.TransferOrderDetailsList = TransferOrderDetailsList;
                    var EditModel = JsonConvert.SerializeObject(TransOrder);
                    var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/TransferOrder/EditTransferOrder", content);
                }
            }
            if (result.IsSuccessStatusCode)
            {
                StateHasChanged();
            }
            }
            else
            {
                ShowBranchChecker = true;
                StateHasChanged();
            }
        }
        protected async Task GetItemData(TransferOrderDetailsVM row)
        {
            if (TransOrder.BranchToID != 0 && TransOrder.BranchToID != null)
            {

                HttpResponseMessage result = new HttpResponseMessage();
                LookupItem SelectedITem = ItemList.Find(x => x.ID == row.ItemId);
                ItemImg = SelectedITem.ItemImg ?? "";
                row.Code = SelectedITem.Code;
                string Storeuri = "/api/Items/GetBranchAndMianStoreQuantity?ItemId=" + row.ItemId + "&BranchId=" + TransOrder.BranchToID;
                HttpResponseMessage Storeresponse = await Http.GetAsync(Storeuri);
                var StorevMs = JObject.Parse(Storeresponse.Content.ReadAsStringAsync().Result);
                var itemModel = JsonConvert.DeserializeObject<LookupItem>(StorevMs["data"].ToString());
                row.BranchToQuantity = itemModel.BranchToQuantity;
                row.MainStoreQuantity = itemModel.MainStoreQuantity;
                StateHasChanged();
                StateHasChanged();
                if (TransOrder.BranchToID != 0)
                {
                    if (FirstAdd == false)
                    {
                        var AddModel = JsonConvert.SerializeObject(TransOrder);
                        var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/TransferOrder/AddTransferOrder", content);
                        var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                        TransOrder.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                        FirstAdd = true;
                    }
                    else
                    {
                        TransOrder.TransferOrderDetailsList = TransferOrderDetailsList;
                        var EditModel = JsonConvert.SerializeObject(TransOrder);
                        var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/TransferOrder/EditTransferOrder", content);
                    }
                }
                if (result.IsSuccessStatusCode)
                {
                    StateHasChanged();
                }
            }
            else
            {
                ShowBranchChecker = true;
                StateHasChanged();
            }
        }
        protected async Task GetItemStoreData(TransferOrderDetailsVM row)
        {
            string uri = "/api/Items/GetItemBranch?ItemId=" + row.ItemId + "&BranchId=" + row.BranchFromID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            row.SelectedBranchQuantity = JsonConvert.DeserializeObject<double>(vMs["data"].ToString());
            StateHasChanged();
        }
        //protected async Task GetTransRequestData(TransferOrderVM row)
        //{
        //    TransferOrderDetailsList = new List<TransferOrderDetailsVM>();
        //    string uri = "/api/TransferRequest/GetSpecificTransferRequest?Id=" + row.TransferRequestId;
        //    HttpResponseMessage response = await Http.GetAsync(uri);
        //    var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
        //    TransferRequestVM TempTransObj = JsonConvert.DeserializeObject<TransferRequestVM>(vMs["data"].ToString());
        //    int index = 0;
        //    TempTransObj.TransferRequestItemList.ForEach(async p =>
        //    {
        //        string Storeuri = "/api/Items/GetItemStore?ItemId=" + p.ItemId + "&StoreId=" + TempTransObj.BranchToID;
        //        HttpResponseMessage Storeresponse = await Http.GetAsync(Storeuri);
        //        var StorevMs = JObject.Parse(Storeresponse.Content.ReadAsStringAsync().Result);
        //        Item_StoreVM TempItemObj = JsonConvert.DeserializeObject<Item_StoreVM>(StorevMs["data"].ToString());

        //        string StoreFromuri = "/api/Items/GetItemStore?ItemId=" + p.ItemId + "&StoreId=" + p.BranchFromID;
        //        HttpResponseMessage StoreFromresponse = await Http.GetAsync(Storeuri);
        //        var StoreFromvMs = JObject.Parse(Storeresponse.Content.ReadAsStringAsync().Result);
        //        Item_StoreVM TempStoreObj = JsonConvert.DeserializeObject<Item_StoreVM>(StorevMs["data"].ToString());

        //        TransferOrderDetailsList.Add(new TransferOrderDetailsVM
        //        {
        //            Index=index,
        //            Code = p.Code,
        //            ItemId = p.ItemId,
        //            ItemName=p.ItemName,
        //            BranchFromID = p.BranchFromID,
        //            StoreToQuantity =TempItemObj.Quantity,
        //            SelectedStoreQuantity= TempStoreObj.Quantity,
        //        });
        //        index++;
        //   });
        //}
    }
}