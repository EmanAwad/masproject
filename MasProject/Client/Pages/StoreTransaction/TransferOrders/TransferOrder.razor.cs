﻿using MasProject.Domain.ViewModel.StoreTransaction.Transfer;
using MasProject.Domain.ViewModel.Hierarchy;
using MatBlazor;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using MasProject.Domain.ViewModel;
using Microsoft.JSInterop;
namespace MasProject.Client.Pages.StoreTransaction.TransferOrders
{
    public partial class TransferOrder
    {
        private PageTitle PageTitle;
        TransferOrderVM TransOrder = new TransferOrderVM();
        //List<TransferOrderVM> TransferOrderList = new List<TransferOrderVM>();
        List<TransferRequestAndOrder> TransferList;
        List<BranchVM> BranchList;
        //List<LookupItem> ItemList = new List<LookupItem>();
       

     
        bool ShowDelete = false;
        bool ShowConfirm = false;
        int TransferOrderID;
        int TransRequestId;
        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        {
            //var ListReturn = await Http.GetAsync("api/TransferOrder/GetTransferOrder");
            //var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            //TransferOrderList = JsonConvert.DeserializeObject<List<TransferOrderVM>>(vMs["data"].ToString());
            var ListReturndrp = await Http.GetAsync("api/TransferOrder/GetListsOfDDl");
            var vMMs = JObject.Parse(ListReturndrp.Content.ReadAsStringAsync().Result);
            TransOrder = JsonConvert.DeserializeObject<TransferOrderVM>(vMMs["data"].ToString());
            BranchList = TransOrder.BranchList;
           // ItemList = TransOrder.ItemList;
           TransferList = TransOrder.TransferList;//get requests 
        }
        protected async Task GetAllByBranch()
        {


            var ListReturn = await Http.GetAsync("/api/TransferOrder/GetAllByBranch?BranchId=" + TransOrder.BranchToID);
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            //TransferOrderList = JsonConvert.DeserializeObject<List<TransferOrderVM>>(vMs["data"].ToString());
            TransOrder = JsonConvert.DeserializeObject<TransferOrderVM>(vMs["data"].ToString());
            TransferList = TransOrder.TransferList;
            StateHasChanged();
        }
        protected async Task AddNewTransferOrder()
        {
            await jsRuntime.InvokeAsync<object>("open", "/TransferOrderAdd" , "_blank");
        }

        protected void Confirm()
        {
            ShowConfirm = false;
        }
        protected async Task EditOldTransferOrder(int? TransOrderId, int? TransRequestID,string Type)
        { 
            await jsRuntime.InvokeAsync<object>("open", "/TransferOrderEdit?TransOrderId="+TransOrderId+"&TransRequestID="+TransRequestID +"&Type="+Type, "_blank");
        }
        protected void DeleteTransferOrder(int? TransOrderId, int? TransRequestID)
        {
            TransferOrderID =TransOrderId.HasValue? TransOrderId.Value : 0;
            TransRequestId = TransRequestID.HasValue? TransRequestID.Value : 0;
            ShowDelete = true;
        }
        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                HttpResponseMessage result = new HttpResponseMessage();

                if (TransRequestId != 0)
                {
                    string uri = "api/TransferRequest/DeleteTransferRequest?ID=" + TransRequestId;
                    result = await Http.GetAsync(uri);
                }
                else
                {
                    string uri = "/api/TransferOrder/DeleteTransferOrder?ID=" + TransferOrderID;
                     result = await Http.GetAsync(uri);
                }
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
            ShowDelete = false;
        }
    
    }
}