﻿using MasProject.Domain.ViewModel.StoreTransaction.Transfer;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Items;
using MatBlazor;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using MasProject.Domain.ViewModel;

namespace MasProject.Client.Pages.StoreTransaction.TransferRequests
{
    public partial class TransferRequestEdit
    {
        private PageTitle PageTitle;
        TransferRequestVM TransRequest = new TransferRequestVM();
        TransferRequestItemVM TransRequestItem = new TransferRequestItemVM();
        List<TransferRequestItemVM> TransferRequestItemList = new List<TransferRequestItemVM>();
        List<BranchVM> BranchList;
        List<LookupItem> ItemList = new List<LookupItem>();
        bool ShowConfirm = false;
        bool FirstAdd = false;
        string ItemImg = "";
        bool IsDisabled = false;
        bool ShowImg = false;
        bool ShowBranchChecker = false;

        protected override async Task OnInitializedAsync()
        {
            var querystring = ExtensionMethods.QueryString(navigationManager);
            var id = querystring["ID"];
            await BindList();
            await EditOldTransferRequest(int.Parse(id));
            var Type = querystring["Type"];
            if (Type == "View")
            {
                //disable controls
                IsDisabled = true;
                PageTitle.Title = "عرض طلب التحويل";
            }
            else
            {
                IsDisabled = false;
                PageTitle.Title = "تعديل طلب التحويل";
            }
        }
        async Task BindList()
        {
             var ListReturndrp = await Http.GetAsync("api/TransferRequest/GetListsOfDDl");
            var vMMs = JObject.Parse(ListReturndrp.Content.ReadAsStringAsync().Result);
            var tempmodel = JsonConvert.DeserializeObject<TransferRequestVM>(vMMs["data"].ToString());
            BranchList = tempmodel.BranchList;
            ItemList = tempmodel.ItemList;
        }
        protected async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
            IsDisabled = false;
        }
        protected async Task EditOldTransferRequest(int ID)
        {
            string uri = "/api/TransferRequest/GetSpecificTransferRequest?Id=" + ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            TransRequest = JsonConvert.DeserializeObject<TransferRequestVM>(vMs["data"].ToString());
            TransferRequestItemList = TransRequest.TransferRequestItemList;
        }
        protected async Task EditTransferRequest()
        {
            IsDisabled = true;
            TransRequest.TransferRequestItemList = TransferRequestItemList;
            var EditModel = JsonConvert.SerializeObject(TransRequest);
            var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
            var result = await Http.PostAsync("api/TransferRequest/EditTransferRequest", content);
            if (result.IsSuccessStatusCode)
            {
                if(TransRequest.Convert > 0)
                {
                    var ConvertModel = JsonConvert.SerializeObject(TransRequest);
                    var ConvertContent = new StringContent(ConvertModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/TransferOrder/ConvertTransferOrder", ConvertContent);
                }
                 ShowConfirm = true;
             }

        }
        void AddNewRow()
        {
            TransRequestItem = new TransferRequestItemVM { Note = "" };
            TransferRequestItemList.Add(TransRequestItem);
            StateHasChanged();
        }
        void RemoveRow(TransferRequestItemVM row)
        {
            TransferRequestItemList.Remove(row);
            StateHasChanged();
        }
        private void ShowImage(TransferRequestItemVM row)
        {
            ItemImg = row.ItemImg;
            ShowImg = true;
        }
        protected async Task GetItemDataByCode(TransferRequestItemVM row)
        {
            if (TransRequest.BranchToID != 0 && TransRequest.BranchToID != null)
            {
                HttpResponseMessage result = new HttpResponseMessage();
                LookupItem SelectedITem = ItemList.Find(x => (x.BoxCode == row.Code || x.GuaranteeParCode == row.Code || x.InterationalParCode == row.Code || x.Code == row.Code || x.ParCodeNote == row.Code || x.SupplierParCode == row.Code));
                ItemImg = SelectedITem.ItemImg ?? "";
                row.ItemId = SelectedITem.ID;
                string Storeuri = "/api/Items/GetBranchAndMianStoreQuantity?ItemId=" + row.ItemId + "&BranchId=" + TransRequest.BranchToID;
                HttpResponseMessage Storeresponse = await Http.GetAsync(Storeuri);
                var StorevMs = JObject.Parse(Storeresponse.Content.ReadAsStringAsync().Result);
                var itemModel = JsonConvert.DeserializeObject<LookupItem>(StorevMs["data"].ToString());
                row.BranchToQuantity = itemModel.BranchToQuantity;
                row.MainStoreQuantity = itemModel.MainStoreQuantity;
                StateHasChanged();
                //check for save
                    if (FirstAdd == false)
                    {
                        var AddModel = JsonConvert.SerializeObject(TransRequest);
                        var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/TransferRequest/AddTransferRequest", content);
                        var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                        TransRequest.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                        FirstAdd = true;
                    }
                    else
                    {
                        TransRequest.TransferRequestItemList = TransferRequestItemList;
                        var EditModel = JsonConvert.SerializeObject(TransRequest);
                        var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/TransferRequest/EditTransferRequest", content);
                    }
                if (result.IsSuccessStatusCode)
                {
                    StateHasChanged();
                }
            }
            else
            {
                ShowBranchChecker = true;
                StateHasChanged();
            }
        }
        protected async Task GetItemData(TransferRequestItemVM row)
        {
            if (TransRequest.BranchToID != 0 && TransRequest.BranchToID != null)
            {
            HttpResponseMessage result = new HttpResponseMessage();
            LookupItem SelectedITem = ItemList.Find(x => x.ID == row.ItemId);
            ItemImg = SelectedITem.ItemImg ?? "";
            row.Code = SelectedITem.Code;
            string Storeuri = "/api/Items/GetBranchAndMianStoreQuantity?ItemId=" + row.ItemId + "&BranchId=" + TransRequest.BranchToID;
            HttpResponseMessage Storeresponse = await Http.GetAsync(Storeuri);
            var StorevMs = JObject.Parse(Storeresponse.Content.ReadAsStringAsync().Result);
            var itemModel = JsonConvert.DeserializeObject<LookupItem>(StorevMs["data"].ToString());
            row.BranchToQuantity = itemModel.BranchToQuantity;
            row.MainStoreQuantity = itemModel.MainStoreQuantity;
            StateHasChanged();
            //check for save
            if (TransRequest.BranchToID != 0)
            {
                if (FirstAdd == false)
                {
                    var AddModel = JsonConvert.SerializeObject(TransRequest);
                    var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/TransferRequest/AddTransferRequest", content);
                    var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    TransRequest.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                    FirstAdd = true;
                }
                else
                {
                    TransRequest.TransferRequestItemList = TransferRequestItemList;
                    var EditModel = JsonConvert.SerializeObject(TransRequest);
                    var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/TransferRequest/EditTransferRequest", content);
                }
            }
            if (result.IsSuccessStatusCode)
            {
               StateHasChanged();
            }
            }
            else
            {
                ShowBranchChecker = true;
                StateHasChanged();
            }
        }
        protected async Task GetItemStoreData(TransferRequestItemVM row)
        {
            string uri = "/api/Items/GetItemBranch?ItemId=" + row.ItemId + "&BranchId=" + row.BranchFromID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            row.SelectedBranchQuantity  = JsonConvert.DeserializeObject<double>(vMs["data"].ToString());
            StateHasChanged();
        }
    }
}