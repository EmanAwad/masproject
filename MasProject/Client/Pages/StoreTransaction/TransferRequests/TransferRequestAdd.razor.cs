﻿using MasProject.Domain.ViewModel.StoreTransaction.Transfer;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Items;
using MatBlazor;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using MasProject.Domain.ViewModel;
namespace MasProject.Client.Pages.StoreTransaction.TransferRequests
{
    public partial class TransferRequestAdd
    {
        private PageTitle PageTitle;
        TransferRequestVM TransRequest = new TransferRequestVM();
        TransferRequestItemVM TransRequestItem = new TransferRequestItemVM();
        List<TransferRequestItemVM> TransferRequestItemList = new List<TransferRequestItemVM>();
        List<BranchVM> BranchList;
        List<LookupItem> ItemList = new List<LookupItem>();
        List<Item_StoreVM> ItemStoreList = new List<Item_StoreVM>();
        bool ShowConfirm = false;
        bool FirstAdd = false;
        bool showAddrow = false;
        string ItemImg = "";
        bool ShowImg = false;
        bool IsDisabled = false;
        bool ShowBranchChecker = false;
        protected override async Task OnInitializedAsync()
        {
            FirstAdd = false;
            TransRequest = new TransferRequestVM();
            //edit 21-09-2020 to save bill first then each item
            TransferRequestItemList = new List<TransferRequestItemVM>();
            TransRequestItem = new TransferRequestItemVM { Note = "" };
            TransferRequestItemList.Add(TransRequestItem);
             await BindList();
        }
        async Task BindList()
        {
            var ListReturndrp = await Http.GetAsync("api/TransferRequest/GetListsOfDDl");
            var vMMs = JObject.Parse(ListReturndrp.Content.ReadAsStringAsync().Result);
            TransRequest = JsonConvert.DeserializeObject<TransferRequestVM>(vMMs["data"].ToString());
            BranchList = TransRequest.BranchList;
            ItemList = TransRequest.ItemList;
        }
        protected async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
            IsDisabled = false;
        }
        protected async Task EditTransferRequest()
        {
            IsDisabled = true;
            TransRequest.TransferRequestItemList = TransferRequestItemList;
            var EditModel = JsonConvert.SerializeObject(TransRequest);
            var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
            var result = await Http.PostAsync("api/TransferRequest/EditTransferRequest", content);
            if (result.IsSuccessStatusCode)
            {
                if(TransRequest.Convert > 0)
                {
                    var ConvertModel = JsonConvert.SerializeObject(TransRequest);
                    var ConvertContent = new StringContent(ConvertModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/TransferOrder/ConvertTransferRequest", ConvertContent);
                }
                 ShowConfirm = true;
             }

        }
        void AddNewRow()
        {
            TransRequestItem = new TransferRequestItemVM { Note = "" };
            TransferRequestItemList.Add(TransRequestItem);
            StateHasChanged();
        }
        void RemoveRow(TransferRequestItemVM row)
        {
            TransferRequestItemList.Remove(row);
            StateHasChanged();
        }
       
        protected async Task GetItemDataByCode(TransferRequestItemVM row)
        {
            if (TransRequest.BranchToID != 0 && TransRequest.BranchToID != null)
            {
                HttpResponseMessage result = new HttpResponseMessage();
                LookupItem SelectedITem = ItemList.Find(x => (x.BoxCode == row.Code || x.GuaranteeParCode == row.Code || x.InterationalParCode == row.Code || x.Code == row.Code || x.ParCodeNote == row.Code || x.SupplierParCode == row.Code));
                ItemImg = SelectedITem.ItemImg ?? "";
                row.ItemId = SelectedITem.ID;
                string Storeuri = "/api/Items/GetBranchAndMianStoreQuantity?ItemId=" + row.ItemId + "&BranchId=" + TransRequest.BranchToID;
                HttpResponseMessage Storeresponse = await Http.GetAsync(Storeuri);
                var StorevMs = JObject.Parse(Storeresponse.Content.ReadAsStringAsync().Result);
                var itemModel = JsonConvert.DeserializeObject<LookupItem>(StorevMs["data"].ToString());
                row.BranchToQuantity = itemModel.BranchToQuantity;
                row.MainStoreQuantity = itemModel.MainStoreQuantity;
                StateHasChanged();
                //check for save
                if (TransRequest.BranchToID != 0)
                {
                    if (FirstAdd == false)
                    {
                        var AddModel = JsonConvert.SerializeObject(TransRequest);
                        var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/TransferRequest/AddTransferRequest", content);
                        var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                        TransRequest.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                        FirstAdd = true;
                    }
                    else
                    {
                        TransRequest.TransferRequestItemList = TransferRequestItemList;
                        var EditModel = JsonConvert.SerializeObject(TransRequest);
                        var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/TransferRequest/EditTransferRequest", content);
                    }
                }
                if (result.IsSuccessStatusCode)
                {
                    showAddrow = false;
                    StateHasChanged();
                }
            }
            else
            {
                ShowBranchChecker = true;
                StateHasChanged();
            }
        }
        protected async Task GetItemData(TransferRequestItemVM row)
        {
            if (TransRequest.BranchToID != 0 && TransRequest.BranchToID != null )
            {
                HttpResponseMessage result = new HttpResponseMessage();
            LookupItem SelectedITem = ItemList.Find(x => x.ID == row.ItemId);
            ItemImg = SelectedITem.ItemImg ?? "";
            row.Code = SelectedITem.Code;

            string Storeuri = "/api/Items/GetBranchAndMianStoreQuantity?ItemId=" + row.ItemId + "&BranchId=" + TransRequest.BranchToID;
            HttpResponseMessage Storeresponse = await Http.GetAsync(Storeuri);
            var StorevMs = JObject.Parse(Storeresponse.Content.ReadAsStringAsync().Result);
            var itemModel = JsonConvert.DeserializeObject<LookupItem>(StorevMs["data"].ToString());
            row.BranchToQuantity = itemModel.BranchToQuantity;
            row.MainStoreQuantity = itemModel.MainStoreQuantity;
            StateHasChanged();
            //check for save
            if (TransRequest.BranchToID != 0)
            {
                if (FirstAdd == false)
                {
                    var AddModel = JsonConvert.SerializeObject(TransRequest);
                    var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/TransferRequest/AddTransferRequest", content);
                    var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    TransRequest.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                    FirstAdd = true;
                }
                else
                {
                    TransRequest.TransferRequestItemList = TransferRequestItemList;
                    var EditModel = JsonConvert.SerializeObject(TransRequest);
                    var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/TransferRequest/EditTransferRequest", content);
                }
            }
            if (result.IsSuccessStatusCode)
            { 
                showAddrow=false;
                StateHasChanged();
            }
            }
            else
            {
                ShowBranchChecker = true;
                StateHasChanged();
            }
        }
        protected async Task GetItemStoreData(TransferRequestItemVM row)
        {
            string uri = "/api/Items/GetItemBranch?ItemId=" + row.ItemId + "&BranchId=" + row.BranchFromID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            row.SelectedBranchQuantity  = JsonConvert.DeserializeObject<double>(vMs["data"].ToString());
            StateHasChanged();
        }
    }

}