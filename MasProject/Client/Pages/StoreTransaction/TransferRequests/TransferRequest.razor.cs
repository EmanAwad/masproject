﻿using MasProject.Domain.ViewModel.StoreTransaction.Transfer;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Items;
using MatBlazor;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using Microsoft.JSInterop;
namespace MasProject.Client.Pages.StoreTransaction.TransferRequests
{
    public partial class TransferRequest
    {
        private PageTitle PageTitle;
        List<TransferRequestVM> TransferRequestList = new List<TransferRequestVM>();
        bool ShowDelete = false;
        bool ShowConfirm = false;
        int TransferRequestID;
        TransferRequestVM TransRequest = new TransferRequestVM();
      
        List<TransferRequestAndOrder> TransferList;
        List<BranchVM> BranchList;
        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        {
            var ListReturn = await Http.GetAsync("api/TransferRequest/GetTransferRequest");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            TransferRequestList = JsonConvert.DeserializeObject<List<TransferRequestVM>>(vMs["data"].ToString());
            //FillDrop
            var ListReturndrp = await Http.GetAsync("api/TransferRequest/GetListsOfDDl");
            var vMMs = JObject.Parse(ListReturndrp.Content.ReadAsStringAsync().Result);
            TransRequest = JsonConvert.DeserializeObject<TransferRequestVM>(vMMs["data"].ToString());
            BranchList = TransRequest.BranchList;
            

        }
        protected async Task GetAllByBranch()
        {


            var ListReturn = await Http.GetAsync("/api/TransferRequest/GetAllByBranch?BranchId=" + TransRequest.BranchToID);
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
          
            TransferRequestList = JsonConvert.DeserializeObject<List<TransferRequestVM>>(vMs["data"].ToString());

            StateHasChanged();
        }
        protected async Task AddNewTransferRequest()
        { await jsRuntime.InvokeAsync<object>("open", "/TransferRequestAdd", "_blank");

        }

        protected void Confirm()
        {
            ShowConfirm = false;
        }
        protected async Task EditOldTransferRequest(int ID, string Type)
        {
            await jsRuntime.InvokeAsync<object>("open", "/TransferRequestEdit?ID="+ID +"&Type="+Type, "_blank");

        }


        protected void DeleteTransferRequest(int Id)
        {
            TransferRequestID = Id;
            ShowDelete = true;
        }
        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                string uri = "/api/TransferRequest/DeleteTransferRequest?ID=" + TransferRequestID;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
            ShowDelete = false;
        }
       
    }
}