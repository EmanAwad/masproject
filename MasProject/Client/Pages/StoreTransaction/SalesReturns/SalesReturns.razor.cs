﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using MasProject.Client.Shared;
using Microsoft.JSInterop;
using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Hierarchy;
using System;
using System.Net.Http;

namespace MasProject.Client.Pages.StoreTransaction.SalesReturns
{
    public partial class SalesReturns
    {
        private PageTitle PageTitle;
        bool ShowDelete = false;
        bool ShowConfirm = false;
        List<SalesReturnsVM> ReturnsList = new List<SalesReturnsVM>();
        int ReturnBillID;
        int ClientId;
        int SerialNumber;
        bool FirstAdd = false;
        ClientsVM clients = new ClientsVM();
        List<LookupKeyValueVM> ClientList = new List<LookupKeyValueVM>();
        List<BranchVM> BranchList = new List<BranchVM>();

        SalesReturnsVM SalesReturnModel = new SalesReturnsVM();

        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        {
            //Fill Table
            var ListReturn = await Http.GetAsync("api/SalesReturns/GetSalesReturns");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ReturnsList = JsonConvert.DeserializeObject<List<SalesReturnsVM>>(vMs["data"].ToString());
            //Fill Dropdowns
            var SalesListReturn = await Http.GetAsync("api/SalesReturns/GetListDDL");
            var SalesvMs = JObject.Parse(SalesListReturn.Content.ReadAsStringAsync().Result);
            SalesReturnModel = JsonConvert.DeserializeObject<SalesReturnsVM>(SalesvMs["data"].ToString());
            ClientList = SalesReturnModel.ClientList;
            BranchList = SalesReturnModel.BranchList;
        }

        protected async Task AddNewReturnBill()
        {
            await jsRuntime.InvokeAsync<object>("open", "/SalesReturnsAdd", "_blank");

        }

        protected void Confirm()
        {
            ShowConfirm = false;
        }
        protected async Task EditOldReturnBill(int ID, string Type)
        {
            await jsRuntime.InvokeAsync<object>("open", "/SalesReturnsEdit?ID=" + ID + "&Type=" + Type, "_blank");


        }

        protected void DeleteReturnBill(int Id, int serialNumber, int Clientid)
        {
            ReturnBillID = Id;
            ClientId = Clientid;
            SerialNumber = serialNumber;
            ShowDelete = true;
        }
        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                string uri = "/api/SalesReturns/DeleteSalesReturns?ID=" + ReturnBillID + "&SerialNumber=" + SerialNumber + "&ClientId=" + ClientId;

                var result = await Http.GetAsync(uri);

                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
            ShowDelete = false;

        }

        async Task ReloadClientsList()
        {
            clients.Name = String.Empty;

            var ListReturn = await Http.GetAsync("api/Client/GetClients");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ClientList = JsonConvert.DeserializeObject<List<LookupKeyValueVM>>(vMs["data"].ToString());

        }

        protected async Task GetAllByClient()
        {
            SalesReturnModel.ClientId = ClientList.Find(x => x.Name == clients.Name).ID;
            string uri = "/api/SalesReturns/GetSalesByClient?ClientId=" + SalesReturnModel.ClientId;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            ReturnsList = JsonConvert.DeserializeObject<List<SalesReturnsVM>>(vMs["data"].ToString());
            StateHasChanged();
            //SalesModel.ClientId = ClientList.Find(x => x.Name == clients.Name).ID;
            //if (SalesModel.ClientId != 0)
            //{
            //    SalesList = SalesList.Where(x => x.ClientId == SalesModel.ClientId).ToList();
            //}
            //else
            //{
            //    SalesList = TempSalesList;
            //}
        }
        //protected async Task GetAllByBranch()
        //{
        //    //if (SalesModel.BranchId != 0)
        //    //{
        //    //    SalesList = SalesList.Where(x => x.BranchId == SalesModel.BranchId).ToList();
        //    //}
        //    //else
        //    //{
        //    //    SalesList = TempSalesList;
        //    //}
        //    //SalesList = SalesList.Where(x => x.BranchId == SalesModel.BranchId).ToList();
        //    string SalesBranch = "/api/SalesBills/GetSalesByBranch?BranchId=" + SalesModel.BranchId;
        //    HttpResponseMessage SalesBranchReturn = await Http.GetAsync(SalesBranch);
        //    var SalesBranchVMs = JObject.Parse(SalesBranchReturn.Content.ReadAsStringAsync().Result);
        //    SalesList = JsonConvert.DeserializeObject<List<SalesBillsVM>>(SalesBranchVMs["data"].ToString());
        //}

        protected async Task GetAllByBranch()
        {


            string uri = "/api/SalesReturns/GetSalesByBranch?BranchId=" + SalesReturnModel.BranchId;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            ReturnsList = JsonConvert.DeserializeObject<List<SalesReturnsVM>>(vMs["data"].ToString());


            StateHasChanged();
        }
    }
}