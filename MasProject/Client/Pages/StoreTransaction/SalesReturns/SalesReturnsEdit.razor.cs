﻿using MasProject.Domain.ViewModel.Hierarchy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel.Items;
using MasProject.Client.Shared;
using System;
using static MasProject.Domain.Enums.Enums;

namespace MasProject.Client.Pages.StoreTransaction.SalesReturns
{
    public partial class SalesReturnsEdit
    {
        private PageTitle PageTitle;
        bool ShowConfirm = false;
        SalesReturnsItemsVM sItemModel = new SalesReturnsItemsVM();
        List<SalesReturnsItemsVM> sItemList = new List<SalesReturnsItemsVM>();
        SalesReturnsVM ReturnsModel = new SalesReturnsVM();
        List<SalesReturnsVM> ReturnsList = new List<SalesReturnsVM>();
        List<StoreVM> StoreList = new List<StoreVM>();
        List<LookupKeyValueVM> ClientList = new List<LookupKeyValueVM>();
        List<LookupItem> ItemList = new List<LookupItem>();
        List<SellingExpensesVM> ExpensesList = new List<SellingExpensesVM>();
        SalesReturnsExpensesVM sExpenseModel = new SalesReturnsExpensesVM();
        List<SalesReturnsExpensesVM> ExpenseList = new List<SalesReturnsExpensesVM>();
        List<BranchVM> BranchList = new List<BranchVM>();
        ClientsVM clients = new ClientsVM();

        bool FirstAdd = false;
        int FlagTypeID = 0;
        bool ShowTaxPart = false;
        string TotalInWords = "";
        string TotalInWordsTax = "";
        string ItemImg = "";
        bool IsDisabled = false;
        bool ShowImg = false;
        bool SelectClientChecker = false;
        protected override async Task OnInitializedAsync()
        {
            await BindList();
            var querystring = ExtensionMethods.QueryString(navigationManager);
            var id = querystring["ID"];
            await EditOldReturnBill(int.Parse(id));
            var Type = querystring["Type"];
            if (Type == "View")
            {
                //disable controls
                IsDisabled = true;
                PageTitle.Title = "عرض مردودات مبيعات";
            }
            else
            {
                IsDisabled = false;
                PageTitle.Title = "تعديل مردودات مبيعات";
            }
        }
        async Task BindList()
        {
            var SalesListReturn = await Http.GetAsync("api/SalesReturns/GetListDDL");
            var SalesvMs = JObject.Parse(SalesListReturn.Content.ReadAsStringAsync().Result);
            ReturnsModel = JsonConvert.DeserializeObject<SalesReturnsVM>(SalesvMs["data"].ToString());
            StoreList = ReturnsModel.StoreList;
            //DealTypeList = ReturnsModel.DealTypeList;
            ClientList = ReturnsModel.ClientList;
            ItemList = ReturnsModel.ItemList;
            ExpensesList = ReturnsModel.sellingExpensesList;
            BranchList = ReturnsModel.BranchList;
           
        }


        async Task BalancesRenew()
        {
            string priceuri = "/api/ClientsBalance/GetBalance?ClientId=" + ReturnsModel.ClientId;
            HttpResponseMessage ClientListReturn = await Http.GetAsync(priceuri);
            var ClientvMs = JObject.Parse(ClientListReturn.Content.ReadAsStringAsync().Result);
            ReturnsModel.ClientBalance = JsonConvert.DeserializeObject<decimal>(ClientvMs["data"].ToString());
        }
        async Task ClientsRenew()
        {
            var ClientListReturn = await Http.GetAsync("api/Client/GetClient");
            var ClientvMs = JObject.Parse(ClientListReturn.Content.ReadAsStringAsync().Result);
            ClientList = JsonConvert.DeserializeObject<List<LookupKeyValueVM>>(ClientvMs["data"].ToString());

        }
        async Task ItemsRenew(SalesReturnsItemsVM row)
        {
            var ItemListReturn = await Http.GetAsync("api/Items/GetItems");
            var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
            ItemList = JsonConvert.DeserializeObject<List<LookupItem>>(ItemvMs["data"].ToString());
            RecalculateTotal();
            StateHasChanged();
        }

        async Task ExpensesRenew()
        {
            var ExpensesListReturn = await Http.GetAsync("api/SellingExpenses/GetSellingExpenses");
            var ExpensesvMs = JObject.Parse(ExpensesListReturn.Content.ReadAsStringAsync().Result);
            ExpensesList = JsonConvert.DeserializeObject<List<SellingExpensesVM>>(ExpensesvMs["data"].ToString());
        }
        protected async Task AddReturnBill()
        {
            ReturnsModel.ClientId = ClientList.Find(x => x.Name == clients.Name).ID;
            HttpResponseMessage result = new HttpResponseMessage();
            //check for save
            if (ReturnsModel.SerialNumber != 0 & ReturnsModel.SerialNumber != null)
            {
                if (ReturnsModel.ClientId != 0)
                {
                    if (FirstAdd == false)
                    {
                        var AddModel = JsonConvert.SerializeObject(ReturnsModel);
                        var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/SalesReturns/AddSalesReturns", content);
                        var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                        ReturnsModel.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                        FirstAdd = true;
                    }
                    else
                    {
                        ReturnsModel.SalesReturnsItems = sItemList;
                        // ReturnsModel.SalesReturnsBillStores = BillStoresList;
                        var EditModel = JsonConvert.SerializeObject(ReturnsModel);
                        var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/SalesReturns/EditSalesReturns", content);
                    }
                    if (result.IsSuccessStatusCode)
                    {
                        StateHasChanged();
                    }
                }
                else
                {
                    SelectClientChecker = true;
                }
            }
        }

        //protected async Task Confirm()
        //{
        //    ShowConfirm = false;
        //    IsDisabled = false;
        //}
        protected async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        }
        private void ShowImage(SalesReturnsItemsVM row)
        {
            ItemImg = row.ItemImg;
            ShowImg = true;
        }
        protected async Task EditOldReturnBill(int ID)
        {
           
            FirstAdd = true;
            string uri = "/api/SalesReturns/GetSpecificSalesReturns?Id=" + ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            ReturnsModel = JsonConvert.DeserializeObject<SalesReturnsVM>(vMs["data"].ToString());
            ReturnsModel.convert = 0;
            sItemList = new List<SalesReturnsItemsVM>();
            sItemList = ReturnsModel.SalesReturnsItems;
            clients.Name = ReturnsModel.ClientName;
            // BillStoresList=ReturnsModel.SalesReturnsBillStores;
            ExpenseList = new List<SalesReturnsExpensesVM>();
            ExpenseList = ReturnsModel.SalesReturnsExpenses;
            if (ReturnsModel.FlagType)
            {
                FlagTypeID = 1;
                await TaxPart();
            }
            RecalculateTotal();
            StateHasChanged();
        }
        protected async Task EditReturnBill()
        {
            IsDisabled = true;
            ReturnsModel.ClientId = ClientList.Find(x => x.Name == clients.Name).ID;
            HttpResponseMessage result = new HttpResponseMessage();
            //check for save
            if (ReturnsModel.SerialNumber != 0 & ReturnsModel.SerialNumber != null)
            {
                if (ReturnsModel.ClientId != 0)
                {
                    if (FirstAdd == false)
                    {
                        var AddModel = JsonConvert.SerializeObject(ReturnsModel);
                        var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/SalesReturns/AddSalesReturns", content);
                        var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                        ReturnsModel.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                        FirstAdd = true;
                    }
                    else
                    {
                        ReturnsModel.SalesReturnsItems = sItemList;
                        // ReturnsModel.SalesReturnsBillStores = BillStoresList;
                        var EditModel = JsonConvert.SerializeObject(ReturnsModel);
                        var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/SalesReturns/EditSalesReturns", content);
                    }
                    if (result.IsSuccessStatusCode)
                    {
                        if (ReturnsModel.convert == 1 || ReturnsModel.IsConverted == true)
                        {
                            var EditModel = JsonConvert.SerializeObject(ReturnsModel);
                            var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                            var Convertresult = await Http.PostAsync("api/SalesReturns/ConvertSalesReturns", content);

                        }
                        ShowConfirm = true;
                    }
                }
                else
                {
                    SelectClientChecker = true;
                }
            }
        }
        protected async Task GetDealType()
        {
            ReturnsModel.ClientId = ClientList.Find(x => x.Name == clients.Name).ID;
            string priceuri = "/api/ClientsBalance/GetBalance?ClientId=" + ReturnsModel.ClientId;
            HttpResponseMessage ClientListReturn = await Http.GetAsync(priceuri);
            var ClientvMs = JObject.Parse(ClientListReturn.Content.ReadAsStringAsync().Result);
            ReturnsModel.ClientBalance = JsonConvert.DeserializeObject<decimal>(ClientvMs["data"].ToString());

            string Taxuri = "/api/Client/GetTaxFile?Id=" + ReturnsModel.ClientId;
            HttpResponseMessage TaxReturn = await Http.GetAsync(Taxuri);
            var TaxvMs = JObject.Parse(TaxReturn.Content.ReadAsStringAsync().Result);
            ClientsVM TempFile = JsonConvert.DeserializeObject<ClientsVM>(TaxvMs["data"].ToString());
            ReturnsModel.TaxFileNumber = TempFile.Taxfile;

            StateHasChanged();
        }
        void AddNewRow()
        {
            int IdentifierCount = sItemList.Count;
            sItemModel = new SalesReturnsItemsVM { Identifer = IdentifierCount + 1, };
            sItemList.Add(sItemModel);

            StateHasChanged();
        }
        void RemoveRow(SalesReturnsItemsVM row)
        {
            sItemList.Remove(row);//, int? BillStoresIdentifer
            //var tempList = BillStoresList.Find(x => x.Identifer == BillStoresIdentifer).SalesReturnsItems;
            //tempList.Remove(row);
            StateHasChanged();
        }
        void AddNewExpenseRow()
        {
            int IdentifierCount = ExpenseList.Count;
            sExpenseModel = new SalesReturnsExpensesVM { Identifer = IdentifierCount + 1, };
            ExpenseList.Add(sExpenseModel);
            StateHasChanged();
        }
        void RemoveExpenseRow(SalesReturnsExpensesVM row)
        {
            ExpenseList.Remove(row);
            StateHasChanged();
        }
        void ChangeItemQuantity()
        {
            RecalculateTotal();
            AddNewRow();//BillStoresIdentifer//int BillStoresIdentifer
        }
        protected async Task GetItemDataByCode(SalesReturnsItemsVM row)
        {
            LookupItem SelectedITem = ItemList.Find(x => (x.BoxCode == row.Box_ParCode || x.GuaranteeParCode == row.Box_ParCode || x.InterationalParCode == row.Box_ParCode || x.Code == row.Box_ParCode || x.ParCodeNote == row.Box_ParCode || x.SupplierParCode == row.Box_ParCode));
            ItemImg = SelectedITem.ItemImg ?? "";
            row.ItemId = SelectedITem.ID;
            row.ItemName = SelectedITem.Name;
            var url = "/api/SalesReturns/GetLatestPrice?ItemId=" + row.ItemId;
            HttpResponseMessage PriceReturn = await Http.GetAsync(url);
            var PricevMs = JObject.Parse(PriceReturn.Content.ReadAsStringAsync().Result);
            row.Price = JsonConvert.DeserializeObject<decimal>(PricevMs["data"].ToString());
           
            row.Quantity = null;
            row.Total = null;
            HttpResponseMessage result = new HttpResponseMessage();
            //check for save
            if (ReturnsModel.SerialNumber != 0)
            {
              
                if (ReturnsModel.ClientId != 0)
                {
                    if (FirstAdd == false)
                    {
                        var AddModel = JsonConvert.SerializeObject(ReturnsModel);
                        var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/SalesReturns/AddSalesReturns", content);
                        var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                        ReturnsModel.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                        FirstAdd = true;
                    }
                    else
                    {
                        ReturnsModel.SalesReturnsItems = sItemList;
                        // ReturnsModel.SalesReturnsBillStores = BillStoresList;
                        ReturnsModel.SalesReturnsExpenses = ExpenseList;
                        var EditModel = JsonConvert.SerializeObject(ReturnsModel);
                        var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/SalesReturns/EditSalesReturns", content);
                    }
                    if (result.IsSuccessStatusCode)
                    {

                        StateHasChanged();
                    }
                }
                else
                {
                    SelectClientChecker = true;
                }
            }
        }

        protected async Task GetItemData(SalesReturnsItemsVM row)
        {
            //  LookupItem SelectedITem = ItemList.Find(x => x.ID == row.ItemId);
            LookupItem SelectedITem = ItemList.Find(x => x.Name == row.ItemName);
            ItemImg = SelectedITem.ItemImg;
            row.Box_ParCode = SelectedITem.Code;
            row.ItemId = SelectedITem.ID;
            var url = "/api/SalesReturns/GetLatestPrice?ItemId=" + row.ItemId;
            HttpResponseMessage PriceReturn = await Http.GetAsync(url);
            var PricevMs = JObject.Parse(PriceReturn.Content.ReadAsStringAsync().Result);
            row.Price = JsonConvert.DeserializeObject<decimal>(PricevMs["data"].ToString());

            row.Quantity = null;
            row.Total = null;
            HttpResponseMessage result = new HttpResponseMessage();
            //check for save
            if (ReturnsModel.SerialNumber != 0 & ReturnsModel.SerialNumber != null)
            {
                if (ReturnsModel.ClientId != 0)
                {
                    if (FirstAdd == false)
                    {
                        var AddModel = JsonConvert.SerializeObject(ReturnsModel);
                        var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/SalesReturns/AddSalesReturns", content);
                        var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                        ReturnsModel.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                        FirstAdd = true;
                    }
                    else
                    {
                        ReturnsModel.SalesReturnsItems = sItemList;
                        // ReturnsModel.SalesReturnsBillStores = BillStoresList;
                        ReturnsModel.SalesReturnsExpenses = ExpenseList;
                        var EditModel = JsonConvert.SerializeObject(ReturnsModel);
                        var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/SalesReturns/EditSalesReturns", content);
                    }
                    if (result.IsSuccessStatusCode)
                    {
                        
                        StateHasChanged();
                    }
                }
                else
                {
                    SelectClientChecker = true;
                }
            }
        }
       
        protected async Task TaxPart()
        {
            if (FlagTypeID == 1)
            {
                ReturnsModel.FlagType = true;
                ShowTaxPart = true;
                //get 14% from otherconstant where id=1
                //calculate addtions amount
                if (ReturnsModel.AddtionTax == 0)
                {
                    string uri = "/api/OtherConstant/GetOtherConstantRatio?Id=1";
                    HttpResponseMessage response = await Http.GetAsync(uri);
                    var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                    ReturnsModel.AddtionTax = JsonConvert.DeserializeObject<decimal>(vMs["data"].ToString());
                }
                RecalculateTotal();
            }
            else
            {
                ReturnsModel.FlagType = false;
                ShowTaxPart = false;
            }
        }
        void RecalculateTotal()
        {
            decimal CalculatedTotal = 0;
            foreach (var rowitem in sItemList)
            {
                if (rowitem.Quantity != null && rowitem.Price != null)
                {
                    rowitem.Total = rowitem.Quantity.Value * rowitem.Price.Value;
                    CalculatedTotal = (decimal)(CalculatedTotal + rowitem.Total);
                }
            }
            foreach (var rowExpense in ExpenseList)
            {
                if (rowExpense.Amount != null)
                {
                    CalculatedTotal = (decimal)(CalculatedTotal + rowExpense.Amount.Value);
                }
            }
            ReturnsModel.TotalPrice = CalculatedTotal;
            ReturnsModel.TotalAfterDiscount = ReturnsModel.TotalPrice;

            if (ReturnsModel.DiscountValue != null & ReturnsModel.DiscountValue != 0 & ReturnsModel.DiscountPrecentage == 0)
            {
                decimal temp = ReturnsModel.DiscountValue.Value / ReturnsModel.TotalPrice.Value;
                ReturnsModel.DiscountPrecentage = temp * 100;
                ReturnsModel.TotalAfterDiscount = (ReturnsModel.TotalPrice == null ? 0 : ReturnsModel.TotalPrice) - ReturnsModel.DiscountValue;
            }
            else if (ReturnsModel.DiscountPrecentage != null & ReturnsModel.DiscountPrecentage != 0 & ReturnsModel.DiscountValue == 0)
            {
                decimal temp = (ReturnsModel.TotalPrice.Value * ReturnsModel.DiscountPrecentage.Value) / 100;
                ReturnsModel.DiscountValue = temp;
                ReturnsModel.TotalAfterDiscount = (ReturnsModel.TotalPrice == null ? 0 : ReturnsModel.TotalPrice) - ReturnsModel.DiscountValue;
            }
            else
            {
                ReturnsModel.TotalAfterDiscount = (ReturnsModel.TotalPrice == null ? 0 : ReturnsModel.TotalPrice) - ReturnsModel.DiscountValue;
            }
            if (ReturnsModel.TotalAfterDiscount == null)
            {
                ReturnsModel.TotalAfterDiscount = ReturnsModel.TotalPrice;
            }
            ReturnsModel.TotalAfterDiscount = Math.Round((decimal)ReturnsModel.TotalAfterDiscount, 0);
            try
            {
                NumberToAlphabetic Obj = new NumberToAlphabetic();
                Obj.ConvertNumbersToArabicAlphabet(ReturnsModel.TotalAfterDiscount.ToString());
                TotalInWords = Obj.GetNumberAr();
            }
            catch (Exception)
            {

            }
            decimal DeductionAmount = 0;
            decimal AddtionalAmount = 0;
            if (ReturnsModel.SourceDeduction != 0)
            {
                ReturnsModel.SourceDeductionAmount = (decimal)(((ReturnsModel.TotalAfterDiscount == null ? 0 : ReturnsModel.TotalAfterDiscount) * ReturnsModel.SourceDeduction) / 100);
                DeductionAmount = ReturnsModel.SourceDeductionAmount;
            }
            if (ReturnsModel.AddtionTax != 0)
            {
                ReturnsModel.AddtionTaxAmount = (decimal)(((ReturnsModel.TotalAfterDiscount == null ? 0 : ReturnsModel.TotalAfterDiscount) * ReturnsModel.AddtionTax) / 100);
                AddtionalAmount = ReturnsModel.AddtionTaxAmount;
            }
            ReturnsModel.TotalAfterTax = (decimal)(ReturnsModel.TotalAfterDiscount - DeductionAmount + AddtionalAmount);
            ReturnsModel.TotalAfterTax = Math.Round(ReturnsModel.TotalAfterTax, 0);

            try
            {
                NumberToAlphabetic Obj = new NumberToAlphabetic();
                Obj.ConvertNumbersToArabicAlphabet(ReturnsModel.TotalAfterTax.ToString());
                TotalInWordsTax = Obj.GetNumberAr();
            }
            catch (Exception)
            {
            }
            StateHasChanged();
        }
        protected async Task AddNewClient()
        {
            await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/ClientAdd", "جارى التحميل", 1500);
        }
        protected async Task AddNewClientPayment()
        {
            decimal? Total = ShowTaxPart ? ReturnsModel.TotalAfterTax : ReturnsModel.TotalAfterDiscount;
            await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/ClientPaymentAdd?ClientId=" + ReturnsModel.ClientId + "&SerialNumber=" + ReturnsModel.SerialNumber + "&Total=" + Total, "جارى التحميل", 640);

        }
        async Task ReloadItemsList(SalesReturnsItemsVM row)
        {
            row.ItemName = String.Empty;
            row.Box_ParCode = String.Empty;
            row.Price = 0;


            var ItemListReturn = await Http.GetAsync("api/Items/GetItem");
            var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
            ItemList = JsonConvert.DeserializeObject<List<LookupItem>>(ItemvMs["data"].ToString());


        }

        async Task ReloadClientsList()
        {
            clients.Name = String.Empty;

            var ListReturn = await Http.GetAsync("api/Client/GetClients");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ClientList = JsonConvert.DeserializeObject<List<LookupKeyValueVM>>(vMs["data"].ToString());

        }

    }
}