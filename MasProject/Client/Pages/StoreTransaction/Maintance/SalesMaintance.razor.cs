﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using MasProject.Client.Shared;
using Microsoft.JSInterop;
using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel;
using System;
using System.Net.Http;

namespace MasProject.Client.Pages.StoreTransaction.Maintance
{
    public partial class SalesMaintance
    {
        private PageTitle PageTitle;
        bool ShowDelete = false;
        bool ShowConfirm = false;
        List<MantienceBillsVM> MaintanceList = new List<MantienceBillsVM>();
        int MaintanceBillID;
        int ClientId;
        int SerialNumber;
        ClientsVM clients = new ClientsVM();
        List<LookupKeyValueVM> ClientList = new List<LookupKeyValueVM>();
        List<BranchVM> BranchList = new List<BranchVM>();

        MantienceBillsVM MantienceModel = new MantienceBillsVM();
        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        {
            //Fill Table
            var ListReturn = await Http.GetAsync("api/MantienceBills/GetMantienceBills");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            MaintanceList = JsonConvert.DeserializeObject<List<MantienceBillsVM>>(vMs["data"].ToString());
            //Fill Dropdowns
            var SalesListReturn = await Http.GetAsync("api/MantienceBills/GetListDDL");
            var SalesvMs = JObject.Parse(SalesListReturn.Content.ReadAsStringAsync().Result);
            MantienceModel = JsonConvert.DeserializeObject<MantienceBillsVM>(SalesvMs["data"].ToString());
            ClientList = MantienceModel.ClientList;
            BranchList = MantienceModel.BranchList;
        }

        protected async Task AddNewMaintanceBill()
        {
            // await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/SalesMaintanceAdd", "جارى التحميل", 1000);
            await jsRuntime.InvokeAsync<object>("open", "/SalesMaintanceAdd", "_blank");
        }

        protected void Confirm()
        {
            ShowConfirm = false;
        }
        protected async Task EditOldMaintanceBill(int ID, string Type)
        {
            //  await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/SalesMaintanceEdit?ID=" + ID, "جارى التحميل", 1000);
            await jsRuntime.InvokeAsync<object>("open", "/SalesMaintanceEdit?ID=" + ID + "&Type=" + Type, "_blank");
        }

        protected void DeleteMaintanceBill(int Id, int serialNumber, int Clientid)
        {
            MaintanceBillID = Id;
            ClientId = Clientid;
            SerialNumber = serialNumber;
            ShowDelete = true;
        }
        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                string uri = "/api/MantienceBills/DeleteMantienceBills?ID=" + MaintanceBillID + "&SerialNumber=" + SerialNumber + "&ClientId=" + ClientId;

                var result = await Http.GetAsync(uri);

                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
            ShowDelete = false;
        }
        async Task ReloadClientsList()
        {
            clients.Name = String.Empty;

            var ListReturn = await Http.GetAsync("api/Client/GetClients");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ClientList = JsonConvert.DeserializeObject<List<LookupKeyValueVM>>(vMs["data"].ToString());

        }

        protected async Task GetAllByClient()
        {
            MantienceModel.ClientId = ClientList.Find(x => x.Name == clients.Name).ID;
            string uri = "/api/MantienceBills/GetSalesByClient?ClientId=" + MantienceModel.ClientId;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            MaintanceList = JsonConvert.DeserializeObject<List<MantienceBillsVM>>(vMs["data"].ToString());
            StateHasChanged();
            //SalesModel.ClientId = ClientList.Find(x => x.Name == clients.Name).ID;
            //if (SalesModel.ClientId != 0)
            //{
            //    SalesList = SalesList.Where(x => x.ClientId == SalesModel.ClientId).ToList();
            //}
            //else
            //{
            //    SalesList = TempSalesList;
            //}
        }
        //protected async Task GetAllByBranch()
        //{
        //    //if (SalesModel.BranchId != 0)
        //    //{
        //    //    SalesList = SalesList.Where(x => x.BranchId == SalesModel.BranchId).ToList();
        //    //}
        //    //else
        //    //{
        //    //    SalesList = TempSalesList;
        //    //}
        //    //SalesList = SalesList.Where(x => x.BranchId == SalesModel.BranchId).ToList();
        //    string SalesBranch = "/api/SalesBills/GetSalesByBranch?BranchId=" + SalesModel.BranchId;
        //    HttpResponseMessage SalesBranchReturn = await Http.GetAsync(SalesBranch);
        //    var SalesBranchVMs = JObject.Parse(SalesBranchReturn.Content.ReadAsStringAsync().Result);
        //    SalesList = JsonConvert.DeserializeObject<List<SalesBillsVM>>(SalesBranchVMs["data"].ToString());
        //}

        protected async Task GetAllByBranch()
        {


            string uri = "/api/MantienceBills/GetSalesByBranch?BranchId=" + MantienceModel.BranchId;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            MaintanceList = JsonConvert.DeserializeObject<List<MantienceBillsVM>>(vMs["data"].ToString());


            StateHasChanged();
        }
    }
}