﻿using MasProject.Domain.ViewModel.Hierarchy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel.Items;
using MasProject.Client.Shared;
using MasProject.Domain.ViewModel;
using System;
using static MasProject.Domain.Enums.Enums;
using Microsoft.JSInterop;
namespace MasProject.Client.Pages.StoreTransaction.Maintance
{
    public partial class SalesMaintanceEdit
    {
        private PageTitle PageTitle;
        bool ShowConfirm = false;
        MantienceItemsVM sItemModel = new MantienceItemsVM();
        List<MantienceItemsVM> sItemList = new List<MantienceItemsVM>();
        MaintanceSellExpensesVM sExpenseModel = new MaintanceSellExpensesVM();
        List<MaintanceSellExpensesVM> ExpenseList = new List<MaintanceSellExpensesVM>();
        MantienceBillsVM MaintanceModel = new MantienceBillsVM();
        List<StoreVM> StoreList = new List<StoreVM>();
        List<LookupKeyValueVM> ClientList = new List<LookupKeyValueVM>();
        List<DealTypeVM> DealTypeList = new List<DealTypeVM>();
        List<LookupItem> ItemList = new List<LookupItem>();
        List<SellingExpensesVM> ExpensesList = new List<SellingExpensesVM>();
        List<BranchVM> BranchList = new List<BranchVM>();
        List<MaintanceBillStoresVM> BillStoresList = new List<MaintanceBillStoresVM>();
        MaintanceBillStoresVM BillStoresModel = new MaintanceBillStoresVM();
        ClientsVM clients = new ClientsVM();

        bool FirstAdd = false;
        bool ShowTaxPart = false;
        string TotalInWords = "";
        string TotalInWordsTax = "";
        int FlagTypeID = 0; string ItemImg = "";
        // ItemVM ItemSrc = new ItemVM();
        bool IsDisabled = false;
        bool ShowImg = false;
        bool SelectDealTypeChecker = false;
        bool SelectClientChecker = false;
     
        protected override async Task OnInitializedAsync()
        {

            await BindList();
            var querystring = ExtensionMethods.QueryString(navigationManager);
            var id = querystring["ID"];
            await EditOldMaintanceBill(int.Parse(id));
            var Type = querystring["Type"];
            if (Type == "View")
            {
                //disable controls
                IsDisabled = true;
                PageTitle.Title = "عرض فاتورة الصيانة";
            }
            else
            {
                IsDisabled = false;
                PageTitle.Title = "تعديل فاتورة الصيانة";
            }
        }
        async Task BindList()
        {
            var SalesListReturn = await Http.GetAsync("api/MantienceBills/GetListDDL");
            var SalesvMs = JObject.Parse(SalesListReturn.Content.ReadAsStringAsync().Result);
            MaintanceModel = JsonConvert.DeserializeObject<MantienceBillsVM>(SalesvMs["data"].ToString());
            StoreList = MaintanceModel.StoreList;
            DealTypeList = MaintanceModel.DealTypeList;
            ClientList = MaintanceModel.ClientList;
            ItemList = MaintanceModel.ItemList;
            ExpensesList = MaintanceModel.sellingExpensesList;
            BranchList = MaintanceModel.BranchList;
            //var StoreListReturn = await Http.GetAsync("api/Store/GetStores");
            //var StorevMs = JObject.Parse(StoreListReturn.Content.ReadAsStringAsync().Result);
            //StoreList = JsonConvert.DeserializeObject<List<StoreVM>>(StorevMs["data"].ToString());

            //var ClientListReturn = await Http.GetAsync("api/Client/GetClient");
            //var ClientvMs = JObject.Parse(ClientListReturn.Content.ReadAsStringAsync().Result);
            //ClientList = JsonConvert.DeserializeObject<List<ClientsVM>>(ClientvMs["data"].ToString());

            //var GetDealTypeListReturn = await Http.GetAsync("api/DealType/GetDealType");
            //var GetDealTypevMs = JObject.Parse(GetDealTypeListReturn.Content.ReadAsStringAsync().Result);
            //DealTypeList = JsonConvert.DeserializeObject<List<DealTypeVM>>(GetDealTypevMs["data"].ToString());

            //var ItemListReturn = await Http.GetAsync("api/Items/GetItems");
            //var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
            //ItemList = JsonConvert.DeserializeObject<List<ItemVM>>(ItemvMs["data"].ToString());

            //var ExpensesListReturn = await Http.GetAsync("api/SellingExpenses/GetSellingExpenses");
            //var ExpensesvMs = JObject.Parse(ExpensesListReturn.Content.ReadAsStringAsync().Result);
            //ExpensesList = JsonConvert.DeserializeObject<List<SellingExpensesVM>>(ExpensesvMs["data"].ToString());

        }
        protected async Task TaxPart()
        {
            if (FlagTypeID == 1)
            {
                MaintanceModel.FlagType = true;
                ShowTaxPart = true;
                //get 14% from otherconstant where id=1
                //calculate addtions amount
                if (MaintanceModel.AddtionTax == 0)
                {
                    string uri = "/api/OtherConstant/GetOtherConstantRatio?Id=1";
                    HttpResponseMessage response = await Http.GetAsync(uri);
                    var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                    MaintanceModel.AddtionTax = JsonConvert.DeserializeObject<decimal>(vMs["data"].ToString());
                }
                RecalculateTotal();
            }
            else
            {
                MaintanceModel.FlagType = false;
                ShowTaxPart = false;
            }
        }
        async Task BalancesRenew()
        {
            string priceuri = "/api/ClientsBalance/GetBalance?ClientId=" + MaintanceModel.ClientId;
            HttpResponseMessage ClientListReturn = await Http.GetAsync(priceuri);
            var ClientvMs = JObject.Parse(ClientListReturn.Content.ReadAsStringAsync().Result);
            MaintanceModel.ClientBalance = JsonConvert.DeserializeObject<decimal>(ClientvMs["data"].ToString());
        }
        async Task ClientsRenew()
        {
            var ClientListReturn = await Http.GetAsync("api/Client/GetClient");
            var ClientvMs = JObject.Parse(ClientListReturn.Content.ReadAsStringAsync().Result);
            ClientList = JsonConvert.DeserializeObject<List<LookupKeyValueVM>>(ClientvMs["data"].ToString());

        }
        //async Task GetSerialNumber()
        //{
        //    string serailuri = "/api/MantienceBills/GenerateSerial?BranchId=" + MaintanceModel.BranchId;
        //    HttpResponseMessage serialReturn = await Http.GetAsync(serailuri);
        //    var serialvms = JObject.Parse(serialReturn.Content.ReadAsStringAsync().Result);
        //    MaintanceModel.SerialNumber = JsonConvert.DeserializeObject<int>(serialvms["data"].ToString());
        //    StateHasChanged();

        //}
        async Task ItemsRenew(MantienceItemsVM row)
        {
            var ItemListReturn = await Http.GetAsync("api/Items/GetItems");
            var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
            ItemList = JsonConvert.DeserializeObject<List<LookupItem>>(ItemvMs["data"].ToString());
            //get price for selected items
            //string priceuri = "/api/Items/GetItemPriceByDealId?DealId=" + MaintanceModel.DealTypeId + "&ItemId=" + row.ItemId;
            //HttpResponseMessage priceresponse = await Http.GetAsync(priceuri);
            //var pricevMs = JObject.Parse(priceresponse.Content.ReadAsStringAsync().Result);
            //row.Price = JsonConvert.DeserializeObject<decimal>(pricevMs["data"].ToString());
            LookupItem SelectedITem = ItemList.Find(x => x.ID == row.ItemId);
            ItemImg = SelectedITem.ItemImg;
            row.Box_ParCode = SelectedITem.Code;
            if (MaintanceModel.DealTypeId != null && MaintanceModel.DealTypeId != 0)
            {
                row.Price = GetPriceFromDealType((int)MaintanceModel.DealTypeId, SelectedITem);
            }
            else
            {
                SelectDealTypeChecker = true;
            }
            row.Quantity = null;
            row.Total = null;
            RecalculateTotal();
            StateHasChanged();
        }
        //async Task ItemsRenew()
        //{
        //    var ItemListReturn = await Http.GetAsync("api/Items/GetItems");
        //    var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
        //    ItemList = JsonConvert.DeserializeObject<List<ItemVM>>(ItemvMs["data"].ToString());

        //}
        async Task ExpensesRenew()
        {
            var ExpensesListReturn = await Http.GetAsync("api/SellingExpenses/GetSellingExpenses");
            var ExpensesvMs = JObject.Parse(ExpensesListReturn.Content.ReadAsStringAsync().Result);
            ExpensesList = JsonConvert.DeserializeObject<List<SellingExpensesVM>>(ExpensesvMs["data"].ToString());
        }
        void ChangeDealType()
        {
            //loop on items for delete price and
            foreach (var BillStore in BillStoresList)
            {
                foreach (var row in BillStore.MantienceItem)
                {
                    if (row.ItemId != 0)
                    {
                        LookupItem SelectedITem = ItemList.Find(x => x.ID == row.ItemId);
                        ItemImg = SelectedITem.ItemImg;
                        row.Box_ParCode = SelectedITem.Code;
                        row.Price = GetPriceFromDealType((int)MaintanceModel.DealTypeId, SelectedITem);
                    }
                    if (row.Quantity != 0 && row.Quantity != null)
                    {
                        row.Total = row.Price * row.Quantity;
                    }
                }
            }
            RecalculateTotal();
        }
        protected async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        }
        //void Confirm()
        //{
        //    //await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        //    //changed 30-10-2020// protected async Task
        //    ShowConfirm = false;
        //    IsDisabled = false;
        //}
        protected async Task AddMaintanceBill()
        {
            MaintanceModel.ClientId = ClientList.Find(x => x.Name == clients.Name).ID;
            HttpResponseMessage result = new HttpResponseMessage();
            //check for save
            if (MaintanceModel.SerialNumber != 0 & MaintanceModel.SerialNumber != null)
            {
                if (MaintanceModel.ClientId != 0)
                {
                    if (FirstAdd == false)
                    {
                        var AddModel = JsonConvert.SerializeObject(MaintanceModel);
                        var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/MantienceBills/AddMantienceBills", content);
                        var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                        MaintanceModel.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                        FirstAdd = true;
                    }
                    else
                    {
                        // MaintanceModel.MantienceItems = sItemList;
                        MaintanceModel.MaintanceBillStores = BillStoresList;
                        MaintanceModel.MaintanceSellExpenses = ExpenseList;
                        var EditModel = JsonConvert.SerializeObject(MaintanceModel);
                        var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/MantienceBills/EditMantienceBills", content);
                    }
                    if (result.IsSuccessStatusCode)
                    {
                        StateHasChanged();
                    }
                }
                else
                {
                    SelectClientChecker = true;
                }
            }
        }
        protected async Task EditOldMaintanceBill(int ID)
        {
         //   IsDisabled = true;
           
            FirstAdd = true;
            string uri = "/api/MantienceBills/GetSpecificMantienceBills?Id=" + ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            MaintanceModel = JsonConvert.DeserializeObject<MantienceBillsVM>(vMs["data"].ToString());
            MaintanceModel.convert = 0;
            sItemList = new List<MantienceItemsVM>();
           // sItemList = MaintanceModel.MantienceItems;
            BillStoresList = MaintanceModel.MaintanceBillStores;
            ExpenseList = new List<MaintanceSellExpensesVM>();
            ExpenseList = MaintanceModel.MaintanceSellExpenses;
            clients.Name = MaintanceModel.ClientName;
            if (MaintanceModel.FlagType)
            {
                FlagTypeID = 1;
                await TaxPart();
            }
            RecalculateTotal();
            StateHasChanged();
        }
        private void ShowImage(MantienceItemsVM row)
        {
            ItemImg = row.ItemImg;
            ShowImg = true;
        }
        protected async Task EditMaintanceBill()
        {
            MaintanceModel.ClientId = ClientList.Find(x => x.Name == clients.Name).ID;
            //  MaintanceModel.MantienceItems = sItemList;
            MaintanceModel.MaintanceBillStores = BillStoresList;
            var EditModel = JsonConvert.SerializeObject(MaintanceModel);
            var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
            var result = await Http.PostAsync("api/MantienceBills/EditMantienceBills", content);
            if (result.IsSuccessStatusCode)
            {
                if (MaintanceModel.convert == 1 || MaintanceModel.IsConverted)
                {
                    var Convertresult = await Http.PostAsync("api/MantienceBills/ConvertMantienceBills", content);

                }
                ShowConfirm = true;
            }
        }

        protected async Task GetDealType()
        {
            MaintanceModel.ClientId = ClientList.Find(x => x.Name == clients.Name).ID;
            string uri = "/api/Client/GetDealType?Id=" + MaintanceModel.ClientId;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            MaintanceModel.DealTypeId = JsonConvert.DeserializeObject<int>(vMs["data"].ToString());

            string priceuri = "/api/ClientsBalance/GetBalance?ClientId=" + MaintanceModel.ClientId;
            HttpResponseMessage ClientListReturn = await Http.GetAsync(priceuri);
            var ClientvMs = JObject.Parse(ClientListReturn.Content.ReadAsStringAsync().Result);
            MaintanceModel.ClientBalance = JsonConvert.DeserializeObject<decimal>(ClientvMs["data"].ToString());

            string Taxuri = "/api/Client/GetTaxFile?Id=" + MaintanceModel.ClientId;
            HttpResponseMessage TaxReturn = await Http.GetAsync(Taxuri);
            var TaxvMs = JObject.Parse(TaxReturn.Content.ReadAsStringAsync().Result);
            ClientsVM TempFile = JsonConvert.DeserializeObject<ClientsVM>(TaxvMs["data"].ToString());
            MaintanceModel.TaxFileNumber = TempFile.Taxfile;

            StateHasChanged();
        }
        void AddNewRow(int? BillStoresIdentifer)
        {
            //int IdentifierCount = sItemList.Count;
            //sItemModel = new ExecutiveItemsVM { Identifer = IdentifierCount + 1, };
            //sItemList.Add(sItemModel);
            var tempList = BillStoresList.Find(x => x.Identifer == BillStoresIdentifer).MantienceItem;
            int IdentifierCount = tempList.Count;
            sItemModel = new MantienceItemsVM { Identifer = IdentifierCount + 1, };
            tempList.Add(sItemModel);
            //int IdentifierCount = sItemList.Count;
            //sItemModel = new MantienceItemsVM { Identifer = IdentifierCount + 1, };
            //sItemList.Add(sItemModel);
            StateHasChanged();
        }
        void RemoveRow(MantienceItemsVM row, int? BillStoresIdentifer)
        {
            //sItemList.Remove(row);
            var tempList = BillStoresList.Find(x => x.Identifer == BillStoresIdentifer).MantienceItem;
            tempList.Remove(row);
            StateHasChanged();
        }
        void AddNewExpenseRow()
        {
            int IdentifierCount = ExpenseList.Count;
            sExpenseModel = new MaintanceSellExpensesVM { Identifer = IdentifierCount + 1, };
            ExpenseList.Add(sExpenseModel);
            StateHasChanged();
        }
        void RemoveExpenseRow(MaintanceSellExpensesVM row)
        {
            ExpenseList.Remove(row);
            StateHasChanged();
        }
        protected async Task GetItemDataByCode(MantienceItemsVM row)
        {
            HttpResponseMessage result = new HttpResponseMessage();
            LookupItem SelectedITem = ItemList.Find(x => (x.BoxCode == row.Box_ParCode || x.GuaranteeParCode == row.Box_ParCode || x.InterationalParCode == row.Box_ParCode || x.Code == row.Box_ParCode || x.ParCodeNote == row.Box_ParCode || x.SupplierParCode == row.Box_ParCode));
            ItemImg = SelectedITem.ItemImg ?? "";
            row.ItemId = SelectedITem.ID;
            row.ItemName = SelectedITem.Name;
            if (MaintanceModel.DealTypeId != null && MaintanceModel.DealTypeId != 0)
            {
                row.Price = GetPriceFromDealType((int)MaintanceModel.DealTypeId, SelectedITem);
            }
            else
            {
                SelectDealTypeChecker = true;
            }
            row.Quantity = null;
            row.Total = null;
            //check for save
            if (MaintanceModel.SerialNumber != 0)
            {
                //string uri = "/api/Items/GetItemByAnyCode?AnyCode=" + row.Box_ParCode;
                //HttpResponseMessage response = await Http.GetAsync(uri);
                //var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                //ItemVM temp = JsonConvert.DeserializeObject<ItemVM>(vMs["data"].ToString());
                //row.Box_ParCode = temp.National_ParCode;
                //row.ItemId = temp.ID;
                ////row.Price = null;
                //string priceuri = "/api/Items/GetItemPriceByDealId?DealId=" + MaintanceModel.DealTypeId + "&ItemId=" + row.ItemId;
                //HttpResponseMessage priceresponse = await Http.GetAsync(priceuri);
                //var pricevMs = JObject.Parse(priceresponse.Content.ReadAsStringAsync().Result);
                //row.Price = JsonConvert.DeserializeObject<decimal>(pricevMs["data"].ToString());
                //row.Quantity = null;
                //row.Total = null;
                //string uriImage = "/api/Items/GetItemImage?ItemId=" + row.ItemId;
                //HttpResponseMessage responseImage = await Http.GetAsync(uriImage);
                //var vMsImage = JObject.Parse(responseImage.Content.ReadAsStringAsync().Result);
                //ItemSrc = JsonConvert.DeserializeObject<ItemVM>(vMsImage["data"].ToString());
                //ItemImg = ItemSrc.ItemImg;
                if (MaintanceModel.ClientId != 0)
                {
                    if (FirstAdd == false)
                    {
                        var AddModel = JsonConvert.SerializeObject(MaintanceModel);
                        var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/MantienceBills/AddMantienceBills", content);
                        var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                        MaintanceModel.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                        FirstAdd = true;
                    }
                    else
                    {
                        //   MaintanceModel.MantienceItems = sItemList;
                        MaintanceModel.MaintanceBillStores = BillStoresList;
                        var EditModel = JsonConvert.SerializeObject(MaintanceModel);
                        var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/MantienceBills/EditMantienceBills", content);
                    }
                    if (result.IsSuccessStatusCode)
                    {

                        StateHasChanged();
                    }
                }
                else
                {
                    SelectClientChecker = true;
                }
            }
        }
        protected async Task GetItemData(MantienceItemsVM row)
        {
            HttpResponseMessage result = new HttpResponseMessage();
            //   LookupItem SelectedITem = ItemList.Find(x => x.ID == row.ItemId);
            LookupItem SelectedITem = ItemList.Find(x => x.Name == row.ItemName);
            ItemImg = SelectedITem.ItemImg;
            row.Box_ParCode = SelectedITem.Code;
            row.ItemId = SelectedITem.ID;
            if (MaintanceModel.DealTypeId != null && MaintanceModel.DealTypeId != 0)
            {
                row.Price = GetPriceFromDealType((int)MaintanceModel.DealTypeId, SelectedITem);
            }
            else
            {
                SelectDealTypeChecker = true;
            }
            row.Quantity = null;
            row.Total = null;
            //check for save
            if (MaintanceModel.SerialNumber != 0 & MaintanceModel.SerialNumber != null)
            {
                if (MaintanceModel.ClientId != 0)
                {
                    if (FirstAdd == false)
                    {
                        var AddModel = JsonConvert.SerializeObject(MaintanceModel);
                        var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/MantienceBills/AddMantienceBills", content);
                        var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                        MaintanceModel.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                        FirstAdd = true;
                    }
                    else
                    {
                        // MaintanceModel.MantienceItems = sItemList;
                        MaintanceModel.MaintanceBillStores = BillStoresList;
                        MaintanceModel.MaintanceSellExpenses = ExpenseList;
                        var EditModel = JsonConvert.SerializeObject(MaintanceModel);
                        var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/MantienceBills/EditMantienceBills", content);
                    }
                    if (result.IsSuccessStatusCode)
                    {
                        //string uri = "/api/Items/GetNationalCodeByID?Id=" + row.ItemId;
                        //HttpResponseMessage response = await Http.GetAsync(uri);
                        //var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                        //row.Box_ParCode = JsonConvert.DeserializeObject<string>(vMs["data"].ToString());
                        //string priceuri = "/api/Items/GetItemPriceByDealId?DealId=" + MaintanceModel.DealTypeId + "&ItemId=" + row.ItemId;
                        //HttpResponseMessage priceresponse = await Http.GetAsync(priceuri);
                        //var pricevMs = JObject.Parse(priceresponse.Content.ReadAsStringAsync().Result);
                        //row.Price = JsonConvert.DeserializeObject<decimal>(pricevMs["data"].ToString());
                        //row.Quantity = null;
                        //row.Total = null;
                        //string uriImage = "/api/Items/GetItemImage?ItemId=" + row.ItemId;
                        //HttpResponseMessage responseImage = await Http.GetAsync(uriImage);
                        //var vMsImage = JObject.Parse(responseImage.Content.ReadAsStringAsync().Result);
                        //ItemSrc = JsonConvert.DeserializeObject<ItemVM>(vMsImage["data"].ToString());
                        //ItemImg = ItemSrc.ItemImg;
                        StateHasChanged();
                    }
                }
                else
                {
                    SelectClientChecker = true;
                }
            }
        }
        private decimal GetPriceFromDealType(int DealTypeId, LookupItem SelectedITem)
        {
            float price = 0;
            switch (DealTypeId)
            {
                case (int)EnPriceTypes.التكلفة:
                    price = SelectedITem.CostPrice;
                    break;
                case (int)EnPriceTypes.الموزع:
                    price = SelectedITem.SupplierPrice;
                    break;
                case (int)EnPriceTypes.الجملة:
                    price = SelectedITem.WholesalePrice;
                    break;
                case (int)EnPriceTypes.البيع:
                    price = SelectedITem.SellingPrice;
                    break;
                case (int)EnPriceTypes.التنفيذ:
                    price = SelectedITem.ExecutionPrice;
                    break;
                case (int)EnPriceTypes.العرض:
                    price = SelectedITem.ShowPrice;
                    break;
                case (int)EnPriceTypes.التجزئة:
                    price = SelectedITem.SellingPrice;
                    break;
                case (int)EnPriceTypes.أخرى:
                    price = SelectedITem.other_Price;
                    break;
            }
            return (decimal)price;
        }
        void ChangeItemQuantity(int BillStoresIdentifer)
        {
            RecalculateTotal();
            AddNewRow(BillStoresIdentifer);
        }
        void RecalculateTotal()
        {
            decimal CalculatedTotal = 0;
            foreach (var BillStore in BillStoresList)
            {
                foreach (var rowitem in BillStore.MantienceItem)
                {
                    if (rowitem.Quantity != null && rowitem.Price != null)
                    {
                        rowitem.Total = rowitem.Quantity.Value * rowitem.Price.Value;
                        CalculatedTotal = (decimal)(CalculatedTotal + rowitem.Total);
                    }
                }
            }
            foreach (var rowExpense in ExpenseList)
            {
                if (rowExpense.Amount != null)
                {
                    CalculatedTotal = (decimal)(CalculatedTotal + rowExpense.Amount.Value);
                }
            }
            MaintanceModel.TotalPrice = CalculatedTotal;
            MaintanceModel.TotalAfterDiscount = MaintanceModel.TotalPrice;

            if (MaintanceModel.DiscountValue != null & MaintanceModel.DiscountValue != 0 & MaintanceModel.DiscountPrecentage == 0)
            {
                decimal temp = MaintanceModel.DiscountValue.Value / MaintanceModel.TotalPrice.Value;
                MaintanceModel.DiscountPrecentage = temp * 100;
                MaintanceModel.TotalAfterDiscount = (MaintanceModel.TotalPrice == null ? 0 : MaintanceModel.TotalPrice) - MaintanceModel.DiscountValue;
            }
            else if (MaintanceModel.DiscountPrecentage != null & MaintanceModel.DiscountPrecentage != 0 & MaintanceModel.DiscountValue == 0)
            {
                decimal temp = (MaintanceModel.TotalPrice.Value * MaintanceModel.DiscountPrecentage.Value) / 100;
                MaintanceModel.DiscountValue = temp;
                MaintanceModel.TotalAfterDiscount = (MaintanceModel.TotalPrice == null ? 0 : MaintanceModel.TotalPrice) - MaintanceModel.DiscountValue;
            }
            else
            {
                MaintanceModel.TotalAfterDiscount = (MaintanceModel.TotalPrice == null ? 0 : MaintanceModel.TotalPrice) - MaintanceModel.DiscountValue;
            }
            if (MaintanceModel.TotalAfterDiscount == null)
            {
                MaintanceModel.TotalAfterDiscount = MaintanceModel.TotalPrice;
            }
            MaintanceModel.TotalAfterDiscount = Math.Round((decimal)MaintanceModel.TotalAfterDiscount, 0);
            try
            {
                NumberToAlphabetic Obj = new NumberToAlphabetic();
                Obj.ConvertNumbersToArabicAlphabet(MaintanceModel.TotalAfterDiscount.ToString());
                TotalInWords = Obj.GetNumberAr();
            }
            catch (Exception)
            {

            }
            decimal DeductionAmount = 0;
            decimal AddtionalAmount = 0;
            if (MaintanceModel.SourceDeduction != 0)
            {
                MaintanceModel.SourceDeductionAmount = (decimal)(((MaintanceModel.TotalAfterDiscount == null ? 0 : MaintanceModel.TotalAfterDiscount) * MaintanceModel.SourceDeduction) / 100);
                DeductionAmount = MaintanceModel.SourceDeductionAmount;
            }
            if (MaintanceModel.AddtionTax != 0)
            {
                MaintanceModel.AddtionTaxAmount = (decimal)(((MaintanceModel.TotalAfterDiscount == null ? 0 : MaintanceModel.TotalAfterDiscount) * MaintanceModel.AddtionTax) / 100);
                AddtionalAmount = MaintanceModel.AddtionTaxAmount;
            }
            MaintanceModel.TotalAfterTax = (decimal)(MaintanceModel.TotalAfterDiscount - DeductionAmount + AddtionalAmount);
            MaintanceModel.TotalAfterTax = Math.Round(MaintanceModel.TotalAfterTax, 0);

            try
            {
                NumberToAlphabetic Obj = new NumberToAlphabetic();
                Obj.ConvertNumbersToArabicAlphabet(MaintanceModel.TotalAfterTax.ToString());
                TotalInWordsTax = Obj.GetNumberAr();
            }
            catch (Exception)
            {
            }
            StateHasChanged();
        }
        //protected void GetItemTotal(MantienceItemsVM row)
        //{
        //    row.Total = row.Quantity * row.Price;
        //    MaintanceModel.TotalPrice = (MaintanceModel.TotalPrice == null ? 0 : MaintanceModel.TotalPrice) + row.Total;
        //    if (MaintanceModel.DiscountPrecentage != 0 && MaintanceModel.DiscountPrecentage != null)
        //    {
        //        decimal temp = (MaintanceModel.TotalPrice.Value * MaintanceModel.DiscountPrecentage.Value) / 100;
        //        MaintanceModel.TotalAfterDiscount = MaintanceModel.TotalPrice - temp;
        //        MaintanceModel.DiscountValue = temp;
        //        MaintanceModel.TotalAfterDiscount = MaintanceModel.TotalPrice - MaintanceModel.DiscountValue;
        //    }
        //    StateHasChanged();
        //}
        //protected void GetItemTotalEdit(MantienceItemsVM row)
        //{
        //    decimal? olditemtotal = (row.Total == null ? 0 : row.Total);
        //    row.Total = row.Quantity * row.Price;
        //    MaintanceModel.TotalPrice = (MaintanceModel.TotalPrice == null ? 0 : MaintanceModel.TotalPrice) + row.Total - olditemtotal;
        //    if (MaintanceModel.DiscountPrecentage != 0 && MaintanceModel.DiscountPrecentage != null)
        //    {
        //        decimal temp = (MaintanceModel.TotalPrice.Value * MaintanceModel.DiscountPrecentage.Value) / 100;
        //        MaintanceModel.TotalAfterDiscount = MaintanceModel.TotalPrice - temp;
        //        MaintanceModel.DiscountValue = temp;
        //        MaintanceModel.TotalAfterDiscount = MaintanceModel.TotalPrice - MaintanceModel.DiscountValue;
        //    }
        //    StateHasChanged();
        //}
        //protected void GetExpenseTotal(MaintanceSellExpensesVM row)
        //{
        //    MaintanceModel.TotalPrice = (MaintanceModel.TotalPrice == null ? 0 : MaintanceModel.TotalPrice) + row.Amount;
        //    if (MaintanceModel.DiscountPrecentage != 0 && MaintanceModel.DiscountPrecentage != null)
        //    {
        //        decimal temp = (MaintanceModel.TotalPrice.Value * MaintanceModel.DiscountPrecentage.Value) / 100;
        //        MaintanceModel.TotalAfterDiscount = MaintanceModel.TotalPrice - temp;
        //        MaintanceModel.DiscountValue = temp;
        //        MaintanceModel.TotalAfterDiscount = MaintanceModel.TotalPrice - MaintanceModel.DiscountValue;
        //    }
        //    StateHasChanged();
        //}
        //protected void GetExpenseTotalEdit(MaintanceSellExpensesVM row)
        //{
        //    MaintanceModel.TotalPrice = (MaintanceModel.TotalPrice == null ? 0 : MaintanceModel.TotalPrice) + row.Amount;
        //    if (MaintanceModel.DiscountPrecentage != 0 && MaintanceModel.DiscountPrecentage != null)
        //    {
        //        decimal temp = (MaintanceModel.TotalPrice.Value * MaintanceModel.DiscountPrecentage.Value) / 100;
        //        MaintanceModel.TotalAfterDiscount = MaintanceModel.TotalPrice - temp;
        //        MaintanceModel.DiscountValue = temp;
        //        MaintanceModel.TotalAfterDiscount = MaintanceModel.TotalPrice - MaintanceModel.DiscountValue;
        //    }
        //    StateHasChanged();
        //}
        //void CalculateDiscountPrec()
        //{
        //    if (MaintanceModel.DiscountPrecentage != 0 && MaintanceModel.DiscountPrecentage != null)
        //    {
        //        decimal temp = (MaintanceModel.TotalPrice.Value * MaintanceModel.DiscountPrecentage.Value) / 100;
        //        MaintanceModel.TotalAfterDiscount = (MaintanceModel.TotalPrice == null ? 0 : MaintanceModel.TotalPrice) - temp;
        //        MaintanceModel.DiscountValue = temp;
        //    }

        //}
        //void CalculateDiscountAmt()
        //{
        //    if (MaintanceModel.DiscountValue != 0 && MaintanceModel.DiscountValue != null)
        //    {
        //        MaintanceModel.TotalAfterDiscount = (MaintanceModel.TotalPrice == null ? 0 : MaintanceModel.TotalPrice) - MaintanceModel.DiscountValue;
        //        decimal temp = MaintanceModel.DiscountValue.Value / MaintanceModel.TotalPrice.Value;
        //        MaintanceModel.DiscountPrecentage = temp * 100;
        //    }
        //}
        //protected async Task AddNewItem()
        //{
        //    await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/AddItems", "جارى التحميل", 1500);
        //}
        protected async Task AddNewClient()
        {
            await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/ClientAdd", "جارى التحميل", 1500);
        }

        protected async Task AddNewClientPayment()
        {
            decimal? Total = ShowTaxPart ? MaintanceModel.TotalAfterTax : MaintanceModel.TotalAfterDiscount;
            await jsRuntime.InvokeAsync<object>("open", "/ClientPaymentAdd?ClientId=" + MaintanceModel.ClientId + "&SerialNumber=" + MaintanceModel.SerialNumber + "&Total=" + Total + "&Branch=" + MaintanceModel.BranchId, "_blank");

        }
        void AddNewStore()
        {
            int IdentifierCount = BillStoresList.Count;
            BillStoresModel = new MaintanceBillStoresVM { Identifer = IdentifierCount + 1, };
            sItemModel = new MantienceItemsVM { Identifer = 1, };
            var TempsItemList = new List<MantienceItemsVM>();
            TempsItemList.Add(sItemModel);
            BillStoresModel.MantienceItem = TempsItemList;
            //SalesBillStoresModel.Store = StoreList;
            BillStoresList.Add(BillStoresModel);
            StateHasChanged();
        }
        void RemoveNewStore(MaintanceBillStoresVM Store)
        {
            BillStoresList.Remove(Store);
            StateHasChanged();
        }
        async Task ReloadClientsList()
        {
            clients.Name = String.Empty;

            var ListReturn = await Http.GetAsync("api/Client/GetClients");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ClientList = JsonConvert.DeserializeObject<List<LookupKeyValueVM>>(vMs["data"].ToString());

        }
        async Task ReloadItemsList(MantienceItemsVM row)
        {
            row.ItemName = String.Empty;
            row.Box_ParCode = String.Empty;
            row.Price = 0;


            var ItemListReturn = await Http.GetAsync("api/Items/GetItem");
            var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
            ItemList = JsonConvert.DeserializeObject<List<LookupItem>>(ItemvMs["data"].ToString());


        }
    }
}