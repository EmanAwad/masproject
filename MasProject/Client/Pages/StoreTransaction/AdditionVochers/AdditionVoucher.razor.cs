﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using MasProject.Domain.ViewModel.StoreTransaction.Voucher;
using MasProject.Client.Shared;
using Microsoft.JSInterop;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Persons;
using System;
using System.Net.Http;

namespace MasProject.Client.Pages.StoreTransaction.AdditionVochers
{
    public partial class AdditionVoucher
    {
       // private PageTitle PageTitle;
        bool ShowDelete = false;
        List<AdditionVoucherVM> AdditionVoucherList = new List<AdditionVoucherVM>();
        List<LookupKeyValueVM> Client_Supplier_List = new List<LookupKeyValueVM>();
        AdditionVoucherVM AdditionVoucherModel = new AdditionVoucherVM();
        List<StoreVM> StoreList = new List<StoreVM>();
        ClientsVM clients = new ClientsVM();
        //bool FirstAdd = false;
        int AdditionVoucherID;
        string AdditionType = "";
        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        {
            var ListReturn = await Http.GetAsync("api/AdditionVoucher/GetAdditionVoucher");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            BillsTbl<AdditionVoucherVM> ReturnModel = JsonConvert.DeserializeObject<BillsTbl<AdditionVoucherVM>>(vMs["data"].ToString());
            AdditionVoucherList = ReturnModel.BillsList;
            StoreList = ReturnModel.SearchItems.StoreList;
            Client_Supplier_List = ReturnModel.SearchItems.Client_Supplier_List;

            //Fill Table
            //var ListReturn = await Http.GetAsync("api/AdditionVoucher/GetAdditionVoucher");
            //var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            //AdditionVoucherList = JsonConvert.DeserializeObject<List<AdditionVoucherVM>>(vMs["data"].ToString());
            ////Fill Dropdowns
            //AdditionVoucherModel = new AdditionVoucherVM { };
            //var AdditionVoucherListReturn = await Http.GetAsync("api/AdditionVoucher/GetListsOfDDl");
            //var AdditionVouchervMs = JObject.Parse(AdditionVoucherListReturn.Content.ReadAsStringAsync().Result);
            //AdditionVoucherModel = JsonConvert.DeserializeObject<AdditionVoucherVM>(AdditionVouchervMs["data"].ToString());
            //StoreList = AdditionVoucherModel.StoreList;       
            //Client_Supplier_List = AdditionVoucherModel.Client_Supplier_List;

        }
        protected async Task GetAllByClientSupplierEmp()
        {
            AdditionVoucherModel.Client_Supplier_Id = Client_Supplier_List.Find(x => x.Name == clients.Name).ID;
            if (AdditionVoucherModel.Client_Supplier_Id != 0)
            {
                string uri = "/api/AdditionVoucher/GetAdditionVoucherByUserType?UserType=" + AdditionVoucherModel.Client_Supplier_Id;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                AdditionVoucherList = JsonConvert.DeserializeObject<List<AdditionVoucherVM>>(vMs["data"].ToString());
            }
            else
            {
                var ListReturn = await Http.GetAsync("api/AdditionVoucher/GetAdditionVoucher");
                var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
                AdditionVoucherList = JsonConvert.DeserializeObject<List<AdditionVoucherVM>>(vMs["data"].ToString());
            }

            StateHasChanged();

        }
        protected async Task GetAllByStore()
        {
           
            if (AdditionVoucherModel.StoreId != 0)
            {
                string uri = "/api/AdditionVoucher/GetAdditionVoucherByStore?StoreId=" + AdditionVoucherModel.StoreId;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                AdditionVoucherList = JsonConvert.DeserializeObject<List<AdditionVoucherVM>>(vMs["data"].ToString());
            }
            else
            {
                var ListReturn = await Http.GetAsync("api/AdditionVoucher/GetAdditionVoucher");
                var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
                AdditionVoucherList = JsonConvert.DeserializeObject<List<AdditionVoucherVM>>(vMs["data"].ToString());
            }

            StateHasChanged();

        }
        protected async Task AddNewAdditionVoucher()
        {
            await jsRuntime.InvokeAsync<object>("open", "/AdditionVoucherAdd", "_blank");
        }
        protected async Task EditOldAdditionVoucher(int ID  , string Show)//, string Type+"&Type="+Type
        {
            await jsRuntime.InvokeAsync<object>("open", "/AdditionVoucherEdit?ID="+ID+ "&Type=" + Show, "_blank");

        }
        protected async Task ViewOldAdditionVoucher(int ID, string Show)//, string Type+ "&Type=" + Type
        {
            await jsRuntime.InvokeAsync<object>("open", "/AdditionVoucherView?ID=" + ID  + "&Type=" + Show, "_blank");

        }
        protected void DeleteAdditionVoucher(int Id, string Type)
        {
            AdditionVoucherID = Id;
            AdditionType = Type;
            ShowDelete = true;
        }
        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                string uri = "/api/AdditionVoucher/DeleteAdditionVoucher?ID="+AdditionVoucherID+"&Type="+AdditionType;

                var result = await Http.GetAsync(uri);

                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
            ShowDelete = false;

        }

        async Task ReloadClientsList()
        {
            clients.Name = String.Empty;

            var ListReturn = await Http.GetAsync("api/Client/GetClients");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            Client_Supplier_List = JsonConvert.DeserializeObject<List<LookupKeyValueVM>>(vMs["data"].ToString());

        }

        async Task ClientsRenew()
        {
            var ClientListReturn = await Http.GetAsync("api/AdditionVoucher/GetListsOfSupplierClientDDl");
            var ClientvMs = JObject.Parse(ClientListReturn.Content.ReadAsStringAsync().Result);
            Client_Supplier_List = JsonConvert.DeserializeObject<List<LookupKeyValueVM>>(ClientvMs["data"].ToString());

        }
    }
}