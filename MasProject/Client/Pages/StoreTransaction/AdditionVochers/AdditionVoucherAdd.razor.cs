﻿using MasProject.Domain.ViewModel.Hierarchy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Domain.ViewModel.StoreTransaction.Voucher;
using MasProject.Domain.ViewModel.Items;
using MasProject.Domain.ViewModel;
using MasProject.Client.Shared;
using System;
using MasProject.Domain.ViewModel.Persons;

namespace MasProject.Client.Pages.StoreTransaction.AdditionVochers
{
    public partial class AdditionVoucherAdd
    {
        private PageTitle PageTitle;
        bool ShowConfirm = false;
        AdditionVoucherItemVM sItemModel = new AdditionVoucherItemVM();
        List<AdditionVoucherItemVM> sItemList = new List<AdditionVoucherItemVM>();
        AdditionVoucherVM AdditionVoucherModel = new AdditionVoucherVM();
        List<StoreVM> StoreList = new List<StoreVM>();
        List<LookupKeyValueVM> Client_Supplier_List = new List<LookupKeyValueVM>();
        List<LookupItem> ItemList = new List<LookupItem>();
        List<Item_StoreVM> ItemStoreList = new List<Item_StoreVM>();
        ClientsVM clients = new ClientsVM();
        bool FirstAdd = false;
        bool ShowImg = false;
        string ItemImg = "";
        bool SelectTypeChecker = false;
        bool SelectStoreChecker = false;
        bool IsDisabled = false;
        protected override async Task OnInitializedAsync()
        {
            FirstAdd = false;
            sItemModel = new AdditionVoucherItemVM { Identifer = 1 };
            sItemList = new List<AdditionVoucherItemVM>();
            sItemList.Add(sItemModel);
            AdditionVoucherModel = new AdditionVoucherVM();
            await BindList();
            StateHasChanged();

        }
        async Task BindList()
        {
            AdditionVoucherModel = new AdditionVoucherVM { };
            var AdditionVoucherListReturn = await Http.GetAsync("api/AdditionVoucher/GetListsOfDDl");
            var AdditionVouchervMs = JObject.Parse(AdditionVoucherListReturn.Content.ReadAsStringAsync().Result);
            AdditionVoucherModel = JsonConvert.DeserializeObject<AdditionVoucherVM>(AdditionVouchervMs["data"].ToString());
            StoreList = AdditionVoucherModel.StoreList;
            ItemList = AdditionVoucherModel.ItemList;
            Client_Supplier_List = AdditionVoucherModel.Client_Supplier_List;
        }
        async Task ClientsRenew()
        {
            var ClientListReturn = await Http.GetAsync("api/AdditionVoucher/GetListsOfSupplierClientDDl");
            var ClientvMs = JObject.Parse(ClientListReturn.Content.ReadAsStringAsync().Result);
            Client_Supplier_List = JsonConvert.DeserializeObject<List<LookupKeyValueVM>>(ClientvMs["data"].ToString());

        }
        async Task ItemsRenew()
        {
            var ItemListReturn = await Http.GetAsync("api/Items/GetItem");
            var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
            ItemList = JsonConvert.DeserializeObject<List<LookupItem>>(ItemvMs["data"].ToString());

        }
        protected async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        }
        //void Confirm()
        //{
        //    ShowConfirm = false;
        //    IsDisabled = false;
        //}
        protected async Task EditAdditionVoucher()
        {
            IsDisabled = true;
            HttpResponseMessage result = new HttpResponseMessage();
            if (AdditionVoucherModel.StoreId != 0 && AdditionVoucherModel.StoreId != null)
            {
                //if (AdditionVoucherModel.Name != "0" && AdditionVoucherModel.Name != "" && AdditionVoucherModel.Name != null)
                if (clients.Name != "0" && clients.Name != "" && clients.Name != null)
                {
                    AdditionVoucherModel.AdditionVoucherItem = sItemList;
                    //AdditionVoucherModel.UserType = Client_Supplier_List.Find(x => x.Name == AdditionVoucherModel.Name).Type;
                    // AdditionVoucherModel.Client_Supplier_Id = Client_Supplier_List.Find(x => x.Name == AdditionVoucherModel.Name).ID;
                    AdditionVoucherModel.Client_Supplier_Id = Client_Supplier_List.Find(x => x.Name == clients.Name).ID;
                    AdditionVoucherModel.UserType = Client_Supplier_List.Find(x => x.Name == clients.Name).Type;
                    if (FirstAdd == false)
                    {
                        FirstAdd = true;
                        var AddModel = JsonConvert.SerializeObject(AdditionVoucherModel);
                        var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/AdditionVoucher/AddAdditionVoucher", content);
                        var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                        AdditionVoucherModel.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                        
                    }
                    else
                    {
                        AdditionVoucherModel.AdditionVoucherItem = sItemList;
                        var EditModel = JsonConvert.SerializeObject(AdditionVoucherModel);
                        var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/AdditionVoucher/EditAdditionVoucher", content);
                    }
                    if (result.IsSuccessStatusCode)
                    {
                        ShowConfirm = true;
                    }
                }
                else
                {
                    SelectTypeChecker = true;
                }
            }
            else
            {
                SelectStoreChecker = true;
            }
        }
        void AddNewRow()
        {
            int IdentifierCount = sItemList.Count;
            sItemModel = new AdditionVoucherItemVM { Identifer = IdentifierCount + 1 };
            sItemList.Add(sItemModel);
            StateHasChanged();
        }
        void RemoveRow(AdditionVoucherItemVM row)
        {
            sItemList.Remove(row);
            StateHasChanged();
        }
        protected async Task GetItemStore()
        {
            string uri = "/api/Items/GetItemStore?StoreId=" + AdditionVoucherModel.StoreId;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            ItemStoreList = JsonConvert.DeserializeObject<List<Item_StoreVM>>(vMs["data"].ToString());
        }
        protected async Task GetItemDataByCode(AdditionVoucherItemVM row)
        {
            HttpResponseMessage result = new HttpResponseMessage();
            LookupItem SelectedITem = ItemList.Find(x => (x.BoxCode == row.Box_ParCode || x.GuaranteeParCode == row.Box_ParCode || x.InterationalParCode == row.Box_ParCode || x.Code == row.Box_ParCode || x.ParCodeNote == row.Box_ParCode || x.SupplierParCode == row.Box_ParCode));
            ItemImg = SelectedITem.ItemImg ?? "";
            row.ItemId = SelectedITem.ID;
            row.ItemName = SelectedITem.Name;
            row.Quantity = null;
            row.BoxCode = SelectedITem.BoxCode ?? null;
            row.SheilfNo = ItemStoreList?.Find(x => x.ItemId == row.ItemId)?.SheilfNo;
            StateHasChanged();
            //check for save
            if (AdditionVoucherModel.SerialNumber != 0)
            {
                if (AdditionVoucherModel.StoreId != 0 && AdditionVoucherModel.StoreId != null)
                {
                    //if (AdditionVoucherModel.Name != "0" && AdditionVoucherModel.Name != "" && AdditionVoucherModel.Name != null)
                    if (clients.Name != "0" && clients.Name != "" && clients.Name != null)
                    {
                        AdditionVoucherModel.AdditionVoucherItem = sItemList;
                        //AdditionVoucherModel.UserType = Client_Supplier_List.Find(x => x.Name == AdditionVoucherModel.Name).Type;
                        // AdditionVoucherModel.Client_Supplier_Id = Client_Supplier_List.Find(x => x.Name == AdditionVoucherModel.Name).ID;
                        AdditionVoucherModel.Client_Supplier_Id = Client_Supplier_List.Find(x => x.Name == clients.Name).ID;
                        AdditionVoucherModel.UserType = Client_Supplier_List.Find(x => x.Name == clients.Name).Type;
                        if (FirstAdd == false)
                        {
                            FirstAdd = true;
                            var AddModel = JsonConvert.SerializeObject(AdditionVoucherModel);
                            var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                            result = await Http.PostAsync("api/AdditionVoucher/AddAdditionVoucher", content);
                            var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                            AdditionVoucherModel.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                        }
                        else
                        {
                            AdditionVoucherModel.AdditionVoucherItem = sItemList;
                            var EditModel = JsonConvert.SerializeObject(AdditionVoucherModel);
                            var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                            result = await Http.PostAsync("api/AdditionVoucher/EditAdditionVoucher", content);
                        }
                        StateHasChanged();
                    }
                    else
                    {
                        SelectTypeChecker = true;
                    }
                }
                else
                {
                    SelectStoreChecker = true;
                }
            }
        }

        protected async Task GetItemData(AdditionVoucherItemVM row)
        {
            //  LookupItem SelectedITem = ItemList.Find(x => x.ID == row.ItemId);
            LookupItem SelectedITem = ItemList.Find(x => x.Name == row.ItemName);
            ItemImg = SelectedITem.ItemImg ?? "";
            row.Box_ParCode = SelectedITem.Code;
            row.Quantity = null;
            row.BoxCode = SelectedITem.BoxCode ?? null;
            row.ItemId = SelectedITem.ID;
            row.SheilfNo = ItemStoreList?.Find(x => x.ItemId == row.ItemId)?.SheilfNo;
            StateHasChanged();
            HttpResponseMessage result = new HttpResponseMessage();
            //check for save
            if (AdditionVoucherModel.SerialNumber != 0)
            {
                if (AdditionVoucherModel.StoreId != 0 && AdditionVoucherModel.StoreId != null)
                {
                    //if (AdditionVoucherModel.Name != "0" && AdditionVoucherModel.Name != "" && AdditionVoucherModel.Name != null)
                        if (clients.Name != "0" && clients.Name != "" && clients.Name != null)
                        {
                        AdditionVoucherModel.AdditionVoucherItem = sItemList;
                        //AdditionVoucherModel.UserType = Client_Supplier_List.Find(x => x.Name == AdditionVoucherModel.Name).Type;
                        // AdditionVoucherModel.Client_Supplier_Id = Client_Supplier_List.Find(x => x.Name == AdditionVoucherModel.Name).ID;
                        AdditionVoucherModel.Client_Supplier_Id = Client_Supplier_List.Find(x => x.Name == clients.Name).ID;
                        AdditionVoucherModel.UserType = Client_Supplier_List.Find(x => x.Name == clients.Name).Type;
                        if (FirstAdd == false)
                        {
                            FirstAdd = true;
                            var AddModel = JsonConvert.SerializeObject(AdditionVoucherModel);
                            var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                            result = await Http.PostAsync("api/AdditionVoucher/AddAdditionVoucher", content);
                            var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                            AdditionVoucherModel.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                        }
                        else
                        {
                            AdditionVoucherModel.AdditionVoucherItem = sItemList;
                            var EditModel = JsonConvert.SerializeObject(AdditionVoucherModel);
                            var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                            result = await Http.PostAsync("api/AdditionVoucher/EditAdditionVoucher", content);
                        }
                        StateHasChanged();
                    }
                    else
                    {
                        SelectTypeChecker = true;
                    }
                }
                else
                {
                    SelectStoreChecker = true;
                }
            }

        }

        async Task ReloadItemsList(AdditionVoucherItemVM row)
        {
            row.ItemName = String.Empty;
            row.Box_ParCode = String.Empty;
            row.SheilfNo = String.Empty;
            row.BoxCode = String.Empty;

            var ItemListReturn = await Http.GetAsync("api/Items/GetItem");
            var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
            ItemList = JsonConvert.DeserializeObject<List<LookupItem>>(ItemvMs["data"].ToString());


        }

        async Task ReloadClientsList()
        {
            clients.Name = String.Empty;

            var ListReturn = await Http.GetAsync("api/Client/GetClients");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            Client_Supplier_List = JsonConvert.DeserializeObject<List<LookupKeyValueVM>>(vMs["data"].ToString());

        }
    }
}