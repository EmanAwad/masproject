﻿using MasProject.Domain.ViewModel.Hierarchy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using static MasProject.Domain.Enums.Enums;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel.Items;
using MasProject.Client.Shared;
using System;
using Microsoft.JSInterop;

namespace MasProject.Client.Pages.StoreTransaction.SalesBills
{
    public partial class SalesBillConfirm
    {
        SalesBillsVM BillsModel = new SalesBillsVM();
        string ReportResult = "";
        bool ShowConfirm = false;
        protected override async Task OnInitializedAsync()
        {
            var querystring = ExtensionMethods.QueryString(navigationManager);
            BillsModel.ID = int.Parse(querystring["ID"]);

            string uri = "/api/SalesBill/SalesBillsReport?Id=" + BillsModel.ID;
            ReportResult = uri;
        }
        protected async Task ReviewPurchasesBill()
        {
            HttpResponseMessage result = new HttpResponseMessage();

            var AddModel = JsonConvert.SerializeObject(BillsModel);
            var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
            result = await Http.PostAsync("api/SalesBill/ConfirmSalesBills", content);

            if (result.IsSuccessStatusCode)
            {
                StateHasChanged();
                ShowConfirm = true;
            }

        }
        protected async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        }
    }
}