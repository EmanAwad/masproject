﻿using MasProject.Domain.ViewModel.Hierarchy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using static MasProject.Domain.Enums.Enums;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel.Items;
using MasProject.Client.Shared;
using System;
using Microsoft.JSInterop;

namespace MasProject.Client.Pages.StoreTransaction.SalesBills
{
    public partial class SalesBillView
    {
        SalesBillsVM BillsModel = new SalesBillsVM();
        string ReportResult = "";
        protected override async Task OnInitializedAsync()
        {
            var querystring = ExtensionMethods.QueryString(navigationManager);
            BillsModel.ID = int.Parse(querystring["ID"]);

            string uri = "/api/SalesBill/SalesBillsReport?Id=" + BillsModel.ID;
            ReportResult = uri;
        }

    }
}