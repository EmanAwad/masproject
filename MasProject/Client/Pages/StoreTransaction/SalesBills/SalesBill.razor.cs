﻿using MasProject.Domain.ViewModel.Hierarchy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel.Items;
using MasProject.Client.Shared;
using Microsoft.JSInterop;
using System;
using System.Linq;
using Radzen.Blazor;

namespace MasProject.Client.Pages.StoreTransaction.SalesBills
{
    public partial class SalesBill
    {
        private PageTitle PageTitle;
        bool ShowDelete = false;
        List<SalesBillsVM> SalesList = new List<SalesBillsVM>();
        //List<SalesBillsVM> TempSalesList = new List<SalesBillsVM>();
        //SalesBillsVM SalesBills = new SalesBillsVM();
        int SalesBillID;
      //  RadzenGrid<SalesBillsVM> grid;
        //int ClientId;
        //int SerialNumber;
        ClientsVM clients = new ClientsVM();
        List<LookupKeyValueVM> ClientList = new List<LookupKeyValueVM>();
        List<BranchVM> BranchList = new List<BranchVM>();

        SalesBillsVM SalesModel = new SalesBillsVM();
        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        {
            var ListReturn = await Http.GetAsync("api/SalesBills/GetSalesBills");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            BillsTbl<SalesBillsVM> ReturnModel = JsonConvert.DeserializeObject<BillsTbl<SalesBillsVM>>(vMs["data"].ToString());
            SalesList = ReturnModel.BillsList;
            BranchList = ReturnModel.SearchItems.BranchList;
            ClientList = ReturnModel.SearchItems.ClientList;
        }
        protected async Task AddNewSalesBill()
        {
            //await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/SalesBillAdd", "جارى التحميل", 1000);
            await jsRuntime.InvokeAsync<object>("open", "/SalesBillAdd", "_blank");
        }
        protected async Task EditOldSalesBill(int ID)//, string Type+ "&Type=" + Type
        {
            //await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/SalesBillEdit?ID=" + ID, "جارى التحميل", 1000);
            await jsRuntime.InvokeAsync<object>("open", "/SalesBillEdit?ID=" + ID , "_blank");
        }
        protected async Task ViewOldSalesBill(int ID)//, string Type//+"&Type="+Type
        {
            await jsRuntime.InvokeAsync<object>("open", "/SalesBillView?ID=" + ID, "_blank");
        }

        protected async Task ConfirmSalesBill(int ID)
        {
            await jsRuntime.InvokeAsync<object>("open", "/SalesBillConfirm?ID=" + ID, "_blank");
        }
        protected async Task RevirewSalesBill(int ID)
        {
            await jsRuntime.InvokeAsync<object>("open", "/SalesBillReview?ID=" + ID, "_blank");
        }
        protected void DeleteSalesBill(int Id)//, int serialNumber, int Clientid)
        {
            SalesBillID = Id;
            //ClientId = Clientid;
            //SerialNumber = serialNumber;
            ShowDelete = true;
        }
        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                string uri = "/api/SalesBills/DeleteSalesBills?ID=" + SalesBillID;// + "&SerialNumber=" + SerialNumber + "&ClientId=" +ClientId;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
            ShowDelete = false;

        }

        async Task ReloadClientsList()
        {
            clients.Name = String.Empty;

            var ListReturn = await Http.GetAsync("api/Client/GetClients");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ClientList = JsonConvert.DeserializeObject<List<LookupKeyValueVM>>(vMs["data"].ToString());

        }

        protected async Task GetAllByClient()
        {
            SalesModel.ClientId = ClientList.Find(x => x.Name == clients.Name).ID;
            if (SalesModel.ClientId != 0)
            {
                string uri = "/api/SalesBills/GetSalesByClient?ClientId=" + SalesModel.ClientId;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                SalesList = JsonConvert.DeserializeObject<List<SalesBillsVM>>(vMs["data"].ToString());
            }
            else
            {
                var ListReturn = await Http.GetAsync("api/SalesBills/GetSalesBills");
                var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
                SalesList = JsonConvert.DeserializeObject<List<SalesBillsVM>>(vMs["data"].ToString());
            }

            StateHasChanged();
            //SalesModel.ClientId = ClientList.Find(x => x.Name == clients.Name).ID;
            //if (SalesModel.ClientId != 0)
            //{
            //    SalesList = SalesList.Where(x => x.ClientId == SalesModel.ClientId).ToList();
            //}
            //else
            //{
            //    SalesList = TempSalesList;
            //}
        }
        //protected async Task GetAllByBranch()
        //{
        //    //if (SalesModel.BranchId != 0)
        //    //{
        //    //    SalesList = SalesList.Where(x => x.BranchId == SalesModel.BranchId).ToList();
        //    //}
        //    //else
        //    //{
        //    //    SalesList = TempSalesList;
        //    //}
        //    //SalesList = SalesList.Where(x => x.BranchId == SalesModel.BranchId).ToList();
        //    string SalesBranch = "/api/SalesBills/GetSalesByBranch?BranchId=" + SalesModel.BranchId;
        //    HttpResponseMessage SalesBranchReturn = await Http.GetAsync(SalesBranch);
        //    var SalesBranchVMs = JObject.Parse(SalesBranchReturn.Content.ReadAsStringAsync().Result);
        //    SalesList = JsonConvert.DeserializeObject<List<SalesBillsVM>>(SalesBranchVMs["data"].ToString());
        //}

        protected async Task GetAllByBranch()
        {

            if (SalesModel.BranchId != 0)
            {
                string uri = "/api/SalesBills/GetSalesByBranch?BranchId=" + SalesModel.BranchId;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                SalesList = JsonConvert.DeserializeObject<List<SalesBillsVM>>(vMs["data"].ToString());
            }
            else
            {
                var ListReturn = await Http.GetAsync("api/SalesBills/GetSalesBills");
                var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
                SalesList = JsonConvert.DeserializeObject<List<SalesBillsVM>>(vMs["data"].ToString());
            }


            StateHasChanged();
        }

        //protected async Task GetAllByDate()
        //{
        //    if (SalesModel.Date != null)
        //    {
        //        string uri = "/api/SalesBills/GetSalesByDate?date=" + SalesModel.Date;
        //        HttpResponseMessage response = await Http.GetAsync(uri);
        //        var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
        //        SalesList = JsonConvert.DeserializeObject<List<SalesBillsVM>>(vMs["data"].ToString());
        //    }
        //    else
        //    {
        //        var ListReturn = await Http.GetAsync("api/SalesBills/GetSalesBills");
        //        var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
        //        SalesList = JsonConvert.DeserializeObject<List<SalesBillsVM>>(vMs["data"].ToString());
        //    }


        //    StateHasChanged();

        //}
    }
}