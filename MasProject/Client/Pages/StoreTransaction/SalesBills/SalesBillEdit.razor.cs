﻿using MasProject.Domain.ViewModel.Hierarchy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using static MasProject.Domain.Enums.Enums;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel.Items;
using MasProject.Client.Shared;
using System;
using Microsoft.JSInterop;

namespace MasProject.Client.Pages.StoreTransaction.SalesBills
{
    public partial class SalesBillEdit
    {
       // private PageTitle PageTitle;
        bool ShowConfirm = false;
        SalesItemsVM sItemModel = new SalesItemsVM();
        SalesSellExpensesVM sExpenseModel = new SalesSellExpensesVM();
        List<SalesSellExpensesVM> ExpenseList = new List<SalesSellExpensesVM>();
        SalesBillsVM SalesModel = new SalesBillsVM();
        List<StoreVM> StoreList = new List<StoreVM>();
        List<LookupKeyValueVM> ClientList = new List<LookupKeyValueVM>();
        List<DealTypeVM> DealTypeList = new List<DealTypeVM>();
        List<LookupItem> ItemList = new List<LookupItem>();
        List<SellingExpensesVM> ExpensesList = new List<SellingExpensesVM>();
        List<BranchVM> BranchList = new List<BranchVM>();
        List<SalesBillStoresVM> SalesBillStoresList = new List<SalesBillStoresVM>();
        SalesBillStoresVM SalesBillStoresModel = new SalesBillStoresVM();
        ClientsVM clients = new ClientsVM();
        string TotalInWords = "";
        string TotalInWordsTax = "";
        bool ShowTaxPart = false;
        int FlagTypeID = 0;
        string ItemImg = "";
     //   bool IsDisabled = false;
        bool ShowImg = false;
        bool SelectDealTypeChecker = false;
        bool SelectClientChecker = false;
        bool ItemCodeChecker = false;
        protected override async Task OnInitializedAsync()
        {
            var querystring = ExtensionMethods.QueryString(navigationManager);
            var id = querystring["ID"];
            //await BindList();
            await EditOldSalesBill(int.Parse(id));
            //var Type = querystring["Type"];
            //if (Type == "View")
            //{
            //    //disable controls
            //    IsDisabled = true;
            //    PageTitle.Title = "عرض فاتورة البيع";
            //}
            //else
            //{
            //    IsDisabled = false;
            //    PageTitle.Title = "تعديل فاتورة البيع";
            //}
        }

        protected async Task TaxPart()
        {
            if (FlagTypeID == 1)
            {
                SalesModel.FlagType = true;
                ShowTaxPart = true;
                //get 14% from otherconstant where id=1
                //calculate addtions amount
                if (SalesModel.AddtionTax == 0)
                {
                    string uri = "/api/OtherConstant/GetOtherConstantRatio?Id=1";
                    HttpResponseMessage response = await Http.GetAsync(uri);
                    var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                    SalesModel.AddtionTax = JsonConvert.DeserializeObject<decimal>(vMs["data"].ToString());
                }
                RecalculateTotal();
            }
            else
            {
                SalesModel.FlagType = false;
                ShowTaxPart = false;
            }
        }

        void ChangeDealType()
        {
            //loop on items for delete price and
            foreach (var BillStore in SalesBillStoresList)
            {
                foreach (var row in BillStore.SalesItem)
                {
                    if (row.ItemId != 0)
                    {
                        LookupItem SelectedITem = ItemList.Find(x => x.ID == row.ItemId);
                        ItemImg = SelectedITem.ItemImg;
                        row.Box_ParCode = SelectedITem.Code;
                        row.Price = GetPriceFromDealType((int)SalesModel.DealTypeId, SelectedITem);
                    }
                    if (row.Quantity != 0 && row.Quantity != null)
                    {
                        row.Total = row.Price * row.Quantity;
                    }
                }
            }
            RecalculateTotal();
        }
        void ChangeItemQuantity(int BillStoresIdentifer)
        {
            RecalculateTotal();
            AddNewRow(BillStoresIdentifer);
        }

        void RecalculateTotal()
        {
            decimal CalculatedTotal = 0;
            foreach (var BillStore in SalesBillStoresList)
            {
                foreach (var rowitem in BillStore.SalesItem)
                {
                    if (rowitem.Quantity != null && rowitem.Price != null)
                    {
                        rowitem.Total = rowitem.Quantity.Value * rowitem.Price.Value;
                        CalculatedTotal = (decimal)(CalculatedTotal + rowitem.Total);
                    }
                }
            }
            foreach (var rowExpense in ExpenseList)
            {
                if (rowExpense.Amount != null)
                {
                    CalculatedTotal = (decimal)(CalculatedTotal + rowExpense.Amount.Value);
                }
            }
            SalesModel.TotalPrice = CalculatedTotal;
            SalesModel.TotalAfterDiscount = SalesModel.TotalPrice;
            if (SalesModel.DiscountValue != null & SalesModel.DiscountValue != 0 & SalesModel.DiscountPrecentage == 0)
            {
                decimal temp = SalesModel.DiscountValue.Value / SalesModel.TotalPrice.Value;
                SalesModel.DiscountPrecentage = temp * 100;
                SalesModel.TotalAfterDiscount = (SalesModel.TotalPrice == null ? 0 : SalesModel.TotalPrice) - SalesModel.DiscountValue;
            }
            else if (SalesModel.DiscountPrecentage != null & SalesModel.DiscountPrecentage != 0 & SalesModel.DiscountValue == 0)
            {
                decimal temp = (SalesModel.TotalPrice.Value * SalesModel.DiscountPrecentage.Value) / 100;
                SalesModel.DiscountValue = temp;
                SalesModel.TotalAfterDiscount = (SalesModel.TotalPrice == null ? 0 : SalesModel.TotalPrice) - SalesModel.DiscountValue;
            }
            else
            {
                SalesModel.TotalAfterDiscount = (SalesModel.TotalPrice == null ? 0 : SalesModel.TotalPrice) - SalesModel.DiscountValue;
            }
            if (SalesModel.TotalAfterDiscount == null)
            {
                SalesModel.TotalAfterDiscount = SalesModel.TotalPrice;
            }
            SalesModel.TotalAfterDiscount = Math.Round((decimal)SalesModel.TotalAfterDiscount, 0);
            NumberToAlphabetic Obj = new NumberToAlphabetic();
            Obj.ConvertNumbersToArabicAlphabet(SalesModel.TotalAfterDiscount.ToString());

            TotalInWords = Obj.GetNumberAr();
            decimal DeductionAmount = 0;
            decimal AddtionalAmount = 0;
            if (SalesModel.SourceDeduction != 0)
            {
                SalesModel.SourceDeductionAmount = (decimal)(((SalesModel.TotalAfterDiscount == null ? 0 : SalesModel.TotalAfterDiscount) * SalesModel.SourceDeduction) / 100);
                DeductionAmount = SalesModel.SourceDeductionAmount;
            }
            if (SalesModel.AddtionTax != 0)
            {
                SalesModel.AddtionTaxAmount = (decimal)(((SalesModel.TotalAfterDiscount == null ? 0 : SalesModel.TotalAfterDiscount) * SalesModel.AddtionTax) / 100);
                AddtionalAmount = SalesModel.AddtionTaxAmount;
            }
            SalesModel.TotalAfterTax = (decimal)(SalesModel.TotalAfterDiscount - DeductionAmount + AddtionalAmount);
            SalesModel.TotalAfterTax = Math.Round(SalesModel.TotalAfterTax, 0);

            Obj = new NumberToAlphabetic();
            Obj.ConvertNumbersToArabicAlphabet(SalesModel.TotalAfterTax.ToString());
            TotalInWordsTax = Obj.GetNumberAr();
            StateHasChanged();
        }

        //async Task BindList()
        //{
        //    var SalesListReturn = await Http.GetAsync("api/SalesBills/GetListDDL");
        //    var SalesvMs = JObject.Parse(SalesListReturn.Content.ReadAsStringAsync().Result);
        //    SalesModel = JsonConvert.DeserializeObject<SalesBillsVM>(SalesvMs["data"].ToString());
        //    StoreList = SalesModel.StoreList;
        //    DealTypeList = SalesModel.DealTypeList;
        //    ClientList = SalesModel.ClientList;
        //    ItemList = SalesModel.ItemList;
        //    ExpensesList = SalesModel.sellingExpensesList;
        //    BranchList = SalesModel.BranchList;
        //}
        async Task ClientsRenew()
        {
            var ClientListReturn = await Http.GetAsync("api/Client/GetClient");
            var ClientvMs = JObject.Parse(ClientListReturn.Content.ReadAsStringAsync().Result);
            ClientList = JsonConvert.DeserializeObject<List<LookupKeyValueVM>>(ClientvMs["data"].ToString());

        }
        async Task BalancesRenew()
        {
            string priceuri = "/api/ClientsBalance/GetBalance?ClientId=" + SalesModel.ClientId;
            HttpResponseMessage ClientListReturn = await Http.GetAsync(priceuri);
            var ClientvMs = JObject.Parse(ClientListReturn.Content.ReadAsStringAsync().Result);
            SalesModel.ClientBalance = JsonConvert.DeserializeObject<decimal>(ClientvMs["data"].ToString());
        }

        async Task ItemsRenew(SalesItemsVM row)
        {
            var ItemListReturn = await Http.GetAsync("api/Items/GetItem");
            var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
            ItemList = JsonConvert.DeserializeObject<List<LookupItem>>(ItemvMs["data"].ToString());
            //get price for selected items
            //if (SalesModel.DealTypeId != null)
            //{
            //    if (SalesModel.DealTypeId != 0)
            //    {
            //        string priceuri = "/api/Items/GetItemPriceByDealId?DealId=" + SalesModel.DealTypeId + "&ItemId=" + row.ItemId;
            //        HttpResponseMessage priceresponse = await Http.GetAsync(priceuri);
            //        var pricevMs = JObject.Parse(priceresponse.Content.ReadAsStringAsync().Result);
            //        row.Price = JsonConvert.DeserializeObject<decimal>(pricevMs["data"].ToString());

            //    }
            //}
            LookupItem SelectedITem = ItemList.Find(x => x.ID == row.ItemId);
            ItemImg = SelectedITem.ItemImg;
            row.Box_ParCode = SelectedITem.Code;
            if (SalesModel.DealTypeId != null && SalesModel.DealTypeId != 0)
            {
                row.Price = GetPriceFromDealType((int)SalesModel.DealTypeId, SelectedITem);
            }
            else
            {
                SelectDealTypeChecker = true;
            }
            row.Quantity = null;
            row.Total = null;
            RecalculateTotal();
            StateHasChanged();
        }
        async Task ExpensesRenew()
        {
            var ExpensesListReturn = await Http.GetAsync("api/SellingExpenses/GetSellingExpenses");
            var ExpensesvMs = JObject.Parse(ExpensesListReturn.Content.ReadAsStringAsync().Result);
            ExpensesList = JsonConvert.DeserializeObject<List<SellingExpensesVM>>(ExpensesvMs["data"].ToString());
        }
        protected async Task AddSalesBill()
        {
            SalesModel.ClientId = ClientList.Find(x => x.Name == clients.Name).ID;
            HttpResponseMessage result = new HttpResponseMessage();
            //check for save
            if (SalesModel.SerialNumber != 0 & SalesModel.SerialNumber != null)
            {
                if (SalesModel.ClientId != 0)
                {

                    //SalesModel.SalesItem = sItemList;
                    SalesModel.SalesBillStores = SalesBillStoresList;
                    SalesModel.SalesSellExpenses = ExpenseList;
                    var EditModel = JsonConvert.SerializeObject(SalesModel);
                    var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/SalesBills/EditSalesBills", content);

                    if (result.IsSuccessStatusCode)
                    {
                        StateHasChanged();
                    }
                }
                else
                {
                    SelectClientChecker = true;
                }
            }

        }
        protected async Task EditSalesBill()
        {

          //  IsDisabled = true;
            SalesModel.ClientId = ClientList.Find(x => x.Name == clients.Name).ID;
            //SalesModel.SalesItem = sItemList;
            if (SalesModel.ClientId != 0)
            {
                SalesModel.SalesBillStores = SalesBillStoresList;
                SalesModel.SalesSellExpenses = ExpenseList;
                var EditModel = JsonConvert.SerializeObject(SalesModel);
                var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/SalesBills/EditSalesBills", content);
                if (result.IsSuccessStatusCode)
                {
                    if (SalesModel.convert == 1 || SalesModel.IsConverted)
                    {
                        var Convertresult = await Http.PostAsync("api/SalesBills/ConvertSalesBills", content);

                    }
                    ShowConfirm = true;
                }
            }
            else
            {
                SelectClientChecker = true;
            }
        }
        private void ShowImage(SalesItemsVM row)
        {
            ItemImg = row.ItemImg;
            ShowImg = true;
        }
        protected async Task EditOldSalesBill(int ID)
        {

            string uri = "/api/SalesBills/GetSpecificSalesBills?Id=" + ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            SalesModel = JsonConvert.DeserializeObject<SalesBillsVM>(vMs["data"].ToString());
            clients.Name = SalesModel.ClientName;
            SalesBillStoresList = new List<SalesBillStoresVM>();
            SalesBillStoresList = SalesModel.SalesBillStores;
            SalesModel.convert = 0;
            ExpenseList = new List<SalesSellExpensesVM>();
            ExpenseList = SalesModel.SalesSellExpenses;
            StoreList = SalesModel.StoreList;
            DealTypeList = SalesModel.DealTypeList;
            ClientList = SalesModel.ClientList;
            ItemList = SalesModel.ItemList;
            ExpensesList = SalesModel.sellingExpensesList;
            BranchList = SalesModel.BranchList;
            if (SalesModel.FlagType)
            {
                FlagTypeID = 1;
                await TaxPart();
            }
            RecalculateTotal();
            StateHasChanged();
        }
        //void Confirm()
        //{
        //    ShowConfirm = false;
        //    IsDisabled = false;
        //}
        protected async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        }
        async Task GetSerialNumber()
        {
            string serailuri = "/api/SalesBills/GenerateSerial?BranchId=" + SalesModel.BranchId;
            HttpResponseMessage serialReturn = await Http.GetAsync(serailuri);
            var serialvms = JObject.Parse(serialReturn.Content.ReadAsStringAsync().Result);
            SalesModel.SerialNumber = JsonConvert.DeserializeObject<int>(serialvms["data"].ToString());
            StateHasChanged();

        }
        protected async Task GetDealType()
        {
            SalesModel.ClientId = ClientList.Find(x => x.Name == clients.Name).ID;
            string uri = "/api/Client/GetDealType?Id=" + SalesModel.ClientId;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            SalesModel.DealTypeId = JsonConvert.DeserializeObject<int>(vMs["data"].ToString());

            string priceuri = "/api/ClientsBalance/GetBalance?ClientId=" + SalesModel.ClientId;
            HttpResponseMessage ClientListReturn = await Http.GetAsync(priceuri);
            var ClientvMs = JObject.Parse(ClientListReturn.Content.ReadAsStringAsync().Result);
            SalesModel.ClientBalance = JsonConvert.DeserializeObject<decimal>(ClientvMs["data"].ToString());

            string Taxuri = "/api/Client/GetTaxFile?Id=" + SalesModel.ClientId;
            HttpResponseMessage TaxReturn = await Http.GetAsync(Taxuri);
            var TaxvMs = JObject.Parse(TaxReturn.Content.ReadAsStringAsync().Result);
            ClientsVM TempFile = JsonConvert.DeserializeObject<ClientsVM>(TaxvMs["data"].ToString());
            SalesModel.TaxFileNumber = TempFile.Taxfile;

            StateHasChanged();
        }
        void AddNewRow(int? BillStoresIdentifer)
        {
            //get store row 
            var tempList = SalesBillStoresList.Find(x => x.Identifer == BillStoresIdentifer).SalesItem;
            int IdentifierCount = tempList.Count;
            sItemModel = new SalesItemsVM { Identifer = IdentifierCount + 1, };
            tempList.Add(sItemModel);
            StateHasChanged();
        }
        void RemoveRow(SalesItemsVM row, int? BillStoresIdentifer)
        {
            var tempList = SalesBillStoresList.Find(x => x.Identifer == BillStoresIdentifer).SalesItem;
            tempList.Remove(row);
            StateHasChanged();
        }
        void AddNewExpenseRow()
        {
            int IdentifierCount = ExpenseList.Count;
            sExpenseModel = new SalesSellExpensesVM { Identifer = IdentifierCount + 1, };
            ExpenseList.Add(sExpenseModel);
            StateHasChanged();
        }
        void RemoveExpenseRow(SalesSellExpensesVM row)
        {
            ExpenseList.Remove(row);
            StateHasChanged();
        }
        protected async Task GetItemDataByCode(SalesItemsVM row)
        {
            //try to not get item if empty//not working
            //if (row.Box_ParCode == ""&& row.Box_ParCode==null)
            //{
            //    return;
            //}
            LookupItem SelectedITem = ItemList.Find(x => (x.BoxCode == row.Box_ParCode || x.GuaranteeParCode == row.Box_ParCode || x.InterationalParCode == row.Box_ParCode || x.Code == row.Box_ParCode || x.ParCodeNote == row.Box_ParCode || x.SupplierParCode == row.Box_ParCode));
            if (SelectedITem != null)
            {
                //new check 30-01-2021
                if (row.ItemId == SelectedITem.ID)
                {
                    return;
                }
                ItemImg = SelectedITem.ItemImg ?? "";
                row.ItemId = SelectedITem.ID;
                row.ItemName = SelectedITem.Name;
                if (SalesModel.DealTypeId != null && SalesModel.DealTypeId != 0)
                {
                    row.Price = GetPriceFromDealType((int)SalesModel.DealTypeId, SelectedITem);
                }
                else
                {
                    SelectDealTypeChecker = true;
                }
                row.Quantity = null;
                row.Total = null;
                StateHasChanged();
                HttpResponseMessage result = new HttpResponseMessage();
                //check for save
                if (SalesModel.SerialNumber != 0)
                {
                    if (SalesModel.ClientId != 0)
                    {

                        //SalesModel.SalesItem = sItemList;
                        SalesModel.SalesBillStores = SalesBillStoresList;
                        var EditModel = JsonConvert.SerializeObject(SalesModel);
                        var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                        result = await Http.PostAsync("api/SalesBills/EditSalesBills", content);

                        if (result.IsSuccessStatusCode)
                        {

                            StateHasChanged();
                        }
                    }
                    else
                    {
                        SelectClientChecker = true;
                    }
                }
            }
            else
            {
                ItemCodeChecker = true;
            }
        }

        protected async Task GetItemData(SalesItemsVM row)
        {
            // LookupItem SelectedITem = ItemList.Find(x => x.ID == row.ItemId);
            LookupItem SelectedITem = ItemList.Find(x => x.Name == row.ItemName);
            ItemImg = SelectedITem.ItemImg;
            row.Box_ParCode = SelectedITem.Code;
            row.ItemId = SelectedITem.ID;
            if (SalesModel.DealTypeId != null && SalesModel.DealTypeId != 0)
            {
                row.Price = GetPriceFromDealType((int)SalesModel.DealTypeId, SelectedITem);
            }
            else
            {
                SelectDealTypeChecker = true;
            }
            row.Quantity = null;
            row.Total = null;
            StateHasChanged();
            HttpResponseMessage result = new HttpResponseMessage();
            //check for save
            if (SalesModel.SerialNumber != 0 & SalesModel.SerialNumber != null)
            {
                if (SalesModel.ClientId != 0)
                {

                    //SalesModel.SalesItem = sItemList;
                    SalesModel.SalesBillStores = SalesBillStoresList;
                    SalesModel.SalesSellExpenses = ExpenseList;
                    var EditModel = JsonConvert.SerializeObject(SalesModel);
                    var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/SalesBills/EditSalesBills", content);

                    if (result.IsSuccessStatusCode)
                    {
                        StateHasChanged();
                    }
                }
                else
                {
                    SelectClientChecker = true;
                }
            }
        }
        private decimal GetPriceFromDealType(int DealTypeId, LookupItem SelectedITem)
        {
            float price = 0;
            switch (DealTypeId)
            {
                case (int)EnPriceTypes.التكلفة:
                    price = SelectedITem.CostPrice;
                    break;
                case (int)EnPriceTypes.الموزع:
                    price = SelectedITem.SupplierPrice;
                    break;
                case (int)EnPriceTypes.الجملة:
                    price = SelectedITem.WholesalePrice;
                    break;
                case (int)EnPriceTypes.البيع:
                    price = SelectedITem.SellingPrice;
                    break;
                case (int)EnPriceTypes.التنفيذ:
                    price = SelectedITem.ExecutionPrice;
                    break;
                case (int)EnPriceTypes.العرض:
                    price = SelectedITem.ShowPrice;
                    break;
                case (int)EnPriceTypes.التجزئة:
                    price = SelectedITem.SellingPrice;
                    break;
                case (int)EnPriceTypes.أخرى:
                    price = SelectedITem.other_Price;
                    break;
            }
            return (decimal)price;
        }
        protected async Task AddNewClient()
        {
            await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/ClientAdd", "جارى التحميل", 1500);
        }
        protected async Task AddNewClientPayment()
        {
            decimal? Total = ShowTaxPart ? SalesModel.TotalAfterTax : SalesModel.TotalAfterDiscount;
            // await jsRuntime.InvokeAsync<object>("open", "/ClientPaymentAdd?ClientName=" + SalesModel.ClientName + "&SerialNumber=" + SalesModel.SerialNumber + "&Total=" + Total + "&Branch=" + SalesModel.BranchId, "_blank");
            await jsRuntime.InvokeAsync<object>("open", "/ClientPaymentAdd?ClientId=" + SalesModel.ClientId + "&SerialNumber=" + SalesModel.SerialNumber + "&Total=" + Total + "&Branch=" + SalesModel.BranchId, "_blank");

        }
        void AddNewStore()
        {
            int IdentifierCount = SalesBillStoresList.Count;
            SalesBillStoresModel = new SalesBillStoresVM { Identifer = IdentifierCount + 1, };
            sItemModel = new SalesItemsVM { Identifer = 1, };
            var TempsItemList = new List<SalesItemsVM>();
            TempsItemList.Add(sItemModel);
            SalesBillStoresModel.SalesItem = TempsItemList;
            //SalesBillStoresModel.Store = StoreList;
            SalesBillStoresList.Add(SalesBillStoresModel);
            StateHasChanged();
        }
        void RemoveNewStore(SalesBillStoresVM Store)
        {
            SalesBillStoresList.Remove(Store);
            StateHasChanged();
        }
        void EraseText(SalesItemsVM row)
        {

            row.ItemName = String.Empty;
            row.Box_ParCode = String.Empty;
            row.Price = 0;
        }

        async Task ReloadItemsList(SalesItemsVM row)
        {
            row.ItemName = String.Empty;
            row.Box_ParCode = String.Empty;
            row.Price = 0;


            var ItemListReturn = await Http.GetAsync("api/Items/GetItem");
            var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
            ItemList = JsonConvert.DeserializeObject<List<LookupItem>>(ItemvMs["data"].ToString());


        }

        async Task ReloadClientsList()
        {
            clients.Name = String.Empty;

            var ListReturn = await Http.GetAsync("api/Client/GetClients");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ClientList = JsonConvert.DeserializeObject<List<LookupKeyValueVM>>(vMs["data"].ToString());

        }
    }
}