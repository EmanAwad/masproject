﻿using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Items;
using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel.StoreTransaction.ExecutionWithDrawals;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using MasProject.Domain.ViewModel;
using static MasProject.Domain.Enums.Enums;

namespace MasProject.Client.Pages.StoreTransaction.ExecutionWithdrawl
{
    public partial class ExecutionWithDrawalsEdit
    {
        private PageTitle PageTitle;
        bool ShowConfirm = false;
        ExecutionWithDrawals_ItemsVM ExItemModel = new ExecutionWithDrawals_ItemsVM();
        List<ExecutionWithDrawals_ItemsVM> ExItemList = new List<ExecutionWithDrawals_ItemsVM>();
        ExecutionithDrawals_BillVM ExecutionModel = new ExecutionithDrawals_BillVM();
        List<StoreVM> StoreList = new List<StoreVM>();
        List<BranchVM> BranchList = new List<BranchVM>();
        List<DealTypeVM> DealTypeList = new List<DealTypeVM>();
        List<LookupItem> ItemList = new List<LookupItem>();
        List<ExecutionWithdrawl_BillStoresVM> BillStoresList = new List<ExecutionWithdrawl_BillStoresVM>();
        ExecutionWithdrawl_BillStoresVM BillStoresModel = new ExecutionWithdrawl_BillStoresVM();
        bool FirstAdd = false;
        bool ShowTaxPart = false;
        string TotalInWords = "";
        string TotalInWordsTax = "";
        int FlagTypeID = 0;
        string ItemImg = "";
        bool ShowImg = false;
        bool IsDisabled = false;
        bool SelectDealTypeChecker = false;
        protected override async Task OnInitializedAsync()
        {
            var querystring = ExtensionMethods.QueryString(navigationManager);
            var id = querystring["ID"];
            await BindList();
            await EditOldExecutionBill(int.Parse(id));
            var Type = querystring["Type"];
            if (Type == "View")
            {
                //disable controls
                IsDisabled = true;
                PageTitle.Title = "عرض مسحوبات التنفيذ";
            }
            else
            {
                IsDisabled = false;
                PageTitle.Title = "تعديل مسحوبات التنفيذ";
            }
        }
        async Task BindList()
        {
            var SalesListReturn = await Http.GetAsync("api/ExecutionWithDrawals/GetListDDL");
            var SalesvMs = JObject.Parse(SalesListReturn.Content.ReadAsStringAsync().Result);
            ExecutionModel = JsonConvert.DeserializeObject<ExecutionithDrawals_BillVM>(SalesvMs["data"].ToString());
            StoreList = ExecutionModel.StoreList;
            DealTypeList = ExecutionModel.DealTypeList;
            ItemList = ExecutionModel.ItemList;
            BranchList = ExecutionModel.BranchList;
            //var StoreListReturn = await Http.GetAsync("api/Store/GetStores");
            //var StorevMs = JObject.Parse(StoreListReturn.Content.ReadAsStringAsync().Result);
            //StoreList = JsonConvert.DeserializeObject<List<StoreVM>>(StorevMs["data"].ToString());

            //var SupListReturn = await Http.GetAsync("api/Branch/GetBranchs");
            //var SupvMs = JObject.Parse(SupListReturn.Content.ReadAsStringAsync().Result);
            //BranchList = JsonConvert.DeserializeObject<List<BranchVM>>(SupvMs["data"].ToString());

            //var GetDealTypeListReturn = await Http.GetAsync("api/DealType/GetDealType");
            //var GetDealTypevMs = JObject.Parse(GetDealTypeListReturn.Content.ReadAsStringAsync().Result);
            //DealTypeList = JsonConvert.DeserializeObject<List<DealTypeVM>>(GetDealTypevMs["data"].ToString());

            //var ItemListReturn = await Http.GetAsync("api/Items/GetItems");
            //var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
            //ItemList = JsonConvert.DeserializeObject<List<LookupItem>>(ItemvMs["data"].ToString());

        }
        //async Task GetSerialNumber()
        //{
        //    string serailuri = "/api/ExecutionWithDrawals/GenerateSerial?BranchId=" + ExecutionModel.BranchId;
        //    HttpResponseMessage serialReturn = await Http.GetAsync(serailuri);
        //    var serialvms = JObject.Parse(serialReturn.Content.ReadAsStringAsync().Result);
        //    ExecutionModel.SerialNumber = JsonConvert.DeserializeObject<int>(serialvms["data"].ToString());
        //    StateHasChanged();

        //}
        void ChangeDealType()
        {
            //loop on items for delete price and
            foreach (var BillStore in BillStoresList)
            {
                foreach (var row in BillStore.ExItem)
                {
                    if (row.ItemId != 0)
                    {
                        LookupItem SelectedITem = ItemList.Find(x => x.ID == row.ItemId);
                        ItemImg = SelectedITem.ItemImg;
                        row.Box_ParCode = SelectedITem.Code;
                        row.Price = GetPriceFromDealType((int)ExecutionModel.DealTypeId, SelectedITem);
                    }
                    if (row.Quantity != 0 && row.Quantity != null)
                    {
                        row.Total = row.Price * row.Quantity;
                    }
                }
            }
            RecalculateTotal();
            StateHasChanged();
        }
        private void ShowImage(ExecutionWithDrawals_ItemsVM row)
        {
            ItemImg = row.ItemImg;
            ShowImg = true;
        }
        void ChangeItemQuantity(int BillStoresIdentifer)
        {
            RecalculateTotal();
            AddNewRow(BillStoresIdentifer);
        }
        async Task BranchesRenew()
        {
            var SupplierListReturn = await Http.GetAsync("api/Branch/GetBranchs");
            var SuppliervMs = JObject.Parse(SupplierListReturn.Content.ReadAsStringAsync().Result);
            BranchList = JsonConvert.DeserializeObject<List<BranchVM>>(SuppliervMs["data"].ToString());
        }

        //async Task ItemsRenew()
        //{
        //    var ItemListReturn = await Http.GetAsync("api/Items/GetItems");
        //    var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
        //    ItemList = JsonConvert.DeserializeObject<List<ItemVM>>(ItemvMs["data"].ToString());
        //}
        async Task ItemsRenew(ExecutionWithDrawals_ItemsVM row)
        {
            var ItemListReturn = await Http.GetAsync("api/Items/GetItems");
            var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
            ItemList = JsonConvert.DeserializeObject<List<LookupItem>>(ItemvMs["data"].ToString());
            //get price for selected items
            if (ExecutionModel.DealTypeId != null)
            {
                if (ExecutionModel.DealTypeId != 0)
                {
                    string priceuri = "/api/Items/GetItemPriceByDealId?DealId=" + ExecutionModel.DealTypeId + "&ItemId=" + row.ItemId;
                    HttpResponseMessage priceresponse = await Http.GetAsync(priceuri);
                    var pricevMs = JObject.Parse(priceresponse.Content.ReadAsStringAsync().Result);
                    row.Price = JsonConvert.DeserializeObject<decimal>(pricevMs["data"].ToString());
                }
            }
            RecalculateTotal();
            StateHasChanged();
        }
        protected async Task AddExecutionBill()
        {
            HttpResponseMessage result = new HttpResponseMessage();
            //check for save
            if (ExecutionModel.SerialNumber != 0)
            {
                if (FirstAdd == false)
                {
                    var AddModel = JsonConvert.SerializeObject(ExecutionModel);
                    var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/ExecutionWithDrawals/AddExecutionWithDrawals", content);
                    var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    ExecutionModel.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                    FirstAdd = true;
                }
                else
                {
                    // ExecutionModel.ExecutionItems = ExItemList;
                    ExecutionModel.BillStores = BillStoresList;
                    var EditModel = JsonConvert.SerializeObject(ExecutionModel);
                    var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/ExecutionWithDrawals/EditExecutionWithDrawals", content);
                }
            }
            if (result.IsSuccessStatusCode)
            {
                StateHasChanged();
            }
        }
        protected async Task EditExecutionBill()
        {
            IsDisabled = true;
            //ExecutionModel.ExecutionItems = ExItemList;
            ExecutionModel.BillStores = BillStoresList;
            var EditModel = JsonConvert.SerializeObject(ExecutionModel);
            var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
            var result = await Http.PostAsync("api/ExecutionWithDrawals/EditExecutionWithDrawals", content);
            if (result.IsSuccessStatusCode)
            {
               if (ExecutionModel.convert == 1 || ExecutionModel.IsConverted)
               {
                    var Convertresult = await Http.PostAsync("api/ExecutionWithDrawals/ConvertExecutionWithDrawals", content);

               }
              ShowConfirm = true;
           }
        }

        protected async Task EditOldExecutionBill(int ID)
        {
            FirstAdd = true;
            string uri = "/api/ExecutionWithDrawals/GetSpecificExecutionWithDrawal?Id=" + ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            ExecutionModel = JsonConvert.DeserializeObject<ExecutionithDrawals_BillVM>(vMs["data"].ToString());
            //ExItemList = new List<ExecutionWithDrawals_ItemsVM>();
            BillStoresList = new List<ExecutionWithdrawl_BillStoresVM>();
            BillStoresList = ExecutionModel.BillStores;
            ExecutionModel.convert = 0;
            if (ExecutionModel.FlagType)
            {
                FlagTypeID = 1;
                await TaxPart();
            }
            RecalculateTotal();
            StateHasChanged();
        }
        protected async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        }
        //protected async Task Confirm()
        //{
        //        ShowConfirm = false;
        //    IsDisabled = false;
        //}
        void RemoveRow(ExecutionWithDrawals_ItemsVM row, int? BillStoresIdentifer)
        {
            var tempList = BillStoresList.Find(x => x.Identifer == BillStoresIdentifer).ExItem;
            tempList.Remove(row);
            StateHasChanged();
        }
        protected async Task TaxPart()
        {
            if (FlagTypeID == 1)
            {
                ExecutionModel.FlagType = true;
                ShowTaxPart = true;
                //get 14% from otherconstant where id=1
                //calculate addtions amount
                if (ExecutionModel.AddtionTax == 0)
                {
                    string uri = "/api/OtherConstant/GetOtherConstantRatio?Id=1";
                    HttpResponseMessage response = await Http.GetAsync(uri);
                    var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                    ExecutionModel.AddtionTax = JsonConvert.DeserializeObject<decimal>(vMs["data"].ToString());
                }
                RecalculateTotal();
            }
            else
            {
                ExecutionModel.FlagType = false;
                ShowTaxPart = false;
            }
        }
        void AddNewRow(int? BillStoresIdentifer)
        {
            //get store row 
            var tempList = BillStoresList.Find(x => x.Identifer == BillStoresIdentifer).ExItem;
            int IdentifierCount = tempList.Count;
            ExItemModel = new ExecutionWithDrawals_ItemsVM { Identifer = IdentifierCount + 1, };
            tempList.Add(ExItemModel);
            StateHasChanged();
            //{
            //    int IdentifierCount = ExItemList.Count;
            //    ExItemModel = new ExecutionWithDrawals_ItemsVM { Identifer = IdentifierCount + 1, };
            //    ExItemList.Add(ExItemModel);
        }
        protected async Task GetItemDataByCode(ExecutionWithDrawals_ItemsVM row)
        {
            HttpResponseMessage result = new HttpResponseMessage();
            LookupItem SelectedITem = ItemList.Find(x => (x.BoxCode == row.Box_ParCode || x.GuaranteeParCode == row.Box_ParCode || x.InterationalParCode == row.Box_ParCode || x.Code == row.Box_ParCode || x.ParCodeNote == row.Box_ParCode || x.SupplierParCode == row.Box_ParCode));
            ItemImg = SelectedITem.ItemImg ?? "";
            row.ItemId = SelectedITem.ID;
            row.ItemName = SelectedITem.Name;
            if (ExecutionModel.DealTypeId != null && ExecutionModel.DealTypeId != 0)
            {
                row.Price = GetPriceFromDealType((int)ExecutionModel.DealTypeId, SelectedITem);
            }
            else
            {
                SelectDealTypeChecker = true;
            }
            row.Quantity = null;
            row.Total = null;
            //check for save
            if (ExecutionModel.SerialNumber != 0)
            {
                if (FirstAdd == false)
                {
                    var AddModel = JsonConvert.SerializeObject(ExecutionModel);
                    var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/ExecutionWithDrawals/AddExecutionWithDrawals", content);
                    var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    ExecutionModel.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                    FirstAdd = true;
                }
                else
                {
                    // ExecutionModel.ExecutionItems = ExItemList;
                    ExecutionModel.BillStores = BillStoresList;
                    var EditModel = JsonConvert.SerializeObject(ExecutionModel);
                    var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/ExecutionWithDrawals/EditExecutionWithDrawals", content);
                }
                if (result.IsSuccessStatusCode)
                {
                    //string uri = "/api/Items/GetNationalCodeByID?Id=" + row.ItemId;
                    //HttpResponseMessage response = await Http.GetAsync(uri);
                    //var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                    //row.Box_ParCode = JsonConvert.DeserializeObject<string>(vMs["data"].ToString());
                    StateHasChanged();
                }
            }
        }
        protected async Task GetItemData(ExecutionWithDrawals_ItemsVM row)
        {
            HttpResponseMessage result = new HttpResponseMessage();
            // LookupItem SelectedITem = ItemList.Find(x => x.ID == row.ItemId);
            LookupItem SelectedITem = ItemList.Find(x => x.Name == row.ItemName);
            ItemImg = SelectedITem.ItemImg;
            row.Box_ParCode = SelectedITem.Code;
            row.ItemId = SelectedITem.ID;
            if (ExecutionModel.DealTypeId != null && ExecutionModel.DealTypeId != 0)
            {
                row.Price = GetPriceFromDealType((int)ExecutionModel.DealTypeId, SelectedITem);
            }
            else
            {
                SelectDealTypeChecker = true;
            }
            row.Quantity = null;
            row.Total = null;
            //check for save
            if (ExecutionModel.SerialNumber != 0)
            {
                if (FirstAdd == false)
                {
                    var AddModel = JsonConvert.SerializeObject(ExecutionModel);
                    var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/ExecutionWithDrawals/AddExecutionWithDrawals", content);
                    var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    ExecutionModel.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                    FirstAdd = true;
                }
                else
                {
                    //ExecutionModel.ExecutionItems = ExItemList;
                    ExecutionModel.BillStores = BillStoresList;
                    var EditModel = JsonConvert.SerializeObject(ExecutionModel);
                    var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/ExecutionWithDrawals/EditExecutionWithDrawals", content);
                }
                if (result.IsSuccessStatusCode)
                {
                    //string uri = "/api/Items/GetNationalCodeByID?Id=" + row.ItemId;
                    //HttpResponseMessage response = await Http.GetAsync(uri);
                    //var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                    //row.Box_ParCode = JsonConvert.DeserializeObject<string>(vMs["data"].ToString());

                    //string priceuri = "/api/Items/GetItemPriceByDealId?DealId=" + ExecutionModel.DealTypeId + "&ItemId=" + row.ItemId;
                    //HttpResponseMessage priceresponse = await Http.GetAsync(priceuri);
                    //var pricevMs = JObject.Parse(priceresponse.Content.ReadAsStringAsync().Result);
                    //row.Price = JsonConvert.DeserializeObject<decimal>(pricevMs["data"].ToString());
                    //row.Quantity = null;
                    //row.Total = null;
                    ////new 09-11-2020
                    //string uriImage = "/api/Items/GetItemImage?ItemId=" + row.ItemId;
                    //HttpResponseMessage responseImage = await Http.GetAsync(uriImage);
                    //var vMsImage = JObject.Parse(responseImage.Content.ReadAsStringAsync().Result);
                    //ItemSrc = JsonConvert.DeserializeObject<ItemVM>(vMsImage["data"].ToString());
                    //ItemImg = ItemSrc.ItemImg;
                    StateHasChanged();
                }
            }
        }
        private decimal GetPriceFromDealType(int DealTypeId, LookupItem SelectedITem)
        {
            float price = 0;
            switch (DealTypeId)
            {
                case (int)EnPriceTypes.التكلفة:
                    price = SelectedITem.CostPrice;
                    break;
                case (int)EnPriceTypes.الموزع:
                    price = SelectedITem.SupplierPrice;
                    break;
                case (int)EnPriceTypes.الجملة:
                    price = SelectedITem.WholesalePrice;
                    break;
                case (int)EnPriceTypes.البيع:
                    price = SelectedITem.SellingPrice;
                    break;
                case (int)EnPriceTypes.التنفيذ:
                    price = SelectedITem.ExecutionPrice;
                    break;
                case (int)EnPriceTypes.العرض:
                    price = SelectedITem.ShowPrice;
                    break;
                case (int)EnPriceTypes.التجزئة:
                    price = SelectedITem.SellingPrice;
                    break;
                case (int)EnPriceTypes.أخرى:
                    price = SelectedITem.other_Price;
                    break;
            }
            return (decimal)price;
        }
        void RecalculateTotal()
        {
            decimal CalculatedTotal = 0;
            foreach (var BillStore in BillStoresList)
            {
                foreach (var rowitem in BillStore.ExItem)
                {
                    if (rowitem.Quantity != null && rowitem.Price != null)
                    {
                        rowitem.Total = rowitem.Quantity.Value * rowitem.Price.Value;
                        CalculatedTotal = (decimal)(CalculatedTotal + rowitem.Total);
                    }
                }
            }

            ExecutionModel.TotalPrice = CalculatedTotal;
            ExecutionModel.TotalAfterDiscount = ExecutionModel.TotalPrice;

            if (ExecutionModel.DiscountValue != null & ExecutionModel.DiscountValue != 0 & ExecutionModel.DiscountPrecentage == 0)
            {
                decimal temp = ExecutionModel.DiscountValue.Value / ExecutionModel.TotalPrice.Value;
                ExecutionModel.DiscountPrecentage = temp * 100;
                ExecutionModel.TotalAfterDiscount = (ExecutionModel.TotalPrice == null ? 0 : ExecutionModel.TotalPrice) - ExecutionModel.DiscountValue;
            }
            else if (ExecutionModel.DiscountPrecentage != null & ExecutionModel.DiscountPrecentage != 0 & ExecutionModel.DiscountValue == 0)
            {
                decimal temp = (ExecutionModel.TotalPrice.Value * ExecutionModel.DiscountPrecentage.Value) / 100;
                ExecutionModel.DiscountValue = temp;
                ExecutionModel.TotalAfterDiscount = (ExecutionModel.TotalPrice == null ? 0 : ExecutionModel.TotalPrice) - ExecutionModel.DiscountValue;
            }
            else
            {
                ExecutionModel.TotalAfterDiscount = (ExecutionModel.TotalPrice == null ? 0 : ExecutionModel.TotalPrice) - ExecutionModel.DiscountValue;
            }
            if (ExecutionModel.TotalAfterDiscount == null)
            {
                ExecutionModel.TotalAfterDiscount = ExecutionModel.TotalPrice;
            }
            ExecutionModel.TotalAfterDiscount = Math.Round((decimal)ExecutionModel.TotalAfterDiscount, 0);
            try
            {
                NumberToAlphabetic Obj = new NumberToAlphabetic();
                Obj.ConvertNumbersToArabicAlphabet(ExecutionModel.TotalAfterDiscount.ToString());
                TotalInWords = Obj.GetNumberAr();
            }
            catch (Exception)
            {

            }
           
            decimal DeductionAmount = 0;
            decimal AddtionalAmount = 0;
            if (ExecutionModel.SourceDeduction != 0)
            {
                ExecutionModel.SourceDeductionAmount = (decimal)(((ExecutionModel.TotalAfterDiscount == null ? 0 : ExecutionModel.TotalAfterDiscount) * ExecutionModel.SourceDeduction) / 100);
                DeductionAmount = ExecutionModel.SourceDeductionAmount;
            }
            if (ExecutionModel.AddtionTax != 0)
            {
                ExecutionModel.AddtionTaxAmount = (decimal)(((ExecutionModel.TotalAfterDiscount == null ? 0 : ExecutionModel.TotalAfterDiscount) * ExecutionModel.AddtionTax) / 100);
                AddtionalAmount = ExecutionModel.AddtionTaxAmount;
            }
            ExecutionModel.TotalAfterTax = (decimal)(ExecutionModel.TotalAfterDiscount - DeductionAmount + AddtionalAmount);
            ExecutionModel.TotalAfterTax = Math.Round(ExecutionModel.TotalAfterTax, 0);
            try
            {
                NumberToAlphabetic Obj = new NumberToAlphabetic();
                Obj.ConvertNumbersToArabicAlphabet(ExecutionModel.TotalAfterTax.ToString());
                TotalInWordsTax = Obj.GetNumberAr();
            }
            catch (Exception)
            {
            }
            StateHasChanged();
        }

        //protected void GetItemTotal(ExecutionWithDrawals_ItemsVM row)
        //{
        //    row.Total = row.Quantity * row.Price;
        //    ExecutionModel.TotalPrice = (ExecutionModel.TotalPrice == null ? 0 : ExecutionModel.TotalPrice) + row.Total;
        //    if (ExecutionModel.DiscountPrecentage != 0 && ExecutionModel.DiscountPrecentage != null)
        //    {
        //        decimal temp = (ExecutionModel.TotalPrice.Value * ExecutionModel.DiscountPrecentage.Value) / 100;
        //        ExecutionModel.TotalAfterDiscount = (ExecutionModel.TotalPrice == null ? 0 : ExecutionModel.TotalPrice) - temp;
        //        ExecutionModel.DiscountValue = temp;
        //        ExecutionModel.TotalAfterDiscount = (ExecutionModel.TotalPrice == null ? 0 : ExecutionModel.TotalPrice) - ExecutionModel.DiscountValue;
        //    }
        //    StateHasChanged();
        //}

        //protected void GetItemTotalEdit(ExecutionWithDrawals_ItemsVM row)
        //{
        //    decimal? olditemtotal = (row.Total == null ? 0 : row.Total);
        //    row.Total = row.Quantity * row.Price;
        //    ExecutionModel.TotalPrice = (ExecutionModel.TotalPrice == null ? 0 : ExecutionModel.TotalPrice) + row.Total - olditemtotal;
        //    if (ExecutionModel.DiscountPrecentage != 0 && ExecutionModel.DiscountPrecentage != null)
        //    {
        //        decimal temp = (ExecutionModel.TotalPrice.Value * ExecutionModel.DiscountPrecentage.Value) / 100;
        //        ExecutionModel.TotalAfterDiscount = (ExecutionModel.TotalPrice == null ? 0 : ExecutionModel.TotalPrice) - temp;
        //        ExecutionModel.DiscountValue = temp;
        //        ExecutionModel.TotalAfterDiscount = (ExecutionModel.TotalPrice == null ? 0 : ExecutionModel.TotalPrice) - ExecutionModel.DiscountValue;
        //    }
        //    StateHasChanged();
        //}
        //void CalculateDiscountPrec()
        //{
        //    if (ExecutionModel.DiscountPrecentage != 0 && ExecutionModel.DiscountPrecentage != null)
        //    {
        //        decimal temp = (ExecutionModel.TotalPrice.Value * ExecutionModel.DiscountPrecentage.Value) / 100;
        //        ExecutionModel.TotalAfterDiscount = (ExecutionModel.TotalPrice == null ? 0 : ExecutionModel.TotalPrice) - temp;
        //        ExecutionModel.DiscountValue = temp;
        //    }
        //}

        //void CalculateDiscountAmt()
        //{
        //    if (ExecutionModel.DiscountValue != 0 && ExecutionModel.DiscountValue != null)
        //    {
        //        ExecutionModel.TotalAfterDiscount = (ExecutionModel.TotalPrice == null ? 0 : ExecutionModel.TotalPrice) - ExecutionModel.DiscountValue;
        //        decimal temp = ExecutionModel.DiscountValue.Value / ExecutionModel.TotalPrice.Value;
        //        ExecutionModel.DiscountPrecentage = temp * 100;
        //    }
        //}    
        void AddNewStore()
        {
            int IdentifierCount = BillStoresList.Count;
            BillStoresModel = new ExecutionWithdrawl_BillStoresVM { Identifer = IdentifierCount + 1, };
            ExItemModel = new ExecutionWithDrawals_ItemsVM { Identifer = 1, };
            var TempsItemList = new List<ExecutionWithDrawals_ItemsVM>();
            TempsItemList.Add(ExItemModel);
            BillStoresModel.ExItem = TempsItemList;
            //SalesBillStoresModel.Store = StoreList;
            BillStoresList.Add(BillStoresModel);
            StateHasChanged();
        }
        void RemoveNewStore(ExecutionWithdrawl_BillStoresVM Store)
        {
            BillStoresList.Remove(Store);
            StateHasChanged();
        }
        async Task ReloadItemsList(ExecutionWithDrawals_ItemsVM row)
        {
            row.ItemName = String.Empty;
            row.Box_ParCode = String.Empty;
            row.Price = 0;


            var ItemListReturn = await Http.GetAsync("api/Items/GetItem");
            var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
            ItemList = JsonConvert.DeserializeObject<List<LookupItem>>(ItemvMs["data"].ToString());


        }
    }
}
