﻿using MasProject.Domain.ViewModel.StoreTransaction.ExecutionWithDrawals;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using Microsoft.JSInterop;
using MasProject.Domain.ViewModel.Hierarchy;
using System.Net.Http;

namespace MasProject.Client.Pages.StoreTransaction.ExecutionWithdrawl
{
    public partial class ExecutionWithDrawals
    {
        private PageTitle PageTitle;
        bool ShowDelete = false;
        bool ShowConfirm = false;
        List<ExecutionithDrawals_BillVM> ExecutionList = new List<ExecutionithDrawals_BillVM>();
        List<BranchVM> BranchList = new List<BranchVM>();

        ExecutionithDrawals_BillVM ExecutionModel = new ExecutionithDrawals_BillVM();

        int ExecutionBillID;
        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        {
            //Fill Table
            var ListReturn = await Http.GetAsync("api/ExecutionWithDrawals/GetExecutionWithDrawals");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ExecutionList = JsonConvert.DeserializeObject<List<ExecutionithDrawals_BillVM>>(vMs["data"].ToString());
            //Fill Dropdowns
            var listReturn = await Http.GetAsync("api/ExecutionWithDrawals/GetListDDL");
            var VMs = JObject.Parse(listReturn.Content.ReadAsStringAsync().Result);
            ExecutionModel = JsonConvert.DeserializeObject<ExecutionithDrawals_BillVM>(VMs["data"].ToString());
            BranchList = ExecutionModel.BranchList;
        }
        protected async Task AddNewExecutionBill()
        {
           // await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/ExecutionWithDrawalsAdd", "جارى التحميل", 1200);
            await jsRuntime.InvokeAsync<object>("open", "/ExecutionWithDrawalsAdd", "_blank");
        }


        protected async Task EditOldExecutionBill(int ID, string Type)
        {
           // await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/ExecutionWithDrawalsEdit?ID=" + ID, "جارى التحميل", 1200);
            await jsRuntime.InvokeAsync<object>("open", "/ExecutionWithDrawalsEdit?ID=" + ID + "&Type=" + Type, "_blank");
        }


        protected void Confirm()
        {
            ShowConfirm = false;
        }

        protected void DeleteExecutionBill(int Id)
        {
            ExecutionBillID = Id;
            ShowDelete = true;
        }

        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                string uri = "/api/ExecutionWithDrawals/DeleteExecutionWithDrawals?ID=" + ExecutionBillID;

                var result = await Http.GetAsync(uri);

                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
            ShowDelete = false;
        }
        protected async Task GetAllByBranch()
        {


            string uri = "/api/ExecutionWithDrawals/GetAllByBranch?BranchId=" + ExecutionModel.BranchId;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            ExecutionList = JsonConvert.DeserializeObject<List<ExecutionithDrawals_BillVM>>(vMs["data"].ToString());


            StateHasChanged();
        }
    }
}
