﻿using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Items;
using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel.StoreTransaction.ExecutionWithDrawals;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using MasProject.Domain.ViewModel;
using static MasProject.Domain.Enums.Enums;

namespace MasProject.Client.Pages.StoreTransaction.ExecutionWithdrawl
{
    public partial class ExecutionWithDrawalsAdd
    {
        private PageTitle PageTitle;
        bool ShowConfirm = false;
        ExecutionWithDrawals_ItemsVM ExItemModel = new ExecutionWithDrawals_ItemsVM();
        List<ExecutionWithDrawals_ItemsVM> ExItemList = new List<ExecutionWithDrawals_ItemsVM>();
        ExecutionithDrawals_BillVM ExecutionModel = new ExecutionithDrawals_BillVM();
        List<StoreVM> StoreList = new List<StoreVM>();
        List<BranchVM> BranchList = new List<BranchVM>();
        List<DealTypeVM> DealTypeList = new List<DealTypeVM>();
        List<LookupItem> ItemList = new List<LookupItem>();
        List<ExecutionWithdrawl_BillStoresVM> BillStoresList = new List<ExecutionWithdrawl_BillStoresVM>();
        ExecutionWithdrawl_BillStoresVM BillStoresModel = new ExecutionWithdrawl_BillStoresVM();
        bool FirstAdd = false;
        bool ShowTaxPart = false;
        string TotalInWords = "";
        string TotalInWordsTax = "";
        int FlagTypeID = 0;
        string ItemImg = "";
        //ItemVM ItemSrc = new ItemVM();
        bool ShowImg = false;
        bool SelectDealTypeChecker = false;
        bool IsDisabled = false;
        protected override async Task OnInitializedAsync()
        {
            ItemImg = "";
            FirstAdd = false;
            await BindList();
            //edit 21-09-2020 to save bill first then each item
            BillStoresList = new List<ExecutionWithdrawl_BillStoresVM>();
            BillStoresModel = new ExecutionWithdrawl_BillStoresVM { Identifer = 1, };
            ExItemModel = new ExecutionWithDrawals_ItemsVM { Identifer = 1, };
            ExItemList = new List<ExecutionWithDrawals_ItemsVM>();
            ExItemList.Add(ExItemModel);
            BillStoresModel.ExItem = ExItemList;
            //SalesBillStoresModel.Store = StoreList;
            BillStoresList.Add(BillStoresModel);
            ExecutionModel.BillStores = BillStoresList;
            //ExecutionModel = new ExecutionithDrawals_BillVM();
            //ExecutionModel.ExecutionItems = ExItemList;
            //ExecutionModel.Date = DateTime.Now;
            //ExecutionModel.DiscountPrecentage = 0;
            //ExecutionModel.DiscountValue = 0;
            //var IdReturn = await Http.GetAsync("api/ExecutionWithDrawals/GenerateSerial");
            //var IdvMs = JObject.Parse(IdReturn.Content.ReadAsStringAsync().Result);
            //ExecutionModel.SerialNumber = JsonConvert.DeserializeObject<int>(IdvMs["data"].ToString());

        }
        async Task BindList()
        {
            var SalesListReturn = await Http.GetAsync("api/ExecutionWithDrawals/GetListDDL");
            var SalesvMs = JObject.Parse(SalesListReturn.Content.ReadAsStringAsync().Result);
            ExecutionModel = JsonConvert.DeserializeObject<ExecutionithDrawals_BillVM>(SalesvMs["data"].ToString());
            StoreList = ExecutionModel.StoreList;
            DealTypeList = ExecutionModel.DealTypeList;
            ItemList = ExecutionModel.ItemList;
            BranchList = ExecutionModel.BranchList;
            //var StoreListReturn = await Http.GetAsync("api/Store/GetStores");
            //var StorevMs = JObject.Parse(StoreListReturn.Content.ReadAsStringAsync().Result);
            //StoreList = JsonConvert.DeserializeObject<List<StoreVM>>(StorevMs["data"].ToString());

            //var SupListReturn = await Http.GetAsync("api/Branch/GetBranchs");
            //var SupvMs = JObject.Parse(SupListReturn.Content.ReadAsStringAsync().Result);
            //BranchList = JsonConvert.DeserializeObject<List<BranchVM>>(SupvMs["data"].ToString());

            //var GetDealTypeListReturn = await Http.GetAsync("api/DealType/GetDealType");
            //var GetDealTypevMs = JObject.Parse(GetDealTypeListReturn.Content.ReadAsStringAsync().Result);
            //DealTypeList = JsonConvert.DeserializeObject<List<DealTypeVM>>(GetDealTypevMs["data"].ToString());

            //var ItemListReturn = await Http.GetAsync("api/Items/GetItems");
            //var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
            //ItemList = JsonConvert.DeserializeObject<List<ItemVM>>(ItemvMs["data"].ToString());

        }
        async Task GetSerialNumber()
        {
            string serailuri = "/api/ExecutionWithDrawals/GenerateSerial?BranchId=" + ExecutionModel.BranchId;
            HttpResponseMessage serialReturn = await Http.GetAsync(serailuri);
            var serialvms = JObject.Parse(serialReturn.Content.ReadAsStringAsync().Result);
            ExecutionModel.SerialNumber = JsonConvert.DeserializeObject<int>(serialvms["data"].ToString());
            StateHasChanged();

        }
        async Task BranchesRenew()
        {
            var SupplierListReturn = await Http.GetAsync("api/Branch/GetBranchs");
            var SuppliervMs = JObject.Parse(SupplierListReturn.Content.ReadAsStringAsync().Result);
            BranchList = JsonConvert.DeserializeObject<List<BranchVM>>(SuppliervMs["data"].ToString());
        }

        //async Task ItemsRenew()
        //{
        //    var ItemListReturn = await Http.GetAsync("api/Items/GetItems");
        //    var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
        //    ItemList = JsonConvert.DeserializeObject<List<ItemVM>>(ItemvMs["data"].ToString());
        //}
        async Task ItemsRenew(ExecutionWithDrawals_ItemsVM row)
        {
            var ItemListReturn = await Http.GetAsync("api/Items/GetItems");
            var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
            ItemList = JsonConvert.DeserializeObject<List<LookupItem>>(ItemvMs["data"].ToString());
            //get price for selected items
            if (ExecutionModel.DealTypeId != null)
            {
                if (ExecutionModel.DealTypeId != 0)
                {
                    string priceuri = "/api/Items/GetItemPriceByDealId?DealId=" + ExecutionModel.DealTypeId + "&ItemId=" + row.ItemId;
                    HttpResponseMessage priceresponse = await Http.GetAsync(priceuri);
                    var pricevMs = JObject.Parse(priceresponse.Content.ReadAsStringAsync().Result);
                    row.Price = JsonConvert.DeserializeObject<decimal>(pricevMs["data"].ToString());
                }
            }
            RecalculateTotal();
            StateHasChanged();
        }
        void ChangeDealType()
        {
            //loop on items for delete price and
            foreach (var BillStore in BillStoresList)
            {
                foreach (var row in ExItemList)
                {
                    if (row.ItemId != 0)
                    {
                        LookupItem SelectedITem = ItemList.Find(x => x.ID == row.ItemId);
                        ItemImg = SelectedITem.ItemImg;
                        row.Box_ParCode = SelectedITem.Code;
                        row.Price = GetPriceFromDealType((int)ExecutionModel.DealTypeId, SelectedITem);
                    }
                    if (row.Quantity != 0 && row.Quantity != null)
                    {
                        row.Total = row.Price * row.Quantity;
                    }
                }
            }
            RecalculateTotal();
            StateHasChanged();
        }
        protected async Task AddExecutionBill()
        {
            HttpResponseMessage result = new HttpResponseMessage();
            //check for save
            if (ExecutionModel.SerialNumber != 0)
            {
                if (FirstAdd == false)
                {
                    var AddModel = JsonConvert.SerializeObject(ExecutionModel);
                    var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/ExecutionWithDrawals/AddExecutionWithDrawals", content);
                    var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    ExecutionModel.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                    FirstAdd = true;
                }
                else
                {
                    //ExecutionModel.ExecutionItems = ExItemList;
                    ExecutionModel.BillStores = BillStoresList;
                    var EditModel = JsonConvert.SerializeObject(ExecutionModel);
                    var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/ExecutionWithDrawals/EditExecutionWithDrawals", content);
                }
            }
            if (result.IsSuccessStatusCode)
            {
                StateHasChanged();
            }
        }
        protected async Task EditExecutionBill()
        {
            IsDisabled = true;
            HttpResponseMessage result = new HttpResponseMessage();
            //check for save
            if (ExecutionModel.SerialNumber != 0)
            {
                if (FirstAdd == false)
                {
                    var AddModel = JsonConvert.SerializeObject(ExecutionModel);
                    var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/ExecutionWithDrawals/AddExecutionWithDrawals", content);
                    var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    ExecutionModel.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                    FirstAdd = true;
                }
                else
                {
                    //ExecutionModel.ExecutionItems = ExItemList;
                    ExecutionModel.BillStores = BillStoresList;
                    var EditModel = JsonConvert.SerializeObject(ExecutionModel);
                    var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/ExecutionWithDrawals/EditExecutionWithDrawals", content);
                }
            }
            if (result.IsSuccessStatusCode)
            {
                ShowConfirm = true;
             
            }
        }
        protected async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        }
        //void Confirm()
        //{
        //    //await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        //    //changed 30-10-2020// protected async Task
        //    ShowConfirm = false;
        //    IsDisabled = false;
        //}

        void RemoveRow(ExecutionWithDrawals_ItemsVM row, int? BillStoresIdentifer)
        {
            var tempList = BillStoresList.Find(x => x.Identifer == BillStoresIdentifer).ExItem;
            tempList.Remove(row);
            StateHasChanged();
        }
        protected async Task TaxPart()
        {
            if (FlagTypeID == 1)
            {
                ExecutionModel.FlagType = true;
                ShowTaxPart = true;
                //get 14% from otherconstant where id=1
                //calculate addtions amount
                if (ExecutionModel.AddtionTax == 0)
                {
                    string uri = "/api/OtherConstant/GetOtherConstantRatio?Id=1";
                    HttpResponseMessage response = await Http.GetAsync(uri);
                    var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                    ExecutionModel.AddtionTax = JsonConvert.DeserializeObject<decimal>(vMs["data"].ToString());
                }
                RecalculateTotal();
            }
            else
            {
                ExecutionModel.FlagType = false;
                ShowTaxPart = false;
            }
        }

        void AddNewRow(int? BillStoresIdentifer)
        {
            //get store row 
            var tempList = BillStoresList.Find(x => x.Identifer == BillStoresIdentifer).ExItem;
            int IdentifierCount = tempList.Count;
            ExItemModel = new ExecutionWithDrawals_ItemsVM { Identifer = IdentifierCount + 1, };
            tempList.Add(ExItemModel);
            StateHasChanged();
            //{
            //    int IdentifierCount = ExItemList.Count;
            //    ExItemModel = new ExecutionWithDrawals_ItemsVM { Identifer = IdentifierCount + 1, };
            //    ExItemList.Add(ExItemModel);
        }
        protected async Task GetItemDataByCode(ExecutionWithDrawals_ItemsVM row)
        {
            HttpResponseMessage result = new HttpResponseMessage();
            LookupItem SelectedITem = ItemList.Find(x => (x.BoxCode == row.Box_ParCode || x.GuaranteeParCode == row.Box_ParCode || x.InterationalParCode == row.Box_ParCode || x.Code == row.Box_ParCode || x.ParCodeNote == row.Box_ParCode || x.SupplierParCode == row.Box_ParCode));
            ItemImg = SelectedITem.ItemImg ?? "";
            row.ItemId = SelectedITem.ID;
            row.ItemName = SelectedITem.Name;
            if (ExecutionModel.DealTypeId != null && ExecutionModel.DealTypeId != 0)
            {
                row.Price = GetPriceFromDealType((int)ExecutionModel.DealTypeId, SelectedITem);
            }
            else
            {
                SelectDealTypeChecker = true;
            }
            row.Quantity = null;
            row.Total = null;
            //check for save
            if (ExecutionModel.SerialNumber != 0)
            {
                if (FirstAdd == false)
                {
                    var AddModel = JsonConvert.SerializeObject(ExecutionModel);
                    var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/ExecutionWithDrawals/AddExecutionWithDrawals", content);
                    var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    ExecutionModel.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                    FirstAdd = true;
                }
                else
                {
                    // ExecutionModel.ExecutionItems = ExItemList;
                    ExecutionModel.BillStores = BillStoresList;
                    var EditModel = JsonConvert.SerializeObject(ExecutionModel);
                    var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/ExecutionWithDrawals/EditExecutionWithDrawals", content);
                }
                if (result.IsSuccessStatusCode)
                {
                    //string uri = "/api/Items/GetNationalCodeByID?Id=" + row.ItemId;
                    //HttpResponseMessage response = await Http.GetAsync(uri);
                    //var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                    //row.Box_ParCode = JsonConvert.DeserializeObject<string>(vMs["data"].ToString());
                    StateHasChanged();
                }
            }
        }
        void ChangeItemQuantity(int BillStoresIdentifer)
        {
            RecalculateTotal();
            AddNewRow(BillStoresIdentifer);
        }

        protected async Task GetItemData(ExecutionWithDrawals_ItemsVM row)
        {
            HttpResponseMessage result = new HttpResponseMessage();
            //  LookupItem SelectedITem = ItemList.Find(x => x.ID == row.ItemId);
            LookupItem SelectedITem = ItemList.Find(x => x.Name == row.ItemName);
            ItemImg = SelectedITem.ItemImg;
            row.Box_ParCode = SelectedITem.Code;
            row.ItemId = SelectedITem.ID;
            if (ExecutionModel.DealTypeId != null && ExecutionModel.DealTypeId != 0)
            {
                row.Price = GetPriceFromDealType((int)ExecutionModel.DealTypeId, SelectedITem);
            }
            else
            {
                SelectDealTypeChecker = true;
            }
            row.Quantity = null;
            row.Total = null;
            //check for save
            if (ExecutionModel.SerialNumber != 0)
            {
                if (FirstAdd == false)
                {
                    var AddModel = JsonConvert.SerializeObject(ExecutionModel);
                    var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/ExecutionWithDrawals/AddExecutionWithDrawals", content);
                    var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    ExecutionModel.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                    FirstAdd = true;
                }
                else
                {
                    //ExecutionModel.ExecutionItems = ExItemList;
                    ExecutionModel.BillStores = BillStoresList;
                    var EditModel = JsonConvert.SerializeObject(ExecutionModel);
                    var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/ExecutionWithDrawals/EditExecutionWithDrawals", content);
                }
                if (result.IsSuccessStatusCode)
                {
                     
                    StateHasChanged();
                }
            }
        }
        private decimal GetPriceFromDealType(int DealTypeId, LookupItem SelectedITem)
        {
            float price = 0;
            switch (DealTypeId)
            {
                case (int)EnPriceTypes.التكلفة:
                    price = SelectedITem.CostPrice;
                    break;
                case (int)EnPriceTypes.الموزع:
                    price = SelectedITem.SupplierPrice;
                    break;
                case (int)EnPriceTypes.الجملة:
                    price = SelectedITem.WholesalePrice;
                    break;
                case (int)EnPriceTypes.البيع:
                    price = SelectedITem.SellingPrice;
                    break;
                case (int)EnPriceTypes.التنفيذ:
                    price = SelectedITem.ExecutionPrice;
                    break;
                case (int)EnPriceTypes.العرض:
                    price = SelectedITem.ShowPrice;
                    break;
                case (int)EnPriceTypes.التجزئة:
                    price = SelectedITem.SellingPrice;
                    break;
                case (int)EnPriceTypes.أخرى:
                    price = SelectedITem.other_Price;
                    break;
            }
            return (decimal)price;
        }

        void RecalculateTotal()
        {
            decimal CalculatedTotal = 0;
            foreach (var BillStore in BillStoresList)
            {
                foreach (var rowitem in ExItemList)
                {
                    if (rowitem.Quantity != null && rowitem.Price != null)
                    {
                        rowitem.Total = rowitem.Quantity.Value * rowitem.Price.Value;
                        CalculatedTotal = (decimal)(CalculatedTotal + rowitem.Total);
                    }
                }
            }

            ExecutionModel.TotalPrice = CalculatedTotal;
            ExecutionModel.TotalAfterDiscount = ExecutionModel.TotalPrice;

            if (ExecutionModel.DiscountValue != null & ExecutionModel.DiscountValue != 0 & ExecutionModel.DiscountPrecentage == 0)
            {
                decimal temp = ExecutionModel.DiscountValue.Value / ExecutionModel.TotalPrice.Value;
                ExecutionModel.DiscountPrecentage = temp * 100;
                ExecutionModel.TotalAfterDiscount = (ExecutionModel.TotalPrice == null ? 0 : ExecutionModel.TotalPrice) - ExecutionModel.DiscountValue;
            }
            else if (ExecutionModel.DiscountPrecentage != null & ExecutionModel.DiscountPrecentage != 0 & ExecutionModel.DiscountValue == 0)
            {
                decimal temp = (ExecutionModel.TotalPrice.Value * ExecutionModel.DiscountPrecentage.Value) / 100;
                ExecutionModel.DiscountValue = temp;
                ExecutionModel.TotalAfterDiscount = (ExecutionModel.TotalPrice == null ? 0 : ExecutionModel.TotalPrice) - ExecutionModel.DiscountValue;
            }
            else
            {
                ExecutionModel.TotalAfterDiscount = (ExecutionModel.TotalPrice == null ? 0 : ExecutionModel.TotalPrice) - ExecutionModel.DiscountValue;
            }
            if (ExecutionModel.TotalAfterDiscount == null)
            {
                ExecutionModel.TotalAfterDiscount = ExecutionModel.TotalPrice;
            }
            ExecutionModel.TotalAfterDiscount = Math.Round((decimal)ExecutionModel.TotalAfterDiscount, 0);
            NumberToAlphabetic Obj = new NumberToAlphabetic();
            Obj.ConvertNumbersToArabicAlphabet(ExecutionModel.TotalAfterDiscount.ToString());

            TotalInWords = Obj.GetNumberAr();
            decimal DeductionAmount = 0;
            decimal AddtionalAmount = 0;
            if (ExecutionModel.SourceDeduction != 0)
            {
                ExecutionModel.SourceDeductionAmount = (decimal)(((ExecutionModel.TotalAfterDiscount == null ? 0 : ExecutionModel.TotalAfterDiscount) * ExecutionModel.SourceDeduction) / 100);
                DeductionAmount = ExecutionModel.SourceDeductionAmount;
            }
            if (ExecutionModel.AddtionTax != 0)
            {
                ExecutionModel.AddtionTaxAmount = (decimal)(((ExecutionModel.TotalAfterDiscount == null ? 0 : ExecutionModel.TotalAfterDiscount) * ExecutionModel.AddtionTax) / 100);
                AddtionalAmount = ExecutionModel.AddtionTaxAmount;
            }
            ExecutionModel.TotalAfterTax = (decimal)(ExecutionModel.TotalAfterDiscount - DeductionAmount + AddtionalAmount);
            ExecutionModel.TotalAfterTax = Math.Round(ExecutionModel.TotalAfterTax, 0);

            Obj = new NumberToAlphabetic();
            Obj.ConvertNumbersToArabicAlphabet(ExecutionModel.TotalAfterTax.ToString());
            TotalInWordsTax = Obj.GetNumberAr();
            StateHasChanged();
        }

        
        void AddNewStore()
        {
            int IdentifierCount = BillStoresList.Count;
            BillStoresModel = new ExecutionWithdrawl_BillStoresVM { Identifer = IdentifierCount + 1, };
            ExItemModel = new ExecutionWithDrawals_ItemsVM { Identifer = 1, };
            var TempsItemList = new List<ExecutionWithDrawals_ItemsVM>();
            TempsItemList.Add(ExItemModel);
            BillStoresModel.ExItem = TempsItemList;
            //SalesBillStoresModel.Store = StoreList;
            BillStoresList.Add(BillStoresModel);
            StateHasChanged();
        }
        void RemoveNewStore(ExecutionWithdrawl_BillStoresVM Store)
        {
            BillStoresList.Remove(Store);
            StateHasChanged();
        }
        async Task ReloadItemsList(ExecutionWithDrawals_ItemsVM row)
        {
            row.ItemName = String.Empty;
            row.Box_ParCode = String.Empty;
            row.Price = 0;


            var ItemListReturn = await Http.GetAsync("api/Items/GetItem");
            var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
            ItemList = JsonConvert.DeserializeObject<List<LookupItem>>(ItemvMs["data"].ToString());


        }
    }
}
