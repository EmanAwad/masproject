﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using System;
using MasProject.Domain.ViewModel;
using MasProject.Client.Shared;
using Microsoft.JSInterop;
using MasProject.Domain.ViewModel.Persons;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using MasProject.Domain.ViewModel.Hierarchy;

namespace MasProject.Client.Pages
{
    public partial class Login
    {
        LoginVM model = new LoginVM();
        List<EmployeeVM> UsersList = new List<EmployeeVM>();
        //EmployeeVM employee = new EmployeeVM();
        //BranchVM BranchModel = new BranchVM();
        List<BranchVM> BranchList = new List<BranchVM>();
        protected override async Task OnInitializedAsync()
        {
          await  BindLists();
        }
        async Task BindLists()
        {

            var UsersListReturn = await Http.GetAsync("api/Employee/GetUsers");
            var vMMMMs = JObject.Parse(UsersListReturn.Content.ReadAsStringAsync().Result);
            UsersList = JsonConvert.DeserializeObject<List<EmployeeVM>>(vMMMMs["data"].ToString());
            var BranchListReturn = await Http.GetAsync("api/Branch/GetBranchs");
            var BranchvMs = JObject.Parse(BranchListReturn.Content.ReadAsStringAsync().Result);
            BranchList = JsonConvert.DeserializeObject<List<BranchVM>>(BranchvMs["data"].ToString());


        }

        protected async Task OnValidSubmit()
        {
            HttpResponseMessage result = new HttpResponseMessage();
            var AddModel = JsonConvert.SerializeObject(model);
            var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
            result = await Http.PostAsync("api/Auth/Login", content);
            if (result.IsSuccessStatusCode)
            {
                await jsRuntime.InvokeAsync<object>("open", "/Index", "_self");
            }
        }
    }
}
