﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasProject.Domain.ViewModel.Items;
using System.Net.Http;
using System.Net.Http.Json;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Components;
using System.Text;
using MatBlazor;
using MasProject.Client.Shared;

namespace MasProject.Client.Pages.Items.ItemCollection
{
    public partial class ItemCollection

    {
        private PageTitle PageTitle;
        List<ItemCollectionVM> ItemCollectionList;
        ItemCollectionVM collec = new ItemCollectionVM();
        IEnumerable<ItemCollection> collection; 
#pragma warning disable CS0414 // The field 'ItemCollection.showAddrow' is assigned but its value is never used
        bool showAddrow = false;
#pragma warning restore CS0414 // The field 'ItemCollection.showAddrow' is assigned but its value is never used
#pragma warning disable CS0414 // The field 'ItemCollection.showEditrow' is assigned but its value is never used
        bool showEditrow = false;
#pragma warning restore CS0414 // The field 'ItemCollection.showEditrow' is assigned but its value is never used
#pragma warning disable CS0414 // The field 'ItemCollection.ShowDelete' is assigned but its value is never used
        bool ShowDelete = false;
#pragma warning restore CS0414 // The field 'ItemCollection.ShowDelete' is assigned but its value is never used
#pragma warning disable CS0414 // The field 'ItemCollection.ShowConfirm' is assigned but its value is never used
      bool  ShowConfirm = false;
#pragma warning restore CS0414 // The field 'ItemCollection.ShowConfirm' is assigned but its value is never used
        int CollectionID;
        string ValidateMessageName = "";
        string ValidateMessageArrangement = "";
        bool IsDisabled = false;

        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        {

            var ListReturn = await Http.GetAsync("api/ItemCollection/GetItemCollection");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ItemCollectionList = JsonConvert.DeserializeObject<List<ItemCollectionVM>>(vMs["data"].ToString());
            collection = JsonConvert.DeserializeObject<List<ItemCollection>>(vMs["data"].ToString());
        }
        protected async Task AddItemCollection()
        {
            IsDisabled = true;
            if (ValidateMessageName == "" && ValidateMessageArrangement == "" )
            {
                var AddModel = JsonConvert.SerializeObject(collec);
                var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/ItemCollection/AddItemCollection", content);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                    ShowConfirm = true;
                }
            }
        }
        void AddNewItemCollection()
        {
             ValidateMessageName = "";
             ValidateMessageArrangement = "";
            showAddrow = true;
            showEditrow = false;
            collec = new ItemCollectionVM();
        }
        protected async Task EditOldItemCollection(int ID)
        {
            ValidateMessageName = "";
            ValidateMessageArrangement = "";
            showAddrow = false;
            showEditrow = true;
            string uri = "/api/ItemCollection/GetSpecificItemCollection?Id=" + ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            collec = JsonConvert.DeserializeObject<ItemCollectionVM>(vMs["data"].ToString());
        }

        protected async Task EditItemCollection()
        {
            if (ValidateMessageName == "" && ValidateMessageArrangement == "")
            {
                var EditModel = JsonConvert.SerializeObject(collec);
                var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/ItemCollection/EditItemCollection", content);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                    ShowConfirm = true;
                }
            }
        }

        protected void DeleteItemCollection(int ID)
        {
            CollectionID = ID;
            ShowDelete = true;


        }
        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                string uri = "/api/ItemCollection/DeleteItemCollection?ID=" + CollectionID;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
            ShowDelete = false;
        }
        protected void Cancel()
        {
            showAddrow = false;
            showEditrow = false;
        }
        protected void Confirm()
        {
            ShowConfirm = false;
            showAddrow = false;
            showEditrow = false;
        }
        protected async Task CheckName()

        {
            if (collec.Name != null)
            {

                string uri = "/api/ItemCollection/CheckNameCollection?name=" + collec.Name.Trim() + "&Id=" + collec.ID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateMessageName = "الاسم موجود من قبل";
                }
                else
                {
                    ValidateMessageName = "";
                }
            }
        }
       
        protected async Task CheckArrangement()
        {
            if (collec.arrangement != null)
            {
                string uri = "/api/ItemCollection/CheckArrangement?arrangement=" + collec.arrangement + "&Id=" + collec.ID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateMessageArrangement = "الترتيب موجود من قبل";
                }
                else
                {
                    ValidateMessageArrangement = "";
                }
            }
        }
        //protected void GotoPage()
        //{
        //   // MatTable.PageIndex = Convert.ToInt16(goto) - 1;

        //}

    }
}