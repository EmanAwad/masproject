﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasProject.Domain.ViewModel.Items;
using MasProject.Data.Models.Items;
using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel.Hierarchy;
using System.Net.Http;
using System.Net.Http.Json;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Components;
using System.Text;
using MatBlazor;
using System.IO;
using BlazorInputFile;
using MasProject.Domain.Services.Items;
using Microsoft.JSInterop;
using MasProject.Client.Shared;

namespace MasProject.Client.Pages.Items.Item
{
    public partial class Item
    {
        private PageTitle PageTitle;

        List<ItemVM> ItemList;
        List<Item_StoreVM> ItemStoreList = new List<Item_StoreVM>();
        List<ItemCollectionVM> CollectionList = new List<ItemCollectionVM>();
        List<ItemTypeVM> TypeList = new List<ItemTypeVM>();
        List<SupplierVM> SupplierList = new List<SupplierVM>();
        List<StoreVM> StoreList = new List<StoreVM>();
        ItemVM itm = new ItemVM();
        Item_StoreVM itmstore = new Item_StoreVM();
        List<string> list = new List<string>();
        Item_StoreVM isModel = new Item_StoreVM();
        string status;
        string imageDataURL = "";
        string fileTextContents = "";
        string _PhotoUrl;
        bool dialogOpened = false;
        bool _isedit = false;
        bool SnakBar = false;
        Item tempitem = null;
        int _index = -1;
        bool _isdeleted = false;
        bool showAddrow = false;
        bool showEditrow = false;
        bool ShowConfirm = false;
        bool ShowDelete = false;
        int ItemID;
        string RequiredMessageName = "";
        string ValidateMessageName = "";
        string RequiredMessageNationalParCode = "";
        int StoreID;
        string ValidateMessageNational = "";
        string ValidateMessageInternational = "";
        string ValidateMessageSupplier = "";
        string ValidateMessageBox = "";
        string ValidateMessageGuarantee = "";
        bool ItemUsed = false;
        protected void Confirm()
        {
            ShowConfirm = false;
            showAddrow = false;
            showEditrow = false;
            //  return View("Item");
        }
        bool ShowImg = false;
        IFileListEntry file;

        void HandleSelection(IFileListEntry[] files)
        {
            file = files.FirstOrDefault();
        }

        #region Dialog Methods
        //private void OpenDialog(bool ISEdit)
        //{
        //    _isedit = ISEdit;
        //    if (!_isedit) itm = new ItemVM();
        //    dialogOpened = true;
        //}

        //private void Onclick()
        //{
        //    dialogOpened = false;
        //    if (!_isedit) await this.AddItem(I);
        //    else await this.EditItem(item);
        //}

        //private void CloseDialog()
        //{
        //    dialogOpened = false;
        //    if (Selecteditem != null)
        //    {
        //        item = Selecteditem;
        //        this.EditItem(Selecteditem);
        //    }
        //}

        //   private void SelectionChangedEvent(object itm)
        // {
        //     var Currentitm = (Item)itm;
        //     if (Currentitm != null)
        //     {
        //         item = Currentitm;
        //          Selecteditem = new Item(Currentitm.ItemID, Currentitm.ItemName, Currentitm.ItemGroup, Currentitm.ItemType, Currentitm.Address);
        //      }
        //      else
        //       {
        //         Selecteditem = new Item();
        //     }
        //      if (_isdeleted) this.DeleteItem();
        //   }

        #endregion

        #region Methods
        protected override async Task OnInitializedAsync()
        {


            await BindList();
            await BindLists();
            await BindStoreList();
            //  await JSRuntime.InvokeAsync<object>("AddRow", "classAdd");
        }
        async Task BindList()
        {
            var ListReturn = await Http.GetAsync("api/Items/GetItems");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ItemList = JsonConvert.DeserializeObject<List<ItemVM>>(vMs["data"].ToString());

        }
        protected async Task AddNewItem()
        {
            //    await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/AddItems", "جارى التحميل", 1500);
            await jsRuntime.InvokeAsync<object>("open", "/AddItems", "_blank");
            ItemStoreList = new List<Item_StoreVM>();
            file = null;
            isModel = new Item_StoreVM { Name = "Try" };
            ItemStoreList.Add(isModel);
            ValidateMessageName = "";
            RequiredMessageName = "";
            ValidateMessageNational = "";
            ValidateMessageInternational = "";
            ValidateMessageSupplier = "";
            ValidateMessageBox = "";
            ValidateMessageGuarantee = "";
            //  showAddrow = true;
            //  showEditrow = false;
            itm = new ItemVM();
            itm.ItemStoreList = ItemStoreList;
        }
        protected async Task EditOldItem(int ID)
        {
            //  await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/EditItems?ID=" + ID, "جارى التحميل", 1500);
            await jsRuntime.InvokeAsync<object>("open", "/EditItems?ID=" + ID, "_blank");
        }
        protected async Task AddItem()
        {
            if (ValidateOB())
            {
                //insert image
                string pathlogo = $"/Items/";
                if (!Directory.Exists(pathlogo))
                    Directory.CreateDirectory(pathlogo);
                if (file != null && file.Size > 0)
                {
                    var namelogo = Guid.NewGuid().ToString() + Path.GetExtension(file.Name);
                    var ms = new MemoryStream();
                    await file.Data.CopyToAsync(ms);
                    var contentFile = new MultipartFormDataContent {
                {
                            new ByteArrayContent(ms.GetBuffer()), "\"Items\"", namelogo}
                    };
                    await Http.PostAsync("upload?FolderName=" + "Items", contentFile);

                    itm.ItemImg = "/Images/Items/" + namelogo;
                }
                itm.ItemStoreList = ItemStoreList;
                var AddModel = JsonConvert.SerializeObject(itm);
                var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/Items/AddItem", content);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                    showAddrow = false;
                    ShowConfirm = true;
                }
            }
        }

        protected async Task EditItem()
        {
            if (ValidateOB())
            {
                string pathlogo = $"/Items/";
                if (!Directory.Exists(pathlogo))
                    Directory.CreateDirectory(pathlogo);
                if (file != null && file.Size > 0)
                {
                    var namelogo = Guid.NewGuid().ToString() + Path.GetExtension(file.Name);
                    var ms = new MemoryStream();
                    await file.Data.CopyToAsync(ms);
                    var contentFile = new MultipartFormDataContent {
                {
                            new ByteArrayContent(ms.GetBuffer()), "\"Items\"", namelogo}
                    };
                    await Http.PostAsync("upload?FolderName=" + "Items", contentFile);

                    itm.ItemImg = "/Images/Items/" + namelogo;
                }
                itm.ItemStoreList = ItemStoreList;
                var EditModel = JsonConvert.SerializeObject(itm);

                var content = new StringContent(EditModel, Encoding.UTF8, "application/json");

                var result = await Http.PostAsync("api/Items/EditItem", content);

                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                    showEditrow = false;
                    ShowConfirm = true;
                }
            }
        }
        protected void DeleteItem(int Id)
        {
            ItemID = Id;
            ShowDelete = true;
        }
        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                string uri = "/api/Items/DeleteItem?ID=" + ItemID;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
                else
                {
                    ItemUsed = true;
                }
            }
            ShowDelete = false;
        }



        protected async Task Hide(int ID)
        {

            string uri = "/api/Items/HideItem?ID=" + ItemID;
            var result = await Http.GetAsync(uri);
            if (result.IsSuccessStatusCode)
            {
                await BindList();
            }

        }


        //async Task HandleSelection(IFileListEntry[] files)
        //{

        //    var file = files.FirstOrDefault();
        //    if ( file != null)
        //    {
        //        var ms = new MemoryStream();
        //        await file.Data.CopyToAsync(ms);
        //        statues = $"Upload {file.Size} bytes from {file.Name}";
        //        var content = new MultipartFormDataContent
        //        {
        //            {new ByteArrayContent(ms.GetBuffer()),"\"Upload\"",file.Name }
        //        };
        //        await Http.PostAsync("upload", content);
        //    }

        //}

        //private void UndoDelete()
        //{
        //    if (tempitem != null && _index > 0)
        //    {
        //        ItemList.Insert(_index, tempitem);
        //        tempitem = null;
        //        _index = -1;
        //    }
        //}
        //async Task HandleSelection(IFileListEntry[] files)
        //{
        //    imageDataURL = "";
        //    fileTextContents = "";
        //    var file = files.FirstOrDefault();
        //    if (file != null)
        //    {
        //        //check to see if the file is an image or a text file
        //        var fileType = GetFileType(file.Name);
        //        if (fileType == "image")
        //        {
        //            var ms = new MemoryStream();
        //            await file.Data.CopyToAsync(ms);
        //            string imageBase64Data = Convert.ToBase64String(ms.ToArray());
        //            imageDataURL = string.Format("data:image/png;base64,{0}", imageBase64Data);

        //            using (FileStream newFile = new FileStream("wwwroot\\uploads\\" + file.Name, FileMode.Create, FileAccess.Write))
        //            {
        //                ms.WriteTo(newFile);
        //            }
        //        }
        //        else if (fileType == "text")
        //        {
        //            using (var reader = new StreamReader(file.Data))
        //            {
        //                fileTextContents = await reader.ReadToEndAsync();
        //            }
        //        }
        //    }

        //    status = $"Finished loading {file.Size} bytes from {file.Name}";
        //}
        //private string GetFileType(string name)
        //{
        //    string result = "";
        //    if (name.Contains("png") || name.Contains("jpg") || name.Contains("gif") || name.Contains("JPEG"))
        //    {
        //        result = "image";
        //    }
        //    else if (name.Contains("txt"))
        //    {
        //        result = "text";
        //    }
        //    return result;
        //}
        //protected async Task HandleSelection(IFileListEntry[] files)
        //{
        //    var SourceFile = files.FirstOrDefault();
        //    if (SourceFile !=null)
        //    {
        //        var ImageFile = await SourceFile.ToImageFileAsync("image/jpeg", 800, 600);
        //        MemoryStream bytes = await ImageFile.ReadAllAsync();
        //        _PhotoUrl = bytes.ToDataUrl("image/jpeg");
        //    }
        //}

        async Task BindLists()
        {

            var ListReturn = await Http.GetAsync("api/Items/GetDDLs");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            itm = JsonConvert.DeserializeObject<ItemVM>(vMs["data"].ToString());
            CollectionList = itm.ItemCollection;
            TypeList = itm.ItemType;
            SupplierList = itm.Supplier;
        }

        async Task BindStoreList()
        {
            var ListReturn = await Http.GetAsync("api/Store/GetStores");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            StoreList = JsonConvert.DeserializeObject<List<StoreVM>>(vMs["data"].ToString());
        }
        protected void Cancel()
        {
            showAddrow = false;
            showEditrow = false;
        }
        protected async Task CheckName()

        {
            ValidateMessageName = "";

            string uri = "/api/Items/CheckNameItem?name=" + itm.Name + "&Id=" + itm.ID;

            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            string result = vMs["data"].ToString();
            if (result == "True")
            {
                ValidateMessageName = "الاسم موجود من قبل";
            }
            else
            {
                ValidateMessageName = "";
            }
        }
        protected async Task CheckNationalParCode()

        {
            ValidateMessageNational = "";

            string uri = "/api/Items/CheckNationalParCode?NationalParCod=" + itm.National_ParCode + "&Id=" + itm.ID;

            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            string result = vMs["data"].ToString();
            if (result == "True")
            {
                ValidateMessageNational = "الباركود المحلي موجود من قبل";
            }
            else
            {
                ValidateMessageNational = "";
            }
        }

        protected async Task CheckInternationalParCode()

        {
            ValidateMessageInternational = "";

            string uri = "/api/Items/CheckInternationalParCode?InternationalParCode=" + itm.Interational_ParCode + "&Id=" + itm.ID;

            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            string result = vMs["data"].ToString();
            if (result == "True")
            {
                ValidateMessageInternational = "الباركود الدولي موجود من قبل";
            }
            else
            {
                ValidateMessageInternational = "";
            }
        }

        protected async Task CheckSupplierParCode()

        {
            ValidateMessageSupplier = "";

            string uri = "/api/Items/CheckSupplierParCode?SupplierParCode=" + itm.Supplier_ParCode + "&Id=" + itm.ID;

            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            string result = vMs["data"].ToString();
            if (result == "True")
            {
                ValidateMessageSupplier = " باركود المورد موجود من قبل ";
            }
            else
            {
                ValidateMessageSupplier = "";
            }
        }

        protected async Task CheckBoxParCode()

        {
            ValidateMessageBox = "";

            string uri = "/api/Items/CheckBoxParCode?BoxParCode=" + itm.Box_ParCode + "&Id=" + itm.ID;

            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            string result = vMs["data"].ToString();
            if (result == "True")
            {
                ValidateMessageBox = " باركود العلبة موجود من قبل ";
            }
            else
            {
                ValidateMessageBox = "";
            }
        }
        protected async Task CheckGuaranteeParCode()

        {
            ValidateMessageGuarantee = "";

            string uri = "/api/Items/CheckGuaranteeParCode?GuaranteeParCode=" + itm.Guarantee_ParCode + "&Id=" + itm.ID;

            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            string result = vMs["data"].ToString();
            if (result == "True")
            {
                ValidateMessageGuarantee = " باركود الضمان موجود من قبل ";
            }
            else
            {
                ValidateMessageGuarantee = "";
            }
        }
        bool ValidateOB()
        {
            if (itm.Name == "")
            {
                RequiredMessageName = "الصنف مطلوب";
            }
            else
            {
                RequiredMessageName = "";
            }

            if (itm.National_ParCode == "")
            {
                RequiredMessageNationalParCode = "الكود المحلي مطلوب";
            }
            else
            {
                RequiredMessageNationalParCode = "";
            }

            if (RequiredMessageName == "" && RequiredMessageNationalParCode == "")

            {
                return true;
            }
            else
            {
                return false;
            }
        }


        void AddNewRow()
        {
            isModel = new Item_StoreVM { Name = "Try" };
            ItemStoreList.Add(isModel);
            // to clear the inputs
            //isModel = new Item_Store();
            StateHasChanged();
        }

        void RemoveRow(Item_StoreVM row)
        {
            ItemStoreList.Remove(row);
            StateHasChanged();
        }
        #endregion


    }
}
