﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasProject.Domain.ViewModel.Items;
using MasProject.Data.Models.Items;
using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel.Hierarchy;
using System.Net.Http;
using System.Net.Http.Json;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Components;
using System.Text;
using MatBlazor;
using System.IO;
using BlazorInputFile;
using MasProject.Domain.Services.Items;
using Microsoft.JSInterop;
using MasProject.Client.Shared;

namespace MasProject.Client.Pages.Items.Item
{
    public partial class AddItem
    {
        List<ItemVM> ItemList;
        private PageTitle PageTitle;
        bool ShowConfirm = false;
        List<Item_StoreVM> ItemStoreList = new List<Item_StoreVM>();
        List<ItemCollectionVM> CollectionList = new List<ItemCollectionVM>();
        List<ItemTypeVM> TypeList = new List<ItemTypeVM>();
        List<SupplierVM> SupplierList = new List<SupplierVM>();
        List<StoreVM> StoreList = new List<StoreVM>();
        ItemVM itm = new ItemVM();
        Item_StoreVM itmstore = new Item_StoreVM();
        List<string> list = new List<string>();
        Item_StoreVM isModel = new Item_StoreVM();
        string status;
        string imageDataURL = "";
        string fileTextContents = "";
        string _PhotoUrl;
        bool dialogOpened = false;
        bool _isedit = false;
        bool SnakBar = false;
        Item tempitem = null;
        int _index = -1;
        bool _isdeleted = false;
        bool showAddrow = false;
        bool showEditrow = false;
        bool ShowDelete = false;
        int ItemID;
        string RequiredMessageName = "";
        string ValidateMessageName = "";
        string RequiredMessageNationalParCode = "";
        int StoreID;
        string ValidateMessageNational = "";
        string ValidateMessageInternational = "";
        string ValidateMessageSupplier = "";
        string ValidateMessageBox = "";
        string ValidateMessageGuarantee = "";
        IFileListEntry file;
        bool IsDisabled = false;

        //bool ShowImg = false;

        protected override async Task OnInitializedAsync()
        {
            itmstore = new Item_StoreVM();
            itmstore = new Item_StoreVM { ItemName = "" };
            ItemStoreList = new List<Item_StoreVM>();
            ItemStoreList.Add(itmstore);
            await BindLists();
            await BindStoreList();
            //  isModel = new Item_StoreVM { Name = "Try" };
        }

        void HandleSelection(IFileListEntry[] files)
        {
            file = files.FirstOrDefault();
        }

        protected async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);

        }
        protected async Task AddItems()
        {
            IsDisabled = true;
            if (ValidateOB() & ValidateMessageName == "" & ValidateMessageBox == "" & ValidateMessageGuarantee == "" & ValidateMessageInternational == "" & ValidateMessageNational == "" & ValidateMessageSupplier == "")
            {
                //insert image
                string pathlogo = $"/Items/";
                if (!Directory.Exists(pathlogo))
                    Directory.CreateDirectory(pathlogo);
                if (file != null && file.Size > 0)
                {
                    var namelogo = Guid.NewGuid().ToString() + Path.GetExtension(file.Name);
                    var ms = new MemoryStream();
                    await file.Data.CopyToAsync(ms);
                    var contentFile = new MultipartFormDataContent {
                {
                            new ByteArrayContent(ms.GetBuffer()), "\"Items\"", namelogo}
                    };
                    await Http.PostAsync("upload?FolderName=" + "Items", contentFile);

                    itm.ItemImg = "/Images/Items/" + namelogo;
                }
                itm.ItemStoreList = ItemStoreList;
                var AddModel = JsonConvert.SerializeObject(itm);
                var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/Items/AddItem", content);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                    showAddrow = false;
                    ShowConfirm = true;
                }
            }
        }


        protected async Task Hide(int ID)
        {

            string uri = "/api/Items/HideItem?ID=" + ItemID;
            var result = await Http.GetAsync(uri);
            if (result.IsSuccessStatusCode)
            {
                await BindList();
            }

        }
        async Task BindStoreList()
        {
            var ListReturn = await Http.GetAsync("api/Store/GetStores");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            StoreList = JsonConvert.DeserializeObject<List<StoreVM>>(vMs["data"].ToString());
        }

        async Task BindLists()
        {

            var ListReturn = await Http.GetAsync("api/Items/GetDDLs");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            itm = JsonConvert.DeserializeObject<ItemVM>(vMs["data"].ToString());
            CollectionList = itm.ItemCollection;
            TypeList = itm.ItemType;
            SupplierList = itm.Supplier;
        }

        protected async Task CheckName()

        {
            if (itm.Name != null)
            {
               // ValidateMessageName = "";
                string uri = "/api/Items/CheckNameItem?name="+itm.Name+"&id="+itm.ID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateMessageName = "الاسم موجود من قبل";

                }
                else
                {
                    ValidateMessageName = "";
                }
            }

        }
        protected async Task CheckNationalParCode()

        {
            if (itm.National_ParCode != null)
            {
                ValidateMessageNational = "";

                string uri = "/api/Items/CheckNationalParCode?NationalParCod=" + itm.National_ParCode.Trim() + "&Id=" + itm.ID;

                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateMessageNational = "الباركود المحلي موجود من قبل";
                }
                else
                {
                    ValidateMessageNational = "";
                }
            }
        }

        protected async Task CheckInternationalParCode()

        {
            
            ValidateMessageInternational = "";
            if (itm.Interational_ParCode != null)
            {
                string uri = "/api/Items/CheckInternationalParCode?InternationalParCode=" + itm.Interational_ParCode.Trim() + "&Id=" + itm.ID;

                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateMessageInternational = "الباركود الدولي موجود من قبل";
                }
                else
                {
                    ValidateMessageInternational = "";
                }
            }
        }
        
        protected async Task CheckSupplierParCode()

        {
            if (itm.Supplier_ParCode != null)
            {
                ValidateMessageSupplier = "";

                string uri = "/api/Items/CheckSupplierParCode?SupplierParCode=" + itm.Supplier_ParCode.Trim() + "&Id=" + itm.ID;

                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateMessageSupplier = " باركود المورد موجود من قبل ";
                }
                else
                {
                    ValidateMessageSupplier = "";
                }
            }
        }

        protected async Task CheckBoxParCode()

        {
            if (itm.Box_ParCode != null)
            {
                ValidateMessageBox = "";

                string uri = "/api/Items/CheckBoxParCode?BoxParCode=" + itm.Box_ParCode.Trim() + "&Id=" + itm.ID;

                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateMessageBox = " باركود العلبة موجود من قبل ";
                }
                else
                {
                    ValidateMessageBox = "";
                }
            }
        }
        protected async Task CheckGuaranteeParCode()

        {
            if (itm.Guarantee_ParCode != null)
            {
                ValidateMessageGuarantee = "";

                string uri = "/api/Items/CheckGuaranteeParCode?GuaranteeParCode=" + itm.Guarantee_ParCode.Trim() + "&Id=" + itm.ID;

                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateMessageGuarantee = " باركود الضمان موجود من قبل ";
                }
                else
                {
                    ValidateMessageGuarantee = "";
                }
            }
        }
        bool ValidateOB()
        {
            if (itm.Name == "")
            {
                RequiredMessageName = "الصنف مطلوب";
            }
            else
            {
                RequiredMessageName = "";
            }

            if (itm.National_ParCode == "")
            {
                RequiredMessageNationalParCode = "الكود المحلي مطلوب";
            }
            else
            {
                RequiredMessageNationalParCode = "";
            }

            if (RequiredMessageName == "" && RequiredMessageNationalParCode == "")

            {
                return true;
            }
            else
            {
                return false;
            }
        }

        void AddNewRow()
        {
            isModel = new Item_StoreVM { Name = "Try" };
            ItemStoreList.Add(isModel);
            // to clear the inputs
            //isModel = new Item_Store();
            StateHasChanged();
        }

        void RemoveRow(Item_StoreVM row)
        {
            ItemStoreList.Remove(row);
            StateHasChanged();
        }
        async Task BindList()
        {
            var ListReturn = await Http.GetAsync("api/Items/GetItems");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ItemList = JsonConvert.DeserializeObject<List<ItemVM>>(vMs["data"].ToString());

        }
    }
}
