﻿using MasProject.Domain.ViewModel.Hierarchy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using MasProject.Domain.ViewModel.Items;
using MasProject.Domain.ViewModel.Persons;
using System;
using MasProject.Domain.ViewModel;
using System.Linq;

namespace MasProject.Client.Pages.Items.ItemOB
{
    public partial class ItemOBAdd
    {
        private PageTitle PageTitle;
        ItemOpeningBalanceVM Balance = new ItemOpeningBalanceVM();
        ItemOpeningBalanceDetailsVM BalanceDetails = new ItemOpeningBalanceDetailsVM();
        List<ItemOpeningBalanceDetailsVM> BalanceDetailsList = new List<ItemOpeningBalanceDetailsVM>();
        List<LookupKeyValueVM> StoreList = new List<LookupKeyValueVM>();
        List<LookupItem> ItemList = new List<LookupItem>();
        List<LookupKeyValueVM> EmployeeList = new List<LookupKeyValueVM>();
        //StoreVM Stores = new StoreVM();
        //ItemVM item = new ItemVM();
        //string ValidateMessageDate = "";
        //string ValidateMessageStore = "";
        //string ValidateMessageItem = "";
        bool ShowConfirm = false;
        bool FirstAdd = false;
        string ItemImg = "";
        bool ShowImg = false;
        bool IsDisabled = false;
        bool ItemCodeChecker = false;
        //async Task BindStoreList()
        //{

        //    var ListReturn = await Http.GetAsync("api/Store/GetStores");
        //    var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
        //    StoreList = JsonConvert.DeserializeObject<List<StoreVM>>(vMs["data"].ToString());
        //}

        //async Task BindItemList()
        //{
        //    var ListReturn = await Http.GetAsync("api/Items/GetItem");
        //    var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
        //    ItemList = JsonConvert.DeserializeObject<List<LookupItem>>(vMs["data"].ToString());
        //}
        //async Task BindEmployeeList()
        //{
        //    var ListReturn = await Http.GetAsync("api/Employee/GetEmployees");
        //    var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
        //    EmployeeList = JsonConvert.DeserializeObject<List<EmployeeVM>>(vMs["data"].ToString());
        //}

        protected override async Task OnInitializedAsync()
        {
            FirstAdd = false;
            Balance = new ItemOpeningBalanceVM();
            //Balance = new ItemOpeningBalanceVM { Date=DateTime.Now };
            var IdReturn = await Http.GetAsync("api/ItemOpeningBalance/GenerateSerial");
            var IdvMs = JObject.Parse(IdReturn.Content.ReadAsStringAsync().Result);
            Balance = JsonConvert.DeserializeObject<ItemOpeningBalanceVM>(IdvMs["data"].ToString());
            BalanceDetails = new ItemOpeningBalanceDetailsVM { Notes = "" };
            BalanceDetailsList = new List<ItemOpeningBalanceDetailsVM>();
            BalanceDetailsList.Add(BalanceDetails);
            Balance.Date = DateTime.Now;
            StoreList = Balance.StoreList;
            EmployeeList = Balance.EmployeeList;
            ItemList = Balance.ItemList;
            //await BindItemList();
            //await BindStoreList();
            //await BindEmployeeList();
        }

        //bool ValidateOB()
        //{
        //    if (Balance.StoreId == 0)
        //    {
        //        ValidateMessageStore = "عليك اختيار المخزن";
        //    }
        //    else
        //    {
        //        ValidateMessageStore = "";
        //    }
        //    DateTime CheckDate = new DateTime();
        //    if (Balance.Date == CheckDate)
        //    {
        //        ValidateMessageDate = "عليك ادخال التاريخ";
        //    }
        //    else
        //    {
        //        ValidateMessageDate = "";
        //    }
        //    if (ValidateMessageDate == "" && ValidateMessageStore == "" && ValidateMessageItem == "")
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}
        void ChangeItemQuantity()
        {
            RecalculateTotal();
            AddNewRow();
        }
        void RecalculateTotal()
        {
            foreach (var row in BalanceDetailsList)
            {
                if (row.price != null & row.Quantity != null)
                {
                    row.TotalPrice = (float)(row.Quantity * row.price);
                }
            }
            StateHasChanged();
        }
        protected async Task AddDetail(ItemOpeningBalanceDetailsVM itemModel)
        {
            RecalculateTotal();
            StateHasChanged();
            if (Balance.DocumentNumber > 0)
            {
                HttpResponseMessage result = new HttpResponseMessage();
                HttpResponseMessage resultItem = new HttpResponseMessage();

                if (FirstAdd == false)
                {
                    FirstAdd = true;
                    var AddModel = JsonConvert.SerializeObject(Balance);
                    var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/ItemOpeningBalance/AddItemOpeningBalance", content);
                    var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    Balance.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                }
                // itemModel.TotalPrice = BalanceDetailsList.LastOrDefault().TotalPrice;
                itemModel.ItemDocumentNumber = Balance.DocumentNumber;
                var AddItemModel = JsonConvert.SerializeObject(itemModel);
                var contentItem = new StringContent(AddItemModel, Encoding.UTF8, "application/json");
                resultItem = await Http.PostAsync("api/ItemOpeningBalance/AddItemOpeningBalanceDetails", contentItem);
                var AddvMsItem = JObject.Parse(resultItem.Content.ReadAsStringAsync().Result);
                itemModel.ID = JsonConvert.DeserializeObject<int>(AddvMsItem["data"].ToString());
            }
            StateHasChanged();
        }

        void AddNewRow()
        {
            BalanceDetails = new ItemOpeningBalanceDetailsVM { Notes = "" };
            BalanceDetailsList.Add(BalanceDetails);
            StateHasChanged();
        }
        void RemoveRow(ItemOpeningBalanceDetailsVM row)
        {
            BalanceDetailsList.Remove(row);
            StateHasChanged();
        }
        //protected async Task AddItemOB(ItemOpeningBalanceVM Balance)
        //{
        //    HttpResponseMessage result = new HttpResponseMessage();
        //    //check for save
        //    StateHasChanged();
        //}
        async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);//
            //ShowConfirm = false;
            //IsDisabled = false;
        }
        protected async Task EditClientOpeningBalance()
        {
            IsDisabled = true;
            HttpResponseMessage result = new HttpResponseMessage();
            Balance.ItemDetails = BalanceDetailsList;
            var EditModel = JsonConvert.SerializeObject(Balance);
            var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
            result = await Http.PostAsync("api/ItemOpeningBalance/EditItemOpeningBalance", content);
            if (result.IsSuccessStatusCode)
            {
                ShowConfirm = true;
            }
            StateHasChanged();
        }
        protected void GetItemDataByCode(ItemOpeningBalanceDetailsVM row)
        {
            LookupItem SelectedITem = ItemList.Find(x => (x.BoxCode == row.ItemCode || x.GuaranteeParCode == row.ItemCode || x.InterationalParCode == row.ItemCode || x.Code == row.ItemCode || x.ParCodeNote == row.ItemCode || x.SupplierParCode == row.ItemCode));
            if (SelectedITem != null)
            {
                ItemImg = SelectedITem.ItemImg ?? "";
                row.ItemId = SelectedITem.ID;
                row.Quantity = null;
                row.price = null;
                row.TotalPrice = null;
                row.ItemName = SelectedITem.Name;
                StateHasChanged();
            }
            else
            {
                ItemCodeChecker = true;
            }
        }
        protected void GetItemData(ItemOpeningBalanceDetailsVM row)
        {
            //  LookupItem SelectedITem = ItemList.Find(x => x.ID == row.ItemId);//hna bl name ya osho
            LookupItem SelectedITem = ItemList.Find(x => x.Name == row.ItemName);
            ItemImg = SelectedITem.ItemImg;
            row.ItemCode = SelectedITem.Code;
            row.ItemId = SelectedITem.ID;//id
            row.Quantity = null;
            row.price = null;
            row.TotalPrice = null;
            StateHasChanged();
        }
        //protected void GetItemTotal(ItemOpeningBalanceDetailsVM row)
        //{
        //    if (row.price != null & row.Quantity != null)
        //    {
        //        row.TotalPrice = (float)(row.Quantity * row.price);
        //        StateHasChanged();
        //    }

        //}
        //protected void GetItemTotalEdit(ItemOpeningBalanceDetailsVM row)
        //{
        //    if (row.price != null & row.Quantity != null)
        //    {
        //        float olditemtotal = row.TotalPrice.Value;
        //        row.TotalPrice = (float)(row.Quantity * row.price);
        //        StateHasChanged();
        //    }
        //}
        //void EraseText(ItemOpeningBalanceDetailsVM row)
        //{

        //    row.ItemName = String.Empty;
        //    row.ItemCode = String.Empty;
        //    row.price = 0;
        //}

        async Task ReloadItemsList(ItemOpeningBalanceDetailsVM row)
        {
            row.ItemName = String.Empty;
            row.ItemCode = String.Empty;
            row.price = 0;


            var ItemListReturn = await Http.GetAsync("api/Items/GetItem");
            var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
            ItemList = JsonConvert.DeserializeObject<List<LookupItem>>(ItemvMs["data"].ToString());
        }
    }
}