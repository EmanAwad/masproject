﻿using System.Threading.Tasks;
using MasProject.Domain.ViewModel.Items;
using MasProject.Domain.ViewModel.Hierarchy;
using System.Net.Http;
using MasProject.Domain.ViewModel.Persons;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Collections.Generic;
using System;
using MasProject.Client.Shared;
using MasProject.Domain.ViewModel;

namespace MasProject.Client.Pages.Items.ItemOB
{
    public partial class ItemOBEdit
    {
        private PageTitle PageTitle;
        ItemOpeningBalanceVM Balance = new ItemOpeningBalanceVM();
        ItemOpeningBalanceDetailsVM BalanceDetails = new ItemOpeningBalanceDetailsVM();
        List<ItemOpeningBalanceDetailsVM> BalanceDetailsList = new List<ItemOpeningBalanceDetailsVM>();
        List<LookupKeyValueVM> StoreList = new List<LookupKeyValueVM>();
        List<LookupItem> ItemList = new List<LookupItem>();
        List<LookupKeyValueVM> EmployeeList = new List<LookupKeyValueVM>();
        //StoreVM Stores = new StoreVM();
        //ItemVM item = new ItemVM();
        string ValidateMessageDate = "";
        string ValidateMessageStore = "";
        string ValidateMessageItem = "";
        bool ShowConfirm = false;
        bool FirstAdd = false;
        string ItemImg = "";
        bool ShowImg = false;
        bool IsDisabled = false;
        bool ItemCodeChecker = false;
        protected override async Task OnInitializedAsync()
        {
            //await BindItemList();
            //await BindStoreList();
            //await BindEmployeeList();

            var querystring = ExtensionMethods.QueryString(nav);
            var id = querystring["ID"];
            await BindLists();
            await EditOldItemOB(int.Parse(id));
            var Type = querystring["Type"];
            if (Type == "View")
            {
                //disable controls
                IsDisabled = true;
                PageTitle.Title = "    عرض الأرصدة الافتتاحية للمخازن";
            }
            else
            {
                IsDisabled = false;
                PageTitle.Title = "تعديل  الأرصدة الافتتاحية للمخازن";
            }

        }
        async Task BindLists()
        {
            FirstAdd = false;
            var IdReturn = await Http.GetAsync("api/ItemOpeningBalance/GenerateSerial");
            var IdvMs = JObject.Parse(IdReturn.Content.ReadAsStringAsync().Result);
            Balance = JsonConvert.DeserializeObject<ItemOpeningBalanceVM>(IdvMs["data"].ToString());
            StoreList = Balance.StoreList;
            EmployeeList = Balance.EmployeeList;
            ItemList = Balance.ItemList;
        }
        protected async Task EditOldItemOB(int ID)
        {
            ValidateMessageDate = "";
            ValidateMessageStore = "";
            ValidateMessageItem = "";
            FirstAdd = true;
            string uri = "/api/ItemOpeningBalance/GetSpecificItemOpeningBalance?Id=" + ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            Balance = JsonConvert.DeserializeObject<ItemOpeningBalanceVM>(vMs["data"].ToString());
            BalanceDetailsList = Balance.ItemDetails;
        }
        //async Task BindStoreList()
        //{

        //    var ListReturn = await Http.GetAsync("api/Store/GetStores");
        //    var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
        //    StoreList = JsonConvert.DeserializeObject<List<StoreVM>>(vMs["data"].ToString());
        //}
        //async Task BindItemList()
        //{
        //    var ListReturn = await Http.GetAsync("api/Items/GetItems");
        //    var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
        //    ItemsList = JsonConvert.DeserializeObject<List<ItemVM>>(vMs["data"].ToString());
        //}

        //async Task BindEmployeeList()
        //{
        //    var ListReturn = await Http.GetAsync("api/Employee/GetEmployees");
        //    var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
        //    EmployeeList = JsonConvert.DeserializeObject<List<EmployeeVM>>(vMs["data"].ToString());
        //}
        void ChangeItemQuantity()
        {
            RecalculateTotal();
            AddNewRow();
        }
        void RecalculateTotal()
        {
            foreach (var row in BalanceDetailsList)
            {
                if (row.price != null & row.Quantity != null)
                {
                    row.TotalPrice = (float)(row.Quantity * row.price);

                }
            }
            StateHasChanged();
        }
        void AddNewRow()
        {
            BalanceDetails = new ItemOpeningBalanceDetailsVM { Notes = "" };
            BalanceDetailsList.Add(BalanceDetails);
            StateHasChanged();
        }
        void RemoveRow(ItemOpeningBalanceDetailsVM row)
        {
            BalanceDetailsList.Remove(row);
            StateHasChanged();
        }
        protected async Task EditItemOpeningBalance()
        {

            if (ValidateOB())
            {
                Balance.ItemDetails = BalanceDetailsList;
                var EditModel = JsonConvert.SerializeObject(Balance);
                var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/ItemOpeningBalance/EditItemOpeningBalance", content);
                if (result.IsSuccessStatusCode)
                {
                    ShowConfirm = true;
                    StateHasChanged();
                }

            }
        }
        protected async Task GetItemDataByCode(ItemOpeningBalanceDetailsVM row)
        {
            HttpResponseMessage result = new HttpResponseMessage();
            //string uri = "/api/Items/GetSpecificItem?Id=" + row.ItemId;
            //HttpResponseMessage response = await Http.GetAsync(uri);
            //var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            //ItemVM TempItemObj = JsonConvert.DeserializeObject<ItemVM>(vMs["data"].ToString());
            //row.ItemCode = TempItemObj.National_ParCode;

            LookupItem SelectedITem = ItemList.Find(x => (x.BoxCode == row.ItemCode || x.GuaranteeParCode == row.ItemCode || x.InterationalParCode == row.ItemCode || x.Code == row.ItemCode || x.ParCodeNote == row.ItemCode || x.SupplierParCode == row.ItemCode));
            if (SelectedITem != null)
            {
                ItemImg = SelectedITem.ItemImg ?? "";
                row.ItemId = SelectedITem.ID;
                row.ItemName = SelectedITem.Name;
                row.Quantity = null;
                row.price = null;
                row.TotalPrice = null;
                if (Balance.DocumentNumber > 0)
                {

                    Balance.ItemDetails = BalanceDetailsList;
                    var EditModel = JsonConvert.SerializeObject(Balance);
                    var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/ItemOpeningBalance/EditItemOpeningBalance", content);
                    // ShowConfirm = true;

                    if (result.IsSuccessStatusCode)
                    {

                        StateHasChanged();
                    }
                }
            }
            else
            {
                ItemCodeChecker = true;
            }

        }
        protected async Task GetItemData(ItemOpeningBalanceDetailsVM row)
        {
            HttpResponseMessage result = new HttpResponseMessage();
            //string uri = "/api/Items/GetSpecificItem?Id=" + row.ItemId;
            //HttpResponseMessage response = await Http.GetAsync(uri);
            //var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            //ItemVM TempItemObj = JsonConvert.DeserializeObject<ItemVM>(vMs["data"].ToString());
            //row.ItemCode = TempItemObj.National_ParCode;

            //  LookupItem SelectedITem = ItemList.Find(x => x.ID == row.ItemId);
            LookupItem SelectedITem = ItemList.Find(x => x.Name == row.ItemName);
            ItemImg = SelectedITem.ItemImg;
            row.ItemCode = SelectedITem.Code;
            row.ItemId = SelectedITem.ID;
            row.Quantity = null;
            row.price = null;
            row.TotalPrice = null;
            if (Balance.DocumentNumber > 0)
            {

                Balance.ItemDetails = BalanceDetailsList;
                var EditModel = JsonConvert.SerializeObject(Balance);
                var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                result = await Http.PostAsync("api/ItemOpeningBalance/EditItemOpeningBalance", content);
                // ShowConfirm = true;

                if (result.IsSuccessStatusCode)
                {

                    StateHasChanged();
                }
            }
        }

        //protected void GetItemTotal(ItemOpeningBalanceDetailsVM row)
        //{
        //    if (row.price != null & row.Quantity != null)
        //    {
        //        row.TotalPrice = (float)(row.Quantity * row.price);
        //        StateHasChanged();
        //    }
        //}
        //protected void GetItemTotalEdit(ItemOpeningBalanceDetailsVM row)
        //{
        //    if (row.price != null & row.Quantity != null)
        //    {
        //        float olditemtotal = row.TotalPrice.Value;
        //        row.TotalPrice = (float)(row.Quantity * row.price);
        //        StateHasChanged();
        //    }
        //}

        bool ValidateOB()
        {
            if (Balance.StoreId == 0)
            {
                ValidateMessageStore = "عليك اختيار المخزن";
            }
            else
            {
                ValidateMessageStore = "";
            }
            DateTime CheckDate = new DateTime();
            if (Balance.Date == CheckDate)
            {
                ValidateMessageDate = "عليك ادخال التاريخ";
            }
            else
            {
                ValidateMessageDate = "";
            }
            if (ValidateMessageDate == "" && ValidateMessageStore == "" && ValidateMessageItem == "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        void Confirm()
        {
            //await navigationManager.NavigateToExitWindowAsync(jsRuntime);//
            ShowConfirm = false;
            IsDisabled = false;
        }

        //void EraseText(ItemOpeningBalanceDetailsVM row)
        //{

        //    row.ItemName = String.Empty;
        //    row.ItemCode = String.Empty;
        //    row.price = 0;
        //}

        async Task ReloadItemsList(ItemOpeningBalanceDetailsVM row)
        {
            row.ItemName = String.Empty;
            row.ItemCode = String.Empty;
            row.price = 0;


            var ItemListReturn = await Http.GetAsync("api/Items/GetItem");
            var ItemvMs = JObject.Parse(ItemListReturn.Content.ReadAsStringAsync().Result);
            ItemList = JsonConvert.DeserializeObject<List<LookupItem>>(ItemvMs["data"].ToString());
        }
    }
}
