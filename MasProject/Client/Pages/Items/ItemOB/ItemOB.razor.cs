﻿using System.Threading.Tasks;
using MasProject.Domain.ViewModel.Items;
using MasProject.Domain.ViewModel.Hierarchy;
using System.Net.Http;
using System.Net.Http.Json;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Components;
using System.Text;
using System.Collections.Generic;
using System;
using MasProject.Client.Shared;
using Microsoft.JSInterop;

namespace MasProject.Client.Pages.Items.ItemOB
{
    public partial class ItemOB
    {
        private PageTitle PageTitle;
        List<ItemOpeningBalanceVM> ItemOpeningBalanceList;
        bool ShowDelete = false;
        int OBID;
        bool ShowConfirm = false;
        string ValidateMessageDate = "";
        string ValidateMessageStore = "";
        string ValidateMessageItem = "";
        protected override async Task OnInitializedAsync()
        {
          
            await BindList();

        }
        async Task BindList()
        {
            var ListReturn = await Http.GetAsync("api/ItemOpeningBalance/GetItemOpeningBalance");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ItemOpeningBalanceList = JsonConvert.DeserializeObject<List<ItemOpeningBalanceVM>>(vMs["data"].ToString());
        }
        protected void Confirm()
        {
            ShowConfirm = false;
        }

        protected async Task AddNewItemOBAsync()
        {
           // await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/ItemOBAdd", "جارى التحميل", 1500);
            await jsRuntime.InvokeAsync<object>("open", "/ItemOBAdd", "_blank");
            ValidateMessageDate = "";
            ValidateMessageStore = "";
            ValidateMessageItem = "";
        }

        protected async Task EditOldItemOB(int ID ,string Type)
        {
            //  await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/ItemOBEdit?ID=" + ID, "جارى التحميل", 1500);
            await jsRuntime.InvokeAsync<object>("open", "/ItemOBEdit?ID=" + ID + "&Type=" + Type, "_blank");
        }
        protected void DeleteItemOB(int ID)
        {
            OBID = ID;
            ShowDelete = true;

        }
        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                string uri = "/api/ItemOpeningBalance/DeleteItemOpeningBalance?ID=" + OBID;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
            ShowDelete = false;
        }


    }
}
