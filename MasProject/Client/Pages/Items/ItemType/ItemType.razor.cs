﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasProject.Domain.ViewModel.Items;
using System.Net.Http;
using System.Net.Http.Json;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Components;
using System.Text;
using MasProject.Client.Shared;

namespace MasProject.Client.Pages.Items.ItemType
{
    public partial class ItemType
    {
        private PageTitle PageTitle;
        List<ItemTypeVM> ItemTypeList;
        ItemTypeVM Type = new ItemTypeVM();
#pragma warning disable CS0414 // The field 'ItemType.showAddrow' is assigned but its value is never used
        bool showAddrow = false;
#pragma warning restore CS0414 // The field 'ItemType.showAddrow' is assigned but its value is never used
#pragma warning disable CS0414 // The field 'ItemType.showEditrow' is assigned but its value is never used
        bool showEditrow = false;
#pragma warning restore CS0414 // The field 'ItemType.showEditrow' is assigned but its value is never used
#pragma warning disable CS0414 // The field 'ItemType.ShowDelete' is assigned but its value is never used
        bool ShowDelete = false;
#pragma warning restore CS0414 // The field 'ItemType.ShowDelete' is assigned but its value is never used
#pragma warning disable CS0414 // The field 'ItemType.ShowConfirm' is assigned but its value is never used
        bool ShowConfirm = false;
#pragma warning restore CS0414 // The field 'ItemType.ShowConfirm' is assigned but its value is never used
        string ValidateMessageName = "";
        string ValidateMessageArrangement = "";
        int TypeID;
        bool IsDisabled = false;

        async Task BindList()
        {
            var ListReturn = await Http.GetAsync("api/ItemType/GetItemTypes");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ItemTypeList = JsonConvert.DeserializeObject<List<ItemTypeVM>>(vMs["data"].ToString());
        }

        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        protected async Task CheckName()
        {
            if (Type.Name != null)
            {
                string uri = "/api/ItemType/CheckNameItemType?name=" + Type.Name.Trim() + "&Id=" + Type.ID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateMessageName = "الاسم موجود من قبل";
                }
                else
                {
                    ValidateMessageName = "";
                }
            }
        }
        protected async Task CheckArrangement()
        {
            if (Type.arrangement != null)
            {
                string uri = "/api/ItemCollection/CheckArrangement?arrangement=" + Type.arrangement + "&Id=" + Type.ID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateMessageArrangement = "الترتيب موجود من قبل";
                }
                else
                {
                    ValidateMessageArrangement = "";
                }
            }
        }
        protected async Task AddItemType()
        {
            IsDisabled = true;
            if (ValidateMessageName == "" & ValidateMessageArrangement == "")
            {
                var AddModel = JsonConvert.SerializeObject(Type);
                var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/ItemType/AddItemType", content);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                    ShowConfirm = true;
                }
            }
        }

        void AddNewItemType()
        {
            ValidateMessageName = "";
            showAddrow = true;
            showEditrow = false;
            Type = new ItemTypeVM();
        }
        protected void Confirm()
        {
            ShowConfirm = false;
            showAddrow = false;
            showEditrow = false;
        }

        protected async Task EditOldItemType(int ID)
        {
            ValidateMessageName = "";
            ValidateMessageArrangement = "";
            showAddrow = false;
            showEditrow = true;
            string uri = "/api/ItemType/GetSpecificItemType?Id=" + ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            Type = JsonConvert.DeserializeObject<ItemTypeVM>(vMs["data"].ToString());
        }

        protected async Task EditItemType()
        {
            if (ValidateMessageName == "" & ValidateMessageArrangement == "")
            {
                var EditModel = JsonConvert.SerializeObject(Type);
                var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/ItemType/EditItemType", content);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                    ShowConfirm = true;
                }
            }
        }
        protected void DeleteItemType(int ID)
        {
            TypeID = ID;
            ShowDelete = true;
        }
        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                string uri = "/api/ItemType/DeleteItemType?ID=" + TypeID;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
            ShowDelete = false;
        }

        protected void Cancel()
        {
            showAddrow = false;
            showEditrow = false;
        }
    }
}