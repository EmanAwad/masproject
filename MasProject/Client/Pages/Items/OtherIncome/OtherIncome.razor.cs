﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasProject.Domain.ViewModel.Items;
using System.Net.Http;
using System.Net.Http.Json;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Components;
using System.Text;
using MatBlazor;
using MasProject.Client.Shared;

namespace MasProject.Client.Pages.Items.OtherIncome
{
    public partial class OtherIncome
    {
        private PageTitle PageTitle;
        List<OtherIncomeVM> OtherIncomeList;
        OtherIncomeVM otherIncome = new OtherIncomeVM();
        bool showAddrow = false;
        bool showEditrow = false;
        bool ShowDelete = false;
        bool  ShowConfirm = false;
        int OtherIncomeID;
        string ValidateMessageName = "";
        bool IsUsed = false;
        bool IsDisabled = false;

        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        {
            var ListReturn = await Http.GetAsync("api/OtherIncome/GetOtherIncome");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            OtherIncomeList = JsonConvert.DeserializeObject<List<OtherIncomeVM>>(vMs["data"].ToString());
        }
        protected async Task AddOtherIncome()
        {
            IsDisabled = true;
            if (ValidateMessageName == "" )
            {
                var AddModel = JsonConvert.SerializeObject(otherIncome);
                var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/OtherIncome/AddOtherIncome", content);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                    ShowConfirm = true;
                }
            }
        }
        void AddNewOtherIncome()
        {
             ValidateMessageName = "";
            showAddrow = true;
            showEditrow = false;
            otherIncome = new OtherIncomeVM();
        }
        protected async Task EditOldOtherIncome(int ID)
        {
            ValidateMessageName = "";
            showAddrow = false;
            showEditrow = true;
            string uri = "/api/OtherIncome/GetSpecificOtherIncome?Id=" + ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            otherIncome = JsonConvert.DeserializeObject<OtherIncomeVM>(vMs["data"].ToString());
        }

        protected async Task EditOtherIncome()
        {
            if (ValidateMessageName == "" )
            {
                var EditModel = JsonConvert.SerializeObject(otherIncome);
                var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/OtherIncome/EditOtherIncome", content);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                    ShowConfirm = true;
                }
            }
        }

        protected void DeleteOtherIncome(int ID)
        {
            OtherIncomeID = ID;
            ShowDelete = true;


        }
        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                string uri = "/api/OtherIncome/DeleteOtherIncome?ID=" + OtherIncomeID;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
                else
                {
                    IsUsed = true;
                }
            }
            ShowDelete = false;
        }
        protected void Cancel()
        {
            showAddrow = false;
            showEditrow = false;
        }
        protected void Confirm()
        {
            ShowConfirm = false;
            showAddrow = false;
            showEditrow = false;
        }
        protected async Task CheckName()
        {
            if (otherIncome.Name != null)
            {
                string uri = "/api/OtherIncome/CheckName?name=" + otherIncome.Name.Trim() + "&Id=" + otherIncome.ID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateMessageName = "الاسم موجود من قبل";
                }
                else
                {
                    ValidateMessageName = "";
                }
            }
        }

    }
}