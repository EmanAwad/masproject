﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasProject.Domain.ViewModel.Items;
using System.Net.Http;
using System.Net.Http.Json;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Components;
using System.Text;
using MatBlazor;
using MasProject.Client.Shared;

namespace MasProject.Client.Pages.Items.SellingExpenses
{
    public partial class SellingExpenses
    {
        private PageTitle PageTitle;
        List<SellingExpensesVM> SellingExpensesList;
        SellingExpensesVM selling = new SellingExpensesVM();
        bool showAddrow = false;
        bool showEditrow = false;
        bool ShowDelete = false;
        bool ShowConfirm = false;
        int sellingtionID;
        string ValidateMessageName = "";
        bool IsUsed = false;
        bool IsDisabled = false;

        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        {
            var ListReturn = await Http.GetAsync("api/SellingExpenses/GetSellingExpenses");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            SellingExpensesList = JsonConvert.DeserializeObject<List<SellingExpensesVM>>(vMs["data"].ToString());
        }
        protected async Task AddSellingExpenses()
        {
            IsDisabled = true;
            if (ValidateMessageName == "")
            {
                var AddModel = JsonConvert.SerializeObject(selling);
                var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/SellingExpenses/AddSellingExpenses", content);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                    ShowConfirm = true;
                }
            }
        }
        void AddNewSellingExpenses()
        {
            ValidateMessageName = "";
            showAddrow = true;
            showEditrow = false;
            selling = new SellingExpensesVM();
        }
        protected async Task EditOldSellingExpenses(int ID)
        {
            ValidateMessageName = "";
            showAddrow = false;
            showEditrow = true;
            string uri = "/api/SellingExpenses/GetSpecificSellingExpenses?Id=" + ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            selling = JsonConvert.DeserializeObject<SellingExpensesVM>(vMs["data"].ToString());
        }

        protected async Task EditSellingExpenses()
        {
            if (ValidateMessageName == "")
            {
                var EditModel = JsonConvert.SerializeObject(selling);
                var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/SellingExpenses/EditSellingExpenses", content);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                    ShowConfirm = true;
                }
            }
        }

        protected void DeleteSellingExpenses(int ID)
        {
            sellingtionID = ID;
            ShowDelete = true;


        }
        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                string uri = "/api/SellingExpenses/DeleteSellingExpenses?ID=" + sellingtionID;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
                else
                {
                    IsUsed = true;
                }
            }
            ShowDelete = false;
        }
        protected void Cancel()
        {
            showAddrow = false;
            showEditrow = false;
        }
        protected void Confirm()
        {
            ShowConfirm = false;
            showAddrow = false;
            showEditrow = false;
        }
        protected async Task CheckName()
        {
            if (selling.Name != null)
            {

                string uri = "/api/SellingExpenses/CheckName?name=" + selling.Name.Trim() + "&Id=" + selling.ID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateMessageName = "الاسم موجود من قبل";
                }
                else
                {
                    ValidateMessageName = "";
                }
            }
        }
    }
}