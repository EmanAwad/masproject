﻿using MasProject.Client.Shared;
using MasProject.Domain.ViewModel.Accounting;
using MasProject.Domain.ViewModel.Hierarchy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MasProject.Client.Pages.Accounting.EntriesScreen
{
    public partial class EntriesEdit
    {
        private PageTitle PageTitle;
        bool ShowConfirm = false;
        EntriesDetailsVM entryDetail = new EntriesDetailsVM();
        List<EntriesDetailsVM> EntriesDetailsList = new List<EntriesDetailsVM>();
        EntriesVM entry = new EntriesVM();
        List<EntriesTypeVM> EntriesTypes = new List<EntriesTypeVM>();
        List<BranchVM> BranchList = new List<BranchVM>();
        List<AccountingGuideVM> GuideList = new List<AccountingGuideVM>();
        List<AccountingGuideVM> CostCentersList = new List<AccountingGuideVM>();
        decimal CalculatedDebitTotal = 0;
        decimal CalculatedCreditTotal = 0;
        decimal CalculatedDifference = 0;
        bool IsDisabled = false;
        bool Diff = false;
        protected override async Task OnInitializedAsync()
        {
            await BindList();
            entryDetail = new EntriesDetailsVM();
            entryDetail = new EntriesDetailsVM { AccountName = "", Credit = 0, Debit = 0, };
            EntriesDetailsList = new List<EntriesDetailsVM>();
            EntriesDetailsList.Add(entryDetail);
            var querystring = ExtensionMethods.QueryString(navigationManager);
            var id = querystring["ID"];
            await EditOldEntries(int.Parse(id));
            var Type = querystring["Type"];
            if (Type == "View")
            {
                //disable controls
                IsDisabled = true;
                PageTitle.Title = "عرض القيود";
            }
            else
            {
                IsDisabled = false;
                PageTitle.Title = "تعديل  القيود";
            }

        }
        async Task BindList()
        {
            var Return = await Http.GetAsync("api/Entries/GetListDDL");
            var VMs = JObject.Parse(Return.Content.ReadAsStringAsync().Result);
            entry = JsonConvert.DeserializeObject<EntriesVM>(VMs["data"].ToString());
            CostCentersList = entry.CostsCentersList;
            GuideList = entry.GuidesList;
            EntriesTypes = entry.EntriesTypeList;
        }


        void AddNewRow()
        {
            entryDetail = new EntriesDetailsVM { AccountName = "" };
            EntriesDetailsList.Add(entryDetail);
            StateHasChanged();
        }
        void ChangeDR()
        {
            AddNewRow();
            RecalculateTotal();

        }
        void RemoveRow(EntriesDetailsVM row)
        {
            EntriesDetailsList.Remove(row);
            StateHasChanged();
        }

        protected async Task Confirm()
        {
            //ShowConfirm = false;
            Diff = false;
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        }
        void RecalculateTotal()
        {
            CalculatedDebitTotal = 0;
            CalculatedCreditTotal = 0;
            foreach (var rowitem in EntriesDetailsList)
            {
                CalculatedCreditTotal = (decimal)(CalculatedCreditTotal + rowitem.Credit);
                CalculatedDebitTotal = (decimal)(CalculatedDebitTotal + rowitem.Debit);
            }
            entryDetail.TotalDR = CalculatedDebitTotal;
            entryDetail.TotalCR = CalculatedCreditTotal;
            CalculatedDifference = 0;
            CalculatedDifference = (decimal)(entryDetail.TotalDR - entryDetail.TotalCR);
            entryDetail.DRCR_Difference = CalculatedDifference;
            StateHasChanged();
        }
        async Task ReloadCostCentersList()
        {


            var BranchListReturn = await Http.GetAsync("api/Guide/GetCostCenters");
            var BranchvMs = JObject.Parse(BranchListReturn.Content.ReadAsStringAsync().Result);
            CostCentersList = JsonConvert.DeserializeObject<List<AccountingGuideVM>>(BranchvMs["data"].ToString());

        }

        //void RecalculateDebitTotal()
        //{
        //    CalculatedDebitTotal = 0;
        //    foreach (var rowitem in EntriesDetailsList)
        //    {

        //        CalculatedDebitTotal = (decimal)(CalculatedDebitTotal + rowitem.Debit);
        //    }
        //    entryDetail.TotalDR = CalculatedDebitTotal;
        //    CalculatedDifference = 0;
        //    CalculatedDifference = (decimal)(entryDetail.TotalDR - entryDetail.TotalCR);
        //    entryDetail.DRCR_Difference = CalculatedDifference;
        //    StateHasChanged();
        //}
        //void RecalculateCreditTotal()
        //{
        //    CalculatedCreditTotal = 0;
        //    foreach (var rowitem in EntriesDetailsList)
        //    {

        //        CalculatedCreditTotal = (decimal)(CalculatedCreditTotal + rowitem.Credit);
        //    }
        //    entryDetail.TotalCR = CalculatedCreditTotal;
        //    CalculatedDifference = 0;
        //    CalculatedDifference = (decimal)(entryDetail.TotalDR - entryDetail.TotalCR);
        //    entryDetail.DRCR_Difference = CalculatedDifference;
        //    StateHasChanged();
        //}

        void RecalculateDifference()
        {
            CalculatedDifference = 0;
            CalculatedDifference = (decimal)(entryDetail.TotalCR - entryDetail.TotalCR);
            entryDetail.DRCR_Difference = CalculatedDifference;
        }

        protected async Task EditOldEntries(int ID)
        {
            string uri = "/api/Entries/GetSpecificEntry?ID=" + ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            entry = JsonConvert.DeserializeObject<EntriesVM>(vMs["data"].ToString());
            EntriesDetailsList = entry.EntriesDetailsList;
            RecalculateTotal();
        }

        protected async Task EditEntries()
        {
            if (CalculatedDifference == 0)
            {
                foreach (var rowitem in EntriesDetailsList)
                {
                    if (rowitem.AccountName != "" && rowitem.AccountName != null)
                    {
                        rowitem.AccountNo = GuideList.Find(x => x.Name == rowitem.AccountName).AccountCode;
                    }
                    if (rowitem.CostCenterName != "" && rowitem.CostCenterName != null)
                    {
                        rowitem.CostCenterId = CostCentersList.Find(x => x.Name == rowitem.CostCenterName).AccountCode;
                    }
                }
                entry.EntriesDetailsList = EntriesDetailsList;
                var EditModel = JsonConvert.SerializeObject(entry);
                var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/Entries/EditEntries", content);
                if (result.IsSuccessStatusCode)
                {
                    ShowConfirm = true;
                }

            }
            else
            {
                Diff = true;
            }
        }

        async Task ReloadAccountsList()
        {
            entryDetail.AccountName = String.Empty;

            var ListReturn = await Http.GetAsync("api/Guide/GetGuide");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            GuideList = JsonConvert.DeserializeObject<List<AccountingGuideVM>>(vMs["data"].ToString());

        }
    }
}
