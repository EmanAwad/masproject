﻿using MasProject.Client.Shared;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Accounting;
using MasProject.Domain.ViewModel.Hierarchy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;


namespace MasProject.Client.Pages.Accounting.EntriesScreen
{
    public partial class EntriesAdd
    {
        private PageTitle PageTitle;
        bool ShowConfirm = false;
        bool Diff = false;
        bool IsDisabled = false;
        EntriesDetailsVM entryDetail = new EntriesDetailsVM();
        List<EntriesDetailsVM> EntriesDetailsList = new List<EntriesDetailsVM>();
        EntriesVM entry = new EntriesVM();
        List<EntriesTypeVM> EntriesTypes = new List<EntriesTypeVM>();
        List<AccountingGuideVM> GuideList = new List<AccountingGuideVM>();
        List<AccountingGuideVM> CostCentersList = new List<AccountingGuideVM>();
        // AccountingGuideVM Guide = new AccountingGuideVM();
        decimal CalculatedDebitTotal = 0;
        decimal CalculatedCreditTotal = 0;
        decimal CalculatedDifference = 0;
        protected override async Task OnInitializedAsync()
        {
            await BindList();
            entryDetail = new EntriesDetailsVM();
            entryDetail = new EntriesDetailsVM { AccountName = "", Credit = 0, Debit = 0, };
            EntriesDetailsList = new List<EntriesDetailsVM>();
            EntriesDetailsList.Add(entryDetail);
        }
        async Task BindList()
        {
            var Return = await Http.GetAsync("api/Entries/GetListDDL");
            var VMs = JObject.Parse(Return.Content.ReadAsStringAsync().Result);
            entry = JsonConvert.DeserializeObject<EntriesVM>(VMs["data"].ToString());
            CostCentersList = entry.CostsCentersList;
            GuideList = entry.GuidesList;
            EntriesTypes = entry.EntriesTypeList;
        }
        void AddNewRow()
        {
            entryDetail = new EntriesDetailsVM { AccountName = "", Debit = 0, Credit = 0 };
            EntriesDetailsList.Add(entryDetail);
            StateHasChanged();
        }
        void ChangeDR()
        {
            AddNewRow();
            RecalculateTotal();

        }
        protected async Task DateChanged()
        {
            entry.EntryDate = entry.EntryDate.ToLocalTime();
        }
        protected async Task AddEntries()
        {
            IsDisabled = true;
            if (CalculatedDebitTotal == CalculatedCreditTotal)
            {
                foreach (var rowitem in EntriesDetailsList)
                {
                    if (rowitem.AccountName != "" && rowitem.AccountName != null)
                    {
                        rowitem.AccountNo = GuideList.Find(x => x.Name == rowitem.AccountName).AccountCode;
                    }
                    if (rowitem.CostCenterName != "" && rowitem.CostCenterName != null)
                    {
                        rowitem.CostCenterId = CostCentersList.Find(x => x.Name == rowitem.CostCenterName).AccountCode;
                    }
                }
                HttpResponseMessage result = new HttpResponseMessage();
                entry.EntriesDetailsList = EntriesDetailsList;
                entryDetail.TotalDR = CalculatedDebitTotal;
                entryDetail.TotalCR = CalculatedCreditTotal;
                entryDetail.DRCR_Difference = 0;
                var AddModel = JsonConvert.SerializeObject(entry);
                var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                result = await Http.PostAsync("api/Entries/AddEntries", content);
                var vMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                entry.ID = JsonConvert.DeserializeObject<int>(vMs["data"].ToString());
                if (result.IsSuccessStatusCode)
                {
                    ShowConfirm = true;
                }
            }
            else
            {
                Diff = true;
            }
        }

        //void GetAccountDataByCode()
        //{
        //    entryDetail.AccountName = GuideList.Find(x => x.Code == entryDetail.AccountNo).Name;
        //}AddEntriesStructure
        //void GetAccountData()
        //{
        //    entryDetail.AccountNo = GuideList.Find(x => x.Name == entryDetail.AccountName).AccountCode;
        //}


        void RemoveRow(EntriesDetailsVM row)
        {
            EntriesDetailsList.Remove(row);
            StateHasChanged();
        }

        protected async Task Confirm()
        {
            //ShowConfirm = false;
            Diff = false;
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        }

        void RecalculateTotal()
        {

            CalculatedDebitTotal = 0;
            CalculatedCreditTotal = 0;
            foreach (var rowitem in EntriesDetailsList)
            {
                CalculatedCreditTotal = (decimal)(CalculatedCreditTotal + rowitem.Credit);
                CalculatedDebitTotal = (decimal)(CalculatedDebitTotal + rowitem.Debit);
            }
            entryDetail.TotalDR = CalculatedDebitTotal;
            entryDetail.TotalCR = CalculatedCreditTotal;
            //    CalculatedDifference = 0;
            CalculatedDifference = (decimal)(entryDetail.TotalDR - entryDetail.TotalCR);
            entryDetail.DRCR_Difference = CalculatedDifference;
            StateHasChanged();

        }
        //void RecalculateCreditTotal()
        //{
        //    CalculatedCreditTotal = 0;
        //    foreach (var rowitem in EntriesDetailsList)
        //    {

        //        CalculatedCreditTotal = (decimal)(CalculatedCreditTotal + rowitem.Credit);
        //    }
        //    entryDetail.TotalCR = CalculatedCreditTotal;
        //    CalculatedDifference = 0;
        //    CalculatedDifference = (decimal)(entryDetail.TotalDR - entryDetail.TotalCR);
        //    entryDetail.DRCR_Difference = CalculatedDifference;
        //    StateHasChanged();
        //}

        void RecalculateDifference()
        {
            CalculatedDifference = 0;
            CalculatedDifference = (decimal)(entryDetail.TotalCR - entryDetail.TotalDR);
            entryDetail.DRCR_Difference = CalculatedDifference;
        }
        async Task ReloadAccountsList()
        {
            entryDetail.AccountName = String.Empty;

            var ListReturn = await Http.GetAsync("api/Guide/GetGuide");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            GuideList = JsonConvert.DeserializeObject<List<AccountingGuideVM>>(vMs["data"].ToString());

        }
        async Task ReloadCostCentersList()
        {


            var BranchListReturn = await Http.GetAsync("api/Guide/GetCostCenters");
            var BranchvMs = JObject.Parse(BranchListReturn.Content.ReadAsStringAsync().Result);
            CostCentersList = JsonConvert.DeserializeObject<List<AccountingGuideVM>>(BranchvMs["data"].ToString());

        }
        //protected async Task GetAccountNameByCode(EntriesDetailsVM row)
        //{
        //    var ListReturn = await Http.GetAsync("api/Guide/GetAccountName");
        //    string uri = "/api/Guide/GetAccountName?code=" + row.AccountNo;
        //    HttpResponseMessage response = await Http.GetAsync(uri);
        //    var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
        //    Guide = JsonConvert.DeserializeObject<AccountingGuideVM>(vMs["data"].ToString());
        //    row.AccountName = Guide.Name;
        //}
        //protected async Task GetAccountNo(EntriesDetailsVM row)
        //{
        //    Guide.ID = GuideList.Find(x => x.Name == entryDetail.AccountName).ID;
        //    string uri = "/api/Guide/GetAccountCode?id=" + Guide.ID;
        //    HttpResponseMessage response = await Http.GetAsync(uri);
        //    var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
        //    EntriesDetailsVM model = JsonConvert.DeserializeObject<EntriesDetailsVM>(vMs["data"].ToString());
        //    row.AccountNo = entryDetail.AccountNo;
        //    entryDetail.AccountNo = row.AccountNo;
        //    // Guide.AccountCode = model.AccountCode;
        //    StateHasChanged();
        //}
        // protected async Task GetAccountingGuide(EntriesDetailsVM row)
        //{
        //    if (Guide.ParentCode != null)
        //    {
        //        AccountingGuideVM SelectedGuide = AccountingGuideList.Find(x => x.AccountCode == Guide.ParentCode);
        //        Guide.ParentId = SelectedGuide.ID;
        //        string uri = "/api/Guide/GenerateChildCode?ParentId=" + Guide.ParentId;
        //        HttpResponseMessage response = await Http.GetAsync(uri);
        //        var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
        //        AccountingGuideVM model = JsonConvert.DeserializeObject<AccountingGuideVM>(vMs["data"].ToString());
        //        row.AccountNo = model.Code;

        //        //Guide.AccountCode = model.AccountCode;
        //        StateHasChanged();
        //    }
        //}
    }
}