﻿using MasProject.Client.Shared;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.JSInterop;
using MasProject.Domain.ViewModel.Accounting;

namespace MasProject.Client.Pages.Accounting.EntriesScreen
{
    public partial class Entries
    {
        private PageTitle PageTitle;
        List<EntriesVM> EntriesList;
        bool ShowDelete = false;
        int EntryID;

        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        {
            //Fill Table
            var ListReturn = await Http.GetAsync("api/Entries/GetEntries");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            EntriesList = JsonConvert.DeserializeObject<List<EntriesVM>>(vMs["data"].ToString());
        }



        protected async Task AddNewEntry()
        {
            await jsRuntime.InvokeAsync<object>("open", "/EntriesAdd", "_blank");
        }


        protected async Task EditOldEntries(int ID, string Type)
        {
            await jsRuntime.InvokeAsync<object>("open", "/EntriesEdit?ID=" + ID + "&Type=" + Type, "_blank");
        }

        protected void DeleteEntries(int ID)
        {
            EntryID = ID;//ashraqat enty nsety t7oty l id ely gay mn table
            ShowDelete = true;
        }

        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                string uri = "/api/Entries/DeleteEntries?ID=" + EntryID;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
            ShowDelete = false;
        }
    }
}