﻿using MasProject.Client.Shared;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Accounting;
using MasProject.Domain.ViewModel.Hierarchy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
namespace MasProject.Client.Pages.Accounting.EntriesStructuring
{
    public partial class EntriesStructuringAdd
    {
        private PageTitle PageTitle;
        bool ShowConfirm = false;
        bool Diff = false;
        bool IsDisabled = false;
        EntriesStructuring_DetailsVM EntriesStructuring_Detail = new EntriesStructuring_DetailsVM();
        List<EntriesStructuring_DetailsVM> EntriesStructuring_DetailsList = new List<EntriesStructuring_DetailsVM>();
        EntriesStructuringVM entryStructure = new EntriesStructuringVM();
        List<AccountingGuideVM> GuideList = new List<AccountingGuideVM>();
        List<AccountingGuideVM> CostCentersList = new List<AccountingGuideVM>();
        // AccountingGuideVM Guide = new AccountingGuideVM();
        List<EntriesTypeVM> EntriesTypes = new List<EntriesTypeVM>();
        decimal CalculatedDebitTotal = 0;
        decimal CalculatedCreditTotal = 0;
        decimal CalculatedDifference = 0;


        protected override async Task OnInitializedAsync()
        {
            await BindList();
            EntriesStructuring_Detail = new EntriesStructuring_DetailsVM();
            EntriesStructuring_Detail = new EntriesStructuring_DetailsVM { AccountName = "", Credit = 0, Debit = 0, };
            EntriesStructuring_DetailsList = new List<EntriesStructuring_DetailsVM>();
            EntriesStructuring_DetailsList.Add(EntriesStructuring_Detail);
            //entryStructure = new EntriesStructuringVM();
            //entryStructure.EntryDate = DateTime.Now;

            //var IdReturn = await Http.GetAsync("api/EntriesStructure/GenerateEntryStructureNo");
            //var IdvMs = JObject.Parse(IdReturn.Content.ReadAsStringAsync().Result);
            //entryStructure.EntriesStructureNo = JsonConvert.DeserializeObject<int>(IdvMs["data"].ToString());

            //var GuideListReturn = await Http.GetAsync("api/Guide/GetGuide");
            //var GuidevMs = JObject.Parse(GuideListReturn.Content.ReadAsStringAsync().Result);
            //GuideList = JsonConvert.DeserializeObject<List<AccountingGuideVM>>(GuidevMs["data"].ToString());

            //EntriesStructuring_Detail.TotalCR = 0;
            //EntriesStructuring_Detail.TotalDR = 0;

            //try
            //{
            //    var BranchListReturn = await Http.GetAsync("api/Guide/GetCostCenters");
            //    var BranchvMs = JObject.Parse(BranchListReturn.Content.ReadAsStringAsync().Result);
            //    CostCentersList = JsonConvert.DeserializeObject<List<AccountingGuideVM>>(BranchvMs["data"].ToString());
            //}
            //catch (Exception)
            //{
            //    CostCentersList = new List<AccountingGuideVM>();
            //}
        }
        async Task BindList()
        {
            var Return = await Http.GetAsync("api/EntriesStructure/GetListDDL");
            var VMs = JObject.Parse(Return.Content.ReadAsStringAsync().Result);
            entryStructure = JsonConvert.DeserializeObject<EntriesStructuringVM>(VMs["data"].ToString());
            CostCentersList = entryStructure.CostsCentersList;
            GuideList = entryStructure.GuidesList;
            EntriesTypes = entryStructure.EntriesTypeList;
        }
        void ChangeDR()
        {
            AddNewRow();
            RecalculateTotal();

        }
        protected async Task AddEntriesStructure()
        {
            IsDisabled = true;
            if (CalculatedDebitTotal == CalculatedCreditTotal)
            {
                foreach (var rowitem in EntriesStructuring_DetailsList)
                {
                    if (rowitem.AccountName != "" && rowitem.AccountName != null)
                    {
                        rowitem.AccountNo = GuideList.Find(x => x.Name == rowitem.AccountName).AccountCode;
                    }
                    if (rowitem.CostCenterName != "" && rowitem.CostCenterName != null)
                    {
                        rowitem.CostCenterId = CostCentersList.Find(x => x.Name == rowitem.CostCenterName).AccountCode;
                    }
                }
                HttpResponseMessage result = new HttpResponseMessage();
                entryStructure.EntriesStructuringList = EntriesStructuring_DetailsList;
                var AddModel = JsonConvert.SerializeObject(entryStructure);
                var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                result = await Http.PostAsync("api/EntriesStructure/AddEntries", content);
                var vMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                entryStructure.ID = JsonConvert.DeserializeObject<int>(vMs["data"].ToString());
                if (result.IsSuccessStatusCode)
                {
                    ShowConfirm = true;
                }
            }
            else
            {
                Diff = true;
            }
        }

        void AddNewRow()
        {
            EntriesStructuring_Detail = new EntriesStructuring_DetailsVM { AccountName = "", Credit = 0, Debit = 0, };
            EntriesStructuring_DetailsList.Add(EntriesStructuring_Detail);
            StateHasChanged();
        }
        void RemoveRow(EntriesStructuring_DetailsVM row)
        {
            EntriesStructuring_DetailsList.Remove(row);
            StateHasChanged();
        }

        protected async Task Confirm()
        {

            Diff = false;
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        }
        void RecalculateTotal()
        {
            CalculatedDebitTotal = 0;
            CalculatedCreditTotal = 0;
            foreach (var rowitem in EntriesStructuring_DetailsList)
            {
                CalculatedCreditTotal = (decimal)(CalculatedCreditTotal + rowitem.Credit);
                CalculatedDebitTotal = (decimal)(CalculatedDebitTotal + rowitem.Debit);
            }
            EntriesStructuring_Detail.TotalDR = CalculatedDebitTotal;
            EntriesStructuring_Detail.TotalCR = CalculatedCreditTotal;
            //CalculatedDifference = 0;
            CalculatedDifference = (decimal)(EntriesStructuring_Detail.TotalDR - EntriesStructuring_Detail.TotalCR);
            EntriesStructuring_Detail.DRCR_Difference = CalculatedDifference;
            StateHasChanged();
        }

        void RecalculateDifference()
        {
            CalculatedDifference = 0;
            CalculatedDifference = (decimal)(EntriesStructuring_Detail.TotalCR - EntriesStructuring_Detail.TotalDR);
            EntriesStructuring_Detail.DRCR_Difference = CalculatedDifference;
        }
        async Task ReloadAccountsList()
        {
            EntriesStructuring_Detail.AccountName = String.Empty;

            var ListReturn = await Http.GetAsync("api/Guide/GetGuide");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            GuideList = JsonConvert.DeserializeObject<List<AccountingGuideVM>>(vMs["data"].ToString());

        }

        async Task ReloadCostCentersList()
        {


            var BranchListReturn = await Http.GetAsync("api/Guide/GetCostCenters");
            var BranchvMs = JObject.Parse(BranchListReturn.Content.ReadAsStringAsync().Result);
            CostCentersList = JsonConvert.DeserializeObject<List<AccountingGuideVM>>(BranchvMs["data"].ToString());

        }
    }
}
