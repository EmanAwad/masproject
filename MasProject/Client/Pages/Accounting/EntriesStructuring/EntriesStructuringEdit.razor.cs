﻿using MasProject.Client.Shared;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Accounting;
using MasProject.Domain.ViewModel.Hierarchy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MasProject.Client.Pages.Accounting.EntriesStructuring
{
    public partial class EntriesStructuringEdit
    {
        private PageTitle PageTitle;
        bool ShowConfirm = false;
        bool Diff = false;
        bool IsDisabled = false;
        EntriesStructuring_DetailsVM EntriesStructuring_Detail = new EntriesStructuring_DetailsVM();
        List<EntriesStructuring_DetailsVM> EntriesStructuring_DetailsList = new List<EntriesStructuring_DetailsVM>();
        EntriesStructuringVM entryStructure = new EntriesStructuringVM();
        List<AccountingGuideVM> GuideList = new List<AccountingGuideVM>();
        List<AccountingGuideVM> CostCentersList = new List<AccountingGuideVM>();
        AccountingGuideVM Guide = new AccountingGuideVM();
        List<EntriesTypeVM> Entries = new List<EntriesTypeVM>();
        decimal CalculatedDebitTotal = 0;
        decimal CalculatedCreditTotal = 0;
        decimal CalculatedDifference = 0;

        protected override async Task OnInitializedAsync()
        {
            //entryDetail = new EntriesDetailsVM();
            EntriesStructuring_Detail = new EntriesStructuring_DetailsVM();
            EntriesStructuring_Detail = new EntriesStructuring_DetailsVM { AccountName = "", Credit = 0, Debit = 0, };
            EntriesStructuring_DetailsList = new List<EntriesStructuring_DetailsVM>();
            EntriesStructuring_DetailsList.Add(EntriesStructuring_Detail);
            entryStructure = new EntriesStructuringVM();
            entryStructure.EntryDate = DateTime.Now;

            var IdReturn = await Http.GetAsync("api/EntriesStructure/GenerateEntryStructureNo");
            var IdvMs = JObject.Parse(IdReturn.Content.ReadAsStringAsync().Result);
            entryStructure.EntriesStructureNo = JsonConvert.DeserializeObject<int>(IdvMs["data"].ToString());

            var GuideListReturn = await Http.GetAsync("api/Guide/GetGuide");
            var GuidevMs = JObject.Parse(GuideListReturn.Content.ReadAsStringAsync().Result);
            GuideList = JsonConvert.DeserializeObject<List<AccountingGuideVM>>(GuidevMs["data"].ToString());

            EntriesStructuring_Detail.TotalCR = 0;
            EntriesStructuring_Detail.TotalDR = 0;
            var querystring = ExtensionMethods.QueryString(navigationManager);
            var id = querystring["ID"];
            await EditOldEntriesStructuring(int.Parse(id));
            var Type = querystring["Type"];
            if (Type == "View")
            {
                //disable controls
                IsDisabled = true;
                PageTitle.Title = "عرض هيكلة القيود";
            }
            else
            {
                IsDisabled = false;
                PageTitle.Title = "تعديل  هيكلة القيود";
            }

        }

        void AddNewRow()
        {
            EntriesStructuring_Detail = new EntriesStructuring_DetailsVM { AccountName = "", Credit = 0, Debit = 0, };
            EntriesStructuring_DetailsList.Add(EntriesStructuring_Detail);
            StateHasChanged();
        }
        void RemoveRow(EntriesStructuring_DetailsVM row)
        {
            EntriesStructuring_DetailsList.Remove(row);
            StateHasChanged();
        }

        protected async Task Confirm()
        {

            Diff = false;
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        }

        void RecalculateTotal()
        {
            CalculatedDebitTotal = 0;
            CalculatedCreditTotal = 0;
            foreach (var rowitem in EntriesStructuring_DetailsList)
            {
                CalculatedCreditTotal = (decimal)(CalculatedCreditTotal + rowitem.Credit);
                CalculatedDebitTotal = (decimal)(CalculatedDebitTotal + rowitem.Debit);
            }
            EntriesStructuring_Detail.TotalDR = CalculatedDebitTotal;
            EntriesStructuring_Detail.TotalCR = CalculatedCreditTotal;
            CalculatedDifference = 0;
            CalculatedDifference = (decimal)(EntriesStructuring_Detail.TotalDR - EntriesStructuring_Detail.TotalCR);
            EntriesStructuring_Detail.DRCR_Difference = CalculatedDifference;
            StateHasChanged();
        }

        void RecalculateDifference()
        {
            CalculatedDifference = 0;
            CalculatedDifference = (decimal)(EntriesStructuring_Detail.TotalCR - EntriesStructuring_Detail.TotalCR);
            EntriesStructuring_Detail.DRCR_Difference = CalculatedDifference;
        }
        async Task ReloadAccountsList()
        {
            EntriesStructuring_Detail.AccountName = String.Empty;

            var ListReturn = await Http.GetAsync("api/Guide/GetGuide");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            GuideList = JsonConvert.DeserializeObject<List<AccountingGuideVM>>(vMs["data"].ToString());

        }

        async Task ReloadCostCentersList()
        {


            var BranchListReturn = await Http.GetAsync("api/Guide/GetCostCenters");
            var BranchvMs = JObject.Parse(BranchListReturn.Content.ReadAsStringAsync().Result);
            CostCentersList = JsonConvert.DeserializeObject<List<AccountingGuideVM>>(BranchvMs["data"].ToString());

        }

        protected async Task EditOldEntriesStructuring(int ID)
        {
            string uri = "/api/EntriesStructure/GetSpecificEntryStructure?ID=" + ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            entryStructure = JsonConvert.DeserializeObject<EntriesStructuringVM>(vMs["data"].ToString());
            EntriesStructuring_DetailsList = entryStructure.EntriesStructuringList;
        }
         
        protected async Task EditEntriesStructure()
        {
            if (CalculatedDifference == 0)
            {
                foreach (var rowitem in EntriesStructuring_DetailsList)
                {
                    if (rowitem.AccountName != "" && rowitem.AccountName != null)
                    {
                        rowitem.AccountNo = GuideList.Find(x => x.Name == rowitem.AccountName).AccountCode;
                    }
                    if (rowitem.CostCenterName != "" && rowitem.CostCenterName != null)
                    {
                        rowitem.CostCenterId = CostCentersList.Find(x => x.Name == rowitem.CostCenterName).AccountCode;
                    }
                }
                entryStructure.EntriesStructuringList = EntriesStructuring_DetailsList;
                var EditModel = JsonConvert.SerializeObject(entryStructure);
                var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/EntriesStructure/EditEntriesStructure", content);
                if (result.IsSuccessStatusCode)
                {
                    ShowConfirm = true;
                }

            }
            else
            {
                Diff = true;
            }
        }

    }
}
