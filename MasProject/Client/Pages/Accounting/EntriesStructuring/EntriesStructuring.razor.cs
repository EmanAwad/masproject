﻿using MasProject.Client.Shared;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using Microsoft.JSInterop;
using MasProject.Domain.ViewModel.Accounting;

namespace MasProject.Client.Pages.Accounting.EntriesStructuring
{
    public partial class EntriesStructuring
    {
        private PageTitle PageTitle;
        List<EntriesStructuringVM> EntriesStructuringList;
        bool ShowDelete = false;
        int EntryID;

        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        {
            //Fill Table
            var ListReturn = await Http.GetAsync("api/EntryStructure/GetEntryStructures");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            EntriesStructuringList = JsonConvert.DeserializeObject<List<EntriesStructuringVM>>(vMs["data"].ToString());
        }

        protected async Task AddNewEntry()
        {
            await jsRuntime.InvokeAsync<object>("open", "/EntriesStructuringAdd", "_blank");
        }


        protected async Task EditOldEntriesStructuring(int ID, string Type)
        {
            await jsRuntime.InvokeAsync<object>("open", "/EntriesStructuringEdit?ID=" + ID + "&Type=" + Type, "_blank");
        }

        protected void DeleteEntries(int ID)
        {
            EntryID = ID;
            ShowDelete = true;
        }

        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                string uri = "/api/EntryStructure/GenerateEntryStructureNo?ID=" + EntryID;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
            ShowDelete = false;
        }
    }
}
