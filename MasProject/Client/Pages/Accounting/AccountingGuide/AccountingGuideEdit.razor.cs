﻿using MasProject.Client.Shared;
using MasProject.Domain.ViewModel.Accounting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
namespace MasProject.Client.Pages.Accounting.AccountingGuide
{
    public partial class AccountingGuideEdit
    {
        private PageTitle PageTitle;
        AccountingGuideVM Guide = new AccountingGuideVM();
        List<AccountingGuideVM> AccountingGuideList = new List<AccountingGuideVM>();
        bool EnterNameChecker = false;
        bool ShowConfirm = false;
        int GuideID;
        int FlagType = 0;
        bool IsDisabled = true;
        protected override async Task OnInitializedAsync()
        {
            var querystring = ExtensionMethods.QueryString(navigationManager);
            var id = querystring["ID"];
            await EditOldGuide(int.Parse(id));
            var Type = querystring["Type"];
            if (Type == "View")
            {
                //disable controls
                IsDisabled = true;
                PageTitle.Title = "عرض دليل الحسابات ";
            }
            else
            {
                IsDisabled = false;
                PageTitle.Title = "تعديل  دليل الحسابات";
            }
        }
        protected async Task EditOldGuide(int ID)
        {
            string uri = "/api/Guide/GetSpecificGuide?ID=" + ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            Guide = JsonConvert.DeserializeObject<AccountingGuideVM>(vMs["data"].ToString());
            AccountingGuideList = Guide.AccountingGuideList;
            FlagType = Guide.FlagType == false ? 0 : 1;

        }
        protected async Task EditAccountingGuide()
        {
            HttpResponseMessage result = new HttpResponseMessage();
            if (Guide.Name != null && Guide.Name != "")
            {
                Guide.FlagType = FlagType == 0 ? false : true;
                var AddModel = JsonConvert.SerializeObject(Guide);
                var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                result = await Http.PostAsync("api/Guide/EditGuide", content);
                var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                if (result.IsSuccessStatusCode)
                {
                    ShowConfirm = true;
                }
            }
            else
            {
                EnterNameChecker = true;
            }

            StateHasChanged();
        }
        protected async Task GetAccountingGuide()
        {
            AccountingGuideVM SelectedGuide = AccountingGuideList.Find(x => x.AccountCode == Guide.ParentCode);
            Guide.ParentId = SelectedGuide.ID;
            string uri = "/api/Guide/GenerateChildCode?ParentId=" + Guide.ParentId;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            AccountingGuideVM model = JsonConvert.DeserializeObject<AccountingGuideVM>(vMs["data"].ToString());
            Guide.Code = model.Code;
         //   Guide.AccountCode = model.AccountCode;
            StateHasChanged();
        }
        protected async Task GenerateCode()
        {
            AccountingGuideVM SelectedGuide = AccountingGuideList.Find(x => x.ID == Guide.ParentId);
            Guide.ParentCode = SelectedGuide.AccountCode;
            string uri = "/api/Guide/GenerateChildCode?ParentId=" + Guide.ParentId;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            AccountingGuideVM model = JsonConvert.DeserializeObject<AccountingGuideVM>(vMs["data"].ToString());
            Guide.Code = model.Code;
         //   Guide.AccountCode = model.AccountCode;
            StateHasChanged();
        }
        protected async Task Confirm()
        {
            ShowConfirm = false;
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        }
    }
}
