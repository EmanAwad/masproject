﻿using MasProject.Client.Shared;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.JSInterop;
using MasProject.Domain.ViewModel.Accounting;
namespace MasProject.Client.Pages.Accounting.AccountingGuide
{
    public partial class AccountingGuide
    {
        private PageTitle PageTitle;
        List<AccountingGuideVM> AccountingGuideList;
        bool ShowDelete = false;
        int GuideID;
        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        {
            //Fill Table
            var ListReturn = await Http.GetAsync("api/Guide/GetGuide");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            AccountingGuideList = JsonConvert.DeserializeObject<List<AccountingGuideVM>>(vMs["data"].ToString());
        }
        protected async Task AddNewGuide()
        {
            await jsRuntime.InvokeAsync<object>("open", "/AccountingGuideAdd", "_blank");
        }
        protected async Task EditOldGuide(int ID, string Type)
        {
            await jsRuntime.InvokeAsync<object>("open", "/AccountingGuideEdit?ID=" + ID + "&Type=" + Type, "_blank");
        }
        protected void DeleteGuide(int id)
        {
            ShowDelete = true;
            GuideID = id;
        }
        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                string uri = "/api/Guide/DeleteGuide?ID=" + GuideID;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
            ShowDelete = false;
        }

    }
}
