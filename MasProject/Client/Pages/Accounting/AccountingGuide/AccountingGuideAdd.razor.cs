﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using MasProject.Domain.ViewModel.Accounting;
namespace MasProject.Client.Pages.Accounting.AccountingGuide
{
    public partial class AccountingGuideAdd
    {
        private PageTitle PageTitle;
        AccountingGuideVM Guide = new AccountingGuideVM();
        List<AccountingGuideVM> AccountingGuideList = new List<AccountingGuideVM>();
        bool EnterNameChecker = false;
        bool ShowConfirm = false;
        int GuideID;
        int FlagType = 0;
        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        {
            var ListReturn = await Http.GetAsync("api/Guide/GetListDDL");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            Guide = JsonConvert.DeserializeObject<AccountingGuideVM>(vMs["data"].ToString());
            AccountingGuideList = Guide.AccountingGuideList;
        }
        protected async Task AddAccountingGuide()
        {
            HttpResponseMessage result = new HttpResponseMessage();
            if (Guide.Name != null && Guide.Name != "")
            {
                Guide.FlagType = FlagType == 0 ? false : true;
                var AddModel = JsonConvert.SerializeObject(Guide);
                var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                result = await Http.PostAsync("api/Guide/AddGuide", content);
                var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                Guide.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                if (result.IsSuccessStatusCode)
                {
                    ShowConfirm = true;
                }
            }
            else
            {
                EnterNameChecker = true;
            }

            StateHasChanged();
        }
        protected async Task GetAccountingGuide()
        {
            if (Guide.ParentCode != null)
            {
                AccountingGuideVM SelectedGuide = AccountingGuideList.Find(x => x.AccountCode == Guide.ParentCode);
                Guide.ParentId = SelectedGuide.ID;
                string uri = "/api/Guide/GenerateChildCode?ParentId=" + Guide.ParentId;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                AccountingGuideVM model = JsonConvert.DeserializeObject<AccountingGuideVM>(vMs["data"].ToString());
                Guide.Code = model.Code;
                //Guide.AccountCode = model.AccountCode;
                StateHasChanged();
            }
        }
        protected async Task GenerateCode()
        {
            AccountingGuideVM SelectedGuide = AccountingGuideList.Find(x => x.ID == Guide.ParentId);
            Guide.ParentCode = SelectedGuide.AccountCode;
            string uri = "/api/Guide/GenerateChildCode?ParentId=" + Guide.ParentId;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            AccountingGuideVM model = JsonConvert.DeserializeObject<AccountingGuideVM>(vMs["data"].ToString());
            Guide.Code = model.Code;
           // Guide.AccountCode = model.AccountCode;
            StateHasChanged();
        }
        protected async Task Confirm()
        {
            ShowConfirm = false;
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        }
    }
}
