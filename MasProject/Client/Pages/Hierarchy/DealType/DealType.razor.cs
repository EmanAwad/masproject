﻿using MasProject.Domain.ViewModel.Hierarchy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;

namespace MasProject.Client.Pages.Hierarchy.DealType
{
    public partial class DealType
    {
        private PageTitle PageTitle;

        List<DealTypeVM> DealTypeList;
        DealTypeVM Deal = new DealTypeVM();
        bool showAddrow = false;
        bool showEditrow = false; 
        bool ShowDelete = false; 
        bool ShowConfirm = false;
        string ValidateMessageName = "";
        int DealTypeID;

        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        {
            var ListReturn = await Http.GetAsync("api/DealType/GetDealType");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            DealTypeList = JsonConvert.DeserializeObject<List<DealTypeVM>>(vMs["data"].ToString());
        }
        protected async Task CheckName()
        {
            string uri = "/api/DealType/CheckNameDealType?name=" + Deal.Name + "&Id=" + Deal.ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            string result = vMs["data"].ToString();
            if (result == "True")
            {
                ValidateMessageName = "الاسم موجود من قبل";
            }
            else
            {
                ValidateMessageName = "";
            }
        }
        protected async Task AddDealType()
        {
            if (ValidateMessageName == "")
            {
                var AddModel = JsonConvert.SerializeObject(Deal);
                var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/DealType/AddDealType", content);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                    ShowConfirm = true;
                    showAddrow = false;
                }
            }
        }
        protected async Task AddNewDealType()
        {
            await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/DealTypeAdd", "جارى التحميل",650);
        }
        protected void Cancel()
        {
            showAddrow = false;
            showEditrow = false;
        }
        protected void Confirm()
        {
            ShowConfirm = false;
            showAddrow = false;
            showEditrow = false;
        }
        protected async Task EditOldDealType(int ID)
        {
            await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/DealTypeEdit?ID=" + ID, "جارى التحميل",650);
          
        }

        protected async Task EditDealType()
        {
            if (ValidateMessageName == "")
            {
                var EditModel = JsonConvert.SerializeObject(Deal);
                var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/DealType/EditDealType", content);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                    ShowConfirm = true;
                    showEditrow = false;
                }
            }
        }
        protected void DeleteDealType(int Id)
        {
            DealTypeID = Id;
            ShowDelete = true;
        }
        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            ShowDelete = false;
            if (deleteConfirmed)
            {
                string uri = "/api/DealType/DeleteDealType?ID=" + DealTypeID;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
        }
    }
}