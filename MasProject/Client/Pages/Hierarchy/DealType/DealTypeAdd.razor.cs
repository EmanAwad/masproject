﻿using MasProject.Domain.ViewModel.Hierarchy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using MasProject.Domain.ViewModel;

namespace MasProject.Client.Pages.Hierarchy.DealType
{
    public partial class DealTypeAdd
    {
        private PageTitle PageTitle;

        List<DealTypeVM> DealTypeList;
        DealTypeVM Deal = new DealTypeVM();
        bool showAddrow = false;
        bool showEditrow = false;
        bool ShowDelete = false;
        bool ShowConfirm = false;
        string ValidateMessageName = "";
        string ValidateArrangementMessage = "";
        int DealTypeID;
        List<LookupKeyValueVM> PriceTypeList = new List<LookupKeyValueVM>();
        bool IsDisabled = false;
      
        protected override async Task OnInitializedAsync()
        {
            var ListReturn = await Http.GetAsync("api/DealType/GetPriceTypes");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            Deal = JsonConvert.DeserializeObject<DealTypeVM>(vMs["data"].ToString());
            PriceTypeList = Deal.PriceTypeList;
        }
        protected async Task CheckName()
        {
            if (Deal.Name != null)
            {
                string uri = "/api/DealType/CheckNameDealType?name=" + Deal.Name.Trim() + "&Id=" + Deal.ID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateMessageName = "الاسم موجود من قبل";
                }
                else
                {
                    ValidateMessageName = "";
                }
            }
        }
        protected async Task CheckArrangement()
        {
            if (Deal.arrangement != null)
            {
                string uri = "/api/DealType/CheckArrangement?arrangement=" + Deal.arrangement + "&Id=" + Deal.ID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateArrangementMessage = "الترتيب موجود من قبل";
                }
                else
                {
                    ValidateArrangementMessage = "";
                }
            }
        }
        protected async Task AddDealType()
        {
            IsDisabled = true;
            if (ValidateMessageName == "" & ValidateArrangementMessage == "")
            {
                var AddModel = JsonConvert.SerializeObject(Deal);
                var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/DealType/AddDealType", content);
                if (result.IsSuccessStatusCode)
                {
                  
                    ShowConfirm = true;
                    showAddrow = false;
                }
            }
        }

        protected async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        }


        

    }
}

