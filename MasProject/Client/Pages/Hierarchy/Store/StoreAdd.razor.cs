﻿using MasProject.Domain.ViewModel.Hierarchy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using MasProject.Domain.ViewModel.Persons;

namespace MasProject.Client.Pages.Hierarchy.Store
{
    public partial class StoreAdd
    {
        private PageTitle PageTitle;

        List<StoreVM> StoreList;
        StoreVM StoreModel = new StoreVM();
        List<BranchVM> BranchList = new List<BranchVM>();
        List<EmployeeVM> EmpList = new List<EmployeeVM>();
        bool ShowConfirm = false;
        string ValidateMessageName = "";
        string ValidateMessageBranch = "";
        string ValidateMessageArrangement = "";
        bool IsDisabled = false;

        async Task BindList()
        {
            
            //Fill DropDown
            var ListReturndrp = await Http.GetAsync("api/Branch/GetBranchs");
            var vMMs = JObject.Parse(ListReturndrp.Content.ReadAsStringAsync().Result);
            BranchList = JsonConvert.DeserializeObject<List<BranchVM>>(vMMs["data"].ToString());
            
            //Fill DropDown
            var ListReturndrp2 = await Http.GetAsync("api/Employee/GetEmployees");
            var vMMss = JObject.Parse(ListReturndrp2.Content.ReadAsStringAsync().Result);
            EmpList = JsonConvert.DeserializeObject<List<EmployeeVM>>(vMMss["data"].ToString());
        }
        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }

        protected async Task CheckName()
        {
            if (StoreModel.Name != null)
            {
                string uri = "/api/Store/CheckNameStore?name=" + StoreModel.Name.Trim() + "&Id=" + StoreModel.ID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateMessageName = "الاسم موجود من قبل";
                }
                else
                {
                    ValidateMessageName = "";
                }
            }
        }
        protected async Task CheckArrangement()
        {
            if (StoreModel.arrangement != null)
            {
                string uri = "/api/Store/CheckArrangement?arrangement=" + StoreModel.arrangement + "&Id=" + StoreModel.ID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateMessageArrangement = "الترتيب موجود من قبل";
                }
                else
                {
                    ValidateMessageArrangement = "";
                }
            }
        }
        protected async Task AddStore()
        {
            IsDisabled = true;
            //if (StoreModel.BranchID == 0)
            //{
            //    ValidateMessageBranch = "عليك اختيار الفرع";
            //}
            //else
            //{
            //    ValidateMessageBranch = ""; 
            //}
            if (ValidateMessageName == ""  && ValidateMessageArrangement == "")
            {
                var AddModel = JsonConvert.SerializeObject(StoreModel);
            var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
            var result = await Http.PostAsync("api/Store/AddStore", content);
            if (result.IsSuccessStatusCode)
            {
                
                ShowConfirm = true;
            }
            }
        }
        protected async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        }

    }
}
