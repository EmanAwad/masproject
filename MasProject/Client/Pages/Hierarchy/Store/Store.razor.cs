﻿using MasProject.Domain.ViewModel.Hierarchy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;

namespace MasProject.Client.Pages.Hierarchy.Store
{
    public partial class Store
    {
        private PageTitle PageTitle;

        List<StoreVM> StoreList;
        StoreVM StoreModel = new StoreVM();
        List<BranchVM> BranchList = new List<BranchVM>();
        bool showAddrow = false;
        bool showEditrow = false;
        bool ShowDelete = false;
        bool ShowConfirm = false;
        string ValidateMessageName = "";
        string ValidateMessageBranch = "";
        string ValidateMessageArrangement = "";
        bool StoreUsed = false;

        int StoreID;
        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        { 
            //Fill Table
            var ListReturn = await Http.GetAsync("api/Store/GetStores");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            StoreList = JsonConvert.DeserializeObject<List<StoreVM>>(vMs["data"].ToString()); 
            //Fill DropDown
            var ListReturndrp = await Http.GetAsync("api/Branch/GetBranchs");
            var vMMs = JObject.Parse(ListReturndrp.Content.ReadAsStringAsync().Result);
            BranchList = JsonConvert.DeserializeObject<List<BranchVM>>(vMMs["data"].ToString());
        }
        protected async Task CheckName()
        {
            string uri = "/api/Store/CheckNameStore?name=" + StoreModel.Name + "&Id=" + StoreModel.ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            string result = vMs["data"].ToString();
            if (result == "True")
            {
                ValidateMessageName = "الاسم موجود من قبل";
            }
            else
            {
                ValidateMessageName = "";
            }
        }
        protected async Task CheckArrangement()
        {
            string uri = "/api/Store/CheckArrangement?arrangement=" + StoreModel.arrangement + "&Id=" + StoreModel.ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            string result = vMs["data"].ToString();
            if (result == "True")
            {
                ValidateMessageArrangement = "الترتيب موجود من قبل";
            }
            else
            {
                ValidateMessageArrangement = "";
            }
        }
        protected async Task AddStore()
        {
            //if (StoreModel.BranchID == 0)
            //{
            //    ValidateMessageBranch = "عليك اختيار الفرع";
            //}
            //else
            //{
            //    ValidateMessageBranch = ""; 
            //}
            //if (ValidateMessageName == "" && ValidateMessageBranch =="" && ValidateMessageArrangement == "")
            //{
                var AddModel = JsonConvert.SerializeObject(StoreModel);
                var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/Store/AddStore", content);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                    ShowConfirm = true;
                    showEditrow = false;
                }
           // }
        }
        protected async Task AddNewStore()
        {
            await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/StoreAdd", "جارى التحميل", 800);
            ValidateMessageName = "";
            ValidateMessageBranch = "";
            showAddrow = true;
            showEditrow = false;
            StoreModel = new StoreVM();
        }
        protected void Cancel()
        {
            showAddrow = false;
            showEditrow = false;
        }
        protected void Confirm()
        {
            ShowConfirm = false;
           
        }
        protected async Task EditOldStore(int ID)
        {
            await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/StoreEdit?ID=" + ID, "جارى التحميل", 800);
        }

        protected async Task EditStore()
        {
            //if (StoreModel.BranchID == 0)
            //{
            //    ValidateMessageBranch = "عليك اختيار الفرع";
            //}
            //else
            //{
            //    ValidateMessageBranch = "";
            //}
            //if (ValidateMessageName == "" && ValidateMessageBranch == "" && ValidateMessageArrangement == "")
            //{
                var EditModel = JsonConvert.SerializeObject(StoreModel);
                var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/Store/EditStore", content);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                    ShowConfirm = true;
                    showEditrow = false;
                }
            //}
        }
        protected void DeleteStore(int Id)
        {
            StoreID = Id;
            ShowDelete = true;
        }
        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            ShowDelete = false;
            if (deleteConfirmed)
            {
                string uri = "/api/Store/DeleteStore?ID=" + StoreID;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
                else
                {
                    StoreUsed = true;
                }
                ShowDelete = false;
            }
        }
    }
}