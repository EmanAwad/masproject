﻿using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Persons;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;

namespace MasProject.Client.Pages.Hierarchy.Treasury
{
    public partial class TreasuryEdit
    {
        private PageTitle PageTitle;
        List<EmployeeVM> UsersList = new List<EmployeeVM>();
        TreasuryVM TreasuryModel = new TreasuryVM();
        List<BranchVM> BranchList = new List<BranchVM>();
        bool ShowConfirm = false;
        string ValidateMessage = "";
        //int TreasuryID;
        string ValidateArrangementMessage = "";
        string ValidateCodeMessage = "";

        protected override async Task OnInitializedAsync()
        {
            await BindList();
            var querystring = ExtensionMethods.QueryString(nav);
            var id = querystring["ID"];
            await EditOldTreasury(int.Parse(id));
        }
        async Task BindList()
        {
            //Fill DropDown
            var ListReturndrp = await Http.GetAsync("api/Branch/GetBranchs");
            var vMMs = JObject.Parse(ListReturndrp.Content.ReadAsStringAsync().Result);
            BranchList = JsonConvert.DeserializeObject<List<BranchVM>>(vMMs["data"].ToString());

            var UsersListReturn = await Http.GetAsync("api/Employee/GetEmployees");
            var vMMMs = JObject.Parse(UsersListReturn.Content.ReadAsStringAsync().Result);
            UsersList = JsonConvert.DeserializeObject<List<EmployeeVM>>(vMMMs["data"].ToString());
        }
        protected async Task CheckName()
        {
            if (TreasuryModel.Name != null)
            {


                string uri = "/api/Treasury/CheckNameTreasury?name=" + TreasuryModel.Name.Trim() + "&Id=" + TreasuryModel.ID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateMessage = "الاسم موجود من قبل";
                }
                else
                {
                    ValidateMessage = "";//vMs["data"].ToString();
                }
            }
        }
        protected async Task CheckArrangement()
        {
            if (TreasuryModel.arrangement != null)
            {
                string uri = "/api/Treasury/CheckArrangement?arrangement=" + TreasuryModel.arrangement + "&Id=" + TreasuryModel.ID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateArrangementMessage = "الترتيب موجود من قبل";
                }
                else
                {
                    ValidateArrangementMessage = "";
                }
            }
        }

        protected async Task CheckTreasuryCode()
        {
            if (TreasuryModel.Code != null)
            {
                string uri = "/api/Treasury/CheckTreasuryCode?Code=" + TreasuryModel.Code.Trim() + "&Id=" + TreasuryModel.ID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateArrangementMessage = "كود الخزينة موجود من قبل";
                }
                else
                {
                    ValidateArrangementMessage = "";
                }
            }
        }


        protected async Task Confirm()
        {
            await nav.NavigateToExitWindowAsync(jsRuntime);
        }
        protected async Task EditOldTreasury(int ID)
        {
            ValidateMessage = "";
            ValidateArrangementMessage = "";
            ValidateCodeMessage = "";
            string uri = "/api/Treasury/GetSpecificTreasury?Id=" + ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            TreasuryModel = JsonConvert.DeserializeObject<TreasuryVM>(vMs["data"].ToString());
        }

        protected async Task EditTreasury()
        {
            if (ValidateMessage == "" & ValidateArrangementMessage == "" & ValidateCodeMessage == "")
            {
                var EditModel = JsonConvert.SerializeObject(TreasuryModel);
                var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/Treasury/EditTreasury", content);
                if (result.IsSuccessStatusCode)
                {
                    ShowConfirm = true;
                }
            }
        }

      
    }
}