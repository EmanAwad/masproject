﻿using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Persons;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;

namespace MasProject.Client.Pages.Hierarchy.Treasury
{
    public partial class Treasury
    {
        private PageTitle PageTitle;

        List<TreasuryVM> TreasuryList;
        TreasuryVM TreasuryModel = new TreasuryVM();
        bool ShowDelete = false;
        bool ShowConfirm = false;
        int TreasuryID;

        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        {
            var ListReturn = await Http.GetAsync("api/Treasury/GetTreasurys");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            TreasuryList = JsonConvert.DeserializeObject<List<TreasuryVM>>(vMs["data"].ToString());
        }

        protected async Task AddNewTreasury()
        {
            await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/TreasuryAdd", "جارى التحميل", 650);
        }
      
        protected void Confirm()
        {
            ShowConfirm = false;
        }
        protected async Task EditOldTreasury(int ID)
        {
            await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/TreasuryEdit?ID=" + ID, "جارى التحميل",650);
        }

        protected void DeleteTreasury(int Id)
        {
            TreasuryID = Id;
            ShowDelete = true;
        }
     
        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            ShowDelete = false;
            if (deleteConfirmed)
            {
                string uri = "/api/Treasury/DeleteTreasury?ID=" + TreasuryID;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
        }
    }
}