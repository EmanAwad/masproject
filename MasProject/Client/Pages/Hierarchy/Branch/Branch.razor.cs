﻿using MasProject.Domain.ViewModel.Hierarchy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;

namespace MasProject.Client.Pages.Hierarchy.Branch
{
    public partial class Branch
    {
        private PageTitle PageTitle;
        List<BranchVM> BranchList;
        BranchVM BranchModel = new BranchVM();
        //List<CompanyVM> CompanyList = new List<CompanyVM>();
        CompanyVM CompanyList= new CompanyVM();
        bool showAddrow = false;
        bool showEditrow = false;
        bool ShowDelete = false;
        bool ShowConfirm = false;
        string ValidateMessageName = "";
        string ValidateMessageKey = "";
        string ValidateMessageCompany = "";
        string ValidateMessageArrangement = "";
        bool BranchUsed = false;

        int BranchID;
        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        {
            //Fill Table
            var ListReturn = await Http.GetAsync("api/Branch/GetBranchs");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            BranchList = JsonConvert.DeserializeObject<List<BranchVM>>(vMs["data"].ToString());
            //Fill DropDown
            var ListReturndrp = await Http.GetAsync("api/Company/GetCompanys");
            var vMMs = JObject.Parse(ListReturndrp.Content.ReadAsStringAsync().Result);
            CompanyList = JsonConvert.DeserializeObject<CompanyVM>(vMMs["data"].ToString());  
        }
        protected async Task CheckName()
        {
            string uri = "/api/Branch/CheckNameBranch?name=" + BranchModel.Name + "&Id=" + BranchModel.ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            string result = vMs["data"].ToString();
            if (result == "True")
            {
                ValidateMessageName = "الاسم موجود من قبل";
            }
            else
            {
                ValidateMessageName = "";
            }
        }
        protected async Task CheckKey()
        {
            string uri = "/api/Branch/CheckKeyBranch?BranchKey=" + BranchModel.BranchKey + "&Id=" + BranchModel.ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            string result = vMs["data"].ToString();
            if (result == "True")
            {
                ValidateMessageKey = "الكود موجود من قبل";
            }
            else
            {
                ValidateMessageKey = "";
            }
        }
        protected async Task CheckArrangement()
        {
            string uri = "/api/Branch/CheckArrangement?arrangement=" + BranchModel.arrangement + "&Id=" + BranchModel.ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            string result = vMs["data"].ToString();
            if (result == "True")
            {
                ValidateMessageArrangement = "الترتيب موجود من قبل";
            }
            else
            {
                ValidateMessageArrangement = "";
            }
        }
        protected async Task AddBranch()
        {
            //if (BranchModel.CompanyId == 0)
            //{
            //    ValidateMessageCompany = "عليك اختيار الشركة";
            //}
            //else
            //{
            //    ValidateMessageCompany = "";
            //}
            if (ValidateMessageName == ""  )
            {
                var AddModel = JsonConvert.SerializeObject(BranchModel);
                var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/Branch/AddBranch", content);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                    ShowConfirm = true;
                    showAddrow = false;
                }
            }
        }
        protected async Task AddNewBranch()
        {
            await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/BranchAdd", "جارى التحميل",800);
            ValidateMessageCompany = "";
            ValidateMessageName = "";
          
            BranchModel = new BranchVM();
        }

        protected void Confirm()
        {
            ShowConfirm = false;
        }
        protected async Task EditOldBranch(int ID)
        {
            await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/BranchEdit?ID=" + ID, "جارى التحميل",800);
     
        }

   
        protected void DeleteBranch(int Id)
        {
            BranchID = Id;
            ShowDelete = true;
        }

        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            ShowDelete = false;
            if (deleteConfirmed)
            {
                string uri = "/api/Branch/DeleteBranch?ID=" + BranchID;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
                else
                {
                    BranchUsed = true;
                }
                ShowDelete = false;
            }
        }
    }
}