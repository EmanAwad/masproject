﻿using MasProject.Domain.ViewModel.Hierarchy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;

namespace MasProject.Client.Pages.Hierarchy.Branch
{
    public partial class BranchEdit
    {
        private PageTitle PageTitle;
        BranchVM BranchModel = new BranchVM();
        //List<CompanyVM> CompanyList = new List<CompanyVM>();
        CompanyVM CompanyList = new CompanyVM();
        bool ShowConfirm = false;
        string ValidateMessageName = "";
        string ValidateMessageKey = "";
        string ValidateMessageCompany = "";
        string ValidateMessageArrangement = "";
        bool IsDisabled = false;

        protected override async Task OnInitializedAsync()
        {
            await BindList();
            var querystring = ExtensionMethods.QueryString(navigationManager);
            var id = querystring["ID"];
            await EditOldBranch(int.Parse(id));
        }
        async Task BindList()
        {

            //Fill DropDown
            var ListReturndrp = await Http.GetAsync("api/Company/GetCompanys");
            var vMMs = JObject.Parse(ListReturndrp.Content.ReadAsStringAsync().Result);
            CompanyList = JsonConvert.DeserializeObject<CompanyVM>(vMMs["data"].ToString());
        }
        protected async Task CheckName()
        {
            if (BranchModel.Name != null)
            {
                string uri = "/api/Branch/CheckNameBranch?name=" + BranchModel.Name.Trim() + "&Id=" + BranchModel.ID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateMessageName = "الاسم موجود من قبل";
                }
                else
                {
                    ValidateMessageName = "";
                }
            }
        }
        protected async Task CheckKey()
        {
            if (BranchModel.BranchKey != null)
            {
                string uri = "/api/Branch/CheckKeyBranch?BranchKey=" + BranchModel.BranchKey.Trim() + "&Id=" + BranchModel.ID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateMessageKey = "الرقم الموحد موجود من قبل";
                }
                else
                {
                    ValidateMessageKey = "";
                }
            }
        }
        protected async Task CheckArrangement()
        {
            if (BranchModel.arrangement != null)
            {
                string uri = "/api/Branch/CheckArrangement?arrangement=" + BranchModel.arrangement + "&Id=" + BranchModel.ID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateMessageArrangement = "الترتيب موجود من قبل";
                }
                else
                {
                    ValidateMessageArrangement = "";
                }
            }
        }
        protected async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);

        }
        protected async Task EditOldBranch(int ID)
        {
          //  ValidateMessageCompany = "";
            ValidateMessageName = "";
            ValidateMessageArrangement = "";
            ValidateMessageKey = "";
            string uri = "/api/Branch/GetSpecificBranch?Id=" + ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            BranchModel = JsonConvert.DeserializeObject<BranchVM>(vMs["data"].ToString());
        }


        protected async Task EditBranch()
        {
            
            //if (BranchModel.CompanyId == 0)
            //{
            //    ValidateMessageCompany = "عليك اختيار الشركة";
            //}
            //else
            //{
            //    ValidateMessageCompany = "";
            //}
            if (ValidateMessageName == "" && ValidateMessageKey == "" && ValidateMessageArrangement == "")
            {
                var EditModel = JsonConvert.SerializeObject(BranchModel);
                var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/Branch/EditBranch", content);
                if (result.IsSuccessStatusCode)
                {

                    ShowConfirm = true;
                }
            }
        }
    }
}
