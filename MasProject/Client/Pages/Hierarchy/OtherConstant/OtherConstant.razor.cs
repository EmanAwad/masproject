﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasProject.Domain.ViewModel.Hierarchy;
using System.Net.Http;
using System.Net.Http.Json;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Components;
using System.Text;
using MatBlazor;
using MasProject.Client.Shared;

namespace MasProject.Client.Pages.Hierarchy.OtherConstant
{
    public partial class OtherConstant
    {
        private PageTitle PageTitle;
        List<OtherConstantVM> OtherConstantList;
        OtherConstantVM otherConstant = new OtherConstantVM();
        bool showAddrow = false;
        bool showEditrow = false;
        bool ShowDelete = false;
        bool  ShowConfirm = false;
        int OtherConstantID;
        string ValidateMessageName = "";
        string ValidateArrangementMessage = "";
        
        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        {
            var ListReturn = await Http.GetAsync("api/OtherConstant/GetOtherConstant");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            OtherConstantList = JsonConvert.DeserializeObject<List<OtherConstantVM>>(vMs["data"].ToString());
        }

        protected async Task CheckName()
        {
            if (otherConstant.Name != null)
            {
                string uri = "/api/OtherConstant/CheckName?name=" + otherConstant.Name.Trim() + "&Id=" + otherConstant.ID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateMessageName = "الاسم موجود من قبل";
                }
                else
                {
                    ValidateMessageName = "";
                }
            }
        }

        protected async Task CheckArrangement()
        {
            if (otherConstant.arrangement != null)
            {
                string uri = "/api/OtherConstant/CheckArrangement?arrangement=" + otherConstant.arrangement + "&Id=" + otherConstant.ID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateArrangementMessage = "الترتيب موجود من قبل";
                }
                else
                {
                    ValidateArrangementMessage = "";
                }
            }
        }
        protected async Task AddOtherConstant()
        {
            if (ValidateMessageName == ""  & ValidateArrangementMessage == "")
            {
                var AddModel = JsonConvert.SerializeObject(otherConstant);
                var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/OtherConstant/AddOtherConstant", content);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                    ShowConfirm = true;
                }
            }
        }
        void AddNewOtherConstant()
        {
             ValidateMessageName = "";
            showAddrow = true;
            showEditrow = false;
            otherConstant = new OtherConstantVM();
        }
        protected async Task EditOldOtherConstant(int ID)
        {
            ValidateMessageName = "";
            ValidateArrangementMessage = "";
            showAddrow = false;
            showEditrow = true;
            string uri = "/api/OtherConstant/GetSpecificOtherConstant?Id=" + ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            otherConstant = JsonConvert.DeserializeObject<OtherConstantVM>(vMs["data"].ToString());
        }

        protected async Task EditOtherConstant()
        {
            if (ValidateMessageName == "" & ValidateArrangementMessage == "" )
            {
                var EditModel = JsonConvert.SerializeObject(otherConstant);
                var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/OtherConstant/EditOtherConstant", content);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                    ShowConfirm = true;
                }
            }
        }

        protected void DeleteOtherConstant(int ID)
        {
            OtherConstantID = ID;
            ShowDelete = true;


        }
        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                string uri = "/api/OtherConstant/DeleteOtherConstant?ID=" + OtherConstantID;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
            ShowDelete = false;
        }
        protected void Cancel()
        {
            showAddrow = false;
            showEditrow = false;
        }
        protected void Confirm()
        {
            ShowConfirm = false;
            showAddrow = false;
            showEditrow = false;
        }

    }
}