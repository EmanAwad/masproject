﻿using MasProject.Domain.ViewModel.Hierarchy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;

namespace MasProject.Client.Pages.Hierarchy.Currency
{
    public partial class CurrencyAdd
    {
        private PageTitle PageTitle;
        CurrencyVM curr = new CurrencyVM();
        bool ShowConfirm = false;
        string ValidateMessageName = "";
        string ValidateArrangementMessage = "";
        bool IsDisabled = false;

        void OnInitializedAsync()
        {
            curr = new CurrencyVM();
        }

        protected async Task CheckName()
        {
            if (curr.Name != null)
            {
                string uri = "/api/Currency/CheckNameCurrency?name=" + curr.Name.Trim() + "&Id=" + curr.ID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateMessageName = "الاسم موجود من قبل";
                }
                else
                {
                    ValidateMessageName = "";
                }
            }
        }
        protected async Task CheckArrangement()
        {
            if (curr.arrangement != null)
            {
                string uri = "/api/Currency/CheckArrangement?arrangement=" + curr.arrangement + "&Id=" + curr.ID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateArrangementMessage = "الترتيب موجود من قبل";
                }
                else
                {
                    ValidateArrangementMessage = "";
                }
            }
        }
        protected async Task AddCurrency()
        {
            IsDisabled = true;
            if (ValidateMessageName == "" & ValidateArrangementMessage == "")
            {
                var AddModel = JsonConvert.SerializeObject(curr);
                var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/Currency/AddCurrency", content);
                if (result.IsSuccessStatusCode)
                {
                    ShowConfirm = true;
                }
            }
        }
        protected async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        }


      

    }
}