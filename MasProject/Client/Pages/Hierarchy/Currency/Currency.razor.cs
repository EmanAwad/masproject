﻿using MasProject.Domain.ViewModel.Hierarchy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using Radzen.Blazor;
using Radzen;

namespace MasProject.Client.Pages.Hierarchy.Currency
{
    public partial class Currency
    {
        

        private PageTitle PageTitle;

        IEnumerable<CurrencyVM> CurrencyList;
        bool ShowDelete = false;
        bool ShowConfirm = false;
        int CurrencyID;

        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        {
            var ListReturn = await Http.GetAsync("api/Currency/GetCurrency");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            CurrencyList = JsonConvert.DeserializeObject<IEnumerable<CurrencyVM>>(vMs["data"].ToString());
        }
        //protected async Task CheckName()
        //{
        //    string uri = "/api/Currency/CheckNameCurrency?name=" + curr.Name + "&Id=" + curr.ID;
        //    HttpResponseMessage response = await Http.GetAsync(uri);
        //    var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
        //    string result = vMs["data"].ToString();
        //    if (result == "True")
        //    {
        //        ValidateMessageName = "الاسم موجود من قبل";
        //    }
        //    else
        //    {
        //        ValidateMessageName = "";
        //    }
        //}
        //protected async Task AddCurrency()
        //{
        //    if (ValidateMessageName == "")
        //    {
        //        var AddModel = JsonConvert.SerializeObject(curr);
        //        var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
        //        var result = await Http.PostAsync("api/Currency/AddCurrency", content);
        //        if (result.IsSuccessStatusCode)
        //        {
        //            await BindList();
        //            ShowConfirm = true;
        //            showAddrow = false;
        //        }
        //    }
        //}
        //protected async Task AddNewCurrency()
        //{
        //    showAddrow = true;
        //    showEditrow = false;
        //    curr = new CurrencyVM();
        //}
        //protected void Cancel()
        //{
        //    showAddrow = false;
        //    showEditrow = false;
        //}

        //protected async Task EditOldCurrency(int ID)
        //{
        //    showEditrow = true;
        //    showAddrow = false;
        //    string uri = "/api/Currency/GetSpecificCurrency?Id=" + ID;
        //    HttpResponseMessage response = await Http.GetAsync(uri);
        //    var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
        //    curr = JsonConvert.DeserializeObject<CurrencyVM>(vMs["data"].ToString());
        //}

        //protected async Task EditCurrency()
        //{
        //    if (ValidateMessageName == "")
        //    {
        //        var EditModel = JsonConvert.SerializeObject(curr);
        //        var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
        //        var result = await Http.PostAsync("api/Currency/EditCurrency", content);
        //        if (result.IsSuccessStatusCode)
        //        {
        //            await BindList();
        //            ShowConfirm = true;
        //            showEditrow = false;
        //        }
        //    }
        //}
        protected void Confirm()
        {
            ShowConfirm = false;
        }
        protected async Task AddNewCurrency()
        {
            await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/CurrencyAdd", "جارى التحميل", 650);
        }
        protected async Task EditOldCurrency(int ID)
        {
            await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/CurrencyEdit?ID=" + ID, "جارى التحميل", 650);
        }
        protected void DeleteCurrency(int Id)
        {
            CurrencyID = Id;
            ShowDelete = true;
        }
        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            ShowDelete = false;
            if (deleteConfirmed)
            {
                string uri = "/api/Currency/DeleteCurrency?ID=" + CurrencyID;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
        }
        //pdf part
//RadzenGrid<CurrencyVM> grid;
//       void GeneratePDF()
//        {
//            var query = new Query() { OrderBy = grid.Query.OrderBy, Filter = grid.Query.Filter };
//            navigationManager.NavigateTo(query != null ? query.ToUrl($"/api/Report/ClientsBalance/exportPdf") : $"/api/Report/ClientsBalance/exportPdf", true); 
//        }
    }
}