﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasProject.Domain.ViewModel.Hierarchy;
using System.Net.Http;
using System.Net.Http.Json;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Components;
using System.Text;
using MatBlazor;
using MasProject.Client.Shared;

namespace MasProject.Client.Pages.Hierarchy.EntriesType
{
    public partial class EntriesType
    {
        private PageTitle PageTitle;
        List<EntriesTypeVM> EntriesTypeList;
        EntriesTypeVM EntryType = new EntriesTypeVM();
        bool showAddrow = false;
        bool showEditrow = false;
        bool ShowDelete = false;
        bool  ShowConfirm = false;
        int EntriesTypeID;
        string ValidateMessageName = "";
        string ValidateArrangementMessage = "";
        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        {
            var ListReturn = await Http.GetAsync("api/EntriesType/GetEntriesType");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            EntriesTypeList = JsonConvert.DeserializeObject<List<EntriesTypeVM>>(vMs["data"].ToString());
        }

        protected async Task CheckName()
        {
            if (EntryType.Name != null)
            {
                string uri = "/api/EntriesType/CheckName?name=" + EntryType.Name.Trim() + "&Id=" + EntryType.ID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateMessageName = "الاسم موجود من قبل";
                }
                else
                {
                    ValidateMessageName = "";
                }
            }
        }

        protected async Task AddEntriesType()

        {
            if (ValidateMessageName == ""  & ValidateArrangementMessage == "")
            {
                var AddModel = JsonConvert.SerializeObject(EntryType);
                var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/EntriesType/AddEntriesType", content);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                    ShowConfirm = true;
                }
            }
        }
        void AddNewEntriesType()
        {
             ValidateMessageName = "";
            showAddrow = true;
            showEditrow = false;
            EntryType = new EntriesTypeVM();
        }
        protected async Task EditOldEntriesType(int ID)
        {
            ValidateMessageName = "";
            ValidateArrangementMessage = "";
            showAddrow = false;
            showEditrow = true;
            string uri = "/api/EntriesType/GetSpecificEntriesType?Id=" + ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            EntryType = JsonConvert.DeserializeObject<EntriesTypeVM>(vMs["data"].ToString());
        }

        protected async Task EditEntriesType()
        {
            if (ValidateMessageName == "" & ValidateArrangementMessage == "" )
            {
                var EditModel = JsonConvert.SerializeObject(EntryType);
                var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/EntriesType/EditEntriesType", content);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                    ShowConfirm = true;
                }
            }
        }

        protected void DeleteEntriesType(int ID)
        {
            EntriesTypeID = ID;
            ShowDelete = true;


        }
        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                string uri = "/api/EntriesType/DeleteEntriesType?ID=" + EntriesTypeID;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
            ShowDelete = false;
        }
        protected void Cancel()
        {
            showAddrow = false;
            showEditrow = false;
        }
        protected void Confirm()
        {
            ShowConfirm = false;
            showAddrow = false;
            showEditrow = false;
        }

    }
}