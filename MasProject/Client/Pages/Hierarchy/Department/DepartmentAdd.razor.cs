﻿using MasProject.Domain.ViewModel.Hierarchy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;

namespace MasProject.Client.Pages.Hierarchy.Department
{
    public partial class DepartmentAdd
    {
        private PageTitle PageTitle;

        List<DepartmentVM> DepartmentList;
        DepartmentVM Department = new DepartmentVM();
        List<BranchVM> BranchList = new List<BranchVM>();

        bool showAddrow = false;
        bool showEditrow = false;
        bool ShowDelete = false; bool ShowConfirm = false;
        string ValidateMessageName = "";
        string ValidateMessageBranch = "";
        string ValidateMessageArrangement = "";
        bool IsDisabled = false;

        int DepartmentID;

        async Task BindList()
        {
            
            //Fill DropDown
            var ListReturndrp = await Http.GetAsync("api/Branch/GetBranchs");
            var vMMs = JObject.Parse(ListReturndrp.Content.ReadAsStringAsync().Result);
            BranchList = JsonConvert.DeserializeObject<List<BranchVM>>(vMMs["data"].ToString());
        }

        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }

        protected async Task CheckName()
        {
            if (Department.Name != null)
            {
                string uri = "/api/Department/CheckNameDepartment?name=" + Department.Name.Trim() + "&Id=" + Department.ID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateMessageName = "الاسم موجود من قبل";
                }
                else
                {
                    ValidateMessageName = "";
                }
            }
        }
        protected async Task CheckArrangement()
        {
            if (Department.arrangement != null)
            {
                string uri = "/api/Department/CheckArrangement?arrangement=" + Department.arrangement + "&Id=" + Department.ID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateMessageArrangement = "الترتيب موجود من قبل";
                }
                else
                {
                    ValidateMessageArrangement = "";
                }
            }
        }
        protected async Task AddDepartment()
        {
            IsDisabled = true;
            //if (Department.BranchID == 0)
            //{
            //    ValidateMessageBranch = "عليك اختيار الفرع";
            //}
            //else
            //{
            //    ValidateMessageBranch = "";
            //}
            if (ValidateMessageName == "" && ValidateMessageArrangement == "")
            {
                var AddModel = JsonConvert.SerializeObject(Department);
                var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/Department/AddDepartment", content);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                    ShowConfirm = true;
                    showAddrow = false;
                }
            }
        }

        protected async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);

        }
    }
}
