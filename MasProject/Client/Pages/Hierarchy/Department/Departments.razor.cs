﻿using MasProject.Domain.ViewModel.Hierarchy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;

namespace MasProject.Client.Pages.Hierarchy.Department
{
    public partial class Departments
    {
        private PageTitle PageTitle;

        List<DepartmentVM> DepartmentList;
        DepartmentVM Department = new DepartmentVM();
        List<BranchVM> BranchList = new List<BranchVM>();

        bool showAddrow = false;
        bool showEditrow = false;
        bool ShowDelete = false; bool ShowConfirm = false;
        string ValidateMessageName = "";
        string ValidateMessageBranch = "";
        string ValidateMessageArrangement = "";

        int DepartmentID;
        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        {
            //Fill Table
            var ListReturn = await Http.GetAsync("api/Department/GetDepartments");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            DepartmentList = JsonConvert.DeserializeObject<List<DepartmentVM>>(vMs["data"].ToString());
            //Fill DropDown
            var ListReturndrp = await Http.GetAsync("api/Branch/GetBranchs");
            var vMMs = JObject.Parse(ListReturndrp.Content.ReadAsStringAsync().Result);
            BranchList = JsonConvert.DeserializeObject<List<BranchVM>>(vMMs["data"].ToString());
        }
        protected async Task CheckName()
        {
            string uri = "/api/Department/CheckNameDepartment?name=" + Department.Name + "&Id=" + Department.ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            string result = vMs["data"].ToString();
            if (result == "True")
            {
                ValidateMessageName = "الاسم موجود من قبل";
            }
            else
            {
                ValidateMessageName = "";
            }
        }
        protected async Task CheckArrangement()
        {
            string uri = "/api/Department/CheckArrangement?arrangement=" + Department.arrangement + "&Id=" + Department.ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            string result = vMs["data"].ToString();
            if (result == "True")
            {
                ValidateMessageArrangement = "الترتيب موجود من قبل";
            }
            else
            {
                ValidateMessageArrangement = "";
            }
        }
        protected async Task AddDepartment()
        {
            //if (Department.BranchID == 0)
            //{
            //    ValidateMessageBranch = "عليك اختيار الفرع";
            //}
            //else
            //{
            //    ValidateMessageBranch = "";
            //}
            if (ValidateMessageName == "" && ValidateMessageArrangement == "")
            {
                var AddModel = JsonConvert.SerializeObject(Department);
                var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/Department/AddDepartment", content);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                    ShowConfirm = true;
                    showAddrow = false;
                }
            }
        }
        protected async Task AddNewDepartment()
        {
            await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/DepartmentAdd", "جارى التحميل", 620);
            ValidateMessageName = "";
            ValidateMessageBranch = "";
            showAddrow = true;
            showEditrow = false;
            Department = new DepartmentVM { };
        }
        protected void Cancel()
        {
            showAddrow = false;
            showEditrow = false;
        }
        protected void Confirm()
        {
            ShowConfirm = false;
           
        }
        protected async Task EditOldDepartment(int ID)
        {
            await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/DepartmentEdit?ID=" + ID, "جارى التحميل", 620);
        }

        protected async Task EditDepartment()
        {
            //if (Department.BranchID == 0)
            //{
            //    ValidateMessageBranch = "عليك اختيار الفرع";
            //}
            //else
            //{
            //    ValidateMessageBranch = "";
            //}
            if (ValidateMessageName == ""  && ValidateMessageArrangement == "")
            {
                var EditModel = JsonConvert.SerializeObject(Department);
                var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/Department/EditDepartment", content);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                    ShowConfirm = true;
                    showEditrow = false;
                }
            }
        }
        protected void DeleteDepartment(int Id)
        {
            DepartmentID = Id;
            ShowDelete = true;
        }
        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            ShowDelete = false;
            if (deleteConfirmed)
            {
                string uri = "/api/Department/DeleteDepartment?ID=" + DepartmentID;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
        }
    }
}