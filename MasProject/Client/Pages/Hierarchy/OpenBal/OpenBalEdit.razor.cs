﻿using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Persons;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;

namespace MasProject.Client.Pages.Hierarchy.OpenBal
{
    public partial class OpenBalEdit
    {
        private PageTitle PageTitle;
        TreasuryOpenBalanceVM OpenBalTreasury = new TreasuryOpenBalanceVM();
        List<TreasuryVM> TreasuryList = new List<TreasuryVM>();
        List<CurrencyVM> CurrencyList = new List<CurrencyVM>();
        List<EmployeeVM> UsersList = new List<EmployeeVM>();
        bool ShowConfirm = false;
        //string ValidateMessageTreasury = "";
        //string ValidateMessageCurrency = "";
        //string ValidateMessageEntryD = "";
        //int OpenBalID;
        decimal CurrRatio = 0;
        bool IsDisabled = false;
        protected override async Task OnInitializedAsync()
        {
            await BindList();
            var querystring = ExtensionMethods.QueryString(nav);
            var id = querystring["ID"];
            await EditOldOpenBal(int.Parse(id));
            var Type = querystring["Type"];
            if (Type == "View")
            {
                //disable controls
                IsDisabled = true;
                PageTitle.Title = "    عرض الأرصدة الافتتاحية للخزائن";
            }
            else
            {
                IsDisabled = false;
                PageTitle.Title = "تعديل  الأرصدة الافتتاحية للخزائن";
            }
        }
        async Task BindList()
        {
            //Fill DropDown
            var ListReturndrp = await Http.GetAsync("api/Currency/GetCurrency");
            var vMMs = JObject.Parse(ListReturndrp.Content.ReadAsStringAsync().Result);
            CurrencyList = JsonConvert.DeserializeObject<List<CurrencyVM>>(vMMs["data"].ToString());
            //Fill DropDown
            var ListReturnddrp = await Http.GetAsync("api/Treasury/GetTreasurys");
            var vMMMs = JObject.Parse(ListReturnddrp.Content.ReadAsStringAsync().Result);
            TreasuryList = JsonConvert.DeserializeObject<List<TreasuryVM>>(vMMMs["data"].ToString());

            //Fill Esm el mas2ol Drop down from users 
            var UsersListReturn = await Http.GetAsync("api/Employee/GetUsers");
            var vMMMMs = JObject.Parse(UsersListReturn.Content.ReadAsStringAsync().Result);
            UsersList = JsonConvert.DeserializeObject<List<EmployeeVM>>(vMMMMs["data"].ToString());
        }
        protected async Task Confirm()
        {
            await nav.NavigateToExitWindowAsync(jsRuntime);
            ShowConfirm = false;
            IsDisabled = false;
        }

        async Task EditOldOpenBal(int ID)
        {
            //ValidateMessageTreasury = "";
            //ValidateMessageCurrency = "";
            string uri = "/api/TreasuryOpenBal/GetOneTreasuryOpenBal?Id=" + ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            OpenBalTreasury = JsonConvert.DeserializeObject<TreasuryOpenBalanceVM>(vMs["data"].ToString());
        }

        protected async Task EditOpenBal()
        {
            
            //if (OpenBalTreasury.TreasuryID == 0)
            //{
            //    ValidateMessageTreasury = "عليك اختيار الخزنة";
            //}
            //else
            //{
            //    ValidateMessageTreasury = "";
            //}
            //if (OpenBalTreasury.CurrencyID == 0)
            //{
            //    ValidateMessageCurrency = "عليك اختيار العملة";
            //}
            //else
            //{
            //    ValidateMessageCurrency = "";
            //}
            //if (ValidateMessageTreasury == "" && ValidateMessageCurrency == "")
            //{
            var EditModel = JsonConvert.SerializeObject(OpenBalTreasury);
            var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
            var result = await Http.PostAsync("api/TreasuryOpenBal/EditTreasuryOpenBal", content);
            if (result.IsSuccessStatusCode)
            {
                ShowConfirm = true;
            }
            //}
        }
        void CalculateCurrRatio()
        {
            OpenBalTreasury.CurrRatio = CurrRatio * OpenBalTreasury.Amount;
        }
        protected async Task SetCurrRatio()
        {
            string uri = "/api/Currency/GetCurrencyRatio?Id=" + OpenBalTreasury.CurrencyID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            CurrRatio = JsonConvert.DeserializeObject<decimal>(vMs["data"].ToString());
            OpenBalTreasury.CurrRatio = CurrRatio * OpenBalTreasury.Amount;
        }
    }
}