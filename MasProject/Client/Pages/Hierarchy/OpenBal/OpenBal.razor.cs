﻿using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Persons;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;

namespace MasProject.Client.Pages.Hierarchy.OpenBal
{
    public partial class OpenBal
    {
        private PageTitle PageTitle;

        List<TreasuryOpenBalanceVM> OpenBalList;
        bool ShowDelete = false;
        bool ShowConfirm = false;
        int OpenBalID;
        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        {
            //Fill Table
            var ListReturn = await Http.GetAsync("api/TreasuryOpenBal/GetTreasuryOpenBal");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            OpenBalList = JsonConvert.DeserializeObject<List<TreasuryOpenBalanceVM>>(vMs["data"].ToString());
        }

        protected async Task AddNewOpenBal()
        {
            await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/OpenBalAddTreasury", "جارى التحميل", 650);
        }
              protected void Confirm()
        {
            ShowConfirm = false;
        }
        protected async Task EditOldOpenBal(int ID,string Type)
        {
            await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/OpenBalEditTreasury?ID=" + ID + "&Type=" + Type, "جارى التحميل", 650);
          
        }

              protected void DeleteOpenBal(int Id)
        {
            OpenBalID = Id;
            ShowDelete = true;
        }
        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            ShowDelete = false;
            if (deleteConfirmed)
            {
                string uri = "/api/TreasuryOpenBal/DeleteTreasuryOpenBal?ID=" + OpenBalID;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
        }
    }
}