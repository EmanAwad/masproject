﻿using BlazorInputFile;
using MasProject.Domain.ViewModel.Hierarchy;
using MatBlazor;
using Microsoft.AspNetCore.Components;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System;
using MasProject.Client.Shared;

namespace MasProject.Client.Pages.Hierarchy.Company 
{
    public partial class Company : ComponentBase
    {
        private PageTitle PageTitle;

       List<CompanyVM> CompanyList=new List<CompanyVM>();
        CompanyVM CompanyModel = new CompanyVM();
        bool showAddrow = false;
        bool showEditrow = false;
        bool ShowDelete = false;
        bool ShowConfirm = false;
        bool ShowImg = false;
        string ValidateMessageName = "";
        string ValidateMessageWebsite = "";
        string ValidateMessageArrangement = "";

        int CompanyID;
        IFileListEntry file;

       void HandleSelection(IFileListEntry[] files)
        {
            file = files.FirstOrDefault();
        }
        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        {
            var ListReturn = await Http.GetAsync("/api/Company/GetCompanys");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            var temp = JsonConvert.DeserializeObject<CompanyVM>(vMs["data"].ToString());
            CompanyList.Add(temp);
        }
        protected async Task CheckName()
        {
            string uri = "/api/Company/CheckNameCompany?name=" + CompanyModel.Name + "&Id=" + CompanyModel.ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            string result = vMs["data"].ToString();
            if (result == "True")
            {
                ValidateMessageName = "الاسم موجود من قبل";
            }
            else
            {
                ValidateMessageName = "";
            }
        }
        //void CheckWebsite()
        //{
        //    //may want more here for https, etc
        //    Regex regex = new Regex(@"(http://)?(www\.)?\w+\.(com|net|edu|org)");
        //    bool returned = false;
        //    if (CompanyModel.Website == null) returned = false;

        //    if (!regex.IsMatch(CompanyModel.Website.ToString())) returned = true;
        //    if (returned)
        //    {
        //        ValidateMessageWebsite = "الموقع غير صحيح";
        //    }
        //    else
        //    {
        //        ValidateMessageWebsite = "";
        //    }
        //}
        protected async Task CheckArrangement()
        {
            string uri = "/api/Company/CheckArrangement?arrangement=" + CompanyModel.arrangement + "&Id=" + CompanyModel.ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            string result = vMs["data"].ToString();
            if (result == "True")
            {
                ValidateMessageArrangement = "الترتيب موجود من قبل";
            }
            else
            {
                ValidateMessageArrangement = "";
            }
        }
        protected async Task AddCompany()
        {
            if (ValidateMessageName == "" && ValidateMessageWebsite == "" && ValidateMessageArrangement == "")
            {
                string pathlogo = $"/Company/";
                if (!Directory.Exists(pathlogo))
                    Directory.CreateDirectory(pathlogo);
                if (file != null && file.Size > 0)
                {
                    var namelogo = Guid.NewGuid().ToString() + Path.GetExtension(file.Name);
                    var ms = new MemoryStream();
                    await file.Data.CopyToAsync(ms);
                    var contentFile = new MultipartFormDataContent {
                {
                            new ByteArrayContent(ms.GetBuffer()), "\"Company\"", namelogo}
                    };
                    await Http.PostAsync("upload?FolderName="+ "Company", contentFile);

                    CompanyModel.Logo = "/Images/Company/" + namelogo;
                }
                var AddModel = JsonConvert.SerializeObject(CompanyModel);
                var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("/api/Company/AddCompany", content);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                    ShowConfirm = true;
                    showAddrow = false;
                }
            }
        }
        void AddNewCompany()
        {
            file = null;
            ValidateMessageName = "";
            showAddrow = true;
            showEditrow = false;
            CompanyModel = new CompanyVM();
        }
        protected void Cancel()
        {
            showAddrow = false;
            showEditrow = false;
            ShowConfirm = false;
        }
        protected void Confirm()
        {
            ShowConfirm = false;
            showAddrow = false;
            showEditrow = false;
        }
        protected async Task EditOldCompany(int ID)
        {
            file = null;
            ValidateMessageName = "";
            showEditrow = true;
            showAddrow = false;
            string uri = "/api/Company/GetSpecificCompany?Id=" + ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            CompanyModel = JsonConvert.DeserializeObject<CompanyVM>(vMs["data"].ToString());
        }

        protected async Task EditCompany()
        {
            if (ValidateMessageName == "" && ValidateMessageWebsite == "" && ValidateMessageArrangement == "")
            {
                string pathlogo = $"/Company/";
                if (!Directory.Exists(pathlogo))
                    Directory.CreateDirectory(pathlogo);
                if (file != null && file.Size > 0)
                {
                    var namelogo = Guid.NewGuid().ToString() + Path.GetExtension(file.Name);
                    var ms = new MemoryStream();
                    await file.Data.CopyToAsync(ms);
                    var contentFile = new MultipartFormDataContent {
                {
                            new ByteArrayContent(ms.GetBuffer()), "\"Company\"", namelogo}
                    };
                    await Http.PostAsync("upload?FolderName=" + "Company", contentFile);

                    CompanyModel.Logo = "/Images/Company/" + namelogo;
                }
                var EditModel = JsonConvert.SerializeObject(CompanyModel);
                var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("/api/Company/EditCompany", content);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                    ShowConfirm = true;
                    showEditrow = false;
                }
            }
        }

        protected void DeleteCompany(int Id)
        {
            CompanyID = Id;
            ShowDelete = true;
        }

        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            ShowDelete = false;
            if (deleteConfirmed)
            {
                string uri = "/api/Company/DeleteCompany?ID=" + CompanyID;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
        }
        //string LogoString;
        //string path = $"{Directory.GetCurrentDirectory()}{@"\wwwroot\images"}";
        //void FilesReady(IMatFileUploadEntry file)
        //{
        //    LogoString = $"Name: {file.Name} - Size: {file.Size}";
        //}
    }
}