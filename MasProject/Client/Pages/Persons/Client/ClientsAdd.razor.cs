﻿using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Persons;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;

namespace MasProject.Client.Pages.Persons.Client
{
    public partial class ClientsAdd
    {
        private PageTitle PageTitle;
        List<BranchVM> BranchList = new List<BranchVM>();
        List<SocialMediaVM> SocialMediaList = new List<SocialMediaVM>();
        List<LookupKeyValueVM> CityList = new List<LookupKeyValueVM>();
        List<DealTypeVM> DealTypeList = new List<DealTypeVM>();
        SalesArchivesVM SalesModel = new SalesArchivesVM();
        List<SalesArchivesVM> SalesList = new List<SalesArchivesVM>();
        ClientsVM client = new ClientsVM();
        bool ShowConfirm = false;
        //string ValidateMessageCity = "";
        //string ValidateMessageBranch = "";
        //string ValidateMessageFirstDealingD = "";
        //string ValidateMessageLastDealingD = ""; 
        //string ValidateMessagMail = "";
        //string ValidateMessagEmployee = "";
        string ValidateMessagCode = "";
        string ValidateMessagMobile2 = "";
        string ValidateMessagName = "";
        string ValidateMessagArrangment = "";
        string ValidateMessagPhone = "";
        string ValidateMessagMobile = "";
        bool FirstAdd = false;
        bool IsDisabled = false;
        bool CheckValidate = false;

        protected override async Task OnInitializedAsync()
        { 
            //Fill DropDown
            client = new ClientsVM { };
            var TypeListReturn = await Http.GetAsync("api/Client/GetListsOfDDl");
            var TypevMs = JObject.Parse(TypeListReturn.Content.ReadAsStringAsync().Result);
            client = JsonConvert.DeserializeObject<ClientsVM>(TypevMs["data"].ToString());
            BranchList = client.BranchList;
            SocialMediaList = client.SocialMediaList;
            CityList = client.CityList;
            DealTypeList = client.DealTypeList;
            client.FirstDealingDate = DateTime.Now;
            client.LastDealingDate = DateTime.Now;
            SalesModel = new SalesArchivesVM { Note = "" };
            SalesList.Add(SalesModel);
            FirstAdd = false;
        }
      
        //bool ValidateClient()
        //{
        //    if (client.CityID == 0)
        //    {
        //        ValidateMessageCity = "عليك اختيار المدينة";
        //    }
        //    else
        //    {
        //        ValidateMessageCity = "";
        //    }
        //    if (client.BranchID == 0)
        //    {
        //        ValidateMessageBranch = "عليك اختيار الفرع ";
        //    }
        //    else
        //    {
        //        ValidateMessageBranch = "";
        //    }
        //    DateTime CheckDate = new DateTime();
        //    if (client.LastDealingDate == CheckDate)
        //    {
        //        ValidateMessageLastDealingD = "عليك ادخال تاريخ ";
        //    }
        //    else
        //    {
        //        ValidateMessageLastDealingD = "";
        //    }
        //    if (client.FirstDealingDate == CheckDate)
        //    {
        //        ValidateMessageFirstDealingD = "عليك ادخال تاريخ ";
        //    }
        //    else
        //    {
        //        ValidateMessageFirstDealingD = "";
        //    }
           
        //    if (ValidateMessagCode == ""&& ValidateMessageFirstDealingD == "" && ValidateMessageLastDealingD == "" && ValidateMessageBranch == "" && ValidateMessageCity == "" && ValidateMessageEmployee== "")
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}
      
        //protected async Task CheckMail()
        //{
        //    string uri = "/api/Client/CheckClientMail?Mail=" + client.Mail + "&Id=" + client.ID;
        //    HttpResponseMessage response = await Http.GetAsync(uri);
        //    var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
        //    string result = vMs["data"].ToString();
        //    if (result == "True")
        //    {
        //        ValidateMessagMail = "البريد الالكترونى موجود من قبل";
        //    }
        //    else
        //    {
        //        ValidateMessagMail = "";
        //    }
        //}
        protected async Task CheckArrangment()
        {
            if (client.arrangement != null)
            {
                string uri = "/api/Client/CheckArrangment?arrangment=" + client.arrangement + "&Id=" + client.ID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateMessagArrangment = "رقم الترتيب موجود من قبل";
                }
                else
                {
                    ValidateMessagArrangment = "";
                }
            }
        }
        protected async Task CheckPhone()
        {
            if (client.Phone != "" && client.Phone != null)
            {
                string uri = "/api/Client/CheckClientPhone?Phone=" + client.Phone + "&Id=" + client.ID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateMessagPhone = "رقم التليفون موجود من قبل";
                }
                else
                {
                    ValidateMessagPhone = "";
                }
            }
        }
        protected async Task CheckMobile()
        {
            if (client.Mobile != "" && client.Mobile != null)
            {
                string uri = "/api/Client/CheckClientMobile?Mobile=" + client.Mobile + "&Id=" + client.ID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateMessagMobile = "رقم الموبايل موجود من قبل";
                }
                else
                {
                    ValidateMessagMobile = "";
                }
            }
        }
        protected async Task CheckMobile2()
        {
            if (client.Mobile2 != "" && client.Mobile2 != null)
            {
                string uri = "/api/Client/CheckClientMobile2?Mobile=" + client.Mobile2 + "&Id=" + client.ID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateMessagMobile2 = "رقم الموبايل موجود من قبل";
                }
                else
                {
                    ValidateMessagMobile2 = "";
                }
            }
        }
        protected async Task CheckCode()
        {
            if (client.Code != "" && client.Code != null)
            {
                string uri = "/api/Client/CheckClientCode?Code=" + client.Code + "&Id=" + client.ID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateMessagCode = "الكود موجود من قبل";
                }
                else
                {
                    ValidateMessagCode = "";
                }
            }
        }
        protected async Task CheckName()
        {
            if (client.Name != "" && client.Name != null)
            {
                string uri = "/api/Client/CheckClientName?Name=" + client.Name.Trim() + "&Id=" + client.ID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateMessagName = "الاسم موجود من قبل";
                }
                else
                {
                    ValidateMessagName = "";
                }
            }
        }
        protected bool Validate()
        {
            if (client.Code == "")
            {
                ValidateMessagCode = "الكود مطلوب ";
                return false;
            }
            else if (client.Name == "")
            {
                ValidateMessagName = "الاسم مطلوب ";
                return false;
            }
            //else if (client.arrangement == null || client.arrangement==0)
            //{
            //    ValidateMessagName = "الترتيب مطلوب ";
            //    return false;
            //}
            else
            {
                return true;
            }

        }

        protected async Task AddClient()
        {

            if (Validate() & ValidateMessagCode == "" & ValidateMessagName == "" & ValidateMessagArrangment == ""
                & ValidateMessagPhone == "" & ValidateMessagMobile == "" & ValidateMessagMobile2 == "")
            {
                HttpResponseMessage result = new HttpResponseMessage();
                if (FirstAdd == false)
                {
                    var AddModel = JsonConvert.SerializeObject(client);
                    var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/Client/AddClient", content);
                    var vMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    client.ID = JsonConvert.DeserializeObject<int>(vMs["data"].ToString());
                    FirstAdd = true;

                }
                else
                {
                    client.SalesList = SalesList;
                    client.SocialMediaList = SocialMediaList;
                    var EditModel = JsonConvert.SerializeObject(client);
                    var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/Client/EditClient", content);
                }
                if (result.IsSuccessStatusCode)
                {
                    StateHasChanged();
                }
            }
            else
            {
                CheckValidate = true;
            }
        }
        protected async Task EditClient()
        {
            IsDisabled = true;
            if (Validate() & ValidateMessagCode == "" & ValidateMessagName == "")
            {
                //client.SocialMediaList = SocialMediaList;
                //client.SalesList = SalesList;
                //var EditModel = JsonConvert.SerializeObject(client);
                //var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                //var result = await Http.PostAsync("api/Client/EditClient", content);
                HttpResponseMessage result = new HttpResponseMessage();
                if (FirstAdd == false)
                {
                    var AddModel = JsonConvert.SerializeObject(client);
                    var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/Client/AddClient", content);
                    var vMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    client.ID = JsonConvert.DeserializeObject<int>(vMs["data"].ToString());
                    FirstAdd = true;

                }
                else
                {
                    client.SalesList = SalesList;
                    client.SocialMediaList = SocialMediaList;
                    var EditModel = JsonConvert.SerializeObject(client);
                    var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/Client/EditClient", content);
                }
                if (result.IsSuccessStatusCode)
                {
                    ShowConfirm = true;
                }

            }
        }
        protected async Task Confirm()
        {
             await navigationManager.NavigateToExitWindowAsync(jsRuntime);
            //ShowConfirm = false;
        }

        void  AddNewRow()
        {
            SalesModel = new SalesArchivesVM { Note = "" };
            SalesList.Add(SalesModel);
            StateHasChanged();
        }

        void RemoveRow(SalesArchivesVM row)
        {
            SalesList.Remove(row);
            StateHasChanged();
        }
    }
}