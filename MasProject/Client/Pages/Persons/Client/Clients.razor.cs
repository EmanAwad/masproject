﻿using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Persons;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using Microsoft.JSInterop;
namespace MasProject.Client.Pages.Persons.Client
{
    public partial class Clients
    {
        private PageTitle PageTitle;
        List<ClientsVM> ClientList = new List<ClientsVM>();
        bool ShowDelete = false;
        bool ShowConfirm = false;
        int ClientID;

        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        {
            //Fill Table
            var ListReturn = await Http.GetAsync("api/Client/GetClient");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ClientList = JsonConvert.DeserializeObject<List<ClientsVM>>(vMs["data"].ToString());
        }

        protected async Task AddNewClient()
        {
            await jsRuntime.InvokeAsync<object>("open", "/ClientAdd", "_blank");

        }

        protected void Confirm()
        {
            ShowConfirm = false;
        }
        protected async Task EditOldClient(int ID)
        {
            await jsRuntime.InvokeAsync<object>("open", "/ClientEdit?ID=" + ID, "_blank");

        }

        protected void DeleteClient(int Id)
        {
            ClientID = Id;
            ShowDelete = true;
        }
        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                string uri = "/api/Client/DeleteClient?ID=" + ClientID;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
            ShowDelete = false;
        }
    }
}