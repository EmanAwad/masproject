﻿using MasProject.Domain.ViewModel.Persons;
using MatBlazor;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using MasProject.Domain.ViewModel.Hierarchy;

namespace MasProject.Client.Pages.Persons.Suppliers
{
    public partial class SupplierEdit
    {
        private PageTitle PageTitle;
        SupplierVM supplier = new SupplierVM();
        PurchasesArchivesVM PurchaseModel = new PurchasesArchivesVM();
        List<PurchasesArchivesVM> PurchaseList = new List<PurchasesArchivesVM>();
        List<CurrencyVM> CurrencyList = new List<CurrencyVM>();
        bool ShowConfirm = false;
        string ValidateMessagCode = "";
        string ValidateMessagName = "";
        //string ValidateMessagMail = "";
        //string ValidateMessagPhone = "";
        //string ValidateMessagMobile = "";
        string ValidateMessageArrangement = "";
        int SupplierID;
        protected override async Task OnInitializedAsync()
        {
            var querystring = ExtensionMethods.QueryString(nav);
            var id = querystring["ID"];
            await EditOldSupplier(int.Parse(id));
        }

        protected async Task CheckCode()
        {
            if (supplier.Code.Trim() != null)
            {
                string uri = "/api/Supplier/CheckSupplierCode?Code=" + supplier.Code.Trim() + "&Id=" + supplier.ID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateMessagCode = "الكود موجود من قبل";
                }
                else
                {
                    ValidateMessagCode = "";
                }
            }
        }
        protected async Task CheckName()
        {
            if (supplier.Name.Trim() != null)
            {


                string uri = "/api/Supplier/CheckSupplierName?Name=" + supplier.Name.Trim() + "&Id=" + supplier.ID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateMessagName = "الاسم موجود من قبل";
                }
                else
                {
                    ValidateMessagName = "";
                }
            }
        }
        protected async Task CheckArrangement()
        {
            if (supplier.arrangement != null)
            {
                string uri = "/api/Supplier/CheckArrangement?arrangement=" + supplier.arrangement + "&Id=" + supplier.ID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateMessageArrangement = "الترتيب موجود من قبل";
                }
                else
                {
                    ValidateMessageArrangement = "";
                }
            }
        }
        //protected async Task CheckMail()
        //{
        //    string uri = "/api/Supplier/CheckSupplierMail?Mail=" + supplier.Mail + "&Id=" + supplier.ID;
        //    HttpResponseMessage response = await Http.GetAsync(uri);
        //    var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
        //    string result = vMs["data"].ToString();
        //    if (result == "True")
        //    {
        //        ValidateMessagMail = "البريد الالكترونى موجود من قبل";
        //    }
        //    else
        //    {
        //        ValidateMessagMail = "";
        //    }
        //}
        //protected async Task CheckPhone()
        //{
        //    string uri = "/api/Supplier/CheckSupplierPhone?Phone=" + supplier.Phone + "&Id=" + supplier.ID;
        //    HttpResponseMessage response = await Http.GetAsync(uri);
        //    var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
        //    string result = vMs["data"].ToString();
        //    if (result == "True")
        //    {
        //        ValidateMessagPhone = "رقم التليفون موجود من قبل";
        //    }
        //    else
        //    {
        //        ValidateMessagPhone = "";
        //    }
        //}
        //protected async Task CheckMobile()
        //{
        //    string uri = "/api/Supplier/CheckSupplierMobile?Mobile=" + supplier.Mobile + "&Id=" + supplier.ID;
        //    HttpResponseMessage response = await Http.GetAsync(uri);
        //    var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
        //    string result = vMs["data"].ToString();
        //    if (result == "True")
        //    {
        //        ValidateMessagMobile = "رقم الموبايل موجود من قبل";
        //    }
        //    else
        //    {
        //        ValidateMessagMobile = "";
        //    }

        //}
        protected bool Validate()
        {
            if (supplier.Code == "")
            {
                ValidateMessagCode = "الكود مطلوب ";
                return false;
            }
            else if (supplier.Name == "")
            {
                ValidateMessagName = "الكود مطلوب ";
                return false;
            }
            else
            {
                return true;
            }

        }

        void Confirm()
        {
            // await nav.NavigateToExitWindowAsync(jsRuntime);
            ShowConfirm = false;
        }
        protected async Task EditOldSupplier(int ID)
        {

            ValidateMessagName = "";
            ValidateMessagCode = "";
            ValidateMessageArrangement = "";
            //ValidateMessagMail = "";
            //ValidateMessagPhone = "";
            //ValidateMessagMobile = "";

            var ListReturndrp = await Http.GetAsync("api/Currency/GetCurrency");
            var vMMs = JObject.Parse(ListReturndrp.Content.ReadAsStringAsync().Result);
            CurrencyList = JsonConvert.DeserializeObject<List<CurrencyVM>>(vMMs["data"].ToString());

            string uri = "/api/Supplier/GetSpecificSupplier?Id=" + ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            supplier = JsonConvert.DeserializeObject<SupplierVM>(vMs["data"].ToString());
            PurchaseList = supplier.PurchaseList;
        }
        protected async Task TempEditSupplier()
        {
            if (Validate() & ValidateMessagName == "" & ValidateMessagCode == "" & ValidateMessageArrangement == "")
            {
                HttpResponseMessage result = new HttpResponseMessage();

                supplier.PurchaseList = PurchaseList;
                var EditModel = JsonConvert.SerializeObject(supplier);
                var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                result = await Http.PostAsync("api/Supplier/EditSupplier", content);
                if (result.IsSuccessStatusCode)
                {
                    StateHasChanged();
                }
            }
        }
        protected async Task EditSupplier()
        {
            if (Validate() & ValidateMessagName == "" & ValidateMessagCode == "" & ValidateMessageArrangement == "")
            {
                supplier.PurchaseList = PurchaseList;
                var EditModel = JsonConvert.SerializeObject(supplier);
                var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/Supplier/EditSupplier", content);
                if (result.IsSuccessStatusCode)
                {
                    ShowConfirm = true;
                }
            }
        }

        void AddNewRow()
        {
            PurchaseModel = new PurchasesArchivesVM { Note = "" };
            PurchaseList.Add(PurchaseModel);
            StateHasChanged();
        }

        void RemoveRow(PurchasesArchivesVM row)
        {
            PurchaseList.Remove(row);
            StateHasChanged();
        }
    }
}