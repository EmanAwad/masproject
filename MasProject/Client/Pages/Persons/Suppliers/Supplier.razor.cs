﻿using MasProject.Domain.ViewModel.Persons;
using MatBlazor;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using Microsoft.JSInterop;
namespace MasProject.Client.Pages.Persons.Suppliers
{
    public partial class Supplier
    {
        private PageTitle PageTitle;
        List<SupplierVM> SupplierList;
        bool ShowConfirm = false;
        bool ShowDelete = false;
        int SupplierID;
        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        {
            var ListReturn = await Http.GetAsync("api/Supplier/GetSuppliers");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            SupplierList = JsonConvert.DeserializeObject<List<SupplierVM>>(vMs["data"].ToString());
        }

        protected async Task AddNewSupplier()
        {
            await jsRuntime.InvokeAsync<object>("open", "/SupplierAdd", "_blank");

        }

        protected void Confirm()
        {
            ShowConfirm = false;
        }
        protected async Task EditOldSupplier(int ID)
        {
            await jsRuntime.InvokeAsync<object>("open", "/SupplierEdit?ID=" + ID, "_blank");

        }


        protected void DeleteSupplier(int Id)
        {
            SupplierID = Id;
            ShowDelete = true;
        }
        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                string uri = "/api/Supplier/DeleteSupplier?ID=" + SupplierID;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
            ShowDelete = false;
        }
    }
}