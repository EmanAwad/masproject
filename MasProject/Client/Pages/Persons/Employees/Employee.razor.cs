﻿using MasProject.Domain.ViewModel.Persons;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using Microsoft.JSInterop;
using System.Net.Http;
using Microsoft.AspNetCore.Components;

namespace MasProject.Client.Pages.Persons.Employees
{
    public partial class Employee
    {
        private PageTitle PageTitle;
        List<EmployeeVM> EmployeeList;
        bool ShowHide = false;
        bool ShowDelete = false;
        int EmployeeID;
        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        {
            //Fill Table
            var ListReturn = await Http.GetAsync("api/Employee/GetEmployees");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            EmployeeList = JsonConvert.DeserializeObject<List<EmployeeVM>>(vMs["data"].ToString());
        }
        protected async Task AddNewEmployee()
        {
            await jsRuntime.InvokeAsync<object>("open", "/EmployeeAdd", "_blank");
        }
        protected async Task EditOldEmployee(int ID)
        {
            await jsRuntime.InvokeAsync<object>("open", "/EmployeeEdit?ID=" + ID, "_blank");

        }
        protected void DeleteEmployee(int Id)
        {
            EmployeeID = Id;
            ShowDelete = true;
        }
        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                string uri = "/api/Employee/DeleteEmployee?ID=" + EmployeeID;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
            ShowDelete = false;
        }

        protected async Task GetAllByEmployees()
        {
            ShowHide = !ShowHide;
            if (ShowHide)
            {
                string uri = "/api/Employee/GetAllEmployee";
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                EmployeeList = JsonConvert.DeserializeObject<List<EmployeeVM>>(vMs["data"].ToString());
            }
            else
            {
                var ListReturn = await Http.GetAsync("api/Employee/GetEmployees");
                var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
                EmployeeList = JsonConvert.DeserializeObject<List<EmployeeVM>>(vMs["data"].ToString());
            }

            StateHasChanged();
        }
    }
}