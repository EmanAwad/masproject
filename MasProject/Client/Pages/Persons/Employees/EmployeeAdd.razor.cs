﻿using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel.Hierarchy;
using MatBlazor;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System;
using System.IO;
using BlazorInputFile;
using System.Linq;
using MasProject.Client.Shared;
using MasProject.Domain.ViewModel;

namespace MasProject.Client.Pages.Persons.Employees
{
    public partial class EmployeeAdd
    {
        private PageTitle PageTitle;
        EmployeeVM employee = new EmployeeVM();
       // RegisterVM userModel = new RegisterVM();
        List<DepartmentVM> DepartmentList = new List<DepartmentVM>();
        List<BranchVM> BranchList = new List<BranchVM>();
        bool showAddrow = false;
        bool showEditrow = false;
        bool ShowDelete = false;
        bool ShowConfirm = false;
        string ValidateMessageBranch = "";
        string ValidateMessageDepartment = "";
        string ValidateMessageBirthD = "";
        string ValidateMessageJoinD = "";
        string ValidateMessageUsername = "";
        string ValidateMessagePass = "";
        string ValidateMessagCode = "";
        string ValidateMessagName = "";
        string ValidateMessagMail = "";
        string ValidateMessagPhone = "";
        string ValidateMessagMobile = "";
        string ValidateMessageArrangement = "";
        bool IsDisabled = false;
        IFileListEntry file;
        void HandleSelection(IFileListEntry[] files)
        {
            file = files.FirstOrDefault();
        }

        protected override async Task OnInitializedAsync()
        {
            employee = new EmployeeVM { DateOfBirth = DateTime.Now, JoiningDate = DateTime.Now };
            await BindList();
        }
        async Task BindList()
        {
            //Fill DropDown
            var ListReturndrp = await Http.GetAsync("api/Employee/GetListsOfDDl");
            var vMMs = JObject.Parse(ListReturndrp.Content.ReadAsStringAsync().Result);
            var ListDDL = JsonConvert.DeserializeObject<EmployeeVM>(vMMs["data"].ToString());
            BranchList = ListDDL.BranchList;
            DepartmentList = ListDDL.DepartmentList;
        }
        //bool ValidateEmployee()
        //{
        //    if (employee.BranchID == 0)
        //    {
        //        ValidateMessageBranch = "عليك اختيار الفرع";
        //    }
        //    else
        //    {
        //        ValidateMessageBranch = "";
        //    }
        //    if (employee.DepartmentID == 0)
        //    {
        //        ValidateMessageDepartment = "عليك اختيار القسم";
        //    }
        //    else
        //    {
        //        ValidateMessageDepartment = "";
        //    }
        //    DateTime CheckDate = new DateTime();
        //    if (employee.DateOfBirth == CheckDate)
        //    {
        //        ValidateMessageBirthD = "عليك ادخال تاريخ الميلاد";
        //    }
        //    else
        //    {
        //        ValidateMessageBirthD = "";
        //    }
        //    if (employee.JoiningDate == CheckDate)
        //    {
        //        ValidateMessageJoinD = "عليك ادخال تاريخ الالتحاق بالشركة";
        //    }
        //    else
        //    {
        //        ValidateMessageJoinD = "";
        //    }
        //    if (employee.IsUser)
        //    {
        //        if (employee.UserName.Trim() == "")
        //        {
        //            ValidateMessageUsername = "عليك ادخال اسم المستخدم";
        //        }
        //        else
        //        {
        //            ValidateMessageUsername = "";
        //        }
        //        if (employee.Passwordouble.Trim() == "")
        //        {
        //            ValidateMessagePass = "عليك ادخال كلمة السر";
        //        }
        //        else
        //        {
        //            ValidateMessagePass = "";
        //        }
        //    }
        //    if (ValidateMessagCode == "" && ValidateMessageDepartment == "" && ValidateMessageBranch == "" && ValidateMessageUsername == "" && ValidateMessagePass == "" && ValidateMessageBirthD == "" && ValidateMessageJoinD == "")
        //    { 
        //        return true; 
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}
        protected async Task CheckCode()
        {
            if (employee.Code != null)
            {
                string uri = "/api/Employee/CheckEmployeeCode?Code=" + employee.Code.Trim() + "&Id=" + employee.ID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateMessagCode = "كود الموظف موجود من قبل";
                }
                else
                {
                    ValidateMessagCode = "";
                }
            }
        }
        protected async Task CheckName()
        {
            if (employee.Name != null)
            {
                string uri = "/api/Employee/CheckEmployeeName?Name=" + employee.Name.Trim() + "&Id=" + employee.ID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateMessagName = "الاسم موجود من قبل";
                }
                else
                {
                    ValidateMessagName = "";
                }
            }
        }

        protected async Task CheckArrangement()
        {
            if (employee.arrangement != null)
            {
                string uri = "/api/Employee/CheckArrangement?arrangement=" + employee.arrangement + "&Id=" + employee.ID;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                string result = vMs["data"].ToString();
                if (result == "True")
                {
                    ValidateMessageArrangement = "الترتيب موجود من قبل";
                }
                else
                {
                    ValidateMessageArrangement = "";
                }
            }
        }
        //protected async Task CheckMail()
        //{
        //    string uri = "/api/Employee/CheckEmployeeMail?Mail=" + employee.Mail + "&Id=" + employee.ID;
        //    HttpResponseMessage response = await Http.GetAsync(uri);
        //    var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
        //    string result = vMs["data"].ToString();
        //    if (result == "True")
        //    {
        //        ValidateMessagMail = "البريد الالكترونى موجود من قبل";
        //    }
        //    else
        //    {
        //        ValidateMessagMail = "";
        //    }
        //}
        //protected async Task CheckPhone()
        //{
        //    string uri = "/api/Employee/CheckEmployeePhone?Phone=" + employee.Phone + "&Id=" + employee.ID;
        //    HttpResponseMessage response = await Http.GetAsync(uri);
        //    var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
        //    string result = vMs["data"].ToString();
        //    if (result == "True")
        //    {
        //        ValidateMessagPhone = "رقم التليفون موجود من قبل";
        //    }
        //    else
        //    {
        //        ValidateMessagPhone = "";
        //    }
        //}
        //protected async Task CheckMobile()
        //{
        //    string uri = "/api/Employee/CheckEmployeeMobile?Mobile=" + employee.Mobile + "&Id=" + employee.ID;
        //    HttpResponseMessage response = await Http.GetAsync(uri);
        //    var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
        //    string result = vMs["data"].ToString();
        //    if (result == "True")
        //    {
        //        ValidateMessagMobile = "رقم الموبايل موجود من قبل";
        //    }
        //    else
        //    {
        //        ValidateMessagMobile = "";
        //    }

        //}
        protected async Task AddEmployee()
        {
            IsDisabled = true;
            //if (ValidateMessagName == "" && ValidateMessagMail == "" && ValidateMessagPhone == "" && ValidateMessagMobile == "")
            //{
            if (ValidateMessagName == "" & ValidateMessagCode == "" & ValidateMessageArrangement == "")
            {


                //if (ValidateEmployee())
                //{
                string pathlogo = $"/Employees/";
                if (!Directory.Exists(pathlogo))
                    Directory.CreateDirectory(pathlogo);
                if (file != null && file.Size > 0)
                {
                    var namelogo = Guid.NewGuid().ToString() + Path.GetExtension(file.Name);
                    var ms = new MemoryStream();
                    await file.Data.CopyToAsync(ms);
                    var contentFile = new MultipartFormDataContent {
                {
                            new ByteArrayContent(ms.GetBuffer()), "\"Employees\"", namelogo}
                    };
                    await Http.PostAsync("upload?FolderName=" + "Employees", contentFile);

                    employee.Image = "/Images/Employees/" + namelogo;
                }
               
                //var AddModel2 = JsonConvert.SerializeObject(userModel);
                //var content2 = new StringContent(AddModel2, Encoding.UTF8, "application/json");
               
                //var result2 = await Http.PostAsync("api/Auth/Register", content2);//result2.IsSuccessStatusCode &&
                var AddModel = JsonConvert.SerializeObject(employee);
                var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
             
                var result = await Http.PostAsync("api/Employee/AddEmployee", content);
            
             
                if ( result.IsSuccessStatusCode)
                {
                    await BindList();
                    ShowConfirm = true;
                    showAddrow = false;
                }
                // }
            }
        
        }
        protected void Cancel()
        {
            showAddrow = false;
            showEditrow = false;
        }
        void Confirm()
        {
            // await navigationManager.NavigateToExitWindowAsync(jsRuntime);
            ShowConfirm = false;
        }
    }
}