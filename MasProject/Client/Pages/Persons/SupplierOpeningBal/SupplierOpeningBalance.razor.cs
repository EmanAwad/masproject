﻿using MasProject.Data.Models.Persons;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Persons;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using Microsoft.JSInterop;
namespace MasProject.Client.Pages.Persons. SupplierOpeningBal
{
    public partial class  SupplierOpeningBalance
    {
        private PageTitle PageTitle;
        List< SupplierOpeningBalanceVM>  SupplierOpeningBalanceList;
        //List<SupplierOpeningBalanceDetails> SupplierOpeningBalanceDetailsList;
        bool ShowDelete = false;
        int OpenBalID;
        int SupplierId;
        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        protected async Task BindList()
        {
            var ListReturn = await Http.GetAsync("api/SupplierOpeningBalance/GetSupplierOpeningBalance");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
             SupplierOpeningBalanceList = JsonConvert.DeserializeObject<List< SupplierOpeningBalanceVM>>(vMs["data"].ToString());
        }
        protected async Task AddNewSupplierOpeningBalance()
        {
            await jsRuntime.InvokeAsync<object>("open", "/SupplierOpeningBalanceAdd", "_blank");

        }

        protected async Task EditOldSupplierOpeningBalance(int ID,string Type)
        {
            //await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/SupplierOpeningBalanceEdit?ID=" + ID + "&Type=" + Type, "جارى التحميل" ,650);
            await jsRuntime.InvokeAsync<object>("open", "/SupplierOpeningBalanceEdit?ID=" + ID + "&Type=" + Type, "_blank");
        }
        protected void DeleteSupplierOpeningBalance(int Id, int Supplierid)
        {
            SupplierId = Supplierid;
            OpenBalID = Id;
            ShowDelete = true;
        }
        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                string uri = "/api/SupplierOpeningBalance/DeleteSupplierOpeningBalance?ID=" + OpenBalID + "&SupplierId=" + SupplierId;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
            ShowDelete = false;
        }

    }
}