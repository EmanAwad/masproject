﻿using MasProject.Data.Models.Persons;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Persons;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
namespace MasProject.Client.Pages.Persons.SupplierOpeningBal
{
    public partial class SupplierOpeningBalanceEdit
    {
        private PageTitle PageTitle;
        SupplierOpeningBalanceVM balance = new SupplierOpeningBalanceVM();
        List<LookupKeyValueVM> SupplierList = new List<LookupKeyValueVM>();
        List<LookupKeyValueVM> CurrencyList = new List<LookupKeyValueVM>();
        SupplierOpeningBalaneDetailsVM balancedetails = new SupplierOpeningBalaneDetailsVM();
        List<SupplierOpeningBalaneDetailsVM> SupplierDetailsList = new List<SupplierOpeningBalaneDetailsVM>();

        bool ShowConfirm = false;
        bool IsDisabled = false;
        protected override async Task OnInitializedAsync()
        {
            var querystring = ExtensionMethods.QueryString(nav);
            var id = querystring["ID"];
            await EditOldSupplierOpeningBalance(int.Parse(id));
            var Type = querystring["Type"];
            if (Type == "View")
            {
                IsDisabled = true;
                PageTitle.Title = "عرض  الارصدة الافتتاحية للموردين";
            }
            else
            {
                IsDisabled = false;
                PageTitle.Title = "تعديل الارصدة الافتتاحية للموردين";
            }
            
        }
    

        protected async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
            IsDisabled = false;

        }
       
        protected async Task GetCurrency(SupplierOpeningBalaneDetailsVM row)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            string uri = "/api/Supplier/GetSupplierCurrency?Id=" + row.SupplierID;
            response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            row.CurrencyID = JsonConvert.DeserializeObject<int>(vMs["data"].ToString());
            if (row.CurrencyID > 0)
            {
                uri = "/api/Currency/GetCurrencyRatio?Id=" + row.CurrencyID;
                response = await Http.GetAsync(uri);
                vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                row.Ratio = JsonConvert.DeserializeObject<double>(vMs["data"].ToString());
            }
            else
            {
                //get default currency egyptian pound//new 23-12-2020//must add check for default currency
                uri = "/api/Currency/GetSpecificCurrency?Id=2";
                response = await Http.GetAsync(uri);
                vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                CurrencyVM temp = JsonConvert.DeserializeObject<CurrencyVM>(vMs["data"].ToString());
                row.CurrencyID = temp.ID;
                row.Ratio = temp.Ratio;
            }
            StateHasChanged();
        }

        
        protected async Task GetRatio(SupplierOpeningBalaneDetailsVM row)
        {
            string uri = "/api/Currency/GetCurrencyRatio?Id=" + row.CurrencyID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            row.Ratio = JsonConvert.DeserializeObject<double>(vMs["data"].ToString());
            StateHasChanged();
        }
        protected async Task EditOldSupplierOpeningBalance(int ID)
        {
            string uri = "/api/SupplierOpeningBalance/GetSpecificSupplierOpeningBalance?Id=" + ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            balance = JsonConvert.DeserializeObject<SupplierOpeningBalanceVM>(vMs["data"].ToString());
            SupplierList = balance.SupplierList;
            CurrencyList = balance.CurrencyList;
            SupplierDetailsList = balance.SupplierDetails;
        }

       
        protected async Task EditSupplierOpeningBalance()
        {
            // IsTaskRunning = true;
            HttpResponseMessage result = new HttpResponseMessage();
            if (balance.DocumentNumber > 0)
            {
                balance.SupplierDetails = SupplierDetailsList;
                var EditModel = JsonConvert.SerializeObject(balance);
                var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                result = await Http.PostAsync("api/SupplierOpeningBalance/EditSupplierOpeningBalance", content);
                if (result.IsSuccessStatusCode)
                {
                    ShowConfirm = true;
                }
                StateHasChanged();
            }
        }
        protected async Task AddSupplierOpeningBalance(SupplierOpeningBalaneDetailsVM Sup)
        {
            StateHasChanged();
            HttpResponseMessage result = new HttpResponseMessage();
            if (balance.DocumentNumber > 0)
            {

                balance.SupplierDetails = SupplierDetailsList;
                var EditModel = JsonConvert.SerializeObject(balance);
                var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                result = await Http.PostAsync("api/SupplierOpeningBalance/EditSupplierOpeningBalance", content);
                await GetCurrency(Sup);

            }
            if (result.IsSuccessStatusCode)
            {
                StateHasChanged();
            }
        }

        void RemoveRow(SupplierOpeningBalaneDetailsVM row)
        {
            SupplierDetailsList.Remove(row);
            StateHasChanged();
        }

        void AddNewRow()
        {
            balancedetails = new SupplierOpeningBalaneDetailsVM { Note = "" };
            SupplierDetailsList.Add(balancedetails);
            StateHasChanged();
        }
    }
}