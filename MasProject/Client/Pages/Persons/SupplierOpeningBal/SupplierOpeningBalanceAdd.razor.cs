﻿using MasProject.Data.Models.Persons;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Persons;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
namespace MasProject.Client.Pages.Persons.SupplierOpeningBal
{
    public partial class SupplierOpeningBalanceAdd
    {
        private PageTitle PageTitle;
       // List<SupplierOpeningBalanceVM> SupplierOpenningList = new List<SupplierOpeningBalanceVM>();
       // SupplierOpeningBalanceVM Supplier = new SupplierOpeningBalanceVM();
        SupplierOpeningBalanceVM balance = new SupplierOpeningBalanceVM();
        SupplierOpeningBalaneDetailsVM balancedetails = new SupplierOpeningBalaneDetailsVM();
        List<LookupKeyValueVM> SupplierList = new List<LookupKeyValueVM>();
        List<LookupKeyValueVM> CurrencyList = new List<LookupKeyValueVM>();
        List<SupplierOpeningBalaneDetailsVM> SupplierDetailsList = new List<SupplierOpeningBalaneDetailsVM>();
        bool ShowConfirm = false;
        bool SelectSupplierChecker = false;
        bool IsTaskRunning = false;
        bool FirstAdd = false;
        protected override async Task OnInitializedAsync()
        {
            await BindList();
            balancedetails = new SupplierOpeningBalaneDetailsVM { Note = "" };
            SupplierDetailsList.Add(balancedetails);
        }
        async Task BindList()
        {
            //fill DropDown
            balance = new SupplierOpeningBalanceVM { };
            var TypeListReturn = await Http.GetAsync("api/SupplierOpeningBalance/GetListsOfDDl");
            var TypevMs = JObject.Parse(TypeListReturn.Content.ReadAsStringAsync().Result);
            balance = JsonConvert.DeserializeObject<SupplierOpeningBalanceVM>(TypevMs["data"].ToString());
            SupplierList = balance.SupplierList;
            CurrencyList = balance.CurrencyList;
            balance.EntryDate = DateTime.Now;
        }
        void AddNewRow()
        {
            balancedetails = new SupplierOpeningBalaneDetailsVM { Note = "" };
            SupplierDetailsList.Add(balancedetails);
            StateHasChanged();
        }
        void RemoveRow(SupplierOpeningBalaneDetailsVM row)
        {
            SupplierDetailsList.Remove(row);
            StateHasChanged();
        }
   
        protected async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        }

        protected async Task GetCurrency(SupplierOpeningBalaneDetailsVM row)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            string uri = "/api/Supplier/GetSupplierCurrency?Id=" + row.SupplierID;
            response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            row.CurrencyID = JsonConvert.DeserializeObject<int>(vMs["data"].ToString());
            if (row.CurrencyID > 0)
            {
                uri = "/api/Currency/GetCurrencyRatio?Id=" + row.CurrencyID;
                response = await Http.GetAsync(uri);
                vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                row.Ratio = JsonConvert.DeserializeObject<double>(vMs["data"].ToString());
            }
            else
            {
                //get default currency egyptian pound//new 23-12-2020//must add check for default currency
                uri = "/api/Currency/GetSpecificCurrency?Id=2";
                response = await Http.GetAsync(uri);
                vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                CurrencyVM temp = JsonConvert.DeserializeObject<CurrencyVM>(vMs["data"].ToString());
                row.CurrencyID = temp.ID;
                row.Ratio = temp.Ratio;
            }
            StateHasChanged();
        }
        protected async Task GetRatio(SupplierOpeningBalaneDetailsVM row)
        {
            string uri = "/api/Currency/GetCurrencyRatio?Id=" + row.CurrencyID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            row.Ratio = JsonConvert.DeserializeObject<double>(vMs["data"].ToString());
            StateHasChanged();
        }
        protected async Task AddSupplierOpeningBalance(SupplierOpeningBalaneDetailsVM Sup)
        {
            StateHasChanged();
            HttpResponseMessage result = new HttpResponseMessage();
            if (balance.DocumentNumber > 0)
            {
                if (FirstAdd == false)
                {
                    FirstAdd = true;

                    balance.SupplierDetails = SupplierDetailsList;
                    var AddModel = JsonConvert.SerializeObject(balance);
                    var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/SupplierOpeningBalance/AddSupplierOpeningBalance", content);
                    var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    balance.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                    await GetCurrency(Sup);
                }
                else
                {
                    balance.SupplierDetails = SupplierDetailsList;
                    var EditModel = JsonConvert.SerializeObject(balance);
                    var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/SupplierOpeningBalance/EditSupplierOpeningBalance", content);
                    await GetCurrency(Sup);
                }
            }
            if (result.IsSuccessStatusCode)
            {
                StateHasChanged();
            }
        }
        protected async Task EditSupplierOpeningBalance()
        {
            IsTaskRunning = true;
            HttpResponseMessage result = new HttpResponseMessage();
            if (balance.DocumentNumber > 0)
            {
                balance.SupplierDetails = SupplierDetailsList;
                var EditModel = JsonConvert.SerializeObject(balance);
                var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                result = await Http.PostAsync("api/SupplierOpeningBalance/EditSupplierOpeningBalance", content);

                if (result.IsSuccessStatusCode)
                {
                    ShowConfirm = true;
                }
                StateHasChanged();
            }
        }
    }
}