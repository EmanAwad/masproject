﻿using MasProject.Data.Models.Persons;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Persons;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
namespace MasProject.Client.Pages.Persons.EmployeeOpeningBal
{
    public partial class EmployeeOpeningBalanceEdit
    {
        private PageTitle PageTitle;
        EmployeeOpeningBalanceVM balance = new EmployeeOpeningBalanceVM();
        List<LookupKeyValueVM> EmployeeList = new List<LookupKeyValueVM>();
        bool ShowConfirm = false;
        string ValidateMessagePerson = "";
        string ValidateMessageCurrency = "";
        string ValidateMessageEntryD = "";
        bool IsDisabled = false;
        protected override async Task OnInitializedAsync()
        {
            await BindList();
            var querystring = ExtensionMethods.QueryString(nav);
            var id = querystring["ID"];
            await EditOldEmployeeOpeningBalance(int.Parse(id));
            var Type = querystring["Type"];
            if (Type == "View")
            {
                IsDisabled = true;
                PageTitle.Title = "عرض  الارصدة الافتتاحية للموظفين";
            }
            else
            {
                IsDisabled = false;
                PageTitle.Title = "تعديل الارصدة الافتتاحية للموظفين";
            }
        }
        async Task BindList()
        {
            //fill DropDown
            balance = new EmployeeOpeningBalanceVM { };
            var TypeListReturn = await Http.GetAsync("api/EmployeeOpeningBalance/GetListsOfDDl");
            var TypevMs = JObject.Parse(TypeListReturn.Content.ReadAsStringAsync().Result);
            var ListDDl = JsonConvert.DeserializeObject<EmployeeOpeningBalanceVM>(TypevMs["data"].ToString());
            EmployeeList = ListDDl.EmployeeList;
        }

        protected async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
            IsDisabled = false;

        }
        protected async Task EditOldEmployeeOpeningBalance(int ID)
        {
            
            string uri = "/api/EmployeeOpeningBalance/GetSpecificEmployeeOpeningBalance?Id=" + ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            balance = JsonConvert.DeserializeObject<EmployeeOpeningBalanceVM>(vMs["data"].ToString());
           
        }

        protected async Task EditEmployeeOpeningBalance()
        {
          
            //if (ValidateBalance())
            //{
            var EditModel = JsonConvert.SerializeObject(balance);
                var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/EmployeeOpeningBalance/EditEmployeeOpeningBalance", content);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                    ShowConfirm = true;
                }
           // }
        }

    }
}