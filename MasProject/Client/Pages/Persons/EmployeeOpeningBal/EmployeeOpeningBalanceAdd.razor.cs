﻿using MasProject.Data.Models.Persons;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Persons;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
namespace MasProject.Client.Pages.Persons.EmployeeOpeningBal
{
    public partial class EmployeeOpeningBalanceAdd
    {
        private PageTitle PageTitle;
        List<EmployeeOpeningBalanceVM> EmployeeOpenningList = new List<EmployeeOpeningBalanceVM>();
        EmployeeOpeningBalanceVM employee = new EmployeeOpeningBalanceVM();
        EmployeeOpeningBalanceVM balance = new EmployeeOpeningBalanceVM();
        List<LookupKeyValueVM> EmployeeList = new List<LookupKeyValueVM>();
        bool ShowConfirm = false;
        bool SelectEmployeeChecker= false;
        bool IsDisabled = false;
        protected override async Task OnInitializedAsync()
        {
            employee = new EmployeeOpeningBalanceVM();
            balance = new EmployeeOpeningBalanceVM { EntryDate = DateTime.Now, Note = "" };
            EmployeeOpenningList = new List<EmployeeOpeningBalanceVM>();
            EmployeeOpenningList.Add(balance);
            await BindList();
        }
        async Task BindList()
        {
            //fill DropDown
            balance = new EmployeeOpeningBalanceVM { };
            var TypeListReturn = await Http.GetAsync("api/EmployeeOpeningBalance/GetListsOfDDl");
            var TypevMs = JObject.Parse(TypeListReturn.Content.ReadAsStringAsync().Result);
            balance = JsonConvert.DeserializeObject<EmployeeOpeningBalanceVM>(TypevMs["data"].ToString());
            EmployeeList = balance.EmployeeList;
        }
        void AddNewRow()
        {
            employee = new EmployeeOpeningBalanceVM { EntryDate = DateTime.Now, Note = "" };
            EmployeeOpenningList.Add(employee);
            StateHasChanged();
        }
        void RemoveRow(EmployeeOpeningBalanceVM row)
        {
            EmployeeOpenningList.Remove(row);
            StateHasChanged();
        }
        //bool ValidateBalance()
        //{
        //    if (balance.CurrencyID == 0)
        //    {
        //        ValidateMessageCurrency = "عليك اختيار العميلة";
        //    }
        //    else
        //    {
        //        ValidateMessageCurrency = "";
        //    }
        //    if (balance.EmployeeID == 0)
        //    {
        //        ValidateMessagePerson = "عليك اختيار الموظف";
        //    }
        //    else
        //    {
        //        ValidateMessagePerson = "";
        //    }
        //    DateTime CheckDate = new DateTime();
        //    if (balance.EntryDate == CheckDate)
        //    {
        //        ValidateMessageEntryD = "عليك ادخال تاريخ ";
        //    }
        //    else
        //    {
        //        ValidateMessageEntryD = "";
        //    }

        //    if (ValidateMessageCurrency == "" && ValidateMessagePerson == "" && ValidateMessageEntryD == "")
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        protected async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
            IsDisabled = false;

        }

        protected async Task AddEmployeeOpeningBalance(EmployeeOpeningBalanceVM Emp)
        {
            IsDisabled = true;
            //if (ValidateBalance())
            //{

            HttpResponseMessage result = new HttpResponseMessage();
            if (Emp.EmployeeID != 0 && Emp.EmployeeID!=null )
            {
                if (Emp.ID == 0)
                {
                    var AddModel = JsonConvert.SerializeObject(Emp);
                    var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/EmployeeOpeningBalance/AddEmployeeOpeningBalance", content);
                    var vMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    Emp.ID = JsonConvert.DeserializeObject<int>(vMs["data"].ToString());
                }
                else
                {
                    var AddModel = JsonConvert.SerializeObject(Emp);
                    var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/EmployeeOpeningBalance/EditEmployeeOpeningBalance", content);
                }
                if (result.IsSuccessStatusCode)
                {
                    StateHasChanged();
                    ShowConfirm = true;
                }
            }
            else
            {
                SelectEmployeeChecker = true;
            }
            //}
        }
    }
}