﻿using MasProject.Data.Models.Persons;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Persons;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using Microsoft.JSInterop;
namespace MasProject.Client.Pages.Persons.EmployeeOpeningBal
{
    public partial class EmployeeOpeningBalance
    {
        private PageTitle PageTitle;
        List<EmployeeOpeningBalanceVM> EmployeeOpeningBalanceList;
        bool ShowDelete = false;
        int OpenBalID;
        int EmployeeId;
        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        protected async Task BindList()
        {
            var ListReturn = await Http.GetAsync("api/EmployeeOpeningBalance/GetEmployeeOpeningBalance");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            EmployeeOpeningBalanceList = JsonConvert.DeserializeObject<List<EmployeeOpeningBalanceVM>>(vMs["data"].ToString());
        }
        protected async Task AddNewEmployeeOpeningBalance()
        {
            await jsRuntime.InvokeAsync<object>("open", "/EmployeeOpeningBalanceAdd", "_blank");

        }
        protected async Task EditOldEmployeeOpeningBalance(int ID,string Type)
        {
            await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/EmployeeOpeningBalanceEdit?ID=" + ID + "&Type=" + Type, "جارى التحميل" ,650);

        }
        protected void DeleteEmployeeOpeningBalance(int Id,  int Employeeid)
        {
            OpenBalID = Id;
            EmployeeId = Employeeid;
            ShowDelete = true;
        }
        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                string uri = "/api/EmployeeOpeningBalance/DeleteEmployeeOpeningBalance?ID="+ OpenBalID +"&EmployeeId=" + EmployeeId;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
            ShowDelete = false;
        }

    }
}