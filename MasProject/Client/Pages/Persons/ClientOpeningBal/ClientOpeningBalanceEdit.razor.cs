﻿using MasProject.Data.Models.Persons;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Persons;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
namespace MasProject.Client.Pages.Persons.ClientOpeningBal
{
    public partial class ClientOpeningBalanceEdit
    {
        private PageTitle PageTitle;
        ClientOpeningBalanceVM balance = new ClientOpeningBalanceVM();
        ClientOpeningBalanceDetailsVM balanceDetails = new ClientOpeningBalanceDetailsVM();
        List<ClientOpeningBalanceDetailsVM> BalanceDetailsList = new List<ClientOpeningBalanceDetailsVM>();
        List<LookupKeyValueVM> ClientList = new List<LookupKeyValueVM>();
        List<LookupKeyValueVM> BranchList = new List<LookupKeyValueVM>();
        List<LookupKeyValueVM> EmployeeList = new List<LookupKeyValueVM>();

        // bool FirstAdd = false;
        bool ShowConfirm = false;
        bool IsDisabled = false;
        protected override async Task OnInitializedAsync()
        {
            // await BindList();
            var querystring = ExtensionMethods.QueryString(nav);
            var id = querystring["ID"];
            await EditOldClientOpeningBalance(int.Parse(id));
            var Type = querystring["Type"];
            if (Type == "View")
            {
                IsDisabled = true;
                PageTitle.Title = "عرض  الارصدة الافتتاحية للعملاء";
            }
            else
            {
                IsDisabled = false;
                PageTitle.Title = "تعديل الارصدة الافتتاحية للعملاء";
            }
        }
        //async Task BindList()
        //{
        //    //fill DropDown
        //    balance = new ClientOpeningBalanceVM { };
        //    var TypeListReturn = await Http.GetAsync("api/ClientOpeningBalance/GetListsOfDDl");
        //    var TypevMs = JObject.Parse(TypeListReturn.Content.ReadAsStringAsync().Result);
        //    var ListDDl = JsonConvert.DeserializeObject<ClientOpeningBalanceVM>(TypevMs["data"].ToString());
        //    ClientList = ListDDl.ClientList;
        //    EmployeeList = ListDDl.EmployeeList;
        //    BranchList = ListDDl.BranchList;

        //}
        void AddNewRow()
        {
            balanceDetails = new ClientOpeningBalanceDetailsVM { Note = "" };
            BalanceDetailsList.Add(balanceDetails);
            StateHasChanged();
        }
        void RemoveRow(ClientOpeningBalanceDetailsVM row)
        {
            BalanceDetailsList.Remove(row);
            StateHasChanged();
        }
        void ChangeItemAmount()
        {
            AddNewRow();
        }
        protected async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
            //ShowConfirm = false;
            //IsTaskRunning = false;
        }
      
        protected async Task EditOldClientOpeningBalance(int ID)
        {
            //FirstAdd = true;
            string uri = "/api/ClientOpeningBalance/GetSpecificClientOpeningBalance?Id=" + ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            balance = JsonConvert.DeserializeObject<ClientOpeningBalanceVM>(vMs["data"].ToString());
            BalanceDetailsList = balance.ClientDetails;
            ClientList = balance.ClientList;
            EmployeeList = balance.EmployeeList;
            BranchList = balance.BranchList;
        }
        protected async Task EditClientOpeningBalance()
        {

            HttpResponseMessage result = new HttpResponseMessage();
            balance.ClientDetails = BalanceDetailsList;
            var EditModel = JsonConvert.SerializeObject(balance);
            var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
            result = await Http.PostAsync("api/ClientOpeningBalance/EditClientOpeningBalance", content);
            if (result.IsSuccessStatusCode)
            {
                ShowConfirm = true;
            }
            StateHasChanged();
        }

        async Task ReloadClientsList(ClientOpeningBalanceDetailsVM row)
        {
            row.ClientName = String.Empty;
            var ListReturn = await Http.GetAsync("api/Client/GetClients");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ClientList = JsonConvert.DeserializeObject<List<LookupKeyValueVM>>(vMs["data"].ToString());

        }
    }
}