﻿using MasProject.Data.Models.Persons;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Persons;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using Microsoft.JSInterop;
namespace MasProject.Client.Pages.Persons.ClientOpeningBal
{
    public partial class ClientOpeningBalance
    {
        private PageTitle PageTitle;
        List<ClientOpeningBalanceVM> ClientOpeningBalanceList;
        bool ShowDelete = false;
        int OpenBalID;
        int DocumentNumber;
        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        protected async Task BindList()
        {
            var ListReturn = await Http.GetAsync("api/ClientOpeningBalance/GetClientOpeningBalance");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ClientOpeningBalanceList = JsonConvert.DeserializeObject<List<ClientOpeningBalanceVM>>(vMs["data"].ToString());
        }
        protected async Task AddNewClientOpeningBalance()
        {
            await jsRuntime.InvokeAsync<object>("open", "/ClientOpeningBalanceAdd", "_blank");

        }

        protected async Task EditOldClientOpeningBalance(int ID,string Type)
        {

            await jsRuntime.InvokeAsync<object>("open", "/ClientOpeningBalanceEdit?ID=" + ID + "&Type=" + Type, "_blank");

        }
        protected void DeleteClientOpeningBalance(int documentNumber,int  Id)
        {
            DocumentNumber = documentNumber;
            OpenBalID = Id;
            ShowDelete = true;
        }
        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                string uri = "/api/ClientOpeningBalance/DeleteClientOpeningBalance?DocumentNumber="+DocumentNumber+"&ID=" + OpenBalID;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
            ShowDelete = false;
        }

    }
}