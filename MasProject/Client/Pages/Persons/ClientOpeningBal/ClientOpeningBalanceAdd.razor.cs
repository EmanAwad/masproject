﻿using MasProject.Data.Models.Persons;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Persons;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
namespace MasProject.Client.Pages.Persons.ClientOpeningBal
{
    public partial class ClientOpeningBalanceAdd
    {
        private PageTitle PageTitle;
        ClientOpeningBalanceVM balance = new ClientOpeningBalanceVM();
        ClientOpeningBalanceDetailsVM balanceDetails = new ClientOpeningBalanceDetailsVM();
        List<ClientOpeningBalanceDetailsVM> BalanceDetailsList = new List<ClientOpeningBalanceDetailsVM>();
        List<LookupKeyValueVM> ClientList = new List<LookupKeyValueVM>();
        List<LookupKeyValueVM> BranchList = new List<LookupKeyValueVM>();
        List<LookupKeyValueVM> EmployeeList = new List<LookupKeyValueVM>();
        bool FirstAdd = false;
        bool ShowConfirm = false;
        bool IsTaskRunning = false;

        protected override async Task OnInitializedAsync()
        {
            balanceDetails = new ClientOpeningBalanceDetailsVM { Note = "" };
            BalanceDetailsList.Add(balanceDetails);
            await BindList();
        }
        async Task BindList()
        {
            //fill DropDown
            balance = new ClientOpeningBalanceVM { };
            var TypeListReturn = await Http.GetAsync("api/ClientOpeningBalance/GetListsOfDDl");
            var TypevMs = JObject.Parse(TypeListReturn.Content.ReadAsStringAsync().Result);
            balance = JsonConvert.DeserializeObject<ClientOpeningBalanceVM>(TypevMs["data"].ToString());
            ClientList = balance.ClientList;
            BranchList = balance.BranchList;
            EmployeeList = balance.EmployeeList;
            balance.EntryDate = DateTime.Now;
        }
        void AddNewRow()
        {
            balanceDetails = new ClientOpeningBalanceDetailsVM { Note = "" };
            BalanceDetailsList.Add(balanceDetails);
            StateHasChanged();
        }
        void RemoveRow(ClientOpeningBalanceDetailsVM row)
        {
            BalanceDetailsList.Remove(row);
            StateHasChanged();
        }
        //bool ValidateBalance()
        //{
        //    if (balance.CurrencyID == 0)
        //    {
        //        ValidateMessageCurrency = "عليك اختيار العملة";
        //    }
        //    else
        //    {
        //        ValidateMessageCurrency = "";
        //    }
        //    if (balance.ClientID == 0)
        //    {
        //        ValidateMessagePerson = "عليك اختيار العميل";
        //    }
        //    else
        //    {
        //        ValidateMessagePerson = "";
        //    }
        //    DateTime CheckDate = new DateTime();
        //    if (balance.EntryDate == CheckDate)
        //    {
        //        ValidateMessageEntryD = "عليك ادخال تاريخ ";
        //    }
        //    else
        //    {
        //        ValidateMessageEntryD = "";
        //    }

        //    if (ValidateMessageCurrency == "" && ValidateMessagePerson == "" && ValidateMessageEntryD == "")
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}
        void ChangeItemAmount()
        {
            AddNewRow();
        }
        protected async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        }

        protected async Task AddClientOpeningBalance(ClientOpeningBalanceDetailsVM row)
        {
            row.ClientID = ClientList.Find(x => x.Name == row.ClientName).ID;
            StateHasChanged();
            HttpResponseMessage result = new HttpResponseMessage();
            //check for save
            if (balance.DocumentNumber > 0)
            {
                if (FirstAdd == false)
                {
                    FirstAdd = true;
                    var AddModel = JsonConvert.SerializeObject(balance);
                    var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/ClientOpeningBalance/AddClientOpeningBalance", content);
                    var AddvMs = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    balance.ID = JsonConvert.DeserializeObject<int>(AddvMs["data"].ToString());
                    
                }
                else
                {
                    balance.ClientDetails = BalanceDetailsList;
                    var EditModel = JsonConvert.SerializeObject(balance);
                    var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                    result = await Http.PostAsync("api/ClientOpeningBalance/EditClientOpeningBalance", content);
                }
                if (result.IsSuccessStatusCode)
                {
                    StateHasChanged();
                }
            }
        }
        protected async Task EditClientOpeningBalance()
        {
            IsTaskRunning = true;
            HttpResponseMessage result = new HttpResponseMessage();
            if (balance.DocumentNumber > 0)
            {
                balance.ClientDetails = BalanceDetailsList;
                var EditModel = JsonConvert.SerializeObject(balance);
                var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                result = await Http.PostAsync("api/ClientOpeningBalance/EditClientOpeningBalance", content);

                if (result.IsSuccessStatusCode)
                {
                    ShowConfirm = true;
                }
                StateHasChanged();
            }
        }

        async Task ReloadClientsList(ClientOpeningBalanceDetailsVM row)
        {
            row.ClientName = String.Empty;
            var ListReturn = await Http.GetAsync("api/Client/GetClients");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ClientList = JsonConvert.DeserializeObject<List<LookupKeyValueVM>>(vMs["data"].ToString());

        }
    }
}