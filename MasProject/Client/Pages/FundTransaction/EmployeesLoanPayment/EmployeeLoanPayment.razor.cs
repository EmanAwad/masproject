﻿using MasProject.Domain.ViewModel.FundTransaction;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using Microsoft.JSInterop;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Persons;
using System.Net.Http;

namespace MasProject.Client.Pages.FundTransaction.EmployeesLoanPayment
{
    public partial class EmployeeLoanPayment
    {
        private PageTitle PageTitle;
        List<EmployeesLoansPaymentVM> EmployeePaymentList;
        EmployeesLoansPaymentVM EmployeePayment = new EmployeesLoansPaymentVM();
         List<TreasuryVM> TreasuryList = new List<TreasuryVM>();
        List<EmployeeVM> EmployeeList = new List<EmployeeVM>();
        bool ShowDelete = false;
        int EmployeePaymentID; 
        int EmployeeId;
        int SerialNumber;
        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        {
            //Fill Table
            var ListReturn = await Http.GetAsync("api/EmployeesLoansPayment/GetEmployeesLoansPayment");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            EmployeePaymentList = JsonConvert.DeserializeObject<List<EmployeesLoansPaymentVM>>(vMs["data"].ToString());
            //Fill Drop
            var ListReturndrp = await Http.GetAsync("api/EmployeesLoansPayment/GetListsOfDDl");
            var vMMs = JObject.Parse(ListReturndrp.Content.ReadAsStringAsync().Result);
            var ListDDL = JsonConvert.DeserializeObject<EmployeesLoansPaymentVM>(vMMs["data"].ToString());
            TreasuryList = ListDDL.TreasuryList;
            EmployeeList = ListDDL.EmployeeList;
        }

        protected async Task GetAllByTreasury()
        {

            if (EmployeePayment.TreasuryId != 0)
            {
                string uri = "/api/EmployeesLoansPayment/GetEmployeesLoansPaymentByTres?TresID=" + EmployeePayment.TreasuryId;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                EmployeePaymentList = JsonConvert.DeserializeObject<List<EmployeesLoansPaymentVM>>(vMs["data"].ToString());
            }
            else
            {
                var ListReturn = await Http.GetAsync("api/EmployeesLoansPayment/GetEmployeesLoansPayment");
                var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
                EmployeePaymentList = JsonConvert.DeserializeObject<List<EmployeesLoansPaymentVM>>(vMs["data"].ToString());
            }


            StateHasChanged();
        }

        protected async Task GetAllByEmployee()
        {

            if (EmployeePayment.TreasuryId != 0)
            {
                string uri = "/api/EmployeesLoansPayment/GetEmployeesLoansPaymentByEmp?EmpID=" + EmployeePayment.EmployeeId;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                EmployeePaymentList = JsonConvert.DeserializeObject<List<EmployeesLoansPaymentVM>>(vMs["data"].ToString());
            }
            else
            {
                var ListReturn = await Http.GetAsync("api/EmployeesLoansPayment/GetEmployeesLoansPayment");
                var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
                EmployeePaymentList = JsonConvert.DeserializeObject<List<EmployeesLoansPaymentVM>>(vMs["data"].ToString());
            }


            StateHasChanged();
        }
        protected async Task AddNewEmployeesLoansPayment()
        {
        //    await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/EmployeeLoanPaymentAdd", "جارى التحميل", 640);
            await jsRuntime.InvokeAsync<object>("open", "/EmployeeLoanPaymentAdd", "_blank");
        }
        protected async Task EditOldEmployeesLoansPayment(int ID,string Type)
        {
            // await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/EmployeeLoanPaymentEdit?ID=" + ID, "جارى التحميل", 640);
            //await jsRuntime.InvokeAsync<object>("open", "/EmployeeLoanPaymentEdit?ID=" + ID, "_blank");
            await jsRuntime.InvokeAsync<object>("open", "/EmployeeLoanPaymentEdit?ID=" + ID + "&Type=" + Type, "_blank");
        }
        protected void DeleteEmployeePayment(int Id, int serialNumber, int Employeeid)
        {
            EmployeePaymentID = Id;
            EmployeeId = Employeeid;
            SerialNumber = serialNumber;
            ShowDelete = true;
        }
        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                string uri = "/api/EmployeesLoansPayment/DeleteEmployeesLoansPayment?ID=" + EmployeePaymentID + "&SerialNumber=" + SerialNumber + "&EmployeeId=" + EmployeeId;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
            ShowDelete = false;
        }
    }
}