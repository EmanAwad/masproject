﻿using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.FundTransaction;
using MatBlazor;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System;
using System.IO;
using BlazorInputFile;
using System.Linq;
using MasProject.Client.Shared;
namespace MasProject.Client.Pages.FundTransaction.EmployeesLoanPayment
{
    public partial class EmployeeLoanPaymentAdd
    {
        private PageTitle PageTitle;
        EmployeesLoansPaymentVM EmployeePayment = new EmployeesLoansPaymentVM();
        List<TreasuryVM> TreasuryList = new List<TreasuryVM>();
        List<EmployeeVM> EmployeeList = new List<EmployeeVM>();
        bool ShowConfirm = false;
        bool SelectEmployeeChecker = false;
        bool IsDisabled = false;
        protected override async Task OnInitializedAsync()
        {

            EmployeePayment = new EmployeesLoansPaymentVM();
            EmployeePayment.Date = DateTime.Now.Date;
            await BindList();
            var IdReturn = await Http.GetAsync("api/EmployeesLoansPayment/GenerateSerial");
            var IdvMs = JObject.Parse(IdReturn.Content.ReadAsStringAsync().Result);
            EmployeePayment.SerialNumber = JsonConvert.DeserializeObject<int>(IdvMs["data"].ToString());
        }
        async Task BindList()
        {
            //Fill DropDown
            var ListReturndrp = await Http.GetAsync("api/EmployeesLoansPayment/GetListsOfDDl");
            var vMMs = JObject.Parse(ListReturndrp.Content.ReadAsStringAsync().Result);
            var ListDDL = JsonConvert.DeserializeObject<EmployeesLoansPaymentVM>(vMMs["data"].ToString());
            TreasuryList = ListDDL.TreasuryList;
            EmployeeList = ListDDL.EmployeeList;
        }
        protected async Task AddEmployeePayment()
        {
            IsDisabled = true;
            if (EmployeePayment.EmployeeId != 0 && EmployeePayment.EmployeeId != null)
            {
                var AddModel = JsonConvert.SerializeObject(EmployeePayment);
                var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/EmployeesLoansPayment/AddEmployeesLoansPayment", content);
                if (result.IsSuccessStatusCode)
                {
                    ShowConfirm = true;
                }
            }
            else
            {
                SelectEmployeeChecker = true;
            }
        }
        protected async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        }


    }
}