﻿using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.FundTransaction;
using MatBlazor;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
namespace MasProject.Client.Pages.FundTransaction.EmployeesLoanPayment
{
    public partial class EmployeeLoanPaymentEdit
    {
        private PageTitle PageTitle;
        EmployeesLoansPaymentVM EmployeePayment = new EmployeesLoansPaymentVM();
        List<TreasuryVM> TreasuryList = new List<TreasuryVM>();
        List<EmployeeVM> EmployeeList = new List<EmployeeVM>();
        bool ShowConfirm = false;
        bool IsDisabled = false;
        bool SelectEmployeeChecker = false;
        protected override async Task OnInitializedAsync()
        {

            await BindList();
            var querystring = ExtensionMethods.QueryString(nav);
            var id = querystring["ID"];
            await EditOldEmployeesLoansPayment(int.Parse(id));
            var Type = querystring["Type"];
            if (Type == "View")
            {
                //disable controls
                IsDisabled = true;
                PageTitle.Title = "عرض   سداد سلف الموظفين";
            }
            else
            {
                IsDisabled = false;
                PageTitle.Title = "تعديل  سداد سلف الموظفين ";
            }
        }
        async Task BindList()
        {
            //Fill DropDown
            var ListReturndrp = await Http.GetAsync("api/EmployeesLoansPayment/GetListsOfDDl");
            var vMMs = JObject.Parse(ListReturndrp.Content.ReadAsStringAsync().Result);
            var ListDDL = JsonConvert.DeserializeObject<EmployeesLoansPaymentVM>(vMMs["data"].ToString());
            TreasuryList = ListDDL.TreasuryList;
            EmployeeList = ListDDL.EmployeeList;
        }
        protected async Task Confirm()
        {
            await nav.NavigateToExitWindowAsync(jsRuntime);
        }
        protected async Task EditOldEmployeesLoansPayment(int ID)
        {
            string uri = "/api/EmployeesLoansPayment/GetSpecificEmployeesLoansPayment?Id=" + ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            EmployeePayment = JsonConvert.DeserializeObject<EmployeesLoansPaymentVM>(vMs["data"].ToString());
        }
        protected async Task EditEmployeesLoansPayment()
        {
            if (EmployeePayment.EmployeeId != 0 && EmployeePayment.EmployeeId != null)
            {
                var EditModel = JsonConvert.SerializeObject(EmployeePayment);
                var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/EmployeesLoansPayment/EditEmployeesLoansPayment", content);
                if (result.IsSuccessStatusCode)
                {
                    ShowConfirm = true;
                }
            }
            else
            {
                SelectEmployeeChecker = true;
            }
        }
    }
}