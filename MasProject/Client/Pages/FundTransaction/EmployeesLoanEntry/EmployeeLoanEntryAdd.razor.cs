﻿using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.FundTransaction;
using MatBlazor;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System;
using System.IO;
using BlazorInputFile;
using System.Linq;
using MasProject.Client.Shared;
namespace MasProject.Client.Pages.FundTransaction.EmployeesLoanEntry
{
    public partial class EmployeeLoanEntryAdd
    {
        private PageTitle PageTitle;
        //  List<EmployeesLoansEntryVM> EmployeeEntryList;
        EmployeesLoansEntryVM EmployeeEntry = new EmployeesLoansEntryVM();
        List<CurrencyVM> CurrencyList = new List<CurrencyVM>();
        List<TreasuryVM> TreasuryList = new List<TreasuryVM>();
        List<EmployeeVM> EmployeeList = new List<EmployeeVM>();
    
        bool ShowConfirm = false;
        bool SelectEmployeeChecker = false;
        bool IsDisabled = false;
        protected override async Task OnInitializedAsync()
        {

            EmployeeEntry = new EmployeesLoansEntryVM();
            EmployeeEntry.Date = DateTime.Now.Date;

            await BindList();
            var IdReturn = await Http.GetAsync("api/EmployeesLoansEntry/GenerateSerial");
            var IdvMs = JObject.Parse(IdReturn.Content.ReadAsStringAsync().Result);
            EmployeeEntry.SerialNumber = JsonConvert.DeserializeObject<int>(IdvMs["data"].ToString());
        }
        async Task BindList()
        {
            //Fill DropDown
            var ListReturndrp = await Http.GetAsync("api/EmployeesLoansEntry/GetListsOfDDl");
            var vMMs = JObject.Parse(ListReturndrp.Content.ReadAsStringAsync().Result);
            var ListDDL = JsonConvert.DeserializeObject<EmployeesLoansEntryVM>(vMMs["data"].ToString());
            TreasuryList = ListDDL.TreasuryList;
            EmployeeList = ListDDL.EmployeeList;
        }
        //protected async Task GetCurrencyRatio()
        //{
        //    string uri = "/api/EmployeeEntry/GetCurrencyRatio?Id=" + EmployeeEntry.CurrencyId;
        //    HttpResponseMessage response = await Http.GetAsync(uri);
        //    var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
        //    EmployeeEntry.Ratio  = JsonConvert.DeserializeObject<double>(vMs["data"].ToString());
        //    if (EmployeeEntry.Amount != null && EmployeeEntry.Amount > 0 && EmployeeEntry.Ratio > 0)
        //    {
        //        EmployeeEntry.Total = EmployeeEntry.Amount.Value * EmployeeEntry.Ratio;
        //    }
        //    StateHasChanged();
        //}
        //protected void GetTotal()
        //{
        //    var total = 0.0;
        //    if (EmployeeEntry.Amount != null && EmployeeEntry.Amount > 0 && EmployeeEntry.Ratio > 0)
        //    {
        //        total = EmployeeEntry.Amount.Value * EmployeeEntry.Ratio;
        //    }
        //    EmployeeEntry.Total = total;
        //    StateHasChanged();
        //}
        protected async Task AddEmployeeEntry()
        {

            IsDisabled = true;
            if (EmployeeEntry.EmployeeId != 0 && EmployeeEntry.EmployeeId != null)
            {
                var AddModel = JsonConvert.SerializeObject(EmployeeEntry);
                var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/EmployeesLoansEntry/AddEmployeesLoansEntry", content);
                if (result.IsSuccessStatusCode)
                {
                    ShowConfirm = true;
                }
            }
            else
            {
                SelectEmployeeChecker = true;
            }
        }
        protected async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        }
    }
}