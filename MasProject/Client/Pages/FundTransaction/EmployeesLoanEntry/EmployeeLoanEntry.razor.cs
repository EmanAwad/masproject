﻿using MasProject.Domain.ViewModel.FundTransaction;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using Microsoft.JSInterop;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Persons;
using System.Net.Http;

namespace MasProject.Client.Pages.FundTransaction.EmployeesLoanEntry
{
    public partial class EmployeeLoanEntry
    {
        private PageTitle PageTitle;
        List<EmployeesLoansEntryVM> EmployeeEntryList;
        EmployeesLoansEntryVM EmployeeEntry = new EmployeesLoansEntryVM();
      
      
        List<TreasuryVM> TreasuryList = new List<TreasuryVM>();
        List<EmployeeVM> EmployeeList = new List<EmployeeVM>();
        bool ShowDelete = false;
        int EmployeeEntryID;
        int EmployeeId;
        int SerialNumber;
        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        {
            //Fill Table
            var ListReturn = await Http.GetAsync("api/EmployeesLoansEntry/GetEmployeesLoansEntry");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            EmployeeEntryList = JsonConvert.DeserializeObject<List<EmployeesLoansEntryVM>>(vMs["data"].ToString());
            //Fill DropDown
            var ListReturndrp = await Http.GetAsync("api/EmployeesLoansEntry/GetListsOfDDl");
            var vMMs = JObject.Parse(ListReturndrp.Content.ReadAsStringAsync().Result);
            var ListDDL = JsonConvert.DeserializeObject<EmployeesLoansEntryVM>(vMMs["data"].ToString());
            TreasuryList = ListDDL.TreasuryList;
            EmployeeList = ListDDL.EmployeeList;
        }
        protected async Task GetAllByTreasury()
        {
            if (EmployeeEntry.TreasuryId != 0)
            {
                string uri = "/api/EmployeesLoansEntry/GetEmployeesLoansEntryByTres?TresID=" + EmployeeEntry.TreasuryId;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                EmployeeEntryList = JsonConvert.DeserializeObject<List<EmployeesLoansEntryVM>>(vMs["data"].ToString());
            }
            else
            {
                var ListReturn = await Http.GetAsync("api/EmployeesLoansEntry/GetEmployeesLoansEntry");
                var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
                EmployeeEntryList = JsonConvert.DeserializeObject<List<EmployeesLoansEntryVM>>(vMs["data"].ToString());
            }
            StateHasChanged();
        }

        protected async Task GetAllByEmployee()
        {

            if (EmployeeEntry.EmployeeId != 0)
            {
                string uri = "/api/EmployeesLoansEntry/GetEmployeesLoansEntryByEmployee?EmpID=" + EmployeeEntry.EmployeeId;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                EmployeeEntryList = JsonConvert.DeserializeObject<List<EmployeesLoansEntryVM>>(vMs["data"].ToString());
            }
            else
            {
                var ListReturn = await Http.GetAsync("api/EmployeesLoansEntry/GetEmployeesLoansEntry");
                var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
                EmployeeEntryList = JsonConvert.DeserializeObject<List<EmployeesLoansEntryVM>>(vMs["data"].ToString());
            }


            StateHasChanged();
        }
        protected async Task AddNewEmployeesLoansEntry()
        {
            //await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/EmployeeLoanEntryAdd", "جارى التحميل", 640);
            await jsRuntime.InvokeAsync<object>("open", "/EmployeeLoanEntryAdd", "_blank");
        }
        protected async Task EditOldEmployeesLoansEntry(int ID,string Type)
        {
            // await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/EmployeeLoanEntryEdit?ID=" + ID, "جارى التحميل", 640);
            //   await jsRuntime.InvokeAsync<object>("open", "/EmployeeLoanEntryEdit?ID=" + ID, "_blank");
            await jsRuntime.InvokeAsync<object>("open", "/EmployeeLoanEntryEdit?ID=" + ID + "&Type=" + Type, "_blank");
        }
        protected void DeleteEmployeeEntry(int Id, int serialNumber, int Employeeid)
        {
            EmployeeEntryID = Id;
            EmployeeId = Employeeid;
            SerialNumber = serialNumber;
            ShowDelete = true;
        }
        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                string uri = "/api/EmployeesLoansEntry/DeleteEmployeesLoansEntry?ID=" + EmployeeEntryID + "&SerialNumber=" + SerialNumber + "&EmployeeId=" + EmployeeId;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
            ShowDelete = false;

        }
    }
}