﻿using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.FundTransaction;
using MatBlazor;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
namespace MasProject.Client.Pages.FundTransaction.EmployeesLoanEntry
{
    public partial class EmployeeLoanEntryEdit
    {
        private PageTitle PageTitle;
        EmployeesLoansEntryVM EmployeeEntry = new EmployeesLoansEntryVM();
        List<CurrencyVM> CurrencyList = new List<CurrencyVM>();
        List<TreasuryVM> TreasuryList = new List<TreasuryVM>();
        List<EmployeeVM> EmployeeList = new List<EmployeeVM>();
        bool ShowConfirm = false;
        bool IsDisabled = false;
        bool SelectEmployeeChecker = false;
        protected override async Task OnInitializedAsync()
        {

            await BindList();
            var querystring = ExtensionMethods.QueryString(nav);
            var id = querystring["ID"];
            await EditOldEmployeesLoansEntry(int.Parse(id));
            var Type = querystring["Type"];
            if (Type == "View")
            {
                //disable controls
                IsDisabled = true;
                PageTitle.Title = "عرض    سلف الموظفين";
            }
            else
            {
                IsDisabled = false;
                PageTitle.Title = "تعديل   سلف الموظفين ";
            }

        }
        async Task BindList()
        {
            //Fill DropDown
            var ListReturndrp = await Http.GetAsync("api/EmployeesLoansEntry/GetListsOfDDl");
            var vMMs = JObject.Parse(ListReturndrp.Content.ReadAsStringAsync().Result);
            var ListDDL = JsonConvert.DeserializeObject<EmployeesLoansEntryVM>(vMMs["data"].ToString());
            TreasuryList = ListDDL.TreasuryList;
            EmployeeList = ListDDL.EmployeeList;
        }
        //protected async Task GetCurrencyRatio()
        //{
        //    string uri = "/api/EmployeeEntry/GetCurrencyRatio?Id=" + EmployeeEntry.CurrencyId;
        //    HttpResponseMessage response = await Http.GetAsync(uri);
        //    var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
        //    EmployeeEntry.Ratio  = JsonConvert.DeserializeObject<double>(vMs["data"].ToString());
        //    if (EmployeeEntry.Amount != null && EmployeeEntry.Amount > 0 && EmployeeEntry.Ratio > 0)
        //    {
        //        EmployeeEntry.Total = EmployeeEntry.Amount.Value * EmployeeEntry.Ratio;
        //    }
        //    StateHasChanged();
        //}
        //protected void GetTotal()
        //{
        //    var total = 0.0;
        //    if (EmployeeEntry.Amount != null && EmployeeEntry.Amount > 0 && EmployeeEntry.Ratio > 0)
        //    {
        //        total = EmployeeEntry.Amount.Value * EmployeeEntry.Ratio;
        //    }
        //    EmployeeEntry.Total = total;
        //    StateHasChanged();
        //}
        protected async Task Confirm()
        {
            await nav.NavigateToExitWindowAsync(jsRuntime);
        }
        protected async Task EditOldEmployeesLoansEntry(int ID)
        {
            string uri = "/api/EmployeesLoansEntry/GetSpecificEmployeesLoansEntry?Id=" + ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            EmployeeEntry = JsonConvert.DeserializeObject<EmployeesLoansEntryVM>(vMs["data"].ToString());
        }
        protected async Task EditEmployeesLoansEntry()
        {
            if (EmployeeEntry.EmployeeId != 0 && EmployeeEntry.EmployeeId != null)
            {
                var EditModel = JsonConvert.SerializeObject(EmployeeEntry);
                var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/EmployeesLoansEntry/EditEmployeesLoansEntry", content);
                if (result.IsSuccessStatusCode)
                {
                    ShowConfirm = true;
                }
            }
            else
            {
                SelectEmployeeChecker = true;
            }
        }
    }
}