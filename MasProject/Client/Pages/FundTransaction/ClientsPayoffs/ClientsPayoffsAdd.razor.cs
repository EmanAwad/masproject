﻿using MasProject.Client.Shared;
using MasProject.Domain.ViewModel.FundTransaction;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Persons;
using Microsoft.AspNetCore.Components;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using static System.Net.WebRequestMethods;

namespace MasProject.Client.Pages.FundTransaction.ClientsPayoffs
{
    public partial class ClientsPayoffsAdd
    {
        private PageTitle PageTitle;
        ClientsPayoffsVM Client = new ClientsPayoffsVM();
        ClientsVM clients = new ClientsVM();
        List<CurrencyVM> CurrencyList = new List<CurrencyVM>();
        List<TreasuryVM> TreasuryList = new List<TreasuryVM>();
        List<ClientsVM> ClientList = new List<ClientsVM>();
        bool ShowConfirm = false;
        bool SelectClientChecker = false;
        bool IsDisabled = false;

        //IEnumerable<CurrencyVM> CurrencyList;
        //IEnumerable<TreasuryVM> TreasuryList;
        //IEnumerable<ClientsVM> ClientList;

        //List<ClientsVM> ClientsList = new List<ClientsVM>();

        protected override async Task OnInitializedAsync()
        {
            Client = new ClientsPayoffsVM();
            await BindList();
        }
        async Task BindList()
        {

            var ListReturndrp = await Http.GetAsync("api/ClientsPayoffs/GetListsOfDDl");
            var vMMs = JObject.Parse(ListReturndrp.Content.ReadAsStringAsync().Result);
            Client = JsonConvert.DeserializeObject<ClientsPayoffsVM>(vMMs["data"].ToString());
            CurrencyList = Client.CurrencyList;
            TreasuryList = Client.TreasuryList;
            ClientList = Client.ClientList;
        }

        protected async Task GetCurrencyRatio()
        {
            string uri = "/api/ClientsPayoffs/GetCurrencyRatio?Id=" + Client.CurrencyId;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            Client.Ratio = JsonConvert.DeserializeObject<double>(vMs["data"].ToString());
            if (Client.Amount != null && Client.Amount > 0 && Client.Ratio > 0)
            {
                Client.Total = Client.Amount.Value * Client.Ratio;
            }
            StateHasChanged();
        }

        protected void GetTotal()
        {
            var total = 0.0;
            if (Client.Amount != null && Client.Amount > 0 && Client.Ratio > 0)
            {
                total = Client.Amount.Value * Client.Ratio;
            }
            Client.Total = total;
            StateHasChanged();
        }


        protected async Task AddClientsPayoffs()
        {
            IsDisabled = true;
            Client.ClientId = ClientList.Find(x => x.Name == clients.Name).ID;
            if (Client.ClientId != 0 && Client.ClientId != null)
            {
                var AddModel = JsonConvert.SerializeObject(Client);
                var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/ClientsPayoffs/AddClientsPayoffs", content);
                if (result.IsSuccessStatusCode)
                {
                    ShowConfirm = true;
                }
            }
            else
            {
                SelectClientChecker = true;
            }
        }
        protected async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        }

        async Task ReloadClientsList()
        {
            clients.Name = String.Empty;

            var ListReturn = await Http.GetAsync("api/Client/GetClients");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ClientList = JsonConvert.DeserializeObject<List<ClientsVM>>(vMs["data"].ToString());

        }
    }
}