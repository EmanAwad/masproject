﻿using MasProject.Client.Shared;
using MasProject.Domain.ViewModel.FundTransaction;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Persons;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using static System.Net.WebRequestMethods;

namespace MasProject.Client.Pages.FundTransaction.ClientsPayoffs
{
    public partial class ClientsPayoffsEdit
    {
        private PageTitle PageTitle;
        ClientsPayoffsVM Client = new ClientsPayoffsVM();
        List<CurrencyVM> CurrencyList = new List<CurrencyVM>();
        List<TreasuryVM> TreasuryList = new List<TreasuryVM>();
        List<ClientsVM> ClientList = new List<ClientsVM>();
        ClientsVM clients = new ClientsVM();
        bool ShowConfirm = false;
        bool IsDisabled = false;
        bool SelectClientChecker = false;
        List<ClientsVM> ClientsList = new List<ClientsVM>();
        protected override async Task OnInitializedAsync()
        {

            await BindList();
            var querystring = ExtensionMethods.QueryString(nav);
            var id = querystring["ID"];
            await EditOldClientsPayoffs(int.Parse(id));
            var Type = querystring["Type"];
            if (Type == "View")
            {
                //disable controls
                IsDisabled = true;
                PageTitle.Title = "عرض  مردودات العميل";
            }
            else
            {
                IsDisabled = false;
                PageTitle.Title = "تعديل  مردودات العميل";
            }
        }

        async Task BindList()
        {

            var ListReturndrp = await Http.GetAsync("api/ClientsPayoffs/GetListsOfDDl");
            var vMMs = JObject.Parse(ListReturndrp.Content.ReadAsStringAsync().Result);
            var ListDDL = JsonConvert.DeserializeObject<ClientsPayoffsVM>(vMMs["data"].ToString());
            CurrencyList = ListDDL.CurrencyList;
            TreasuryList = ListDDL.TreasuryList;
            ClientList = ListDDL.ClientList;
        }

        protected async Task GetCurrencyRatio()
        {
            string uri = "/api/ClientsPayoffs/GetCurrencyRatio?Id=" + Client.CurrencyId;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            Client.Ratio = JsonConvert.DeserializeObject<double>(vMs["data"].ToString());
            if (Client.Amount != null && Client.Amount > 0 && Client.Ratio > 0)
            {
                Client.Total = Client.Amount.Value * Client.Ratio;
            }
            StateHasChanged();
        }

        protected void GetTotal()
        {
            var total = 0.0;
            if (Client.Amount != null && Client.Amount > 0 && Client.Ratio > 0)
            {
                total = Client.Amount.Value * Client.Ratio;
            }
            Client.Total = total;
            StateHasChanged();
        }


        protected async Task Confirm()
        {
            await nav.NavigateToExitWindowAsync(jsRuntime);
        }

        protected async Task EditOldClientsPayoffs(int ID)
        {
            string uri = "/api/ClientsPayoffs/GetSpecificClientsPayoffs?Id=" + ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            Client = JsonConvert.DeserializeObject<ClientsPayoffsVM>(vMs["data"].ToString());
            clients.Name = Client.ClientName;

        }

        protected async Task EditClientsPayoffs()
        {
            Client.ClientId = ClientList.Find(x => x.Name == clients.Name).ID;
            if (Client.ClientId != 0 && Client.ClientId != null)
            {
                var EditModel = JsonConvert.SerializeObject(Client);
                var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/ClientsPayoffs/EditClientsPayoffs", content);
                if (result.IsSuccessStatusCode)
                {
                    ShowConfirm = true;
                }
            }
            else
            {
                SelectClientChecker = true;
            }
        }

        async Task ReloadClientsList()
        {
            clients.Name = String.Empty;

            var ListReturn = await Http.GetAsync("api/Client/GetClients");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ClientList = JsonConvert.DeserializeObject<List<ClientsVM>>(vMs["data"].ToString());

        }
    }
}