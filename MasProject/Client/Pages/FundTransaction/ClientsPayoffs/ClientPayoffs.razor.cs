﻿using MasProject.Client.Shared;
using MasProject.Domain.ViewModel.FundTransaction;
using MasProject.Domain.ViewModel.FundTransaction.ClientPayments;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Persons;
using Microsoft.JSInterop;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace MasProject.Client.Pages.FundTransaction.ClientsPayoffs
{
    public partial class ClientPayoffs
    {
        private PageTitle PageTitle;
        List<ClientsPayoffsVM> ClientsPayoffsList;
        ClientsPayoffsVM Client = new ClientsPayoffsVM();
        bool ShowDelete = false;
        int ClientsPayoffsID;
        int ClientId;
        int SerialNumber;
        List<ClientsVM> ClientList = new List<ClientsVM>();
        ClientsVM clients = new ClientsVM();
        List<TreasuryVM> TreasuryList = new List<TreasuryVM>();

        ClientsPayoffsVM ClientPayoffsModel = new ClientsPayoffsVM();

        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        {
            //Fill Table
            var ListReturn = await Http.GetAsync("api/ClientsPayoffs/GetClientsPayoffs");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ClientsPayoffsList = JsonConvert.DeserializeObject<List<ClientsPayoffsVM>>(vMs["data"].ToString());

            //Fill Drops 

            var ListReturndrp = await Http.GetAsync("api/ClientsPayoffs/GetListsOfDDl");
            var vMMs = JObject.Parse(ListReturndrp.Content.ReadAsStringAsync().Result);
            var ListDDL = JsonConvert.DeserializeObject<ClientsPayoffsVM>(vMMs["data"].ToString());

            TreasuryList = ListDDL.TreasuryList;
            ClientList = ListDDL.ClientList;
        }

        protected async Task AddNewClientsPayoffs()
        {
            //  await NavigationManager.NavigateToNewWindowAsync(JSRuntime, "/ClientsPayoffsAdd", "جارى التحميل", 640);
            await jsRuntime.InvokeAsync<object>("open", "/ClientsPayoffsAdd", "_blank");
        }
        protected async Task EditOldClientsPayoffs(int ID, string Type)
        {
            //await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/ClientsPayoffsEdit?ID=" + ID, "جارى التحميل", 640);
            await jsRuntime.InvokeAsync<object>("open", "/ClientsPayoffsEdit?ID=" + ID + "&Type=" + Type, "_blank");
        }

        protected void DeleteClientsPayoffs(int Id, int serialNumber, int Clientid)
        {
            ClientsPayoffsID = Id;
            ClientId = Clientid;
            SerialNumber = serialNumber;
            ShowDelete = true;
        }


        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                string uri = "/api/ClientsPayoffs/DeleteClientsPayoffs?ID=" + ClientsPayoffsID + "&SerialNumber=" + SerialNumber + "&ClientId=" + ClientId;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
            ShowDelete = false;
        }
        async Task ReloadClientsList()
        {
            clients.Name = String.Empty;

            var ListReturn = await Http.GetAsync("api/Client/GetClients");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ClientList = JsonConvert.DeserializeObject<List<ClientsVM>>(vMs["data"].ToString());

        }

        protected async Task GetAllByClient()
        {
            ClientPayoffsModel.ClientId = ClientList.Find(x => x.Name == clients.Name).ID;
            if (ClientPayoffsModel.ClientId != 0)
            {
                string uri = "/api/ClientsPayoffs/GetAllByClient?ClientId=" + ClientPayoffsModel.ClientId;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                ClientsPayoffsList = JsonConvert.DeserializeObject<List<ClientsPayoffsVM>>(vMs["data"].ToString());
            }
            else
            {
                var ListReturn = await Http.GetAsync("api/ClientsPayoffs/GetClientsPayoffs");
                var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
                ClientsPayoffsList = JsonConvert.DeserializeObject<List<ClientsPayoffsVM>>(vMs["data"].ToString());
            }

            StateHasChanged();
            //SalesModel.ClientId = ClientList.Find(x => x.Name == clients.Name).ID;
            //if (SalesModel.ClientId != 0)
            //{
            //    SalesList = SalesList.Where(x => x.ClientId == SalesModel.ClientId).ToList();
            //}
            //else
            //{
            //    SalesList = TempSalesList;
            //}
        }
        //protected async Task GetAllByBranch()
        //{
        //    //if (SalesModel.BranchId != 0)
        //    //{
        //    //    SalesList = SalesList.Where(x => x.BranchId == SalesModel.BranchId).ToList();
        //    //}
        //    //else
        //    //{
        //    //    SalesList = TempSalesList;
        //    //}
        //    //SalesList = SalesList.Where(x => x.BranchId == SalesModel.BranchId).ToList();
        //    string SalesBranch = "/api/SalesBills/GetSalesByBranch?BranchId=" + SalesModel.BranchId;
        //    HttpResponseMessage SalesBranchReturn = await Http.GetAsync(SalesBranch);
        //    var SalesBranchVMs = JObject.Parse(SalesBranchReturn.Content.ReadAsStringAsync().Result);
        //    SalesList = JsonConvert.DeserializeObject<List<SalesBillsVM>>(SalesBranchVMs["data"].ToString());
        //}

        protected async Task GetAllByTreasury()
        {

            if (ClientPayoffsModel.TreasuryId != 0)
            {
                string uri = "/api/ClientsPayoffs/GetAllByTreasury?TreasuryId=" + ClientPayoffsModel.TreasuryId;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                ClientsPayoffsList = JsonConvert.DeserializeObject<List<ClientsPayoffsVM>>(vMs["data"].ToString());
            }
            else
            {
                var ListReturn = await Http.GetAsync("api/ClientsPayoffs/GetClientsPayoffs");
                var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
                ClientsPayoffsList = JsonConvert.DeserializeObject<List<ClientsPayoffsVM>>(vMs["data"].ToString());
            }


            StateHasChanged();
        }

        protected async Task GetAllByDate()
        {
            if (ClientPayoffsModel.Date != null)
            {
                string uri = "/api/ClientsPayoffs/GetAllByDate?date=" + ClientPayoffsModel.Date;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                ClientsPayoffsList = JsonConvert.DeserializeObject<List<ClientsPayoffsVM>>(vMs["data"].ToString());
            }
            else
            {
                var ListReturn = await Http.GetAsync("api/ClientsPayoffs/GetClientsPayoffs");
                var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
                ClientsPayoffsList = JsonConvert.DeserializeObject<List<ClientsPayoffsVM>>(vMs["data"].ToString());
            }


            StateHasChanged();

        }


    }
}
