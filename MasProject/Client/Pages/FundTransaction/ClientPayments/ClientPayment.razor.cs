﻿using MasProject.Domain.ViewModel.FundTransaction.ClientPayments;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using Microsoft.JSInterop;
using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Hierarchy;
using System.Net.Http;
using System;

namespace MasProject.Client.Pages.FundTransaction.ClientPayments
{
    public partial class ClientPayment { 
    
        private PageTitle PageTitle;
        List<ClientPaymentVM> ClientPaymentList;
        ClientPaymentVM Client = new ClientPaymentVM();
        bool ShowDelete = false;
        int ClientPaymentID;
        //int ClientId;
        //int SerialNumber;
        List<ClientsVM> ClientList = new List<ClientsVM>();
        ClientsVM clients = new ClientsVM();
        List<TreasuryVM> TreasuryList = new List<TreasuryVM>();

        ClientPaymentVM ClientPaymentModel = new ClientPaymentVM();
        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        {
            //Fill Table
            var ListReturn = await Http.GetAsync("api/ClientPayment/GetClientPayment");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ClientPaymentList = JsonConvert.DeserializeObject<List<ClientPaymentVM>>(vMs["data"].ToString());
            //Fill Dropdowns
            // var SalesListReturn = await Http.GetAsync("api/ClientPayment/GetListsOfDDl");
            // var SalesvMs = JObject.Parse(SalesListReturn.Content.ReadAsStringAsync().Result);
            //ClientPaymentModel = JsonConvert.DeserializeObject<ClientPaymentVM>(SalesvMs["data"].ToString());
            // ClientsList = ClientPaymentModel.ClientList;
            // TreasuryList = ClientPaymentModel.TreasuryList;
            //Fill DropDown
            //Fill DropDown
            var ListReturndrp = await Http.GetAsync("api/ClientPayment/GetListsOfDDl");
            var vMMs = JObject.Parse(ListReturndrp.Content.ReadAsStringAsync().Result);
            var ListDDL = JsonConvert.DeserializeObject<ClientPaymentVM>(vMMs["data"].ToString());
          
            TreasuryList = ListDDL.TreasuryList;
            ClientList = ListDDL.ClientList;
        }
        protected async Task AddNewClientPayment()
        {
            //  await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/ClientPaymentAdd", "جارى التحميل", 640);
            await jsRuntime.InvokeAsync<object>("open", "/ClientPaymentAdd", "_blank");
        }
        protected async Task EditOldClientPayment(int ID, string Type)
        {
            // await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/ClientPaymentEdit?ID=" + ID, "جارى التحميل", 640);
            // await jsRuntime.InvokeAsync<object>("open", "/ClientPaymentEdit?ID=" + ID, "_blank");
            await jsRuntime.InvokeAsync<object>("open", "/ClientPaymentEdit?ID=" + ID + "&Type=" + Type, "_blank");

        }
        protected void DeleteClientPayment(int Id)//, int serialNumber, int Clientid)
        {
            ClientPaymentID = Id; 
            //ClientId = Clientid;
            //SerialNumber = serialNumber;
            ShowDelete = true;
        }
        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                string uri = "/api/ClientPayment/DeleteClientPayment?ID=" + ClientPaymentID;// + "&SerialNumber=" + SerialNumber + "&ClientId=" + ClientId;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
            ShowDelete = false;
        }
        async Task ReloadClientsList()
        {
            clients.Name = String.Empty;

            var ListReturn = await Http.GetAsync("api/Client/GetClients");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ClientList = JsonConvert.DeserializeObject<List<ClientsVM>>(vMs["data"].ToString());

        }

        protected async Task GetAllByClient()
        {
            ClientPaymentModel.ClientId = ClientList.Find(x => x.Name == clients.Name).ID;
            if (ClientPaymentModel.ClientId != 0)
            {
                string uri = "/api/ClientPayment/GetAllByClient?ClientId=" + ClientPaymentModel.ClientId;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                ClientPaymentList = JsonConvert.DeserializeObject<List<ClientPaymentVM>>(vMs["data"].ToString());
            }
            else
            {
                var ListReturn = await Http.GetAsync("api/ClientPayment/GetClientPayment");
                var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
                ClientPaymentList = JsonConvert.DeserializeObject<List<ClientPaymentVM>>(vMs["data"].ToString());
            }

            StateHasChanged();
            //SalesModel.ClientId = ClientList.Find(x => x.Name == clients.Name).ID;
            //if (SalesModel.ClientId != 0)
            //{
            //    SalesList = SalesList.Where(x => x.ClientId == SalesModel.ClientId).ToList();
            //}
            //else
            //{
            //    SalesList = TempSalesList;
            //}
        }
        //protected async Task GetAllByBranch()
        //{
        //    //if (SalesModel.BranchId != 0)
        //    //{
        //    //    SalesList = SalesList.Where(x => x.BranchId == SalesModel.BranchId).ToList();
        //    //}
        //    //else
        //    //{
        //    //    SalesList = TempSalesList;
        //    //}
        //    //SalesList = SalesList.Where(x => x.BranchId == SalesModel.BranchId).ToList();
        //    string SalesBranch = "/api/SalesBills/GetSalesByBranch?BranchId=" + SalesModel.BranchId;
        //    HttpResponseMessage SalesBranchReturn = await Http.GetAsync(SalesBranch);
        //    var SalesBranchVMs = JObject.Parse(SalesBranchReturn.Content.ReadAsStringAsync().Result);
        //    SalesList = JsonConvert.DeserializeObject<List<SalesBillsVM>>(SalesBranchVMs["data"].ToString());
        //}

        protected async Task GetAllByTreasury()
        {

            if (ClientPaymentModel.TreasuryId != 0)
            {
                string uri = "/api/ClientPayment/GetAllByTreasury?TreasuryId=" + ClientPaymentModel.TreasuryId;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                ClientPaymentList = JsonConvert.DeserializeObject<List<ClientPaymentVM>>(vMs["data"].ToString());
            }
            else
            {
                var ListReturn = await Http.GetAsync("api/ClientPayment/GetClientPayment");
                var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
                ClientPaymentList = JsonConvert.DeserializeObject<List<ClientPaymentVM>>(vMs["data"].ToString());
            }


            StateHasChanged();
        }

        protected async Task GetAllByDate()
        {
            if (ClientPaymentModel.Date != null)
            {
                string uri = "/api/ClientPayment/GetAllByDate?date=" + ClientPaymentModel.Date;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                ClientPaymentList = JsonConvert.DeserializeObject<List<ClientPaymentVM>>(vMs["data"].ToString());
            }
            else
            {
                var ListReturn = await Http.GetAsync("api/ClientPayment/GetClientPayment");
                var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
                ClientPaymentList = JsonConvert.DeserializeObject<List<ClientPaymentVM>>(vMs["data"].ToString());
            }


            StateHasChanged();

        }
    }
}