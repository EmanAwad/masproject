﻿using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.FundTransaction.ClientPayments;
using MatBlazor;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System;
using System.IO;
using BlazorInputFile;
using System.Linq;
using MasProject.Client.Shared;
namespace MasProject.Client.Pages.FundTransaction.ClientPayments
{
    public partial class ClientPaymentAdd
    {
        private PageTitle PageTitle;
        ClientPaymentVM Client = new ClientPaymentVM();
        List<CurrencyVM> CurrencyList = new List<CurrencyVM>();
        List<TreasuryVM> TreasuryList = new List<TreasuryVM>();
        List<ClientsVM> ClientList = new List<ClientsVM>();
        ClientsVM clients = new ClientsVM();
        bool ShowConfirm = false;
        bool SelectClientChecker = false;
        bool SelectTreasuryChecker = false;
        bool IsDisabled = false;
        protected override async Task OnInitializedAsync()
        {

            await BindList();
            Client = new ClientPaymentVM();
            Client.Date = DateTime.Now.Date;
            Client.CurrencyId = 2;
            Client.Ratio = 1;
            var querystring = ExtensionMethods.QueryString(navigationManager);
            //edits in 25-01-2021 //beacause of names with extra space in thier string
            // if (querystring["ClientName"] != null && querystring["SerialNumber"] != null && querystring["Total"] != null)
            if (querystring["ClientId"] != null && querystring["SerialNumber"] != null && querystring["Total"] != null)
            {
                // Client.Name = querystring["ClientName"].Replace("%","").Trim();
                //clients.Name = querystring["ClientName"].Replace("%", "").Trim();
                clients.Name = ClientList.Where(x => x.ID == int.Parse(querystring["ClientId"])).FirstOrDefault().Name;
                Client.BillSerialNumber = int.Parse(querystring["SerialNumber"]);
                Client.Amount = double.Parse(querystring["Total"]);
            }
            if (querystring["Branch"] != null)
            {
                string uri = "/api/Treasury/GetTreasuryByBranchId?branchID=" + querystring["Branch"];
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                Client.TreasuryId = JsonConvert.DeserializeObject<int>(vMs["data"].ToString());

            }
            if (Client.TreasuryId != null && Client.TreasuryId != 0)
            {
                string serailuri = "/api/ClientPayment/GenerateSerial?TreasuryId=" + Client.TreasuryId;
                HttpResponseMessage IdReturn = await Http.GetAsync(serailuri);
                //var IdReturn = await Http.GetAsync("api/ClientPayment/GenerateSerial");
                var IdvMs = JObject.Parse(IdReturn.Content.ReadAsStringAsync().Result);
                Client.SerialNumber = JsonConvert.DeserializeObject<int>(IdvMs["data"].ToString());
            }
        }
        async Task GetSerialNumber()
        {
            string serailuri = "/api/ClientPayment/GenerateSerial?TreasuryId=" + Client.TreasuryId;
            HttpResponseMessage serialReturn = await Http.GetAsync(serailuri);
            var serialvms = JObject.Parse(serialReturn.Content.ReadAsStringAsync().Result);
            Client.SerialNumber = JsonConvert.DeserializeObject<int>(serialvms["data"].ToString());
            StateHasChanged();

        }
        async Task BindList()
        {
            //Fill DropDown
            var ListReturndrp = await Http.GetAsync("api/ClientPayment/GetListsOfDDl");
            var vMMs = JObject.Parse(ListReturndrp.Content.ReadAsStringAsync().Result);
            var ListDDL = JsonConvert.DeserializeObject<ClientPaymentVM>(vMMs["data"].ToString());
            CurrencyList = ListDDL.CurrencyList;
            TreasuryList = ListDDL.TreasuryList;
            ClientList = ListDDL.ClientList;
        }
        protected async Task GetCurrencyRatio()
        {
            string uri = "/api/ClientPayment/GetCurrencyRatio?Id=" + Client.CurrencyId;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            Client.Ratio = JsonConvert.DeserializeObject<double>(vMs["data"].ToString());
            if (Client.Amount != null && Client.Amount > 0 && Client.Ratio > 0)
            {
                Client.Total = Client.Amount.Value * Client.Ratio;
            }
            StateHasChanged();
        }
        protected void GetTotal()
        {
            var total = 0.0;
            if (Client.Amount != null && Client.Amount > 0 && Client.Ratio > 0)
            {
                total = Client.Amount.Value * Client.Ratio;
            }
            Client.Total = total;
            StateHasChanged();
        }
        protected async Task AddClientPayment()
        {
            
            if (Client.TreasuryId != 0)
            {
                var TempObj = ClientList.Find(x => x.Name == clients.Name);//.ID;
                if(TempObj!=null)
                {
                    Client.ClientId = TempObj.ID;
                }
                else
                {
                    Client.ClientId = 0;
                }
                if (Client.ClientId != 0 && Client.ClientId != null)
                {
                    IsDisabled = true;
                    var AddModel = JsonConvert.SerializeObject(Client);
                    var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                    var result = await Http.PostAsync("api/ClientPayment/AddClientPayment", content);
                    if (result.IsSuccessStatusCode)
                    {
                        ShowConfirm = true;
                    }
                }
                else
                {
                    SelectClientChecker = true;
                }
            }
            else
            {
                SelectTreasuryChecker = true;
            }
        }
        protected async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        }

       
        async Task ReloadClientsList()
        {
            clients.Name = String.Empty;

            var ListReturn = await Http.GetAsync("api/Client/GetClients");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ClientList = JsonConvert.DeserializeObject<List<ClientsVM>>(vMs["data"].ToString());

        }
    }
}