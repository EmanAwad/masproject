﻿using MasProject.Client.Shared;
using MasProject.Domain.ViewModel.FundTransaction.SupplierPayments;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Persons;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MasProject.Client.Pages.FundTransaction.SupplierPayments
{
    public partial class SupplierPaymentAdd
    {
        private PageTitle PageTitle;
        SupplierPaymentVM Supplier = new SupplierPaymentVM();
        List<CurrencyVM> CurrencyList = new List<CurrencyVM>();
        List<TreasuryVM> TreasuryList = new List<TreasuryVM>();
        List<SupplierVM> SupplierList = new List<SupplierVM>();

        bool ShowConfirm = false;
        bool SelectSupplierChecker = false;
        bool IsDisabled = false;

        protected override async Task OnInitializedAsync()
        {

            Supplier = new SupplierPaymentVM();
            Supplier.Date = DateTime.Now.Date;
            Supplier.CurrencyId = 2;
            Supplier.Ratio = 1;
            await BindList();
            var IdReturn = await Http.GetAsync("api/SupplierPayment/GenerateSerial");
            var IdvMs = JObject.Parse(IdReturn.Content.ReadAsStringAsync().Result);
            Supplier.SerialNumber = JsonConvert.DeserializeObject<int>(IdvMs["data"].ToString());
        }


        async Task BindList()
        {
            //Fill DropDown
            var ListReturndrp = await Http.GetAsync("api/SupplierPayment/GetListsOfDDl");
            var vMMs = JObject.Parse(ListReturndrp.Content.ReadAsStringAsync().Result);
            var ListDDL = JsonConvert.DeserializeObject<SupplierPaymentVM>(vMMs["data"].ToString());
            CurrencyList = ListDDL.CurrencyList;
            TreasuryList = ListDDL.TreasuryList;
            SupplierList = ListDDL.SupplierList;
        }


        protected async Task GetCurrencyRatio()
        {
            string uri = "/api/SupplierPayment/GetCurrencyRatio?Id=" + Supplier.CurrencyId;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            Supplier.Ratio = JsonConvert.DeserializeObject<double>(vMs["data"].ToString());
            if (Supplier.Amount != null && Supplier.Amount > 0 && Supplier.Ratio > 0)
            {
                Supplier.Total = Supplier.Amount.Value * Supplier.Ratio;
            }
            StateHasChanged();
        }


        protected void GetTotal()
        {
            var total = 0.0;
            if (Supplier.Amount != null && Supplier.Amount > 0 && Supplier.Ratio > 0)
            {
                total = Supplier.Amount.Value * Supplier.Ratio;
            }
            Supplier.Total = total;
            StateHasChanged();
        }


        protected async Task AddSupplierPayment()
        {
            IsDisabled = true;
            if (Supplier.SupplierId != 0 && Supplier.SupplierId != null)
            {
                var AddModel = JsonConvert.SerializeObject(Supplier);
                var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/SupplierPayment/AddSupplierPayment", content);
                if (result.IsSuccessStatusCode)
                {
                    ShowConfirm = true;
                }
            }
            else
            {
                SelectSupplierChecker = true;
            }
        }
        protected async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        }

    }
}
