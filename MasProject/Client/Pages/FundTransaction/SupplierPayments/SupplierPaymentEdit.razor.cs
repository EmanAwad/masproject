﻿using MasProject.Client.Shared;
using MasProject.Domain.ViewModel.FundTransaction.ClientPayments;
using MasProject.Domain.ViewModel.FundTransaction.SupplierPayments;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Persons;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MasProject.Client.Pages.FundTransaction.SupplierPayments
{
    public partial class SupplierPaymentEdit
    {
        private PageTitle PageTitle;
        SupplierPaymentVM Supplier = new SupplierPaymentVM();
        List<CurrencyVM> CurrencyList = new List<CurrencyVM>();
        List<TreasuryVM> TreasuryList = new List<TreasuryVM>();
        List<SupplierVM> SupplierList = new List<SupplierVM>();
        bool ShowConfirm = false;
        bool IsDisabled = false;
        bool SelectSupplierChecker = false;
        protected override async Task OnInitializedAsync()
        {

            await BindList();
            var querystring = ExtensionMethods.QueryString(nav);
            var id = querystring["ID"];
            await EditOldSupplierPayment(int.Parse(id));
            var Type = querystring["Type"];
            if (Type == "View")
            {
                //disable controls
                IsDisabled = true;
                PageTitle.Title = "عرض  دفعات المورد";
            }
            else
            {
                IsDisabled = false;
                PageTitle.Title = "تعديل  دفعات المورد";
            }

        }

        async Task BindList()
        {
            //Fill DropDown
            var ListReturndrp = await Http.GetAsync("api/SupplierPayment/GetListsOfDDl");
            var vMMs = JObject.Parse(ListReturndrp.Content.ReadAsStringAsync().Result);
            var ListDDL = JsonConvert.DeserializeObject<SupplierPaymentVM>(vMMs["data"].ToString());
            CurrencyList = ListDDL.CurrencyList;
            TreasuryList = ListDDL.TreasuryList;
            SupplierList = ListDDL.SupplierList;
        }
        protected async Task GetCurrencyRatio()
        {
            string uri = "/api/SupplierPayment/GetCurrencyRatio?Id=" + Supplier.CurrencyId;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            Supplier.Ratio = JsonConvert.DeserializeObject<double>(vMs["data"].ToString());
            if (Supplier.Amount != null && Supplier.Amount > 0 && Supplier.Ratio > 0)
            {
                Supplier.Total = Supplier.Amount.Value * Supplier.Ratio;
            }
            StateHasChanged();
        }

        protected void GetTotal()
        {
            var total = 0.0;
            if (Supplier.Amount != null && Supplier.Amount > 0 && Supplier.Ratio > 0)
            {
                total = Supplier.Amount.Value * Supplier.Ratio;
            }
            Supplier.Total = total;
            StateHasChanged();
        }

        protected async Task Confirm()
        {
            await nav.NavigateToExitWindowAsync(jsRuntime);
        }

        protected async Task EditOldSupplierPayment(int ID)
        {
            string uri = "/api/SupplierPayment/GetSpecificSupplierPayment?Id=" + ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            Supplier = JsonConvert.DeserializeObject<SupplierPaymentVM>(vMs["data"].ToString());
        }

        protected async Task EditSupplierPayment()
        {
            if (Supplier.SupplierId != 0 && Supplier.SupplierId != null)
            {
                var EditModel = JsonConvert.SerializeObject(Supplier);
                var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/SupplierPayment/EditSupplierPayment", content);
                if (result.IsSuccessStatusCode)
                {
                    ShowConfirm = true;
                }
            }
            else
            {
                SelectSupplierChecker = true;
            }
        }
    }
}