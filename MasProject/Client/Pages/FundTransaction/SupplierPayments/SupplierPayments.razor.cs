﻿using MasProject.Client.Shared;
using MasProject.Domain.ViewModel.FundTransaction.SupplierPayments;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.JSInterop;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Persons;
using System.Net.Http;

namespace MasProject.Client.Pages.FundTransaction.SupplierPayments
{
    public partial class SupplierPayments
    {
        private PageTitle PageTitle;
        List<SupplierPaymentVM> SupplierPaymentList;
        
        SupplierPaymentVM Supplier = new SupplierPaymentVM();
       
        List<TreasuryVM> TreasuryList = new List<TreasuryVM>();
        List<SupplierVM> SupplierList = new List<SupplierVM>();

        bool ShowDelete = false;
        int SupplierPaymentID;
        int SupplierId;
        int SerialNumber;
        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }

        async Task BindList()
        {
            //Fill Table
            var ListReturn = await Http.GetAsync("api/SupplierPayment/GetSupplierPayment");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            SupplierPaymentList = JsonConvert.DeserializeObject<List<SupplierPaymentVM>>(vMs["data"].ToString());
            //Fill DropDown
            var ListReturndrp = await Http.GetAsync("api/SupplierPayment/GetListsOfDDl");
            var vMMs = JObject.Parse(ListReturndrp.Content.ReadAsStringAsync().Result);
            var ListDDL = JsonConvert.DeserializeObject<SupplierPaymentVM>(vMMs["data"].ToString());
           
            TreasuryList = ListDDL.TreasuryList;
            SupplierList = ListDDL.SupplierList;
        }
        protected async Task GetAllByTreasury()
        {

            if (Supplier.TreasuryId != 0)
            {
                string uri = "/api/SupplierPayment/GetSupplierPaymentByTres?TreasuryId=" + Supplier.TreasuryId;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                SupplierPaymentList = JsonConvert.DeserializeObject<List<SupplierPaymentVM>>(vMs["data"].ToString());
            }
            else
            {
                var ListReturn = await Http.GetAsync("api/SupplierPayment/GetSupplierPayment");
                var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
                SupplierPaymentList = JsonConvert.DeserializeObject<List<SupplierPaymentVM>>(vMs["data"].ToString());
            }


            StateHasChanged();
        }
        protected async Task GetAllBySupplier()
        {

            if (Supplier.SupplierId != 0)
            {
                string uri = "/api/SupplierPayment/GetSupplierPaymentBySupplier?SuppID=" + Supplier.SupplierId;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                SupplierPaymentList = JsonConvert.DeserializeObject<List<SupplierPaymentVM>>(vMs["data"].ToString());
            }
            else
            {
                var ListReturn = await Http.GetAsync("api/SupplierPayment/GetSupplierPayment");
                var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
                SupplierPaymentList = JsonConvert.DeserializeObject<List<SupplierPaymentVM>>(vMs["data"].ToString());
            }


            StateHasChanged();
        }
        protected async Task AddNewSupplierPayment()
        {
            // await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/SupplierPaymentAdd", "جارى التحميل", 640);
            await jsRuntime.InvokeAsync<object>("open", "/SupplierPaymentAdd", "_blank");
        }


        protected async Task EditOldSupplierPayment(int ID,string Type)
        {
            // await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/SupplierPaymentEdit?ID=" + ID, "جارى التحميل", 640);
            //  await jsRuntime.InvokeAsync<object>("open", "/SupplierPaymentEdit?ID=" + ID, "_blank");
            await jsRuntime.InvokeAsync<object>("open", "/SupplierPaymentEdit?ID=" + ID + "&Type=" + Type, "_blank");

        }

        protected void DeleteSupplierPayment(int Id, int serialNumber, int Supplierid)
        {
            SupplierPaymentID = Id;
            SupplierId = Supplierid;
            SerialNumber = serialNumber;
            ShowDelete = true;
        }

        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                string uri = "/api/SupplierPayment/DeleteSupplierPayment?ID=" + SupplierPaymentID + "&SerialNumber=" + SerialNumber + "&SupplierId=" + SupplierId;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
            ShowDelete = false;
        }
    }
}
