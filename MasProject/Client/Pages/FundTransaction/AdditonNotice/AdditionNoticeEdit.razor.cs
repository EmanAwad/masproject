﻿using MasProject.Client.Shared;
using MasProject.Domain.ViewModel.FundTransaction.AdditionNotice;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Persons;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MasProject.Client.Pages.FundTransaction.AdditonNotice
{
    public partial class AdditionNoticeEdit
    {
        private PageTitle PageTitle;
        AdditionNoticeVM Addition = new AdditionNoticeVM();
        List<CurrencyVM> CurrencyList = new List<CurrencyVM>();
        List<SupplierVM> SupplierList = new List<SupplierVM>();
        List<ClientsVM> ClientList = new List<ClientsVM>();
        ClientsVM clients = new ClientsVM();
        bool ShowConfirm = false;
        int UserTypeFlag = 0;
        bool ShowClient = false;
        bool ShowSupplier = false;
        bool IsDisabled = false;
        string ValidateSupplierOrClient = "";
        protected override async Task OnInitializedAsync()
        {
            
            await BindList();
            var querystring = ExtensionMethods.QueryString(nav);
            var id = querystring["ID"];
            await EditOldAdditionNotice(int.Parse(id));
            var Type = querystring["Type"];
            if (Type == "View")
            {
                //disable controls
                IsDisabled = true;
                PageTitle.Title = "عرض  إشعار  الإضافة";
            }
            else
            {
                IsDisabled = false;
                PageTitle.Title = "تعديل  إشعار الإضافة";
            }
        }

        async Task BindList()
        {
            //Fill DropDown
            var ListReturndrp = await Http.GetAsync("api/AdditiontNotice/GetListsOfDDl");
            var vMMs = JObject.Parse(ListReturndrp.Content.ReadAsStringAsync().Result);
            var ListDDL = JsonConvert.DeserializeObject<AdditionNoticeVM>(vMMs["data"].ToString());
            CurrencyList = ListDDL.CurrencyList;
            ClientList = ListDDL.ClientList;
            SupplierList = ListDDL.SupplierList;
            
        }

        protected async Task Confirm()
        {
            await nav.NavigateToExitWindowAsync(jsRuntime);
        }

        protected async Task EditOldAdditionNotice(int ID)
        {
            string uri = "api/AdditiontNotice/GetSpecificAdditiontNotice?Id=" + ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            Addition = JsonConvert.DeserializeObject<AdditionNoticeVM>(vMs["data"].ToString());
            if (Addition.UserTypeFlag == 1)
            {
                UserTypeFlag = 1;
                ClientOrSupplier();
            }
            else if (Addition.UserTypeFlag == 2)
            {
                UserTypeFlag = 2;
                ClientOrSupplier();
            }
            clients.Name = Addition.ClientName;
        }

        protected async Task EditAdditionNotice()
        {
            Addition.ClientId = ClientList.Find(x => x.Name == clients.Name).ID;
            bool Checker = true;
            if (UserTypeFlag == 1)
            {
                if (Addition.ClientId == 0 || Addition.ClientId == null)
                {
                    Checker = false;
                }
            }
            else if (UserTypeFlag == 2)
            {
                if (Addition.SupplierId == 0 || Addition.SupplierId == null)
                {
                    Checker = false;
                }
            }
            else
            {
                Checker = false;
            }
            if (Checker)
            {
                ValidateSupplierOrClient = "";
                var EditModel = JsonConvert.SerializeObject(Addition);
                var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/AdditiontNotice/EditAdditiontNotice", content);
                if (result.IsSuccessStatusCode)
                {
                    ShowConfirm = true;
                }
            }
            else
            {

                ValidateSupplierOrClient = "يجب إدخال عميل أو مورد";
            }
        }

        protected void ClientOrSupplier()
        {
            if (UserTypeFlag == 1)
            {
                Addition.UserTypeFlag = 1;
                ShowClient = true;
                ShowSupplier = false;
            }
            else
            {
                Addition.UserTypeFlag = 2;
                ShowSupplier = true;
                ShowClient = false;
            }
        }

        async Task ReloadClientsList()
        {
            clients.Name = String.Empty;

            var ListReturn = await Http.GetAsync("api/Client/GetClients");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ClientList = JsonConvert.DeserializeObject<List<ClientsVM>>(vMs["data"].ToString());

        }
    }
}
