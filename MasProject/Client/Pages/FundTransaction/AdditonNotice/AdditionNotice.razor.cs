﻿using MasProject.Client.Shared;
using MasProject.Domain.ViewModel.FundTransaction.AdditionNotice;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.JSInterop;
using MasProject.Domain.ViewModel.Persons;
using System.Net.Http;

namespace MasProject.Client.Pages.FundTransaction.AdditonNotice
{
    public partial class AdditionNotice
    {
        private PageTitle PageTitle;
        List<AdditionNoticeVM> AdditionNoticeList;
        AdditionNoticeVM Discount = new AdditionNoticeVM();
        bool ShowDelete = false;
        int AdditionNoticeID;
        AdditionNoticeVM Addition = new AdditionNoticeVM();
      
        List<SupplierVM> SupplierList = new List<SupplierVM>();
        List<ClientsVM> ClientList = new List<ClientsVM>();
        ClientsVM clients = new ClientsVM();
        //int SupplierId;
        //int SerialNumber;
        protected override async Task OnInitializedAsync()
        {
            await BindList();
           
        }
        protected async Task GetAllByClient()
        {
            Addition.ClientId = ClientList.Find(x => x.Name == clients.Name).ID;
            if (Addition.ClientId != 0)
            {
                string uri = "/api/AdditiontNotice/GetAdditiontNoticeByClient?ClientId=" + Addition.ClientId;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                AdditionNoticeList = JsonConvert.DeserializeObject<List<AdditionNoticeVM>>(vMs["data"].ToString());
            }
            else
            {
                var ListReturn = await Http.GetAsync("api/AdditiontNotice/GetAdditiontNotic");
                var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
                AdditionNoticeList = JsonConvert.DeserializeObject<List<AdditionNoticeVM>>(vMs["data"].ToString());
            }

            StateHasChanged();
            
        }
        async Task ReloadClientsList()
        {
            clients.Name = String.Empty;

            var ListReturn = await Http.GetAsync("api/Client/GetClients");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ClientList = JsonConvert.DeserializeObject<List<ClientsVM>>(vMs["data"].ToString());

        }
        protected async Task GetAllBySupplier()
        {
          
            if (Addition.SupplierId != 0)
            {
                string uri = "/api/AdditiontNotice/GetAdditiontNoticeBySupplier?SuppID=" + Addition.SupplierId;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                AdditionNoticeList = JsonConvert.DeserializeObject<List<AdditionNoticeVM>>(vMs["data"].ToString());
            }
            else
            {
                var ListReturn = await Http.GetAsync("api/AdditiontNotice/GetAdditiontNotic");
                var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
                AdditionNoticeList = JsonConvert.DeserializeObject<List<AdditionNoticeVM>>(vMs["data"].ToString());
            }

            StateHasChanged();

        }

        async Task BindList()
        {
            //Fill Table
            var ListReturn = await Http.GetAsync("api/AdditiontNotice/GetAdditiontNotice");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            AdditionNoticeList = JsonConvert.DeserializeObject<List<AdditionNoticeVM>>(vMs["data"].ToString());
            //Fill DropDown
            var ListReturndrp = await Http.GetAsync("api/AdditiontNotice/GetListsOfDDl");
            var vMMs = JObject.Parse(ListReturndrp.Content.ReadAsStringAsync().Result);
            var ListDDL = JsonConvert.DeserializeObject<AdditionNoticeVM>(vMMs["data"].ToString());
       
            SupplierList = ListDDL.SupplierList;
            ClientList = ListDDL.ClientList;
        }

        protected async Task AddNewAdditionNotice()
        {
            //await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/AdditionNoticeAdd", "جارى التحميل", 635);
            await jsRuntime.InvokeAsync<object>("open", "/AdditionNoticeAdd", "_blank");
        }
        protected async Task EditOldAdditionNotice(int ID, string Type)
        {
            //  await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/AdditionNoticeEdit?ID=" + ID, "جارى التحميل", 635);
            //   await jsRuntime.InvokeAsync<object>("open", "/AdditionNoticeEdit?ID=" + ID, "_blank");
            await jsRuntime.InvokeAsync<object>("open", "/AdditionNoticeEdit?ID=" + ID + "&Type=" + Type, "_blank");
        }

        protected void DeleteAdditionNotice(int Id)//, int serialNumber, int Supplierid)
        {
            AdditionNoticeID = Id;
            //SupplierId = Supplierid;
            //SerialNumber = serialNumber;
            ShowDelete = true;
        }

        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                string uri = "api/AdditiontNotice/DeleteAdditiontNotice?ID=" + AdditionNoticeID;// + "&SerialNumber=" + SerialNumber + "&SupplierId=" + SupplierId;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
            ShowDelete = false;
        }
    }
}
