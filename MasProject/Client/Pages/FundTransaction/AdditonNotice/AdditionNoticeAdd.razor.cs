﻿using MasProject.Client.Shared;
using MasProject.Domain.ViewModel.FundTransaction.AdditionNotice;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Persons;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MasProject.Client.Pages.FundTransaction.AdditonNotice
{
    public partial class AdditionNoticeAdd
    {
        private PageTitle PageTitle;
        AdditionNoticeVM Addition = new AdditionNoticeVM();
        List<CurrencyVM> CurrencyList = new List<CurrencyVM>();
        List<TreasuryVM> TreasuryList = new List<TreasuryVM>();
        List<SupplierVM> SupplierList = new List<SupplierVM>();
        List<ClientsVM> ClientList = new List<ClientsVM>();
        ClientsVM clients = new ClientsVM();
        bool ShowConfirm = false;
        int UserTypeFlag = 0;
        bool ShowClient = false;
        bool ShowSupplier = false;
      
        string ValidateSupplierOrClient = "";
        bool IsDisabled = false;

        protected override async Task OnInitializedAsync()
        {
            Addition = new AdditionNoticeVM();
            var IdReturn = await Http.GetAsync("api/AdditiontNotice/GenerateSerial");
            var IdvMs = JObject.Parse(IdReturn.Content.ReadAsStringAsync().Result);
            Addition.SerialNumber = JsonConvert.DeserializeObject<int>(IdvMs["data"].ToString());
            Addition.Date = DateTime.Now.Date;
            Addition.CurrencyId = 2;
            await BindList();
            
           
        }


        async Task BindList()
        {
            //Fill DropDown
            var ListReturndrp = await Http.GetAsync("api/AdditiontNotice/GetListsOfDDl");
            var vMMs = JObject.Parse(ListReturndrp.Content.ReadAsStringAsync().Result);
            var ListDDL = JsonConvert.DeserializeObject<AdditionNoticeVM>(vMMs["data"].ToString());
            CurrencyList = ListDDL.CurrencyList;
            SupplierList = ListDDL.SupplierList;
            ClientList = ListDDL.ClientList;
        }

        protected async Task AddAddtionNotice()
        {
            Addition.ClientId = ClientList.Find(x => x.Name == clients.Name).ID;
            IsDisabled = true;
            bool Checker = true;
            if (UserTypeFlag == 1)
            {
                if (Addition.ClientId == 0 || Addition.ClientId == null)
                {
                    Checker = false;
                }
            }
            else if (UserTypeFlag == 2)
            {
                if (Addition.SupplierId == 0 || Addition.SupplierId == null)
                {
                    Checker = false;
                }
            }
            else
            {
                Checker = false;
            }
            if (Checker)
            {
                ValidateSupplierOrClient = "";
                var AddModel = JsonConvert.SerializeObject(Addition);
                var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/AdditiontNotice/AddAdditiontNotice", content);
                if (result.IsSuccessStatusCode)
                {
                    ShowConfirm = true;
                }
            }
            else
            {

                ValidateSupplierOrClient = "يجب إدخال عميل أو مورد";
            }
        }

        protected async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        }


        protected void ClientOrSupplier()
        {
            if (UserTypeFlag == 1)
            {
                Addition.UserTypeFlag = 1;
                ShowClient = true;
                ShowSupplier = false;
            }
            else
            {
                Addition.UserTypeFlag = 2;
                ShowSupplier = true;
                ShowClient = false;
            }
        }

        async Task ReloadClientsList()
        {
            clients.Name = String.Empty;

            var ListReturn = await Http.GetAsync("api/Client/GetClients");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ClientList = JsonConvert.DeserializeObject<List<ClientsVM>>(vMs["data"].ToString());

        }


    }
}
