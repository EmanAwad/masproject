﻿using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.FundTransaction.DiscountNotices;
using MatBlazor;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System;
using System.IO;
using BlazorInputFile;
using System.Linq;
using MasProject.Client.Shared;
namespace MasProject.Client.Pages.FundTransaction.DiscountNotices
{
    public partial class DiscountNoticeAdd
    {
        private PageTitle PageTitle;
        DiscountNoticeVM Discount = new DiscountNoticeVM();
        List<CurrencyVM> CurrencyList = new List<CurrencyVM>();
        List<TreasuryVM> TreasuryList = new List<TreasuryVM>();
        List<ClientsVM> ClientList = new List<ClientsVM>();
        List<SupplierVM> SupplierList = new List<SupplierVM>();
        ClientsVM clients = new ClientsVM();
        bool ShowConfirm = false;
        int UserTypeFlag = 0;
        bool ShowClient = false;
        bool ShowSupplier = false;
        bool IsDisabled = false;

        string ValidateSupplierOrClient = "";
        protected override async Task OnInitializedAsync()
        {
            Discount= new DiscountNoticeVM();
            var IdReturn = await Http.GetAsync("api/DiscountNotice/GenerateSerial");
            var IdvMs = JObject.Parse(IdReturn.Content.ReadAsStringAsync().Result);
            Discount.SerialNumber = JsonConvert.DeserializeObject<int>(IdvMs["data"].ToString());
            Discount.Date = DateTime.Now;
            Discount.CurrencyId = 2;
            await BindList();
        }
        async Task BindList()
        {
            //Fill DropDown
            var ListReturndrp = await Http.GetAsync("api/DiscountNotice/GetListsOfDDl");
            var vMMs = JObject.Parse(ListReturndrp.Content.ReadAsStringAsync().Result);
            var ListDDL = JsonConvert.DeserializeObject<DiscountNoticeVM>(vMMs["data"].ToString());
            CurrencyList = ListDDL.CurrencyList;
            ClientList = ListDDL.ClientList;
            SupplierList = ListDDL.SupplierList;
        }
       
        protected async Task AddDiscountNotice()
        {
            IsDisabled = true;
            Discount.ClientId = ClientList.Find(x => x.Name == clients.Name).ID;
            bool Checker = true;
            if (UserTypeFlag == 1)
            {
                if (Discount.ClientId == 0 || Discount.ClientId == null)
                {
                    Checker = false;
                }
            }
            else if (UserTypeFlag == 2)
            {
                if (Discount.SupplierId == 0 || Discount.SupplierId == null)
                {
                    Checker = false;
                }
            }
            else
            {
                Checker = false;
            }
            if (Checker)
            {
                ValidateSupplierOrClient = "";
                var AddModel = JsonConvert.SerializeObject(Discount);
                var content = new StringContent(AddModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/DiscountNotice/AddDiscountNotice", content);
                if (result.IsSuccessStatusCode)
                {
                    ShowConfirm = true;
                }
            }
            else
            {
                ValidateSupplierOrClient = "يجب إدخال عميل أو مورد";
            }
        }
        protected async Task Confirm()
        {
            await navigationManager.NavigateToExitWindowAsync(jsRuntime);
        }

        protected void ClientOrSupplier()
        {
            if (UserTypeFlag == 1)
            {
                Discount.UserTypeFlag = 1;
                ShowClient = true;
                ShowSupplier = false;
            }
            else
            {
                Discount.UserTypeFlag = 2;
                ShowSupplier = true;
                ShowClient = false;
            }
        }

        async Task ReloadClientsList()
        {
            clients.Name = String.Empty;

            var ListReturn = await Http.GetAsync("api/Client/GetClients");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ClientList = JsonConvert.DeserializeObject<List<ClientsVM>>(vMs["data"].ToString());

        }

    }
}