﻿using MasProject.Domain.ViewModel.FundTransaction.DiscountNotices;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using Microsoft.JSInterop;
using MasProject.Domain.ViewModel.Persons;
using System.Net.Http;
using System;

namespace MasProject.Client.Pages.FundTransaction.DiscountNotices
{
    public partial class DiscountNotice
    {
        private PageTitle PageTitle;
        List<DiscountNoticeVM> DiscountNoticeList;
        DiscountNoticeVM Discount = new DiscountNoticeVM();
    

        List<SupplierVM> SupplierList = new List<SupplierVM>();
        List<ClientsVM> ClientList = new List<ClientsVM>();
        ClientsVM clients = new ClientsVM();
        bool ShowDelete = false;
        int DiscountNoticeID;
        int ClientId;
        int SerialNumber;
        protected override async Task OnInitializedAsync()
        {
            await BindList();
        }
        async Task BindList()
        {
            //Fill Table
            var ListReturn = await Http.GetAsync("api/DiscountNotice/GetDiscountNotice");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            DiscountNoticeList = JsonConvert.DeserializeObject<List<DiscountNoticeVM>>(vMs["data"].ToString());
            //Fill DropDown
            var ListReturndrp = await Http.GetAsync("api/AdditiontNotice/GetListsOfDDl");
            var vMMs = JObject.Parse(ListReturndrp.Content.ReadAsStringAsync().Result);
            var ListDDL = JsonConvert.DeserializeObject<DiscountNoticeVM>(vMMs["data"].ToString());

            SupplierList = ListDDL.SupplierList;
            ClientList = ListDDL.ClientList;
        }
        protected async Task GetAllByClient()
        {
            Discount.ClientId = ClientList.Find(x => x.Name == clients.Name).ID;
            if (Discount.ClientId != 0)
            {
                string uri = "/api/DiscountNotice/GetDiscountNoticeByClient?ClientId=" + Discount.ClientId;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                DiscountNoticeList = JsonConvert.DeserializeObject<List<DiscountNoticeVM>>(vMs["data"].ToString());
            }
            else
            {
                var ListReturn = await Http.GetAsync("api/DiscountNotice/GetDiscountNotice");
                var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
                DiscountNoticeList = JsonConvert.DeserializeObject<List<DiscountNoticeVM>>(vMs["data"].ToString());
            }

            StateHasChanged();

        }
        async Task ReloadClientsList()
        {
            clients.Name = String.Empty;

            var ListReturn = await Http.GetAsync("api/Client/GetClients");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ClientList = JsonConvert.DeserializeObject<List<ClientsVM>>(vMs["data"].ToString());

        }
        protected async Task GetAllBySupplier()
        {

            if (Discount.SupplierId != 0)
            {
                string uri = "/api/DiscountNotice/GetDiscountNoticeBySupplier?SuppID=" + Discount.SupplierId;
                HttpResponseMessage response = await Http.GetAsync(uri);
                var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                DiscountNoticeList = JsonConvert.DeserializeObject<List<DiscountNoticeVM>>(vMs["data"].ToString());
            }
            else
            {
                var ListReturn = await Http.GetAsync("api/DiscountNotice/GetDiscountNotice");
                var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
                DiscountNoticeList = JsonConvert.DeserializeObject<List<DiscountNoticeVM>>(vMs["data"].ToString());
            }

            StateHasChanged();

        }
        protected async Task AddNewDiscountNotice()
        {
            // await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/DiscountNoticeAdd", "جارى التحميل", 635);
            await jsRuntime.InvokeAsync<object>("open", "/DiscountNoticeAdd", "_blank");
        }
        protected async Task EditOldDiscountNotice(int ID,string Type)
        {
            //  await navigationManager.NavigateToNewWindowAsync(jsRuntime, "/DiscountNoticeEdit?ID=" + ID, "جارى التحميل", 635);
            // await jsRuntime.InvokeAsync<object>("open", "/DiscountNoticeEdit?ID=" + ID, "_blank");
            await jsRuntime.InvokeAsync<object>("open", "/DiscountNoticeEdit?ID=" + ID + "&Type=" + Type, "_blank");
        }
        protected void DeleteDiscountNotice(int Id, int serialNumber, int Clientid)
        {
            DiscountNoticeID = Id;
            ClientId = Clientid;
            SerialNumber = serialNumber;
            ShowDelete = true;
        }
        protected async Task ConfirmDelete(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                string uri = "api/DiscountNotice/DeleteDiscountNotice?ID=" + DiscountNoticeID + "&SerialNumber=" + SerialNumber + "&ClientId=" + ClientId;
                var result = await Http.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    await BindList();
                }
            }
            ShowDelete = false;
        }
    }
}