﻿using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.FundTransaction.DiscountNotices;
using MatBlazor;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MasProject.Client.Shared;
using System;

namespace MasProject.Client.Pages.FundTransaction.DiscountNotices
{
    public partial class DiscountNoticeEdit
    {
        private PageTitle PageTitle;
        DiscountNoticeVM Discount = new DiscountNoticeVM();
        List<CurrencyVM> CurrencyList = new List<CurrencyVM>();
        List<ClientsVM> ClientList = new List<ClientsVM>();
        List<SupplierVM> SupplierList = new List<SupplierVM>();
        ClientsVM clients = new ClientsVM();
        bool ShowConfirm = false;
        int UserTypeFlag = 0;
        bool ShowClient = false;
        bool ShowSupplier = false;
        bool IsDisabled = false;
        string ValidateSupplierOrClient = "";
        protected override async Task OnInitializedAsync()
        {
           
            await BindList();
            var querystring = ExtensionMethods.QueryString(nav);
            var id = querystring["ID"];
            await EditOldDiscountNotice(int.Parse(id));
            var Type = querystring["Type"];
            if (Type == "View")
            {
                //disable controls
                IsDisabled = true;
                PageTitle.Title = "عرض   إشعار الخصم";
            }
            else
            {
                IsDisabled = false;
                PageTitle.Title = "تعديل   إشعار الخصم";
            }

        }
        async Task BindList()
        {
            //Fill DropDown
            var ListReturndrp = await Http.GetAsync("api/DiscountNotice/GetListsOfDDl");
            var vMMs = JObject.Parse(ListReturndrp.Content.ReadAsStringAsync().Result);
            var ListDDL = JsonConvert.DeserializeObject<DiscountNoticeVM>(vMMs["data"].ToString());
            CurrencyList = ListDDL.CurrencyList;
            ClientList = ListDDL.ClientList;
            SupplierList = ListDDL.SupplierList;
        }
        protected async Task Confirm()
        {
            await nav.NavigateToExitWindowAsync(jsRuntime);
        }
        protected async Task EditOldDiscountNotice(int ID)
        {
            string uri = "/api/DiscountNotice/GetSpecificDiscountNotice?Id=" + ID;
            HttpResponseMessage response = await Http.GetAsync(uri);
            var vMs = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            Discount = JsonConvert.DeserializeObject<DiscountNoticeVM>(vMs["data"].ToString());
            if (Discount.UserTypeFlag == 1)
            {
                UserTypeFlag = 1;
                ClientOrSupplier();
            }
            else if (Discount.UserTypeFlag == 2)
            {
                UserTypeFlag = 2;
                ClientOrSupplier();
            }
            clients.Name = Discount.ClientName;
        }
        protected async Task EditDiscountNotice()
        {
            Discount.ClientId = ClientList.Find(x => x.Name == clients.Name).ID;
            bool Checker = true;
            if (UserTypeFlag == 1)
            {
                if (Discount.ClientId == 0 || Discount.ClientId == null)
                {
                    Checker = false;
                }
            }
            else if (UserTypeFlag == 2)
            {
                if (Discount.SupplierId == 0 || Discount.SupplierId == null)
                {
                    Checker = false;
                }
            }
            else
            {
                Checker = false;
            }
            if (Checker)
            {
                ValidateSupplierOrClient = "";
                var EditModel = JsonConvert.SerializeObject(Discount);
                var content = new StringContent(EditModel, Encoding.UTF8, "application/json");
                var result = await Http.PostAsync("api/DiscountNotice/EditDiscountNotice", content);
                if (result.IsSuccessStatusCode)
                {
                    ShowConfirm = true;
                }
            }
            else
            {
                ValidateSupplierOrClient = "يجب إدخال عميل أو مورد";
            }
        }

        protected void ClientOrSupplier()
        {
            if (UserTypeFlag == 1)
            {
                Discount.UserTypeFlag = 1;
                ShowClient = true;
                ShowSupplier = false;
            }
            else
            {
                Discount.UserTypeFlag = 2;
                ShowSupplier = true;
                ShowClient = false;
            }
        }
        async Task ReloadClientsList()
        {
            clients.Name = String.Empty;

            var ListReturn = await Http.GetAsync("api/Client/GetClients");
            var vMs = JObject.Parse(ListReturn.Content.ReadAsStringAsync().Result);
            ClientList = JsonConvert.DeserializeObject<List<ClientsVM>>(vMs["data"].ToString());

        }

    }
}