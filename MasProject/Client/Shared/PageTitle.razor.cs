﻿using Microsoft.AspNetCore.Components;
using System;
using Microsoft.JSInterop;
using System.Threading.Tasks;
using System.Collections.Specialized;
using System.Web;

namespace MasProject.Client.Shared
{
    public partial class PageTitle
    {
        private String _title = "";
        [Parameter]
        public String Title
        {
            get
            {
                return this._title;
            }
            set
            {
                this._title = value;
                setTitle();
            }
        }
        protected override void OnInitialized()
        {
            setTitle();
        }

        private void setTitle()
        {
            JSRuntime.InvokeAsync<string>("setTitle", new object[] { this._title });
        }
    }
    public static class NavigationManagerExtensions
    {
        public static async Task NavigateToNewWindowAsync(this NavigationManager navigator, IJSRuntime jsRuntime, string url, string content, int size)
        {
            await jsRuntime.InvokeAsync<object>("NavigationManagerExtensions.openInNewWindow", url, content,size);
        }
        public static async Task NavigateToExitWindowAsync(this NavigationManager navigator, IJSRuntime jsRuntime)
        {
            await jsRuntime.InvokeAsync<object>("NavigationManagerExtensions.ExitWindow");
        }
       
    }
    
    public static class ExtensionMethods
    {
        // get entire querystring name/value collection
        public static NameValueCollection QueryString(this NavigationManager navigationManager)
        {
            return HttpUtility.ParseQueryString(new Uri(navigationManager.Uri).Query);
        }

        // get single querystring value with specified key
        public static string QueryString(this NavigationManager navigationManager, string key)
        {
            return navigationManager.QueryString()[key];
        }
    }
}
