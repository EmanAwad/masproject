﻿using MasProject.Data.DataAccess;
using MasProject.Domain;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.StoreTransaction.Quotation;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;


namespace MasProject.Server.Controllers.API.StoreTransaction
{

    public class QuotationController : BaseController
    {
        public QuotationController(DBContext context) : base(context)
        {
        }       

        [Route("api/Quotation/GetSalesByBranch")]
        [HttpGet]
        public ActionResult GetSalesByBranch(int BranchId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.QuotationService.GetAllByBranch(BranchId).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/Quotation/GetQuotations")]
        [HttpGet]
        public ActionResult GetQuotations()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                BillsTbl<QuotationVM> BillsTbl = new BillsTbl<QuotationVM> { };
                QuotationVM SalesBill = new QuotationVM
                {
                    BranchList = UnitOfWork.BranchServices.GetAll().ToList(),
                };
                BillsTbl.SearchItems = SalesBill;
                BillsTbl.BillsList = UnitOfWork.QuotationService.GetAll().ToList();
                obj.Data = BillsTbl;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/Quotation/GetSpecificQuotation")]
        [HttpGet]
        public ActionResult GetSpecificQuotation(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                QuotationVM model = UnitOfWork.QuotationService.Get(Id);
                model.QuotationItems = UnitOfWork.QuotationItemsService.GetBySerial(model.SerialNumber.Value);
                // model.TaxFileNumber = int.Parse(UnitOfWork.ClientsService.Get(model.ClientId).Taxfile);
                int indexer = 0;
                foreach (var item in model.QuotationItems)
                {
                    if (item != null)
                    {
                        if (item.ItemId != 0)
                        {
                            item.Identifer = indexer + 1;
                            item.ItemImg = UnitOfWork.ItemServices.GetItemImage(item.ItemId).ItemImg;
                            indexer++;
                        }
                    }
                }
                model.BranchList = UnitOfWork.BranchServices.GetAll().ToList();
                model.DealTypeList = UnitOfWork.DealTypeService.GetAll().ToList();
                model.ItemList = UnitOfWork.ItemServices.GetItems().ToList();
                model.ItemImg = "";
                obj.Data = model;
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Quotation/GetListDDL")]
        [HttpGet]
        public ActionResult GetListddl()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                QuotationVM QuotVM = new QuotationVM
                {
                    DealTypeList = UnitOfWork.DealTypeService.GetAll().ToList(),
                    ItemList = UnitOfWork.ItemServices.GetItems().ToList(),
                    SerialNumber = UnitOfWork.QuotationService.GenerateSerial(0),
                    BranchList = UnitOfWork.BranchServices.GetAll().ToList(),
                    Date = DateTime.Now,
                    DiscountPrecentage = 0,
                    DiscountValue = 0,
                };
                obj.Data = QuotVM;
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }
        [Route("api/Quotation/GenerateSerial")]
        [HttpGet]
        public ActionResult GenerateSerial(int BranchId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.QuotationService.GenerateSerial(BranchId);
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/Quotation/AddQuotation")]
        [HttpPost]

        public IActionResult AddQuotation(QuotationVM QuotModel)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                QuotModel.SerialNumber = UnitOfWork.QuotationService.GenerateSerial(QuotModel.BranchId);
                var BillSaved = UnitOfWork.QuotationService.Add(QuotModel);
                UnitOfWork.Commit();
                obj.Data = BillSaved.ID;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }


        [Route("api/Quotation/EditQuotation")]
        [HttpPost]
        public IActionResult EditQuotation(QuotationVM QuotModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.QuotationService.Edit(QuotModel);
                //delete old items
                UnitOfWork.QuotationItemsService.DeletePhysical(QuotModel.SerialNumber.Value);
                UnitOfWork.Commit();
                foreach (var Q in QuotModel.QuotationItems)
                {
                    if (Q != null)
                    {
                        if (Q.ItemId != 0)
                        {
                            Q.SerialNumber = QuotModel.SerialNumber;
                            UnitOfWork.QuotationItemsService.Add(Q);
                        }
                    }
                }
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/Quotation/DeleteQuotation")]
        [HttpGet]
        public IActionResult DeleteQuotation(int ID)//by serialNumber
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.QuotationService.Delete(ID);
                UnitOfWork.QuotationItemsService.Delete(ID);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}