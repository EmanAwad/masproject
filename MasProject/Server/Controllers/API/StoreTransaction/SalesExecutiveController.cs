﻿using MasProject.Data.DataAccess;
using MasProject.Domain;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using MasProject.Domain.ViewModel.StoreTransaction.Quotation;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Collections.Generic;
using static MasProject.Domain.Enums.Enums;
using MasProject.Domain.ViewModel.TransVM;

namespace MasProject.Server.Controllers.API.StoreTransaction
{

    public class SalesExecutiveController : BaseController
    {
        public SalesExecutiveController(DBContext context) : base(context)
        {
        }

        [Route("api/ExecutiveBills/GetSalesByClient")]
        [HttpGet]
        public ActionResult GetSalesByClient(int ClientId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ExecutiveBillsService.GetAllByClient(ClientId).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/ExecutiveBills/GetSalesByBranch")]
        [HttpGet]
        public ActionResult GetSalesByBranch(int BranchId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ExecutiveBillsService.GetAllByBranch(BranchId).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/ExecutiveBills/GenerateSerial")]
        [HttpGet]
        public ActionResult GenerateSerial(int BranchId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ExecutiveBillsService.GenerateSerial(BranchId);
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/ExecutiveBills/GetExecutiveBills")]
        [HttpGet]
        public ActionResult GetExecutiveBills()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ExecutiveBillsService.GetAll().ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/ExecutiveBills/GetPrice")]
        [HttpGet]
        public ActionResult GetPrice(int ClientId, int ItemId, int ThisSerial)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                var price = UnitOfWork.ExecutiveBillsService.GetBillIdByClient(ClientId, ThisSerial, ItemId);
                obj.Data = price;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/ExecutiveBills/GetListDDL")]
        [HttpGet]
        public ActionResult GetListddl()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                ExecutiveBillsVM SalesBill = new ExecutiveBillsVM
                {
                    StoreList = UnitOfWork.StoreServices.GetAll().ToList(),
                    ClientList = UnitOfWork.ClientsService.GetClients().ToList(),
                    DealTypeList = UnitOfWork.DealTypeService.GetAll().ToList(),
                    ItemList = UnitOfWork.ItemServices.GetItems().ToList(),
                    sellingExpensesList = UnitOfWork.SellingExpensesService.GetAll().ToList(),
                    SerialNumber = UnitOfWork.ExecutiveBillsService.GenerateSerial(0),
                    BranchList = UnitOfWork.BranchServices.GetAll().ToList(),
                    OtherIncomeList=UnitOfWork.OtherIncomeService.GetAll().ToList(),
                    Date = DateTime.Now,
                    DiscountPrecentage = 0,
                    DiscountValue = 0,
                };
                obj.Data = SalesBill;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/ExecutiveBills/GetSpecificExecutiveBills")]
        [HttpGet]
        public ActionResult GetSpecificExecutiveBills(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                ExecutiveBillsVM model = UnitOfWork.ExecutiveBillsService.Get(Id);
                //model.ExecutiveItems= UnitOfWork.ExecutiveItemsService.GetBySerial(model.SerialNumber.Value);
                model.BillStores = UnitOfWork.ExecutiveBillStoresService.GetBySerial(model.SerialNumber.Value);
                model.TaxFileNumber = UnitOfWork.ClientsService.Get(model.ClientId).Taxfile;
                model.ClientBalance = UnitOfWork.TransClientService.GetBalance(model.ClientId);
                int indexer = 0;
                foreach (var store in model.BillStores)
                {
                    if (store != null)
                    {
                        if (store.StoreId != 0 & store.StoreId != null)
                        {
                            int indexerItem = 0;
                            store.ExecutiveItems = UnitOfWork.ExecutiveItemsService.GetBySerial(model.SerialNumber.Value, (int)store.StoreId, store.Identifer);
                            foreach (var item in store.ExecutiveItems)
                            {
                                if (item != null)
                                {
                                    if (item.ItemId != 0)
                                    {

                                        item.Identifer = indexer + 1;
                                        item.ItemImg = UnitOfWork.ItemServices.GetItemImage(item.ItemId).ItemImg;
                                        indexerItem++;
                                    }
                                }
                            }
                            if (store.Identifer == 0 || store.Identifer == null)
                            {
                                store.Identifer = indexer + 1;
                            }
                            indexer++;
                        }
                    }
                }
                model.ExecutiveSellExpenses = UnitOfWork.ExecutiveSellExpensesService.GetBySerial(model.SerialNumber.Value);
                indexer = 0;
                foreach (var item in model.ExecutiveSellExpenses)
                {
                    if (item != null)
                    {
                    item.Identifer = indexer + 1;
                    indexer++;
                    }
                }
                model.ExecutiveOtherIncome = UnitOfWork.ExecutiveOtherIncomeService.GetBySerial(model.SerialNumber.Value);
                indexer = 0;
                foreach (var item in model.ExecutiveOtherIncome)
                {
                    if (item != null)
                    {
                    item.Identifer = indexer + 1;
                    indexer++;
                    }
                }
                obj.Data = model;
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }
      
        [Route("api/ExecutiveBills/GetExecutiveFromQuotation")]
        [HttpGet]
        public ActionResult GetExecutiveFromQuotation(int QuotaionId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                QuotationVM model = UnitOfWork.QuotationService.Get(QuotaionId);
                model.QuotationItems = UnitOfWork.QuotationItemsService.GetBySerial(model.SerialNumber.Value);
                model.IsConverted = true;
                UnitOfWork.QuotationService.Edit(model);
                UnitOfWork.Commit();
                ExecutiveBillsVM NewModel = new ExecutiveBillsVM
                {
                    SerialNumber = UnitOfWork.ExecutiveBillsService.GenerateSerial(0),
                    Date = model.Date,
                    StoreId = model.StoreId,
                    // ClientId=model.ClientId,
                    ClientBalance = model.ClientBalance,
                    DealTypeId = model.DealTypeId,
                    TaxFileNumber = model.TaxFileNumber.ToString(),
                    FlagType = model.FlagType,
                    TotalPrice = model.TotalPrice,
                    DiscountPrecentage = model.DiscountPrecentage,
                    DiscountValue = model.DiscountValue,
                    TotalAfterDiscount = model.TotalAfterDiscount,
                    ShowNotesFlag = model.ShowNotesFlag,
                    TotalAfterTax = model.TotalAfterTax,
                    AddtionTax = model.AddtionTax,
                    AddtionTaxAmount = model.AddtionTaxAmount,
                    SourceDeduction = model.SourceDeduction,
                    SourceDeductionAmount = model.SourceDeductionAmount,
                    MultiStore = false,
                    BranchId = model.BranchId,
                    Note = model.Note,
                };
                NewModel.BillStores = new List<ExecutiveBillStoresVM>();
                ExecutiveBillStoresVM StoreBill = new ExecutiveBillStoresVM { Identifer = 1, StoreId = model.StoreId };
                StoreBill.ExecutiveItems = new List<ExecutiveItemsVM>();
                int indexer = 0;
                foreach (var item in model.QuotationItems)
                {
                    if (item != null)
                    {
                        if (item.ItemId != 0)
                        {
                            ExecutiveItemsVM Item = new ExecutiveItemsVM
                            {
                                Identifer = indexer + 1,
                                Box_ParCode = item.Box_ParCode,
                                ItemId = item.ItemId,
                                Quantity = item.Quantity,
                                Price = item.Price,
                                Total = item.Total,
                                StoreId = model.StoreId,
                            };

                            indexer++;
                            StoreBill.ExecutiveItems.Add(Item);
                        }
                    }
                }
                NewModel.BillStores.Add(StoreBill);
                obj.Data = NewModel;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/ExecutiveBills/AddExecutiveBills")]
        [HttpPost]
        public IActionResult AddExecutiveBills(ExecutiveBillsVM BillModel)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                BillModel.SerialNumber = UnitOfWork.ExecutiveBillsService.GenerateSerial(BillModel.BranchId);
                var BillSaved = UnitOfWork.ExecutiveBillsService.Add(BillModel);
                UnitOfWork.Commit();
                obj.Data = BillSaved.ID;
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string measage = ex.Message;
                return BadRequest();
            }
        }


        [Route("api/ExecutiveBills/EditExecutiveBills")]
        [HttpPost]
        public IActionResult EditExecutiveBills(ExecutiveBillsVM BillModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                //////check if client changed for this bill
                ExecutiveBillsVM model = UnitOfWork.ExecutiveBillsService.Get(BillModel.ID);
                if (model.ClientId != BillModel.ClientId)
                {
                    int Id = UnitOfWork.TransClientService.RecalculateClientBalance(model.ClientId, (int)model.SerialNumber);
                    UnitOfWork.TransClientService.Delete(Id);
                }
                if (BillModel.BillStores.Count() == 1)
                {
                    BillModel.MultiStore = false;
                }
                else
                {
                    BillModel.MultiStore = true;
                }
                UnitOfWork.ExecutiveBillsService.Edit(BillModel);
                //delete old items
                UnitOfWork.ExecutiveItemsService.DeletePhysical(BillModel.SerialNumber.Value);
                UnitOfWork.ExecutiveBillStoresService.DeletePhysical(BillModel.SerialNumber.Value);
                UnitOfWork.ExecutiveSellExpensesService.DeletePhysical(BillModel.SerialNumber.Value);
                UnitOfWork.ExecutiveOtherIncomeService.DeletePhysical(BillModel.SerialNumber.Value);
                UnitOfWork.Commit();
                foreach (var StoreItem in BillModel.BillStores)
                {
                    if (StoreItem != null)
                    {
                        if (StoreItem.StoreId != 0 & StoreItem.StoreId != null)
                        {
                            StoreItem.SerialNumber = BillModel.SerialNumber;
                            UnitOfWork.ExecutiveBillStoresService.Add(StoreItem);
                            foreach (var items in StoreItem.ExecutiveItems)
                            {
                                if (items != null)
                                {
                                    if (items.ItemId != 0)
                                    {
                                        items.SerialNumber = BillModel.SerialNumber;
                                        items.StoreId = StoreItem.StoreId;
                                        items.Identifer = StoreItem.Identifer;//to take store idetifier and save it
                                        UnitOfWork.ExecutiveItemsService.Add(items);
                                    }
                                }
                            }
                        }
                    }
                }
                foreach (var Expenses in BillModel.ExecutiveSellExpenses)
                {
                    if (Expenses != null)
                    {
                        if (Expenses.ExpensesId != 0 & Expenses.ExpensesId != null)
                        {
                        Expenses.SerialNumber = BillModel.SerialNumber;
                        UnitOfWork.ExecutiveSellExpensesService.Add(Expenses);
                        }
                    }
                }
                foreach (var Income in BillModel.ExecutiveOtherIncome)
                {
                    if (Income != null)
                    {
                        if (Income.IncomeId !=0 & Income.IncomeId != null)
                        {
                        Income.SerialNumber = BillModel.SerialNumber;
                        UnitOfWork.ExecutiveOtherIncomeService.Add(Income);
                        }
                    }
                }
                UnitOfWork.Commit();
                ///////////////////////////////////////////////////trans///////////////////////////////
                var currentBalance = UnitOfWork.TransClientService.GetLatestBalance((int)BillModel.ClientId, BillModel.Date.ToLocalTime());
                var Balance = BillModel.FlagType ? (decimal)BillModel.TotalAfterTax : (decimal)BillModel.TotalAfterDiscount;
                decimal NewBalance =  currentBalance + Balance;
                TransClientVM TransClient = new TransClientVM
                {
                    ClientId = BillModel.ClientId,
                    ClientType = TransClientTypeEnum.تنفيذ,
                    Credit = 0,
                    CreateDate = DateTime.Now,
                    OpenBalFlag = false,
                    Debit = Balance,
                    Date = BillModel.Date,
                    SerialNumber = BillModel.SerialNumber.Value,
                    IsDeleted = false,
                    Balance = NewBalance,
                };
                bool checkreturn = UnitOfWork.TransClientService.Add(TransClient, Balance);
                UnitOfWork.Commit();
                //recacaluate
                if (!checkreturn)
                {
                    UnitOfWork.TransClientService.RecalculateClientBalance(BillModel.ClientId, (int)BillModel.SerialNumber);
                }
                ///////////////////////////////////////////////////////////////////////////
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/ExecutiveBills/ConvertExecutiveBills")]
        [HttpPost]
        public IActionResult ConvertExecutiveBills(ExecutiveBillsVM BillModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                List<ExecutiveBillStoresVM> BillStoresList = UnitOfWork.ExecutiveBillStoresService.GetBySerial(BillModel.SerialNumber.Value);
                if (BillStoresList != null && BillStoresList.Count > 0)
                {
                    foreach (var BillStore in BillStoresList)
                    {
                        if (BillStore != null)
                        {
                            if (BillStore.StoreId != 0 & BillStore.StoreId != null)
                            {
                                BillModel.StoreId = BillStore.StoreId;
                                BillStore.ExecutiveItems = UnitOfWork.ExecutiveItemsService.GetBySerial(BillModel.SerialNumber.Value, (int)BillStore.StoreId, BillStore.Identifer);
                                var payment = UnitOfWork.TempPaymentVoucherService.ConvertExecutiveToTemp(BillModel);

                                UnitOfWork.Commit();
                                foreach (var items in BillStore.ExecutiveItems)
                                {
                                    if (items != null)
                                    {
                                        if (items.ItemId != 0)
                                        {

                                            items.SerialNumber = payment.SerialNumber;
                                            UnitOfWork.TempPaymentVoucherItemService.ConvertExecutiveItemToTempItem(items);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                UnitOfWork.Commit();
                BillModel.IsConverted = true;
                UnitOfWork.ExecutiveBillsService.Edit(BillModel);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/ExecutiveBills/DeleteExecutiveBills")]
        [HttpGet]
        public IActionResult DeleteExecutiveBills(int ID, int SerialNumber, int ClientId)//by serialNumber
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.ExecutiveBillsService.Delete(ID);
                UnitOfWork.ExecutiveBillStoresService.Delete(ID);
                UnitOfWork.ExecutiveItemsService.Delete(ID);
                UnitOfWork.ExecutiveSellExpensesService.Delete(ID);
                if (ClientId != 0)
                {
                    int Id = UnitOfWork.TransClientService.RecalculateClientBalance(ClientId, SerialNumber);
                    UnitOfWork.TransClientService.Delete(Id);
                }                
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

    }
}
