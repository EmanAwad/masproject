﻿using AspNetCore.Reporting;
using Microsoft.Extensions.Configuration;
using MasProject.Data.DataAccess;
using MasProject.Domain;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.StoreTransaction.Purchase;
using MasProject.Domain.ViewModel.TransVM;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using static MasProject.Domain.Enums.Enums;

namespace MasProject.Server.Controllers.API.StoreTransaction
{
   
    public class PurchaseController : BaseController
    {
        private IWebHostEnvironment _hostingEnvironment;
        private readonly IConfiguration _config;
        public PurchaseController(DBContext context, IWebHostEnvironment HostingEnvironment, IConfiguration config) : base(context)
        {
            _config = config ?? throw new System.ArgumentNullException(nameof(config));
            this._hostingEnvironment = HostingEnvironment;
            System.Text.Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
        }
        [Route("api/Purchase/GenerateSerial")]
        [HttpGet]
        public ActionResult GenerateSerial(int BranchId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.PurchaseBillService.GenerateSerial(BranchId);
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/Purchase/GetPurchases")]
        [HttpGet]
        public ActionResult GetPurchases()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                BillsTbl<PurchaseBillVM> purchasesTbl = new BillsTbl<PurchaseBillVM> { };
                 PurchaseBillVM SalesBill = new PurchaseBillVM
                {
                    SupplierList = UnitOfWork.SupplierService.GetSuppliers().ToList(),
                    BranchList = UnitOfWork.BranchServices.GetAll().ToList(),
                };
                purchasesTbl.SearchItems = SalesBill;
                purchasesTbl.BillsList=UnitOfWork.PurchaseBillService.GetAll().ToList();
                obj.Data = purchasesTbl;
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [Route("api/Purchase/GetAllBySupplier")]
        [HttpGet]
        public ActionResult GetAllBySupplier(int SupplierId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.PurchaseBillService.GetAllBySupplier(SupplierId).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/Purchase/GetAllByDate")]
        [HttpGet]
        public ActionResult GetAllByDate(DateTime date)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.PurchaseBillService.GetAllByDate(date).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/Purchase/GetAllByBranch")]
        [HttpGet]
        public ActionResult GetAllByBranch(int BranchId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.PurchaseBillService.GetAllByBranch(BranchId).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/Purchase/GetPrice")]
        [HttpGet]
        public ActionResult GetPrice(int SupplierId, int ItemId, int ThisSerial)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                var price = UnitOfWork.PurchaseBillService.GetBillIdBySupplier(SupplierId, ThisSerial, ItemId);
                obj.Data = price;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/Purchase/GetLatestPrice")]
        [HttpGet]

        public IActionResult GetLatestPrice(int ItemId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.PurchaseItemService.GetLatestPrice(ItemId);
                UnitOfWork.Commit();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/Purchase/GetSpecificPurchase")]
        [HttpGet]
        public ActionResult GetSpecificPurchase(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
               PurchaseBillVM model= UnitOfWork.PurchaseBillService.Get(Id);
                model.PurchaseItem = UnitOfWork.PurchaseItemService.GetBySerial(model.SerialNumber.Value);
                model.Balance = UnitOfWork.TransSupplierService.GetBalance(model.SupplierId);
                model.TaxFileNumber = UnitOfWork.SupplierService.Get(model.SupplierId).Taxfile;
                int indexer = 0;
                foreach (var item in model.PurchaseItem)
                {
                    if (item != null)
                    {
                        if (item.ItemId != 0)
                        {
                            item.Identifer = indexer + 1;
                            item.ItemImg = UnitOfWork.ItemServices.GetItemImage(item.ItemId).ItemImg;
                            indexer++;
                        }
                    }
                }
                model.PurchaseIncome = UnitOfWork.PurchaseOtherIncomeService.GetBySerial(model.SerialNumber.Value);
                indexer = 0;
                foreach (var item in model.PurchaseIncome)
                {
                    if (item != null)
                    {
                        item.Identifer = indexer + 1;
                        indexer++;
                    }
                }
                model.StoreList = UnitOfWork.StoreServices.GetAll().ToList();
                model.SupplierList = UnitOfWork.SupplierService.GetSuppliers().ToList();
                model.BranchList = UnitOfWork.BranchServices.GetAll().ToList();
                model.ItemList = UnitOfWork.ItemServices.GetItems().ToList();
                model.OtherIncomeList = UnitOfWork.OtherIncomeService.GetAll().ToList();
                obj.Data = model;
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        //[Route("api/Purchase/GetListTbl")]
        //[HttpGet]
        //public ActionResult GetListTbl()
        //{
        //    var obj = new ResponseVM();
        //    try
        //    {
        //        obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
        //        obj.Message = ApiStatus.Success;
        //        PurchaseBillVM SalesBill = new PurchaseBillVM
        //        {
        //            SupplierList = UnitOfWork.SupplierService.GetSuppliers().ToList(),
        //            BranchList = UnitOfWork.BranchServices.GetAll().ToList(),
        //        };
        //        obj.Data = SalesBill;
        //        return Ok(obj);
        //    }
        //    catch (Exception)
        //    {
        //        return BadRequest();
        //    }
        //}
        //[Route("api/Purchase/GetListEdit")]
        //[HttpGet]
        //public ActionResult GetListEdit()
        //{
        //    var obj = new ResponseVM();
        //    try
        //    {
        //        obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
        //        obj.Message = ApiStatus.Success;
        //        PurchaseBillVM SalesBill = new PurchaseBillVM
        //        {
        //            StoreList = UnitOfWork.StoreServices.GetAll().ToList(),
        //            SupplierList = UnitOfWork.SupplierService.GetSuppliers().ToList(),
        //            BranchList = UnitOfWork.BranchServices.GetAll().ToList(),
        //            //DealTypeList = UnitOfWork.DealTypeService.GetAll().ToList(),
        //            ItemList = UnitOfWork.ItemServices.GetItems().ToList(),
        //            OtherIncomeList = UnitOfWork.OtherIncomeService.GetAll().ToList(),
        //        };
        //        obj.Data = SalesBill;
        //        return Ok(obj);
        //    }
        //    catch (Exception)
        //    {
        //        return BadRequest();
        //    }
        //}
        [Route("api/Purchase/GetListDDL")]
        [HttpGet]
        public ActionResult GetListddl()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                PurchaseBillVM SalesBill = new PurchaseBillVM
                {
                    StoreList = UnitOfWork.StoreServices.GetAll().ToList(),
                    SupplierList = UnitOfWork.SupplierService.GetSuppliers().ToList(),
                    BranchList=UnitOfWork.BranchServices.GetAll().ToList(),
                    //DealTypeList = UnitOfWork.DealTypeService.GetAll().ToList(),
                    ItemList = UnitOfWork.ItemServices.GetItems().ToList(),
                    OtherIncomeList = UnitOfWork.OtherIncomeService.GetAll().ToList(),
                    SerialNumber = UnitOfWork.PurchaseBillService.GenerateSerial(0),
                    Date = DateTime.Now,
                    DiscountPrecentage = 0,
                    DiscountValue = 0,
                };
                obj.Data = SalesBill;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/Purchase/AddPurchases")]
        [HttpPost]
        public IActionResult AddPurchases(PurchaseBillVM BillModel)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                BillModel.SerialNumber = UnitOfWork.PurchaseBillService.GenerateSerial(BillModel.BranchId);
                var BillSaved = UnitOfWork.PurchaseBillService.Add(BillModel);
                UnitOfWork.Commit();
                obj.Data = BillSaved.ID;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/Purchase/ConfirmPurchasesBill")]
        [HttpPost]
        public IActionResult ConfirmPurchasesBill(PurchaseBillVM BillModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                PurchaseBillVM oldmodel = UnitOfWork.PurchaseBillService.Get(BillModel.ID);
                oldmodel.ApproveId = 1;
                UnitOfWork.PurchaseBillService.Edit(oldmodel);
              
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }
        [Route("api/Purchase/ReviewPurchasesBill")]
        [HttpPost]
        public IActionResult ReviewPurchasesBill(PurchaseBillVM BillModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                PurchaseBillVM oldmodel = UnitOfWork.PurchaseBillService.Get(BillModel.ID);
                oldmodel.RevewId = 1;
                UnitOfWork.PurchaseBillService.Edit(oldmodel);

                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }
        [Route("api/Purchase/EditPurchases")]
        [HttpPost]
        public IActionResult EditPurchases(PurchaseBillVM BillModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                //////check if client changed for this bill
                PurchaseBillVM oldmodel = UnitOfWork.PurchaseBillService.Get(BillModel.ID);
                if (BillModel.SupplierId != oldmodel.SupplierId)
                {
                    int Id = UnitOfWork.TransSupplierService.RecalculateSupplierBalance((int)oldmodel.SupplierId, (int)BillModel.SerialNumber);
                    UnitOfWork.TransSupplierService.Delete(Id);
                }
                UnitOfWork.PurchaseBillService.Edit(BillModel);
                //delete old items
                UnitOfWork.PurchaseItemService.DeletePhysical(BillModel.SerialNumber.Value);
                UnitOfWork.PurchaseOtherIncomeService.DeletePhysical(BillModel.SerialNumber.Value);
                UnitOfWork.Commit();
                foreach (var items in BillModel.PurchaseItem)
                {
                    if (items != null)
                    {
                        if (items.ItemId != 0)
                        {
                            items.SerialNumber = BillModel.SerialNumber;
                            UnitOfWork.PurchaseItemService.Add(items);
                        }
                    }
                }
                foreach (var Income in BillModel.PurchaseIncome)
                {
                    if (Income != null)
                    {
                        if (Income.IncomeId != 0)
                        {
                            Income.SerialNumber = BillModel.SerialNumber;
                            UnitOfWork.PurchaseOtherIncomeService.Add(Income);
                        }
                    }
                }
                UnitOfWork.Commit();
                ///////////////////////////////////////////////////trans///////////////////////////////
                var currentBalance = UnitOfWork.TransSupplierService.GetLatestBalance((int)BillModel.SupplierId, BillModel.Date.ToLocalTime());
                var Balance = BillModel.FlagType ? (decimal)BillModel.TotalAfterTax : (decimal)BillModel.TotalAfterDiscount;
                decimal NewBalance = currentBalance - Balance;
                TransSupplierVM TransSupplier = new TransSupplierVM
                {
                    SupplierId = BillModel.SupplierId,
                    SupplierType = TransSupplierTypeEnum.مشتريات,
                    Credit = BillModel.FlagType ? (decimal)BillModel.TotalAfterTax : (decimal)BillModel.TotalAfterDiscount,
                    CreateDate = DateTime.Now,
                    OpenBalFlag = false,
                    Debit = 0,
                    Date = BillModel.Date,
                    SerialNumber = BillModel.SerialNumber.Value,
                    IsDeleted = false,
                    Balance = NewBalance,
                }; 
                bool checkreturn = UnitOfWork.TransSupplierService.Add(TransSupplier, (decimal)Balance);
                UnitOfWork.Commit();
                //recacaluate
                if (!checkreturn)
                {
                    UnitOfWork.TransSupplierService.RecalculateSupplierBalance((int)BillModel.SupplierId, BillModel.SerialNumber.Value);

                }
                ///////////////////////////////////////////////////////////////////////////
                ///////////////////////////////////////////////////////////////////////////
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }
        [Route("api/Purchase/DeletePurchases")]
        [HttpGet]
        public IActionResult DeletePurchases(int ID, int SerialNumber, int SupplierId)//by serialNumber
        {

            var obj = new ResponseVM();
            try
            {
                //int currobj;
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.PurchaseBillService.Delete(ID);
                UnitOfWork.PurchaseItemService.Delete(ID);
                UnitOfWork.PurchaseOtherIncomeService.Delete(ID);
                if (SupplierId != 0)
                {
                    int Id = UnitOfWork.TransSupplierService.RecalculateSupplierBalance(SupplierId, SerialNumber);
                    UnitOfWork.TransSupplierService.Delete(Id);
                }
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/Purchase/ConvertPurchaseBills")]
        [HttpPost]
        public IActionResult ConvertPurchaseBills(PurchaseBillVM BillModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                var addition = UnitOfWork.TempAdditionVoucherService.ConvertPurchaseToTemp(BillModel);
                UnitOfWork.Commit();
                foreach (var items in BillModel.PurchaseItem)
                {
                    if (items != null)
                    {
                        if (items.ItemId != 0)
                        {
                            items.SerialNumber = addition.SerialNumber;
                            UnitOfWork.TempAdditionVoucherItemService.ConvertPurchaseItemToTempItem(items);
                        }
                    }
                }
                UnitOfWork.Commit();
                BillModel.IsConverted = true;
                UnitOfWork.PurchaseBillService.Edit(BillModel);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("api/Purchase/PurchaseBillsReport")]
        public IActionResult PurchaseBillsReport(int Id)
        {
            try
            {
                var dt = new DataTable();
                PurchaseBillVM model = UnitOfWork.PurchaseBillService.Get(Id);
                model.PurchaseItem = UnitOfWork.PurchaseItemService.GetBySerial(model.SerialNumber.Value);
                List<PurchaseBillVM> modelList = new List<PurchaseBillVM>();
                modelList.Add(model);
                dt = ToDataTable<PurchaseBillVM>(modelList);

                var dtItems = new DataTable();
                dtItems = ToDataTable<PurchaseItemVM>(model.PurchaseItem);

                string mimetype = "";
                int extension = 1;
                if (string.IsNullOrWhiteSpace(_hostingEnvironment.WebRootPath))
                {
                   // _hostingEnvironment.WebRootPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot");
                    string RootPath = _config.GetValue<string>("RootPath");
                    _hostingEnvironment.WebRootPath= $"" + RootPath;
                }
                var path = $"{this._hostingEnvironment.WebRootPath}\\Reports\\BillPurchase.rdlc";

                string SupplierName = UnitOfWork.SupplierService.Get(model.SupplierId).Name;
                string Taxfile = UnitOfWork.SupplierService.Get(model.SupplierId).Taxfile;
                string BranchName = UnitOfWork.BranchServices.Get((int)model.BranchId).Name;
                string Footer = " تاريخ الطباعه : " + DateTime.Now.Date.ToString("dd-MM-yyyy") + " / الساعه: " + DateTime.Now.TimeOfDay;
                NumberToAlphabetic Obj = new NumberToAlphabetic();
                Obj.ConvertNumbersToArabicAlphabet(model.TotalAfterTax.ToString());
                string TotalString = Obj.GetNumberAr();
                Dictionary<string, string> paramters = new Dictionary<string, string>();

                paramters.Add("SupplierName", SupplierName);
                paramters.Add("Taxfile", Taxfile);
                paramters.Add("BranchName", BranchName);
                paramters.Add("FooterString", Footer);
                paramters.Add("TotalAlphbetic", TotalString);
                LocalReport localreport = new LocalReport(path);
                localreport.AddDataSource("MasDataSet", dt);
                localreport.AddDataSource("DataSet1", dtItems);
                var result = localreport.Execute(RenderType.Pdf, extension, paramters, mimetype);
                return File(result.MainStream, "application/pdf");
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return null;
            }
         
        }
        public DataTable ToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }
    }
}
