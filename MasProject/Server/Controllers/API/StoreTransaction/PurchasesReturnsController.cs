﻿using MasProject.Data.DataAccess;
using MasProject.Domain;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.StoreTransaction.Purchase;
using MasProject.Domain.ViewModel.TransVM;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using static MasProject.Domain.Enums.Enums;

namespace MasProject.Server.Controllers.API.StoreTransaction
{

    public class PurchasesReturnsController : BaseController
    {
        public PurchasesReturnsController(DBContext context) : base(context)
        {
        }
        [Route("api/PurchasesReturns/GetPurchasesReturns")]
        [HttpGet]
        public ActionResult GetPurchasesReturns()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.PurchasesReturns_BillService.GetAll().ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/PurchasesReturns/GetAllBySupplier")]
        [HttpGet]
        public ActionResult GetAllBySupplier(int SupplierId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.PurchasesReturns_BillService.GetAllBySupplier(SupplierId).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/PurchasesReturns/GetAllByDate")]
        [HttpGet]
        public ActionResult GetAllByDate(DateTime date)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.PurchasesReturns_BillService.GetAllByDate(date).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/PurchasesReturns/GetAllByBranch")]
        [HttpGet]
        public ActionResult GetAllByBranch(int BranchId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.PurchasesReturns_BillService.GetAllByBranch(BranchId).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/PurchasesReturns/GetSpecificPurchaseReturn")]
        [HttpGet]
        public ActionResult GetSpecificPurchaseReturn(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                PurchasesReturns_BillVM model = UnitOfWork.PurchasesReturns_BillService.Get(Id);
                model.Balance = UnitOfWork.TransSupplierService.GetBalance(model.SupplierId);
                model.PurchaseItem = UnitOfWork.PurchaseReturns_ItemsService.GetBySerial(model.SerialNumber.Value);
                model.TaxFileNumber = UnitOfWork.SupplierService.Get(model.SupplierId).Taxfile;
                int indexer = 0;
                foreach (var item in model.PurchaseItem)
                {
                    if (item != null)
                    {
                        if (item.ItemId != 0)
                        {
                            item.Identifer = indexer + 1;
                            item.ItemImg = UnitOfWork.ItemServices.GetItemImage(item.ItemId).ItemImg;
                            indexer++;
                            //get code for each item
                            item.Box_ParCode = UnitOfWork.ItemServices.Get(item.ItemId).National_ParCode;
                        }
                    }
                }
                model.PurchaseReturns_OtherIncome = UnitOfWork.PurchaseReturns_OtherIncomeService.GetBySerial(model.SerialNumber.Value);
                indexer = 0;
                foreach (var item in model.PurchaseReturns_OtherIncome)
                {
                    if (item != null)
                    {
                        item.Identifer = indexer + 1;
                        indexer++;
                    }
                }
                obj.Data = model;
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }

        [Route("api/PurchasesReturns/AddPurchasesReturns")]
        [HttpPost]

        public IActionResult AddPurchasesReturns(PurchasesReturns_BillVM BillModel)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                BillModel.SerialNumber = UnitOfWork.PurchasesReturns_BillService.GenerateSerial(BillModel.BranchId);
                var BillSaved = UnitOfWork.PurchasesReturns_BillService.Add(BillModel);
                UnitOfWork.Commit();
                obj.Data = BillSaved.ID;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/PurchasesReturns/GetLatestPrice")]
        [HttpGet]

        public IActionResult GetLatestPrice(int ItemId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.PurchaseItemService.GetLatestPrice(ItemId);
                UnitOfWork.Commit();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/PurchasesReturns/EditPurchasesReturns")]
        [HttpPost]
        public IActionResult EditPurchasesReturns(PurchasesReturns_BillVM BillModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                //////check if client changed for this bill
                PurchasesReturns_BillVM oldmodel = UnitOfWork.PurchasesReturns_BillService.Get(BillModel.ID);
                if (BillModel.SupplierId != oldmodel.SupplierId)
                {
                    int Id = UnitOfWork.TransSupplierService.RecalculateSupplierBalance((int)oldmodel.SupplierId, (int)BillModel.SerialNumber);
                    UnitOfWork.TransSupplierService.Delete(Id);
                }
                UnitOfWork.PurchasesReturns_BillService.Edit(BillModel);
                //delete old items
                UnitOfWork.PurchaseReturns_ItemsService.DeletePhysical(BillModel.SerialNumber.Value);
                UnitOfWork.PurchaseReturns_OtherIncomeService.DeletePhysical(BillModel.SerialNumber.Value);
                UnitOfWork.Commit();
                foreach (var items in BillModel.PurchaseItem)
                {
                    if (items != null)
                    {
                        if (items.ItemId != 0)
                        {
                            items.SerialNumber = BillModel.SerialNumber;
                            UnitOfWork.PurchaseReturns_ItemsService.Add(items);
                        }
                    }
                }
                foreach (var income in BillModel.PurchaseReturns_OtherIncome)
                {
                    if (income != null)
                    {
                        if (income.IncomeId != 0)
                        {
                            income.SerialNumber = BillModel.SerialNumber;
                            UnitOfWork.PurchaseReturns_OtherIncomeService.Add(income);
                        }
                    }
                }
                UnitOfWork.Commit();
                ///////////////////////////////////////////////////trans///////////////////////////////
                var currentBalance = UnitOfWork.TransSupplierService.GetLatestBalance((int)BillModel.SupplierId, BillModel.Date.ToLocalTime());
                var Balance = BillModel.FlagType ? (decimal)BillModel.TotalAfterTax : (decimal)BillModel.TotalAfterDiscount;
                decimal NewBalance =  currentBalance + Balance;
                TransSupplierVM TransSupplier = new TransSupplierVM
                {
                    SupplierId = BillModel.SupplierId,
                    SupplierType = TransSupplierTypeEnum.مردودات_مشتريات,
                    Credit = 0,
                    CreateDate = DateTime.Now,
                    OpenBalFlag = false,
                    Debit = Balance,
                    Date = BillModel.Date,
                    SerialNumber = BillModel.SerialNumber.Value,
                    IsDeleted = false,
                    Balance = NewBalance,
                };
                bool checkreturn = UnitOfWork.TransSupplierService.Add(TransSupplier, (decimal)Balance);
                UnitOfWork.Commit();
                //recacaluate
                if (!checkreturn)
                {
                    UnitOfWork.TransSupplierService.RecalculateSupplierBalance((int)BillModel.SupplierId, BillModel.SerialNumber.Value);

                }
                ///////////////////////////////////////////////////////////////////////////
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/PurchasesReturns/DeletePurchasesReturns")]
        [HttpGet]
        public IActionResult DeletePurchasesReturns(int ID, int SerialNumber, int SupplierId)
        {
            var obj = new ResponseVM();
            try
            {
                //int currobj;
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.PurchasesReturns_BillService.Delete(ID);
                UnitOfWork.PurchaseReturns_ItemsService.Delete(ID);
                UnitOfWork.PurchaseReturns_OtherIncomeService.Delete(ID);
                if (SupplierId != 0)
                {
                    int Id = UnitOfWork.TransSupplierService.RecalculateSupplierBalance(SupplierId, SerialNumber);
                    UnitOfWork.TransSupplierService.Delete(Id);
                }
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/PurchasesReturns/GenerateSerial")]
        [HttpGet]
        public ActionResult GenerateSerial(int BranchId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.PurchasesReturns_BillService.GenerateSerial(BranchId);
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }
        [Route("api/PurchasesReturns/GetListDDL")]
        [HttpGet]
        public ActionResult GetListddl()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                PurchasesReturns_BillVM Bill = new PurchasesReturns_BillVM
                {
                    StoreList = UnitOfWork.StoreServices.GetAll().ToList(),
                    SupplierList = UnitOfWork.SupplierService.GetSuppliers().ToList(),
                    BranchList = UnitOfWork.BranchServices.GetAll().ToList(),
                    DealTypeList = UnitOfWork.DealTypeService.GetAll().ToList(),
                    ItemList = UnitOfWork.ItemServices.GetItems().ToList(),
                    OtherIncomeList = UnitOfWork.OtherIncomeService.GetAll().ToList(),
                    SerialNumber = UnitOfWork.PurchasesReturns_BillService.GenerateSerial(0),
                    Date = DateTime.Now,
                    DiscountPrecentage = 0,
                    DiscountValue = 0,
                };
                obj.Data = Bill;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/Purchase/ConvertPurchaseReturnBills")]
        [HttpPost]
        public IActionResult ConvertPurchaseReturnBills(PurchasesReturns_BillVM BillModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                var addition = UnitOfWork.TempPaymentVoucherService.ConvertPurchasereturnToTemp(BillModel);
                UnitOfWork.Commit();
                foreach (var items in BillModel.PurchaseItem)
                {
                    if (items != null)
                    {
                        if (items.ItemId != 0)
                        {
                            items.SerialNumber = addition.SerialNumber;
                            UnitOfWork.TempPaymentVoucherItemService.ConvertPurchasesReturnItemToTempItem(items);
                        }
                    }
                }
                UnitOfWork.Commit();
                BillModel.IsConverted = true;
                UnitOfWork.PurchasesReturns_BillService.Edit(BillModel);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}