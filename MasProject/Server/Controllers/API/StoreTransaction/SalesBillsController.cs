﻿using MasProject.Data.DataAccess;
using MasProject.Domain;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using MasProject.Domain.ViewModel.StoreTransaction.Quotation;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Collections.Generic;
using static MasProject.Domain.Enums.Enums;
using MasProject.Domain.ViewModel.TransVM;
using System.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using AspNetCore.Reporting;
using System.ComponentModel;
using MasProject.Domain.ViewModel.Reports;

namespace MasProject.Server.Controllers.API.StoreTransaction
{

    public class SalesBillsController : BaseController
    {
        private IWebHostEnvironment _hostingEnvironment;
        private readonly IConfiguration _config;
        public SalesBillsController(DBContext context, IWebHostEnvironment hostingEnvironment, IConfiguration config) : base(context)
        {
            _hostingEnvironment = hostingEnvironment;
            _config = config;
        }
        [Route("api/SalesBills/GetSalesBills")]
        [HttpGet]
        public ActionResult GetSalesBills()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                BillsTbl<SalesBillsVM> BillsTbl = new BillsTbl<SalesBillsVM> { };
                SalesBillsVM SalesBill = new SalesBillsVM
                {
                    BranchList = UnitOfWork.BranchServices.GetAll().ToList(),
                    ClientList=UnitOfWork.ClientsService.GetClients().ToList(),
                };
                BillsTbl.SearchItems = SalesBill;
                BillsTbl.BillsList = UnitOfWork.SalesBillsService.GetAll().ToList();
                obj.Data = BillsTbl;
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }



        [Route("api/SalesBills/GetSalesByClient")]
        [HttpGet]
        public ActionResult GetSalesByClient(int ClientId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.SalesBillsService.GetAllByClient(ClientId).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/SalesBills/GetSalesByDate")]
        [HttpGet]
        public ActionResult GetSalesByDate(DateTime date)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.SalesBillsService.GetAllByDate(date).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/SalesBills/GetSalesByBranch")]
        [HttpGet]
        public ActionResult GetSalesByBranch(int BranchId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.SalesBillsService.GetAllByBranch(BranchId).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/SalesBills/GetPrice")]
        [HttpGet]
        public ActionResult GetPrice(int ClientId, int ItemId, int ThisSerial)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                var price = UnitOfWork.SalesBillsService.GetBillIdByClient(ClientId, ThisSerial, ItemId);
                obj.Data = price;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
     
        [Route("api/SalesBills/GetListDDL")]
        [HttpGet]
        public ActionResult GetListddl()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                SalesBillsVM SalesBill = new SalesBillsVM
                {
                    StoreList = UnitOfWork.StoreServices.GetAll().ToList(),
                    ClientList = UnitOfWork.ClientsService.GetClients().ToList(),
                    DealTypeList = UnitOfWork.DealTypeService.GetAll().ToList(),
                    ItemList = UnitOfWork.ItemServices.GetItems().ToList(),
                    sellingExpensesList = UnitOfWork.SellingExpensesService.GetAll().ToList(),
                    SerialNumber = UnitOfWork.SalesBillsService.GenerateSerial(0),
                    BranchList = UnitOfWork.BranchServices.GetAll().ToList(),
                    Date = DateTime.Now,
                    DiscountPrecentage = 0,
                    DiscountValue = 0,
                };
                obj.Data = SalesBill;
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }


        [Route("api/SalesBills/GetSpecificSalesBills")]
        [HttpGet]
        public ActionResult GetSpecificSalesBills(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                SalesBillsVM model = UnitOfWork.SalesBillsService.Get(Id);
                model.SalesBillStores = UnitOfWork.SalesBillStoresService.GetBySerial(model.SerialNumber.Value);
                model.TaxFileNumber = UnitOfWork.ClientsService.GetTaxFile(model.ClientId).ToString();
                model.ClientBalance = UnitOfWork.TransClientService.GetBalance(model.ClientId);
                int indexer = 0;
                foreach (var store in model.SalesBillStores)
                {
                    if (store != null)
                    {
                        if (store.StoreId != 0 & store.StoreId !=null)
                        {
                            int indexerItem = 0;
                            store.SalesItem = UnitOfWork.SalesItemsService.GetBySerial(model.SerialNumber.Value, (int)store.StoreId, store.Identifer);
                            foreach (var item in store.SalesItem)
                            {
                                if (item != null)
                                {
                                    if (item.ItemId != 0)
                                    {
                                        item.Identifer = indexerItem + 1;
                                        item.ItemImg = UnitOfWork.ItemServices.GetItemImage(item.ItemId).ItemImg;
                                        indexerItem++;
                                    }
                                }

                            }
                            if (store.Identifer == 0)
                            {
                                store.Identifer = indexer + 1;
                            }
                            indexer++;
                        }
                    }
                }
                model.SalesSellExpenses = UnitOfWork.SalesSellExpensesService.GetBySerial(model.SerialNumber.Value);
                indexer = 0;
                foreach (var item in model.SalesSellExpenses)
                {
                    if (item != null)
                    {
                        item.Identifer = indexer + 1;
                        indexer++;
                    }
                }
                model.StoreList = UnitOfWork.StoreServices.GetAll().ToList();
                model.ClientList = UnitOfWork.ClientsService.GetClients().ToList();
                model.DealTypeList = UnitOfWork.DealTypeService.GetAll().ToList();
                model.ItemList = UnitOfWork.ItemServices.GetItems().ToList();
                model.sellingExpensesList = UnitOfWork.SellingExpensesService.GetAll().ToList();
               model.BranchList = UnitOfWork.BranchServices.GetAll().ToList();

                obj.Data = model;
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }
        [Route("api/SalesBills/GetSalesBillsFromQuotation")]
        [HttpGet]
        public ActionResult GetSalesBillsFromQuotation(int QuotaionId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                QuotationVM model = UnitOfWork.QuotationService.Get(QuotaionId);
                model.QuotationItems = UnitOfWork.QuotationItemsService.GetBySerial(model.SerialNumber.Value);
                model.IsConverted = true;
                UnitOfWork.QuotationService.Edit(model);
                UnitOfWork.Commit();
                SalesBillsVM NewModel = new SalesBillsVM
                {
                    SerialNumber = UnitOfWork.SalesBillsService.GenerateSerial(0),
                    Date = model.Date,
                    StoreId = model.StoreId,
                    // ClientId=model.ClientId,
                    ClientBalance = model.ClientBalance,
                    DealTypeId = model.DealTypeId,
                    TaxFileNumber = model.TaxFileNumber.Value.ToString(),
                    FlagType = model.FlagType,
                    TotalPrice = model.TotalPrice,
                    DiscountPrecentage = model.DiscountPrecentage,
                    DiscountValue = model.DiscountValue,
                    TotalAfterDiscount = model.TotalAfterDiscount,
                    ShowNotesFlag = model.ShowNotesFlag,
                    TotalAfterTax = model.TotalAfterTax,
                    AddtionTax = model.AddtionTax,
                    AddtionTaxAmount = model.AddtionTaxAmount,
                    SourceDeduction = model.SourceDeduction,
                    SourceDeductionAmount = model.SourceDeductionAmount,
                    MultiStore = false,
                    BranchId = model.BranchId,
                    Note = model.Note,
                };
                NewModel.SalesBillStores = new List<SalesBillStoresVM>();
                SalesBillStoresVM StoreBill = new SalesBillStoresVM { Identifer = 1, StoreId = model.StoreId };

                StoreBill.SalesItem = new List<SalesItemsVM>();
                int indexer = 0;
                foreach (var item in model.QuotationItems)
                {
                    SalesItemsVM SalesItem = new SalesItemsVM
                    {
                        Identifer = indexer + 1,
                        Box_ParCode = item.Box_ParCode,
                        ItemId = item.ItemId,
                        Quantity = item.Quantity,
                        Price = item.Price,
                        Total = item.Total,
                        StoreId = model.StoreId,
                    };

                    indexer++;
                    StoreBill.SalesItem.Add(SalesItem);
                }
                NewModel.SalesBillStores.Add(StoreBill);
                obj.Data = NewModel;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/SalesBills/AddSalesBills")]
        [HttpPost]

        public IActionResult AddSalesBills(SalesBillsVM BillModel)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                BillModel.SerialNumber= UnitOfWork.SalesBillsService.GenerateSerial(BillModel.BranchId);
                var BillSaved = UnitOfWork.SalesBillsService.Add(BillModel);
                UnitOfWork.Commit();
                obj.Data = BillSaved.ID;
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }


        [Route("api/SalesBills/EditSalesBills")]
        [HttpPost]
        public IActionResult EditSalesBills(SalesBillsVM BillModel)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                //////check if client changed for this bill
                //if (BillModel.ID != 0)//is not a solution for situations
                //{
                    SalesBillsVM model = UnitOfWork.SalesBillsService.Get(BillModel.ID);
                    if (model.ClientId != BillModel.ClientId)
                    {
                        int Id = UnitOfWork.TransClientService.RecalculateClientBalance(model.ClientId, (int)model.SerialNumber);
                        UnitOfWork.TransClientService.Delete(Id);
                    }
              //  }
                
                if (BillModel.SalesBillStores.Count() == 1)
                {
                    BillModel.MultiStore = false;
                }
                else
                {
                    BillModel.MultiStore = true;
                }
                UnitOfWork.SalesBillsService.Edit(BillModel);
                //delete old items
                UnitOfWork.SalesItemsService.DeletePhysical(BillModel.SerialNumber.Value);
                UnitOfWork.SalesBillStoresService.DeletePhysical(BillModel.SerialNumber.Value);
                UnitOfWork.SalesSellExpensesService.DeletePhysical(BillModel.SerialNumber.Value);
                UnitOfWork.Commit();
                //foreach (var items in BillModel.SalesItem)
                //{
                //    if (items != null)
                //    {
                //        items.SerialNumber = BillModel.SerialNumber;
                //        UnitOfWork.SalesItemsService.Add(items);
                //    }
                //}
                foreach (var StoreItem in BillModel.SalesBillStores)
                {
                    if (StoreItem != null)
                    {
                        if (StoreItem.StoreId != 0 & StoreItem.StoreId != null)
                        {
                            StoreItem.SerialNumber = BillModel.SerialNumber;
                            UnitOfWork.SalesBillStoresService.Add(StoreItem);
                            foreach (var items in StoreItem.SalesItem)
                            {
                                if (items != null)
                                {
                                    if (items.ItemId != 0)
                                    {
                                        items.SerialNumber = BillModel.SerialNumber;
                                        items.StoreId = StoreItem.StoreId;
                                        items.Identifer = StoreItem.Identifer;//to take store idetifier and save it
                                        UnitOfWork.SalesItemsService.Add(items);
                                        UnitOfWork.Commit();
                                    }
                                }
                            }
                        }
                    }
                }
                foreach (var Expenses in BillModel.SalesSellExpenses)
                {
                    if (Expenses != null)
                    {
                        if (Expenses.ExpensesId != 0 & Expenses.ExpensesId != null)
                        {
                            Expenses.SerialNumber = BillModel.SerialNumber;
                            UnitOfWork.SalesSellExpensesService.Add(Expenses);
                        }
                    }
                }
                UnitOfWork.Commit();
                ///////////////////////////////////////////////////trans///////////////////////////////
                var currentBalance = UnitOfWork.TransClientService.GetLatestBalance((int)BillModel.ClientId, BillModel.Date.ToLocalTime());
                var Balance = BillModel.FlagType ? (decimal)BillModel.TotalAfterTax : (decimal)BillModel.TotalAfterDiscount;
                decimal NewBalance = currentBalance + Balance;
                TransClientVM TransClient = new TransClientVM
                {
                    ClientId = BillModel.ClientId,
                    ClientType = TransClientTypeEnum.مبيعات,
                    Credit = 0,
                    CreateDate = DateTime.Now,
                    OpenBalFlag = false,
                    Debit = Balance,
                    Date = BillModel.Date,
                    SerialNumber = BillModel.SerialNumber.Value,
                    IsDeleted = false,
                    Balance = NewBalance,
                };
                bool checkreturn = UnitOfWork.TransClientService.Add(TransClient, Balance);
                UnitOfWork.Commit();
                //recacaluate
                if (!checkreturn)
                {
                    UnitOfWork.TransClientService.RecalculateClientBalance(BillModel.ClientId, (int)BillModel.SerialNumber);
                }
                ///////////////////////////////////////////////////////////////////////////
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }
        [Route("api/SalesBills/DeleteSalesBills")]
        [HttpGet]
        public IActionResult DeleteSalesBills(int ID)//, int SerialNumber, int ClientId)//by serialNumber
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                SalesBillsVM DeletedModel = UnitOfWork.SalesBillsService.Get(ID);
                UnitOfWork.SalesBillsService.Delete(ID);
                UnitOfWork.SalesBillStoresService.Delete(ID);
                UnitOfWork.SalesItemsService.Delete(ID);
                UnitOfWork.SalesSellExpensesService.Delete(ID);

                //add trans later
                if (DeletedModel.ClientId != 0   && DeletedModel.SerialNumber != null && DeletedModel.SerialNumber != 0)
                {
                    int Id = UnitOfWork.TransClientService.RecalculateClientBalance(DeletedModel.ClientId, (int)DeletedModel.SerialNumber);
                    UnitOfWork.TransClientService.Delete(Id);
                }
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)

            {
                string message = ex.Message;
                return BadRequest();
            }
        }
        [Route("api/SalesBills/ConvertSalesBills")]
        [HttpPost]
        public IActionResult ConvertSalesBills(SalesBillsVM BillModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                ////////////////////////////////////////////////////////////
                //--------edited 17-07-2021 ---- only insert one time
                if (!BillModel.IsConverted)
                {
                    List<SalesBillStoresVM> BillStoresList = UnitOfWork.SalesBillStoresService.GetBySerial(BillModel.SerialNumber.Value);
                    if (BillStoresList != null && BillStoresList.Count > 0)
                    {
                        foreach (var BillStore in BillStoresList)
                        {
                            if (BillStore != null)
                            {
                                if (BillStore.StoreId != 0 & BillStore.StoreId != null)
                                {
                                    BillModel.StoreId = BillStore.StoreId;
                                    BillStore.SalesItem = UnitOfWork.SalesItemsService.GetBySerial(BillModel.SerialNumber.Value, (int)BillStore.StoreId, BillStore.Identifer);

                                    var payment = UnitOfWork.TempPaymentVoucherService.ConvertSalesToTemp(BillModel);
                                    UnitOfWork.Commit();
                                    foreach (var items in BillStore.SalesItem)
                                    {
                                        if (items != null)
                                        {
                                            if (items.ItemId != 0)
                                            {
                                                items.SerialNumber = payment.SerialNumber;
                                                UnitOfWork.TempPaymentVoucherItemService.ConvertSalesItemToTempItem(items);
                                            }

                                        }
                                    }

                                }
                            }
                        }
                    }
                }
                ////////////////////////////////////////////////////////////
                UnitOfWork.Commit();
                BillModel.IsConverted = true;
                UnitOfWork.SalesBillsService.Edit(BillModel);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/SalesBills/GenerateSerial")]
        [HttpGet]
        public ActionResult GenerateSerial(int BranchId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.SalesBillsService.GenerateSerial(BranchId);
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }

        //[Route("api/SalesBills/UpdateSerial")]
        //[HttpGet]
        //public ActionResult UpdateSerial()
        //{
        //    var obj = new ResponseVM();
        //    try
        //    {
        //        obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
        //        obj.Message = ApiStatus.Success;
        //        List<SalesBillsVM> ListOFBills = UnitOfWork.SalesBillsService.GetAllForEdit().ToList();
        //        //set client id into list
        //        List<int> clientsList = ListOFBills.Select(c => c.ClientId).Distinct().ToList();

        //        foreach (var NotBillModel in ListOFBills)
        //        {
        //            SalesBillsVM BillModel = UnitOfWork.SalesBillsService.Get(NotBillModel.ID);
        //            int NewSerial = UnitOfWork.SalesBillsService.GenerateSerialByBranch(BillModel.BranchId);
        //            if(NewSerial != BillModel.SerialNumber)
        //            {
        //                string message = "serial exist for other bill";
        //                //update other serial
        //                int OtherBillID = UnitOfWork.SalesBillsService.GetId(NewSerial);
        //                if (OtherBillID != 0)
        //                {
        //                    SalesBillsVM OtherBillModel = UnitOfWork.SalesBillsService.Get(OtherBillID);
        //                    int NotNewSerial = NewSerial + 1000;
        //                    UpdateBills(OtherBillModel, NotNewSerial);
        //                    UnitOfWork.Commit();
        //                }
        //            }
        //            UpdateBills(BillModel, NewSerial);//already updated--no must to update name 
        //            UnitOfWork.Commit();
        //        }
        //        //must update clients balance
        //        foreach (var ClientId in clientsList)
        //        {
        //            UnitOfWork.TransClientService.UpdateClientBalance(ClientId);
        //        }
        //        UnitOfWork.Commit();
        //        return Ok(obj);
        //    }
        //    catch (Exception ex)
        //    {
        //        string message = ex.Message;
        //        return BadRequest();
        //    }
        //}

        //void UpdateBills(SalesBillsVM BillModel,int NewSerial)
        //{
        //    BillModel.SalesBillStores = UnitOfWork.SalesBillStoresService.GetBySerial(BillModel.SerialNumber.Value);
        //    foreach (var StoreItem in BillModel.SalesBillStores)
        //    {
        //        if (StoreItem != null)
        //        {
        //            if (StoreItem.StoreId != 0 & StoreItem.StoreId != null)
        //            {
        //                try
        //                {
        //                    StoreItem.SerialNumber = NewSerial;
        //                    StoreItem.SalesItem = UnitOfWork.SalesItemsService.GetBySerial(BillModel.SerialNumber.Value, (int)StoreItem.StoreId, StoreItem.Identifer);
        //                    UnitOfWork.SalesBillStoresService.Edit(StoreItem);
        //                    foreach (var items in StoreItem.SalesItem)
        //                    {
        //                        if (items != null)
        //                        {
        //                            if (items.ItemId != 0)
        //                            {
        //                                try
        //                                {
        //                                    items.SerialNumber = NewSerial;
        //                                    items.StoreId = StoreItem.StoreId;
        //                                    items.Identifer = StoreItem.Identifer;
        //                                    UnitOfWork.SalesItemsService.Edit(items);
        //                                }
        //                                catch (Exception ex)
        //                                {
        //                                    string message = ex.Message;
        //                                    //return BadRequest();
        //                                }

        //                            }
        //                        }
        //                    }
        //                }
        //                catch (Exception ex)
        //                {
        //                    string message = ex.Message;
        //                    //return BadRequest();
        //                }

        //            }
        //        }
        //    }
        //    BillModel.SalesSellExpenses = UnitOfWork.SalesSellExpensesService.GetBySerial(BillModel.SerialNumber.Value);
        //    foreach (var Expenses in BillModel.SalesSellExpenses)
        //    {
        //        if (Expenses != null)
        //        {
        //            if (Expenses.ExpensesId != 0 & Expenses.ExpensesId != null)
        //            {
        //                Expenses.SerialNumber = NewSerial;
        //                UnitOfWork.SalesSellExpensesService.Edit(Expenses);
        //            }
        //        }
        //    }
        //    //trans is missing //think about it
        //    TransClientVM trans = UnitOfWork.TransClientService.GetForEdit(BillModel.ClientId, BillModel.SerialNumber.Value);
        //    if (trans != null)
        //    {
        //        trans.SerialNumber = NewSerial;
        //        UnitOfWork.TransClientService.EditForEdit(trans);
        //    }
        //    BillModel.SerialNumber = NewSerial;
        //    UnitOfWork.SalesBillsService.Edit(BillModel);
        //}


        [Route("api/SalesBill/ConfirmSalesBills")]
        [HttpPost]
        public IActionResult ConfirmPurchasesBill(SalesBillsVM BillModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                SalesBillsVM oldmodel = UnitOfWork.SalesBillsService.Get(BillModel.ID);
                oldmodel.ApproveId = 1;
                UnitOfWork.SalesBillsService.Edit(oldmodel);

                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }


        [Route("api/SalesBill/ReviewSalesBills")]
        [HttpPost]
        public IActionResult ReviewSalesBill(SalesBillsVM BillModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                SalesBillsVM oldmodel = UnitOfWork.SalesBillsService.Get(BillModel.ID);
                oldmodel.RevewId = 1;
                UnitOfWork.SalesBillsService.Edit(oldmodel);

                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }
        

        [HttpGet]
        [Route("api/SalesBill/SalesBillsReport")]
        public IActionResult SalesBillsReport(int Id,int? StoreIdentifier)
        {
            try
            {
                var dt = new DataTable();
                SalesBillsVM model = UnitOfWork.SalesBillsService.Get(Id);
                model.SalesBillStores = UnitOfWork.SalesBillStoresService.GetBySerial(model.SerialNumber.Value);
                //hna bn7ot l table l asasy bta3 l fatora
                List<SalesBillsVM> modelList = new List<SalesBillsVM>();
                modelList.Add(model);
                dt = ToDataTable<SalesBillsVM>(modelList);
                int indexer = 0;
                //new
                List<SalesItemsReport> ItemWithStores = new List<SalesItemsReport> { };
                foreach (var store in model.SalesBillStores)
                {
                    if (store != null)
                    {
                        if (store.StoreId != 0 & store.StoreId != null)
                        {
                            int indexerItem = 0;
                            store.SalesItem = UnitOfWork.SalesItemsService.GetBySerial(model.SerialNumber.Value, (int)store.StoreId, store.Identifer);
                            foreach (var item in store.SalesItem)
                            {
                               
                                if (item != null)
                                {
                                    if (item.ItemId != 0)
                                    {
                                        item.Identifer = indexerItem + 1;                                       
                                        string StoreName = UnitOfWork.StoreServices.Get((int)store.StoreId).Name;
                                        SalesItemsReport NewObj = new SalesItemsReport
                                        {
                                            Box_ParCode = item.Box_ParCode,
                                            Identifer = item.Identifer,
                                            ItemName = item.ItemName,
                                            Price = item.Price,
                                            Quantity = item.Quantity,
                                            StoreId = store.ID,
                                            Total = item.Total,
                                            StoreName= StoreName,
                                        };
                                        ItemWithStores.Add(NewObj);
                                        indexerItem++;
                                    }
                                }
                               
                            }
                            if (store.Identifer == 0)
                            {
                                store.Identifer = indexer + 1;
                            }
                            indexer++;
                        }
                    }
                }
                var dtStores = new DataTable();
              //  List<SalesBillStoresVM> modelListStores = new List<SalesBillStoresVM>();
                //modelListStores.AddRange(model.SalesBillStores);
                dtStores = ToDataTable<SalesItemsReport>(ItemWithStores);

              

                string mimetype = "";
                int extension = 1;
                if (string.IsNullOrWhiteSpace(_hostingEnvironment.WebRootPath))
                {
                    // _hostingEnvironment.WebRootPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot");
                    string RootPath = _config.GetValue<string>("RootPath");
                    _hostingEnvironment.WebRootPath = $"" + RootPath;
                }
                var path = $"{this._hostingEnvironment.WebRootPath}\\Reports\\SalesBill.rdlc";
                string BranchName = UnitOfWork.BranchServices.Get((int)model.BranchId).Name;
                string ClientName = UnitOfWork.ClientsService.Get(model.ClientId).Name;
                string Footer = " تاريخ الطباعه : " + DateTime.Now.Date.ToString("dd-MM-yyyy") + " / الساعه: " + DateTime.Now.TimeOfDay.Hours + ":" + DateTime.Now.TimeOfDay.Minutes;// +":"+ DateTime.Now.TimeOfDay.Seconds ;
                NumberToAlphabetic Obj = new NumberToAlphabetic();
                Obj.ConvertNumbersToArabicAlphabet(model.TotalAfterTax.ToString());
                string TotalString = Obj.GetNumberAr();
                Dictionary<string, string> paramters = new Dictionary<string, string>();

                paramters.Add("ClientName", ClientName);
                paramters.Add("FooterString", Footer);
                paramters.Add("TotalAlphbetic", TotalString);
                paramters.Add("BranchName", BranchName);
                LocalReport localreport = new LocalReport(path);
                localreport.AddDataSource("SalesMasDataSet", dt);
                localreport.AddDataSource("SalesMasDataSet1", dtStores);
                var result = localreport.Execute(RenderType.Pdf, extension, paramters, mimetype);
                return File(result.MainStream, "application/pdf");
                //var result2 = localreport.Execute(RenderType.Excel, extension, paramters, mimetype);
                //return File(result.MainStream, "application/Excel");
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return null;
            }

        }
        public DataTable ToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }
    }
}