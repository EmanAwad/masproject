﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.StoreTransaction.Transfer;
using MasProject.Data.DataAccess;
using Microsoft.AspNetCore.Mvc;


namespace MasProject.Server.Controllers.API.StoreTransaction
{
    public class TransferRequestController : BaseController
    {
        public TransferRequestController(DBContext context) : base(context)
        {
        }
        [Route("api/TransferRequest/GetListsOfDDl")]
        [HttpGet]
        public ActionResult GetListsOfDDl()
        {
            try
            {
                var obj = new ResponseVM();
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                TransferRequestVM TransferRequest = new TransferRequestVM
                {
                    BranchList = UnitOfWork.BranchServices.GetAll().ToList(),
                    ItemList = UnitOfWork.ItemServices.GetItems().ToList(),
                    SerialNumber = UnitOfWork.TransferRequestService.GenerateSerial(),
                    Date = DateTime.Now.ToLocalTime()
                };
                obj.Data = TransferRequest;
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [Route("api/TransferRequest/GetAllByBranch")]
        [HttpGet]
        public ActionResult GetAllByBranch(int BranchId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.TransferRequestService.GetAllByBranch(BranchId).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/TransferRequest/GetTransferRequest")]
        [HttpGet]
        public ActionResult GetTransferRequest()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.TransferRequestService.GetAll().ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/TransferRequest/GetSpecificTransferRequest")]
        [HttpGet]
        public ActionResult GetSpecificTransferRequest(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                TransferRequestVM model = UnitOfWork.TransferRequestService.Get(Id);
                model.TransferRequestItemList = UnitOfWork.TransferRequestItemService.GetByTransID(model.ID);
                foreach (var itm in model.TransferRequestItemList)
                {
                    if (itm != null)
                    {
                        if (itm.ItemId != 0 & itm.ItemId != null)
                        {
                            var item = UnitOfWork.ItemStoreServices.GetBranchAndMianStoreQuantity(itm.ItemId.Value, model.BranchToID.Value);
                            itm.BranchToQuantity = item.BranchToQuantity;
                            itm.SelectedBranchQuantity = UnitOfWork.ItemStoreServices.GetItemQuantityByBranch(itm.ItemId.Value, itm.BranchFromID.Value);
                            itm.MainStoreQuantity = item.MainStoreQuantity;
                        }
                    }
                }
                obj.Data = model;
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/TransferRequest/GetTransferRequestAsOrder")]
        [HttpGet]
        public ActionResult GetTransferRequestAsOrder(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                TransferOrderVM model = UnitOfWork.TransferRequestService.GetTransferRequestAsOrder(Id);
                model.TransferOrderDetailsList = UnitOfWork.TransferRequestItemService.GetByTransOrderID(model.ID);
                foreach (var itm in model.TransferOrderDetailsList)
                {
                    if (itm != null)
                    {
                        if (itm.ItemId != 0 & itm.ItemId != null)
                        {
                            var item = UnitOfWork.ItemStoreServices.GetBranchAndMianStoreQuantity(itm.ItemId.Value, model.BranchToID.Value);
                            itm.BranchToQuantity = item.BranchToQuantity;
                            itm.SelectedBranchQuantity = UnitOfWork.ItemStoreServices.GetItemQuantityByBranch(itm.ItemId.Value, itm.BranchFromID.Value);
                            itm.MainStoreQuantity = item.MainStoreQuantity;
                        }
                    }
                }
                obj.Data = model;
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/TransferRequest/AddTransferRequest")]
        [HttpPost]
        public IActionResult AddTransferRequest(TransferRequestVM TransRequestModel)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                var TransRequestSaved = UnitOfWork.TransferRequestService.Add(TransRequestModel);
                UnitOfWork.Commit();
                obj.Data = TransRequestSaved.ID;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/TransferRequest/EditTransferRequest")]
        [HttpPost]
        public IActionResult EditTransferRequest(TransferRequestVM TransRequestModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                //edit 21-09-2020
                //TransRequestModel.TransferRequestItemList = UnitOfWork.TransferRequestItemService.GetByTransID(TransRequestModel.ID);
                UnitOfWork.TransferRequestService.Edit(TransRequestModel);
                //delete old items
                UnitOfWork.TransferRequestItemService.DeletePhysical(TransRequestModel.ID);
                UnitOfWork.Commit();
                foreach (var items in TransRequestModel.TransferRequestItemList)
                {
                    if (items != null)
                    {
                        if (items.ItemId != 0 & items.ItemId != null)
                        {
                            items.TransferRequestId = TransRequestModel.ID;
                            UnitOfWork.TransferRequestItemService.Add(items);
                        }
                    }
                }
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }
        [Route("api/TransferRequest/DeleteTransferRequest")]
        [HttpGet]
        public IActionResult DeleteTransferRequest(int ID)//by serialNumber
        {

            var obj = new ResponseVM();
            try
            {
                //int currobj;
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.TransferRequestService.Delete(ID);
                UnitOfWork.TransferRequestItemService.Delete(ID);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}