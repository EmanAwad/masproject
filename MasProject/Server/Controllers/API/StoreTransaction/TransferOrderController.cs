﻿using System;
using System.Linq;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.StoreTransaction.Transfer;
using MasProject.Data.DataAccess;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace MasProject.Server.Controllers.API.StoreTransaction
{
    public class TransferOrderController : BaseController
    {
        public TransferOrderController(DBContext context) : base(context)
        {
        }
        [Route("api/TransferOrder/GetListsOfDDl")]
        [HttpGet]
        public ActionResult GetListsOfDDl()
        {
            try
            {
                var obj = new ResponseVM();
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                List<TransferRequestAndOrder> TransferList = new List<TransferRequestAndOrder>();
                TransferList = UnitOfWork.TransferRequestService.GetTransferRequest();
                TransferList.AddRange(UnitOfWork.TransferOrderService.GetTransferOrder());
                TransferOrderVM TransferOrder = new TransferOrderVM
                {
                    BranchList = UnitOfWork.BranchServices.GetAll().ToList(),
                    ItemList = UnitOfWork.ItemServices.GetItems().ToList(),
                    TransferList = TransferList.OrderByDescending(x => x.Date).ToList(),
                    SerialNumber = UnitOfWork.TransferOrderService.GenerateSerial(),
                    Date = DateTime.Now.ToLocalTime()
                };
                obj.Data = TransferOrder;
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        [Route("api/TransferOrder/GetAllByBranch")]
        [HttpGet]
        public ActionResult GetAllByBranch(int BranchId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                //obj.Data = UnitOfWork.TransferOrderService.GetAllByBranch(BranchId).ToList();
                List<TransferRequestAndOrder> TransferList = new List<TransferRequestAndOrder>();
                TransferList = UnitOfWork.TransferRequestService.GetTransferRequestByBranch(BranchId);
                TransferList.AddRange(UnitOfWork.TransferOrderService.GetTransferOrderByBranch(BranchId));
                TransferOrderVM TransferOrder = new TransferOrderVM
                {
                    TransferList = TransferList.OrderByDescending(x => x.Date).ToList(),
                };
                obj.Data = TransferOrder;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/TransferOrder/GetTransferOrder")]
        [HttpGet]
        public ActionResult GetTransferOrder()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.TransferOrderService.GetAll().ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/TransferOrder/GetSpecificTransferOrder")]
        [HttpGet]
        public ActionResult GetSpecificTransferOrder(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                TransferOrderVM model = UnitOfWork.TransferOrderService.Get(Id);
                model.TransferOrderDetailsList = UnitOfWork.TransferOrderDetailsService.GetByTransferOrderID(model.ID);
                foreach (var itm in model.TransferOrderDetailsList)
                {
                    if (itm != null)
                    {
                        if (itm.ItemId != 0 & itm.ItemId != null)
                        {
                            var item = UnitOfWork.ItemStoreServices.GetBranchAndMianStoreQuantity(itm.ItemId.Value, model.BranchToID.Value);
                            itm.BranchToQuantity = item.BranchToQuantity;
                            itm.SelectedBranchQuantity = UnitOfWork.ItemStoreServices.GetItemQuantityByBranch(itm.ItemId.Value, itm.BranchFromID.Value);
                            itm.MainStoreQuantity = item.MainStoreQuantity;
                        }
                    }
                }
                obj.Data = model;
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/TransferOrder/AddTransferOrder")]
        [HttpPost]
        public IActionResult AddTransferOrder(TransferOrderVM TransferOrderModel)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                var TransferOrderSaved = UnitOfWork.TransferOrderService.Add(TransferOrderModel);
                UnitOfWork.Commit();
                obj.Data = TransferOrderSaved.ID;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/TransferOrder/EditTransferOrder")]
        [HttpPost]
        public IActionResult EditTransferOrder(TransferOrderVM TransferOrderModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                //edit 21-09-2020
                UnitOfWork.TransferOrderService.Edit(TransferOrderModel);
                //delete old items
                UnitOfWork.TransferOrderDetailsService.DeletePhysical(TransferOrderModel.ID);
                UnitOfWork.Commit();
                foreach (var items in TransferOrderModel.TransferOrderDetailsList)
                {
                    if (items != null)
                    {
                        if (items.ItemId != 0 & items.ItemId != null)
                        {
                            items.TransferOrderId = TransferOrderModel.ID;
                            UnitOfWork.TransferOrderDetailsService.Add(items);
                        }
                    }
                }
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }
        [Route("api/TransferOrder/ConvertTransferRequest")]
        [HttpPost]
        public IActionResult ConvertTransferRequest(TransferRequestVM TransferRequestModel)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                var TransferOrderSaved = UnitOfWork.TransferOrderService.ConvertTransferRequest(TransferRequestModel);
                UnitOfWork.Commit();
                UnitOfWork.TransferOrderDetailsService.ConvertTransferRequestItem(TransferOrderSaved.ID, TransferRequestModel.TransferRequestItemList);
                var TransferRequest = UnitOfWork.TransferRequestService.Get(TransferRequestModel.ID);
                TransferRequest.IsOrder = true;
                UnitOfWork.TransferRequestService.Edit(TransferRequest);
                UnitOfWork.Commit();
                obj.Data = TransferOrderSaved.ID;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/TransferOrder/AddTransferOrderFromRequest")]
        [HttpPost]
        public IActionResult AddTransferOrderFromRequest(TransferOrderVM Trans)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                var TransferRequestModel = UnitOfWork.TransferRequestService.Get(Trans.TransferRequestId.Value);
                var TransferOrderSaved = UnitOfWork.TransferOrderService.ConvertTransferRequest(TransferRequestModel);
                UnitOfWork.Commit();
                foreach (var itm in Trans.TransferOrderDetailsList)
                {
                    if (itm != null)
                    {
                        if (itm.ItemId != 0 & itm.ItemId != null)
                        {
                            itm.TransferOrderId = TransferOrderSaved.ID;
                            UnitOfWork.TransferOrderDetailsService.Add(itm);
                        }
                    }
                }
                var TransferRequest = UnitOfWork.TransferRequestService.Get(Trans.TransferRequestId.Value);
                TransferRequest.IsOrder = true;
                UnitOfWork.TransferRequestService.Edit(TransferRequest);
                UnitOfWork.Commit();
                obj.Data = TransferOrderSaved.ID;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/TransferOrder/DeleteTransferOrder")]
        [HttpGet]
        public IActionResult DeleteTransferOrder(int ID)//by serialNumber
        {

            var obj = new ResponseVM();
            try
            {
                //int currobj;
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.TransferOrderService.Delete(ID);
                UnitOfWork.TransferOrderDetailsService.Delete(ID);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/TransferOrder/ConvertTransferOrder")]
        [HttpPost]
        public IActionResult ConvertTransferOrder(TransferOrderVM TransModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                List<TransferOrderDetailsVM> TransDetailsList = UnitOfWork.TransferOrderDetailsService.GetByTransferOrderID(TransModel.ID);
                if (TransDetailsList != null && TransDetailsList.Count > 0)
                {
                    foreach (var Detail in TransDetailsList)
                    {
                        if (Detail != null)
                        {
                            if (Detail.BranchFromID != 0 & Detail.BranchFromID != null)
                            {
                                Detail.StoreId = UnitOfWork.StoreServices.GetSoreByBranch(Detail.BranchFromID.Value);
                                var payment = UnitOfWork.TempPaymentVoucherService.ConvertTransOrderToTemp(Detail);
                                UnitOfWork.TempPaymentVoucherItemService.ConvertTransOrderItemToTempItem(Detail);
                            }
                        }
                    }
                }
                UnitOfWork.Commit();
                TransModel.IsConverted = true;
                UnitOfWork.TransferOrderService.Edit(TransModel);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}