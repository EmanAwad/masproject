﻿using MasProject.Data.DataAccess;
using MasProject.Domain;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using MasProject.Domain.ViewModel.StoreTransaction.ExecutionWithDrawals;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using System.Linq;
namespace MasProject.Server.Controllers.API.StoreTransaction
{
  
    public class ExecutionWithDrawalsController : BaseController
    {
        public ExecutionWithDrawalsController(DBContext context) : base(context)
        {
        }
        [Route("api/ExecutionWithDrawals/GenerateSerial")]
        [HttpGet]
        public ActionResult GenerateSerial(int BranchId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ExecutionWithDrawals_BillService.GenerateSerial(BranchId);
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/ExecutionWithDrawals/GetAllByBranch")]
        [HttpGet]
        public ActionResult GetAllByBranch(int BranchId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ExecutionWithDrawals_BillService.GetAllByBranch(BranchId).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/ExecutionWithDrawals/GetListDDL")]
        [HttpGet]
        public ActionResult GetListddl()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                ExecutionithDrawals_BillVM SalesBill = new ExecutionithDrawals_BillVM
                {
                    StoreList = UnitOfWork.StoreServices.GetAll().ToList(),
                    DealTypeList = UnitOfWork.DealTypeService.GetAll().ToList(),
                    ItemList = UnitOfWork.ItemServices.GetItems().ToList(),
                    SerialNumber = UnitOfWork.ExecutionWithDrawals_BillService.GenerateSerial(0),
                    BranchList = UnitOfWork.BranchServices.GetAll().ToList(),
                    Date = DateTime.Now,
                    DiscountPrecentage = 0,
                    DiscountValue = 0,
                };
                obj.Data = SalesBill;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/ExecutionWithDrawals/GetExecutionWithDrawals")]
        [HttpGet]
        public ActionResult GetExecutionWithDrawals()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ExecutionWithDrawals_BillService.GetAll().ToList();
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [Route("api/ExecutionWithDrawals/GetSpecificExecutionWithDrawal")]
        [HttpGet]
        public ActionResult GetSpecificExecutionWithDrawal(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                ExecutionithDrawals_BillVM model = UnitOfWork.ExecutionWithDrawals_BillService.Get(Id);
                model.BillStores = UnitOfWork.ExecutionWithdrawl_BillStoresService.GetBySerial(model.SerialNumber.Value);
                // model.ExecutionItems = UnitOfWork.ExecutionWithDrawals_ItemsService.GetBySerial(model.SerialNumber.Value);
                int indexer = 0;
                foreach (var store in model.BillStores)
                {
                    if (store != null)
                    {
                        if (store.StoreId != 0 & store.StoreId != null)
                        {
                            store.Identifer = indexer + 1;
                            indexer++;
                            int indexerItem = 0;
                            store.ExItem = UnitOfWork.ExecutionWithDrawals_ItemsService.GetBySerial(model.SerialNumber.Value, (int)store.StoreId);
                            foreach (var item in store.ExItem)
                            {
                                if (item != null)
                                {
                                    if (item.ItemId != 0)
                                    {
                                        item.Identifer = indexerItem + 1;
                                        item.ItemImg = UnitOfWork.ItemServices.GetItemImage(item.ItemId).ItemImg;
                                        indexerItem++;
                                    }
                                }

                            }
                        }
                    }
                }
                obj.Data = model;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/ExecutionWithDrawals/AddExecutionWithDrawals")]
        [HttpPost]

        public IActionResult AddExecutionWithDrawals(ExecutionithDrawals_BillVM BillModel)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                BillModel.SerialNumber = UnitOfWork.ExecutionWithDrawals_BillService.GenerateSerial(BillModel.BranchId);
                var BillSaved = UnitOfWork.ExecutionWithDrawals_BillService.Add(BillModel);
                UnitOfWork.Commit();
                obj.Data = BillSaved.ID;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/ExecutionWithDrawals/EditExecutionWithDrawals")]
        [HttpPost]
        public IActionResult EditExecutionWithDrawals(ExecutionithDrawals_BillVM BillModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.ExecutionWithDrawals_BillService.Edit(BillModel);
                //delete old items
                UnitOfWork.ExecutionWithDrawals_ItemsService.DeletePhysical(BillModel.SerialNumber.Value);
                UnitOfWork.ExecutionWithdrawl_BillStoresService.DeletePhysical(BillModel.SerialNumber.Value);
                UnitOfWork.Commit();
                foreach (var StoreItem in BillModel.BillStores)
                {
                    if (StoreItem != null)
                    {
                        if (StoreItem.StoreId != 0 & StoreItem.StoreId != null)
                        {
                            StoreItem.SerialNumber = BillModel.SerialNumber;
                            UnitOfWork.ExecutionWithdrawl_BillStoresService.Add(StoreItem);
                            foreach (var items in StoreItem.ExItem)
                            {
                                if (items != null)
                                {
                                    if (items.ItemId != 0)
                                    {
                                        items.SerialNumber = BillModel.SerialNumber;
                                        items.StoreId = StoreItem.StoreId;
                                        UnitOfWork.ExecutionWithDrawals_ItemsService.Add(items);
                                    }
                                }
                            }
                        }
                    }
                }
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/ExecutionWithDrawals/DeleteExecutionWithDrawals")]
        [HttpGet]
        public IActionResult DeleteExecutionWithDrawals(int ID)
        {
            var obj = new ResponseVM();
            try
            {
                //int currobj;
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.ExecutionWithDrawals_BillService.Delete(ID);
                UnitOfWork.ExecutionWithDrawals_ItemsService.Delete(ID);
                UnitOfWork.ExecutionWithdrawl_BillStoresService.Delete(ID);

                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/ExecutionWithDrawals/ConvertExecutionWithDrawals")]
        [HttpPost]
        public IActionResult ConvertExecutionWithDrawals(ExecutionithDrawals_BillVM BillModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                List<ExecutionWithdrawl_BillStoresVM> BillStoresList = UnitOfWork.ExecutionWithdrawl_BillStoresService.GetBySerial(BillModel.SerialNumber.Value);
                if (BillStoresList != null && BillStoresList.Count > 0)
                {
                    foreach (var BillStore in BillStoresList)
                    {
                        if (BillStore != null)
                        {
                            if (BillStore.StoreId != 0 & BillStore.StoreId != null)
                            {
                                BillModel.StoreId = BillStore.StoreId;
                                BillStore.ExItem = UnitOfWork.ExecutionWithDrawals_ItemsService.GetBySerial(BillModel.SerialNumber.Value, (int)BillStore.StoreId);
                                var payment = UnitOfWork.TempPaymentVoucherService.ConvertExecutionWithdrawlToTemp(BillModel);
                                UnitOfWork.Commit();
                                foreach (var items in BillStore.ExItem)
                                {
                                    if (items != null)
                                    {
                                        if (items.ItemId != 0)
                                        {
                                            items.SerialNumber = payment.SerialNumber;
                                            UnitOfWork.TempPaymentVoucherItemService.ConvertExecutionWithdrawlItemToTempItem(items);
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                UnitOfWork.Commit();
                BillModel.IsConverted = true;
                UnitOfWork.ExecutionWithDrawals_BillService.Edit(BillModel);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}
