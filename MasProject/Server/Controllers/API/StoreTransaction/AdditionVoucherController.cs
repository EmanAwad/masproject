﻿using MasProject.Data.DataAccess;
using MasProject.Domain;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.StoreTransaction.Voucher;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Collections.Generic;
using MasProject.Domain.ViewModel.TransVM;
using static MasProject.Domain.Enums.Enums;

namespace MasProject.Server.Controllers.API.StoreTransaction
{
    public class AdditionVoucherController : BaseController
    {
        public AdditionVoucherController(DBContext context) : base(context)
        {
        }
        [Route("api/AdditionVoucher/GetAdditionVoucherByUserType")]
        [HttpGet]
        public ActionResult GetAdditionVoucherByUserType(int UserType)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.AdditionVoucherService.GetAllByUserType(UserType).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/AdditionVoucher/GetAdditionVoucherByStore")]
        [HttpGet]
        public ActionResult GetAdditionVoucherByStore(int StoreId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.AdditionVoucherService.GetAllByStore(StoreId).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/AdditionVoucher/GetAdditionVoucher")]
        [HttpGet]
        public ActionResult GetAdditionVoucher()
        {
            var obj = new ResponseVM();
            try
            {

                //List<AdditionVoucherVM> AdditionVoucherList = new List<AdditionVoucherVM>();
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                BillsTbl<AdditionVoucherVM> BillsTbl = new BillsTbl<AdditionVoucherVM> { };
                List<LookupKeyValueVM> LookupKeyValue = new List<LookupKeyValueVM>();
                LookupKeyValue.AddRange(UnitOfWork.ClientsService.GetClients().ToList());
                LookupKeyValue.AddRange(UnitOfWork.SupplierService.GetSuppliers().ToList());
                LookupKeyValue.AddRange(UnitOfWork.EmployeeService.GetEmployees().ToList());
                AdditionVoucherVM AdditionVoucher = new AdditionVoucherVM
                {
                    StoreList = UnitOfWork.StoreServices.GetAll().ToList(),
                    Client_Supplier_List = LookupKeyValue.OrderBy(x => x.Name).ToList(),
                };
                BillsTbl.SearchItems = AdditionVoucher;
                BillsTbl.BillsList = UnitOfWork.AdditionVoucherService.GetAll().ToList();
                BillsTbl.BillsList.AddRange(UnitOfWork.TempAdditionVoucherService.GetAll().ToList());
             //   BillsTbl.BillsList= BillsTbl.BillsList.OrderByDescending(x => x.CreatedDate);
                obj.Data = BillsTbl;
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [Route("api/AdditionVoucher/GetSpecificAdditionVoucher")]
        [HttpGet]
        public ActionResult GetSpecificAdditionVoucher(int Id, string Type)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                AdditionVoucherVM model = new AdditionVoucherVM();
                if (Type == "True")
                {
                    model = UnitOfWork.TempAdditionVoucherService.Get(Id);
                    model.AdditionVoucherItem = UnitOfWork.TempAdditionVoucherItemService.GetBySerial(model.SerialNumber.Value, model.StoreId.Value);
                    int indexer = 0;
                    foreach (var item in model.AdditionVoucherItem)
                    {
                        if (item != null)
                        {
                        item.Identifer = indexer + 1;
                        indexer++;
                        }
                    }
                }
                else
                {
                    model = UnitOfWork.AdditionVoucherService.Get(Id);
                    model.AdditionVoucherItem = UnitOfWork.AdditionVoucherItemService.GetBySerial(model.SerialNumber.Value, model.StoreId.Value);
                    int indexer = 0;
                    foreach (var item in model.AdditionVoucherItem)
                    {
                        if (item != null)
                        {
                        item.Identifer = indexer + 1;
                        indexer++;
                        }
                    }
                }
                List<LookupKeyValueVM> LookupKeyValue = new List<LookupKeyValueVM>();
                LookupKeyValue.AddRange(UnitOfWork.ClientsService.GetClients().ToList());
                LookupKeyValue.AddRange(UnitOfWork.SupplierService.GetSuppliers().ToList());
                LookupKeyValue.AddRange(UnitOfWork.EmployeeService.GetEmployees().ToList());
                model.StoreList = UnitOfWork.StoreServices.GetAll().ToList();
                model.ItemList = UnitOfWork.ItemServices.GetItems().ToList();
                model.Client_Supplier_List = LookupKeyValue.OrderBy(x => x.Name).ToList();
                obj.Data = model;
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/AdditionVoucher/AddAdditionVoucher")]
        [HttpPost]
        public IActionResult AddAdditionVoucher(AdditionVoucherVM AdditionVoucherModel)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                var AdditionVoucherSaved = UnitOfWork.AdditionVoucherService.Add(AdditionVoucherModel);
                UnitOfWork.Commit();
                obj.Data = AdditionVoucherSaved.ID;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/AdditionVoucher/EditAdditionVoucher")]
        [HttpPost]
        public IActionResult EditAdditionVoucher(AdditionVoucherVM AdditionVoucherModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.AdditionVoucherService.Edit(AdditionVoucherModel);
                UnitOfWork.Commit();
                //delete old items
                UnitOfWork.AdditionVoucherItemService.DeletePhysical(AdditionVoucherModel.SerialNumber.Value);
                UnitOfWork.Commit();
                foreach (var items in AdditionVoucherModel.AdditionVoucherItem)
                {
                    if (items != null)
                    {
                        if (items.ItemId != 0)
                        {
                            items.SerialNumber = AdditionVoucherModel.SerialNumber;
                            UnitOfWork.AdditionVoucherItemService.Add(items);
                            UnitOfWork.Commit();
                            ///////////////////////////////////////////////////trans///////////////////////////////
                            var currentBalance = UnitOfWork.TransItemService.GetLatestBalance( items.ItemId, (int)AdditionVoucherModel.StoreId, AdditionVoucherModel.Date.ToLocalTime());
                            decimal NewBalance = (decimal)(currentBalance + items.Quantity);
                            TransItemVM TransItem = new TransItemVM
                            {
                               ItemId=items.ItemId,
                                ItemType = TransItemTypeEnum.سند_إضافة,
                                Subtraction = 0,
                                CreateDate = DateTime.Now,
                                OpenBalFlag = false,
                                Addition = (decimal)items.Quantity,
                                Date = AdditionVoucherModel.Date,
                                SerialNumber = AdditionVoucherModel.SerialNumber.Value,
                                IsDeleted = false,
                                Balance = NewBalance,
                                StoreId = (int)AdditionVoucherModel.StoreId,
                            };
                            bool checkreturn = UnitOfWork.TransItemService.Add(TransItem, (decimal)items.Quantity);
                            UnitOfWork.Commit();
                            //recacaluate
                            if (!checkreturn)
                            {
                                UnitOfWork.TransItemService.RecalculateItemBalance(items.ItemId, (int)AdditionVoucherModel.StoreId, (int)AdditionVoucherModel.SerialNumber);
                            }
                            UnitOfWork.Commit();
                            ///////////////////////////////////////////////////////////////////////////
                        }
                    }
                }
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/AdditionVoucher/DeleteAdditionVoucher")]
        [HttpGet]
        public IActionResult DeleteAdditionVoucher(int ID, string Type)//by serialNumber
        {

            var obj = new ResponseVM();
            try
            {
                //int currobj;
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                if (Type == "temp")
                {
                    UnitOfWork.TempAdditionVoucherService.Delete(ID);
                    UnitOfWork.TempAdditionVoucherItemService.Delete(ID);
                }
                else
                {
                    UnitOfWork.AdditionVoucherService.Delete(ID);
                    UnitOfWork.AdditionVoucherItemService.Delete(ID);
                    AdditionVoucherVM DeletedModel = UnitOfWork.AdditionVoucherService.Get(ID);
                    List<AdditionVoucherItemVM> AdditionVoucherItem = UnitOfWork.AdditionVoucherItemService.GetBySerial((int)DeletedModel.SerialNumber);

                    foreach (var item in AdditionVoucherItem)
                    {
                        int Id = UnitOfWork.TransItemService.RecalculateItemBalance(item.ItemId, (int)DeletedModel.StoreId, (int)DeletedModel.SerialNumber);
                        UnitOfWork.TransItemService.Delete(Id);
                    }
                   
                }
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/AdditionVoucher/GetListsOfDDl")]
        [HttpGet]
        public ActionResult GetListsOfDDl()
        {
            try
            {
                var obj = new ResponseVM();
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                List<LookupKeyValueVM> LookupKeyValue = new List<LookupKeyValueVM>();
                LookupKeyValue.AddRange(UnitOfWork.ClientsService.GetClients().ToList());
                LookupKeyValue.AddRange(UnitOfWork.SupplierService.GetSuppliers().ToList());
                LookupKeyValue.AddRange(UnitOfWork.EmployeeService.GetEmployees().ToList());
                AdditionVoucherVM AdditionVoucher = new AdditionVoucherVM
                {
                    StoreList = UnitOfWork.StoreServices.GetAll().ToList(),
                    ItemList = UnitOfWork.ItemServices.GetItems().ToList(),
                    Client_Supplier_List = LookupKeyValue.OrderBy(x => x.Name).ToList(),
                    SerialNumber = UnitOfWork.AdditionVoucherService.GenerateSerial(0),
                    Date = DateTime.Now
                };
                obj.Data = AdditionVoucher;
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        [Route("api/AdditionVoucher/GetListsOfSupplierClientDDl")]
        [HttpGet]
        public ActionResult GetListsOfSupplierClientDDl()
        {
            try
            {
                var obj = new ResponseVM();
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                List<LookupKeyValueVM> LookupKeyValue = new List<LookupKeyValueVM>();
                LookupKeyValue.AddRange(UnitOfWork.ClientsService.GetAll().Select(s => new LookupKeyValueVM
                {
                    ID = s.ID,
                    Name = s.Name,
                    Type = 1
                }).ToList());
                LookupKeyValue.AddRange(UnitOfWork.SupplierService.GetAll().Select(s => new LookupKeyValueVM
                {
                    ID = s.ID,
                    Name = s.Name,
                    Type = 2
                }).ToList());
                obj.Data = LookupKeyValue;
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        [Route("api/AdditionVoucher/ConvertToAddition")]
        [HttpPost]
        public ActionResult ConvertToAddition(AdditionVoucherVM TempModel)
        {
            try
            {
                var obj = new ResponseVM();
                obj.Message = ApiStatus.Success;
                var result = UnitOfWork.AdditionVoucherService.ConvertTempToAdditionVoucher(TempModel);
                if (result != null)
                {
                    UnitOfWork.Commit();
                    foreach (var item in TempModel.AdditionVoucherItem)
                    {
                        if (item != null)
                        {
                            if (item.ItemId != 0)
                            {
                                item.SerialNumber = result.SerialNumber;
                                UnitOfWork.AdditionVoucherItemService.ConvertTempItemToAdditionItem(item);
                                UnitOfWork.Commit();
                                ///////////////////////////////////////////////////trans///////////////////////////////
                                var currentBalance = UnitOfWork.TransItemService.GetLatestBalance(item.ItemId, (int)TempModel.StoreId, TempModel.Date.ToLocalTime());
                                decimal NewBalance = (decimal)(currentBalance + item.Quantity);
                                TransItemVM TransItem = new TransItemVM
                                {
                                    ItemId = item.ItemId,
                                    ItemType = TransItemTypeEnum.سند_إضافة,
                                    Subtraction = 0,
                                    CreateDate = DateTime.Now,
                                    OpenBalFlag = false,
                                    Addition = (decimal)item.Quantity,
                                    Date = TempModel.Date,
                                    SerialNumber = TempModel.SerialNumber.Value,
                                    IsDeleted = false,
                                    Balance = NewBalance,
                                    StoreId= (int)TempModel.StoreId,
                                };
                                bool checkreturn = UnitOfWork.TransItemService.Add(TransItem, (decimal)item.Quantity);
                                UnitOfWork.Commit();
                                //recacaluate
                                if (!checkreturn)
                                {
                                    UnitOfWork.TransItemService.RecalculateItemBalance(item.ItemId, (int)TempModel.StoreId, (int)TempModel.SerialNumber);
                                }
                                UnitOfWork.Commit();
                                ///////////////////////////////////////////////////////////////////////////
                            }
                        }
                    }
                    UnitOfWork.Commit();
                    UnitOfWork.TempAdditionVoucherService.DeletePhysical(TempModel.ID);
                    UnitOfWork.TempAdditionVoucherItemService.DeletePhysical(TempModel.SerialNumber.Value);
                    UnitOfWork.Commit();
                    obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                }
                else
                    obj.StatusCode = int.Parse(ApiStatus.ErrorCode);
                obj.Data = result;
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

    }
}