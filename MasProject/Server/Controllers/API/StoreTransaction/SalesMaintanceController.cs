﻿using MasProject.Data.DataAccess;
using MasProject.Domain;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using MasProject.Domain.ViewModel.StoreTransaction.Quotation;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Collections.Generic;
using static MasProject.Domain.Enums.Enums;
using MasProject.Domain.ViewModel.TransVM;

namespace MasProject.Server.Controllers.API.StoreTransaction
{

    public class SalesMaintanceController : BaseController
    {
        public SalesMaintanceController(DBContext context) : base(context)
        {
        }
        [Route("api/MantienceBills/GenerateSerial")]
        [HttpGet]
        public ActionResult GenerateSerial(int BranchId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.MantienceBillsService.GenerateSerial(BranchId);
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/MantienceBills/GetSalesByClient")]
        [HttpGet]
        public ActionResult GetSalesByClient(int ClientId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.MantienceBillsService.GetAllByClient(ClientId).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/MantienceBills/GetSalesByBranch")]
        [HttpGet]
        public ActionResult GetSalesByBranch(int BranchId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.MantienceBillsService.GetAllByBranch(BranchId).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/MantienceBills/GetMantienceBills")]
        [HttpGet]
        public ActionResult GetMantienceBills()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.MantienceBillsService.GetAll().ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/MantienceBills/GetListDDL")]
        [HttpGet]
        public ActionResult GetListddl()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                MantienceBillsVM SalesBill = new MantienceBillsVM
                {
                    StoreList = UnitOfWork.StoreServices.GetAll().ToList(),
                    ClientList = UnitOfWork.ClientsService.GetClients().ToList(),
                    DealTypeList = UnitOfWork.DealTypeService.GetAll().ToList(),
                    ItemList = UnitOfWork.ItemServices.GetItems().ToList(),
                    sellingExpensesList = UnitOfWork.SellingExpensesService.GetAll().ToList(),
                    SerialNumber = UnitOfWork.MantienceBillsService.GenerateSerial(0),
                    BranchList = UnitOfWork.BranchServices.GetAll().ToList(),
                    Date = DateTime.Now,
                    DiscountPrecentage = 0,
                    DiscountValue = 0,
                };
                obj.Data = SalesBill;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/MantienceBills/GetSpecificMantienceBills")]
        [HttpGet]
        public ActionResult GetSpecificMantienceBills(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                MantienceBillsVM model = UnitOfWork.MantienceBillsService.Get(Id);
                //model.MantienceItems = UnitOfWork.MantienceItemsService.GetBySerial(model.SerialNumber.Value);
                model.MaintanceBillStores = UnitOfWork.MaintanceBillStoresService.GetBySerial(model.SerialNumber.Value);
                model.TaxFileNumber = UnitOfWork.ClientsService.Get(model.ClientId).Taxfile;
                model.ClientBalance = UnitOfWork.TransClientService.GetBalance(model.ClientId);
                int indexer = 0;
                foreach (var store in model.MaintanceBillStores)
                {
                    if (store != null)
                    {
                        if (store.StoreId != 0 & store.StoreId != null)
                        {
                            int indexerItem = 0;
                            store.MantienceItem = UnitOfWork.MantienceItemsService.GetBySerial(model.SerialNumber.Value, (int)store.StoreId, store.Identifer);
                            foreach (var item in store.MantienceItem)
                            {
                                if (item != null)
                                {
                                    if (item.ItemId != 0)
                                    {
                                        item.Identifer = indexer + 1;
                                        item.ItemImg = UnitOfWork.ItemServices.GetItemImage(item.ItemId).ItemImg;
                                        indexerItem++;
                                    }
                                }

                            }
                            if (store.Identifer == 0)
                            {
                                store.Identifer = indexer + 1;
                            }
                            indexer++;
                        }
                    }
                }
                model.MaintanceSellExpenses = UnitOfWork.MaintanceSellExpensesService.GetBySerial(model.SerialNumber.Value);
                indexer = 0;
                foreach (var item in model.MaintanceSellExpenses)
                {
                    if (item != null)
                    {
                        if (item.ExpensesId != 0)
                        {
                            item.Identifer = indexer + 1;
                            indexer++;
                        }
                    }
                }
                obj.Data = model;
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }
        [Route("api/MantienceBills/GetSalesMaintanceFromQuotation")]
        [HttpGet]
        public ActionResult GetSalesMaintanceFromQuotation(int QuotaionId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                QuotationVM model = UnitOfWork.QuotationService.Get(QuotaionId);
                model.QuotationItems = UnitOfWork.QuotationItemsService.GetBySerial(model.SerialNumber.Value);
                model.IsConverted = true;
                UnitOfWork.QuotationService.Edit(model);
                UnitOfWork.Commit();
                MantienceBillsVM NewModel = new MantienceBillsVM
                {
                    SerialNumber = UnitOfWork.MantienceBillsService.GenerateSerial(0),
                    Date = model.Date,
                    StoreId = model.StoreId,
                    // ClientId=model.ClientId,
                    ClientBalance = model.ClientBalance,
                    DealTypeId = model.DealTypeId,
                    TaxFileNumber = model.TaxFileNumber.ToString(),
                    FlagType = model.FlagType,
                    TotalPrice = model.TotalPrice,
                    DiscountPrecentage = model.DiscountPrecentage,
                    DiscountValue = model.DiscountValue,
                    TotalAfterDiscount = model.TotalAfterDiscount,
                    ShowNotesFlag = model.ShowNotesFlag,
                    TotalAfterTax = model.TotalAfterTax,
                    AddtionTax = model.AddtionTax,
                    AddtionTaxAmount = model.AddtionTaxAmount,
                    SourceDeduction = model.SourceDeduction,
                    SourceDeductionAmount = model.SourceDeductionAmount,
                    MultiStore = false,
                    BranchId = model.BranchId,
                    Note = model.Note,
                };
                NewModel.MaintanceBillStores = new List<MaintanceBillStoresVM>();
                MaintanceBillStoresVM StoreBill = new MaintanceBillStoresVM { Identifer = 1, StoreId = model.StoreId };
                StoreBill.MantienceItem = new List<MantienceItemsVM>();
                int indexer = 0;
                foreach (var item in model.QuotationItems)
                {
                    if (item != null)
                    {
                        if (item.ItemId != 0)
                        {
                            MantienceItemsVM Item = new MantienceItemsVM
                            {
                                Identifer = indexer + 1,
                                Box_ParCode = item.Box_ParCode,
                                ItemId = item.ItemId,
                                Quantity = item.Quantity,
                                Price = item.Price,
                                Total = item.Total,
                                StoreId = model.StoreId,
                            };

                            indexer++;
                            StoreBill.MantienceItem.Add(Item);
                        }
                    }
                }
                NewModel.MaintanceBillStores.Add(StoreBill);
                obj.Data = NewModel;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/MantienceBills/AddMantienceBills")]
        [HttpPost]
        public IActionResult AddMantienceBills(MantienceBillsVM BillModel)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success; 
                BillModel.SerialNumber = UnitOfWork.MantienceBillsService.GenerateSerial(BillModel.BranchId);
                var BillSaved = UnitOfWork.MantienceBillsService.Add(BillModel);
                UnitOfWork.Commit();
                obj.Data = BillSaved.ID;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }


        [Route("api/MantienceBills/EditMantienceBills")]
        [HttpPost]
        public IActionResult EditMantienceBills(MantienceBillsVM BillModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                //////check if client changed for this bill
                MantienceBillsVM model = UnitOfWork.MantienceBillsService.Get(BillModel.ID);
                if (model.ClientId != BillModel.ClientId)
                {
                    int Id = UnitOfWork.TransClientService.RecalculateClientBalance(model.ClientId, (int)model.SerialNumber);
                    UnitOfWork.TransClientService.Delete(Id);
                }
                if (BillModel.MaintanceBillStores.Count() == 1)
                {
                    BillModel.MultiStore = false;
                }
                else
                {
                    BillModel.MultiStore = true;
                }
                UnitOfWork.MantienceBillsService.Edit(BillModel);
                //delete old items
                UnitOfWork.MantienceItemsService.DeletePhysical(BillModel.SerialNumber.Value);
                UnitOfWork.MaintanceBillStoresService.DeletePhysical(BillModel.SerialNumber.Value);
                UnitOfWork.MaintanceSellExpensesService.DeletePhysical(BillModel.SerialNumber.Value);
                UnitOfWork.Commit();
                foreach (var StoreItem in BillModel.MaintanceBillStores)
                {
                    if (StoreItem != null)
                    {
                        if (StoreItem.StoreId != 0 & StoreItem.StoreId != null)
                        {
                            StoreItem.SerialNumber = BillModel.SerialNumber;
                            UnitOfWork.MaintanceBillStoresService.Add(StoreItem);
                            foreach (var items in StoreItem.MantienceItem)
                            {
                                if (items != null)
                                {
                                    if (items.ItemId != 0)
                                    {
                                        items.SerialNumber = BillModel.SerialNumber;
                                        items.StoreId = StoreItem.StoreId;
                                        items.Identifer = StoreItem.Identifer;//to take store idetifier and save it
                                        UnitOfWork.MantienceItemsService.Add(items);
                                    }
                                }
                            }
                        }

                    }
                }
                foreach (var Expenses in BillModel.MaintanceSellExpenses)
                {
                    if (Expenses != null)
                    {
                        if (Expenses.ExpensesId != 0 & Expenses.ExpensesId != null)
                        {
                            Expenses.SerialNumber = BillModel.SerialNumber;
                            UnitOfWork.MaintanceSellExpensesService.Add(Expenses);
                        }
                    }
                }
                UnitOfWork.Commit();
                ///////////////////////////////////////////////////trans///////////////////////////////
                var currentBalance = UnitOfWork.TransClientService.GetLatestBalance((int)BillModel.ClientId, BillModel.Date.ToLocalTime());
                var Balance = BillModel.FlagType ? (decimal)BillModel.TotalAfterTax : (decimal)BillModel.TotalAfterDiscount;
                decimal NewBalance = currentBalance + Balance;
                TransClientVM TransClient = new TransClientVM
                {
                    ClientId = BillModel.ClientId,
                    ClientType = TransClientTypeEnum.صيانة,
                    Credit = 0,
                    CreateDate = DateTime.Now,
                    OpenBalFlag = false,
                    Debit = Balance,
                    Date = BillModel.Date,
                    SerialNumber = BillModel.SerialNumber.Value,
                    IsDeleted = false,
                    Balance = NewBalance,
                };
                bool checkreturn = UnitOfWork.TransClientService.Add(TransClient, Balance);
                UnitOfWork.Commit();
                //recacaluate
                if (!checkreturn)
                {
                    UnitOfWork.TransClientService.RecalculateClientBalance(BillModel.ClientId, (int)BillModel.SerialNumber);
                }
                ///////////////////////////////////////////////////////////////////////////
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/MantienceBills/ConvertMantienceBills")]
        [HttpPost]
        public IActionResult ConvertMantienceBills(MantienceBillsVM BillModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                List<MaintanceBillStoresVM> BillStoresList = UnitOfWork.MaintanceBillStoresService.GetBySerial(BillModel.SerialNumber.Value);
                if (BillStoresList != null && BillStoresList.Count > 0)
                {
                    foreach (var BillStore in BillStoresList)
                    {
                        if (BillStore != null)
                        {
                            if (BillStore.StoreId != 0 & BillStore.StoreId != null)
                            {
                                BillModel.StoreId = BillStore.StoreId;
                                BillStore.MantienceItem = UnitOfWork.MantienceItemsService.GetBySerial(BillModel.SerialNumber.Value, (int)BillStore.StoreId, BillStore.Identifer);
                                var payment = UnitOfWork.TempPaymentVoucherService.ConvertMantienceToTemp(BillModel);
                                UnitOfWork.Commit();
                                foreach (var items in BillStore.MantienceItem)
                                {
                                    if (items != null)
                                    {
                                        if (items.ItemId != 0)
                                        {
                                            items.SerialNumber = payment.SerialNumber;
                                            UnitOfWork.TempPaymentVoucherItemService.ConvertMantienceItemToTempItem(items);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                UnitOfWork.Commit();
                BillModel.IsConverted = true;
                UnitOfWork.MantienceBillsService.Edit(BillModel);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/MantienceBills/DeleteMantienceBills")]
        [HttpGet]
        public IActionResult DeleteMantienceBills(int ID, int SerialNumber, int ClientId)//by serialNumber
        {

            var obj = new ResponseVM();
            try
            {
                //int currobj;
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.MantienceBillsService.Delete(ID);
                UnitOfWork.MantienceItemsService.Delete(ID);
                UnitOfWork.MaintanceBillStoresService.Delete(ID);
                UnitOfWork.MaintanceSellExpensesService.Delete(ID);
                if (ClientId != 0)
                {
                    int Id = UnitOfWork.TransClientService.RecalculateClientBalance(ClientId, SerialNumber);
                    UnitOfWork.TransClientService.Delete(Id);
                }
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}
