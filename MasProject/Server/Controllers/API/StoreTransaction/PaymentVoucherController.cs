﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasProject.Data.DataAccess;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.StoreTransaction.Voucher;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using MasProject.Domain.ViewModel.StoreTransaction.Purchase;
using MasProject.Domain.ViewModel.StoreTransaction.ExecutionWithDrawals;

using Microsoft.AspNetCore.Mvc;
using static MasProject.Domain.Enums.Enums;
using MasProject.Domain.ViewModel.TransVM;

namespace MasProject.Server.Controllers.API.StoreTransaction
{
    public class PaymentVoucherController : BaseController
    {
        public PaymentVoucherController(DBContext context) : base(context)
        {
        }
        [Route("api/PaymentVoucher/GetPaymentVoucherByUserType")]
        [HttpGet]
        public ActionResult GetPaymentVoucherByUserType(int UserType)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.PaymentVoucherService.GetAllByUserType(UserType).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/PaymentVoucher/GetPaymentVoucherByStore")]
        [HttpGet]
        public ActionResult GetAdditionVoucherByStore(int StoreId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.PaymentVoucherService.GetAllByStore(StoreId).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/PaymentVoucher/GetPaymentVoucher")]
        [HttpGet]

        public ActionResult GetPaymentVoucher()
        {
            var obj = new ResponseVM();
            try
            {
               // List<PaymentVoucherVM> paymentVoucherList = new List<PaymentVoucherVM>();
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Message = ApiStatus.Success;
                BillsTbl<PaymentVoucherVM> BillsTbl = new BillsTbl<PaymentVoucherVM> { };
                List<LookupKeyValueVM> LookupKeyValue = new List<LookupKeyValueVM>();
                LookupKeyValue.AddRange(UnitOfWork.ClientsService.GetClients().ToList());
                LookupKeyValue.AddRange(UnitOfWork.SupplierService.GetSuppliers().ToList());
                LookupKeyValue.AddRange(UnitOfWork.EmployeeService.GetEmployees().ToList());
                PaymentVoucherVM AdditionVoucher = new PaymentVoucherVM
                {
                    StoreList = UnitOfWork.StoreServices.GetAll().ToList(),
                    Client_Supplier_List = LookupKeyValue.OrderBy(x => x.Name).ToList(),
                };
                BillsTbl.SearchItems = AdditionVoucher;
                BillsTbl.BillsList = UnitOfWork.PaymentVoucherService.GetAll().ToList();
                BillsTbl.BillsList.AddRange(UnitOfWork.TempPaymentVoucherService.GetAll().ToList());
                // obj.Data = paymentVoucherList.OrderByDescending(x => x.CreatedDate);
                obj.Data = BillsTbl;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/PaymentVoucher/GetSpecificPaymentVoucher")]
        [HttpGet]
        public ActionResult GetSpecificPaymentVoucher(int Id, string Type)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                PaymentVoucherVM model = new PaymentVoucherVM();
                if (Type == "True")
                {
                    model = UnitOfWork.TempPaymentVoucherService.Get(Id);
                    model.PaymentVoucherItem = UnitOfWork.TempPaymentVoucherItemService.GetBySerial(model.SerialNumber.Value, model.StoreId.Value);
                    //get branch id from table
                    int BranchId = 0;
                    if(model.BillType== "فاتورة مبيعات")
                    {
                        var tempcheck = UnitOfWork.SalesBillsService.GetIdBySerial((int)model.BillSerialNumber);
                        if(tempcheck != null)
                        {
                            BranchId = (int)tempcheck.BranchId;
                        }                       
                    }
                    model.SerialNumber = UnitOfWork.PaymentVoucherService.GenerateSerial(BranchId);
                    int indexer = 0;
                    foreach (var item in model.PaymentVoucherItem)
                    {
                        if (item != null)
                        {
                            item.Identifer = indexer + 1;
                            indexer++;
                        }
                    }
                }
                else
                {
                    model = UnitOfWork.PaymentVoucherService.Get(Id);
                    model.PaymentVoucherItem = UnitOfWork.PaymentVoucherItemService.GetBySerial(model.SerialNumber.Value, model.StoreId.Value);
                    int indexer = 0;
                    foreach (var item in model.PaymentVoucherItem)
                    {
                        if (item != null)
                        {
                            item.Identifer = indexer + 1;
                            indexer++;
                        }
                    }
                }
                List<LookupKeyValueVM> LookupKeyValue = new List<LookupKeyValueVM>();
                LookupKeyValue.AddRange(UnitOfWork.ClientsService.GetClients().ToList());
                LookupKeyValue.AddRange(UnitOfWork.SupplierService.GetSuppliers().ToList());
                LookupKeyValue.AddRange(UnitOfWork.EmployeeService.GetEmployees().ToList());
                model.StoreList = UnitOfWork.StoreServices.GetAll().ToList();
                model.ItemList = UnitOfWork.ItemServices.GetItems().ToList();
                model.Client_Supplier_List = LookupKeyValue.OrderBy(x => x.Name).ToList();
                obj.Data = model;
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/PaymentVoucher/AddPaymentVoucher")]
        [HttpPost]

        public IActionResult AddPaymentVoucher(PaymentVoucherVM PaymentVoucherModel)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                var PaymentVoucherSaved = UnitOfWork.PaymentVoucherService.Add(PaymentVoucherModel);
                UnitOfWork.Commit();
                obj.Data = PaymentVoucherSaved.ID;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }


        [Route("api/PaymentVoucher/EditPaymentVoucher")]
        [HttpPost]
        public IActionResult EditPaymentVoucher(PaymentVoucherVM PaymentVoucherModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.PaymentVoucherService.Edit(PaymentVoucherModel);
                //delete old items
                UnitOfWork.PaymentVoucherItemService.DeletePhysical(PaymentVoucherModel.SerialNumber.Value);
                UnitOfWork.Commit();
                foreach (var items in PaymentVoucherModel.PaymentVoucherItem)
                {
                    if (items != null)
                    {
                        if (items.ItemId != 0)
                        {
                            items.SerialNumber = PaymentVoucherModel.SerialNumber;
                            UnitOfWork.PaymentVoucherItemService.Add(items);
                            UnitOfWork.Commit();
                            ///////////////////////////////////////////////////trans///////////////////////////////
                            var currentBalance = UnitOfWork.TransItemService.GetLatestBalance(items.ItemId, (int)PaymentVoucherModel.StoreId, PaymentVoucherModel.Date.ToLocalTime());
                            decimal NewBalance = (decimal)(currentBalance - items.Quantity);
                            TransItemVM TransItem = new TransItemVM
                            {
                                ItemId = items.ItemId,
                                ItemType = TransItemTypeEnum.سند_صرف,
                                Subtraction = (decimal)items.Quantity,
                                CreateDate = DateTime.Now,
                                OpenBalFlag = false,
                                Addition = 0,
                                Date = PaymentVoucherModel.Date,
                                SerialNumber = PaymentVoucherModel.SerialNumber.Value,
                                IsDeleted = false,
                                Balance = NewBalance,
                                StoreId = (int)PaymentVoucherModel.StoreId,
                            };
                            bool checkreturn = UnitOfWork.TransItemService.Add(TransItem, (decimal)items.Quantity);
                            UnitOfWork.Commit();
                            //recacaluate
                            if (!checkreturn)
                            {
                                UnitOfWork.TransItemService.RecalculateItemBalance(items.ItemId, (int)PaymentVoucherModel.StoreId, (int)PaymentVoucherModel.SerialNumber);
                            }
                            UnitOfWork.Commit();
                            ///////////////////////////////////////////////////////////////////////////
                        }
                    }
                }
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/PaymentVoucher/DeletePaymentVoucher")]
        [HttpGet]
        public IActionResult DeletePaymentVoucher(int ID, string Type)//by serialNumber
        {

            var obj = new ResponseVM();
            try
            {
                //int currobj;
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                if (Type == "temp")
                {
                    UnitOfWork.TempPaymentVoucherService.Delete(ID);
                    int TempSerial = (int)UnitOfWork.TempPaymentVoucherService.Get(ID).SerialNumber;
                    UnitOfWork.TempPaymentVoucherItemService.Delete(TempSerial);
                }
                else
                {
                    PaymentVoucherVM DeletedModel = UnitOfWork.PaymentVoucherService.Get(ID);
                    UnitOfWork.PaymentVoucherService.Delete(ID);
                    UnitOfWork.PaymentVoucherItemService.Delete((int)DeletedModel.SerialNumber);
                    List<PaymentVoucherItemVM> AdditionVoucherItem = UnitOfWork.PaymentVoucherItemService.GetBySerial((int)DeletedModel.SerialNumber);

                    foreach (var item in AdditionVoucherItem)
                    {
                        int Id = UnitOfWork.TransItemService.RecalculateItemBalance(item.ItemId, (int)DeletedModel.StoreId, (int)DeletedModel.SerialNumber);
                        UnitOfWork.TransItemService.Delete(Id);
                    }
                }
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/PaymentVoucher/GetListsOfDDl")]
        [HttpGet]
        public ActionResult GetListsOfDDl()
        {
            try
            {
                var obj = new ResponseVM();
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                List<LookupKeyValueVM> LookupKeyValue = new List<LookupKeyValueVM>();
                LookupKeyValue.AddRange(UnitOfWork.ClientsService.GetClients().ToList());
                LookupKeyValue.AddRange(UnitOfWork.SupplierService.GetSuppliers().ToList());
                LookupKeyValue.AddRange(UnitOfWork.EmployeeService.GetEmployees().ToList());
                PaymentVoucherVM PaymentVoucher = new PaymentVoucherVM
                {
                    StoreList = UnitOfWork.StoreServices.GetAll().ToList(),
                    ItemList = UnitOfWork.ItemServices.GetItems().ToList(),
                    Client_Supplier_List = LookupKeyValue.OrderBy(x => x.Name).ToList(),
                    SerialNumber = UnitOfWork.PaymentVoucherService.GenerateSerial(0),
                    Date = DateTime.Now
                };

                obj.Data = PaymentVoucher;
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        [Route("api/PaymentVoucher/GetListsOfSupplierClientDDl")]
        [HttpGet]
        public ActionResult GetListsOfSupplierClientDDl()
        {
            try
            {
                var obj = new ResponseVM();
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                List<LookupKeyValueVM> LookupKeyValue = new List<LookupKeyValueVM>();
                LookupKeyValue.AddRange(UnitOfWork.ClientsService.GetAll().Select(s => new LookupKeyValueVM
                {
                    ID = s.ID,
                    Name = s.Name,
                    Type = 1

                }).ToList());
                LookupKeyValue.AddRange(UnitOfWork.SupplierService.GetAll().Select(s => new LookupKeyValueVM
                {
                    ID = s.ID,
                    Name = s.Name,
                    Type = 2
                }).ToList());
                obj.Data = LookupKeyValue;
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [Route("api/PaymentVoucher/ConvertToPayment")]
        [HttpPost]
        public ActionResult ConvertToPayment(PaymentVoucherVM TempModel)
        {
            try
            {
                var obj = new ResponseVM();
                obj.Message = ApiStatus.Success;
                var result = UnitOfWork.PaymentVoucherService.ConvertTempToPaymentVoucher(TempModel);
                if (result != null)
                {
                    UnitOfWork.Commit();
                    foreach (var item in TempModel.PaymentVoucherItem)
                    {
                        if (item != null)
                        {
                            if (item.ItemId != 0)
                            {
                                item.SerialNumber = result.SerialNumber;
                                UnitOfWork.PaymentVoucherItemService.ConvertTempItemToPaymentItem(item);
                                UnitOfWork.Commit();
                                ///////////////////////////////////////////////////trans///////////////////////////////
                                var currentBalance = UnitOfWork.TransItemService.GetLatestBalance(item.ItemId, (int)TempModel.StoreId, TempModel.Date.ToLocalTime());
                                decimal NewBalance = (decimal)(currentBalance - item.Quantity);
                                TransItemVM TransItem = new TransItemVM
                                {
                                    ItemId = item.ItemId,
                                    ItemType = TransItemTypeEnum.سند_صرف,
                                    Subtraction = (decimal)item.Quantity,
                                    CreateDate = DateTime.Now,
                                    OpenBalFlag = false,
                                    Addition = 0,
                                    Date = TempModel.Date,
                                    SerialNumber = TempModel.SerialNumber.Value,
                                    IsDeleted = false,
                                    Balance = NewBalance,
                                    StoreId = (int)TempModel.StoreId,
                                };
                                bool checkreturn = UnitOfWork.TransItemService.Add(TransItem, (decimal)item.Quantity);
                                UnitOfWork.Commit();
                                //recacaluate
                                if (!checkreturn)
                                {
                                    UnitOfWork.TransItemService.RecalculateItemBalance(item.ItemId, (int)TempModel.StoreId, (int)TempModel.SerialNumber);
                                }
                                UnitOfWork.Commit();
                                ///////////////////////////////////////////////////////////////////////////
                            }
                        }
                    }
                    UnitOfWork.Commit();
                    UnitOfWork.TempPaymentVoucherService.DeletePhysical(TempModel.ID);
                    UnitOfWork.TempPaymentVoucherItemService.DeletePhysical(TempModel.SerialNumber.Value);
                    UnitOfWork.Commit();
                    obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                }
                else
                    obj.StatusCode = int.Parse(ApiStatus.ErrorCode);
                obj.Data = result;
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}