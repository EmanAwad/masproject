﻿using MasProject.Data.DataAccess;
using MasProject.Domain;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using MasProject.Domain.ViewModel.TransVM;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using static MasProject.Domain.Enums.Enums;

namespace MasProject.Server.Controllers.API.StoreTransaction
{

    public class SalesReturnsController : BaseController
    {
        public SalesReturnsController(DBContext context) : base(context)
        {
        }
        [Route("api/SalesReturns/GenerateSerial")]
        [HttpGet]
        public ActionResult GenerateSerial(int BranchId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.SalesReturnsService.GenerateSerial(BranchId);
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/SalesReturns/GetSalesByClient")]
        [HttpGet]
        public ActionResult GetSalesByClient(int ClientId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.SalesReturnsService.GetAllByClient(ClientId).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/SalesReturns/GetSalesByBranch")]
        [HttpGet]
        public ActionResult GetSalesByBranch(int BranchId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.SalesReturnsService.GetAllByBranch(BranchId).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/SalesReturns/GetSalesReturns")]
        [HttpGet]
        public ActionResult GetSalesReturns()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.SalesReturnsService.GetAll().ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/SalesReturns/GetLatestPrice")]
        [HttpGet]

        public IActionResult GetLatestPrice(int ItemId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.SalesItemsService.GetLatestPrice(ItemId);
                UnitOfWork.Commit();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/SalesReturns/GetListDDL")]
        [HttpGet]
        public ActionResult GetListddl()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                SalesReturnsVM SalesBill = new SalesReturnsVM
                {
                    StoreList = UnitOfWork.StoreServices.GetAll().ToList(),
                    ClientList = UnitOfWork.ClientsService.GetClients().ToList(),
                    DealTypeList = UnitOfWork.DealTypeService.GetAll().ToList(),
                    ItemList = UnitOfWork.ItemServices.GetItems().ToList(),
                    sellingExpensesList = UnitOfWork.SellingExpensesService.GetAll().ToList(),
                    SerialNumber = UnitOfWork.SalesReturnsService.GenerateSerial(0),
                    BranchList = UnitOfWork.BranchServices.GetAll().ToList(),
                    Date = DateTime.Now,
                    DiscountPrecentage = 0,
                    DiscountValue = 0,
                };
                obj.Data = SalesBill;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/SalesReturns/GetSpecificSalesReturns")]
        [HttpGet]
        public ActionResult GetSpecificSalesReturns(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                SalesReturnsVM model = UnitOfWork.SalesReturnsService.Get(Id);
                //model.SalesReturnsBillStores = UnitOfWork.SalesReturnsBillStoresService.GetBySerial(model.SerialNumber.Value);
                model.SalesReturnsItems = UnitOfWork.SalesReturnsItemsService.GetBySerial(model.SerialNumber.Value);
                model.TaxFileNumber = UnitOfWork.ClientsService.Get(model.ClientId).Taxfile;
                int indexer = 0;
                //foreach (var store in model.SalesReturnsBillStores)
                //{
                //    store.Identifer = indexer + 1;
                //    indexer++;
                //    int indexerItem = 0;
                // store.SalesReturnsItems = UnitOfWork.SalesReturnsItemsService.GetBySerial(model.SerialNumber.Value, (int)store.StoreId);
                foreach (var item in model.SalesReturnsItems)
                {
                    if (item != null)
                    {
                        if (item.ItemId != 0)
                        {

                            item.Identifer = indexer + 1;
                            item.ItemImg = UnitOfWork.ItemServices.GetItemImage(item.ItemId).ItemImg;
                            indexer++;
                        }
                    }
                }
                // }
                model.SalesReturnsExpenses = UnitOfWork.SalesReturnsExpensesService.GetBySerial(model.SerialNumber.Value);
                indexer = 0;
                foreach (var item in model.SalesReturnsExpenses)
                {
                    if (item != null)
                    {
                        if (item.ExpensesId != 0)
                        {
                            item.Identifer = indexer + 1;
                            indexer++;
                        }
                    }
                }
                obj.Data = model;
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/SalesReturns/AddSalesReturns")]
        [HttpPost]

        public IActionResult AddSalesReturns(SalesReturnsVM BillModel)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                BillModel.SerialNumber = UnitOfWork.SalesReturnsService.GenerateSerial(BillModel.BranchId);
                var BillSaved = UnitOfWork.SalesReturnsService.Add(BillModel);
                UnitOfWork.Commit();
                obj.Data = BillSaved.ID;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }


        [Route("api/SalesReturns/EditSalesReturns")]
        [HttpPost]
        public IActionResult EditSalesReturns(SalesReturnsVM BillModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                //////check if client changed for this bill
                SalesReturnsVM model = UnitOfWork.SalesReturnsService.Get(BillModel.ID);
                if (model.ClientId != BillModel.ClientId)
                {
                    int Id = UnitOfWork.TransClientService.RecalculateClientBalance(model.ClientId, (int)model.SerialNumber);
                    UnitOfWork.TransClientService.Delete(Id);
                }
               
                UnitOfWork.SalesReturnsService.Edit(BillModel);
                //edit 24-09-2020
                UnitOfWork.SalesReturnsItemsService.DeletePhysical(BillModel.SerialNumber.Value);
                //UnitOfWork.SalesReturnsBillStoresService.DeletePhysical(BillModel.SerialNumber.Value);
                UnitOfWork.SalesReturnsExpensesService.DeletePhysical(BillModel.SerialNumber.Value);
                UnitOfWork.Commit();
                //foreach (var StoreItem in BillModel.SalesReturnsBillStores)
                //{
                //    if (StoreItem != null)
                //    {
                //        StoreItem.SerialNumber = BillModel.SerialNumber;
                //        UnitOfWork.SalesReturnsBillStoresService.Add(StoreItem);
                foreach (var items in BillModel.SalesReturnsItems)
                {
                    if (items != null)
                    {
                        if (items.ItemId != 0)
                        {
                            items.SerialNumber = BillModel.SerialNumber;
                            //items.StoreId = StoreItem.StoreId;
                            UnitOfWork.SalesReturnsItemsService.Add(items);
                        }
                    }
                }
                //}
                // }
                foreach (var Expenses in BillModel.SalesReturnsExpenses)
                {
                    if (Expenses != null)
                    {
                        if (Expenses.ExpensesId != 0 & Expenses.ExpensesId != null)
                        {
                            Expenses.SerialNumber = BillModel.SerialNumber;
                            UnitOfWork.SalesReturnsExpensesService.Add(Expenses);
                        }
                    }
                }
                UnitOfWork.Commit();
                ///////////////////////////////////////////////////trans///////////////////////////////
                var currentBalance = UnitOfWork.TransClientService.GetLatestBalance((int)BillModel.ClientId, BillModel.Date.ToLocalTime());
                var Balance = BillModel.FlagType ? (decimal)BillModel.TotalAfterTax : (decimal)BillModel.TotalAfterDiscount;
                decimal NewBalance = currentBalance - Balance;
                TransClientVM TransClient = new TransClientVM
                {
                    ClientId = BillModel.ClientId,
                    ClientType = TransClientTypeEnum.مردودات_مبيعات,
                    Credit = Balance,
                    CreateDate = DateTime.Now,
                    OpenBalFlag = false,
                    Debit = 0,
                    Date = BillModel.Date,
                    SerialNumber = BillModel.SerialNumber.Value,
                    IsDeleted = false,
                    Balance = NewBalance,
                };
                bool checkreturn = UnitOfWork.TransClientService.Add(TransClient, Balance);
                UnitOfWork.Commit();
                //recacaluate
                if (!checkreturn)
                {
                    UnitOfWork.TransClientService.RecalculateClientBalance(BillModel.ClientId, (int)BillModel.SerialNumber);
                }
                ///////////////////////////////////////////////////////////////////////////
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/SalesReturns/DeleteSalesReturns")]
        [HttpGet]
        public IActionResult DeleteSalesReturns(int ID, int SerialNumber, int ClientId)//by serialNumber
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.SalesReturnsService.Delete(ID);
                UnitOfWork.SalesReturnsItemsService.Delete(ID);
                //UnitOfWork.SalesReturnsBillStoresService.Delete(ID);
                UnitOfWork.SalesReturnsExpensesService.Delete(ID);
                if (ClientId != 0)
                {
                    int Id = UnitOfWork.TransClientService.RecalculateClientBalance(ClientId, SerialNumber);
                    UnitOfWork.TransClientService.Delete(Id);
                }
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/SalesReturns/ConvertSalesReturns")]
        [HttpPost]
        public IActionResult ConvertSalesReturnss(SalesReturnsVM BillModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                //List<SalesReturnsBillStoresVM> BillStoresList = UnitOfWork.SalesReturnsBillStoresService.GetBySerial(BillModel.SerialNumber.Value);
                //if (BillStoresList != null && BillStoresList.Count > 0)
                //{
                //    foreach (var BillStore in BillStoresList)
                //    {
                //        BillModel.StoreId = BillStore.StoreId;, (int)BillStore.StoreId
                BillModel.SalesReturnsItems = UnitOfWork.SalesReturnsItemsService.GetBySerial(BillModel.SerialNumber.Value);
                var addation = UnitOfWork.TempAdditionVoucherService.ConvertSalesReturnsToTemp(BillModel);
                UnitOfWork.Commit();
                foreach (var items in BillModel.SalesReturnsItems)
                {
                    if (items != null)
                    {
                        if (items.ItemId != 0)
                        {
                            items.SerialNumber = addation.SerialNumber;
                            UnitOfWork.TempAdditionVoucherItemService.ConvertReturnItemToTempItem(items);
                        }
                    }
                }
                //    }
                //}
                UnitOfWork.Commit();
                BillModel.IsConverted = true;
                UnitOfWork.SalesReturnsService.Edit(BillModel);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}