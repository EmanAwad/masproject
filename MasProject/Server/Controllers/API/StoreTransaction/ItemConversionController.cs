﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasProject.Data.DataAccess;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MasProject.Domain.ViewModel.StoreTransaction.Conversion;

namespace MasProject.Server.Controllers.API.StoreTransaction
{
   
    public class ItemConversionController : BaseController
    {
        public ItemConversionController(DBContext context) : base(context)
        {
        }
        [Route("api/ItemConversion/GetAllByStore")]
        [HttpGet]
        public ActionResult GetAllByStore(int StoreId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ItemConversion_BillService.GetAllByStore(StoreId).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }


        [Route("api/ItemConversion/GetItemConversion")]
        [HttpGet]
        public ActionResult GetItemConversion()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                BillsTbl<ItemConversion_BillVM> BillTbl = new BillsTbl<ItemConversion_BillVM> { };
                ItemConversion_BillVM BillModel = new ItemConversion_BillVM
                {
                    BranchList = UnitOfWork.BranchServices.GetAll().ToList(),
                    Store = UnitOfWork.StoreServices.GetAll().ToList(),
                    //Employee = UnitOfWork.UserService.GetAllUsers().Select(s => new LookupKeyValueVM
                    //{
                    //    ID = s.ID,
                    //    Name = s.UserName,
                    //}).ToList(),
                    //ItemList = UnitOfWork.ItemServices.GetItems().ToList(),
                };
                BillTbl.SearchItems = BillModel;
                BillTbl.BillsList = UnitOfWork.ItemConversion_BillService.GetAll().ToList();
                obj.Data = BillTbl;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/ItemConversion/GenerateSerial")]
        [HttpGet]
        public ActionResult GenerateSerial(int BranchId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ItemConversion_BillService.GenerateSerial(BranchId);
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    
        [Route("api/ItemConversion/GetListDDL")]
        [HttpGet]
        public ActionResult GetListddl()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                ItemConversion_BillVM SalesBill = new ItemConversion_BillVM
                {
                    BranchList = UnitOfWork.BranchServices.GetAll().ToList(),
                    Store = UnitOfWork.StoreServices.GetAll().ToList(),
                    Employee = UnitOfWork.UserService.GetAllUsers().Select(s => new LookupKeyValueVM
                    {
                        ID = s.ID,
                        Name = s.UserName,
                    }).ToList(),
                    ItemList = UnitOfWork.ItemServices.GetItems().ToList(),
                    ConversionRecordID = UnitOfWork.ItemConversion_BillService.GenerateSerial(0),
                    Date = DateTime.Now,
                };
                obj.Data = SalesBill;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/ItemConversion/GetItemConversion_Items")]
        [HttpGet]
        public ActionResult GetItemConversion_Items()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ItemConversion_ItemsService.GetAll().ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }


        [Route("api/ItemConversion/GetSpecificItemConversion")]
        [HttpGet]
        public ActionResult GetSpecificItemConversion(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                ItemConversion_BillVM model = UnitOfWork.ItemConversion_BillService.Get(Id);
                var AllItems = UnitOfWork.ItemConversion_ItemsService.Get(model.ConversionRecordID.Value);
                model.ConversionItemsFrom = AllItems.Where(x => x.ItemIdFrom != null).ToList();
                int indexer = 0;
                foreach (var item in model.ConversionItemsFrom)
                {
                    if (item != null)
                    {
                        item.IdentiferFrom = indexer + 1;
                        item.Name = "Test";
                        indexer++;
                    }
                }
                model.ConversionItemsTo = AllItems.Where(x => x.ItemIdTo != null).ToList();
                indexer = 0;
                foreach (var item in model.ConversionItemsTo)
                {
                    if (item != null)
                    {
                        item.IdentiferTo = indexer + 1;
                        item.Name = "Test";
                        indexer++;
                    }
                }
                model.Store = UnitOfWork.StoreServices.GetAll().ToList();
                model.BranchList = UnitOfWork.BranchServices.GetAll().ToList();
                model.Employee = UnitOfWork.UserService.GetAllUsers().Select(s => new LookupKeyValueVM
                {
                    ID = s.ID,
                    Name = s.UserName,
                }).ToList();
                model.ItemList = UnitOfWork.ItemServices.GetItems().ToList();
                obj.Data = model;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }


        //[Route("api/ItemConversion/GetSpecificItemConversion_Items")]
        //[HttpGet]
        //public ActionResult GetSpecificItemConversion_Items(int Id)
        //{
        //    var obj = new ResponseVM();
        //    try
        //    {
        //        obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
        //        obj.Message = ApiStatus.Success;
        //        ItemConversion_ItemVM model = UnitOfWork.ItemConversion_ItemsService.Get(Id);
        //        //model. = UnitOfWork.SalesItemsService.GetBySerial(model.ConversionRecordID.Value);
        //        //int indexer = 0;
        //        //foreach (var item in model.SalesItem)
        //        //{
        //        //    item.Identifer = indexer + 1;
        //        //    indexer++;
        //        //}
        //        //model.SalesSellExpenses = UnitOfWork.SalesSellExpensesService.GetBySerial(model.SerialNumber.Value);
        //        //indexer = 0;
        //        //foreach (var item in model.SalesSellExpenses)
        //        //{
        //        //    item.Identifer = indexer + 1;
        //        //    indexer++;
        //        //}
        //        obj.Data = model;
        //        return Ok(obj);
        //    }
        //    catch (Exception)
        //    {
        //        return BadRequest();
        //    }
        //}



        [Route("api/ItemConversion/AddItemConversion")]
        [HttpPost]

        public IActionResult AddItemConversion(ItemConversion_BillVM BillModel)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                var ItemSaved = UnitOfWork.ItemConversion_BillService.Add(BillModel);
                UnitOfWork.Commit();
                UnitOfWork.ItemConversion_ItemsService.AddFrom(BillModel.ConversionItemsFrom, BillModel.ConversionRecordID);
                UnitOfWork.ItemConversion_ItemsService.AddTo(BillModel.ConversionItemsTo, BillModel.ConversionRecordID);
                UnitOfWork.Commit();
                obj.Data = ItemSaved.ID;
                return Ok(obj);
            }
            catch (Exception ex)
            {
                var x = ex.Message;
                return BadRequest();
            }
        }

        //[Route("api/ItemConversion/AddItemConversion_Items")]
        //[HttpPost]

        //public IActionResult AddItemConversion_Items(ItemConversion_ItemVM ItemModel)
        //{
        //    var obj = new ResponseVM();
        //    try
        //    {
        //        obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
        //        obj.Message = ApiStatus.Success;
        //        var BillSaved = UnitOfWork.ItemConversion_ItemsService.Add(ItemModel);
        //        UnitOfWork.Commit();
        //        obj.Data = BillSaved.ID;
        //        return Ok(obj);
        //    }
        //    catch (Exception)
        //    {
        //        return BadRequest();
        //    }
        //}


        [Route("api/ItemConversion/EditItemConversion")]
        [HttpPost]
        public IActionResult EditItemConversion(ItemConversion_BillVM BillModel)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                var ItemSaved = UnitOfWork.ItemConversion_BillService.Edit(BillModel);
                UnitOfWork.ItemConversion_ItemsService.Delete((int)BillModel.ConversionRecordID);
                UnitOfWork.Commit();
                UnitOfWork.ItemConversion_ItemsService.AddFrom(BillModel.ConversionItemsFrom, BillModel.ConversionRecordID);
                UnitOfWork.ItemConversion_ItemsService.AddTo(BillModel.ConversionItemsTo, BillModel.ConversionRecordID);
                UnitOfWork.Commit();
                obj.Data = ItemSaved.ID;
                //foreach (var item in BillModel.ConversionItemsFrom)
                //{
                //    item.ID = ItemSaved.ID;
                //    item.CreatedDate = DateTime.Now;
                //    UnitOfWork.ItemConversion_ItemsService.EditFrom(item);

                //}
                //foreach (var item in BillModel.ConversionItemsTo)
                //{

                //    item.ID = ItemSaved.ID;
                //    item.CreatedDate = DateTime.Now;
                //    UnitOfWork.ItemConversion_ItemsService.EditTo(item);

                //}
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }

        //[Route("api/ItemConversion/EditItemConversion_Items")]
        //[HttpPost]
        //public IActionResult EditItemConversion_Items(ItemConversion_ItemVM BillModel)
        //{
        //    var obj = new ResponseVM();
        //    try
        //    {
        //        obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
        //        obj.Message = ApiStatus.Success;
        //        UnitOfWork.ItemConversion_ItemsService.Edit(BillModel);
        //        //delete old items
        //        //UnitOfWork.SalesItemsService.DeletePhysical(BillModel.SerialNumber.Value);
        //        //UnitOfWork.SalesSellExpensesService.DeletePhysical(BillModel.SerialNumber.Value);
        //        //UnitOfWork.Commit();
        //        //foreach (var items in BillModel.SalesItem)
        //        //{
        //        //    if (items != null)
        //        //    {
        //        //        items.SerialNumber = BillModel.SerialNumber;
        //        //        UnitOfWork.SalesItemsService.Add(items);
        //        //    }
        //        //}
        //        //foreach (var Expenses in BillModel.SalesSellExpenses)
        //        //{
        //        //    if (Expenses != null)
        //        //    {
        //        //        Expenses.SerialNumber = BillModel.SerialNumber;
        //        //        UnitOfWork.SalesSellExpensesService.Add(Expenses);
        //        //    }
        //        //}
        //        UnitOfWork.Commit();
        //        obj.Data = "Done";
        //        return Ok(obj);
        //    }
        //    catch (Exception ex)
        //    {
        //        string message = ex.Message;
        //        return BadRequest();
        //    }
        //}


        [Route("api/ItemConversion/DeleteItemConversion")]
        [HttpGet]
        public IActionResult DeleteItemConversion(int ConversionRecordId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.ItemConversion_BillService.Delete(ConversionRecordId);
                UnitOfWork.ItemConversion_ItemsService.Delete(ConversionRecordId);
               
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/ItemConversion/ConversionItemToTemp")]
        [HttpPost]
        public IActionResult ConversionItemToTemp(ItemConversion_BillVM BillModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                var addition = UnitOfWork.TempAdditionVoucherService.ConversionItemToTemp(BillModel);
                UnitOfWork.Commit();
                foreach (var items in BillModel.ConversionItemsTo)
                {
                    if (items != null)
                    {
                        if (items.ItemIdTo != 0 & items.ItemIdTo != null)
                        {
                            items.ConversionRecordID = addition.SerialNumber;
                            UnitOfWork.TempAdditionVoucherItemService.ConversionItemToTempItem(items);
                        }
                    }
                }
                UnitOfWork.Commit();
                var payment = UnitOfWork.TempPaymentVoucherService.ConversionItemFromTemp(BillModel);
                UnitOfWork.Commit();
                foreach (var items in BillModel.ConversionItemsFrom)
                {
                    if (items != null)
                    {
                        if (items.ItemIdFrom != 0 & items.ItemIdFrom != null)
                        {
                            items.ConversionRecordID = payment.SerialNumber;
                            UnitOfWork.TempPaymentVoucherItemService.ConversionItemFromTempItem(items);
                        }
                    }
                }
                BillModel.IsConverted = true;
                UnitOfWork.ItemConversion_BillService.Edit(BillModel);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}
