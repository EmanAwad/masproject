﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasProject.Data.DataAccess;
using MasProject.Domain;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Items;
using MasProject.Domain.ViewModel.Hierarchy;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MasProject.Server.Controllers.API.Items
{

    public class ItemsController : BaseController
    {
        private readonly IWebHostEnvironment environment;
        public ItemsController(DBContext context, IWebHostEnvironment environment) : base(context)
        {
            this.environment = environment;
        }
        [Route("api/Items/GetItem")]
        [HttpGet]
        public ActionResult GetItem()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ItemServices.GetItems().ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Items/GetItemStore")]
        [HttpGet]
        public ActionResult GetItemStore(int StoreId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ItemStoreServices.GetItemStore(StoreId);
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
        [Route("api/Items/GetItems")]
        [HttpGet]
        public ActionResult GetItems()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                var Items = UnitOfWork.ItemServices.GetAll().ToList();
                int index = 1;
                foreach(var itm in Items)
                {
                    itm.Index = index;
                    index++;
                }
                obj.Data = Items;
                return Ok(obj);
            }
            catch (Exception ex)
            {
                // return null;
                return BadRequest();
            }
        }

        [Route("api/Items/GetItemsList")]
        [HttpGet]
        public ActionResult GetItemsList()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                var Items = UnitOfWork.ItemServices.ItemSearch().ToList();
                //int index = 1;
                //foreach (var itm in Items)
                //{
                //    itm.Index = index;
                //    index++;
                //}
                obj.Data = Items;
                return Ok(obj);
            }
            catch (Exception ex)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Items/GetItemByAnyCode")]
        [HttpGet]
        public ActionResult GetItemByAnyCode(string AnyCode)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ItemServices.GetByAnyCode(AnyCode);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Items/GetItemPriceByDealId")]
        [HttpGet]
        public ActionResult GetItemPriceByDealId(int DealId,int ItemId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ItemServices.GetItemPriceByDealId(DealId,ItemId);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Items/GetItemImage")]
        [HttpGet]
        public ActionResult GetItemImage(int ItemId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ItemServices.GetItemImage(ItemId);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        //[Route("api/Items/GetItems_Store")]
        //[HttpGet]
        //public ActionResult GetItems_Store()
        //{
        //    var obj = new ResponseVM();
        //    try
        //    {
        //        obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
        //        obj.Message = ApiStatus.Success;
        //        obj.Data = UnitOfWork.ItemStoreServices.GetAll().ToList();
        //        return Ok(obj);
        //    }
        //    catch (Exception ex)
        //    {
        //        // return null;
        //        return BadRequest();
        //    }
        //}

        [Route("api/Items/GetSpecificItem")]
        [HttpGet]
        public ActionResult GetSpecificItem(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                ItemVM itm = UnitOfWork.ItemServices.Get(Id);
                itm.ItemCollection = UnitOfWork.ItemCollectionServices.GetAll().ToList();
                itm.ItemType = UnitOfWork.ItemTypeServices.GetAll().ToList();
                itm.Supplier = UnitOfWork.SupplierService.GetAll().ToList();
                itm.ItemStoreList = UnitOfWork.ItemStoreServices.GetByItemID(Id);
                obj.Data = itm;
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }

        [Route("api/Items/GetNationalCodeByID")]
        [HttpGet]
        public ActionResult GetNationalCodeByID(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ItemServices.GetCodeById(Id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Items/GetItemDataById")]
        [HttpGet]
        public ActionResult GetItemDataById(int DealId, int ItemId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                ItemBillVM item=UnitOfWork.ItemServices.GetItemDataById(ItemId);
                item.Price = UnitOfWork.ItemServices.GetItemPriceByDealId(DealId, ItemId);
                obj.Data = item;
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }

        //[Route("api/Items/GetSpecificItem_Store")]
        //[HttpGet]
        //public ActionResult GetSpecificItem_Store(int Id)
        //{
        //    var obj = new ResponseVM();
        //    try
        //    {
        //        obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
        //        obj.Message = ApiStatus.Success;

        //       // obj.Data = UnitOfWork.ItemStoreServices.Get(Id);
        //        return Ok(obj);



        //    }
        //    catch (Exception)
        //    {
        //        // return nullGetItemBranch
        //        return BadRequest();
        //    }
        //}
        [Route("api/Items/GetItemBranch")]
        [HttpGet]
        public ActionResult GetItemBranch(int ItemId, int BranchId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ItemStoreServices.GetItemQuantityByBranch(ItemId, BranchId);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Items/GetBranchAndMianStoreQuantity")]
        [HttpGet]
        public ActionResult GetMainStoreItemQuantity(int ItemId,int BranchId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ItemStoreServices.GetBranchAndMianStoreQuantity(ItemId,BranchId);

                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Items/AddItem")]
        [HttpPost]
        public IActionResult AddItem(ItemVM ItemModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                var ItemSaved = UnitOfWork.ItemServices.Add(ItemModel);
                UnitOfWork.Commit();
                foreach (var store in ItemModel.ItemStoreList)
                {
                    if (store.Quantity > 0)
                    {
                        store.ItemId = ItemSaved.ID;
                        store.CreatedDate = DateTime.Now;
                        UnitOfWork.ItemStoreServices.Add(store);
                    }
                }
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                var x = ex.Message;
                return BadRequest();
            }
        }

        //[Route("api/Items/AddItemStore")]
        //[HttpPost]
        //public IActionResult AddItemStore(Item_StoreVM ItemStoreModel)
        //{

        //    var obj = new ResponseVM();
        //    try
        //    {
        //        obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
        //        obj.Message = ApiStatus.Success;
        //        UnitOfWork.ItemStoreServices.Add(ItemStoreModel);
        //        UnitOfWork.Commit();
        //        obj.Data = "Done";
        //        return Ok(obj);
        //    }
        //    catch (Exception)
        //    {
        //        return BadRequest();
        //    }
        //}
        [Route("api/Items/CheckNameItem")]
        [HttpGet]
        public ActionResult CheckNameItem(string name, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ItemServices.IsExist(name, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }

        [Route("api/Items/EditItem")]
        [HttpPost]
        public IActionResult EditItem(ItemVM ItemModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.ItemServices.Edit(ItemModel);
                foreach (var store in ItemModel.ItemStoreList)
                {
                    Item_StoreVM ItemStoreObj = UnitOfWork.ItemStoreServices.GetByItemAndStoreID(ItemModel.ID, store.StoreId);
                    if (ItemStoreObj != null)
                    {
                        ItemStoreObj.Value = store.Value == null ? "" : store.Value;
                        ItemStoreObj.SheilfNo = store.SheilfNo;
                        ItemStoreObj.StoreId = store.StoreId;
                        ItemStoreObj.Quantity = store.Quantity;
                        ItemStoreObj.OrderLimit = store.OrderLimit == null ? 0 : store.OrderLimit;
                        UnitOfWork.ItemStoreServices.Edit(ItemStoreObj);
                    }
                    else
                    {
                        if (store.Quantity > 0)
                        {
                            store.ItemId = ItemModel.ID;
                            store.CreatedDate = DateTime.Now;
                            UnitOfWork.ItemStoreServices.Add(store);
                        }
                    }
                }
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        //[Route("api/Items/EditItemStore")]
        //[HttpPost]
        //public IActionResult EditItemStore(Item_StoreVM ItemStoreModel)
        //{

        //    var obj = new ResponseVM();
        //    try
        //    {
        //        obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
        //        obj.Message = ApiStatus.Success;
        //        UnitOfWork.ItemStoreServices.Edit(ItemStoreModel);
        //        UnitOfWork.Commit();
        //        obj.Data = "Done";
        //        return Ok(obj);
        //    }
        //    catch (Exception)
        //    {
        //        return BadRequest();
        //    }
        //}


        [Route("api/Items/DeleteItem")]
        [HttpGet]
        public IActionResult DeleteItem(int ID)
        {

            var obj = new ResponseVM();
            try
            {
                if (!UnitOfWork.ItemServices.IsItemUsed(ID))
                {
                    obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                    obj.Message = ApiStatus.Success;
                    UnitOfWork.ItemServices.Delete(ID);
                    UnitOfWork.Commit();
                    obj.Data = "Done";
                }
                else
                {
                    return BadRequest();
                }
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }



        [Route("api/Items/HideItem")]
        [HttpGet]
        public IActionResult HideItem(int ID)
        {

            var obj = new ResponseVM();
            try
            {


                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.ItemServices.Hide(ID);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/Items/GetDDLs")]
        [HttpGet]
        public IActionResult GetDDLs()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                ItemVM itm = new ItemVM { };

                itm.ItemCollection = UnitOfWork.ItemCollectionServices.GetAll().ToList();
                itm.ItemType = UnitOfWork.ItemTypeServices.GetAll().ToList();
                itm.Supplier = UnitOfWork.SupplierService.GetAll().ToList();

                obj.Data = itm;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/Items/CheckNationalParCode")]
        [HttpGet]
        public ActionResult CheckNationalParCode(string NationalParCod, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ItemServices.IsExistNationalParCode(NationalParCod, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }


        [Route("api/Items/CheckInternationalParCode")]
        [HttpGet]
        public ActionResult CheckInternationalParCode(string InternationalParCode, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ItemServices.IsExistInternationalParCode(InternationalParCode, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }

        [Route("api/Items/CheckSupplierParCode")]
        [HttpGet]
        public ActionResult CheckSupplierParCode(string SupplierParCode, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ItemServices.IsExistSupplierParCode(SupplierParCode, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }

        [Route("api/Items/CheckBoxParCode")]
        [HttpGet]
        public ActionResult CheckBoxParCode(string BoxParCode, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ItemServices.IsExistBoxParCode(BoxParCode, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }

        [Route("api/Items/CheckGuaranteeParCode")]
        [HttpGet]
        public ActionResult CheckGuaranteeParCode(string GuaranteeParCode, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ItemServices.IsExistGuaranteeParCode(GuaranteeParCode, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }

        //try 
        //[Route("api/Currency/EditCurrency")]
        //[HttpPost]
        //public IActionResult EditCurrency(CurrencyVM currModel)
        //{

        //    var obj = new ResponseVM();
        //    try
        //    {
        //        obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
        //        obj.Message = ApiStatus.Success;
        //        UnitOfWork.CurrencyServices.Edit(currModel);
        //        UnitOfWork.Commit();
        //        obj.Data = "Done";
        //        return Ok(obj);
        //    }
        //    catch (Exception)
        //    {
        //        return BadRequest();
        //    }
        //}
    }
}
