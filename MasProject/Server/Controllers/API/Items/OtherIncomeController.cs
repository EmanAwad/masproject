﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasProject.Data.DataAccess;
using MasProject.Domain;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Items;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MasProject.Server.Controllers.API.Items
{
    public class OtherIncomeController : BaseController
    {
        public OtherIncomeController(DBContext context) : base(context)
        {
        }
        [Route("api/OtherIncome/GetOtherIncome")]
        [HttpGet]
        public ActionResult GetOtherIncome()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.OtherIncomeService.GetAll().ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/OtherIncome/GetSpecificOtherIncome")]
        [HttpGet]
        public ActionResult GetSpecificOtherIncome(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.OtherIncomeService.Get(Id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/OtherIncome/AddOtherIncome")]
        [HttpPost]

        public IActionResult AddOtherIncome(OtherIncomeVM collecModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.OtherIncomeService.Add(collecModel);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }


        [Route("api/OtherIncome/EditOtherIncome")]
        [HttpPost]
        public IActionResult EditOtherIncome(OtherIncomeVM collecModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.OtherIncomeService.Edit(collecModel);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/OtherIncome/DeleteOtherIncome")]
        [HttpGet]
        public IActionResult DeleteOtherIncome(int ID)
        {

            var obj = new ResponseVM();
            try
            {
                if (!UnitOfWork.OtherIncomeService.IsOtherIncomeUsed(ID))
                {
                    obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                    obj.Message = ApiStatus.Success;
                    UnitOfWork.OtherIncomeService.Delete(ID);
                    UnitOfWork.Commit();
                }
                else
                {
                    return BadRequest();
                }
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/OtherIncome/CheckNameCollection")]
        [HttpGet]
        public ActionResult CheckNameCollection(string name, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.OtherIncomeService.IsExist(name, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }

        [Route("api/OtherIncome/CheckName")]
        [HttpGet]
        public ActionResult CheckName(string Name, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.OtherIncomeService.IsExistName(Name, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }





    }
}