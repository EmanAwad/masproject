﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasProject.Data.DataAccess;
using MasProject.Domain;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Items;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MasProject.Server.Controllers.API.Items
{
   
    public class ItemTypeController : BaseController
    {
        public ItemTypeController(DBContext context) : base(context)
        {
        }

        [Route("api/ItemType/GetItemTypes")]
        [HttpGet]
        public ActionResult GetItemTypes()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ItemTypeServices.GetAll().ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                
                return BadRequest();
            }
        }

        [Route("api/ItemType/GetSpecificItemType")]
        [HttpGet]
        public ActionResult GetSpecificItemType(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ItemTypeServices.Get(Id);
                return Ok(obj);
            }
            catch (Exception)
            {
               
                return BadRequest();
            }
        }
        [Route("api/ItemType/CheckNameItemType")]
        [HttpGet]
        public ActionResult CheckNameItemType(string name, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ItemTypeServices.IsExist(name, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }


        [Route("api/ItemType/AddItemType")]
        [HttpPost]

        public IActionResult AddItemType(ItemTypeVM TypeModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.ItemTypeServices.Add(TypeModel);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/ItemType/EditItemType")]
        [HttpPost]
        public IActionResult EditItemType(ItemTypeVM TypeModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.ItemTypeServices.Edit(TypeModel);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/ItemType/DeleteItemType")]
        [HttpGet]
        public IActionResult DeleteItemType(int ID)
        {

            var obj = new ResponseVM();
            try
            {


                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.ItemTypeServices.Delete(ID);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }


        [Route("api/ItemType/CheckArrangement")]
        [HttpGet]
        public ActionResult CheckArrangement(int arrangement, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ItemTypeServices.IsExistArrangement(arrangement, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
    }
}
