﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasProject.Data.DataAccess;
using MasProject.Domain;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Items;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MasProject.Server.Controllers.API.Items
{
    public class SellingExpensesController : BaseController
    {
        public SellingExpensesController(DBContext context) : base(context)
        {
        }
        [Route("api/SellingExpenses/GetSellingExpenses")]
        [HttpGet]
        public ActionResult GetSellingExpenses()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.SellingExpensesService.GetAll().ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/SellingExpenses/GetSpecificSellingExpenses")]
        [HttpGet]
        public ActionResult GetSpecificSellingExpenses(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.SellingExpensesService.Get(Id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/SellingExpenses/AddSellingExpenses")]
        [HttpPost]

        public IActionResult AddSellingExpenses(SellingExpensesVM collecModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.SellingExpensesService.Add(collecModel);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }


        [Route("api/SellingExpenses/EditSellingExpenses")]
        [HttpPost]
        public IActionResult EditSellingExpenses(SellingExpensesVM collecModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.SellingExpensesService.Edit(collecModel);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/SellingExpenses/DeleteSellingExpenses")]
        [HttpGet]
        public IActionResult DeleteSellingExpenses(int ID)
        {

            var obj = new ResponseVM();
            try
            {

                if (!UnitOfWork.SellingExpensesService.IsSellingExpensesUsed(ID))
                {
                    obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                    obj.Message = ApiStatus.Success;
                    UnitOfWork.SellingExpensesService.Delete(ID);
                    UnitOfWork.Commit();
                    obj.Data = "Done";
                }
                else
                {
                    return BadRequest();
                }
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/SellingExpenses/CheckNameCollection")]
        [HttpGet]
        public ActionResult CheckNameCollection(string name, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.SellingExpensesService.IsExist(name, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }

        [Route("api/SellingExpenses/CheckName")]
        [HttpGet]
        public ActionResult CheckArrangement(string Name, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.SellingExpensesService.IsExistName(Name, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
    }
}