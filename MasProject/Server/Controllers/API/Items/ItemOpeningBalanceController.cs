﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasProject.Data.DataAccess;
using MasProject.Domain;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Items;
using MasProject.Domain.ViewModel.Hierarchy;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using static MasProject.Domain.Enums.Enums;
using MasProject.Domain.ViewModel.TransVM;

namespace MasProject.Server.Controllers.API.Items
{

    public class ItemOpeningBalanceController : BaseController
    {
        public ItemOpeningBalanceController(DBContext context) : base(context)
        {
        }

        [Route("api/ItemOpeningBalance/GetItemOpeningBalance")]
        [HttpGet]
        public ActionResult GetItemOpeningBalance()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ItemOpeningBalanceService.GetAll().ToList();
                return Ok(obj);
            }
            catch (Exception ex)
            {
                // return null;
                return BadRequest();
            }
        }

        [Route("api/ItemOpeningBalance/GetSpecificItemOpeningBalance")]
        [HttpGet]
        public ActionResult GetSpecificItemOpeningBalance(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                ItemOpeningBalanceVM balance = UnitOfWork.ItemOpeningBalanceService.Get(Id);
                balance.ItemDetails = UnitOfWork.ItemOpeningBalanceDetailsService.GetByDocumentNumber(balance.DocumentNumber);
                obj.Data = balance;
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }

        [Route("api/ItemOpeningBalance/AddItemOpeningBalance")]
        [HttpPost]

        public IActionResult AddItemOpeningBalance(ItemOpeningBalanceVM OpeningBalanceModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                var modelSaved = UnitOfWork.ItemOpeningBalanceService.Add(OpeningBalanceModel);
                UnitOfWork.Commit();
                obj.Data = modelSaved.ID;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/ItemOpeningBalance/AddItemOpeningBalanceDetails")]
        [HttpPost]

        public IActionResult AddItemOpeningBalanceDetails(ItemOpeningBalanceDetailsVM OpeningBalanceModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                var modelSaved = UnitOfWork.ItemOpeningBalanceDetailsService.Add(OpeningBalanceModel);
                UnitOfWork.Commit();
                obj.Data = modelSaved.ID;
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }
        [Route("api/ItemOpeningBalance/EditItemOpeningBalance")]
        [HttpPost]
        public IActionResult EditItemOpeningBalance(ItemOpeningBalanceVM OpeningBalanceModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.ItemOpeningBalanceService.Edit(OpeningBalanceModel);
                UnitOfWork.Commit();
                UnitOfWork.ItemOpeningBalanceDetailsService.DeletePhysical(OpeningBalanceModel.DocumentNumber);
                UnitOfWork.Commit();
                foreach (var detail in OpeningBalanceModel.ItemDetails)
                {
                    if (detail != null)
                    {
                        if (detail.ItemId != null && detail.ItemId != 0)
                        {
                            detail.ItemDocumentNumber = OpeningBalanceModel.DocumentNumber;
                            UnitOfWork.ItemOpeningBalanceDetailsService.Add(detail);
                            ///////////////////////////////////////////////////trans///////////////////////////////
                            TransItemVM TransItem = new TransItemVM
                            {
                                ItemId = (int)detail.ItemId,
                                ItemType = TransItemTypeEnum.رصيد_اول_المدة,
                                Subtraction = 0,
                                CreateDate = DateTime.Now,
                                OpenBalFlag = true,
                                Addition = 0,
                                Date = OpeningBalanceModel.Date,
                                SerialNumber = OpeningBalanceModel.DocumentNumber,
                                IsDeleted = false,
                                Balance = detail.Quantity == null ? 0 : (decimal)detail.Quantity,
                                StoreId = (int)OpeningBalanceModel.StoreId,
                            };
                            bool checkreturn = UnitOfWork.TransItemService.Add(TransItem, detail.Quantity == null ? 0 : (decimal)detail.Quantity);
                            UnitOfWork.Commit();
                            //recacaluate
                            if (!checkreturn)
                            {
                                UnitOfWork.TransItemService.RecalculateItemBalance(TransItem.ItemId, (int)OpeningBalanceModel.StoreId, OpeningBalanceModel.DocumentNumber);
                            }
                            ///////////////////////////////////////////////////////////////////////////
                        }
                    }
                }
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }

        [Route("api/ItemOpeningBalance/DeleteItemOpeningBalance")]
        [HttpGet]
        public IActionResult DeleteItemOpeningBalance(int ID)
        {

            var obj = new ResponseVM();
            try
            {
                //int currobj;
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.ItemOpeningBalanceService.Delete(ID);
                UnitOfWork.ItemOpeningBalanceDetailsService.Delete(ID);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/ItemOpeningBalance/GenerateSerial")]
        [HttpGet]
        public ActionResult GenerateSerial()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                ItemOpeningBalanceVM NewModel = new ItemOpeningBalanceVM
                {
                    DocumentNumber = UnitOfWork.ItemOpeningBalanceService.GenerateSerial(),
                    EmployeeList = UnitOfWork.UserService.GetAllUsers().Select(s => new LookupKeyValueVM
                    {
                        ID = s.ID,
                        Name = s.UserName,
                    }).ToList(),
                    ItemList = UnitOfWork.ItemServices.GetItems().ToList(),
                    Date = DateTime.Now,
                    StoreList = UnitOfWork.StoreServices.GetStores().ToList(),
                };
                obj.Data = NewModel;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/ItemOpeningBalance/InsertItemTrans")]
        [HttpGet]
        public ActionResult InsertItemTrans()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                //get all not inserted trans items
                List<ItemOpeningBalanceVM> AllOpenBal = UnitOfWork.ItemOpeningBalanceService.GetAllForInsert().ToList();
                foreach (var OpenBal in AllOpenBal)
                {
                    OpenBal.ItemDetails = UnitOfWork.ItemOpeningBalanceDetailsService.GetByDocumentNumber(OpenBal.DocumentNumber);
                    if (OpenBal.ItemDetails.Count != 0)
                    {
                        foreach (var item in OpenBal.ItemDetails)
                        {
                            //check if exist in trans
                            if (!UnitOfWork.TransItemService.CheckExistForInsert((int)item.ItemId, (int)OpenBal.StoreId, OpenBal.DocumentNumber))
                            {
                                ///////////////////////////////////////////////////trans///////////////////////////////
                                TransItemVM TransItem = new TransItemVM
                                {
                                    ItemId = (int)item.ItemId,
                                    ItemType = TransItemTypeEnum.رصيد_اول_المدة,
                                    Subtraction = 0,
                                    CreateDate = DateTime.Now,
                                    OpenBalFlag = true,
                                    Addition = 0,
                                    Date = OpenBal.Date,
                                    SerialNumber = OpenBal.DocumentNumber,
                                    IsDeleted = false,
                                    Balance = item.Quantity == null ? 0 : (decimal)item.Quantity,
                                    StoreId = (int)OpenBal.StoreId,
                                };
                                bool checkreturn = UnitOfWork.TransItemService.Add(TransItem, item.Quantity == null ? 0 : (decimal)item.Quantity);
                                UnitOfWork.Commit();
                                //recacaluate
                                if (!checkreturn)
                                {
                                    UnitOfWork.TransItemService.RecalculateItemBalance(TransItem.ItemId, (int)OpenBal.StoreId, OpenBal.DocumentNumber);
                                }
                                ///////////////////////////////////////////////////////////////////////////
                                UnitOfWork.Commit();
                            }
                        }
                    }
                    
                }
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}

