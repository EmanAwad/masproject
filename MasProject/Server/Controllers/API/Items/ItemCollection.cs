﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasProject.Data.DataAccess;
using MasProject.Domain;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Items;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MasProject.Server.Controllers.API.Items
{

    public class ItemCollection : BaseController
    {
        public ItemCollection(DBContext context) : base(context)
        {
        }
        [Route("api/ItemCollection/GetItemCollection")]
        [HttpGet]
        public ActionResult GetItemCollection()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ItemCollectionServices.GetAll().ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/ItemCollection/GetSpecificItemCollection")]
        [HttpGet]
        public ActionResult GetSpecificItemCollection(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ItemCollectionServices.Get(Id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/ItemCollection/AddItemCollection")]
        [HttpPost]

        public IActionResult AddItemCollection(ItemCollectionVM collecModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.ItemCollectionServices.Add(collecModel);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }


        [Route("api/ItemCollection/EditItemCollection")]
        [HttpPost]
        public IActionResult EditItemCollection(ItemCollectionVM collecModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.ItemCollectionServices.Edit(collecModel);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/ItemCollection/DeleteItemCollection")]
        [HttpGet]
        public IActionResult DeleteItemCollection(int ID)
        {

            var obj = new ResponseVM();
            try
            {


                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.ItemCollectionServices.Delete(ID);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/ItemCollection/CheckNameCollection")]
        [HttpGet]
        public ActionResult CheckNameCollection(string name, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ItemCollectionServices.IsExist(name, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }

        [Route("api/ItemCollection/CheckArrangement")]
        [HttpGet]
        public ActionResult CheckArrangement(int arrangement, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ItemCollectionServices.IsExistArrangement(arrangement, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
    }
}
