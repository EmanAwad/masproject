﻿using MasProject.Data.DataAccess;
using Microsoft.AspNetCore.Mvc;
using MasProject.Domain.ViewModel;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using MasProject.Data.Models.Hierarchy;
using MasProject.Domain.Resources;
using System;
using MasProject.Domain.ViewModel.TransVM;
using MasProject.Data.Models.Trans;
using System.Data;
using Microsoft.AspNetCore.Hosting;
using GemBox.Spreadsheet;
using Microsoft.Extensions.Configuration;
using AspNetCore.Reporting;
using System.ComponentModel;
//using AspNetCore.Reporting;


namespace MasProject.Server.Controllers.API.Reports
{
    public class ClientsBalanceController : BaseController
    {
        private DBContext _context;
        private ExportController Export;
        private IWebHostEnvironment _hostingEnvironment;
        private readonly IConfiguration _config;
        public ClientsBalanceController(DBContext context , IWebHostEnvironment webHostEnvironment, IConfiguration config) : base(context)
        {
            _config = config ?? throw new System.ArgumentNullException(nameof(config));
            _context = context;
            Export = new ExportController();
            _hostingEnvironment = webHostEnvironment;
        }
        //[HttpGet("api/Report/ClientsBalance/exportexcel")]
        //public FileStreamResult exportexcel(int ClientId, DateTime? DateFrom)
        //{
        //    var book = new ExcelFile();
        //    var sheet = book.Worksheets.Add("ClientBalance");
        //    //  IList<TransClient> data = TransClientService.GetCurrentBalance(ClientId, DateFrom).ToList();
        //    //var obj = new ResponseVM();
        //    //obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
        //    //obj.Message = ApiStatus.Success;
        //    //obj.Data = UnitOfWork.TransClientService.GetCurrentBalance(ClientId, DateFrom);
        //    //  var tempcc = Request.Query;
        //    IList<TransClient> model = _context.TransClient.AsQueryable().Where(x => x.ClientId == ClientId).ToList();

        //    ClientBalanceReportVM trans = new ClientBalanceReportVM();
        //   trans.ClientName = _context.Client.FirstOrDefault(x => x.ID == ClientId).Name;
        //    //&& x.Date.Date <= DateFrom.Value.Date
        //     //&& !x.IsDeleted
        //    //trans.Balance = DateFrom.HasValue && DateFrom != DateTime.MinValue ? model != null ? model.Balance : 0 : 0;
         
        //    CellStyle style = sheet.Rows[0].Style;
        //    style.Font.Weight = ExcelFont.BoldWeight;
        //    style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
        //    sheet.Columns[0].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
        //    sheet.Columns[0].SetWidth(150, LengthUnit.Pixel);
        //    sheet.Columns[1].SetWidth(150, LengthUnit.Pixel);
        //    sheet.Columns[2].SetWidth(150, LengthUnit.Pixel);
        //    sheet.Columns[3].SetWidth(150, LengthUnit.Pixel);
        //    sheet.Columns[5].SetWidth(150, LengthUnit.Pixel);
        //    sheet.Columns[6].SetWidth(300, LengthUnit.Pixel);
        //    sheet.Cells["A1"].Value = "ShowDate";
        //    sheet.Cells["B1"].Value = "DisplayName";
        //    sheet.Cells["C1"].Value = "SerialNumber";
        //    sheet.Cells["D1"].Value = "Debit";
        //    sheet.Cells["E1"].Value = "Credit";
        //    sheet.Cells["F1"].Value = "Balance";
        //    for (int r = 1; r <= model.Count; r++)
        //    {
        //        TransClient item = model[r - 1];
        //        sheet.Cells[r, 0].Value = item.Date;
        //        sheet.Cells[r, 1].Value = item.Name;
        //        sheet.Cells[r, 2].Value = item.SerialNumber;
        //        sheet.Cells[r, 3].Value = item.Debit;
        //        sheet.Cells[r, 4].Value = item.Credit;
        //        sheet.Cells[r, 5].Value = item.Balance;
        //    }
        //    SaveOptions options = GetSaveOptions("XLSX");
        //    using (var stream = new MemoryStream())
        //    {
        //        book.Save(stream, options);
        //        var result = new FileStreamResult(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        //        result.FileDownloadName = $"ClientBalance.xlsx";

        //        return result;

        //        // return File(stream.ToArray(), options.ContentType, "ClientBalance." + "XLSX");
        //    }
        //    // return (IActionResult)trans;
        //}
        //private static SaveOptions GetSaveOptions(string format)
        //{
        //    switch (format.ToUpper())
        //    {
        //        case "XLSX":
        //            return SaveOptions.XlsxDefault;
        //        case "XLS":
        //            return SaveOptions.XlsDefault;
        //        case "ODS":
        //            return SaveOptions.OdsDefault;
        //        case "CSV":
        //            return SaveOptions.CsvDefault;
        //        case "HTML":
        //            return SaveOptions.HtmlDefault;
        //        case "PDF":
        //            return SaveOptions.PdfDefault;
        //        case "XPS":
        //        case "PNG":
        //        case "JPG":
        //        case "GIF":
        //        case "TIF":
        //        case "BMP":
        //        case "WMP":
        //            throw new InvalidOperationException("To enable saving to XPS or image format, add 'Microsoft.WindowsDesktop.App' framework reference.");
        //        default:
        //            throw new NotSupportedException();
        //    }

        //}

        ////[HttpGet("api/Report/ClientsBalance/exportexcel")]
        ////public FileStreamResult exportexcel()
        ////{
        ////    return Export.ToExcel(Export.ApplyQuery(_context.TransClient, Request.Query));
        ////}

        //[HttpGet("api/Report/ClientsBalance/exportPdf")]
        //public FileContentResult exportPdf()//
        //{
        //    try
        //    {
        //        ClientBalanceReportVM clientBalance = new ClientBalanceReportVM();
        //        var QueryClient = Export.ApplyQuery(_context.TransClient, Request.Query);
        //        var srchlList = QueryClient.Cast<TransClient>().ToList();
        //        if (srchlList.Count != 0)
        //        {
        //            //get client name from client id
        //            clientBalance.ClientName = UnitOfWork.ClientsService.Get(srchlList.FirstOrDefault().ClientId).Name;
        //        }
        //        ReportClientBalance repLip = new ReportClientBalance();
        //        byte[] PDFShape = repLip.PrepareReport(srchlList , clientBalance); 
        //        return File(PDFShape, "application/pdf", "كشف حساب العميل.pdf"); 
        //    }
        //    catch (System.Exception ex)
        //    {
        //        string message = ex.Message;
        //        return null;
        //    }
        //}


        [Route("api/ClientsBalance/RecalualteBalance")]
        [HttpGet]
        public ActionResult RecalualteBalance(int ClientId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                 UnitOfWork.TransClientService.UpdateClientBalance(ClientId);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }


        [Route("api/ClientsBalance/GetBalance")]
        [HttpGet]
        public ActionResult GetBalance(int ClientId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.TransClientService.GetBalance(ClientId);
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
        [Route("api/ClientsBalance/GetClientBalance")]
        [HttpPost]
        public ActionResult GetClientBalance(ClientBalanceReportVM clientBalance)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.TransClientService.GetClientBalance(clientBalance);
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
        [Route("api/ClientsBalance/GetCurrentBalance")]
        [HttpGet]
        public ActionResult GetCurrentBalance(int ClientId , DateTime? DateFrom)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.TransClientService.GetCurrentBalance(ClientId, DateFrom);
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
            //DataTable dt = new DataTable();
            //dt = UnitOfWork.TransClientService.GetCurrentBalance(ClientId, DateFrom);
            //SSRS Report 10/02
            //string mimetype = "";
            //int extension = 1;
            //var path = $"{this._webHostEnvironment.WebRootPath}\\Reports\\ClientBalanceRpt.rdlc";

            ////Dictionary<string, int> param1 = new Dictionary<string, int>();
            ////Dictionary<string, DateTime?> param2 = new Dictionary<string, DateTime?>();

            ////param1.Add("ClientID", ClientId);

            ////param2.Add("Date", DateFrom);
            //Dictionary<string, string> paramters = new Dictionary<string, string>();
            //paramters.Add("param", "blazor report");
            //LocalReport localReport = new LocalReport(path);

            //localReport.AddDataSource("ds", obj.Data);

            //var result =  localReport.Execute(RenderType.Pdf,extension , paramters , mimetype);

            //return File(result.MainStream, "app/pdf");
        }
        [Route("api/ClientsBalance/GetBill")]
        [HttpPost]
        public ActionResult GetBill(TransClientVM transClient)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.TransClientService.GetBill(transClient.SerialNumber, transClient.ClientTypeID);
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("api/ClientsBalance/ClientBalanceReport")]
        public IActionResult PurchaseBillsReport(int Id, bool IsPdf, DateTime DateFrom, DateTime DateTo)
        {
            try
            {
                ClientBalanceReportVM ClientBalance = new ClientBalanceReportVM
                {
                    ClientId = Id,
                    DateForm = DateFrom,
                    DateTo = DateTo
                };
                var dt = new DataTable();
                List<TransClientVM> sup = UnitOfWork.TransClientService.GetClientBalance(ClientBalance).ToList();

                dt = ToDataTable<TransClientVM>(sup);
                string mimetype = "";
                int extension = 1;
                if (string.IsNullOrWhiteSpace(_hostingEnvironment.WebRootPath))
                {
                    string RootPath = _config.GetValue<string>("RootPath");
                    _hostingEnvironment.WebRootPath = $"" + RootPath;
                }
                var path = $"{this._hostingEnvironment.WebRootPath}\\Reports\\ClientBalance.rdlc";

                string ClientName = UnitOfWork.ClientsService.Get(ClientBalance.ClientId).Name;
                string Footer = " تاريخ الطباعه : " + DateTime.Now.Date.ToString("dd-MM-yyyy") + " / الساعه: " + DateTime.Now.TimeOfDay;
                Dictionary<string, string> paramters = new Dictionary<string, string>();
                string ReportDate = DateTime.Now.Date.ToString("dd-MM-yyyy");
                paramters.Add("ClientName", ClientName);
                paramters.Add("FooterString", Footer);
                paramters.Add("ReportDate", ReportDate);
                LocalReport localreport = new LocalReport(path);
                localreport.AddDataSource("MasDataSet", dt);
                if (IsPdf)
                {
                    var result = localreport.Execute(RenderType.Pdf, extension, paramters, mimetype);
                    return File(result.MainStream, "application/pdf");
                }
                else
                {
                    //ExcelLibrary.DataSetHelper.CreateWorkbook("MyExcelFile.xls", ds);
                    //var stream = new MemoryStream();
                    //var result = new FileStreamResult(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    //result.FileDownloadName = $"Client Balance Report.xls";
                    var result = localreport.Execute(RenderType.ExcelOpenXml, extension, paramters, mimetype);
                    return File(result.MainStream, "application/excel");
                }
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return null;
            }

        }
        public DataTable ToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }
    }
}
