﻿using MasProject.Data.DataAccess;
using Microsoft.AspNetCore.Mvc;
using MasProject.Domain.ViewModel;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using MasProject.Data.Models.Hierarchy;
using MasProject.Domain.Resources;
using System;
using MasProject.Domain.ViewModel.TransVM;
using MasProject.Data.Models.Trans;

namespace MasProject.Server.Controllers.API.Reports
{
    public class ItemsBalanceController : BaseController
    {
        private DBContext _context;
        private ExportController Export;
        public ItemsBalanceController(DBContext context) : base(context)
        {
            _context = context;
            Export = new ExportController();
        }

        [HttpGet("api/Report/ItemsBalance/exportexcel")]
        public FileStreamResult exportexcel()
        {
            return Export.ToExcel(Export.ApplyQuery(_context.TransItem, Request.Query));
        }

        [HttpGet("api/Report/ItemsBalance/exportPdf")]
        public FileContentResult exportPdf()//
        {
            try
            {
                ItemBalanceReportVM ItemBalance = new ItemBalanceReportVM();
                var QueryItem = Export.ApplyQuery(_context.TransItem, Request.Query);
                var srchlList = QueryItem.Cast<TransItem>().ToList();
                if (srchlList.Count != 0)
                {
                    //get Item name from Item id
                    ItemBalance.ItemName = UnitOfWork.ItemServices.Get(srchlList.FirstOrDefault().ItemId).Name;
                }
                ReportItemBalance repLip = new ReportItemBalance();
                byte[] PDFShape = repLip.PrepareReport(srchlList, ItemBalance);
                return File(PDFShape, "application/pdf", "كشف حساب الصنف.pdf");
            }
            catch (System.Exception ex)
            {
                string message = ex.Message;
                return null;
            }
        }

        [Route("api/ItemsBalance/GetListDDL")]
        [HttpGet]
        public ActionResult GetListddl()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                ItemBalanceReportVM reportVM = new ItemBalanceReportVM
                {
                    StoreList = UnitOfWork.StoreServices.GetAll().ToList(),
                    ItemList = UnitOfWork.ItemServices.GetItemsSpeical().ToList(),
                };
                obj.Data = reportVM;
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }

        [Route("api/ItemsBalance/RecalualteBalance")]
        [HttpGet]
        public ActionResult RecalualteBalance(int ItemId,int StoreId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.TransItemService.UpdateItemBalance(ItemId, StoreId);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }


        [Route("api/ItemsBalance/GetBalance")]
        [HttpGet]
        public ActionResult GetBalance(int ItemId, int StoreId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.TransItemService.GetBalance(ItemId, StoreId);
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
        [Route("api/ItemsBalance/GetItemBalance")]
        [HttpPost]
        public ActionResult GetItemBalance(ItemBalanceReportVM ItemBalance)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                List< TransItemVM> Temp = UnitOfWork.TransItemService.GetItemBalance(ItemBalance).ToList();
                foreach (var item in Temp)
                {
                    item.DisplayName = item.DisplayName + " #" + item.ForeignSerialNumber;
                    switch (item.PersonType)
                    {
                        case 0:
                            item.PersonName = "";
                            continue;
                        case 1:
                            item.PersonName ="العميل: "+ UnitOfWork.ClientsService.Get(item.PersonId).Name ;
                            continue;
                        case 2:
                            item.PersonName = "المورد: " + UnitOfWork.SupplierService.Get(item.PersonId).Name;
                            continue;
                        case 3:
                            item.PersonName = "الموظف: " + UnitOfWork.EmployeeService.Get(item.PersonId).Name;
                            continue;
                        default:
                            break;
                    }
                }
                obj.Data = Temp;
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
        [Route("api/ItemsBalance/GetCurrentBalance")]
        [HttpGet]
        public ActionResult GetCurrentBalance(int ItemId, int StoreId, DateTime? DateFrom)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.TransItemService.GetCurrentBalance(ItemId,StoreId, DateFrom);
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
        [Route("api/ItemsBalance/GetBill")]
        [HttpPost]
        public ActionResult GetBill(TransItemVM transItem)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.TransItemService.GetBill(transItem.SerialNumber, transItem.ItemTypeID);
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
    }
}