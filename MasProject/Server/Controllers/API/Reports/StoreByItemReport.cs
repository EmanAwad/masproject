﻿using MasProject.Data.DataAccess;
using Microsoft.AspNetCore.Mvc;
using MasProject.Domain.ViewModel;
using System.Collections.Generic;
using System.Linq;
using MasProject.Domain.Resources;
using System;
using MasProject.Domain.ViewModel.TransVM;
using MasProject.Data.Models.Trans;
using static MasProject.Domain.Enums.Enums;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using MasProject.Domain.ViewModel.Items;

namespace MasProject.Server.Controllers.API.Reports
{
    public class StoreByItemReportController : BaseController
    {
        private DBContext _context;
        private ExportController Export;
        public StoreByItemReportController(DBContext context) : base(context)
        {
            _context = context;
            Export = new ExportController();
        }

        [HttpGet("api/Report/StoreByItemReport/exportexcel")]
        public FileStreamResult exportexcel()
        {
            return Export.ToExcel(Export.ApplyQuery(_context.TransItem, Request.Query));
        }

        [HttpGet("api/Report/StoreByItemReport/exportPdf")]
        public FileContentResult exportPdf()//
        {
            try
            {
                ItemBalanceReportVM ItemBalance = new ItemBalanceReportVM();
                var QueryItem = Export.ApplyQuery(_context.TransItem, Request.Query);
                var srchlList = QueryItem.Cast<TransItem>().ToList();
                if (srchlList.Count != 0)
                {
                    //get Item name from Item id
                    ItemBalance.ItemName = UnitOfWork.ItemServices.Get(srchlList.FirstOrDefault().ItemId).Name;
                }
                ReportItemBalance repLip = new ReportItemBalance();
                byte[] PDFShape = repLip.PrepareReport(srchlList, ItemBalance);
                return File(PDFShape, "application/pdf", "كشف حساب الصنف.pdf");
            }
            catch (System.Exception ex)
            {
                string message = ex.Message;
                return null;
            }
        }

        [Route("api/StoreByItemReport/GetListDDL")]
        [HttpGet]
        public ActionResult GetListddl()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                ItemBalanceReportVM reportVM = new ItemBalanceReportVM
                {
                    ItemList = UnitOfWork.ItemServices.GetItemsSpeical().ToList(),
                    StoreList = UnitOfWork.StoreServices.GetAll().ToList(),
                };
                obj.Data = reportVM;
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }


        [Route("api/StoreByItemReport/GetStoreByItemReport")]
        [HttpPost]
        public ActionResult GetStoreReport(ItemBalanceReportVM ItemBalance)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                List<StoreReportVM> Temp = new List<StoreReportVM>();
                if (ItemBalance.ItemId != 0)
                {
                    Temp.AddRange(CombineList(ItemBalance.ItemId, ItemBalance.StoreId, ItemBalance.DateForm, ItemBalance.DateTo));
                }
                obj.Data = Temp;
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        List<StoreReportVM> CombineList(int ItemId,int StoreId,DateTime? DateForm,DateTime? DateTo)
        {
            List<StoreReportVM> Temp = new List<StoreReportVM>();
            Temp = UnitOfWork.SalesItemsService.GetByItemId(ItemId).ToList();
            if (StoreId != 0)
            {
                Temp = Temp.Where(x => x.StoreId == StoreId).ToList();
            }
            foreach (var item in Temp)
            {
                var TempObj = UnitOfWork.SalesBillsService.GetIdBySerial(item.SerialNumber);
                if (TempObj == null)
                {
                    continue;
                }
                if (DateForm != null)
                {
                    if (TempObj.Date <= DateForm)
                    {
                        TempObj = null;
                    }
                }
                if (TempObj == null)
                {
                    continue;
                }
                if (DateTo != null)
                {
                    if (TempObj.Date >= DateTo)
                    {
                        TempObj = null;
                    }
                }
                if (TempObj == null)
                {
                    continue;
                }
                item.PersonId = TempObj.ClientId;
                item.PersonName = UnitOfWork.ClientsService.Get(item.PersonId).Name;
                item.BranchId = (int)TempObj.BranchId;
                item.BranchName = UnitOfWork.BranchServices.Get(item.BranchId).Name;
                item.ItemName = UnitOfWork.ItemServices.Get(item.ItemId).Name;
                List<SalesBillStoresVM> tempStores = UnitOfWork.SalesBillStoresService.GetBySerial(item.SerialNumber);
                int TempStoreId = (int)tempStores.FirstOrDefault(x => x.Identifer.ToString() == item.StoreIdentifier).StoreId;
                item.StoreId = TempStoreId;
                item.StoreName = UnitOfWork.StoreServices.Get(TempStoreId).Name;
                item.ShowDate = TempObj.Date;
                item.TransactionType = TransClientTypeEnum.مبيعات;
                item.CreateDate = TempObj.CreatedDate;
            }
            return Temp;
        }
        [Route("api/StoreByItemReport/GetBill")]
        [HttpPost]
        public ActionResult GetBill(StoreReportVM transItem)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                //case sales only
                if (transItem.TransactionType == TransClientTypeEnum.مبيعات)
                {
                    obj.Data = UnitOfWork.SalesBillsService.GetIdBySerial(transItem.SerialNumber);
                }
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
    }
}