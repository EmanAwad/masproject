﻿using MasProject.Data.DataAccess;
using Microsoft.AspNetCore.Mvc;
using MasProject.Domain.ViewModel;
using System.IO;
using MasProject.Domain.ViewModel.Hierarchy;
using System.Collections.Generic;
using System.Linq;
using MasProject.Data.Models.Hierarchy;
using MasProject.Domain.Resources;
using System;
using MasProject.Domain.ViewModel.TransVM;
using MasProject.Data.Models.Trans;

namespace MasProject.Server.Controllers.API.Reports
{
    public class EmployeeBalanceController : BaseController
    {
        private DBContext _context;
        private ExportController Export;
        public EmployeeBalanceController(DBContext context) : base(context)
        {
            _context = context;
            Export = new ExportController();
        }

        [HttpGet("api/Report/EmployeeBalance/exportexcel")]
        public FileStreamResult exportexcel()
        {
            return Export.ToExcel(Export.ApplyQuery(_context.TransEmployee, Request.Query));
        }

        [HttpGet("api/Report/EmployeeBalance/exportPdf")]
        public FileContentResult exportPdf()//
        {
            try
            {
                EmpolyeeBalanceReportVM EmployeeBalance = new EmpolyeeBalanceReportVM();
                var QueryCurreny = Export.ApplyQuery(_context.TransEmployee, Request.Query);
                var srchlList = QueryCurreny.Cast<TransEmployee>().ToList();
                if (srchlList.Count != 0)
                {
                    //get Employee name from Employee id
                    EmployeeBalance.EmployeeName = UnitOfWork.EmployeeService.Get(srchlList.FirstOrDefault().EmployeeId).Name;
                }
                ReportEmployeeBalance repLip = new ReportEmployeeBalance();
                byte[] PDFShape = repLip.PrepareReport(srchlList, EmployeeBalance);
                return File(PDFShape, "application/pdf", "كشف حساب الموظف.pdf");
            }
            catch (System.Exception ex)
            {
                string message = ex.Message;
                return null;
            }
        }

        [Route("api/EmployeeBalance/RecalualteBalance")]
        [HttpGet]
        public ActionResult RecalualteBalance(int EmployeeId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.TransEmployeeService.UpdateEmployeeBalance(EmployeeId);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [Route("api/EmployeeBalance/GetBalance")]
        [HttpGet]
        public ActionResult GetBalance(int EmployeeId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.TransEmployeeService.GetBalance(EmployeeId);
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
        [Route("api/EmployeeBalance/GetCurrentBalance")]
        [HttpGet]
        public ActionResult GetCurrentBalance(int EmployeeId, DateTime? DateFrom)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.TransEmployeeService.GetCurrentBalance(EmployeeId, DateFrom);
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
        [Route("api/EmployeeBalance/GetEmployeeBalance")]
        [HttpPost]
        public ActionResult GetEmployeeBalance(EmpolyeeBalanceReportVM EmployeeBalance)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.TransEmployeeService.GetEmployeeBalance(EmployeeBalance);
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
        [Route("api/EmployeeBalance/GetBill")]
        [HttpPost]
        public ActionResult GetBill(TransEmployeeVM TransEmployee)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.TransEmployeeService.GetBill(TransEmployee.SerialNumber, TransEmployee.EmployeeTypeID);
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
    }
}
