﻿using MasProject.Data.DataAccess;
using Microsoft.AspNetCore.Mvc;
using MasProject.Domain.ViewModel;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using MasProject.Data.Models.Hierarchy;
using MasProject.Domain.Resources;
using System;
using MasProject.Domain.ViewModel.TransVM;
using MasProject.Data.Models.Trans;
using static MasProject.Domain.Enums.Enums;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using MasProject.Domain.ViewModel.Items;

namespace MasProject.Server.Controllers.API.Reports
{
    public class StoreController : BaseController
    {
        private DBContext _context;
        private ExportController Export;
        public StoreController(DBContext context) : base(context)
        {
            _context = context;
            Export = new ExportController();
        }

        [HttpGet("api/Report/StoreReport/exportexcel")]
        public FileStreamResult exportexcel()
        {
            return Export.ToExcel(Export.ApplyQuery(_context.SalesBills, Request.Query));
        }

        [HttpGet("api/Report/StoreReport/exportPdf")]
        public FileContentResult exportPdf()//
        {
            try
            {
                ItemBalanceReportVM ItemBalance = new ItemBalanceReportVM();
                var QueryItem = Export.ApplyQuery(_context.TransItem, Request.Query);
                var srchlList = QueryItem.Cast<TransItem>().ToList();
                if (srchlList.Count != 0)
                {
                    //get Item name from Item id
                    ItemBalance.ItemName = UnitOfWork.ItemServices.Get(srchlList.FirstOrDefault().ItemId).Name;
                }
                ReportItemBalance repLip = new ReportItemBalance();
                byte[] PDFShape = repLip.PrepareReport(srchlList, ItemBalance);
                return File(PDFShape, "application/pdf", "كشف حساب الصنف.pdf");
            }
            catch (System.Exception ex)
            {
                string message = ex.Message;
                return null;
            }
        }

        [Route("api/StoreReport/GetListDDL")]
        [HttpGet]
        public ActionResult GetListddl()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                ItemBalanceReportVM reportVM = new ItemBalanceReportVM
                {
                    ItemList = UnitOfWork.ItemServices.GetItemsSpeical().ToList(),
                    TransactionTypeList = ((StoreTypeEnum[])Enum.GetValues(typeof(StoreTypeEnum))).ToList(),
                    BranchList = UnitOfWork.BranchServices.GetAll().ToList(),
                    ItemCollectionList = UnitOfWork.ItemCollectionServices.GetAll().ToList(),
                    StoreList = UnitOfWork.StoreServices.GetAll().ToList(),
                    PersionList=UnitOfWork.ClientsService.GetClients().ToList(),
                };
                obj.Data = reportVM;
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }


        [Route("api/StoreReport/GetStoreReport")]
        [HttpPost]
        public ActionResult GetStoreReport(ItemBalanceReportVM ItemBalance)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                List<StoreReportVM> Temp = new List<StoreReportVM>();
                //case sales only
                if (ItemBalance.TransactionTypeId == 1)
                {
                    if (ItemBalance.PersionId != 0)
                    {
                        Temp.AddRange(CombineList(ItemBalance.PersionId, ItemBalance.StoreId, ItemBalance.BranchId, ItemBalance.DateForm, ItemBalance.DateTo));
                    }
                    else
                    {
                        List<LookupKeyValueVM> clients = UnitOfWork.ClientsService.GetClients().ToList();
                        foreach (var item in clients)
                        {
                            Temp.AddRange(CombineList(item.ID, ItemBalance.StoreId, ItemBalance.BranchId, ItemBalance.DateForm, ItemBalance.DateTo));
                        }
                    }
                }
                obj.Data = Temp;
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
        List<StoreReportVM> CombineList(int PersionId, int StoreId, int BranchId, DateTime? DateForm, DateTime? DateTo)
        {
            List<StoreReportVM>  Temp = new List<StoreReportVM>();
            List<SalesBillsVM> ReturnTemp = new List<SalesBillsVM>();
            ReturnTemp = UnitOfWork.SalesBillsService.GetAllByClient(PersionId).ToList();
            if (BranchId != 0)
            {
                ReturnTemp = ReturnTemp.Where(x => x.BranchId == BranchId).ToList();
            }
            foreach (var itemSales in ReturnTemp)
            {
                bool checknotExist = false;
                if (DateForm != null)
                {
                    if (itemSales.Date <= DateForm)
                    {
                        checknotExist = true;
                    }
                }
                if (checknotExist == true)
                {
                    continue;
                }
                if (DateTo != null)
                {
                    if (itemSales.Date >= DateTo)
                    {
                        checknotExist = true;
                    }
                }
                if (checknotExist == true)
                {
                    continue;
                }
                StoreReportVM item = new StoreReportVM();
                item.PersonId = PersionId;
                if (PersionId == 511)
                {
                    string dosomtheing = ";";
                }
                item.PersonName = UnitOfWork.ClientsService.Get(item.PersonId).Name;
                item.ShowDate = itemSales.Date;
                item.TransactionType = TransClientTypeEnum.مبيعات;
                item.CreateDate = itemSales.CreatedDate;
                item.Total = itemSales.TotalAfterTax;
                item.SerialNumber = (int)itemSales.SerialNumber;
                item.BranchId = (int)itemSales.BranchId;
                item.BranchName = UnitOfWork.BranchServices.Get(item.BranchId).Name;
                List<SalesBillStoresVM> tempStores = UnitOfWork.SalesBillStoresService.GetBySerial((int)itemSales.SerialNumber);

                if (StoreId != 0)
                {
                    var TempStoreId = tempStores.FirstOrDefault(x => x.StoreId == StoreId);
                    if (TempStoreId != null)
                    {
                        item.StoreId = StoreId;
                        item.StoreName = UnitOfWork.StoreServices.Get(StoreId).Name;
                        Temp.Add(item);
                    }
                }
                else
                {
                    foreach (var itemStore in tempStores)
                    {
                        item.StoreId = (int)itemStore.StoreId;
                        item.StoreName = UnitOfWork.StoreServices.Get(item.StoreId).Name;
                        Temp.Add(item);
                    }
                }
            }
            return Temp;
        }


        [Route("api/StoreReport/GetBill")]
        [HttpPost]
        public ActionResult GetBill(StoreReportVM transItem)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                //case sales only
                if (transItem.TransactionType == TransClientTypeEnum.مبيعات)
                {
                    ItemBalanceReportVM trans = new ItemBalanceReportVM();
                    trans.PageName = "SalesBillEdit";
                    trans.BillId = UnitOfWork.SalesBillsService.GetIdBySerial(transItem.SerialNumber).ID;
                    obj.Data = trans;
                }
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
    }
}