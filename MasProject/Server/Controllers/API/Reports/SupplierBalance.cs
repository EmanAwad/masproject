﻿using System;
using System.Linq;
using MasProject.Data.DataAccess;
using MasProject.Data.Models.TransTables;
using MasProject.Domain.ViewModel.TransVM;
using Microsoft.AspNetCore.Mvc;
using MasProject.Domain.ViewModel;
using MasProject.Domain.Resources;
using System.Data;
using Microsoft.Extensions.Hosting.Internal;
using AspNetCore.Reporting;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System.Text;

namespace MasProject.Server.Controllers.API.Reports
{

    public class SupplierBalanceController : BaseController
    {
        private DBContext _context;
      //  private ExportController Export;
        private IWebHostEnvironment _hostingEnvironment;
        private readonly IConfiguration _config;
        public SupplierBalanceController(DBContext context, IWebHostEnvironment HostingEnvironment, IConfiguration config): base(context)
        {
            _config = config ?? throw new System.ArgumentNullException(nameof(config));
            this._hostingEnvironment = HostingEnvironment;
            System.Text.Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            _context = context;
            //Export = new ExportController();
        }

        //[HttpGet("api/Report/SupplierBalance/exportexcel")]
        //public FileStreamResult exportexcel()
        //{
        //    return Export.ToExcel(Export.ApplyQuery(_context.TransSupplier, Request.Query));
        //}

        //[HttpGet("api/Report/SupplierBalance/exportPdf")]
        //public FileContentResult exportPdf()//
        //{
        //    try
        //    {
        //        SupplierBalanceReportVM SupplierBalance = new SupplierBalanceReportVM();
        //        var QuerySupplier = Export.ApplyQuery(_context.TransSupplier, Request.Query);
        //        var srchlList = QuerySupplier.Cast<TransSupplier>().ToList();
        //        if (srchlList.Count != 0)
        //        {
        //            //get Supplier name from Supplier id
        //            SupplierBalance.SupplierName = UnitOfWork.SupplierService.Get(srchlList.FirstOrDefault().SupplierId).Name;
        //        }
        //        ReportSupplierBalance repLip = new ReportSupplierBalance();
        //        byte[] PDFShape = repLip.PrepareReport(srchlList, SupplierBalance);
        //        return File(PDFShape, "application/pdf", "كشف حساب المورد.pdf");
        //    }
        //    catch (System.Exception ex)
        //    {
        //        string message = ex.Message;
        //        return null;
        //    }
        //}

        [Route("api/SupplierBalance/RecalualteBalance")]
        [HttpGet]
        public ActionResult RecalualteBalance(int SupplierId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.TransSupplierService.UpdateSupplierBalance(SupplierId);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [Route("api/SupplierBalance/GetBalance")]
        [HttpGet]
        public ActionResult GetBalance(int SupplierId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.TransSupplierService.GetBalance(SupplierId);
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
        [Route("api/SupplierBalance/GetCurrentBalance")]
        [HttpGet]
        public ActionResult GetCurrentBalance(int SupplierId, DateTime? DateFrom)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.TransSupplierService.GetCurrentBalance(SupplierId, DateFrom);
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
        [Route("api/SupplierBalance/GetSupplierBalance")]
        [HttpPost]
        public ActionResult GetSupplierBalance(SupplierBalanceReportVM SupplierBalance)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.TransSupplierService.GetSupplierBalance(SupplierBalance);
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
        [Route("api/SupplierBalance/GetBill")]
        [HttpPost]
        public ActionResult GetBill(TransSupplierVM transSupplier)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.TransSupplierService.GetBill(transSupplier.SerialNumber, transSupplier.SupplierTypeID);
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("api/SupplierBalance/SupplierBalanceReport")]
        public IActionResult PurchaseBillsReport(int Id,bool IsPdf,DateTime DateFrom, DateTime DateTo)
        {
            try
            {
                SupplierBalanceReportVM SupplierBalance= new SupplierBalanceReportVM
                {
                    SupplierId = Id,
                    DateForm = DateFrom,
                    DateTo= DateTo
                };
                var dt = new DataTable();
                List<TransSupplierVM> sup = UnitOfWork.TransSupplierService.GetSupplierBalance(SupplierBalance).ToList();
               
                dt = ToDataTable<TransSupplierVM>(sup);
                string mimetype = "";
                int extension = 1;
                if (string.IsNullOrWhiteSpace(_hostingEnvironment.WebRootPath))
                {
                    string RootPath = _config.GetValue<string>("RootPath");
                    _hostingEnvironment.WebRootPath = $"" + RootPath;
                }
                var path = $"{this._hostingEnvironment.WebRootPath}\\Reports\\SupplierBalance.rdlc";

                string SupplierName = UnitOfWork.SupplierService.Get(SupplierBalance.SupplierId).Name;
                string Footer = " تاريخ الطباعه : " + DateTime.Now.Date.ToString("dd-MM-yyyy") + " / الساعه: " + DateTime.Now.TimeOfDay;
                Dictionary<string, string> paramters = new Dictionary<string, string>();
                string ReportDate = DateTime.Now.Date.ToString("dd-MM-yyyy");
                paramters.Add("SupplierName", SupplierName);
                paramters.Add("FooterString", Footer);
                paramters.Add("ReportDate", ReportDate);
                LocalReport localreport = new LocalReport(path);
                localreport.AddDataSource("MasDataSet", dt);
                if (IsPdf)
                {
                    var result = localreport.Execute(RenderType.Pdf, extension, paramters, mimetype);
                    return File(result.MainStream, "application/pdf");
                }
                else
                {
                    //ExcelLibrary.DataSetHelper.CreateWorkbook("MyExcelFile.xls", ds);
                    //var stream = new MemoryStream();
                    //var result = new FileStreamResult(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    //result.FileDownloadName = $"Supplier Balance Report.xls";
                    var result = localreport.Execute(RenderType.ExcelOpenXml, extension, paramters, mimetype);
                    return File(result.MainStream, "application/excel");
                }
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return null;
            }

        }
        public DataTable ToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }
    }
}