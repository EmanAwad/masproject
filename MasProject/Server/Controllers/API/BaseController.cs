﻿using MasProject.Data.DataAccess;
using MasProject.Domain;
using Microsoft.AspNetCore.Mvc;
using MasProject.Domain.ViewModel;
using System.Collections.Generic;
using System.Linq;

namespace MasProject.Server.Controllers.API
{
    [ApiController]
   // [EnableCors("AllowOrigin")]
    public class BaseController : ControllerBase
    {
       // private readonly DBContext _context;
        public UnitOfWork UnitOfWork { get; set; }
        public static List<LookupItem> ItemList = new List<LookupItem>();
        public BaseController(DBContext context)
        {
            // _context = context;
            UnitOfWork = new UnitOfWork(context);
          //  ItemList = UnitOfWork.ItemServices.GetItems().ToList();
        }

    }
}
