﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasProject.Data.DataAccess;
using MasProject.Domain;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Accounting;

using Microsoft.AspNetCore.Mvc;

namespace MasProject.Server.Controllers.API.Accounting
{
    public class GuideController : BaseController
    {
        public GuideController(DBContext context) : base(context)
        {
        }
        [Route("api/Guide/GetGuide")]
        [HttpGet]
        public ActionResult GetGuide()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.AccountingGuideService.GetAllAccounts().ToList();
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [Route("api/Guide/GetCostCenters")]
        [HttpGet]
        public ActionResult GetCostCenters()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.AccountingGuideService.GetCostCenters().ToList();
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
        [Route("api/Guide/GetAccountName")]
        [HttpGet]
        public ActionResult GetAccountName(int code)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.AccountingGuideService.GetAccountNameByCode(code);
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
        [Route("api/Guide/GetAccountCode")]
        [HttpGet]
        public ActionResult GetAccountCode(int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.AccountingGuideService.GetAccountCodeById(id);
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
        [Route("api/Guide/GetListDDL")]
        [HttpGet]
        public ActionResult GetListddl()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                AccountingGuideVM Guide = new AccountingGuideVM();
                Guide.AccountingGuideList= UnitOfWork.AccountingGuideService.GetAll().ToList();
              //  Guide.AccountCode = UnitOfWork.AccountingGuideService.GenerateParentCode();
                obj.Data = Guide;
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
        [Route("api/Guide/GenerateChildCode")]
        [HttpGet]
        public ActionResult GenerateChildCode(int ParentId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                AccountingGuideVM Guide = UnitOfWork.AccountingGuideService.GenerateChildCode(ParentId);
                obj.Data = Guide;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/Guide/GetSpecificGuide")]
        [HttpGet]
        public ActionResult GetSpecificGuide(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                AccountingGuideVM Guide = UnitOfWork.AccountingGuideService.Get(Id);
                Guide.AccountingGuideList = UnitOfWork.AccountingGuideService.GetAll().ToList();
                obj.Data = Guide;
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }
        [Route("api/Guide/AddGuide")]
        [HttpPost]

        public IActionResult AddGuide(AccountingGuideVM Model)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                var ModelSaved = UnitOfWork.AccountingGuideService.Add(Model);
                UnitOfWork.Commit();
                obj.Data = ModelSaved.ID;
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }


        [Route("api/Guide/EditGuide")]
        [HttpPost]
        public IActionResult EditGuide(AccountingGuideVM Model)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.AccountingGuideService.Edit(Model);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }
        [Route("api/Guide/DeleteGuide")]
        [HttpGet]
        public IActionResult DeleteGuide(int ID)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.AccountingGuideService.Delete(ID);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}