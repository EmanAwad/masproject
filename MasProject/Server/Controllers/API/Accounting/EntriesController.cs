﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasProject.Data.DataAccess;
using MasProject.Data.Models.Accounting;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Accounting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MasProject.Server.Controllers.API.Accounting
{

    public class EntriesController : BaseController
    {
        public EntriesController(DBContext context) : base(context)
        {
        }
        [Route("api/Entries/GetEntries")]
        [HttpGet]
        public ActionResult GetEntries()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.EntriesService.GetAll().ToList(); //UnitOfWork.EntriesDetailsService.GetAll().ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/Entries/GetListDDL")]
        [HttpGet]
        public ActionResult GetListddl()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                EntriesVM Entries = new EntriesVM
                {
                    EntriesTypeList = UnitOfWork.EntriesTypeService.GetAll().ToList(),
                    CostsCentersList = UnitOfWork.AccountingGuideService.GetCostCenters().ToList(),
                    GuidesList = UnitOfWork.AccountingGuideService.GetAllAccounts().ToList(),
                    EntriesNo = UnitOfWork.EntriesService.GenerateEntryNo(),
                    EntryDate = DateTime.Now,
                };
                obj.Data = Entries;
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }

        [Route("api/Entries/GetSpecificEntry")]
        [HttpGet]
        public ActionResult GetSpecificEntry(int ID)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                EntriesVM entry = UnitOfWork.EntriesService.Get(ID);
                entry.EntriesDetailsList = UnitOfWork.EntriesDetailsService.Get(ID);
               // entry.B = UnitOfWork.BranchServices.GetAll().ToList();
                obj.Data = entry;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/Entries/AddEntries")]
        [HttpPost]

        public IActionResult AddEntries(EntriesVM Model)
        {
            try
            {
                var obj = new ResponseVM();

                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                Entries saved = UnitOfWork.EntriesService.Add(Model);
                UnitOfWork.Commit();
                foreach (var entry in Model.EntriesDetailsList)
                {
                    if (entry != null)
                    {
                        if (entry.AccountNo != 0 && entry.AccountNo !=null)
                        {
                            entry.EntriesId = saved.ID;
                            //if (entry.TotalCR > entry.TotalDR)
                            //{
                            //    entry.DRCR_Difference = entry.TotalCR - entry.TotalDR;
                            //}
                            //else
                            //{
                            //    entry.DRCR_Difference = entry.TotalDR - entry.TotalCR;
                            //}
                            //entry.TotalCR = 0;
                            //entry.TotalDR = 0;
                            entry.DRCR_Difference = 0;
                            UnitOfWork.EntriesDetailsService.Add(entry);
                        }
                    }
                }
                UnitOfWork.Commit();
                obj.Data = saved.ID;

               // obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.InnerException.Message;
                return BadRequest();
            }
        }
        [Route("api/Entries/EditEntries")]
        [HttpPost]
        public IActionResult EditEntries(EntriesVM Model)
        {
            try
            {
                var obj = new ResponseVM();

                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                Entries saved = UnitOfWork.EntriesService.Edit(Model);
                UnitOfWork.EntriesDetailsService.Delete(Model.ID);
                UnitOfWork.Commit();
                foreach (var entry in Model.EntriesDetailsList)
                {
                    if (entry != null)
                    {
                        if (entry.AccountNo != 0 && entry.AccountNo != null)
                        {
                            entry.EntriesId = saved.ID;
                            entry.DRCR_Difference = entry.Credit - entry.Debit;
                            UnitOfWork.EntriesDetailsService.Add(entry);
                            //entry.TotalCR = 0;
                            //entry.TotalDR = 0;
                            //UnitOfWork.EntriesDetailsService.Edit(entry);
                        }
                    }
                }
                UnitOfWork.Commit();
                obj.Data = saved.ID;

                // obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.InnerException.Message;
                return BadRequest();
            }
        }

        //[Route("api/Entries/EditEntries")]
        //[HttpPost]
                

        [Route("api/Entries/DeleteEntries")]
        [HttpGet]
        public IActionResult DeleteEntries(int ID)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.EntriesService.Delete(ID);
                UnitOfWork.EntriesDetailsService.Delete(ID);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }

        [Route("api/Entries/GenerateEntryNo")]
        [HttpGet]
        public ActionResult GenerateEntryNo()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.EntriesService.GenerateEntryNo();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}