﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Accounting;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Accounting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MasProject.Server.Controllers.API.Accounting
{
    [Route("api/[controller]")]
    [ApiController]
    public class EntryStructureController : BaseController
    {
        public EntryStructureController(DBContext context) : base(context)
        {
        }

        [Route("api/EntryStructure/GetEntryStructures")]
        [HttpGet]
        public ActionResult GetEntryStructures()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.EntriesStructureService.GetAll().ToList(); 
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }


        [Route("api/EntryStructure/GetSpecificEntryStructure")]
        [HttpGet]
        public ActionResult GetSpecificEntryStructure(int ID)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                EntriesStructuringVM entry = UnitOfWork.EntriesStructureService.Get(ID);
                entry.EntriesStructuringList = UnitOfWork.EntriesStructure_DetailsService.Get(ID);
               
                obj.Data = entry;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }


        [Route("api/EntryStructure/AddEntriesStructure")]
        [HttpPost]

        public IActionResult AddEntriesStructure(EntriesStructuringVM Model)
        {
            try
            {
                var obj = new ResponseVM();

                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                EntriesStructuring saved = UnitOfWork.EntriesStructureService.Add(Model);
                UnitOfWork.Commit();
                foreach (var entry in Model.EntriesStructuringList)
                {
                    if (entry != null)
                    {
                        entry.EntriesStructuringId = saved.ID;
                        entry.DRCR_Difference = entry.Credit - entry.Debit;
                        //entry.TotalCR = 0;
                        //entry.TotalDR = 0;
                        UnitOfWork.EntriesStructure_DetailsService.Add(entry);
                    }
                }
                UnitOfWork.Commit();
                obj.Data = saved.ID;

                // obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.InnerException.Message;
                return BadRequest();
            }
        }

        [Route("api/EntryStructure/EditEntriesStructure")]
        [HttpPost]
        public IActionResult EditEntriesStructure(EntriesStructuringVM Model)
        {
            try
            {
                var obj = new ResponseVM();

                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                EntriesStructuring saved = UnitOfWork.EntriesStructureService.Edit(Model);
                UnitOfWork.EntriesStructure_DetailsService.Delete(Model.ID);
                UnitOfWork.Commit();
                foreach (var entry in Model.EntriesStructuringList)
                {
                    if (entry != null)
                    {
                        entry.EntriesStructuringId = saved.ID;
                        entry.DRCR_Difference = entry.Credit - entry.Debit;
                        UnitOfWork.EntriesStructure_DetailsService.Add(entry);
                        //entry.TotalCR = 0;
                        //entry.TotalDR = 0;
                       UnitOfWork.EntriesStructure_DetailsService.Edit(entry);
                    }
                }
                UnitOfWork.Commit();
                obj.Data = saved.ID;

                // obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.InnerException.Message;
                return BadRequest();
            }
        }

        [Route("api/EntryStructure/DeleteEntriesStructure")]
        [HttpGet]
        public IActionResult DeleteEntriesStructure(int ID)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.EntriesStructureService.Delete(ID);
                UnitOfWork.EntriesStructure_DetailsService.Delete(ID);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }

        [Route("api/EntryStructure/GenerateEntryStructureNo")]
        [HttpGet]
        public ActionResult GenerateEntryNo()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.EntriesStructureService.GenerateEntryNo();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

    }
}
