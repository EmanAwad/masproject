﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using MasProject.Data.DataAccess;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel.TransVM;
using Microsoft.AspNetCore.Mvc;
using static MasProject.Domain.Enums.Enums;

namespace MasProject.Server.Controllers.API.Persons
{
    public class ClientOpeningBalanceController : BaseController
    {
        public ClientOpeningBalanceController(DBContext context) : base(context)
        {
        }

        [Route("api/ClientOpeningBalance/GetClientOpeningBalance")]
        [HttpGet]
        public ActionResult GetClientOpeningBalance()
        {
            var obj = new ResponseVM();
            List<ClientOpeningBalanceVM> PersonBalanceList = UnitOfWork.ClientOpeningBalanceService.GetAll().ToList();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = PersonBalanceList;
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/ClientOpeningBalance/GetListsOfDDl")]
        [HttpGet]
        public ActionResult GetListsOfDDl()
        {
            try
            {
                var obj = new ResponseVM();
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                var EmployeeList = UnitOfWork.UserService.GetAllUsers().Select(s => new LookupKeyValueVM
                {
                    ID = s.ID,
                    Name = s.UserName,
                }).ToList();
                var BranchList = UnitOfWork.BranchServices.GetAll().Select(s => new LookupKeyValueVM
                {
                    ID = s.ID,
                    Name = s.Name,
                }).ToList();
                var ClientList = UnitOfWork.ClientsService.GetAll().Select(s => new LookupKeyValueVM
                {
                    ID = s.ID,
                    Name = s.Name,
                }).ToList();
                ClientOpeningBalanceVM balance = new ClientOpeningBalanceVM
                {

                    EmployeeList = EmployeeList,
                    BranchList= BranchList,
                    ClientList = ClientList,
                    DocumentNumber= UnitOfWork.ClientOpeningBalanceService.GenerateSerial(),
                };
                obj.Data = balance;
                return Ok(obj);
            }
            catch (Exception)
            {
                return null;
            }
        }
        [Route("api/ClientOpeningBalance/GetSpecificClientOpeningBalance")]
        [HttpGet]
        public ActionResult GetSpecificClientOpeningBalance(int ID)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                ClientOpeningBalanceVM balance = UnitOfWork.ClientOpeningBalanceService.Get(ID);
                balance.ClientDetails = UnitOfWork.ClientOpeningBalanceDetailsService.GetByDocumentNumber(balance.DocumentNumber);
                var EmployeeList = UnitOfWork.UserService.GetAllUsers().Select(s => new LookupKeyValueVM
                {
                    ID = s.ID,
                    Name = s.UserName,
                }).ToList();
                var BranchList = UnitOfWork.BranchServices.GetAll().Select(s => new LookupKeyValueVM
                {
                    ID = s.ID,
                    Name = s.Name,
                }).ToList();
                var ClientList = UnitOfWork.ClientsService.GetAll().Select(s => new LookupKeyValueVM
                {
                    ID = s.ID,
                    Name = s.Name,
                }).ToList();
                balance.BranchList = BranchList;
                balance.ClientList = ClientList;
                balance.EmployeeList = EmployeeList;
                obj.Data = balance;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/ClientOpeningBalance/AddClientOpeningBalance")]
        [HttpPost]
        public IActionResult AddClientOpeningBalance(ClientOpeningBalanceVM BalanceModel)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                var balance=UnitOfWork.ClientOpeningBalanceService.Add(BalanceModel);
                UnitOfWork.Commit();
                obj.Data = balance.ID;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/ClientOpeningBalance/EditClientOpeningBalance")]
        [HttpPost]
        public IActionResult EditClientOpeningBalance(ClientOpeningBalanceVM BalanceModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.ClientOpeningBalanceService.Edit(BalanceModel);
                UnitOfWork.Commit();
                UnitOfWork.ClientOpeningBalanceDetailsService.DeletePhysical(BalanceModel.DocumentNumber);
                UnitOfWork.Commit();
                foreach (var detail in BalanceModel.ClientDetails)
                {
                    if (detail != null)
                    {
                        detail.ClientDocumentNumber = BalanceModel.DocumentNumber;
                        UnitOfWork.ClientOpeningBalanceDetailsService.Add(detail);
                        ///////////////////////////////////////////////////trans///////////////////////////////
                        if (detail.ClientID != null && detail.ClientID != 0)
                        {
                            TransClientVM TransClient = new TransClientVM
                            {
                                ClientId = (int)detail.ClientID,
                                ClientType = TransClientTypeEnum.رصيد_اول_المدة,
                                Credit = 0,
                                CreateDate = DateTime.Now,
                                OpenBalFlag = true,
                                Debit = 0,
                                Date = BalanceModel.EntryDate,
                                SerialNumber = BalanceModel.ID,
                                IsDeleted = false,
                                Balance = detail.Amount == null ? 0 : (decimal)detail.Amount,
                            };
                            bool checkreturn = UnitOfWork.TransClientService.Add(TransClient, detail.Amount == null ? 0 : (decimal)detail.Amount);
                            UnitOfWork.Commit();
                            //recacaluate
                            if (!checkreturn)
                            {
                                UnitOfWork.TransClientService.RecalculateClientBalance((int)TransClient.ClientId, BalanceModel.ID);

                            }
                        }
                        ///////////////////////////////////////////////////////////////////////////
                    }
                }
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }
        [Route("api/ClientOpeningBalance/DeleteClientOpeningBalance")]
        [HttpGet]
        public IActionResult DeleteClientOpeningBalance(int DocumentNumber,int ID)
        {

            var obj = new ResponseVM();
            try
            {
                //int currobj;
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.ClientOpeningBalanceService.Delete(DocumentNumber);
                UnitOfWork.ClientOpeningBalanceDetailsService.Delete(DocumentNumber,ID);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        //[Route("api/ClientOpeningBalance/GenerateSerial")]
        //[HttpGet]
        //public ActionResult GenerateSerial()
        //{
        //    var obj = new ResponseVM();
        //    try
        //    {
        //        obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
        //        obj.Message = ApiStatus.Success;
        //        obj.Data = UnitOfWork.ClientOpeningBalanceService.GenerateSerial();
        //        return Ok(obj);
        //    }
        //    catch (Exception)
        //    {
        //        return BadRequest();
        //    }
        //}
    }
}
