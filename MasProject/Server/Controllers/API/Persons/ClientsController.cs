﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasProject.Data.DataAccess;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Persons;
using Microsoft.AspNetCore.Mvc;

namespace MasProject.Server.Controllers.API.Persons
{

    [ApiController]
    public class ClientsController : BaseController
    {
        public ClientsController(DBContext context) : base(context)
        {
        }
        [Route("api/Client/GetClients")]
        [HttpGet]
        public ActionResult GetClients()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ClientsService.GetClients().ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Client/GetClient")]
        [HttpGet]
        public ActionResult GetClient()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ClientsService.GetAll().ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Client/GetSpecificClient")]
        [HttpGet]
        public ActionResult GetSpecificClient(int ID)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                ClientsVM client = UnitOfWork.ClientsService.Get(ID);
                client.SocialMediaList = UnitOfWork.SocialClientService.GetByClientID(ID);
                client.SalesList = UnitOfWork.SalesArchivesService.GetByClientID(ID);
                List<DateTime?> DateRange = new List<DateTime?> { };
                DateRange.Add(client.LastDealingDate);
                DateTime? LastDateTransaction = UnitOfWork.SalesBillsService.GetLastDateByClient(ID);
                if (LastDateTransaction != null)
                {
                    DateRange.Add(LastDateTransaction);
                }
                LastDateTransaction = UnitOfWork.ExecutiveBillsService.GetLastDateByClient(ID);
                if (LastDateTransaction != null)
                {
                    DateRange.Add(LastDateTransaction);
                }
                LastDateTransaction = UnitOfWork.MantienceBillsService.GetLastDateByClient(ID);
                if (LastDateTransaction != null)
                {
                    DateRange.Add(LastDateTransaction);
                }
                LastDateTransaction = UnitOfWork.SalesReturnsService.GetLastDateByClient(ID);
                if (LastDateTransaction != null)
                {
                    DateRange.Add(LastDateTransaction);
                }              
                client.LastDealingDate = (DateTime)DateRange.Max();
                client.LastDealingDate = client.LastDealingDate.ToLocalTime();
                obj.Data = client;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/Client/GetDealType")]
        [HttpGet]
        public ActionResult GetDealType(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ClientsService.GetDealType(Id);
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/Client/GetTaxFile")]
        [HttpGet]
        public ActionResult GetTaxFile(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ClientsService.Get(Id);
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/Client/GetListsOfDDl")]
        [HttpGet]
        public ActionResult GetListsOfDDl()
        {
            try
            {
                var obj = new ResponseVM();
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                ClientsVM client = new ClientsVM
                {
                    SocialMediaList = UnitOfWork.SocialMediaService.GetAll().ToList(),
                    CityList = UnitOfWork.CityService.GetAll().ToList(),
                    BranchList = UnitOfWork.BranchServices.GetAll().ToList(),
                    DealTypeList=UnitOfWork.DealTypeService.GetAll().ToList()
                };
                obj.Data = client;
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        [Route("api/Client/CheckArrangment")]
        [HttpGet]
        public ActionResult CheckClientCode(int? arrangment, int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ClientsService.IsExistArrangment(arrangment, Id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Client/CheckClientCode")]
        [HttpGet]
        public ActionResult CheckClientCode(string Code, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ClientsService.IsExistCode(Code, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Client/CheckClientName")]
        [HttpGet]
        public ActionResult CheckClientName(string Name, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ClientsService.IsExistName(Name, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Client/CheckClientMail")]
        [HttpGet]
        public ActionResult CheckClientMail(string Mail, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ClientsService.IsExistMail(Mail, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Client/CheckClientPhone")]
        [HttpGet]
        public ActionResult CheckClientPhone(string Phone, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ClientsService.IsExistPhone(Phone, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Client/CheckClientMobile")]
        [HttpGet]
        public ActionResult CheckClientMobile(string Mobile, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ClientsService.IsExistMobile(Mobile, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Client/CheckClientMobile2")]
        [HttpGet]
        public ActionResult CheckClientMobile2(string Mobile, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ClientsService.IsExistMobile2(Mobile, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Client/AddClient")]
        [HttpPost]
        public IActionResult AddClient(ClientsVM ClientModel)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                var ClientSaved = UnitOfWork.ClientsService.Add(ClientModel);
                UnitOfWork.Commit();
                //  int ClientID = UnitOfWork.ClientsService.GetAll().OrderByDescending(x => x.ID).FirstOrDefault().ID;
                foreach (var social in ClientModel.SocialMediaList)
                {
                    if (social.Value != "")
                    {
                        SocialClientVM socialClient = new SocialClientVM
                        {
                            ClientID = ClientSaved.ID,
                            SocialID = social.ID,
                            Value = social.Value,
                            Name = social.Name
                        };
                        UnitOfWork.SocialClientService.Add(socialClient);
                    }

                }
                //foreach (var Sale in ClientModel.SalesList)
                //{
                //    if (Sale != null)
                //    {
                //        Sale.ClientID = ClientSaved.ID;
                //        UnitOfWork.SalesArchivesService.Add(Sale);
                //    }
                //}
                UnitOfWork.Commit();
                obj.Data = ClientSaved.ID;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/Client/EditClient")]
        [HttpPost]
        public IActionResult EditClient(ClientsVM ClientModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.ClientsService.Edit(ClientModel);
                 UnitOfWork.Commit();
                foreach (var social in ClientModel.SocialMediaList)
                {
                    SocialClientVM SocialClientbj = UnitOfWork.SocialClientService.GetByClientAndSocialID(ClientModel.ID, social.ID);
                    if (SocialClientbj != null)
                    {
                        SocialClientbj.Value = social.Value;
                        UnitOfWork.SocialClientService.Edit(SocialClientbj);
                    }
                    // UnitOfWork.Commit();
                }
                UnitOfWork.SalesArchivesService.DeletePhysical(ClientModel.ID);
                UnitOfWork.Commit();
                foreach (var Sale in ClientModel.SalesList)
                {
                    if (Sale != null )
                    {
                        Sale.ClientID = ClientModel.ID;
                        UnitOfWork.SalesArchivesService.Add(Sale);
                    }
                }
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/Client/DeleteClient")]
        [HttpGet]
        public IActionResult DeleteClient(int ID)
        {

            var obj = new ResponseVM();
            try
            {
                //int currobj;
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.ClientsService.Delete(ID);
                UnitOfWork.SalesArchivesService.Delete(ID);
                UnitOfWork.SocialClientService.Delete(ID);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}
