﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasProject.Data.DataAccess;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Persons;
using Microsoft.AspNetCore.Mvc;

namespace MasProject.Server.Controllers.API.Persons
{
    [ApiController]
        public class SupplierController : BaseController
        {
        public SupplierController(DBContext context) : base(context)
        {
            }
            [Route("api/Supplier/GetSuppliers")]
            [HttpGet]
            public ActionResult GetSuppliers()
            {
                var obj = new ResponseVM();
                try
                {
                    obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                    obj.Message = ApiStatus.Success;
                    obj.Data = UnitOfWork.SupplierService.GetAll().ToList();
                    return Ok(obj);
                }
                catch (Exception)
                {
                    // return null;
                    return BadRequest();
                }
            }
            [Route("api/Supplier/GetSpecificSupplier")]
            [HttpGet]
            public ActionResult GetSpecificSupplier(int ID)
            {
                var obj = new ResponseVM();
                try
                {
                    obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                    obj.Message = ApiStatus.Success;
                    SupplierVM sup = UnitOfWork.SupplierService.Get(ID);
                    sup.PurchaseList = UnitOfWork.PurchasesArchivesService.GetBySupplierID(ID);
                    obj.Data = sup;
                    return Ok(obj);
                }
                catch (Exception)
                {
                    return BadRequest();
                }
            }
         [Route("api/Supplier/GetSupplierCurrency")]
            [HttpGet]
            public ActionResult GetSupplierCurrency(int Id)
            {
                var obj = new ResponseVM();
                try
                {
                    obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                    obj.Message = ApiStatus.Success;
                    int Currency= UnitOfWork.SupplierService.GetCurrencyBySupplierID(Id);
                    obj.Data = Currency;
                    return Ok(obj);
                }
                catch (Exception)
                {
                    return BadRequest();
                }
            }
        [Route("api/Supplier/CheckSupplierCode")]
        [HttpGet]
        public ActionResult CheckSupplierCode(string Code, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.SupplierService.IsExistCode(Code, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Supplier/GetTaxFile")]
        [HttpGet]
        public ActionResult GetTaxFile(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.SupplierService.Get(Id);
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/Supplier/CheckSupplierName")]
        [HttpGet]
        public ActionResult CheckSupplierName(string Name, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.SupplierService.IsExistName(Name, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Supplier/CheckSupplierMail")]
        [HttpGet]
        public ActionResult CheckSupplierMail(string Mail, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.SupplierService.IsExistMail(Mail, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Supplier/CheckSupplierPhone")]
        [HttpGet]
        public ActionResult CheckSupplierPhone(string Phone, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.SupplierService.IsExistPhone(Phone, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Supplier/CheckSupplierMobile")]
        [HttpGet]
        public ActionResult CheckSupplierMobile(string Mobile, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.SupplierService.IsExistMobile(Mobile, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Supplier/AddSupplier")]
            [HttpPost]
            public IActionResult AddSupplier(SupplierVM SupplierModel)
            {
                var obj = new ResponseVM();
                try
                {
                    obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                    obj.Message = ApiStatus.Success;
                    var SupplierSaved=UnitOfWork.SupplierService.Add(SupplierModel);
                    UnitOfWork.Commit();
                //foreach(var Purchase in SupplierModel.PurchaseList)
                //{
                //    if(Purchase != null)
                //    {
                //        Purchase.SupplierID = SupplierSaved.ID;
                //        UnitOfWork.PurchasesArchivesService.Add(Purchase);
                //    }
                //}
               // UnitOfWork.Commit();
                    obj.Data = SupplierSaved.ID;
                    return Ok(obj);
                }
                catch (Exception)
                {
                    return BadRequest();
                }
            }

            [Route("api/Supplier/EditSupplier")]
            [HttpPost]
            public IActionResult EditSupplier(SupplierVM SupplierModel)
            {

                var obj = new ResponseVM();
                try
                {
                    obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                    obj.Message = ApiStatus.Success;
                    UnitOfWork.SupplierService.Edit(SupplierModel);
                    UnitOfWork.Commit();
                //delete old items
                UnitOfWork.PurchasesArchivesService.DeletePhysical(SupplierModel.ID);
                UnitOfWork.Commit();
                foreach (var purchase in SupplierModel.PurchaseList)
                {
                    if(purchase != null )
                    {
                        purchase.SupplierID = SupplierModel.ID;
                        UnitOfWork.PurchasesArchivesService.Add(purchase);
                        UnitOfWork.Commit();
                    }
                }
                
                obj.Data = "Done";
                    return Ok(obj);
                }
                catch (Exception ex)
                {
                var exh = ex.Message;
                    return BadRequest();
                }
            }
            [Route("api/Supplier/DeleteSupplier")]
            [HttpGet]
            public IActionResult DeleteSupplier(int ID)
            {

                var obj = new ResponseVM();
                try
                {
                    //int currobj;
                    obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                    obj.Message = ApiStatus.Success;
                    UnitOfWork.SupplierService.Delete(ID);
                    UnitOfWork.PurchasesArchivesService.Delete(ID);
                    UnitOfWork.Commit();
                    obj.Data = "Done";
                    return Ok(obj);
                }
                catch (Exception)
                {
                    return BadRequest();
                }
            }
        [Route("api/Supplier/CheckArrangement")]
        [HttpGet]
        public ActionResult CheckArrangement(int arrangement, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.SupplierService.IsExistArrangement(arrangement, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }

    }
}
