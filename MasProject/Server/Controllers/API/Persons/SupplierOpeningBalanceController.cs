﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using MasProject.Data.DataAccess;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel.TransVM;
using Microsoft.AspNetCore.Mvc;
using static MasProject.Domain.Enums.Enums;

namespace MasProject.Server.Controllers.API.Persons
{
    public class SupplierOpeningBalanceController : BaseController
    {
        public SupplierOpeningBalanceController(DBContext context ) : base(context)
        {
        }

        [Route("api/SupplierOpeningBalance/GetSupplierOpeningBalance")]
        [HttpGet]
        public ActionResult GetSupplierOpeningBalance()
        {
            var obj = new ResponseVM();
            List<SupplierOpeningBalanceVM> PersonBalanceList = UnitOfWork.SupplierOpeningBalanceService.GetAll().ToList();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = PersonBalanceList;
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/SupplierOpeningBalance/GetListsOfDDl")]
        [HttpGet]
        public ActionResult GetListsOfDDl()
        {
            try
            {
                var obj = new ResponseVM();
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                var CurrencyList = UnitOfWork.CurrencyServices.GetAll().Select(s => new LookupKeyValueVM
                {
                    ID = s.ID,
                    Name = s.Name,
                }).ToList();
                var SupplierList = UnitOfWork.SupplierService.GetAll().Select(s => new LookupKeyValueVM
                {
                    ID = s.ID,
                    Name = s.Name,
                }).ToList();
                SupplierOpeningBalanceVM balance = new SupplierOpeningBalanceVM
                {
                    CurrencyList = CurrencyList,
                    SupplierList = SupplierList,
                    DocumentNumber = UnitOfWork.SupplierOpeningBalanceService.GenerateSerial(),
                };
                obj.Data = balance;
                return Ok(obj);
            }
            catch (Exception)
            {
                return null;
            }
        }
        [Route("api/SupplierOpeningBalance/GetSpecificSupplierOpeningBalance")]
        [HttpGet]
        public ActionResult GetSpecificSupplierOpeningBalance(int ID)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                SupplierOpeningBalanceVM balance = UnitOfWork.SupplierOpeningBalanceService.Get(ID);
                balance.SupplierDetails = UnitOfWork.SupplierOpeningBalanceDetailsService.GetByDocumentNumber(balance.DocumentNumber);
                var SupplierList = UnitOfWork.SupplierService.GetAll().Select(s => new LookupKeyValueVM
                {
                    ID = s.ID,
                    Name = s.Name,
                }).ToList();
                var CurrencyList = UnitOfWork.CurrencyServices.GetAll().Select(s => new LookupKeyValueVM
                {
                    ID = s.ID,
                    Name = s.Name,
                }).ToList();
                balance.SupplierList = SupplierList;
                balance.CurrencyList = CurrencyList;
                obj.Data = balance;
                return Ok(obj);
              
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/SupplierOpeningBalance/AddSupplierOpeningBalance")]
        [HttpPost]
        public IActionResult AddSupplierOpeningBalance(SupplierOpeningBalanceVM BalanceModel)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                var balance=UnitOfWork.SupplierOpeningBalanceService.Add(BalanceModel);
                UnitOfWork.Commit();
                ///////////////////////////////////////////////////trans///////////////////////////////
                //TransSupplierVM TransSupplier = new TransSupplierVM
                //{
                //    SupplierId = (int)BalanceModel.SupplierID,
                //    SupplierType = TransSupplierTypeEnum.رصيد_اول_المدة,
                //    Credit = 0,
                //    CreateDate = DateTime.Now,
                //    OpenBalFlag = true,
                //    Debit = 0,
                //    Date = BalanceModel.EntryDate,
                //    SerialNumber = balance.ID,
                //    IsDeleted = false,
                //    Balance = BalanceModel.Amount == null ? 0 : (decimal)BalanceModel.Amount,
                //};
                //bool checkreturn = UnitOfWork.TransSupplierService.Add(TransSupplier, BalanceModel.Amount == null ? 0 : (decimal)BalanceModel.Amount);
                //UnitOfWork.Commit();
                ////recacaluate
                //if (!checkreturn)
                //{
                //    UnitOfWork.TransSupplierService.RecalculateSupplierBalance((int)TransSupplier.SupplierId, balance.ID);

                //}
                ///////////////////////////////////////////////////////////////////////////
              //  UnitOfWork.Commit();
                obj.Data = balance.ID;
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("api/SupplierOpeningBalance/EditSupplierOpeningBalance")]
        [HttpPost]
        public IActionResult EditSupplierOpeningBalance(SupplierOpeningBalanceVM BalanceModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.SupplierOpeningBalanceService.Edit(BalanceModel);
                UnitOfWork.Commit();
                UnitOfWork.SupplierOpeningBalanceDetailsService.DeletePhysical(BalanceModel.DocumentNumber);
                UnitOfWork.Commit();
                foreach (var detail in BalanceModel.SupplierDetails)
                {
                    if (detail != null)
                    {
                        detail.SupplierDocumentNumber = BalanceModel.DocumentNumber;
                        UnitOfWork.SupplierOpeningBalanceDetailsService.Add(detail);

                        ///////////////////////////////////////////////////trans///////////////////////////////
                        if (detail.SupplierID != null && detail.SupplierID != 0)
                        {
                            TransSupplierVM TransSupplier = new TransSupplierVM
                            {
                                SupplierId = (int)detail.SupplierID,
                                SupplierType = TransSupplierTypeEnum.رصيد_اول_المدة,
                                Credit = 0,
                                CreateDate = DateTime.Now,
                                OpenBalFlag = true,
                                Debit = 0,
                                Date = BalanceModel.EntryDate,
                                SerialNumber = BalanceModel.ID,
                                IsDeleted = false,
                                Balance = detail.Amount == null ? 0 : (decimal)detail.Amount,
                            };
                            bool checkreturn = UnitOfWork.TransSupplierService.Add(TransSupplier, detail.Amount == null ? 0 : (decimal)detail.Amount);
                            UnitOfWork.Commit();
                            //recacaluate
                            if (!checkreturn)
                            {
                                UnitOfWork.TransSupplierService.RecalculateSupplierBalance((int)TransSupplier.SupplierId, BalanceModel.ID);

                            }
                        }
                    }

                }
            
               
                ///////////////////////////////////////////////////trans///////////////////////////////

                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }
        [Route("api/SupplierOpeningBalance/DeleteSupplierOpeningBalance")]
        [HttpGet]
        public IActionResult DeleteSupplierOpeningBalance(int DocumentNumber,int ID)
        {

            var obj = new ResponseVM();
            try
            {
                //int currobj;
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                //old
                //  UnitOfWork.SupplierOpeningBalanceService.Delete(ID);
                //new 
                UnitOfWork.SupplierOpeningBalanceService.Delete(DocumentNumber);
                UnitOfWork.SupplierOpeningBalanceDetailsService.Delete(DocumentNumber, ID);

                int Id = UnitOfWork.TransSupplierService.RecalculateSupplierBalance(DocumentNumber, ID);
                UnitOfWork.TransSupplierService.Delete(Id);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }


    }
}
