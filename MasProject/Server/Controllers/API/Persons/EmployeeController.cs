﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Login;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Persons;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MasProject.Server.Controllers.API.Persons
{
    [ApiController]
    public class EmployeeController : BaseController
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private IConfiguration _config;
        public EmployeeController(DBContext context, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IConfiguration config) : base(context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _config = config;
        }
        [Route("api/Employee/GetEmployees")]
        [HttpGet]
        public ActionResult GetEmployees()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.EmployeeService.GetAll().ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }

        [Route("api/Employee/GetAllEmployee")]
        [HttpGet]
        public ActionResult GetAllEmployee()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.EmployeeService.GetAllWithHidden().ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }

        [Route("api/Employee/GetUsers")]
        [HttpGet]
        public ActionResult GetUsers()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.UserService.GetAllUsers().ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Employee/GetSpecificEmployee")]
        [HttpGet]
        public ActionResult GetSpecificEmployee(int ID)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                EmployeeVM emp = UnitOfWork.EmployeeService.Get(ID);
                obj.Data = emp;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        
        [Route("api/Employee/CheckEmployeeCode")]
        [HttpGet]
        public ActionResult CheckEmployeeCode(string Code, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.EmployeeService.IsExistCode(Code, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Employee/CheckEmployeeName")]
        [HttpGet]
        public ActionResult CheckEmployeeName(string Name, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.EmployeeService.IsExistName(Name, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Employee/CheckEmployeeMail")]
        [HttpGet]
        public ActionResult CheckEmployeeMail(string Mail, int id)
        {
           var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.EmployeeService.IsExistMail(Mail, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Employee/CheckEmployeePhone")]
        [HttpGet]
        public ActionResult CheckEmployeePhone(string Phone, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.EmployeeService.IsExistPhone(Phone, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Employee/CheckEmployeeMobile")]
        [HttpGet]
        public ActionResult CheckEmployeeMobile(string Mobile, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.EmployeeService.IsExistMobile(Mobile, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }

        [Route("api/Employee/AddEmployee")]
        [HttpPost]
        public async Task<IActionResult> AddEmployee(EmployeeVM EmployeeModel)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.EmployeeService.Add(EmployeeModel);           
                
                ApplicationUser User = new ApplicationUser()
                {
                    UserName = EmployeeModel.UserName,
                    PasswordHash = EmployeeModel.Passwordouble,
                    Email=EmployeeModel.Mail,
                };
                try
                {
                    var user = await _userManager.FindByNameAsync(EmployeeModel.UserName);
                    if (user != null)
                    {
                        return BadRequest(new { message = "This User Already Exist!" });
                    }
                    else
                    {
                        var result = await _userManager.CreateAsync(User, EmployeeModel.Passwordouble);
                        if (!result.Succeeded)
                        {
                            return BadRequest();
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }            
        }

        [Route("api/Employee/EditEmployee")]
        [HttpPost]
        public IActionResult EditEmployee(EmployeeVM EmployeeModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.EmployeeService.Edit(EmployeeModel);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/Employee/DeleteEmployee")]
        [HttpGet]
        public IActionResult DeleteEmployee(int ID)
        {

            var obj = new ResponseVM();
            try
            {
                //int currobj;
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.EmployeeService.Delete(ID);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/Employee/GetListsOfDDl")]
        [HttpGet]
        public IActionResult GetListsOfDDl()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                EmployeeVM emp = new EmployeeVM { };
                emp.DepartmentList = UnitOfWork.DepartmentServices.GetAll().ToList();
                emp.BranchList = UnitOfWork.BranchServices.GetAll().ToList();
                obj.Data = emp;
                return Ok(obj);
            }
            catch (Exception ex )
            {
                return BadRequest();
            }
        }
        [Route("api/Employee/CheckArrangement")]
        [HttpGet]
        public ActionResult CheckArrangement(int arrangement, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.EmployeeService.IsExistArrangement(arrangement, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }

    }
}
