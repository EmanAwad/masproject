﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using MasProject.Data.DataAccess;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel.TransVM;
using Microsoft.AspNetCore.Mvc;
using static MasProject.Domain.Enums.Enums;

namespace MasProject.Server.Controllers.API.Persons
{
    public class EmployeeOpeningBalanceController : BaseController
    {
        public EmployeeOpeningBalanceController(DBContext context) : base(context)
        {
        }

        [Route("api/EmployeeOpeningBalance/GetEmployeeOpeningBalance")]
        [HttpGet]
        public ActionResult GetEmployeeOpeningBalance()
        {
            var obj = new ResponseVM();
            List<EmployeeOpeningBalanceVM> PersonBalanceList = UnitOfWork.EmployeeOpeningBalanceService.GetAll().ToList();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = PersonBalanceList;
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/EmployeeOpeningBalance/GetListsOfDDl")]
        [HttpGet]
        public ActionResult GetListsOfDDl()
        {
            try
            {
                var obj = new ResponseVM();
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                var CurrencyList = UnitOfWork.CurrencyServices.GetAll().Select(s => new LookupKeyValueVM
                {
                    ID = s.ID,
                    Name = s.Name,
                }).ToList();
                var EmployeeList = UnitOfWork.EmployeeService.GetAll().Select(s => new LookupKeyValueVM
                {
                    ID = s.ID,
                    Name = s.Name,
                }).ToList();
                EmployeeOpeningBalanceVM balance = new EmployeeOpeningBalanceVM
                {
                    
                    EmployeeList = EmployeeList
            };
                obj.Data = balance;
                return Ok(obj);
            }
            catch(Exception)
            {
                return null;
            }
            }
        [Route("api/EmployeeOpeningBalance/GetSpecificEmployeeOpeningBalance")]
        [HttpGet]
        public ActionResult GetSpecificEmployeeOpeningBalance(int ID)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                EmployeeOpeningBalanceVM balance = UnitOfWork.EmployeeOpeningBalanceService.Get(ID);
                obj.Data = balance;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/EmployeeOpeningBalance/AddEmployeeOpeningBalance")]
        [HttpPost]
        public IActionResult AddEmployeeOpeningBalance(EmployeeOpeningBalanceVM BalanceModel)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                var balance= UnitOfWork.EmployeeOpeningBalanceService.Add(BalanceModel);
                UnitOfWork.Commit();
                ///////////////////////////////////////////////////trans///////////////////////////////
                TransEmployeeVM TransClient = new TransEmployeeVM
                {
                    EmployeeId = (int)BalanceModel.EmployeeID,
                    EmployeeType = TransEmployeeTypeEnum.رصيد_اول_المدة,
                    Credit = 0,
                    CreateDate = DateTime.Now,
                    OpenBalFlag = true,
                    Debit = 0,
                    Date = BalanceModel.EntryDate,
                    SerialNumber = balance.ID,
                    IsDeleted = false,
                    Balance = BalanceModel.Amount==null?0:(decimal)BalanceModel.Amount,
                };
                bool checkreturn = UnitOfWork.TransEmployeeService.Add(TransClient, BalanceModel.Amount == null ? 0 : (decimal)BalanceModel.Amount);
                UnitOfWork.Commit();
                //recacaluate
                if (!checkreturn)
                {
                    UnitOfWork.TransEmployeeService.RecalculateEmployeeBalance((int)TransClient.EmployeeId, balance.ID);

                }
                ///////////////////////////////////////////////////////////////////////////
                UnitOfWork.Commit();
                obj.Data = balance.ID;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/EmployeeOpeningBalance/EditEmployeeOpeningBalance")]
        [HttpPost]
        public IActionResult EditEmployeeOpeningBalance(EmployeeOpeningBalanceVM BalanceModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.EmployeeOpeningBalanceService.Edit(BalanceModel);
                ///////////////////////////////////////////////////trans///////////////////////////////
                TransEmployeeVM TransClient = new TransEmployeeVM
                {
                    EmployeeId = (int)BalanceModel.EmployeeID,
                    EmployeeType = TransEmployeeTypeEnum.رصيد_اول_المدة,
                    Credit = 0,
                    CreateDate = DateTime.Now,
                    OpenBalFlag = true,
                    Debit = 0,
                    Date = BalanceModel.EntryDate,
                    SerialNumber = BalanceModel.ID,
                    IsDeleted = false,
                    Balance = BalanceModel.Amount == null ? 0 : (decimal)BalanceModel.Amount,
                };
                UnitOfWork.TransEmployeeService.Add(TransClient,(decimal)BalanceModel.Amount);
                ///////////////////////////////////////////////////////////////////////////
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/EmployeeOpeningBalance/DeleteEmployeeOpeningBalance")]
        [HttpGet]
        public IActionResult DeleteEmployeeOpeningBalance(int ID, int EmployeeId)
        {

            var obj = new ResponseVM();
            try
            {
                //int currobj;
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.EmployeeOpeningBalanceService.Delete(ID);
                int Id = UnitOfWork.TransEmployeeService.RecalculateEmployeeBalance(EmployeeId, ID);
                UnitOfWork.TransEmployeeService.Delete(Id);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        
    }
}
