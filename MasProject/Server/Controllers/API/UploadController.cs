﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace MasProject.Server.Controllers.API
{
    [Route("[controller]")]
    [ApiController]
    public class UploadController : ControllerBase
    {
        private readonly IConfiguration _config;
        // private readonly IWebHostEnvironment environment;IWebHostEnvironment environment
        public UploadController(IConfiguration config)
        {
            _config = config ?? throw new System.ArgumentNullException(nameof(config));
            // this.environment = environment;
        }


        [HttpPost]
        public async Task Post(string FolderName)
        {
            if (HttpContext.Request.Form.Files.Any())
            {
                string RootPath = _config.GetValue<string>("RootPath");
                string pathlogo = $"" + RootPath + FolderName;
                if (!Directory.Exists(pathlogo))
                    Directory.CreateDirectory(pathlogo);
                foreach (var file in HttpContext.Request.Form.Files)
                {
                    var path = Path.Combine(pathlogo, file.FileName);
                    try
                    {
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            await file.CopyToAsync(stream);
                        }
                    }
                    catch (Exception ex)
                    {
                        string mess = ex.Message;
                    }
                }
            }
        }
    }
}
