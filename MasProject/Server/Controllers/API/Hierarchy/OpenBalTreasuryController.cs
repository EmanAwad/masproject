﻿using MasProject.Data.DataAccess;
using MasProject.Domain;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Hierarchy;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace MasProject.Server.Controllers.API.Hierarchy
{
    public class TreasuryOpenBalanceController : BaseController
    {
        public TreasuryOpenBalanceController(DBContext context) : base(context)
        {
        }
        [Route("api/TreasuryOpenBal/GetTreasuryOpenBal")]
        [HttpGet]
        public ActionResult GetTreasuryOpenBalances()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.TreasuryOpenBalanceOpenBalanceServices.GetAll().ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/TreasuryOpenBal/GetOneTreasuryOpenBal")]
        [HttpGet]
        public ActionResult GetOneTreasuryOpenBal(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.TreasuryOpenBalanceOpenBalanceServices.Get(Id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/TreasuryOpenBal/AddTreasuryOpenBal")]
        [HttpPost]

        public IActionResult AddTreasuryOpenBalance(TreasuryOpenBalanceVM TreasuryOpenBalanceModel)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.TreasuryOpenBalanceOpenBalanceServices.Add(TreasuryOpenBalanceModel);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/TreasuryOpenBal/EditTreasuryOpenBal")]
        [HttpPost]
        public IActionResult EditTreasuryOpenBalance(TreasuryOpenBalanceVM TreasuryOpenBalanceModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.TreasuryOpenBalanceOpenBalanceServices.Edit(TreasuryOpenBalanceModel);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/TreasuryOpenBal/DeleteTreasuryOpenBal")]
        [HttpGet]
        public IActionResult DeleteTreasuryOpenBalance(int ID)
        {

            var obj = new ResponseVM();
            try
            {
                //int currobj;
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.TreasuryOpenBalanceOpenBalanceServices.Delete(ID);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

    }
}
