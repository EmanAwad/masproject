﻿using MasProject.Data.DataAccess;
using MasProject.Domain;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Hierarchy;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Linq;

namespace MasProject.Server.Controllers.API.Hierarchy
{
    public class CompanyController : BaseController
    {
        public CompanyController(DBContext context) : base(context)
        {
        }
        [Route("api/Company/GetCompanys")]
        [HttpGet]
        public ActionResult GetCompanys()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                //edit 20-09-2020
                obj.Data = UnitOfWork.CompanyServices.GetAll().FirstOrDefault();
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Company/GetSpecificCompany")]
        [HttpGet]
        public ActionResult GetSpecificCompany(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.CompanyServices.Get(Id);
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/Company/CheckNameCompany")]
        [HttpGet]
        public ActionResult CheckNameCompany(string name, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.CompanyServices.IsExist(name, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/Company/CheckArrangement")]
        [HttpGet]
        public ActionResult CheckArrangement(int arrangement, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.CompanyServices.IsExistArrangement(arrangement, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Company/AddCompany")]
        [HttpPost]
        public IActionResult AddCompany(CompanyVM CompanyModel)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.CompanyServices.Add(CompanyModel);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/Company/EditCompany")]
        [HttpPost]
        public IActionResult EditCompany(CompanyVM CompanyModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.CompanyServices.Edit(CompanyModel);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/Company/DeleteCompany")]
        [HttpGet]
        public IActionResult DeleteCompany(int ID)
        {

            var obj = new ResponseVM();
            try
            {
                //int currobj;
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.CompanyServices.Delete(ID);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

    }
}
