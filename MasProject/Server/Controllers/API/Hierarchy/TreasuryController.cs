﻿using MasProject.Data.DataAccess;
using MasProject.Domain;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Hierarchy;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace MasProject.Server.Controllers.API.Hierarchy
{
    public class TreasuryController : BaseController
    {
        public TreasuryController(DBContext context) : base(context)
        {
        }
        [Route("api/Treasury/GetTreasurys")]
        [HttpGet]
        public ActionResult GetTreasurys()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.TreasuryServices.GetAll().ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Treasury/GetSpecificTreasury")]
        [HttpGet]
        public ActionResult GetSpecificTreasury(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.TreasuryServices.Get(Id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Treasury/CheckNameTreasury")]
        [HttpGet]
        public ActionResult CheckNameTreasury(string name ,int id  )
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.TreasuryServices.IsExist(name,id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Treasury/AddTreasury")]
        [HttpPost]

        public IActionResult AddTreasury(TreasuryVM TreasuryModel)
        {
            //_context.Add(curr);
            //await _context.SaveChangesAsync();
            //return Ok(curr.ID);
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                //check forgien key
                if (TreasuryModel.UserId == 0)
                {
                    TreasuryModel.UserId = 1;
                }
                UnitOfWork.TreasuryServices.Add(TreasuryModel);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/Treasury/EditTreasury")]
        [HttpPost]
        public IActionResult EditTreasury(TreasuryVM TreasuryModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                //check forgien key
                if (TreasuryModel.UserId == 0)
                {
                    TreasuryModel.UserId = 1;
                }
                UnitOfWork.TreasuryServices.Edit(TreasuryModel);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/Treasury/DeleteTreasury")]
        [HttpGet]
        public IActionResult DeleteTreasury(int ID)
        {

            var obj = new ResponseVM();
            try
            {
                //int currobj;
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.TreasuryServices.Delete(ID);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/Treasury/GetTreasuryByBranchId")]
        [HttpGet]
        public ActionResult GetTreasuryByBranchId(int branchID)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.TreasuryServices.GetTreasuryByBranch(branchID).ID;
                return Ok(obj);
            }
            catch (Exception)
            {
               
                return BadRequest();
            }
        }

        [Route("api/Treasury/CheckArrangement")]
        [HttpGet]
        public ActionResult CheckArrangement(int arrangement, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.TreasuryServices.IsExistArrangement(arrangement, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }

        [Route("api/Treasury/CheckTreasuryCode")]
        [HttpGet]
        public ActionResult CheckTreasuryCode(string Code, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.TreasuryServices.IsExistTreasuryCode(Code, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
    }
}
