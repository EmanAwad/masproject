﻿using MasProject.Data.DataAccess;
using MasProject.Domain;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Hierarchy;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace MasProject.Server.Controllers.API.Hierarchy
{
    public class DepartmentController : BaseController
    {
        public DepartmentController(DBContext context) : base(context)
        {
        }
        [Route("api/Department/GetDepartments")]
        [HttpGet]
        public ActionResult GetDepartments()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.DepartmentServices.GetAll().ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Department/GetSpecificDepartment")]
        [HttpGet]
        public ActionResult GetSpecificDepartment(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.DepartmentServices.Get(Id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Department/CheckNameDepartment")]
        [HttpGet]
        public ActionResult CheckNameDepartment(string name, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.DepartmentServices.IsExist(name, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Department/CheckArrangement")]
        [HttpGet]
        public ActionResult CheckArrangement(int arrangement, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.DepartmentServices.IsExistArrangement(arrangement, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Department/AddDepartment")]
        [HttpPost]

        public IActionResult AddDepartment(DepartmentVM DepartmentModel)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.DepartmentServices.Add(DepartmentModel);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/Department/EditDepartment")]
        [HttpPost]
        public IActionResult EditDepartment(DepartmentVM DepartmentModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.DepartmentServices.Edit(DepartmentModel);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/Department/DeleteDepartment")]
        [HttpGet]
        public IActionResult DeleteDepartment(int ID)
        {

            var obj = new ResponseVM();
            try
            {
                //int currobj;
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.DepartmentServices.Delete(ID);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

    }
}
