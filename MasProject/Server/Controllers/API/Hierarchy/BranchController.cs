﻿using MasProject.Data.DataAccess;
using MasProject.Domain;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Hierarchy;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace MasProject.Server.Controllers.API.Hierarchy
{
    public class BranchController : BaseController
    {
        public BranchController(DBContext context) : base(context)
        {
        }
        [Route("api/Branch/GetBranchs")]
        [HttpGet]
        public ActionResult GetBranchs()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.BranchServices.GetAll().ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Branch/GetSpecificBranch")]
        [HttpGet]
        public ActionResult GetSpecificBranch(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.BranchServices.Get(Id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Branch/CheckNameBranch")]
        [HttpGet]
        public ActionResult CheckNameBranch(string name, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.BranchServices.IsExist(name, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Branch/CheckKeyBranch")]
        [HttpGet]
        public ActionResult CheckKeyBranch(string BranchKey, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.BranchServices.IsExistKey(BranchKey, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Branch/CheckArrangement")]
        [HttpGet]
        public ActionResult CheckArrangement(int arrangement, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.BranchServices.IsExistArrangement(arrangement, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }

        [Route("api/Branch/AddBranch")]
        [HttpPost]

        public IActionResult AddBranch(BranchVM BranchModel)
        {
            //_context.Add(curr);
            //await _context.SaveChangesAsync();
            //return Ok(curr.ID);
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.BranchServices.Add(BranchModel);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/Branch/EditBranch")]
        [HttpPost]
        public IActionResult EditBranch(BranchVM BranchModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.BranchServices.Edit(BranchModel);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/Branch/DeleteBranch")]
        [HttpGet]
        public IActionResult DeleteBranch(int ID)
        {

            var obj = new ResponseVM();
            try
            {
                if (!UnitOfWork.BranchServices.IsBranchUsed(ID))
                {
                    //int currobj;
                    obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                    obj.Message = ApiStatus.Success;
                    UnitOfWork.BranchServices.Delete(ID);
                    UnitOfWork.Commit();
                    obj.Data = "Done";
                }
                else
                {
                    return BadRequest();
                }
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

    }
}
