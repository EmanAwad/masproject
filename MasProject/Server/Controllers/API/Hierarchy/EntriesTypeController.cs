﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasProject.Data.DataAccess;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Hierarchy;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MasProject.Server.Controllers.API.Hierarchy
{
    public class EntriesTypeController : BaseController
    {
        public EntriesTypeController(DBContext context) : base(context)
        {
        }
        [Route("api/EntriesType/GetEntriesType")]
        [HttpGet]
        public ActionResult GetEntriesType()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.EntriesTypeService.GetAll().ToList();
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }
        
        [Route("api/EntriesType/CheckNameEntriesType")]
        [HttpGet]
        public ActionResult CheckNameDepartment(string name, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.EntriesTypeService.IsExist(name, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/EntriesType/GetSpecificEntriesType")]
        [HttpGet]
        public ActionResult GetSpecificEntriesType(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.EntriesTypeService.Get(Id);
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/EntriesType/AddEntriesType")]
        [HttpPost]

        public IActionResult AddEntriesType(EntriesTypeVM currModel)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.EntriesTypeService.Add(currModel);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }


        [Route("api/EntriesType/EditEntriesType")]
        [HttpPost]
        public IActionResult EditEntriesType(EntriesTypeVM currModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.EntriesTypeService.Edit(currModel);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/EntriesType/DeleteEntriesType")]
        [HttpGet]
        public IActionResult DeleteEntriesType(int ID)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.EntriesTypeService.Delete(ID);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/EntriesType/CheckName")]
        [HttpGet]
        public ActionResult CheckName(string name, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.EntriesTypeService.IsExist(name, id);
                return Ok(obj);
            }
            catch (Exception ex)
            {
              string message =  ex.Message;
                return BadRequest();
            }
        }

    }
}