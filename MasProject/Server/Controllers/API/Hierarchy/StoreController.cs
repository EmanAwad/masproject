﻿using MasProject.Data.DataAccess;
using MasProject.Domain;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Hierarchy;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace MasProject.Server.Controllers.API.Hierarchy
{
    public class StoreController : BaseController
    {
        public StoreController(DBContext context) : base(context)
        {
        }
        [Route("api/Store/GetStores")]
        [HttpGet]
        public ActionResult GetStores()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.StoreServices.GetAll().ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Store/GetSpecificStore")]
        [HttpGet]
        public ActionResult GetSpecificStore(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.StoreServices.Get(Id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Store/CheckNameStore")]
        [HttpGet]
        public ActionResult CheckNameStore(string name, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.StoreServices.IsExist(name, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Store/CheckArrangement")]
        [HttpGet]
        public ActionResult CheckArrangement(int arrangement, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.StoreServices.IsExistArrangement(arrangement, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/Store/AddStore")]
        [HttpPost]

        public IActionResult AddStore(StoreVM StoreModel)
        {
            //_context.Add(curr);
            //await _context.SaveChangesAsync();
            //return Ok(curr.ID);
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.StoreServices.Add(StoreModel);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/Store/EditStore")]
        [HttpPost]
        public IActionResult EditStore(StoreVM StoreModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.StoreServices.Edit(StoreModel);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/Store/DeleteStore")]
        [HttpGet]
        public IActionResult DeleteStore(int ID)
        {

            var obj = new ResponseVM();
            try
            {
                if (!UnitOfWork.StoreServices.IsStoreUsed(ID))
                {
                    //int currobj;
                    obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                    obj.Message = ApiStatus.Success;
                    UnitOfWork.StoreServices.Delete(ID);
                    UnitOfWork.Commit();
                    obj.Data = "Done";
                    
                }
                else
                {
                    return BadRequest();
                }
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

    }
}
