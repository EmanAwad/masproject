﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasProject.Data.DataAccess;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Hierarchy;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MasProject.Server.Controllers.API.Hierarchy
{
    public class OtherConstantController : BaseController
    {
        public OtherConstantController(DBContext context) : base(context)
        {
        }
        [Route("api/OtherConstant/GetOtherConstant")]
        [HttpGet]
        public ActionResult GetOtherConstant()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.OtherConstantService.GetAll().ToList();
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }
        [Route("api/OtherConstant/GetOtherConstantRatio")]
        [HttpGet]
        public ActionResult GetOtherConstantRatio(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.OtherConstantService.GetOtherConstantRatioByID(Id);
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/OtherConstant/CheckNameOtherConstant")]
        [HttpGet]
        public ActionResult CheckNameDepartment(string name, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.OtherConstantService.IsExist(name, id);
                return Ok(obj);
            }
            catch (Exception)
            {
                // return null;
                return BadRequest();
            }
        }
        [Route("api/OtherConstant/GetSpecificOtherConstant")]
        [HttpGet]
        public ActionResult GetSpecificOtherConstant(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.OtherConstantService.Get(Id);
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/OtherConstant/AddOtherConstant")]
        [HttpPost]

        public IActionResult AddOtherConstant(OtherConstantVM currModel)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.OtherConstantService.Add(currModel);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }


        [Route("api/OtherConstant/EditOtherConstant")]
        [HttpPost]
        public IActionResult EditOtherConstant(OtherConstantVM currModel)
        {

            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.OtherConstantService.Edit(currModel);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/OtherConstant/DeleteOtherConstant")]
        [HttpGet]
        public IActionResult DeleteOtherConstant(int ID)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.OtherConstantService.Delete(ID);
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/OtherConstant/CheckName")]
        [HttpGet]
        public ActionResult CheckName(string name, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.OtherConstantService.IsExist(name, id);
                return Ok(obj);
            }
            catch (Exception ex)
            {
              string message =  ex.Message;
                return BadRequest();
            }
        }

        [Route("api/OtherConstant/CheckArrangement")]
        [HttpGet]
        public ActionResult CheckArrangement(int arrangement, int id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.OtherConstantService.IsExistArrangement(arrangement, id);
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                // return null;
                return BadRequest();
            }
        }

    }
}