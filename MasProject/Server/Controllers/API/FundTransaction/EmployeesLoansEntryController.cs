﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasProject.Data.DataAccess;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.FundTransaction;
using MasProject.Domain.ViewModel.TransVM;
using Microsoft.AspNetCore.Mvc;
using static MasProject.Domain.Enums.Enums;

namespace MasProject.Server.Controllers.API.FundTransaction
{
    public class EmployeesLoansEntryController : BaseController
    {
        public EmployeesLoansEntryController(DBContext context) : base(context)
        {
        }
        [Route("api/EmployeesLoansEntry/GetEmployeesLoansEntryByEmployee")]
        [HttpGet]
        public ActionResult GetEmployeesLoansEntryByEmployee(int EmpID)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.EmployeesLoansEntryService.GetAllByEmployee(EmpID).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/EmployeesLoansEntry/GetEmployeesLoansEntryByTres")]
        [HttpGet]
        public ActionResult GetEmployeesLoansEntryByTres(int TresID)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.EmployeesLoansEntryService.GetAllByTreasury(TresID).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/EmployeesLoansEntry/GetEmployeesLoansEntry")]
        [HttpGet]
        public ActionResult GetEmployeesLoansEntry()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.EmployeesLoansEntryService.GetAll().ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/EmployeesLoansEntry/GetSpecificEmployeesLoansEntry")]
        [HttpGet]
        public ActionResult GetSpecificEmployeesLoansEntry(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                EmployeesLoansEntryVM model = UnitOfWork.EmployeesLoansEntryService.Get(Id);
                obj.Data = model;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/EmployeesLoansEntry/GetListsOfDDl")]
        [HttpGet]
        public ActionResult GetListsOfDDl()
        {
            EmployeesLoansEntryVM model = new EmployeesLoansEntryVM();
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                model.TreasuryList = UnitOfWork.TreasuryServices.GetAll().ToList();
                model.EmployeeList = UnitOfWork.EmployeeService.GetAll().ToList();
                obj.Data = model;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/EmployeesLoansEntry/AddEmployeesLoansEntry")]
        [HttpPost]

        public IActionResult AddEmployeesLoansEntry(EmployeesLoansEntryVM Model)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                Model.SerialNumber = UnitOfWork.EmployeesLoansEntryService.GenerateSerial();
                var modelSaved = UnitOfWork.EmployeesLoansEntryService.Add(Model);
                ///////////////////////////////////////////////////trans///////////////////////////////
                var currentBalance = UnitOfWork.TransEmployeeService.GetLatestBalance((int)Model.EmployeeId, Model.Date.ToLocalTime());
                Model.Amount = Model.Amount == null ? 0 : Model.Amount;
                decimal NewBalance = currentBalance + (decimal)Model.Amount;
                TransEmployeeVM TransEmployee = new TransEmployeeVM
                {
                    EmployeeId = (int)Model.EmployeeId,
                    EmployeeType = TransEmployeeTypeEnum.سلف_الموظف,
                    Credit = 0 ,
                    CreateDate = DateTime.Now,
                    OpenBalFlag = false,
                    Debit = (decimal)Model.Amount,
                    Date = Model.Date,
                    SerialNumber = Model.SerialNumber.Value,
                    IsDeleted = false,
                    Balance = NewBalance,
                };
                bool checkreturn = UnitOfWork.TransEmployeeService.Add(TransEmployee, (decimal)Model.Amount);
                UnitOfWork.Commit();
                //recacaluate
                if (!checkreturn)
                {
                    UnitOfWork.TransEmployeeService.RecalculateEmployeeBalance((int)Model.EmployeeId, Model.SerialNumber.Value);

                }
                ///////////////////////////////////////////////////////////////////////////
                UnitOfWork.Commit();
                obj.Data = modelSaved.ID;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }


        [Route("api/EmployeesLoansEntry/EditEmployeesLoansEntry")]
        [HttpPost]
        public IActionResult EditEmployeesLoansEntry(EmployeesLoansEntryVM Model)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                //////check if client changed for this bill
                EmployeesLoansEntryVM oldmodel = UnitOfWork.EmployeesLoansEntryService.Get(Model.ID);
                if (Model.EmployeeId != oldmodel.EmployeeId)
                {
                    int Id = UnitOfWork.TransEmployeeService.RecalculateEmployeeBalance((int)oldmodel.EmployeeId, (int)Model.SerialNumber);
                    UnitOfWork.TransEmployeeService.Delete(Id);
                }
                UnitOfWork.EmployeesLoansEntryService.Edit(Model);
                ///////////////////////////////////////////////////trans///////////////////////////////
                var currentBalance = UnitOfWork.TransEmployeeService.GetLatestBalance((int)Model.EmployeeId, Model.Date.ToLocalTime());
                Model.Amount = Model.Amount == null ? 0 : Model.Amount;
                decimal NewBalance = currentBalance + (decimal)Model.Amount;
                TransEmployeeVM TransEmployee = new TransEmployeeVM
                {
                    EmployeeId = (int)Model.EmployeeId,
                    EmployeeType = TransEmployeeTypeEnum.سلف_الموظف,
                    Credit = 0,
                    CreateDate = DateTime.Now,
                    OpenBalFlag = false,
                    Debit = (decimal)Model.Amount,
                    Date = Model.Date,
                    SerialNumber = Model.SerialNumber.Value,
                    IsDeleted = false,
                    Balance = NewBalance,
                };
                bool checkreturn = UnitOfWork.TransEmployeeService.Add(TransEmployee, (decimal)Model.Amount);
                UnitOfWork.Commit();
                //recacaluate
                if (!checkreturn)
                {
                    UnitOfWork.TransEmployeeService.RecalculateEmployeeBalance((int)Model.EmployeeId, Model.SerialNumber.Value);

                }
                ///////////////////////////////////////////////////////////////////////////
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }
        [Route("api/EmployeesLoansEntry/DeleteEmployeesLoansEntry")]
        [HttpGet]
        public IActionResult DeleteEmployeesLoansEntry(int ID, int SerialNumber, int EmployeeId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.EmployeesLoansEntryService.Delete(ID);
                if (EmployeeId != 0)
                {
                    int Id = UnitOfWork.TransEmployeeService.RecalculateEmployeeBalance(EmployeeId, SerialNumber);
                    UnitOfWork.TransEmployeeService.Delete(Id);
                }
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }


        [Route("api/EmployeesLoansEntry/GenerateSerial")]
        [HttpGet]
        public ActionResult GenerateSerial()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.EmployeesLoansEntryService.GenerateSerial();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }



    }
}
