﻿using MasProject.Data.DataAccess;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.FundTransaction;
using MasProject.Domain.ViewModel.TransVM;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static MasProject.Domain.Enums.Enums;

namespace MasProject.Server.Controllers.API.FundTransaction
{
    public class ClientsPayoffsController : BaseController
    {
   
        public ClientsPayoffsController(DBContext context) : base(context)
        {
        }


        [Route("api/ClientsPayoffs/GetClientsPayoffs")]
        [HttpGet]
        public ActionResult GetClientsPayoffs()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ClientsPayoffsService.GetAll().ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/ClientsPayoffs/GetAllByClient")]
        [HttpGet]
        public ActionResult GetAllByClient(int ClientId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ClientsPayoffsService.GetAllByClient(ClientId).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/ClientsPayoffs/GetAllByDate")]
        [HttpGet]
        public ActionResult GetAllByDate(DateTime date)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ClientsPayoffsService.GetAllByDate(date).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/ClientsPayoffs/GetAllByTreasury")]
        [HttpGet]
        public ActionResult GetAllByTreasury(int TreasuryId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ClientsPayoffsService.GetAllByTreasury(TreasuryId).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/ClientsPayoffs/GetSpecificClientsPayoffs")]
        [HttpGet]
        public ActionResult GetSpecificClientsPayoffs(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                ClientsPayoffsVM model = UnitOfWork.ClientsPayoffsService.Get(Id);
                obj.Data = model;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }



        [Route("api/ClientsPayoffs/GetListsOfDDl")]
        [HttpGet]
        public ActionResult GetListsOfDDl()
        {
            ClientsPayoffsVM model = new ClientsPayoffsVM();
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                model.CurrencyList = UnitOfWork.CurrencyServices.GetAll().ToList();
                model.TreasuryList = UnitOfWork.TreasuryServices.GetAll().ToList();
                model.ClientList = UnitOfWork.ClientsService.GetAll().ToList();
                model.SerialNumber = UnitOfWork.ClientsPayoffsService.GenerateSerial();
                model.Date = DateTime.Now.Date;
                model.CurrencyId = 2;
                model.Ratio = 1;
                obj.Data = model;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }


        [Route("api/ClientsPayoffs/GetCurrencyRatio")]
        [HttpGet]
        public ActionResult GetCurrencyRatio(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                var Ratio = UnitOfWork.CurrencyServices.Get(Id).Ratio;
                obj.Data = Ratio;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }


        [Route("api/ClientsPayoffs/AddClientsPayoffs")]
        [HttpPost]

        public IActionResult AddClientsPayoffs(ClientsPayoffsVM Model)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                Model.SerialNumber= UnitOfWork.ClientsPayoffsService.GenerateSerial();
                var modelSaved = UnitOfWork.ClientsPayoffsService.Add(Model);
                if (!Model.IsCheck || (Model.IsCheck && Model.IsCollected))
                {
                    ///////////////////////////////////////////////////trans///////////////////////////////
                    var currentBalance = UnitOfWork.TransClientService.GetLatestBalance((int)Model.ClientId, Model.Date.ToLocalTime());
                    Model.Total = Model.Total == null ? 0 : Model.Total;
                    decimal NewBalance =  currentBalance - (decimal)Model.Total;
                    TransClientVM TransClient = new TransClientVM
                    {
                        ClientId = (int)Model.ClientId,
                        ClientType = TransClientTypeEnum.مردودات_عملاء,
                        Credit = (decimal)Model.Total,
                        CreateDate = DateTime.Now,
                        OpenBalFlag = false,
                        Debit = 0,
                        Date = Model.Date,
                        SerialNumber = Model.SerialNumber.Value,
                        IsDeleted = false,
                        Balance = NewBalance,
                    };
                    bool checkreturn = UnitOfWork.TransClientService.Add(TransClient, (decimal)Model.Total);
                    UnitOfWork.Commit();
                    //recacaluate
                    if (!checkreturn)
                    {
                        UnitOfWork.TransClientService.RecalculateClientBalance((int)Model.ClientId, Model.SerialNumber.Value);
                    }
                }
                ///////////////////////////////////////////////////////////////////////////
                UnitOfWork.Commit();
                obj.Data = modelSaved.ID;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }



        [Route("api/ClientsPayoffs/EditClientsPayoffs")]
        [HttpPost]
        public IActionResult EditClientsPayoffs(ClientsPayoffsVM Model)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                //////check if client changed for this bill
                ClientsPayoffsVM oldmodel = UnitOfWork.ClientsPayoffsService.Get(Model.ID);
                if (Model.ClientId != oldmodel.ClientId)
                {
                    int Id = UnitOfWork.TransClientService.RecalculateClientBalance((int)oldmodel.ClientId, (int)Model.SerialNumber);
                    UnitOfWork.TransClientService.Delete(Id);
                }
                UnitOfWork.ClientsPayoffsService.Edit(Model);
                if (!Model.IsCheck || (Model.IsCheck && Model.IsCollected))
                {
                    ///////////////////////////////////////////////////trans///////////////////////////////
                    var currentBalance = UnitOfWork.TransClientService.GetLatestBalance((int)Model.ClientId, Model.Date.ToLocalTime());
                    Model.Total = Model.Total == null ? 0 : Model.Total;
                    decimal NewBalance = currentBalance - (decimal)Model.Total;
                    TransClientVM TransClient = new TransClientVM
                    {
                        ClientId = (int)Model.ClientId,
                        ClientType = TransClientTypeEnum.مردودات_عملاء,
                        Credit = (decimal)Model.Total,
                        CreateDate = DateTime.Now,
                        OpenBalFlag = false,
                        Debit = 0,
                        Date = Model.Date,
                        SerialNumber = Model.SerialNumber.Value,
                        IsDeleted = false,
                        Balance = NewBalance,
                    };
                    bool checkreturn = UnitOfWork.TransClientService.Add(TransClient, (decimal)Model.Total);
                    UnitOfWork.Commit();
                    //recacaluate
                    if (!checkreturn)
                    {
                        UnitOfWork.TransClientService.RecalculateClientBalance((int)Model.ClientId, Model.SerialNumber.Value);
                    }
                }
                ///////////////////////////////////////////////////////////////////////////
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }



        [Route("api/ClientsPayoffs/DeleteClientsPayoffs")]
        [HttpGet]
        public IActionResult DeleteClientsPayoffs(int ID, int SerialNumber, int ClientId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.ClientsPayoffsService.Delete(ID);
                if (ClientId != 0)
                {
                    int Id = UnitOfWork.TransClientService.RecalculateClientBalance(ClientId, SerialNumber);
                    UnitOfWork.TransClientService.Delete(Id);
                }
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }

        //[Route("api/ClientsPayoffs/GenerateSerial")]
        //[HttpGet]
        //public ActionResult GenerateSerial()
        //{
        //    var obj = new ResponseVM();
        //    try
        //    {
        //        obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
        //        obj.Message = ApiStatus.Success;
        //        obj.Data = UnitOfWork.ClientsPayoffsService.GenerateSerial();
        //        return Ok(obj);
        //    }
        //    catch (Exception)
        //    {
        //        return BadRequest();
        //    }
        //}



    }
}
