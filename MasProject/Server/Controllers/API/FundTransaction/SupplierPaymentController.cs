﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasProject.Data.DataAccess;
using MasProject.Data.Models.TransTables;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.FundTransaction.SupplierPayments;
using MasProject.Domain.ViewModel.TransVM;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using static MasProject.Domain.Enums.Enums;

namespace MasProject.Server.Controllers.API.FundTransaction
{
    public class SupplierPaymentController : BaseController
    {
        public SupplierPaymentController(DBContext context) : base(context)
        {
        }
        [Route("api/SupplierPayment/GetSupplierPaymentBySupplier")]
        [HttpGet]
        public ActionResult GetSupplierPaymentBySupplier(int SuppID)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.SupplierPaymentService.GetAllBySupplier(SuppID).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/SupplierPayment/GetSupplierPaymentByTres")]
        [HttpGet]
        public ActionResult GetSupplierPaymentByTres(int TresID)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.SupplierPaymentService.GetAllByTreasury(TresID).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/SupplierPayment/GetSupplierPayment")]
        [HttpGet]
        public ActionResult GetSupplierPayment()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.SupplierPaymentService.GetAll().ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }


        [Route("api/SupplierPayment/GetSpecificSupplierPayment")]
        [HttpGet]
        public ActionResult GetSpecificSupplierPayment(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                SupplierPaymentVM model = UnitOfWork.SupplierPaymentService.Get(Id);
                obj.Data = model;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }


        [Route("api/SupplierPayment/GetListsOfDDl")]
        [HttpGet]
        public ActionResult GetListsOfDDl()
        {
            SupplierPaymentVM model = new SupplierPaymentVM();
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                model.CurrencyList = UnitOfWork.CurrencyServices.GetAll().ToList();
                model.TreasuryList = UnitOfWork.TreasuryServices.GetAll().ToList();
                model.SupplierList = UnitOfWork.SupplierService.GetAll().ToList();
                obj.Data = model;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }


        [Route("api/SupplierPayment/GetCurrencyRatio")]
        [HttpGet]
        public ActionResult GetCurrencyRatio(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                var Ratio = UnitOfWork.CurrencyServices.Get(Id).Ratio;
                obj.Data = Ratio;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }


        [Route("api/SupplierPayment/AddSupplierPayment")]
        [HttpPost]
        public IActionResult AddSupplierPayment(SupplierPaymentVM Model)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                Model.SerialNumber = UnitOfWork.SupplierPaymentService.GenerateSerial();
                var modelSaved = UnitOfWork.SupplierPaymentService.Add(Model);
                if (!Model.IsCheck || (Model.IsCheck && Model.IsCollected))
                {
                    ///////////////////////////////////////////////////trans///////////////////////////////
                    var currentBalance = UnitOfWork.TransSupplierService.GetLatestBalance((int)Model.SupplierId, Model.Date.ToLocalTime());
                    Model.Total = Model.Total == null ? 0 : Model.Total;
                    decimal NewBalance = currentBalance + (decimal)Model.Total;
                    TransSupplierVM TransSupplier = new TransSupplierVM
                    {
                        SupplierId = (int)Model.SupplierId,
                        SupplierType = TransSupplierTypeEnum.دفعات_موردين,
                        Credit = 0,
                        CreateDate = DateTime.Now,
                        OpenBalFlag = false,
                        Debit = (decimal)Model.Total,
                        Date = Model.Date,
                        SerialNumber = Model.SerialNumber.Value,
                        IsDeleted = false,
                        Balance = NewBalance,
                    };
                    bool checkreturn = UnitOfWork.TransSupplierService.Add(TransSupplier, (decimal)Model.Amount);
                    UnitOfWork.Commit();
                    //recacaluate
                    if (!checkreturn)
                    {
                        UnitOfWork.TransSupplierService.RecalculateSupplierBalance((int)Model.SupplierId, Model.SerialNumber.Value);

                    }
                }
                ///////////////////////////////////////////////////////////////////////////
                UnitOfWork.Commit();
                obj.Data = modelSaved.ID;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }


        [Route("api/SupplierPayment/EditSupplierPayment")]
        [HttpPost]
        public IActionResult EditSupplierPayment(SupplierPaymentVM Model)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                //////check if client changed for this bill
                SupplierPaymentVM oldmodel = UnitOfWork.SupplierPaymentService.Get(Model.ID);
                if (Model.SupplierId != oldmodel.SupplierId)
                {
                    int Id = UnitOfWork.TransSupplierService.RecalculateSupplierBalance((int)oldmodel.SupplierId, (int)Model.SerialNumber);
                    UnitOfWork.TransSupplierService.Delete(Id);
                }
                UnitOfWork.SupplierPaymentService.Edit(Model);
                if (!Model.IsCheck || (Model.IsCheck && Model.IsCollected))
                {
                    ///////////////////////////////////////////////////trans///////////////////////////////
                    var currentBalance = UnitOfWork.TransSupplierService.GetLatestBalance((int)Model.SupplierId, Model.Date.ToLocalTime());
                    Model.Total = Model.Total == null ? 0 : Model.Total;
                    decimal NewBalance = currentBalance + (decimal)Model.Total;
                    TransSupplierVM TransSupplier = new TransSupplierVM
                    {
                        SupplierId = (int)Model.SupplierId,
                        SupplierType = TransSupplierTypeEnum.دفعات_موردين,
                        Credit = 0,
                        CreateDate = DateTime.Now,
                        OpenBalFlag = false,
                        Debit = (decimal)Model.Total,
                        Date = Model.Date,
                        SerialNumber = Model.SerialNumber.Value,
                        IsDeleted = false,
                        Balance = NewBalance,
                    };
                    bool checkreturn = UnitOfWork.TransSupplierService.Add(TransSupplier, (decimal)Model.Total);
                    UnitOfWork.Commit();
                    //recacaluate
                    if (!checkreturn)
                    {
                        UnitOfWork.TransSupplierService.RecalculateSupplierBalance((int)Model.SupplierId, Model.SerialNumber.Value);

                    }
                    ///////////////////////////////////////////////////////////////////////////
                }
                UnitOfWork.Commit();

                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }


        [Route("api/SupplierPayment/DeleteSupplierPayment")]
        [HttpGet]
        public IActionResult DeleteSupplierPayment(int ID, int SerialNumber, int SupplierId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.SupplierPaymentService.Delete(ID);
                if (SupplierId != 0)
                {
                    int Id = UnitOfWork.TransSupplierService.RecalculateSupplierBalance(SupplierId, SerialNumber);
                    UnitOfWork.TransSupplierService.Delete(Id);
                }
               
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }

        [Route("api/SupplierPayment/GenerateSerial")]
        [HttpGet]
        public ActionResult GenerateSerial()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.SupplierPaymentService.GenerateSerial();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}
