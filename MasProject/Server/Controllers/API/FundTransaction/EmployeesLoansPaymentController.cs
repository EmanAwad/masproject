﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasProject.Data.DataAccess;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.FundTransaction;
using MasProject.Domain.ViewModel.TransVM;
using Microsoft.AspNetCore.Mvc;
using static MasProject.Domain.Enums.Enums;

namespace MasProject.Server.Controllers.API.FundTransaction
{
    public class EmployeesLoansPaymentController : BaseController
    {
        public EmployeesLoansPaymentController(DBContext context) : base(context)
        {
        }
        [Route("api/EmployeesLoansPayment/GetEmployeesLoansPaymentByTres")]
        [HttpGet]
        public ActionResult GetEmployeesLoansPaymentByTres(int TresID)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.EmployeesLoansPaymentService.GetAllByTreasury(TresID).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/EmployeesLoansPayment/GetEmployeesLoansPaymentByEmp")]
        [HttpGet]
        public ActionResult GetEmployeesLoansPaymentByEmp(int EmpID)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.EmployeesLoansPaymentService.GetAllByEmployee(EmpID).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/EmployeesLoansPayment/GetEmployeesLoansPayment")]
        [HttpGet]
        public ActionResult GetEmployeesLoansPayment()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.EmployeesLoansPaymentService.GetAll().ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/EmployeesLoansPayment/GetSpecificEmployeesLoansPayment")]
        [HttpGet]
        public ActionResult GetSpecificEmployeesLoansPayment(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                EmployeesLoansPaymentVM model = UnitOfWork.EmployeesLoansPaymentService.Get(Id);
                obj.Data = model;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/EmployeesLoansPayment/GetListsOfDDl")]
        [HttpGet]
        public ActionResult GetListsOfDDl()
        {
            EmployeesLoansPaymentVM model = new EmployeesLoansPaymentVM();
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                model.TreasuryList = UnitOfWork.TreasuryServices.GetAll().ToList();
                model.EmployeeList = UnitOfWork.EmployeeService.GetAll().ToList();
                obj.Data = model;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/EmployeesLoansPayment/AddEmployeesLoansPayment")]
        [HttpPost]

        public IActionResult AddEmployeesLoansPayment(EmployeesLoansPaymentVM Model)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                Model.SerialNumber = UnitOfWork.EmployeesLoansPaymentService.GenerateSerial();
                var modelSaved = UnitOfWork.EmployeesLoansPaymentService.Add(Model);
                ///////////////////////////////////////////////////trans///////////////////////////////
                var currentBalance = UnitOfWork.TransEmployeeService.GetLatestBalance((int)Model.EmployeeId, Model.Date.ToLocalTime());
                Model.Amount = Model.Amount == null ? 0 : Model.Amount;
                decimal NewBalance = currentBalance - (decimal)Model.Amount;
                TransEmployeeVM TransEmployee = new TransEmployeeVM
                {
                    EmployeeId = (int)Model.EmployeeId,
                    EmployeeType = TransEmployeeTypeEnum.رد_سلف_الموظف,
                    Credit = (decimal)Model.Amount,
                    CreateDate = DateTime.Now,
                    OpenBalFlag = false,
                    Debit = 0,
                    Date = Model.Date,
                    SerialNumber = Model.SerialNumber.Value,
                    IsDeleted = false,
                    Balance = NewBalance,
                };
                bool checkreturn = UnitOfWork.TransEmployeeService.Add(TransEmployee, (decimal)Model.Amount);
                UnitOfWork.Commit();
                //recacaluate
                if (!checkreturn)
                {
                    UnitOfWork.TransEmployeeService.RecalculateEmployeeBalance((int)Model.EmployeeId, Model.SerialNumber.Value);

                }
                ///////////////////////////////////////////////////////////////////////////
                UnitOfWork.Commit();
                obj.Data = modelSaved.ID;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }


        [Route("api/EmployeesLoansPayment/EditEmployeesLoansPayment")]
        [HttpPost]
        public IActionResult EditEmployeesLoansPayment(EmployeesLoansPaymentVM Model)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                EmployeesLoansPaymentVM oldmodel = UnitOfWork.EmployeesLoansPaymentService.Get(Model.ID);
                if (Model.EmployeeId != oldmodel.EmployeeId)
                {
                    int Id = UnitOfWork.TransEmployeeService.RecalculateEmployeeBalance((int)oldmodel.EmployeeId, (int)Model.SerialNumber);
                    UnitOfWork.TransEmployeeService.Delete(Id);
                }
                UnitOfWork.EmployeesLoansPaymentService.Edit(Model);
                ///////////////////////////////////////////////////trans///////////////////////////////
                var currentBalance = UnitOfWork.TransEmployeeService.GetLatestBalance((int)Model.EmployeeId, Model.Date.ToLocalTime());
                Model.Amount = Model.Amount == null ? 0 : Model.Amount;
                decimal NewBalance =  currentBalance - (decimal)Model.Amount;
                TransEmployeeVM TransEmployee = new TransEmployeeVM
                {
                    EmployeeId = (int)Model.EmployeeId,
                    EmployeeType = TransEmployeeTypeEnum.رد_سلف_الموظف,
                    Credit = (decimal)Model.Amount,
                    CreateDate = DateTime.Now,
                    OpenBalFlag = false,
                    Debit = 0,
                    Date = Model.Date,
                    SerialNumber = Model.SerialNumber.Value,
                    IsDeleted = false,
                    Balance = NewBalance,
                };
                bool checkreturn = UnitOfWork.TransEmployeeService.Add(TransEmployee, (decimal)Model.Amount);
                UnitOfWork.Commit();
                //recacaluate
                if (!checkreturn)
                {
                    UnitOfWork.TransEmployeeService.RecalculateEmployeeBalance((int)Model.EmployeeId, Model.SerialNumber.Value);

                }
                ///////////////////////////////////////////////////////////////////////////
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }
        [Route("api/EmployeesLoansPayment/DeleteEmployeesLoansPayment")]
        [HttpGet]
        public IActionResult DeleteEmployeesLoansPayment(int ID, int SerialNumber, int EmployeeId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.EmployeesLoansPaymentService.Delete(ID);
                if (EmployeeId != 0)
                {
                    int Id = UnitOfWork.TransEmployeeService.RecalculateEmployeeBalance(EmployeeId, SerialNumber);
                    UnitOfWork.TransEmployeeService.Delete(Id);
                }
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }

        [Route("api/EmployeesLoansPayment/GenerateSerial")]
        [HttpGet]
        public ActionResult GenerateSerial()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.EmployeesLoansPaymentService.GenerateSerial();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}
