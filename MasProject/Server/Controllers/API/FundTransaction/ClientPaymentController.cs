﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasProject.Data.DataAccess;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.FundTransaction.ClientPayments;
using MasProject.Domain.ViewModel.TransVM;
using Microsoft.AspNetCore.Mvc;
using static MasProject.Domain.Enums.Enums;

namespace MasProject.Server.Controllers.API.FundTransaction
{
    public class ClientPaymentController : BaseController
    {
        public ClientPaymentController(DBContext context) : base(context)
        {
        }
        [Route("api/ClientPayment/GetClientPayment")]
        [HttpGet]
        public ActionResult GetClientPayment()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ClientPaymentService.GetAll().ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/ClientPayment/GetAllByClient")]
        [HttpGet]
        public ActionResult GetAllByClient(int ClientId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ClientPaymentService.GetAllByClient(ClientId).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/ClientPayment/GetAllByDate")]
        [HttpGet]
        public ActionResult GetAllByDate(DateTime date)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ClientPaymentService.GetAllByDate(date).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/ClientPayment/GetAllByTreasury")]
        [HttpGet]
        public ActionResult GetAllByTreasury(int TreasuryId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ClientPaymentService.GetAllByTreasury(TreasuryId).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/ClientPayment/GetSpecificClientPayment")]
        [HttpGet]
        public ActionResult GetSpecificClientPayment(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                ClientPaymentVM model = UnitOfWork.ClientPaymentService.Get(Id);
                obj.Data = model;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/ClientPayment/GetListsOfDDl")]
        [HttpGet]
        public ActionResult GetListsOfDDl()
        {
            ClientPaymentVM model = new ClientPaymentVM();
           var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                model.CurrencyList = UnitOfWork.CurrencyServices.GetAll().ToList();
                model.TreasuryList = UnitOfWork.TreasuryServices.GetAll().ToList();
                model.ClientList = UnitOfWork.ClientsService.GetAll().ToList();//getclients
                obj.Data = model;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/ClientPayment/GetCurrencyRatio")]
        [HttpGet]
        public ActionResult GetCurrencyRatio(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                var Ratio = UnitOfWork.CurrencyServices.Get(Id).Ratio;
                obj.Data = Ratio;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/ClientPayment/AddClientPayment")]
        [HttpPost]

        public IActionResult AddClientPayment(ClientPaymentVM Model)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                //new 13-01-2021//to get serial unique
                Model.SerialNumber = UnitOfWork.ClientPaymentService.GenerateSerial(Model.TreasuryId);
                var modelSaved = UnitOfWork.ClientPaymentService.Add(Model);
                if (!Model.IsCheck ||(Model.IsCheck && Model.IsCollected))
                {
                    ///////////////////////////////////////////////////trans///////////////////////////////
                    var currentBalance = UnitOfWork.TransClientService.GetLatestBalance((int)Model.ClientId, Model.Date.ToLocalTime());
                    Model.Amount = Model.Amount == null ? 0 : Model.Amount;
                    decimal NewBalance = currentBalance - (decimal)Model.Amount;

                    TransClientVM TransClient = new TransClientVM
                    {
                        ClientId = (int)Model.ClientId,
                        ClientType = TransClientTypeEnum.دفعات_عملاء,
                        Credit = (decimal)Model.Amount,
                        CreateDate = DateTime.Now,
                        OpenBalFlag = false,
                        Debit = 0,
                        Date = Model.Date,
                        SerialNumber = Model.SerialNumber.Value,
                        IsDeleted = false,
                        Balance = NewBalance,
                    };
                    bool checkreturn = UnitOfWork.TransClientService.Add(TransClient, (decimal)Model.Amount);
                    UnitOfWork.Commit();
                    //recacaluate
                    if (!checkreturn)
                    {
                        UnitOfWork.TransClientService.RecalculateClientBalance((int)Model.ClientId, Model.SerialNumber.Value);
                    }
                }
                ///////////////////////////////////////////////////////////////////////////
                UnitOfWork.Commit();
                obj.Data = modelSaved.ID;
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }


        [Route("api/ClientPayment/EditClientPayment")]
        [HttpPost]
        public IActionResult EditClientPayment(ClientPaymentVM Model)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                //////check if client changed for this bill
                ClientPaymentVM oldmodel = UnitOfWork.ClientPaymentService.Get(Model.ID);
                if (Model.ClientId != oldmodel.ClientId)
                {
                    int Id = UnitOfWork.TransClientService.RecalculateClientBalance((int)oldmodel.ClientId, (int)Model.SerialNumber);
                    UnitOfWork.TransClientService.Delete(Id);
                }
                UnitOfWork.ClientPaymentService.Edit(Model);
                ///////////////////////////////////////////////////trans///////////////////////////////
                if (!Model.IsCheck || (Model.IsCheck && Model.IsCollected))
                {
                    var currentBalance = UnitOfWork.TransClientService.GetLatestBalance((int)Model.ClientId, Model.Date.ToLocalTime());
                    Model.Amount = Model.Amount == null ? 0 : Model.Amount;
                    decimal NewBalance = currentBalance - (decimal)Model.Amount;
                    if (Model.Total == null|| Model.Total==0)
                    {
                        Model.Total = Model.Amount;
                    }
                    TransClientVM TransClient = new TransClientVM
                    {
                        ClientId = (int)Model.ClientId,
                        ClientType = TransClientTypeEnum.دفعات_عملاء,
                        Credit = (decimal)Model.Total,
                        CreateDate = DateTime.Now,
                        OpenBalFlag = false,
                        Debit = 0,
                        Date = Model.Date,
                        SerialNumber = Model.SerialNumber.Value,
                        IsDeleted = false,
                        Balance = NewBalance,
                    };
                    bool checkreturn = UnitOfWork.TransClientService.Add(TransClient, (decimal)Model.Amount);
                    UnitOfWork.Commit();
                    //recacaluate
                    if (!checkreturn)
                    {
                        UnitOfWork.TransClientService.RecalculateClientBalance((int)Model.ClientId, Model.SerialNumber.Value);
                    }
                }
                ///////////////////////////////////////////////////////////////////////////
                UnitOfWork.Commit();
               obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }
        [Route("api/ClientPayment/DeleteClientPayment")]
        [HttpGet]
        public IActionResult DeleteClientPayment(int ID)//, int SerialNumber, int ClientId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                ClientPaymentVM DeletedModel = UnitOfWork.ClientPaymentService.Get(ID);
                UnitOfWork.ClientPaymentService.Delete(ID);
                if (DeletedModel.ClientId != 0 && DeletedModel.ClientId != null && DeletedModel.SerialNumber != null && DeletedModel.SerialNumber != 0)
                {
                    int Id = UnitOfWork.TransClientService.RecalculateClientBalance((int)DeletedModel.ClientId, (int)DeletedModel.SerialNumber);
                    UnitOfWork.TransClientService.Delete(Id);
                }
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }

        [Route("api/ClientPayment/GenerateSerial")]
        [HttpGet]
        public ActionResult GenerateSerial(int TreasuryId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.ClientPaymentService.GenerateSerial(TreasuryId);
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        //[Route("api/ClientPayment/UpdateSerial")]
        //[HttpGet]
        //public ActionResult UpdateSerial()
        //{
        //    var obj = new ResponseVM();
        //    try
        //    {
        //        obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
        //        obj.Message = ApiStatus.Success;
        //        List<ClientPaymentVM> ListOFBills = UnitOfWork.ClientPaymentService.GetAllForEdit().ToList();
        //        //set client id into list
        //        List<int?> clientsList = ListOFBills.Where(x=>x.ClientId !=null).Select(c => c.ClientId).Distinct().ToList();

        //        foreach (var NotBillModel in ListOFBills)
        //        {
        //            if(NotBillModel.Date.ToString("yyyy-MM-dd")== "2021-02-14" && NotBillModel.ClientId==284)
        //            {
        //                string mess = "";
        //            }
        //            ClientPaymentVM BillModel = UnitOfWork.ClientPaymentService.Get(NotBillModel.ID);
        //            int NewSerial = UnitOfWork.ClientPaymentService.GenerateSerial(BillModel.TreasuryId);
        //            //trans is missing //think about it
        //            TransClientVM trans = UnitOfWork.TransClientService.GetForEdit((int)BillModel.ClientId, BillModel.SerialNumber.Value,6);
        //            if (trans != null)
        //            {
        //                trans.SerialNumber = NewSerial;
        //                UnitOfWork.TransClientService.EditForEdit(trans);
        //            }
        //            else
        //            {
        //                string mess = "";
        //            }
        //            BillModel.SerialNumber = NewSerial;
        //            UnitOfWork.ClientPaymentService.Edit(BillModel);
        //            UnitOfWork.Commit();
        //        }
                
        //        //must update clients balance
        //        foreach (var ClientId in clientsList)
        //        {
        //            UnitOfWork.TransClientService.UpdateClientBalance((int)ClientId);
        //        }
        //        UnitOfWork.Commit();
        //        return Ok(obj);
        //    }
        //    catch (Exception ex)
        //    {
        //        string message = ex.Message;
        //        return BadRequest();
        //    }
        //}

    }
}