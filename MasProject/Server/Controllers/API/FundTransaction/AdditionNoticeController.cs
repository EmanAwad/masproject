﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasProject.Data.DataAccess;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.FundTransaction.AdditionNotice;
using MasProject.Domain.ViewModel.TransVM;
using Microsoft.AspNetCore.Mvc;
using static MasProject.Domain.Enums.Enums;
using Microsoft.AspNetCore.Mvc;

namespace MasProject.Server.Controllers.API.FundTransaction
{
    
    public class AdditionNoticeController : BaseController
    {
        public AdditionNoticeController(DBContext context) : base(context)
        {
        }
        [Route("api/AdditiontNotice/GetAdditiontNoticeByClient")]
        [HttpGet]
        public ActionResult GetAdditiontNoticeByClient(int ClientID)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.AdditionNoticeService.GetAllByClient(ClientID).ToList();
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
        [Route("api/AdditiontNotice/GetAdditiontNoticeBySupplier")]
        [HttpGet]
        public ActionResult GetAdditiontNoticeBySupplier(int SupplierID)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.AdditionNoticeService.GetAllBySupplier(SupplierID).ToList();
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
        [Route("api/AdditiontNotice/GetAdditiontNotice")]
        [HttpGet]
        public ActionResult GetAdditiontNotice()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.AdditionNoticeService.GetAll().ToList();
                return Ok(obj);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [Route("api/AdditiontNotice/GetSpecificAdditiontNotice")]
        [HttpGet]
        public ActionResult GetSpecificAdditiontNotice(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                AdditionNoticeVM model = UnitOfWork.AdditionNoticeService.Get(Id);
                obj.Data = model;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }


        [Route("api/AdditiontNotice/GetListsOfDDl")]
        [HttpGet]
        public ActionResult GetListsOfDDl()
        {
            AdditionNoticeVM model = new AdditionNoticeVM();
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                model.CurrencyList = UnitOfWork.CurrencyServices.GetAll().ToList();
                model.SupplierList = UnitOfWork.SupplierService.GetAll().ToList();
                model.ClientList = UnitOfWork.ClientsService.GetAll().ToList();
                obj.Data = model;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/AdditiontNotice/AddAdditiontNotice")]
        [HttpPost]

        public IActionResult AddAdditiontNotice(AdditionNoticeVM Model)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                Model.SerialNumber = UnitOfWork.AdditionNoticeService.GenerateSerial();
                var modelSaved = UnitOfWork.AdditionNoticeService.Add(Model);
                if (Model.UserTypeFlag == 1)
                {
                    var currentBalance = UnitOfWork.TransClientService.GetLatestBalance((int)Model.ClientId, Model.Date.ToLocalTime());
                    Model.Amount = Model.Amount == null ? 0 : Model.Amount;
                    decimal NewBalance = currentBalance + (decimal)Model.Amount;
                    TransClientVM TransClient = new TransClientVM
                    {
                        ClientId = (int)Model.ClientId,
                        ClientType = TransClientTypeEnum.إشعار_إضافة_عملاء,
                        Credit = 0,
                        CreateDate = DateTime.Now,
                        OpenBalFlag = false,
                        Debit = (decimal)Model.Amount,
                        Date = Model.Date,
                        SerialNumber = Model.SerialNumber.Value,
                        IsDeleted = false,
                        Balance = NewBalance,
                    };
                    bool checkreturn = UnitOfWork.TransClientService.Add(TransClient, (decimal)Model.Amount);
                    UnitOfWork.Commit();
                    //recacaluate
                    if (!checkreturn)
                    {
                        UnitOfWork.TransClientService.RecalculateClientBalance((int)Model.ClientId, Model.SerialNumber.Value);
                    }
                    ///////////////////////////////////////////////////////////////////////////

                }
                if (Model.UserTypeFlag == 2)
                
                {
                    ///////////////////////////////////////////////////trans///////////////////////////////
                    var currentBalance = UnitOfWork.TransSupplierService.GetLatestBalance((int)Model.SupplierId, Model.Date.ToLocalTime());
                    Model.Amount = Model.Amount == null ? 0 : Model.Amount;
                    decimal NewBalance =  currentBalance + (decimal)Model.Amount;
                    TransSupplierVM TransSupplier = new TransSupplierVM
                    {
                        SupplierId = (int)Model.SupplierId,
                        SupplierType = TransSupplierTypeEnum.إشعار_إضافة_موردين,
                        Credit = 0,
                        CreateDate = DateTime.Now,
                        OpenBalFlag = false,
                        Debit = (decimal)Model.Amount,
                        Date = Model.Date,
                        SerialNumber = Model.SerialNumber.Value,
                        IsDeleted = false,
                        Balance = NewBalance,
                    };
                    UnitOfWork.TransSupplierService.Add(TransSupplier, (decimal)Model.Amount);
                    bool checkreturn = UnitOfWork.TransSupplierService.Add(TransSupplier, (decimal)Model.Amount);
                    UnitOfWork.Commit();
                    //recacaluate
                    if (!checkreturn)
                    {
                        UnitOfWork.TransSupplierService.RecalculateSupplierBalance((int)Model.SupplierId, Model.SerialNumber.Value);
                    }
                    //////////////////////////////////////////////////////////////////////////
                }
               
                UnitOfWork.Commit();
                obj.Data = modelSaved.ID;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/AdditiontNotice/EditAdditiontNotice")]
        [HttpPost]
        public IActionResult EditAdditiontNotice(AdditionNoticeVM Model)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                //////check if client changed for this bill
                AdditionNoticeVM Oldmodel = UnitOfWork.AdditionNoticeService.Get(Model.ID);
                UnitOfWork.AdditionNoticeService.Edit(Model);
                if (Model.UserTypeFlag == 1)
                {
                    if (Model.ClientId != Oldmodel.ClientId)
                    {
                        int Id = UnitOfWork.TransClientService.RecalculateClientBalance((int)Oldmodel.ClientId, (int)Model.SerialNumber);
                        UnitOfWork.TransClientService.Delete(Id);
                    }
                    ///////////////////////////////////////////////////trans///////////////////////////////
                    var currentBalance = UnitOfWork.TransClientService.GetLatestBalance((int)Model.ClientId, Model.Date.ToLocalTime());
                    Model.Amount = Model.Amount == null ? 0 : Model.Amount;
                    decimal NewBalance = currentBalance + (decimal)Model.Amount;
                    TransClientVM TransClient = new TransClientVM
                    {
                        ClientId = (int)Model.ClientId,
                        ClientType = TransClientTypeEnum.إشعار_إضافة_عملاء,
                        Credit = 0,
                        CreateDate = DateTime.Now,
                        OpenBalFlag = false,
                        Debit = (decimal)Model.Amount,
                        Date = Model.Date,
                        SerialNumber = Model.SerialNumber.Value,
                        IsDeleted = false,
                        Balance = NewBalance,
                    };
                    bool checkreturn = UnitOfWork.TransClientService.Add(TransClient, (decimal)Model.Amount);
                    UnitOfWork.Commit();
                    //recacaluate
                    if (!checkreturn)
                    {
                        UnitOfWork.TransClientService.RecalculateClientBalance((int)Model.ClientId, Model.SerialNumber.Value);
                    }
                }
                if (Model.UserTypeFlag == 2)
                {
                    if (Model.SupplierId != Oldmodel.SupplierId)
                    {
                        int Id = UnitOfWork.TransSupplierService.RecalculateSupplierBalance((int)Oldmodel.SupplierId, (int)Model.SerialNumber);
                        UnitOfWork.TransSupplierService.Delete(Id);
                    }
                    ///////////////////////////////////////////////////trans///////////////////////////////
                    var currentBalance = UnitOfWork.TransSupplierService.GetLatestBalance((int)Model.SupplierId, Model.Date.ToLocalTime());
                    Model.Amount = Model.Amount == null ? 0 : Model.Amount;
                    decimal NewBalance =  currentBalance + (decimal)Model.Amount;
                    TransSupplierVM TransSupplier = new TransSupplierVM
                    {
                        SupplierId = (int)Model.SupplierId,
                        SupplierType = TransSupplierTypeEnum.إشعار_إضافة_موردين,
                        Credit =0,
                        CreateDate = DateTime.Now,
                        OpenBalFlag = false,
                        Debit = (decimal)Model.Amount,
                        Date = Model.Date,
                        SerialNumber = Model.SerialNumber.Value,
                        IsDeleted = false,
                        Balance = NewBalance,
                    };
                    bool checkreturn = UnitOfWork.TransSupplierService.Add(TransSupplier, (decimal)Model.Amount);
                    UnitOfWork.Commit();
                    //recacaluate
                    if (!checkreturn)
                    {
                        UnitOfWork.TransSupplierService.RecalculateSupplierBalance((int)Model.SupplierId, Model.SerialNumber.Value);

                    }
                    ///////////////////////////////////////////////////////////////////////////
                }
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }

        [Route("api/AdditiontNotice/DeleteAdditiontNotice")]
        [HttpGet]
        public IActionResult DeleteAdditiontNotice(int ID)//, int SerialNumber, int SupplierId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.AdditionNoticeService.Delete(ID);
                AdditionNoticeVM Oldmodel = UnitOfWork.AdditionNoticeService.Get(ID);
                if (Oldmodel.UserTypeFlag == 1)
                {
                    if (Oldmodel.ClientId != 0)
                    {
                        int Id = UnitOfWork.TransClientService.RecalculateClientBalance((int)Oldmodel.ClientId, (int)Oldmodel.SerialNumber);
                        UnitOfWork.TransClientService.Delete(Id);
                    }
                }
                if (Oldmodel.UserTypeFlag == 2)
                {
                    if (Oldmodel.SupplierId != 0)
                    {
                        int Id = UnitOfWork.TransSupplierService.RecalculateSupplierBalance((int)Oldmodel.SupplierId, (int)Oldmodel.SerialNumber);
                        UnitOfWork.TransSupplierService.Delete(Id);
                    }
                }
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }

        [Route("api/AdditiontNotice/GenerateSerial")]
        [HttpGet]
        public ActionResult GenerateSerial()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.AdditionNoticeService.GenerateSerial();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}
