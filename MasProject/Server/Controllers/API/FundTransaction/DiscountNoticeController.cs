﻿using MasProject.Data.DataAccess;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using MasProject.Data.DataAccess;
using MasProject.Domain.Resources;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.FundTransaction.DiscountNotices;
using Microsoft.AspNetCore.Mvc;
using MasProject.Domain.ViewModel.TransVM;
using static MasProject.Domain.Enums.Enums;

namespace MasProject.Server.Controllers.API.FundTransaction
{
    public class DiscountNoticeController : BaseController
    {
        public DiscountNoticeController(DBContext context) : base(context)
        {
        }
        [Route("api/DiscountNotice/GetDiscountNoticeByClient")]
        [HttpGet]
        public ActionResult GetDiscountNoticeByClient(int ClientID)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.DiscountNoticeService.GetAllByClient(ClientID).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/DiscountNotice/GetDiscountNoticeBySupplier")]
        [HttpGet]
        public ActionResult GetDiscountNoticeBySupplier(int SuppID)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.DiscountNoticeService.GetAllBySupplier(SuppID).ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/DiscountNotice/GetDiscountNotice")]
        [HttpGet]
        public ActionResult GetDiscountNotice()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.DiscountNoticeService.GetAll().ToList();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/DiscountNotice/GetSpecificDiscountNotice")]
        [HttpGet]
        public ActionResult GetSpecificDiscountNotice(int Id)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                DiscountNoticeVM model = UnitOfWork.DiscountNoticeService.Get(Id);
                obj.Data = model;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/DiscountNotice/GetListsOfDDl")]
        [HttpGet]
        public ActionResult GetListsOfDDl()
        {
            DiscountNoticeVM model = new DiscountNoticeVM();
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                model.CurrencyList = UnitOfWork.CurrencyServices.GetAll().ToList();
                model.ClientList = UnitOfWork.ClientsService.GetAll().ToList();
                model.SupplierList = UnitOfWork.SupplierService.GetAll().ToList();
                obj.Data = model;
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Route("api/DiscountNotice/AddDiscountNotice")]
        [HttpPost]

        public IActionResult AddDiscountNotice(DiscountNoticeVM Model)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                Model.SerialNumber = UnitOfWork.DiscountNoticeService.GenerateSerial();
                var modelSaved = UnitOfWork.DiscountNoticeService.Add(Model);
                /////////////////////////////////////////////////trans///////////////////////////////
                ///
                if (Model.UserTypeFlag == 1)
                {
                    var currentBalance = UnitOfWork.TransClientService.GetLatestBalance((int)Model.ClientId, Model.Date.ToLocalTime());
                    Model.Amount = Model.Amount == null ? 0 : Model.Amount;
                    decimal NewBalance = currentBalance - (decimal)Model.Amount;
                    TransClientVM TransClient = new TransClientVM
                    {
                        ClientId = (int)Model.ClientId,
                        ClientType = TransClientTypeEnum.تسوية_إشعار_خصم,
                        Credit = (decimal)Model.Amount,
                        CreateDate = DateTime.Now,
                        OpenBalFlag = false,
                        Debit = 0,
                        Date = Model.Date,
                        SerialNumber = Model.SerialNumber.Value,
                        IsDeleted = false,
                        Balance = NewBalance,
                    };
                    bool checkreturn = UnitOfWork.TransClientService.Add(TransClient, (decimal)Model.Amount);
                    UnitOfWork.Commit();
                    //recacaluate
                    if (!checkreturn)
                    {
                        UnitOfWork.TransClientService.RecalculateClientBalance((int)Model.ClientId, Model.SerialNumber.Value);
                    }
                }
                if (Model.UserTypeFlag == 2)

                {
                    ///////////////////////////////////////////////////trans///////////////////////////////
                    var currentBalance = UnitOfWork.TransSupplierService.GetLatestBalance((int)Model.SupplierId, Model.Date.ToLocalTime());
                    Model.Amount = Model.Amount == null ? 0 : Model.Amount;
                    decimal NewBalance = currentBalance + (decimal)Model.Amount;
                    TransSupplierVM TransSupplier = new TransSupplierVM
                    {
                        SupplierId = (int)Model.SupplierId,
                        SupplierType = TransSupplierTypeEnum.إشعار_خصم,
                        Credit = 0,
                        CreateDate = DateTime.Now,
                        OpenBalFlag = false,
                        Debit = (decimal)Model.Amount,
                        Date = Model.Date,
                        SerialNumber = Model.SerialNumber.Value,
                        IsDeleted = false,
                        Balance = NewBalance,
                    };
                    UnitOfWork.TransSupplierService.Add(TransSupplier, (decimal)Model.Amount);
                    ///////////////////////////////////////////////////////////////////////////
                }
                /////////////////////////////////////////////////////////////////////////
                UnitOfWork.Commit();
                obj.Data = modelSaved.ID;
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }


        [Route("api/DiscountNotice/EditDiscountNotice")]
        [HttpPost]
        public IActionResult EditDiscountNotice(DiscountNoticeVM Model)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                DiscountNoticeVM Oldmodel = UnitOfWork.DiscountNoticeService.Get(Model.ID);
                UnitOfWork.DiscountNoticeService.Edit(Model);
                if (Model.UserTypeFlag == 1)
                {
                    if (Model.ClientId != Oldmodel.ClientId)
                    {
                        int Id = UnitOfWork.TransClientService.RecalculateClientBalance((int)Oldmodel.ClientId, (int)Model.SerialNumber);
                        UnitOfWork.TransClientService.Delete(Id);
                    }
                    ///////////////////////////////////////////////////trans///////////////////////////////
                    var currentBalance = UnitOfWork.TransClientService.GetLatestBalance((int)Model.ClientId, Model.Date.ToLocalTime());
                    Model.Amount = Model.Amount == null ? 0 : Model.Amount;
                    decimal NewBalance = currentBalance - (decimal)Model.Amount;
                    TransClientVM TransClient = new TransClientVM
                    {
                        ClientId = (int)Model.ClientId,
                        ClientType = TransClientTypeEnum.تسوية_إشعار_خصم,
                        Credit = (decimal)Model.Amount,
                        CreateDate = DateTime.Now,
                        OpenBalFlag = false,
                        Debit = 0,
                        Date = Model.Date,
                        SerialNumber = Model.SerialNumber.Value,
                        IsDeleted = false,
                        Balance = NewBalance,
                    };
                    bool checkreturn = UnitOfWork.TransClientService.Add(TransClient, (decimal)Model.Amount);
                    UnitOfWork.Commit();
                    //recacaluate
                    if (!checkreturn)
                    {
                        UnitOfWork.TransClientService.RecalculateClientBalance((int)Model.ClientId, Model.SerialNumber.Value);
                    }
                }
                if (Model.UserTypeFlag == 2)
                {
                    if (Model.SupplierId != Oldmodel.SupplierId)
                    {
                        int Id = UnitOfWork.TransSupplierService.RecalculateSupplierBalance((int)Oldmodel.SupplierId, (int)Model.SerialNumber);
                        UnitOfWork.TransSupplierService.Delete(Id);
                    }
                    ///////////////////////////////////////////////////trans///////////////////////////////
                    var currentBalance = UnitOfWork.TransSupplierService.GetLatestBalance((int)Model.SupplierId, Model.Date.ToLocalTime());
                    Model.Amount = Model.Amount == null ? 0 : Model.Amount;
                    decimal NewBalance = currentBalance - (decimal)Model.Amount;
                    TransSupplierVM TransSupplier = new TransSupplierVM
                    {
                        SupplierId = (int)Model.SupplierId,
                        SupplierType = TransSupplierTypeEnum.إشعار_إضافة_موردين,
                        Credit = (decimal)Model.Amount,
                        CreateDate = DateTime.Now,
                        OpenBalFlag = false,
                        Debit = 0,
                        Date = Model.Date,
                        SerialNumber = Model.SerialNumber.Value,
                        IsDeleted = false,
                        Balance = NewBalance,
                    };
                    bool checkreturn = UnitOfWork.TransSupplierService.Add(TransSupplier, (decimal)Model.Amount);
                    UnitOfWork.Commit();
                    //recacaluate
                    if (!checkreturn)
                    {
                        UnitOfWork.TransSupplierService.RecalculateSupplierBalance((int)Model.SupplierId, Model.SerialNumber.Value);

                    }
                }
                ///////////////////////////////////////////////////////////////////////////
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }
        [Route("api/DiscountNotice/DeleteDiscountNotice")]
        [HttpGet]
        public IActionResult DeleteDiscountNotice(int ID, int SerialNumber, int ClientId)
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                UnitOfWork.DiscountNoticeService.Delete(ID);
                DiscountNoticeVM Oldmodel = UnitOfWork.DiscountNoticeService.Get(ID);
                if (Oldmodel.UserTypeFlag == 1)
                {
                    if (Oldmodel.ClientId != 0)
                    {
                        int Id = UnitOfWork.TransClientService.RecalculateClientBalance((int)Oldmodel.ClientId, (int)Oldmodel.SerialNumber);
                        UnitOfWork.TransClientService.Delete(Id);
                    }
                }
                if (Oldmodel.UserTypeFlag == 2)
                {
                    if (Oldmodel.SupplierId != 0)
                    {
                        int Id = UnitOfWork.TransSupplierService.RecalculateSupplierBalance((int)Oldmodel.SupplierId, (int)Oldmodel.SerialNumber);
                        UnitOfWork.TransSupplierService.Delete(Id);
                    }
                }
                UnitOfWork.Commit();
                obj.Data = "Done";
                return Ok(obj);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return BadRequest();
            }
        }


        [Route("api/DiscountNotice/GenerateSerial")]
        [HttpGet]
        public ActionResult GenerateSerial()
        {
            var obj = new ResponseVM();
            try
            {
                obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                obj.Message = ApiStatus.Success;
                obj.Data = UnitOfWork.DiscountNoticeService.GenerateSerial();
                return Ok(obj);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}