﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Login;
using MasProject.Domain.ViewModel;
using MasProject.Server.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;
using MasProject.Domain.Resources;

namespace MasProject.Server.Controllers.API
{
  
    [ApiController]
    public class AuthController : BaseController
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private IConfiguration _config;
        public AuthController(DBContext context, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IConfiguration config) : base(context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _config = config;
        }

        [Authorize]
        [Route("api/Auth/Logout")]
        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            var obj = new ResponseVM(); 
            obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
            obj.Message = ApiStatus.Success;
            await _signInManager.SignOutAsync();
            obj.Data = "Done";
            return Ok(obj);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("api/Auth/Login")]
        public async Task<IActionResult> Login(LoginVM model)
        {
            var obj = new ResponseVM();
          
            var user = await _userManager.FindByIdAsync(model.UserName);
            if (user != null)
            {
                var result = await _signInManager.PasswordSignInAsync(user, model.PasswordHash, model.RememberMe, false);
                if (result.Succeeded)
                {
                    obj.StatusCode = int.Parse(ApiStatus.SuccessCode);
                    obj.Message = ApiStatus.Success;
                    var tokenHandler = new JwtSecurityTokenHandler();
                    var key = Encoding.ASCII.GetBytes("MySuberSecureKey");
                    var tokenDescriptor = new SecurityTokenDescriptor
                    {
                        Subject = new ClaimsIdentity(new Claim[]
                        {
                        new Claim(ClaimTypes.Name, user.Id.ToString())
                        }),
                        Expires = DateTime.UtcNow.AddDays(7),
                        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                    };
                    var token = tokenHandler.CreateToken(tokenDescriptor);
                    var token_val = new JwtSecurityTokenHandler().WriteToken(token);
                    if ((await _userManager.IsInRoleAsync(user, "Administrator")))
                    {
                        obj.Data = "Done";
                        return Ok(obj);
                    }
                    else if ((await _userManager.IsInRoleAsync(user, "UserSystem")))
                    {
                        obj.Data = "Done";
                        return Ok(obj);
                    }
                }
            }
            obj.Data = "Done";
            return Ok(obj);
          
        }


    }
}
