﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Linq;
using MasProject.Data.DataAccess;
using System;
using System.Net.Http;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Microsoft.Extensions.FileProviders;
using System.IO;
using Microsoft.AspNetCore.Http;
using GemBox.Spreadsheet;
using Microsoft.AspNetCore.Identity;
using MasProject.Data.Models.Login;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using MasProject.Data.Models;
using Microsoft.AspNetCore.Components.Authorization;
using System.IdentityModel.Tokens.Jwt;

namespace MasProject.Server
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup()
        {
            Configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<DBContext>(options => options.UseSqlServer(connectionString));
            services.AddSingleton(Configuration.GetSection("ConnectionStrings"));
            services.AddSingleton<HttpClient>();
            //services.AddScoped<IAccountService, AccountService>();
            services.AddAutoMapper(typeof(Startup));
            services.AddControllersWithViews();
            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddMvc(v => v.EnableEndpointRouting = false).AddNewtonsoftJson();
            services.AddControllersWithViews();
            services.AddHttpClient();
            SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY");


            //services.AddScoped<CustomAuthenticationStateProvider>();
            //services.AddScoped<AuthenticationStateProvider>(s => s.GetRequiredService<CustomAuthenticationStateProvider>());
            #region JWT Configurations 
            services.AddDefaultIdentity<ApplicationUser>(options =>
           { 
               options.User.AllowedUserNameCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+ اأبتثجحخدذرزسشصضطظعغفقكلمنهويئلآ";
               options.Password.RequiredLength = 6;
               options.Password.RequireLowercase = false;
               options.Password.RequireUppercase = false;
               options.Password.RequireNonAlphanumeric = false;
               options.Password.RequireDigit = false;
               options.Password.RequiredUniqueChars = 0;
               })
              .AddRoles<IdentityRole>()
              .AddEntityFrameworkStores<DBContext>();
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Remove("role");
            services.AddAuthenticationCore();
            services.AddAuthorizationCore();
            //services.AddIdentity<ApplicationUser, IdentityRole>(options =>
            //{
            //    options.User.AllowedUserNameCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+ اأبتثجحخدذرزسشصضطظعغفقكلمنهويئلآ";
            //    options.Password.RequiredLength = 6;
            //    options.Password.RequireLowercase = false;
            //    options.Password.RequireUppercase = false;
            //    options.Password.RequireNonAlphanumeric = false;
            //    options.Password.RequireDigit = false;
            //    options.Password.RequiredUniqueChars = 0;

            //}).AddEntityFrameworkStores<DBContext>().AddDefaultTokenProviders();

            //var key = Encoding.UTF8.GetBytes(Configuration["ApplicationSettings:JWT_Secret"].ToString());
            //services.AddAuthentication(x =>
            //{
            //    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            //    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            //    x.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            //}).AddJwtBearer(x =>
            //    {
            //        x.RequireHttpsMetadata = false;
            //        x.SaveToken = false;
            //        x.TokenValidationParameters = new TokenValidationParameters
            //        {
            //            ValidateIssuer = true,
            //            ValidateAudience = true,
            //            ValidateLifetime = true,
            //            ValidateIssuerSigningKey = true,
            //            IssuerSigningKey = new SymmetricSecurityKey(key),
            //            //ValidIssuer = Configuration["ApplicationSettings:Issuer"].ToString(),
            //            //ValidAudience = Configuration["ApplicationSettings:Audience"].ToString(),
            //            //ClockSkew = TimeSpan.Zero,
            //        };
            //    });
            #endregion
            //services.AddScoped<AuthenticationStateProvider, TestAuthStateProvider>();
            //  services.AddScoped<TestAuthStateProvider>();
            //services.AddScoped<AuthenticationStateProvider>(s => s.GetRequiredService<TestAuthStateProvider>());
            //services.AddAuthenticationCore();
            //services.AddAuthorizationCore(options => {
            //    options.AddPolicy("mypolicy", policy => {
            //        policy.RequireRole("admin", "Administrator");
            //    });
            //});
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, UserManager<ApplicationUser> userManager, 
            RoleManager<IdentityRole> roleManager, DBContext context)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseWebAssemblyDebugging();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseBlazorFrameworkFiles();
            app.UseStaticFiles();
            app.UseStaticFiles(new StaticFileOptions()
            {
                RequestPath = new PathString(Configuration.GetConnectionString("RootPath"))
            });
            UpdateDatabase(app);
            MyIdentityDataInitializer.SeedData(userManager, roleManager, context);
            SeedDatabase.Intialize(app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope().ServiceProvider);
            //app.UseCors(options => options.AllowAnyOrigin());
            //app.UseCors(options => options.WithOrigins("https://localhost:44342"));
           
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
                endpoints.MapControllers();
                endpoints.MapFallbackToFile("index.html");
            });
           // stateProvider.GetAuthenticationStateAsync();
           
        }
    
        private static void UpdateDatabase(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                using (var context = serviceScope.ServiceProvider.GetService<DBContext>())
                {
                    try
                    {
                        context.Database.Migrate();
                    }
                    catch (Exception ex)
                    {
                        string message = ex.Message;
                    }
                }
            }
        }
    }
}
