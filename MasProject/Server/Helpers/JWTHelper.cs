﻿using MasProject.Data.Models.Login;
using MasProject.Domain.ViewModel;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace MasProject.Server.Helpers
{
    public class JWTHelper
    {
        public IConfiguration Configuration { get; }
        private UserManager<ApplicationUser> UserManager;
        public JWTHelper(IConfiguration configuration, UserManager<ApplicationUser> userManager)
        {
            Configuration = configuration;
            UserManager = userManager;
        }



        public string GenerateJSONWebToken(LoginVM userInfo)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["ApplicationSettings:JWT_Secret"].ToString()));

            var signCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(
                issuer: Configuration["ApplicationSettings:Issuer"].ToString(),
                audience: Configuration["ApplicationSettings:Audience"].ToString(),
                claims: new List<Claim>(),
                expires: DateTime.Now.AddMinutes(30),
                signingCredentials: signCredentials
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

    }
}
