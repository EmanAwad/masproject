﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components.Authorization;
using System.Security.Claims;

namespace MasProject.Domain.AuthProviders
{
    public class TestAuthStateProvider : AuthenticationStateProvider
    {
        //string UserId;
        //string Password;
        //bool IsAuthenticated = false;

        //public void LoadUser(string _UserId, string _Password)
        //{
        //    UserId = _UserId;
        //    Password = _Password;
        //}

        //public async Task LoadUserData()
        //{
        //    var securityService = new SharedServiceLogic.Security();
        //    try
        //    {
        //        var passwordCheck = await securityService.ValidatePassword(UserId, Password);
        //        IsAuthenticated = passwordCheck == true ? true : false;
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex);
        //    }
        //}
        public async override Task<AuthenticationState> GetAuthenticationStateAsync()
        {
            ////var userService = new UserService();           
            //await Task.Delay(1500);
            //var claims = new List<Claim>
            //{
            //    new Claim(ClaimTypes.Name, "Ehab Samir"),
            //    new Claim(ClaimTypes.Role, "Administrator")
            //};
            //var identity = IsAuthenticated
            //    ? new ClaimsIdentity(claims)
            //  : new ClaimsIdentity();
            //var result = new AuthenticationState(new ClaimsPrincipal(identity));
            //return result;
            await Task.Delay(1500);
            var identity = new ClaimsIdentity(new[]{
            
                new Claim(ClaimTypes.Name, "Ehab Samir"),
                new Claim(ClaimTypes.Role, "Administrator")
            },"Fake Auth");
            var anonymous = new ClaimsPrincipal(identity);
            return await Task.FromResult(new AuthenticationState(anonymous));
        }
    }
}
