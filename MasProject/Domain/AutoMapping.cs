﻿using AutoMapper;
using MasProject.Data.Models.Persons;
using MasProject.Domain.ViewModel.Persons;
using System.Collections.Generic;
namespace MasProject.Domain
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            #region Employee
            CreateMap<Employee, EmployeeVM>();
            CreateMap<EmployeeVM, Employee>();
            CreateMap<List<Employee>, List<EmployeeVM>>();
            CreateMap<List<EmployeeVM>, List<Employee>>();
            #endregion
        }
    }

}
