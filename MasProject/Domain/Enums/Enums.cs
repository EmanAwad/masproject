﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MasProject.Domain.Enums
{
    public class Enums
    {
       public enum EnPriceTypes
        {
            التكلفة=7,
            الموزع=3,
            الجملة=2,
            البيع=4,
            التنفيذ=5,
            العرض=6,
            التجزئة = 1,
            أخرى =8,
        }
       public enum TransClientTypeEnum
        {
            [Display(Name = "رصيد اول المدة")]
            رصيد_اول_المدة = 1,
            [Display(Name = "فاتورة مبيعات")]
            مبيعات = 2,
            [Display(Name = "فاتورة تنفيذ")]
            تنفيذ = 3,
            [Display(Name = "فاتورة صيانة")]
            صيانة = 4,
            [Display(Name = "مردودات مبيعات")]
            مردودات_مبيعات = 5,
            [Display(Name = "دفعات عملاء")]
            دفعات_عملاء = 6,
            [Display(Name = "(تسوية (إشعار خصم")]
            تسوية_إشعار_خصم = 7,
            [Display(Name = "إشعار إضافة عملاء")]
            إشعار_إضافة_عملاء = 8,
            [Display(Name = "مردودات عملاء")]
            مردودات_عملاء = 9,
        }
        public enum StoreTypeEnum
        {
            [Display(Name = "مبيعات")]
            مبيعات = 1,
            [Display(Name = "مردودات مبيعات")]
            مردودات_مبيعات = 2,
            [Display(Name = "مشتريات")]
            مشتريات = 3,
            [Display(Name = " مردودات مشتريات")]
            مردودات_مشتريات = 4,
        }
        public enum TransSupplierTypeEnum
        {
            [Display(Name = "رصيد اول المدة")]
            رصيد_اول_المدة = 1,
            [Display(Name = "فاتورة مشتريات")]
            مشتريات= 2,
            [Display(Name = "مردودات مشتريات")]
            مردودات_مشتريات = 3,
            [Display(Name = "دفعات موردين")]
            دفعات_موردين = 4,
            [Display(Name = "إشعار إضافة موردين")]
            إشعار_إضافة_موردين = 5,
            [Display(Name ="إشعار خصم ")]
            إشعار_خصم = 6,
        }
        public enum TransEmployeeTypeEnum
        {
            [Display(Name = "رصيد اول المدة")]
            رصيد_اول_المدة = 1,
            [Display(Name = "سلف الموظفين")]
            سلف_الموظف = 2,
            [Display(Name = "رد سلف الموظفين")]
            رد_سلف_الموظف = 3,
        }
        public enum TransItemTypeEnum
        {
            [Display(Name = "رصيد اول المدة")]
            رصيد_اول_المدة = 1,
            [Display(Name = "سند إضافة")]
            سند_إضافة = 2,
            [Display(Name = "سند صرف")]
            سند_صرف = 3,
        }
        //public enum EnEntryTypes
        //{
        //    [Display(Name = "  فاتورة بيع")]
        //    فاتورة_بيع = 1,
        //    [Display(Name = " فاتورة صيانة")]
        //    فاتورة_صيانة = 2,
        //    [Display(Name = " فاتورة تنفيذ ")]
        //    فاتورة_تنفيذ = 3,
        //    [Display(Name = " فاتورة مردودات مبيعات ")]
        //    فاتورة_مردودات_مبيعات = 4,
        //    [Display(Name = " فاتورة  مشتريات ")]
        //    فاتورة_مشتريات = 5,
        //    [Display(Name = " فاتورة مردودات مشتريات ")]
        //    فاتورة_مردودات_مشتريات = 6,
        //    [Display(Name = "   مسحوبات التنفيذ ")]
        //    مسحوبات_تنفيذ = 7,
        //    [Display(Name = "    سند إضافة مخزن ")]
        //      سند_صرف_مخزن= 8,
        //    [Display(Name = "    تحويل من صنف ل صنف ")]
        //   تحويل_من_صنف_ل_صنف = 9,
        //}

    }
}
