﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MasProject.Domain.ViewModel
{
    public class BaseEntity
    {
        public int ID { get; set; }
        [Required(ErrorMessage = "الاسم مطلوب")]
        [StringLength(50, ErrorMessage = "الاسم طويل جدا")]
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string Note { get; set; }
    }


    public class BillsTbl<T>
    {
        public List<T> BillsList { get; set; }
        public T SearchItems { get; set; }
    }
}
