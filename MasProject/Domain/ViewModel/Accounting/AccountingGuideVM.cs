﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MasProject.Domain.ViewModel.Accounting
{
    public class AccountingGuideVM : AccountingEntity
    {
        public int Code { get; set; }
        public int AccountCode { get; set; }
        public string ViewAccountCode { get; set; }
        public int? ParentId { get; set; }
        public string ParentName { get; set; }
        public int? ParentCode{ get; set; }
        public bool FlagType { get; set; } // true Income false budget

        public bool CostCenter { get; set; }
        public List<AccountingGuideVM> AccountingGuideList = new List<AccountingGuideVM>();
    }
}
