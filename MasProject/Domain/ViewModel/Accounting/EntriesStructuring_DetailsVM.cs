﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MasProject.Domain.ViewModel.Accounting
{
   public class EntriesStructuring_DetailsVM : AccountingEntity
    {
        public int? AccountNo { get; set; }

        public string AccountName { get; set; }

        public decimal? Debit { get; set; }

        public decimal? Credit { get; set; }

        //مركز التكلفة
        public int? CostCenterId { get; set; }

        public string CostCenterName { get; set; }

        //public virtual Branch BranchObj { get; set; }
        // public List<BranchVM> BranchList { get; set; }
        public decimal? TotalDR { get; set; }

        public decimal? TotalCR { get; set; }

        public decimal? DRCR_Difference { get; set; }

        public int? EntriesStructuringId { get; set; }
    }
}
