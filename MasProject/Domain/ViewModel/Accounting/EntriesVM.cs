﻿using MasProject.Domain.ViewModel.Hierarchy;
using System;
using System.Collections.Generic;

namespace MasProject.Domain.ViewModel.Accounting
{
   public class EntriesVM : AccountingEntity
    {
        public int EntriesNo { get; set; }

        public DateTime EntryDate { get; set; }

        public string EntryType { get; set; }
        public string EntryDesc_Note { get; set; }
        public List<EntriesDetailsVM> EntriesDetailsList { get; set; } = new List<EntriesDetailsVM>();
        public List<AccountingGuideVM> GuidesList { get; set; } = new List<AccountingGuideVM>();
        public List<AccountingGuideVM> CostsCentersList { get; set; } = new List<AccountingGuideVM>();
        public List<EntriesTypeVM> EntriesTypeList { get; set; } = new List<EntriesTypeVM>();
    }
}
