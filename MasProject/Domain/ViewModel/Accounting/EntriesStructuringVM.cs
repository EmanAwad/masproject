﻿using System;
using System.Collections.Generic;
using MasProject.Domain.ViewModel.Hierarchy;

namespace MasProject.Domain.ViewModel.Accounting
{
   public class EntriesStructuringVM : AccountingEntity
    {
        public int EntriesStructureNo { get; set; }

        public DateTime EntryDate { get; set; }

        public string EntryType { get; set; }

        public List<EntriesStructuring_DetailsVM> EntriesStructuringList { get; set; } = new List<EntriesStructuring_DetailsVM>();
        public List<AccountingGuideVM> GuidesList { get; set; } = new List<AccountingGuideVM>();
        public List<AccountingGuideVM> CostsCentersList { get; set; } = new List<AccountingGuideVM>();
        public List<EntriesTypeVM> EntriesTypeList { get; set; } = new List<EntriesTypeVM>();
    }
}
