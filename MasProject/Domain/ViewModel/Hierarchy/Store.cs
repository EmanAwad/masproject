﻿using MasProject.Data.Models.Persons;
using MasProject.Domain.ViewModel.Persons;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MasProject.Domain.ViewModel.Hierarchy
{
    public class StoreVM : BaseEntity
    {
        public List<BranchVM> BranchList { get; set; }
        //[Required(ErrorMessage = "اختيار الفرع مطلوب")]
        public int? BranchID { get; set; }
        public string BranchName { get; set; }
        //[Required(ErrorMessage = "العنوان مطلوب")]
        public string Address { get; set; }
        public int CountryID { get; set; }
        public int CityID { get; set; }
        public int AreaID { get; set; }
        //[Required(ErrorMessage = "التليفون مطلوب")]
        //[MinLength(10, ErrorMessage = "الموبيل طويل جدا")]
        //[MaxLength(10, ErrorMessage = "الموبيل طويل جدا")]
        //[DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }
        //[Required(ErrorMessage = "الموبيل مطلوب")]
        //[MinLength(11, ErrorMessage = "الموبيل طويل جدا")]
        //[MaxLength(11, ErrorMessage = "الموبيل طويل جدا")]
        //[DataType(DataType.PhoneNumber)]
        public string Mobile { get; set; }
        //[Required(ErrorMessage = " رقم امين المخزن مطلوب")]
    
        public List<EmployeeVM> EmployeeList { get; set; }

        public int? EmployeeID { get; set; }
        public string EmployeeName { get; set; }

        public int? arrangement { get; set; }
        public bool MainStore { get; set; }
    }
}
