﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MasProject.Domain.ViewModel.Hierarchy
{
    public class BranchVM : BaseEntity
    {
        //[Required(ErrorMessage = "العنوان مطلوب")]
        public string Address { get; set; }
        public int CountryID { get; set; }
        public int CityID { get; set; }
        public int AreaID { get; set; }
        public int? CompanyId { get; set; }
        public string CompanyName { get; set; }
        //[Required(ErrorMessage = "رقم الفرع مطلوب")]
        public string BranchKey { get; set; }
        public List<CompanyVM> CompanyList { get; set; }
        public int? arrangement { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
    }
}
