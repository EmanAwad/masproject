﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.Hierarchy
{
    public class OtherConstantVM : BaseEntity
    {
        public double? Ratio { get; set; }
        public int? arrangement { get; set; }

    }
}
