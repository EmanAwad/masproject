﻿using BlazorInputFile;
using System;
using System.ComponentModel.DataAnnotations;

namespace MasProject.Domain.ViewModel.Hierarchy
{
    public class CompanyVM : BaseEntity
    {
        //[Required(ErrorMessage = "العنوان مطلوب")]
        public string Address { get; set; }
        public int CountryID { get; set; }
        public int CityID { get; set; }
        public int AreaID { get; set; }
        public string Logo { get; set; }
        //[Required(ErrorMessage = "التليفون مطلوب")]
        //[MinLength(10, ErrorMessage = "التليفون 10 أرقام")]
        //[MaxLength(10, ErrorMessage = "التليفون 10 أرقام")]
        //[DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }
        //[Required(ErrorMessage = "الموبيل مطلوب")]
        //[MinLength(11, ErrorMessage = " يجب ادخال 11 رقم")]
        //[MaxLength(11, ErrorMessage = "يجب ادخال 11 رقم")]

        [DataType(DataType.PhoneNumber)]
        public string Mobile { get; set; }
        //[Required(ErrorMessage = "الموقع مطلوب")]
        //[DataType(DataType.Url)]
        public string Website { get; set; }
        //[Required(ErrorMessage = "الرقم الضريبى مطلوب")]
        //[Range(1, 14, ErrorMessage = "(المدى المسموح بيه (1-12")]
        //[MinLength(14, ErrorMessage = "الرقم الضريبي 12 رقم")]
        //[MaxLength(14, ErrorMessage = "الرقم الضريبي 12 رقم")]
      //  [MinLength(12, ErrorMessage = " يجب ادخال 12 رقم")]
      //  [MaxLength(12, ErrorMessage = "يجب ادخال 12 رقم")]
       // [DataType(DataType.Text)]
        public int? TaxNumber { get; set; }
        //[Required(ErrorMessage = "ألسجل التجارى مطلوب")]
        //[Range(1, 9, ErrorMessage = "(المدى المسموح بيه (1-9")]
        //[MinLength(9, ErrorMessage = "السجل التجاري 9 أرقام")]
        //[MaxLength(9, ErrorMessage = "السجل التجاري 9 أرقام")]

        //[MinLength(9, ErrorMessage = "يجب ادخال 9 أرقام ")]
        //[MaxLength(9, ErrorMessage = "يجب ادخال 9 أرقام")]
       // [DataType(DataType.Text)]
        public int? CommerceNumber { get; set; }
        public IFileListEntry LogoSource { get; set; }
        public int? arrangement { get; set; }
    }
}
