﻿using System.ComponentModel.DataAnnotations;

namespace MasProject.Domain.ViewModel.Hierarchy
{
    public class CurrencyVM : BaseEntity
    {
        //[Required(ErrorMessage = "سعر الصرف مطلوب")]
        //[Range(1, 100000, ErrorMessage = "المدى المسموح بيه (1-100000).")]
        public double? Ratio { get; set; }
        public int? arrangement { get; set; }

    }
}
