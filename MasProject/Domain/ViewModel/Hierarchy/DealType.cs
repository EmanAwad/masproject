﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.Hierarchy
{
   public class DealTypeVM :BaseEntity
    {
        public int PriceType { get; set; }
        public List<LookupKeyValueVM> PriceTypeList = new List<LookupKeyValueVM>();
        public int? arrangement { get; set; }

    }
}
