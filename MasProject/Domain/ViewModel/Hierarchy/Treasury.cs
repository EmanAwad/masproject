﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MasProject.Domain.ViewModel.Persons;
namespace MasProject.Domain.ViewModel.Hierarchy
{
    public class TreasuryVM : BaseEntity
    {

        //public string ResponsableName { get; set; }
        public List<EmployeeVM> UsersList { get; set; }

        //[Required(ErrorMessage = "المسئول مطلوب")]
        public int? UserId { get; set; }
        public string UserName { get; set; }


        public List<BranchVM> BranchList { get; set; }
        public int? BranchID { get; set; }
        public string BranchName { get; set; }
        public string Mobile { get; set; }

        public int? arrangement { get; set; }

        public string Code { get; set; }
    }
}
