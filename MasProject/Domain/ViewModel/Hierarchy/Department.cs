﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MasProject.Domain.ViewModel.Hierarchy
{
    public class DepartmentVM : BaseEntity
    {
        public List<BranchVM> BranchList { get; set; }
        //[Required(ErrorMessage = "اختيار الفرع مطلوب")]
        public int? BranchID { get; set; }
        public string BranchName { get; set; }
        public int? arrangement { get; set; }
    }
}
