﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MasProject.Domain.ViewModel.Persons;
namespace MasProject.Domain.ViewModel.Hierarchy
{
    public class TreasuryOpenBalanceVM
    {
        public int ID { get; set; }
        public DateTime EntryDate { get; set; }
        public List<TreasuryVM> TreasuryList { get; set; }
        public int? TreasuryID { get; set; }
        public string TreasuryName { get; set; }
        //[Required(ErrorMessage = "المبلغ مطلوب")]
        //[Range(1, 10000000, ErrorMessage = "المدى المسموح بيه (1-10000000).")]
        public decimal? Amount { get; set; }
        public List<CurrencyVM> CurrencyList { get; set; }
        public int? CurrencyID { get; set; }
        public string CurrencyName { get; set; }
      //  [Required(ErrorMessage = "النسبة مطلوب")]
        //[Range(0, 100000, ErrorMessage = "المدى المسموح بيه (0-100000).")]
        public decimal? CurrRatio { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        
        public string Note { get; set; }

        public List<EmployeeVM> UsersList { get; set; }

        //[Required(ErrorMessage = "المسئول مطلوب")]
        public int? UserId { get; set; }
        public string UserName { get; set; }

    }
}
