﻿using System;

namespace MasProject.Domain.ViewModel
{
    public interface IEntity
    {
        public int ID { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }

        public DateTime ModifiedDate { get; set; }
    }
}
