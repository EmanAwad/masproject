﻿using System;
using System.Collections.Generic;
using System.Text;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Persons;
namespace MasProject.Domain.ViewModel.FundTransaction.ClientPayments
{
   public class ClientPaymentVM
    {
        public int ID { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string Name { get; set; } = "";
        public string Note { get; set; }
        public int? SerialNumber { get; set; }
        public int? BillSerialNumber { get; set; }

        public DateTime Date { get; set; }

        public double? Amount { get; set; }
        public double? Total { get; set; }
        public bool IsCheck { get; set; }
        public DateTime? DueDate { get; set; }
        public int? ClientId { get; set; }
        public string ClientName { get; set; }
        public int? TreasuryId { get; set; }
        public string TreasuryName { get; set; }
        public int? CurrencyId { get; set; }
        public string CurrencyName { get; set; }
        public double Ratio { get; set; }
        public List<CurrencyVM> CurrencyList { get; set; } = new List<CurrencyVM>();
        public List<TreasuryVM> TreasuryList { get; set; } = new List<TreasuryVM>();
        public List<ClientsVM> ClientList { get; set; } = new List<ClientsVM>();

        public List<LookupKeyValueVM> ClientsList { get; set; }

       
        public bool IsCollected { get; set; }

        public DateTime? CollectionDate { get; set; }

        public string Bank { get; set; }
    }
}
