﻿using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Persons;
using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.FundTransaction
{
 public    class EmployeesLoansPaymentVM
    {
        public int ID { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string Name { get; set; } = "";

        public string Note { get; set; }

        public int? SerialNumber { get; set; }
        public DateTime Date { get; set; }

        public int? TreasuryId { get; set; }
        public string TreasuryName { get; set; }

        public int DocumentNo { get; set; }

        public int? EmployeeId { get; set; }
        public string EmployeeName { get; set; }

        public double? Amount { get; set; }
        public List<TreasuryVM> TreasuryList { get; set; } = new List<TreasuryVM>();
        public List<EmployeeVM> EmployeeList { get; set; } = new List<EmployeeVM>();
    }
}
