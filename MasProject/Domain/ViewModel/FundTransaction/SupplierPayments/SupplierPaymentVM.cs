﻿using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Persons;
using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.FundTransaction.SupplierPayments
{
   public class SupplierPaymentVM
    {
        public int ID { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string Name { get; set; } = "";
        public string Note { get; set; }
        public int? SerialNumber { get; set; }
        public DateTime Date { get; set; }

        public double? Amount { get; set; }
        public double? Total { get; set; }
        public bool IsCheck { get; set; }
        public DateTime? DueDate { get; set; }
        public int? SupplierId { get; set; }
        public string SupplierName { get; set; }
        public int? TreasuryId { get; set; }
        public string TreasuryName { get; set; }
        public int? CurrencyId { get; set; }
        public string CurrencyName { get; set; }
        public double Ratio { get; set; }
        public List<CurrencyVM> CurrencyList { get; set; } = new List<CurrencyVM>();
        public List<TreasuryVM> TreasuryList { get; set; } = new List<TreasuryVM>();
        public List<SupplierVM> SupplierList { get; set; } = new List<SupplierVM>();

        public bool IsCollected { get; set; }

        public DateTime? CollectionDate { get; set; }

        public string Bank { get; set; }
    }
}
