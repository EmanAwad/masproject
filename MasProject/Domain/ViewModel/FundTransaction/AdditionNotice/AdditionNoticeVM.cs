﻿using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Persons;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MasProject.Domain.ViewModel.FundTransaction.AdditionNotice
{
   public class AdditionNoticeVM
    {
        public int ID { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string Name { get; set; } = "";
        public string Note { get; set; }
        public int? SerialNumber { get; set; }
        public DateTime Date { get; set; }
        public double? Amount { get; set; }
        public int? SupplierId { get; set; }
        public string SupplierName { get; set; }
        public int? CurrencyId { get; set; }
        public string CurrencyName { get; set; }
        public List<CurrencyVM> CurrencyList { get; set; } = new List<CurrencyVM>();
        public List<SupplierVM> SupplierList { get; set; } = new List<SupplierVM>();

        //[Required(ErrorMessage ="يجب الاختيار بين عميل/مورد")]
        public int UserTypeFlag { get; set; }

        public int? ClientId { get; set; }
        public string ClientName { get; set; }

        public List<ClientsVM> ClientList { get; set; } = new List<ClientsVM>();
    }
}
