﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel
{
   public  class LookupItem
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string ItemImg { get; set; }
        public string Code { get; set; }
        public string BoxCode { get; set; }
        public string InterationalParCode { get; set; }
        public string SupplierParCode { get; set; }
        public string ParCodeNote { get; set; }
        public string GuaranteeParCode { get; set; }
        public float CostPrice { get; set; }
        public float SupplierPrice { get; set; }
        public float WholesalePrice { get; set; }
        public float SellingPrice { get; set; }
        public float ExecutionPrice { get; set; }
        public float ShowPrice { get; set; }
        public float other_Price { get; set; }
        public double? BranchToQuantity { get; set; }
        public double? MainStoreQuantity { get; set; }
        public double? SelectedBranchQuantity { get; set; }
    }
}
