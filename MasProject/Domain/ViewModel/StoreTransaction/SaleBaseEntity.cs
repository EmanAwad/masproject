﻿using MasProject.Data.Models.Hierarchy;
using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.StoreTransaction
{
    public class SaleBaseEntity
    {
        public int ID { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string Note { get; set; }

        //columns for This Modules
        public int? SerialNumber { get; set; }
        public DateTime Date { get; set; }
        public int? StoreId { get; set; }
        public virtual Store Store { get; set; }
        public int? DealTypeId { get; set; }
        public virtual DealType DealType { get; set; }
        public bool FlagType { get; set; }
        public decimal? TotalPrice { get; set; }
        public decimal? DiscountPrecentage { get; set; }
        public decimal? DiscountValue { get; set; }
        public decimal? TotalAfterDiscount { get; set; }
        public bool ShowNotesFlag { get; set; }
        public int UserId { get; set; }
        public int RevewId { get; set; }
        public int ApproveId { get; set; }
        //new 21-10-2020
        public decimal TotalAfterTax { get; set; }
        public decimal SourceDeduction { get; set; }
        public decimal SourceDeductionAmount { get; set; }
        public decimal AddtionTax { get; set; }
        public decimal AddtionTaxAmount { get; set; }
        public int? BranchId { get; set; }
        public virtual Branch Branch { get; set; }
    }
}