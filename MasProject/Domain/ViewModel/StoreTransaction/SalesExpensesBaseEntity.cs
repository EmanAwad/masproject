﻿using MasProject.Data.Models.Items;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MasProject.Domain.ViewModel.StoreTransaction
{
    public class SalesExpensesBaseEntity : IEntity
    {
        [Key, Column(Order = 0)]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string Name { get; set; } 
        //Columns For This Modules
        public int? ExpensesId { get; set; }
        [ForeignKey("ExpensesId")]
        public SellingExpenses Expenses { get; set; }
        public decimal? Amount { get; set; }
    }
}