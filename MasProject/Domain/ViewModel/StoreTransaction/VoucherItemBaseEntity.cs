﻿using MasProject.Data.Models.Items;
using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.StoreTransaction
{
    public class VoucherItemBaseEntity
    {
        public int ID { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string Box_ParCode { get; set; }
        //Columns For This Modules
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public virtual Item Item { get; set; }
        public int? Quantity { get; set; }
        public string SheilfNo { get; set; }
        public string BoxCode { get; set; }
        public int? SerialNumber { get; set; }//bill foriegn key
    }
}
