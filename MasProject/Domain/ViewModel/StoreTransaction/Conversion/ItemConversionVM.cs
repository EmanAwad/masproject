﻿using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Items;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MasProject.Domain.ViewModel.StoreTransaction.Conversion
{
   public class ItemConversionVM 
    {
        public int ID { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }

        public DateTime ModifiedDate { get; set; }
        public int ConversionRecordID { get; set; }
        public DateTime Date { get; set; }
        public string ConversionReason { get; set; }

        public int? Quantity { get; set; }

        public int? StoreId { get; set; }
        public string StoreName { get; set; }
        public List<StoreVM> Store { get; set; }


        public int? ItemId { get; set; }
        public string ItemName { get; set; }
        public List<ItemVM> Item { get; set; }

        public int? EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public List<EmployeeVM> Employee { get; set; }

    }
}
