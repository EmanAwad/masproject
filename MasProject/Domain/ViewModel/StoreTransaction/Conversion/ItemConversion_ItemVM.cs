﻿using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Items;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MasProject.Domain.ViewModel.StoreTransaction.Conversion
{
   public class ItemConversion_ItemVM : BaseEntity
    {
        public int? QuantityFrom { get; set; }
        public int? QuantityTo { get; set; }
        public int? ConversionRecordID { get; set; }
        public int? ItemIdFrom { get; set; }
        public string ItemNameFrom { get; set; }
        public List<ItemVM> ItemFrom { get; set; }


        public int? ItemIdTo{ get; set; }
        public string ItemNameTo { get; set; }
        public List<ItemVM> ItemTo { get; set; }
       
        public decimal? PriceTo { get; set; }
        public decimal? PriceFrom { get; set; }
        public decimal? TotalTo { get; set; }
        public decimal? TotalFrom { get; set; }
        public int? IdentiferFrom { get; set; }
        public int? IdentiferTo { get; set; }

        public virtual ItemConversion_BillVM ConversionBill { get; set; }

        public string ParCodeFrom { get; set; }
        public string ParCodeTo { get; set; }


    }
}
