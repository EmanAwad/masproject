﻿using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Items;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MasProject.Data.Models.Persons;
using MasProject.Data.Models.Hierarchy;

namespace MasProject.Domain.ViewModel.StoreTransaction.Conversion
{
    public class ItemConversion_BillVM : BaseEntity
    {

        public int? ConversionRecordID { get; set; }
        public DateTime Date { get; set; }
        public int? BranchId { get; set; }
        public string BranchName { get; set; }
        public int? EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public List<LookupKeyValueVM> Employee { get; set; }

        public string ConversionReason { get; set; }

        public int? StoreId { get; set; }
        public string StoreName { get; set; }
        public List<StoreVM> Store { get; set; }
        public List<BranchVM> BranchList { get; set; }
        public List<LookupItem> ItemList { get; set; }
        public List<ItemConversion_ItemVM> ConversionItemsFrom { get; set; }
        public List<ItemConversion_ItemVM> ConversionItemsTo { get; set; }
        public decimal? TotalPriceFrom { get; set; }

        public decimal? TotalPriceTo { get; set; }
        public bool IsConverted { get; set; }
    }
}
