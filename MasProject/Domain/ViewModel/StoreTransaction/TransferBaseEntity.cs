﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.StoreTransaction
{
   public  class TransferBaseEntity
    {
        public int ID { get; set; }
        public int SerialNumber { get; set; }
        public DateTime Date { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string Note { get; set; }
        public int UserId { get; set; }
        public int RevewId { get; set; }
        public int ApproveId { get; set; }
    }
}
