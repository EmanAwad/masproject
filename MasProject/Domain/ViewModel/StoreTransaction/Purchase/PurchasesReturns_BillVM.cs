﻿
using MasProject.Data.Models.Persons;
using System.Collections.Generic;
using MasProject.Data.Models.Persons;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Items;

namespace MasProject.Domain.ViewModel.StoreTransaction.Purchase
{
   public class PurchasesReturns_BillVM : SaleBaseEntity
    {
        public int SupplierId { get; set; }
        public virtual Supplier Supplier { get; set; }
        public List<PurchasesReturns_ItemVM> PurchaseItem { get; set; }
        public List<PurchaseReturns_OtherIncomeVM> PurchaseReturns_OtherIncome { get; set; }
        public List<StoreVM> StoreList { get; set; }
        public List<BranchVM> BranchList { get; set; }

        public List<DealTypeVM> DealTypeList { get; set; }
        public List<LookupKeyValueVM> SupplierList { get; set; }
        public List<LookupItem> ItemList { get; set; }
        public List<OtherIncomeVM> OtherIncomeList { get; set; }
        //For Design Show Only
        public decimal? Balance { get; set; }
        public string TaxFileNumber { get; set; }
        public bool IsConverted { get; set; }
        public int convert { get; set; }
    }
}
