﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.StoreTransaction.Purchase
{
  public  class PurchasesReturns_ItemVM : SalesItemsBaseEntity
    {
        public decimal? SavedPrice { get; set; }
        public int? Identifer { get; set; }
        public virtual PurchasesReturns_BillVM PurchaseBill { get; set; }
        public string ItemImg { get; set; }
    }
}
