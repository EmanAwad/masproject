﻿using MasProject.Data.Models.Items;
using MasProject.Domain.ViewModel.StoreTransaction.Purchase;
using System;

namespace MasProject.Domain.ViewModel.StoreTransaction.Purchase
{
  public  class PurchaseOtherIncomeVM  
    {
        public int ID { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string Name { get; set; }
        //Columns For This Modules
        public int? IncomeId { get; set; }
        public decimal? Amount { get; set; }
        public int? Identifer { get; set; }
        public virtual PurchaseBillVM PurchaseBill { get; set; }
        public int? SerialNumber { get; set; }//bill foriegn key
    }
}
