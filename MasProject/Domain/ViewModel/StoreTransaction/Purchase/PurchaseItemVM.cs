﻿using MasProject.Data.Models.Persons;
using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.StoreTransaction.Purchase
{
    public class PurchaseItemVM : SalesItemsBaseEntity
    {
        public int SupplierId { get; set; }
        public virtual Supplier Supplier { get; set; }
        public decimal? SavedPrice { get; set; }
        public int? Identifer { get; set; }
        public virtual PurchaseBillVM PurchaseBill { get; set; }
        public string ItemImg { get; set; }
    }
}
