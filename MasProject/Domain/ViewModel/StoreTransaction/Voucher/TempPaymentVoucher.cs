﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.StoreTransaction.Voucher
{
   public class TempPaymentVoucherVM : VoucherBaseEntity
    {
        public int? Client_Supplier_Id { get; set; }
        public List<TempPaymentVoucherItemVM> PaymentVoucherItem { get; set; }
        public int? BillSerialNumber { get; set; }
        public string Name { get; set; }
        public string BillType { get; set; }
        public int UserType { get; set; }
    }
}
