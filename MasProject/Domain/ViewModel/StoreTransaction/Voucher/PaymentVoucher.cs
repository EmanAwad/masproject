﻿using MasProject.Domain.ViewModel.StoreTransaction;
using System;
using System.Collections.Generic;
using System.Text;
using MasProject.Domain.ViewModel.Items;
using MasProject.Domain.ViewModel.Hierarchy;
namespace MasProject.Domain.ViewModel.StoreTransaction.Voucher
{
    public class PaymentVoucherVM : VoucherBaseEntity
    {
        public int? Client_Supplier_Id { get; set; }
        public string ClientName { get; set; }
        public List<PaymentVoucherItemVM> PaymentVoucherItem { get; set; }
        public int? BillSerialNumber { get; set; }
        public string Name { get; set; }
        public string BillType { get; set; }
        public int Type { get; set; }
        public int UserType { get; set; }
        public List<StoreVM> StoreList { get; set; } = new List<StoreVM>();
        public List<LookupKeyValueVM> Client_Supplier_List { get; set; } = new List<LookupKeyValueVM>();
        public List<LookupItem> ItemList { get; set; } = new List<LookupItem>();
    }
}
