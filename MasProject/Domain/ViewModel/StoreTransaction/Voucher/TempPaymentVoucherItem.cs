﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.StoreTransaction.Voucher
{
    public class TempPaymentVoucherItemVM : VoucherItemBaseEntity
    {
        public int? Identifer { get; set; }
        public virtual TempPaymentVoucherVM PaymentVoucher { get; set; }
    }
}

