﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.StoreTransaction.Voucher
{
    public class TempAdditionVoucherItemVM : VoucherItemBaseEntity
    {
        public int? Identifer { get; set; }
        public virtual TempAdditionVoucherVM TempAdditionVoucher { get; set; }
    }
}
