﻿using MasProject.Domain.ViewModel.StoreTransaction;
using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.StoreTransaction.Voucher
{
   public class AdditionVoucherItemVM : VoucherItemBaseEntity
    {
        public int? Identifer { get; set; }
        public string ItemImg { get; set; }
        public virtual AdditionVoucherVM AdditionVoucher { get; set; }
    }
}
