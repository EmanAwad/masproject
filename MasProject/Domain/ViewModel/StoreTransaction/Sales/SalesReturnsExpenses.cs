﻿using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.StoreTransaction.Sales
{
  public  class SalesReturnsExpensesVM : SalesExpensesBaseEntity
    {
        public int? Identifer { get; set; }
        public virtual SalesReturnsVM SalesReturns { get; set; }
        public int? SerialNumber { get; set; }//bill foriegn key
    }
}
