﻿using MasProject.Data.Models.Hierarchy;
using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.StoreTransaction.Sales
{
    public class SalesBillStoresVM : IEntity
    {
        public int ID { get  ; set  ; }
        public bool IsDeleted { get  ; set  ; }
        public string CreatedBy { get  ; set  ; }
        public DateTime CreatedDate { get  ; set  ; }
        public string ModifiedBy { get  ; set  ; }
        public DateTime ModifiedDate { get  ; set  ; }
        public int? StoreId { get; set; }
        public virtual Store Store { get; set; }
        public List<SalesItemsVM> SalesItem { get; set; }
        public int? SerialNumber { get; set; }
        public int? Identifer { get; set; }
        //public int StoreIdentifier { get; set; }
    }
}
