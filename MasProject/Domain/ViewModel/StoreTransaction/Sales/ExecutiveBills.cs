﻿using MasProject.Data.Models.Persons;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Items;
using System.Collections.Generic;

namespace MasProject.Domain.ViewModel.StoreTransaction.Sales
{
  public  class ExecutiveBillsVM : SaleBaseEntity
    {
        public int ClientId { get; set; }
        public virtual Clients Client { get; set; }

        public string ClientName { get; set; }
        //public List<ExecutiveItemsVM> ExecutiveItems { get; set; }
        public List<ExecutiveSellExpensesVM> ExecutiveSellExpenses { get; set; }
        public List<ExecutiveOtherIncomeVM> ExecutiveOtherIncome { get; set; }

        //For Design Show Only
        public decimal? ClientBalance { get; set; }
        public string TaxFileNumber { get; set; }
        public int convert { get; set; }
        public bool IsConverted { get; set; }
        public bool MultiStore { get; set; }
        public List<ExecutiveBillStoresVM> BillStores { get; set; }
        public List<StoreVM> StoreList { get; set; }
        public List<DealTypeVM> DealTypeList { get; set; }
        public List<LookupKeyValueVM> ClientList { get; set; }
        public List<LookupItem> ItemList { get; set; }
        public List<SellingExpensesVM> sellingExpensesList { get; set; }
        public List<OtherIncomeVM> OtherIncomeList { get; set; }
        public List<BranchVM> BranchList { get; set; }
    }

}
