﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.StoreTransaction.Sales
{
  public  class MantienceItemsVM : SalesItemsBaseEntity
    {
        public decimal? SavedPrice { get; set; }
        public int? Identifer { get; set; }
        public virtual MantienceBillsVM MantienceBill { get; set; }
        public virtual MaintanceBillStoresVM BillStore { get; set; }
        public int? StoreId { get; set; }
        public string ItemImg { get; set; }
    }
}
