﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.StoreTransaction.Sales
{
  public  class ExecutiveSellExpensesVM : SalesExpensesBaseEntity
    {
        public int? Identifer { get; set; }
        public virtual ExecutiveBillsVM ExecutiveBill { get; set; }
        public int? SerialNumber { get; set; }//bill foriegn key
    }
}
