﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.StoreTransaction.Sales
{
  public  class ExecutiveItemsVM : SalesItemsBaseEntity
    {
        public decimal? SavedPrice { get; set; }
        public int? Identifer { get; set; }
        public virtual ExecutiveBillsVM ExecutiveBill { get; set; }
        public virtual ExecutiveBillStoresVM BillStore { get; set; }
        public int? StoreId { get; set; }
        public string ItemImg { get; set; }
    }
}
