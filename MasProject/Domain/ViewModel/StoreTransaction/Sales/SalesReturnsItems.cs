﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.StoreTransaction.Sales
{
  public  class SalesReturnsItemsVM : SalesItemsBaseEntity
    {
        public decimal? SavedPrice { get; set; }
        public int? Identifer { get; set; }
        public virtual SalesReturnsVM SalesReturns { get; set; }
        public virtual SalesReturnsBillStoresVM BillStore { get; set; }
        public int? StoreId { get; set; }
        public string ItemImg { get; set; }
    }
}
