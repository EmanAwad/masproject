﻿using MasProject.Data.Models.Persons;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Items;
using MasProject.Domain.ViewModel.StoreTransaction;
using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.StoreTransaction.Sales
{
  public  class MantienceBillsVM : SaleBaseEntity
    {
        public int ClientId { get; set; }
        public virtual Clients Client { get; set; }

        public string ClientName { get; set; }
       // public List<MantienceItemsVM> MantienceItems { get; set; }
        public List<MaintanceSellExpensesVM> MaintanceSellExpenses { get; set; }
        //For Design Show Only
        public decimal? ClientBalance { get; set; }
        public string TaxFileNumber { get; set; }
        public int convert { get; set; }
        public bool IsConverted { get; set; }
        public bool MultiStore { get; set; }
        public List<MaintanceBillStoresVM> MaintanceBillStores { get; set; }
        public List<StoreVM> StoreList { get; set; }
        public List<DealTypeVM> DealTypeList { get; set; }
        public List<LookupKeyValueVM> ClientList { get; set; }
        public List<LookupItem> ItemList { get; set; }
        public List<SellingExpensesVM> sellingExpensesList { get; set; }
        public List<BranchVM> BranchList { get; set; }

    }

}
