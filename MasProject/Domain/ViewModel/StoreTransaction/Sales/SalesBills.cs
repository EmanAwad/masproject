﻿using MasProject.Data.Models.Persons;
using System.Collections.Generic;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel.Items;
namespace MasProject.Domain.ViewModel.StoreTransaction.Sales
{
  public  class SalesBillsVM : SaleBaseEntity
    {
        public int ClientId { get; set; }
        public virtual Clients Client { get; set; }
       // public List<SalesItemsVM> SalesItem { get; set; }//eman dy l mfrood tt8er
        public List<SalesSellExpensesVM> SalesSellExpenses { get; set; }
        //For Design Show Only
        public decimal? ClientBalance { get; set; }
        public string TaxFileNumber { get; set; }
        public bool IsConverted { get; set; }
        public int convert { get; set; }
        public bool MultiStore { get; set; }
        public List<SalesBillStoresVM> SalesBillStores { get; set; }
        public List<StoreVM> StoreList { get; set; }
        public List<DealTypeVM> DealTypeList { get; set; }
        public List<LookupKeyValueVM> ClientList { get; set; }

        public string ClientName { get; set; }
        public List<LookupItem> ItemList { get; set; }
        public List<SellingExpensesVM> sellingExpensesList { get; set; }
        public List<BranchVM> BranchList { get; set; }
    }
   
}
