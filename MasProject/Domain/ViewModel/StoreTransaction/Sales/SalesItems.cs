﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.StoreTransaction.Sales
{
  public  class SalesItemsVM : SalesItemsBaseEntity
    {
        public decimal? SavedPrice { get; set; }
        public int? Identifer { get; set; }
        public virtual SalesBillsVM SalesBill { get; set; }
        public virtual SalesBillStoresVM BillStore { get; set; }
        public int? StoreId { get; set; }
        public string ItemImg { get; set; }
    }
}
