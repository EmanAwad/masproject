﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.StoreTransaction.ExecutionWithDrawals
{
   public class ExecutionWithDrawals_ItemsVM : SalesItemsBaseEntity
    {
        public decimal? SavedPrice { get; set; }
        public int? Identifer { get; set; }
        public virtual ExecutionithDrawals_BillVM ExecutionBill { get; set; }
        public virtual ExecutionWithdrawl_BillStoresVM BillStore { get; set; }
        public int? StoreId { get; set; }
        public string ItemImg { get; set; }
    }
}
