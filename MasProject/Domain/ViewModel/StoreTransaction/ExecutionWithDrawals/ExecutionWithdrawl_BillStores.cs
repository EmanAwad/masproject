﻿using MasProject.Data.Models.Hierarchy;
using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.StoreTransaction.ExecutionWithDrawals
{
    public class ExecutionWithdrawl_BillStoresVM : IEntity
    {
        public int ID { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int? StoreId { get; set; }
        public virtual Store Store { get; set; }
        public List<ExecutionWithDrawals_ItemsVM> ExItem { get; set; }
        public int? SerialNumber { get; set; }
        public int? Identifer { get; set; }
    }
}