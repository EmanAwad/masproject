﻿using System;
using System.Collections.Generic;
using System.Text;
using MasProject.Data.Models.Hierarchy;
using MasProject.Domain.ViewModel.Hierarchy;

namespace MasProject.Domain.ViewModel.StoreTransaction.ExecutionWithDrawals
{
    public class ExecutionithDrawals_BillVM : SaleBaseEntity
    {
        //public int BranchId { get; set; }
        //public virtual Branch Branch { get; set; }
        public string BranchName { get; set; }
        // public List<ExecutionWithDrawals_ItemsVM> ExecutionItems { get; set; }
        //For Design Show Only
        public decimal Balance { get; set; }
        public int? TaxFileNumber { get; set; }
        public bool IsConverted { get; set; }
        public int convert { get; set; }
        public List<ExecutionWithdrawl_BillStoresVM> BillStores { get; set; }
        public List<StoreVM> StoreList { get; set; }
        public List<DealTypeVM> DealTypeList { get; set; }
        public List<LookupItem> ItemList { get; set; }
        public List<BranchVM> BranchList { get; set; }
    }
}