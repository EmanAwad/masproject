﻿namespace MasProject.Domain.ViewModel.StoreTransaction.Transfer
{
    public class TransferOrderDetailsVM : TransferBaseEntity
    {
        public int Index { get; set; }
        public int? TransferOrderId { get; set; }
        public double? Quantity { get; set; }
        public int? ItemId { get; set; }
        public string ItemName { get; set; }
        public string ItemImg { get; set; }
        public int? BranchFromID { get; set; }
        public string BranchFromName { get; set; }
        public virtual TransferOrderVM TransferOrderVM { get; set; }
        // for View Only
        public string Code { get; set; }
        public double? BranchToQuantity { get; set; }
        public double? MainStoreQuantity { get; set; }
        public double? SelectedBranchQuantity { get; set; }

        public int? StoreId { get; set; }
    }
}
