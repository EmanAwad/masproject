﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.StoreTransaction.Transfer
{
    public class TransferRequestItemVM : TransferBaseEntity
    {
        public int? TransferRequestId { get; set; }
        public double? RequiredQuantity { get; set; }
        public int? BranchFromID { get; set; }
        public string BranchFromName { get; set; }
        public string BranchToID { get; set; }
        public int? ItemId { get; set; }
        public string ItemName { get; set; }
        public string ItemImg { get; set; }
        public virtual TransferRequestVM TransferRequestVM { get; set; }
        // for View Only
        public string Code { get; set; }
        public double? BranchToQuantity { get; set; }
        public double? MainStoreQuantity { get; set; }
        public double? SelectedBranchQuantity { get; set; }
    }
}
