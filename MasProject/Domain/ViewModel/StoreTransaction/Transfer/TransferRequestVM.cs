﻿using System.Collections.Generic;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel;

namespace MasProject.Domain.ViewModel.StoreTransaction.Transfer
{
    public class TransferRequestVM : TransferBaseEntity
    {
      
        public bool IsOrder { get; set; }
        public int? BranchToID { get; set; }
        public int? Convert { get; set; }
        public string BranchToName { get; set; }
        public List<BranchVM> BranchList { get; set; }
        public List<LookupItem> ItemList { get; set; }

        public List<TransferRequestItemVM> TransferRequestItemList { get; set; }

    }
}
