﻿using System.Collections.Generic;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel;
namespace MasProject.Domain.ViewModel.StoreTransaction.Transfer
{
    public class TransferOrderVM :TransferBaseEntity
    {
        public int? TransferRequestId { get; set; }
        public int? BranchToID { get; set; }
        public string BranchToName { get; set; }
        public int? Convert { get; set; }
        public bool IsConverted { get; set; }
        public List<BranchVM> BranchList { get; set; }
        public List<LookupItem> ItemList { get; set; }
        public List<TransferOrderDetailsVM> TransferOrderDetailsList { get; set; }
        public List<TransferRequestAndOrder> TransferList { get; set; }
    }
}
