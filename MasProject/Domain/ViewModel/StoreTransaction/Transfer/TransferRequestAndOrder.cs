﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.StoreTransaction.Transfer
{
    public class TransferRequestAndOrder
    {
        public int SerialNumber { get; set; }
        public string BranchToName { get; set; }
        public int? TransRequestId { get; set; }
        public int? TransOrderId { get; set; }
        public bool IsOrder { get; set; }

        public DateTime Date { get; set; }
    }
}
