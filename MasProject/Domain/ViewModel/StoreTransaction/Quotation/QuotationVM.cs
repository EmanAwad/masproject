﻿using MasProject.Data.Models.Persons;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Items;
using System.Collections.Generic;

namespace MasProject.Domain.ViewModel.StoreTransaction.Quotation
{
   public class QuotationVM : SaleBaseEntity
    {
        public int? BranchId { get; set; }
        public string BranchName { get; set; }
        public string Client { get; set; }

     
        public List<QuotationItemsVM> QuotationItems { get; set; }
        //For Design Show Only
        public decimal? ClientBalance { get; set; }
        public int? TaxFileNumber { get; set; }
        public bool IsConverted { get; set; }

        public string convert { get; set; }
        //new 09-11-2020
        public List<BranchVM> BranchList { get; set; }
        public List<DealTypeVM> DealTypeList { get; set; }
        public List<LookupItem> ItemList { get; set; }
        public string ItemImg { get; set; }
    }
}
