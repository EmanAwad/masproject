﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.StoreTransaction.Quotation
{
   public class QuotationItemsVM: SalesItemsBaseEntity
    {

        public decimal? SavedPrice { get; set; }
        public int? Identifer { get; set; }
        public virtual QuotationVM Quotation { get; set; }
        public string ItemImg { get; set; }

        //public static implicit operator List<object>(QuotationItemsVM v)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
