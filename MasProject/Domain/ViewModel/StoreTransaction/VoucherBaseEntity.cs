﻿using MasProject.Data.Models.Hierarchy;
using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.StoreTransaction
{
    public class VoucherBaseEntity
    {
        public int ID { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string Note { get; set; }

        //columns for This Modules
        public int? SerialNumber { get; set; }
        public DateTime Date { get; set; }
        public int? StoreId { get; set; }
        public virtual Store Store { get; set; }
        public bool ShowNotesFlag { get; set; }
        public int UserId { get; set; }
        public int RevewId { get; set; }
        public int ApproveId { get; set; }
    }
}
