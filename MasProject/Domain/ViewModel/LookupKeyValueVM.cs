﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel
{
    public class LookupKeyValueVM
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Type { get; set; }

        public string Code { get; set; }

        public int? arrangement { get; set; }

    }
}
