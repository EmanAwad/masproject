﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel
{
   public class LoginVM
    {

        public int UserID { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public bool RememberMe { get; set; }
        public string PasswordHash { get; set; }

        public int BranchID { get; set; }
    }
}
