﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.Persons
{
    public class SocialClientVM
    {
        public int ID { get; set; }
        public string Value { get; set; }
        public string Name { get; set; }
        public int SocialID { get; set; }
        public int ClientID { get; set; }
    }
}
