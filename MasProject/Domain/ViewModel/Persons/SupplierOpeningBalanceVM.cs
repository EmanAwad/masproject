﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MasProject.Domain.ViewModel.Persons
{
    public class SupplierOpeningBalanceVM
    {
        public SupplierOpeningBalanceVM()
        {

        }
        public int ID { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string Note { get; set; }
       // [Required(ErrorMessage = "تاريخ  مطلوب")]
        public DateTime EntryDate { get; set; }
     //   [Required(ErrorMessage = "المبلغ مطلوب")]
        //[Range(1, 100000, ErrorMessage = "المدى المسموح بيه (1-100000).")]
       // public double? Amount { get; set; }
       // public double? Ratio { get; set; }
     //   public int? SupplierID { get; set; }
     //   public string SupplierName { get; set; }
     //   public int? CurrencyID { get; set; }
    //    public string CurrencyName { get; set; }
        public List<LookupKeyValueVM> CurrencyList { get; set; } = new List<LookupKeyValueVM>();
        public List<LookupKeyValueVM> SupplierList { get; set; } = new List<LookupKeyValueVM>();
        public List<SupplierOpeningBalaneDetailsVM> SupplierDetails { get; set; } = new List<SupplierOpeningBalaneDetailsVM>();
        public int DocumentNumber { get; set; }

    }
}