﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MasProject.Domain.ViewModel.Persons
{
    public class SupplierVM
    {
        public int ID { get; set; }
        [Required(ErrorMessage = "الاسم مطلوب")]
        [StringLength(50, ErrorMessage = "الأسم طويل جدا")]
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
       
        public string Note { get; set; }
      //  [Required(ErrorMessage = "التليفون مطلوب")]
        public string Phone { get; set; }
      //  [Required(ErrorMessage = "الموبايل مطلوب")]
        public string Mobile { get; set; }
      //  [Required(ErrorMessage = "العنوان مطلوب")]
        public string Address { get; set; }
       // [Required(ErrorMessage = "الايميل مطلوب")]
        public string Mail { get; set; }
        public int? CountryID { get; set; }
        public int? CityID { get; set; }
        public int? AreaID { get; set; }
        //[Required(ErrorMessage = "الكود مطلوب")]
        public string Code { get; set; }
      //  [Required(ErrorMessage = "ملف الضرايب مطلوب")]
        public string Taxfile { get; set; }
        public List<PurchasesArchivesVM> PurchaseList { get; set; }
        public int? CurrencyID { get; set; }
        public string CurrencyName { get; set; }
        public string EmployeeName { get; set; }
        public string WebSiteURL { get; set; }
        public int? arrangement { get; set; }

        public bool IsHide { get; set; }

    }
}
