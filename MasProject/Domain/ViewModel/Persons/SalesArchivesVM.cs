﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.Persons
{
    public class SalesArchivesVM
    {
        public int ID { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string Note { get; set; }
        public string Year { get; set; }
        public string Month { get; set; }
        public Decimal? NetSales{ get; set; }
        public string Note1 { get; set; }
        public int? ClientID { get; set; }
        public string ClientName { get; set; }
    }
}
