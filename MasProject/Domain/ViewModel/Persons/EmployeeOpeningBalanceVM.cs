﻿using MasProject.Domain.ViewModel.Hierarchy;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;


namespace MasProject.Domain.ViewModel.Persons
{
    public class EmployeeOpeningBalanceVM
    {
        public EmployeeOpeningBalanceVM()
        {

        }
        public int ID { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string Note { get; set; }
     //   [Required(ErrorMessage = "تاريخ  مطلوب")]
        public DateTime EntryDate { get; set; }
       // [Required(ErrorMessage = "المبلغ مطلوب")]
        //[Range(1, 100000, ErrorMessage = "المدى المسموح به (1-100000).")]
        public double? Amount { get; set; }
        public int? EmployeeID { get; set; }
        public string EmployeeName { get; set; }

        public List<LookupKeyValueVM> EmployeeList { get; set; } = new List<LookupKeyValueVM>();

    }
}
