﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.Persons
{
   public class SupplierOpeningBalaneDetailsVM
    {
        public int ID { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string Note { get; set; }
        public int SupplierDocumentNumber { get; set; }

        public double? Amount { get; set; }
        public int? SupplierID { get; set; }

       
        public string SupplierName { get; set; }

        public int? CurrencyID { get; set; }

        public double? Ratio { get; set; }
    }
}
