﻿using MasProject.Domain.ViewModel.Hierarchy;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MasProject.Domain.ViewModel.Persons
{
    public class ClientsVM 
    {
        public int ID { get; set; }
        [Required(ErrorMessage = "الاسم مطلوب")]
     //   [StringLength(50, ErrorMessage = "الأسم طويل جدا")]
        public string Name { get; set; }
        //[Required(ErrorMessage = "التليفون مطلوب")]
        //[MinLength(10, ErrorMessage = "التليفون 10 أرقام")]
        //[MaxLength(10, ErrorMessage = "التليفون 10 أرقام")]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }
        //[Required(ErrorMessage = "الموبيل مطلوب")]
        //[MinLength(11, ErrorMessage = " يجب ادخال 11 رقم")]
        //[MaxLength(11, ErrorMessage = "يجب ادخال 11 رقم")]
        public string Mobile { get; set; }
        public string Mobile2 { get; set; }
        // [Required(ErrorMessage = "العنوان مطلوب")]
        public string Address { get; set; }
       // [Required(ErrorMessage = "الايميل مطلوب")]
        public string Mail { get; set; }
       // [Required(ErrorMessage = "الكود مطلوب")]
        public string Code { get; set; }
        public int? CountryID { get; set; }
        public int? CityID { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
      
        public string Note { get; set; }
        //[Required(ErrorMessage = "ملف الضريبى  مطلوب")]
        public string Taxfile { get; set; }
       // [Required(ErrorMessage = "الموقع  مطلوب")]
        public string WebSiteURL { get; set; }
        //[Required(ErrorMessage = "نوع التعامل  مطلوب")]
        public int? Dealingtype { get; set; }
        public string  DealingtypeName { get; set; }
       // [Required(ErrorMessage = "تاريخ  مطلوب")]
        public DateTime FirstDealingDate { get; set; }
        //[Required(ErrorMessage = "تاريخ  مطلوب")]
        public DateTime LastDealingDate { get; set; }
        //[Required(ErrorMessage = "المتوسط الشهرى مطلوب")]
        public double? MonthlyAverage { get; set; }
        public bool IsBlocked { get; set; }
       // [Required(ErrorMessage = "الفرع مطلوب")]
        public int? BranchID { get; set; } 
        public string BranchName { get; set; }
       // [Required(ErrorMessage = "الموظف المسئول مطلوب")]
        public string EmployeeName{ get; set; }
        public int? arrangement { get; set; }
        public string GeneratedCode { get; set; }
        public List<SocialMediaVM> SocialMediaList { get; set; } = new List<SocialMediaVM>();
        public List<LookupKeyValueVM> CityList { get; set; } = new List<LookupKeyValueVM>();
        public List<BranchVM> BranchList { get; set; } = new List<BranchVM>();
        public List<DealTypeVM> DealTypeList { get; set; } = new List<DealTypeVM>();
        public List<SalesArchivesVM> SalesList { get; set; }

        public bool IsHide { get; set; }
    }
}