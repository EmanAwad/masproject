﻿using MasProject.Domain.ViewModel.Hierarchy;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MasProject.Domain.ViewModel.Persons
{
    public class EmployeeVM
    {
        public int ID { get; set; }
        [Required(ErrorMessage = "الاسم مطلوب")]
       // [StringLength(50, ErrorMessage = "الأسم طويل جدا")]
        public string Name { get; set; }
       // [Required(ErrorMessage = "الكود مطلوب")]
        public string Code { get; set; }
       // [Required(ErrorMessage = "العنوان مطلوب")]
        public string Address { get; set; }
       
        public string Mail { get; set; }
        public string Image { get; set; }
       // [Required(ErrorMessage = "تاريخ الميلاد مطلوب")]
        public DateTime DateOfBirth { get; set; }
       // [Required(ErrorMessage = "تاريخ الالتحاق بالشركة مطلوب")]
        public DateTime JoiningDate { get; set; }
       // [Required(ErrorMessage = "المسمى الوظيفى مطلوب")]
        public string JObTitle { get; set; }
       // [Required(ErrorMessage = "بداية الراتب مطلوب")]
       // [Range(1, 100000, ErrorMessage = "المدى المسموح بيه (1-20000).")]
        public double? StartSalary { get; set; }
       // [Required(ErrorMessage = "الزيادة السنوية مطلوبة")]
       // [Range(1, 100000, ErrorMessage = "المدى المسموح بيه (1-10000).")]
        public double? AnnualIncrease { get; set; }
       // [Required(ErrorMessage = "رصيد الاجازات السنوى مطلوب")]
       // [Range(1, 100000, ErrorMessage = "المدى المسموح بيه (1-45).")]
        public double? Annual { get; set; }
        public bool IsUser { get; set; }
        public string UserName { get; set; } = "";
        public string Passwordouble { get; set; } = "";
        public int? CountryID { get; set; }
        public int? CityID { get; set; }
        public int? AreaID { get; set; }

        public int? DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public int? BranchID{ get; set; }
        public string BranchName { get; set; }

        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        
        public string Note { get; set; }

        //[Required(ErrorMessage = "الرقم القومى  مطلوب")]
        //[MinLength(14, ErrorMessage =  "الرقم القومى غير صحيح ")]
        //[MaxLength(14, ErrorMessage =  "الرقم القومى غير صحيح ")]
        public string NationalID { get; set; }

        //[Required(ErrorMessage = "التليفون مطلوب")]
        //[MinLength(10, ErrorMessage = "التليفون 10 أرقام")]
        //[MaxLength(10, ErrorMessage = "التليفون 10 أرقام")]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }
        //[Required(ErrorMessage = "الموبيل مطلوب")]
        //[MinLength(11, ErrorMessage = " يجب ادخال 11 رقم")]
        //[MaxLength(11, ErrorMessage = "يجب ادخال 11 رقم")]
        public string  Mobile { get; set; }

        public int? arrangement { get; set; }

        public List<DepartmentVM> DepartmentList { get; set; }

        public List<BranchVM> BranchList { get; set; }

        public bool IsHide { get; set; }
        //New 27-05-2021
        public DateTime LeavingDate { get; set; }
        public int AttachmentFiles { get; set; }
        public string PhoneWork { get; set; }
        public string PhoneWorkDescription { get; set; }

        //public string userName { get; set; }
        //public string Password { get; set; }
    }
}
