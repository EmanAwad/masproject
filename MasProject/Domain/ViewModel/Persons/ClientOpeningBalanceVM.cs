﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MasProject.Domain.ViewModel.Persons
{
    public class ClientOpeningBalanceVM
    {
        public ClientOpeningBalanceVM()
        {
        }
        public int ID { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string Note { get; set; }
        // [Required(ErrorMessage = "تاريخ  مطلوب")]
        public DateTime EntryDate { get; set; }
        //[Required(ErrorMessage = "المبلغ مطلوب")]
        //[Range(1, 100000, ErrorMessage = "المدى المسموح بيه (1-100000).")]
        public int DocumentNumber { get; set; }
        public int? BranchID { get; set; }
        public string BranchName { get; set; }
        public int? EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public List<ClientOpeningBalanceDetailsVM> ClientDetails { get; set; } = new List<ClientOpeningBalanceDetailsVM>();
        public List<LookupKeyValueVM> BranchList { get; set; } = new List<LookupKeyValueVM>();
        public List<LookupKeyValueVM> ClientList { get; set; } = new List<LookupKeyValueVM>();
        public List<LookupKeyValueVM> EmployeeList { get; set; } = new List<LookupKeyValueVM>();

    }
}