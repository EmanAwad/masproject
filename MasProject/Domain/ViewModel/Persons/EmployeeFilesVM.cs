﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.Persons
{
    public class EmployeeFilesVM : BaseEntity
    {
        public int EmployeeId { get; set; }
        public string FilePath { get; set; }
    }
}
