﻿using System;
using System.Collections.Generic;
using System.Text;
using static MasProject.Domain.Enums.Enums;

namespace MasProject.Domain.ViewModel.TransVM
{
   public class TransItemVM
    {
        public int Id { get; set; }
        public DateTime CreateDate { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime Date { get; set; }
        public TransItemTypeEnum ItemType { get; set; }
        public int ItemTypeID { get; set; }
        public string DisplayName { get; set; }
        public string ShowDate { get; set; }
        public int SerialNumber { get; set; }
        public int ForeignSerialNumber { get; set; }
        public decimal Addition { get; set; }//وارد
        public decimal Subtraction { get; set; }//منصرف
        public decimal Balance { get; set; }
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public int StoreId { get; set; }
        public string StoreName { get; set; }
        public bool OpenBalFlag { get; set; }
        public string PersonName { get; set; }
        public int PersonType { get; set; }
        public int PersonId { get; set; }
      
    }
}
