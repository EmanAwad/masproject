﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.TransVM
{
  public   class EmpolyeeBalanceReportVM
    {
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string PageName { get; set; }
        public int EntryId { get; set; }
        public decimal? Balance { get; set; }

        public DateTime? DateForm { get; set; }
        public DateTime? DateTo { get; set; }
    }
}
