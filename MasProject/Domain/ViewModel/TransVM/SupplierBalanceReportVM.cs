﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.TransVM
{
    public class SupplierBalanceReportVM
    {
        public int SupplierId { get; set; }
        public string SupplierName { get; set; }
        public string PageName { get; set; }
        public int BillId { get; set; }
        public decimal? Balance { get; set; }

        public DateTime? DateForm { get; set; }
        public DateTime? DateTo { get; set; }
    }
}
