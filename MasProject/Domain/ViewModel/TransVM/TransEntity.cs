﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.TransVM
{
  public  class TransEntity
    {
        public int Id { get; set; }
        public DateTime CreateDate { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime Date { get; set; }
        public int SerialNumber { get; set; }
        public decimal Debit { get; set; }//مدين
        public decimal Credit { get; set; }//دائن
        public decimal Balance { get; set; }
        public bool OpenBalFlag { get; set; }

    }
}
