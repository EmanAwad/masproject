﻿using System;
using System.Collections.Generic;
using System.Text;
using static MasProject.Domain.Enums.Enums;

namespace MasProject.Domain.ViewModel.TransVM
{
  public  class StoreReportVM
    {
        public DateTime ShowDate { get; set; }
        public DateTime CreateDate { get; set; }
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public string ItemCode { get; set; }
        public int StoreId { get; set; }
        public string StoreIdentifier { get; set; }
        public string StoreName { get; set; }
        public string PersonName { get; set; }
       // public int PersonType { get; set; }
        public int PersonId { get; set; }
        public int SerialNumber { get; set; }
        //public int ForeignSerialNumber { get; set; }
        //new 05-06-2021
        public TransClientTypeEnum TransactionType { get; set; }
        public int BranchId { get; set; }
        public string BranchName { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Total { get; set; }
    }
}
