﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.TransVM
{
   public class ClientBalanceReportVM
    {
        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public string PageName { get; set; }
        public int BillId { get; set; }
        public DateTime? DateForm { get; set; }
        public DateTime? DateTo { get; set; }
        public decimal? Balance { get; set; }

        public static explicit operator ClientBalanceReportVM(string v)
        {
            throw new NotImplementedException();
        }
    }
}
