﻿using MasProject.Domain.ViewModel.Persons;
using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.TransVM
{
   public class PersonsBalanceReportVM
    {
        public List<LookupKeyValueVM> Client_Supplier_List { get; set; } = new List<LookupKeyValueVM>();
        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public List<ClientsVM> ClientsList { get; set; }
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public int SupplierId { get; set; }
        public string SupplierName { get; set; }

        public string PersonName { get; set; }
        public int PersonId { get; set; }
        public decimal? Balance { get; set; }

        public DateTime? DateForm { get; set; }
        public DateTime? DateTo { get; set; }
    }
}
