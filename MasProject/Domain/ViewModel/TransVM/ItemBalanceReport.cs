﻿using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.ViewModel.Items;
using System;
using System.Collections.Generic;
using System.Text;
using static MasProject.Domain.Enums.Enums;

namespace MasProject.Domain.ViewModel.TransVM
{
    public class ItemBalanceReportVM
    {
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public int StoreId { get; set; }
        public string StoreName { get; set; }
        public string PageName { get; set; }
        public int BillId { get; set; }
        public DateTime? DateForm { get; set; }
        public DateTime? DateTo { get; set; }
        public decimal? Balance { get; set; }

        public List<StoreVM> StoreList { get; set; }
        public List<ItemVM> ItemList { get; set; }
        //new 07-06-2021
        public List<StoreTypeEnum> TransactionTypeList { get; set; }
        public List<BranchVM> BranchList { get; set; }
        public List<ItemCollectionVM> ItemCollectionList { get; set; }
        public List<LookupKeyValueVM> PersionList { get; set; }
        public int PersionId { get; set; }
        public string PersionName { get; set; }
        public int TransactionTypeId { get; set; }
        public string TransactionTypeName { get; set; }

        public int BranchId { get; set; }
        public string BranchName { get; set; }

        public int ItemCollectionId { get; set; }
        public string ItemCollectionName { get; set; }
        //
        public int BillsNumber { get; set; }
        public decimal BillsTotal { get; set; }
    }
}
