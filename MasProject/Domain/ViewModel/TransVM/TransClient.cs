﻿using System;
using System.Collections.Generic;
using System.Text;
using static MasProject.Domain.Enums.Enums;

namespace MasProject.Domain.ViewModel.TransVM
{
    public class TransClientVM
    {
        public int Id { get; set; }
        public DateTime CreateDate { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime Date { get; set; }
        public TransClientTypeEnum ClientType { get; set; }
        public int ClientTypeID { get; set; }
        public string DisplayName { get; set; }
        public string ShowDate { get; set; }
        public int SerialNumber { get; set; }
        public decimal Debit { get; set; }//مدين
        public decimal Credit { get; set; }//دائن
        public decimal Balance { get; set; }
        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public bool OpenBalFlag { get; set; }
    }
}
