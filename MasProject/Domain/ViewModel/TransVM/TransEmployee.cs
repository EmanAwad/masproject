﻿using System;
using System.Collections.Generic;
using System.Text;
using static MasProject.Domain.Enums.Enums;

namespace MasProject.Domain.ViewModel.TransVM
{
  public   class TransEmployeeVM
    {
        public int Id { get; set; }
        public DateTime CreateDate { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime Date { get; set; }
        public TransEmployeeTypeEnum EmployeeType { get; set; }
        public int EmployeeTypeID { get; set; }
        public string DisplayName { get; set; }
        public string ShowDate { get; set; }
        public int SerialNumber { get; set; }
        public decimal Debit { get; set; }//مدين
        public decimal Credit { get; set; }//دائن
        public decimal Balance { get; set; }
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public bool OpenBalFlag { get; set; }
    }
}
