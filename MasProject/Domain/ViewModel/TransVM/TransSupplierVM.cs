﻿using System;
using System.Collections.Generic;
using System.Text;
using static MasProject.Domain.Enums.Enums;

namespace MasProject.Domain.ViewModel.TransVM
{
    public class TransSupplierVM
    {
        public int Id { get; set; }
        public DateTime CreateDate { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime Date { get; set; }
        public TransSupplierTypeEnum SupplierType { get; set; }
        public int SupplierTypeID { get; set; }
        public string DisplayName { get; set; }
        public string ShowDate { get; set; }
        public int SerialNumber { get; set; }
        public decimal Debit { get; set; }//مدين
        public decimal Credit { get; set; }//دائن
        public decimal Balance { get; set; }
        public int SupplierId { get; set; }
        public string SupplierName { get; set; }
        public bool OpenBalFlag { get; set; }
    }
}
