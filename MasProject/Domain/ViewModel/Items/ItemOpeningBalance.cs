﻿using System;
using System.Collections.Generic;
namespace MasProject.Domain.ViewModel.Items
{
    public class ItemOpeningBalanceVM : IEntity //BaseEntity
    {
        public int DocumentNumber { get; set; }
        public DateTime Date { get; set; }
        public int? StoreId { get; set; }
        public string StoreName { get; set; }
        public int? EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public bool IsPrinted { get; set; }
        public int ID { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string Notes { get; set; }

        public List<ItemOpeningBalanceDetailsVM> ItemDetails { get; set; } = new List<ItemOpeningBalanceDetailsVM>();
        public List<LookupItem> ItemList { get; set; }
        public List<LookupKeyValueVM> StoreList { get; set; } = new List<LookupKeyValueVM>();
        public List<LookupKeyValueVM> ClientList { get; set; } = new List<LookupKeyValueVM>();
        public List<LookupKeyValueVM> EmployeeList { get; set; } = new List<LookupKeyValueVM>();
    }
}
