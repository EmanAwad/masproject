﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.Items
{
    public class ItemOpeningBalanceDetailsVM
    {

        public int ID { get; set; }
        public int ItemDocumentNumber { get; set; }
        public string Name { get; set; } = "";
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int? ItemId { get; set; }
        public string ItemName { get; set; }
        public string ItemCode { get; set; }
        public float? price { get; set; }
        public float? TotalPrice { get; set; }
        public int? Quantity { get; set; }
        public string Notes { get; set; }
    }
}
