﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.Items
{
    public class ItemBillVM
    {
        public string ItemImg { get; set; }
        public string National_ParCode { get; set; }
        public decimal Price { get; set; }
    }
}
