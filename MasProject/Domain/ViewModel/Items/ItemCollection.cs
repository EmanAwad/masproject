﻿using System.ComponentModel.DataAnnotations;

namespace MasProject.Domain.ViewModel.Items
{
    public class ItemCollectionVM :BaseEntity
    {
        [Required(ErrorMessage = "الترتيب مطلوب")]
        [Range(1, 1000000, ErrorMessage = "لا يجب أن يقل الترتيب عن 1 ")]
        public int? arrangement { get; set; }
    }
}
