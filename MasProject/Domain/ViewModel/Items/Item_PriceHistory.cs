﻿using System;

namespace MasProject.Domain.ViewModel.Items
{
    public class Item_PriceHistoryVM : BaseEntity
    {
        public int? ItemId { get; set; }
        public virtual ItemVM Item { get; set; }

        public DateTime Date { get; set; }
        public float? Price { get; set; }


        public int? EmployeeId { get; set; }

        //  public virtual EmployeeVM Employee { get; set; }
    }
}
