﻿using MasProject.Domain.ViewModel.Hierarchy;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MasProject.Domain.ViewModel.Items
{
    public class Item_StoreVM
    {

        public int ID { get; set; }
        public string Name { get; set; }
        public int? StoreId { get; set; }

        public string StoreName { get; set; }
        public virtual StoreVM Store { get; set; }

        public int? ItemId { get; set; }
        public string ItemName { get; set; }
        public virtual ItemVM Item { get; set; }

        public string SheilfNo { get; set; }
        public int? Quantity { get; set; }
        public int? OrderLimit { get; set; }
        public string Value { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string Note { get; set; }
    }
}
