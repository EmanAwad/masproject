﻿using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel.Hierarchy;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MasProject.Domain.ViewModel.Items
{
    public class ItemVM : BaseEntity
    {
        public int? StoreId { get; set; }
        public string StoreName { get; set; }
        public List<StoreVM> Store { get; set; }
        public int? CollectionId { get; set; }
        public string CollectionName { get; set; }
        public List<ItemCollectionVM> ItemCollection { get; set; }
        public int? TypeId { get; set; }
        public string TypeName { get; set; }
        public List<ItemTypeVM> ItemType { get; set; }
        [Required(ErrorMessage = "الكود المحلي مطلوب")]
        public string National_ParCode { get; set; }
        public string Interational_ParCode { get; set; }
        public string Box_ParCode { get; set; }
        public string Supplier_ParCode { get; set; }

        public string ParCode_Note { get; set; }


        public decimal? CostPrice { get; set; }
        public float? SupplierPrice { get; set; }
        public float? WholesalePrice { get; set; }
        public float? SellingPrice { get; set; }
        public float? ExecutionPrice { get; set; }
        public int? other_Price { get; set; }

        public int? SupplierId { get; set; }
        public string SupplierName { get; set; }
        public List<SupplierVM> Supplier { get; set; }


        public int? BoxNumber { get; set; }
        public float? CBM { get; set; }
        public float? BoxWeight { get; set; }

        public int? StoredQuantity { get; set; }
        public int? OrderLimit { get; set; }
        public string ItemImg { get; set; }
        public DateTime ItemDate { get; set; }
        public int? ShielfId { get; set; }

        public float? ShowPrice { get; set; }
        public string Guarantee_ParCode { get; set; }
        public int Index { get; set; }
        public bool IsShow { get; set; }
        public List<Item_StoreVM> ItemStoreList { get; set; } = new List<Item_StoreVM>();

    }
}
