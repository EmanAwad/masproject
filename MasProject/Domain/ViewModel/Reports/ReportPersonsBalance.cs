﻿
using iTextSharp.text;
using iTextSharp.text.pdf;
using MasProject.Data.Models.Trans;
using MasProject.Data.Models.TransTables;
using MasProject.Domain.ViewModel.TransVM;
using System;
using System.Collections.Generic;
using System.IO;
namespace MasProject.Domain.ViewModel.Reports
{
   public class ReportPersonsBalanceVM
    {
        #region Declare
        int _totalColumn = 6;
        Document _document;
        Font _fontStyle;
        BaseFont bf = BaseFont.CreateFont(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Fonts), "trado.TTF"), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        PdfPTable _pdfTable = new PdfPTable(6);
        PdfPCell _pdfPCell;
        MemoryStream _memoryStream = new MemoryStream();
       public List<TransClient> _events;
     public   List<TransEmployee> _event2;
      public  List<TransSupplier> _event3;
        PersonsBalanceReportVM _personsBalance;
        #endregion
     
        public byte[] PrepareReport(List<TransClient> events, List<TransEmployee> _events2, List<TransSupplier> _event3, PersonsBalanceReportVM personsBalance)//
        {
            _personsBalance = personsBalance;
            _events = events;
            #region
            _document = new Document(PageSize.A4, 0f, 0f, 0f, 0f);
            _document.SetPageSize(PageSize.A4);
            _document.SetMargins(20f, 20f, 20f, 20f);
            _pdfTable.WidthPercentage = 100;
            _pdfTable.HorizontalAlignment = Element.ALIGN_LEFT;
            //_fontStyle = FontFactory.GetFont("tahoma", 8f, 1);
            _fontStyle = new Font(bf, 10, Font.NORMAL);
            PdfWriter.GetInstance(_document, _memoryStream);
            _document.Open();
            _pdfTable.TotalWidth = 800f;
            #endregion
            this.ReportHeader();
            this.ReportBody();
            _pdfTable.HeaderRows = 2;
            _document.Add(_pdfTable);
            _document.Close();
            return _memoryStream.ToArray();
        }

        private void ReportHeader()
        {
            _fontStyle = FontFactory.GetFont("tahoma", 12f, 1);
            _pdfPCell = new PdfPCell(new Phrase("كشف حساب جميع الأشخاص ", _fontStyle));
            _pdfPCell.Colspan = _totalColumn;
            _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
            _pdfPCell.PaddingTop = 10f;
            _pdfPCell.PaddingBottom = 10f;
            _pdfPCell.Border = 0;
            _pdfPCell.BackgroundColor = BaseColor.White;
            _pdfPCell.ExtraParagraphSpace = 0;
            _pdfTable.AddCell(_pdfPCell);
            _pdfTable.CompleteRow();

            _fontStyle = FontFactory.GetFont("tahoma", 10f, 1);
            _pdfPCell = new PdfPCell(new Phrase("اسم الشخص:" + _personsBalance.PersonName, _fontStyle));
            _pdfPCell.Colspan = _totalColumn;
            _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
            _pdfPCell.PaddingTop = 10f;
            _pdfPCell.PaddingBottom = 10f;
            _pdfPCell.Border = 0;
            _pdfPCell.BackgroundColor = BaseColor.White;
            _pdfPCell.ExtraParagraphSpace = 0;
            _pdfTable.AddCell(_pdfPCell);
            _pdfTable.CompleteRow();

            _fontStyle = FontFactory.GetFont("tahoma", 10f, 1);
            _pdfPCell = new PdfPCell(new Phrase("التاريخ", _fontStyle));
            _pdfPCell.Colspan = _totalColumn;
            _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
            _pdfPCell.PaddingTop = 10f;
            _pdfPCell.PaddingBottom = 10f;
            _pdfPCell.Border = 0;
            _pdfPCell.BackgroundColor = BaseColor.White;
            _pdfPCell.ExtraParagraphSpace = 0;
            _pdfTable.AddCell(_pdfPCell);
            _pdfTable.CompleteRow();
        }
        private void ReportBody()
        {

            _pdfTable.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
            #region Table header
            _fontStyle = FontFactory.GetFont("tahoma", 11f, 1);
            _pdfPCell = new PdfPCell(new Phrase("الشخص", _fontStyle));
            _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
            _pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            _pdfPCell.BackgroundColor = BaseColor.LightGray;
            _pdfTable.AddCell(_pdfPCell);

            _pdfPCell = new PdfPCell(new Phrase("الرصيد", _fontStyle));
            _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
            _pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            _pdfPCell.BackgroundColor = BaseColor.LightGray;
            _pdfTable.AddCell(_pdfPCell);

        
         
            _pdfTable.CompleteRow();
            #endregion
            #region Table Body
            //_fontStyle = FontFactory.GetFont("tahoma", 10f, 0);
            //string arialUNI_TFF = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Fonts), "tahoma.TTF");
            //BaseFont bf = BaseFont.CreateFont(arialUNI_TFF, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            //Font f = new Font(bf, 10, Font.NORMAL);
            //int serialNumber = 1;
            foreach (var eve in _events)
            {
                

                _pdfPCell = new PdfPCell(new Phrase(eve.ClientType.ToString() == null ? "" : eve.ClientType.ToString(), _fontStyle));
                _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
                _pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                _pdfPCell.BackgroundColor = BaseColor.White;
                _pdfTable.AddCell(_pdfPCell);

              

                _pdfPCell = new PdfPCell(new Phrase(eve.Balance.ToString() == null ? "" : eve.Balance.ToString(), _fontStyle));
                _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
                _pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                _pdfPCell.BackgroundColor = BaseColor.White;
                _pdfTable.AddCell(_pdfPCell);
                _pdfTable.CompleteRow();
            }


            foreach (var eve in _event2)
            {


                _pdfPCell = new PdfPCell(new Phrase(eve.EmployeeType.ToString() == null ? "" : eve.EmployeeType.ToString(), _fontStyle));
                _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
                _pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                _pdfPCell.BackgroundColor = BaseColor.White;
                _pdfTable.AddCell(_pdfPCell);



                _pdfPCell = new PdfPCell(new Phrase(eve.Balance.ToString() == null ? "" : eve.Balance.ToString(), _fontStyle));
                _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
                _pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                _pdfPCell.BackgroundColor = BaseColor.White;
                _pdfTable.AddCell(_pdfPCell);
                _pdfTable.CompleteRow();
            }

            foreach (var eve in _event3)
            {


                _pdfPCell = new PdfPCell(new Phrase(eve.SupplierType.ToString() == null ? "" : eve.SupplierType.ToString(), _fontStyle));
                _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
                _pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                _pdfPCell.BackgroundColor = BaseColor.White;
                _pdfTable.AddCell(_pdfPCell);



                _pdfPCell = new PdfPCell(new Phrase(eve.Balance.ToString() == null ? "" : eve.Balance.ToString(), _fontStyle));
                _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
                _pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                _pdfPCell.BackgroundColor = BaseColor.White;
                _pdfTable.AddCell(_pdfPCell);
                _pdfTable.CompleteRow();
            }
            #endregion
        }
    }
}
