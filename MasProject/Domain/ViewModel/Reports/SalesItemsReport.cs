﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.ViewModel.Reports
{
   public class SalesItemsReport
    {
        public int StoreId { get; set; }
        public int? Identifer { get; set; }
        public string ItemName { get; set; }
        public int? Quantity { get; set; }
        public decimal? Price { get; set; }
        public decimal? Total { get; set; }
        public string Box_ParCode { get; set; }
        public string StoreName { get; set; }
    }
}
