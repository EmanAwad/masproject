﻿using MasProject.Data.DataAccess;
using MasProject.Domain.Services;
using MasProject.Domain.Services.Hierarchy;
using MasProject.Domain.Services.Items;
using MasProject.Domain.Services.Persons;
using MasProject.Domain.Services.Quotation;
using MasProject.Domain.Services.StoreTransaction.Sales;
using MasProject.Domain.Services.StoreTransaction.Purchase;
using MasProject.Domain.Services.StoreTransaction.Quotation;
using MasProject.Domain.Services.StoreTransaction.Voucher;
using System;
using MasProject.Domain.Services.StoreTransaction.ExecutionWithDrawals;
using MasProject.Domain.Services.StoreTransaction.Transfer;
using MasProject.Domain.Services.StoreTransaction;
using MasProject.Domain.Services.StoreTransaction.Conversion;
using MasProject.Domain.Services.FundTransaction.ClientsPayment;
using MasProject.Domain.Services.FundTransaction.DiscountNotices;
using MasProject.Domain.Services.TransServices;
using MasProject.Domain.Services.FundTransaction.AdditionNotices;
using MasProject.Domain.Services.FundTransaction.SupplierPayments;
using MasProject.Domain.Services.FundTransaction;
using MasProject.Domain.Services.Accounting;

namespace MasProject.Domain
{
    public class UnitOfWork
    {
        private bool _disposed;
        public DBContext Context { get; private set; }
        public UnitOfWork(DBContext context)
        {
            Context = context;
        }

        public bool Commit()
        {
            bool result = true;
            try
            {
                Context.SaveChanges();
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                message = ex.InnerException.Message;
                result = false;
            }
            return result;
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing && Context != null)
                {
                    Context.Dispose();
                }
            }
            _disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #region Employee
        EmployeeService employeeservice;
        public EmployeeService EmployeeService => employeeservice ?? (employeeservice = new EmployeeService(Context));
        #endregion
        #region Supplier
        SupplierService supplierservice;
        public SupplierService SupplierService => supplierservice ?? (supplierservice = new SupplierService(Context));
        #endregion
        #region Client
        ClientsService clientservice;
        public ClientsService ClientsService => clientservice ?? (clientservice = new ClientsService(Context));
        SocialClientService socialClientService;
        public SocialClientService SocialClientService => socialClientService ?? (socialClientService = new SocialClientService(Context));
        #endregion
        #region PersonOpeningBalances
        EmployeeOpeningBalanceService employeeOpeningBalanceService;
        public EmployeeOpeningBalanceService EmployeeOpeningBalanceService => employeeOpeningBalanceService ?? (employeeOpeningBalanceService = new EmployeeOpeningBalanceService(Context));

        SupplierOpeningBalanceService supplierOpeningBalanceService;
        public SupplierOpeningBalanceService SupplierOpeningBalanceService => supplierOpeningBalanceService ?? (supplierOpeningBalanceService = new SupplierOpeningBalanceService(Context));



        SupplierOpeningBalanceDetailsService supplieropeningbalancedetailsService;
        public SupplierOpeningBalanceDetailsService SupplierOpeningBalanceDetailsService => supplieropeningbalancedetailsService ?? (supplieropeningbalancedetailsService = new SupplierOpeningBalanceDetailsService(Context));


        ClientOpeningBalanceService clientOpeningBalanceService;
        public ClientOpeningBalanceService ClientOpeningBalanceService => clientOpeningBalanceService ?? (clientOpeningBalanceService = new ClientOpeningBalanceService(Context));

        ClientOpenningBalanceDetailsService clientOpenningBalanceDetailsService;
        public ClientOpenningBalanceDetailsService ClientOpeningBalanceDetailsService => clientOpenningBalanceDetailsService ?? (clientOpenningBalanceDetailsService = new ClientOpenningBalanceDetailsService(Context));

        #endregion
        #region SocialMedia
        SocialMediaService socialMediaService;
        public SocialMediaService SocialMediaService => socialMediaService ?? (socialMediaService = new SocialMediaService(Context));

        #endregion
        #region Archieves
        SalesArchivesService salesArchivesService;
        public SalesArchivesService SalesArchivesService => salesArchivesService ?? (salesArchivesService = new SalesArchivesService(Context));
        PurchasesArchivesService purchasesArchivesService;
        public PurchasesArchivesService PurchasesArchivesService => purchasesArchivesService ?? (purchasesArchivesService = new PurchasesArchivesService(Context));
        #endregion
        #region City
        CityService cityService;
        public CityService CityService => cityService ?? (cityService = new CityService(Context));
        #endregion
        #region  Branch
        BranchServices branchservices;
        public BranchServices BranchServices => branchservices ?? (branchservices = new BranchServices(Context));
        #endregion

        #region OtherConstant
        OtherConstantService otherConstantService;
        public OtherConstantService OtherConstantService => otherConstantService ?? (otherConstantService = new OtherConstantService(Context));
        #endregion
        #region Company
        CompanyServices companyservices;
        public CompanyServices CompanyServices => companyservices ?? (companyservices = new CompanyServices(Context));
        #endregion

        #region Currency
        CurrencyServices currencyservices;
        public CurrencyServices CurrencyServices => currencyservices ?? (currencyservices = new CurrencyServices(Context));
        #endregion

        #region Department
        DepartmentServices departmentservices;
        public DepartmentServices DepartmentServices => departmentservices ?? (departmentservices = new DepartmentServices(Context));
        #endregion

        #region Store
        StoreServices storeservices;
        public StoreServices StoreServices => storeservices ?? (storeservices = new StoreServices(Context));
        #endregion

        #region Treasury
        TreasuryServices treasuryservices;
        public TreasuryServices TreasuryServices => treasuryservices ?? (treasuryservices = new TreasuryServices(Context));
        #endregion

        #region Employee
        TreasuryOpenBalanceOpenBalanceServices treasuryBalservices;
        public TreasuryOpenBalanceOpenBalanceServices TreasuryOpenBalanceOpenBalanceServices => treasuryBalservices ?? (treasuryBalservices = new TreasuryOpenBalanceOpenBalanceServices(Context));
        #endregion
        #region ItemsCollection
        ItemCollectionServices itemCollectionservices;
        public ItemCollectionServices ItemCollectionServices => itemCollectionservices ?? (itemCollectionservices = new ItemCollectionServices(Context));
        #endregion
        #region ItemType
        ItemTypeServices itemtypeservices;
        public ItemTypeServices ItemTypeServices => itemtypeservices ?? (itemtypeservices = new ItemTypeServices(Context));
        #endregion

        #region ItemOpeningBalance
        ItemOpeningBalanceDetailsService itemopeningbalanceDetailsservice;
        public ItemOpeningBalanceDetailsService ItemOpeningBalanceDetailsService => itemopeningbalanceDetailsservice ?? (itemopeningbalanceDetailsservice = new ItemOpeningBalanceDetailsService(Context));

        ItemOpeningBalanceService itemopeningbalanceservice;
        public ItemOpeningBalanceService ItemOpeningBalanceService => itemopeningbalanceservice ?? (itemopeningbalanceservice = new ItemOpeningBalanceService(Context));
        #endregion

        #region Items
        ItemServices itemservices;
        public ItemServices ItemServices => itemservices ?? (itemservices = new ItemServices(Context));
        #endregion

        #region ItemStore
        ItemStoreServices itemstoreservices;
        public ItemStoreServices ItemStoreServices => itemstoreservices ?? (itemstoreservices = new ItemStoreServices(Context));
        #endregion
        #region OtherIncome
        OtherIncomeService otherIncomeService;
        public OtherIncomeService OtherIncomeService => otherIncomeService ?? (otherIncomeService = new OtherIncomeService(Context));
        #endregion
        #region SellingExpenses
        SellingExpensesService SellingExpenseService;
        public SellingExpensesService SellingExpensesService => SellingExpenseService ?? (SellingExpenseService = new SellingExpensesService(Context));
        #endregion
        #region ExecutiveOtherIncomeService
        ExecutiveOtherIncomeService executiveOtherIncomeService;
        public ExecutiveOtherIncomeService ExecutiveOtherIncomeService => executiveOtherIncomeService ?? (executiveOtherIncomeService = new ExecutiveOtherIncomeService(Context));
        #endregion
        #region Users
        UserService userservice;
        public UserService UserService => userservice ?? (userservice = new UserService(Context));
        #endregion

        #region DealType
        DealTypeService DealTypeservice;
        public DealTypeService DealTypeService => DealTypeservice ?? (DealTypeservice = new DealTypeService(Context));
        #endregion

        #region  SalesItems
        SalesItemsService Itemservice;
        public SalesItemsService SalesItemsService => Itemservice ?? (Itemservice = new SalesItemsService(Context));
        #endregion

        #region SalesBills
        SalesBillsService SalesBillservice;
        public SalesBillsService SalesBillsService => SalesBillservice ?? (SalesBillservice = new SalesBillsService(Context));
        #endregion

        #region  SalesSellExpenses
        SalesSellExpensesService SalesSellExpenseService;
        public SalesSellExpensesService SalesSellExpensesService => SalesSellExpenseService ?? (SalesSellExpenseService = new SalesSellExpensesService(Context));
        #endregion

        #region Quotation
        QuotationService quotationservice;
        public QuotationService QuotationService => quotationservice ?? (quotationservice = new QuotationService(Context));
        #endregion

        #region QuotationItems
        QuotationItemsService quotationitemsservice;
        public QuotationItemsService QuotationItemsService => quotationitemsservice ?? (quotationitemsservice = new QuotationItemsService(Context));
        #endregion
        #region PurchaseBill
        PurchaseBillService purchaseBillService;
        public PurchaseBillService PurchaseBillService => purchaseBillService ?? (purchaseBillService = new PurchaseBillService(Context));

        PurchaseItemService purchaseItemService;
        public PurchaseItemService PurchaseItemService => purchaseItemService ?? (purchaseItemService = new PurchaseItemService(Context));

        PurchaseOtherIncomeService PurchaseOtherIncome;
        public PurchaseOtherIncomeService PurchaseOtherIncomeService => PurchaseOtherIncome ?? (PurchaseOtherIncome = new PurchaseOtherIncomeService(Context));

        #endregion

        #region  ExecutiveBills
        ExecutiveBillsService ExecutiveBillService;
        public ExecutiveBillsService ExecutiveBillsService => ExecutiveBillService ?? (ExecutiveBillService = new ExecutiveBillsService(Context));
        #endregion


        #region ExecutiveItems
        ExecutiveItemsService ExecutiveItemService;
        public ExecutiveItemsService ExecutiveItemsService => ExecutiveItemService ?? (ExecutiveItemService = new ExecutiveItemsService(Context));
        #endregion

        #region  ExecutiveSellExpenses
        ExecutiveSellExpensesService ExecutiveSellExpenseService;
        public ExecutiveSellExpensesService ExecutiveSellExpensesService => ExecutiveSellExpenseService ?? (ExecutiveSellExpenseService = new ExecutiveSellExpensesService(Context));
        #endregion

        #region MantienceBills
        MantienceBillsService MantienceBillService;
        public MantienceBillsService MantienceBillsService => MantienceBillService ?? (MantienceBillService = new MantienceBillsService(Context));
        #endregion

        #region  MantienceItems
        MantienceItemsService MantienceItemService;
        public MantienceItemsService MantienceItemsService => MantienceItemService ?? (MantienceItemService = new MantienceItemsService(Context));
        #endregion

        #region MaintanceSellExpenses
        MaintanceSellExpensesService MaintanceSellExpenseService;
        public MaintanceSellExpensesService MaintanceSellExpensesService => MaintanceSellExpenseService ?? (MaintanceSellExpenseService = new MaintanceSellExpensesService(Context));
        #endregion

        #region SalesReturns
        SalesReturnsService SalesReturnService;
        public SalesReturnsService SalesReturnsService => SalesReturnService ?? (SalesReturnService = new SalesReturnsService(Context));
        #endregion

        #region  SalesReturnsItems
        SalesReturnsItemsService SalesReturnsItemService;
        public SalesReturnsItemsService SalesReturnsItemsService => SalesReturnsItemService ?? (SalesReturnsItemService = new SalesReturnsItemsService(Context));
        #endregion

        #region  SalesReturnsExpenses
        SalesReturnsExpensesService salesReturnsExpensesService;
        public SalesReturnsExpensesService SalesReturnsExpensesService => salesReturnsExpensesService ?? (salesReturnsExpensesService = new SalesReturnsExpensesService(Context));
        #endregion

        #region Addition Voucher
        AdditionVoucherService additionVoucherService;
        public AdditionVoucherService AdditionVoucherService => additionVoucherService ?? (additionVoucherService = new AdditionVoucherService(Context));
        
        AdditionVoucherItemService additionVoucherItemService;
        public AdditionVoucherItemService AdditionVoucherItemService => additionVoucherItemService ?? (additionVoucherItemService = new AdditionVoucherItemService(Context));

        TempAdditionVoucherService tempAdditionVoucherService;
        public TempAdditionVoucherService TempAdditionVoucherService => tempAdditionVoucherService ?? (tempAdditionVoucherService = new TempAdditionVoucherService(Context));

        TempAdditionVoucherItemService tempAdditionVoucherItemService;
        public TempAdditionVoucherItemService TempAdditionVoucherItemService => tempAdditionVoucherItemService ?? (tempAdditionVoucherItemService = new TempAdditionVoucherItemService(Context));

        #endregion
        #region Payment Voucher
        PaymentVoucherService paymentVoucherService;
        public PaymentVoucherService PaymentVoucherService => paymentVoucherService ?? (paymentVoucherService = new PaymentVoucherService(Context));

        PaymentVoucherItemService paymentVoucherItemService;
        public PaymentVoucherItemService PaymentVoucherItemService => paymentVoucherItemService ?? (paymentVoucherItemService = new PaymentVoucherItemService(Context));

        TempPaymentVoucherService tempPaymentVoucherService;
        public TempPaymentVoucherService TempPaymentVoucherService => tempPaymentVoucherService ?? (tempPaymentVoucherService = new TempPaymentVoucherService(Context));

        TempPaymentVoucherItemService tempPaymentVoucherItemService;
        public TempPaymentVoucherItemService TempPaymentVoucherItemService => tempPaymentVoucherItemService ?? (tempPaymentVoucherItemService = new TempPaymentVoucherItemService(Context));

        #endregion

        #region PurchasesReturns_BillService
        PurchasesReturns_BillService purchasesreturns_pillservice;
        public PurchasesReturns_BillService PurchasesReturns_BillService => purchasesreturns_pillservice ?? (purchasesreturns_pillservice = new PurchasesReturns_BillService(Context));

        PurchaseReturns_ItemsService purchasereturns_itemsService;
        public PurchaseReturns_ItemsService PurchaseReturns_ItemsService => purchasereturns_itemsService ?? (purchasereturns_itemsService = new PurchaseReturns_ItemsService(Context));

        PurchaseReturns_OtherIncomeService purchaseReturns_OtherIncomeService;
        public PurchaseReturns_OtherIncomeService PurchaseReturns_OtherIncomeService => purchaseReturns_OtherIncomeService ?? (purchaseReturns_OtherIncomeService = new PurchaseReturns_OtherIncomeService(Context));
        #endregion

        #region ExecutionWithDrawals_BillService
        ExecutionWithDrawals_BillService executionwithdrawals_billService;
        public ExecutionWithDrawals_BillService ExecutionWithDrawals_BillService => executionwithdrawals_billService ?? (executionwithdrawals_billService = new ExecutionWithDrawals_BillService(Context));

        ExecutionWithDrawals_ItemsService executionwithdrawals_itemsservice;
        public ExecutionWithDrawals_ItemsService ExecutionWithDrawals_ItemsService => executionwithdrawals_itemsservice ?? (executionwithdrawals_itemsservice = new ExecutionWithDrawals_ItemsService(Context));
        #endregion
        #region Item_convertion
        ItemConversion_BillService itemConversion_BillService;
        public ItemConversion_BillService ItemConversion_BillService => itemConversion_BillService ?? (itemConversion_BillService = new ItemConversion_BillService(Context));
   ItemConversion_ItemsService itemConversion_ItemsService;
        public ItemConversion_ItemsService ItemConversion_ItemsService => itemConversion_ItemsService ?? (itemConversion_ItemsService = new ItemConversion_ItemsService(Context));
        #endregion
        #region Transfer Request
        TransferRequestService transferRequestService;
        public TransferRequestService TransferRequestService => transferRequestService ?? (transferRequestService = new TransferRequestService(Context));

        TransferRequestItemService transferItemRequestService;
        public TransferRequestItemService TransferRequestItemService => transferItemRequestService ?? (transferItemRequestService = new TransferRequestItemService(Context));

        #endregion
        #region Transfer Order
        TransferOrderService transferOrderService;
        public TransferOrderService TransferOrderService => transferOrderService ?? (transferOrderService = new TransferOrderService(Context));

        TransferOrderDetailsService transferOrderDetailsService;
        public TransferOrderDetailsService TransferOrderDetailsService => transferOrderDetailsService ?? (transferOrderDetailsService = new TransferOrderDetailsService(Context));

        #endregion
        #region ClientPaymentService
        ClientPaymentService clientPaymentService;
        public ClientPaymentService ClientPaymentService => clientPaymentService ?? (clientPaymentService = new ClientPaymentService(Context));
        #endregion
        #region DiscountNoticeService
        DiscountNoticeService discountNoticeService;
        public DiscountNoticeService DiscountNoticeService => discountNoticeService ?? (discountNoticeService = new DiscountNoticeService(Context));
        #endregion
        //new 28-10-2020
        #region TransClient
        TransClientService TransClientsService;
        public TransClientService TransClientService => TransClientsService ?? (TransClientsService = new TransClientService(Context));
        #endregion
        #region TransSupplierService
        TransSupplierService transSupplierService;
        public TransSupplierService TransSupplierService => transSupplierService ?? (transSupplierService = new TransSupplierService(Context));
        #endregion
        #region AdditionNotice
        AdditionNoticeService AdditionNoticeServices;
        public AdditionNoticeService AdditionNoticeService => AdditionNoticeServices ?? (AdditionNoticeServices = new AdditionNoticeService(Context));
        #endregion
        #region SupplierPayment
        SupplierPaymentService SupplierPaymentServices;
        public SupplierPaymentService SupplierPaymentService => SupplierPaymentServices ?? (SupplierPaymentServices = new SupplierPaymentService(Context));
        #endregion
        #region EmployeesLoans
        EmployeesLoansEntryService employeesLoansEntryService;
        public EmployeesLoansEntryService EmployeesLoansEntryService => employeesLoansEntryService ?? (employeesLoansEntryService = new EmployeesLoansEntryService(Context));
        #endregion

        #region EmployeesLoansPayment
        EmployeesLoansPaymentService EmployeesLoansPaymentServices;
        public EmployeesLoansPaymentService EmployeesLoansPaymentService => EmployeesLoansPaymentServices ?? (EmployeesLoansPaymentServices = new EmployeesLoansPaymentService(Context));
        #endregion

        #region TransEmployee
        TransEmployeeService TransEmployeeServices;
        public TransEmployeeService TransEmployeeService => TransEmployeeServices ?? (TransEmployeeServices = new TransEmployeeService(Context));
        #endregion

        #region  SalesBillStores
        SalesBillStoresService SalesBillStoresServices;
        public SalesBillStoresService SalesBillStoresService => SalesBillStoresServices ?? (SalesBillStoresServices = new SalesBillStoresService(Context));
        #endregion

        #region ClientPayoffsService
        ClientsPayoffsService clientspayoffsService;
        public ClientsPayoffsService ClientsPayoffsService => clientspayoffsService ?? (clientspayoffsService = new ClientsPayoffsService(Context));
        #endregion

        #region  SalesReturnsBillStores
        SalesReturnsBillStoresService SalesReturnsBillStoresServices;
        public SalesReturnsBillStoresService SalesReturnsBillStoresService => SalesReturnsBillStoresServices ?? (SalesReturnsBillStoresServices = new SalesReturnsBillStoresService(Context));
        #endregion

        #region  MaintanceBillStores
        MaintanceBillStoresService MaintanceBillStoresServices;
        public MaintanceBillStoresService MaintanceBillStoresService => MaintanceBillStoresServices ?? (MaintanceBillStoresServices = new MaintanceBillStoresService(Context));
        #endregion

        #region  ExecutiveBillStores
        ExecutiveBillStoresService ExecutiveBillStoresServices;
        public ExecutiveBillStoresService ExecutiveBillStoresService => ExecutiveBillStoresServices ?? (ExecutiveBillStoresServices = new ExecutiveBillStoresService(Context));
        #endregion

        ExecutionWithdrawl_BillStoresService ExecutionWithdrawl_BillStoresServices;
        public ExecutionWithdrawl_BillStoresService ExecutionWithdrawl_BillStoresService => ExecutionWithdrawl_BillStoresServices ?? (ExecutionWithdrawl_BillStoresServices = new ExecutionWithdrawl_BillStoresService(Context));

        #region EntriesDetails
        EntriesDetailsService entriesdetailsService;
        public EntriesDetailsService EntriesDetailsService => entriesdetailsService ?? (entriesdetailsService = new EntriesDetailsService(Context));
        #endregion

        #region Entries
        EntriesService entriesService;
        public EntriesService EntriesService => entriesService ?? (entriesService = new EntriesService(Context));
        #endregion
        #region Guide 
        AccountingGuideService accountingGuideService;
        public AccountingGuideService AccountingGuideService => accountingGuideService ?? (accountingGuideService = new AccountingGuideService(Context));
        #endregion

        //22-01-2021
        #region TransItem
        TransItemService TransItemServices;
        public TransItemService TransItemService => TransItemServices ?? (TransItemServices = new TransItemService(Context));
        #endregion

        #region EntriesStructuring
        EntriesStructureService entriesstructureService;
        public EntriesStructureService EntriesStructureService => entriesstructureService ?? (entriesstructureService = new EntriesStructureService(Context));
        #endregion


        #region EntriesStructuring_Details
        EntriesStructure_DetailsService entriesstructure_detailsService;
        public EntriesStructure_DetailsService EntriesStructure_DetailsService => entriesstructure_detailsService ?? (entriesstructure_detailsService = new EntriesStructure_DetailsService(Context));
        #endregion

        #region EntriesTypeService
        EntriesTypeService EntriesTypeServices;
        public EntriesTypeService EntriesTypeService => EntriesTypeServices ?? (EntriesTypeServices = new EntriesTypeService(Context));
        #endregion
    }
}
