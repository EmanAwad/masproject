﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Trans;
using MasProject.Domain.ViewModel.TransVM;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using static MasProject.Domain.Enums.Enums;

namespace MasProject.Domain.Services.TransServices
{
    public class TransEmployeeService : Repository<TransEmployee>
    {
        private DBContext _context;
        public TransEmployeeService(DBContext context) : base(context)
        {
            _context = context;
        }
        private static string GetDisplayAttribute(object value)
        {
            Type type = value.GetType();
            var field = type.GetField(value.ToString());
            return field == null ? null : field.GetCustomAttribute<DisplayAttribute>().GetName();
        }
        public EmpolyeeBalanceReportVM GetBill(int SerialNumber, int Type)
        {
            EmpolyeeBalanceReportVM trans = new EmpolyeeBalanceReportVM();
            switch (Type)
            {
                case (int)TransEmployeeTypeEnum.رصيد_اول_المدة:
                    trans.PageName = "EmployeeOpeningBalanceEdit";
                    trans.EntryId = SerialNumber;
                    break;
                case (int)TransEmployeeTypeEnum.سلف_الموظف:
                    trans.PageName = "EmployeeLoanEntryEdit";
                    trans.EntryId = _context.EmployeesLoansEntry.FirstOrDefault(x => x.SerialNumber == SerialNumber).ID;
                    break;
                case (int)TransEmployeeTypeEnum.رد_سلف_الموظف:
                    trans.PageName = "EmployeeLoanPaymentEdit";
                    trans.EntryId = _context.EmployeesLoansPayment.FirstOrDefault(x => x.SerialNumber == SerialNumber).ID;
                    break;
            }
            return trans;
        }
        public IEnumerable<TransEmployeeVM> GetEmployeeBalance(EmpolyeeBalanceReportVM EmployeeBalance)
        {
            IEnumerable<TransEmployeeVM> returnList;
            if (EmployeeBalance.DateTo == DateTime.MinValue && EmployeeBalance.DateForm == DateTime.MinValue)
            {
                returnList = dbSet.Where(x => x.EmployeeId == EmployeeBalance.EmployeeId && !x.IsDeleted).Select(s => new TransEmployeeVM
                {
                    Id = s.ID,
                    SerialNumber = (int)s.SerialNumber,
                    ShowDate = s.Date.ToString("dd/MM/yyyy"),
                    EmployeeId = s.EmployeeId,
                    EmployeeType = (Enums.Enums.TransEmployeeTypeEnum)s.EmployeeType,
                    EmployeeTypeID = s.EmployeeType,
                    DisplayName = GetDisplayAttribute((Enums.Enums.TransEmployeeTypeEnum)s.EmployeeType),
                    EmployeeName = _context.Employee.FirstOrDefault(c => c.ID == s.EmployeeId).Name,
                    Balance = s.Balance,
                    Credit = s.Credit,
                    Debit = s.Debit,
                    Date = s.Date
                }).OrderBy(x => x.Date.Date).ThenBy(x => x.Id);
            }
            else if (EmployeeBalance.DateTo == DateTime.MinValue && EmployeeBalance.DateForm != DateTime.MinValue)
            {
                returnList = dbSet.Where(x => x.EmployeeId == EmployeeBalance.EmployeeId && x.Date.Date == EmployeeBalance.DateForm.Value.Date && !x.IsDeleted).Select(s => new TransEmployeeVM
                {
                    Id = s.ID,
                    SerialNumber = (int)s.SerialNumber,
                    ShowDate = s.Date.ToString("dd/MM/yyyy"),
                    EmployeeId = s.EmployeeId,
                    EmployeeTypeID = s.EmployeeType,
                    EmployeeType = (Enums.Enums.TransEmployeeTypeEnum)s.EmployeeType,
                    DisplayName = GetDisplayAttribute((Enums.Enums.TransEmployeeTypeEnum)s.EmployeeType),
                    EmployeeName = _context.Employee.FirstOrDefault(c => c.ID == s.EmployeeId).Name,
                    Balance = s.Balance,
                    Credit = s.Credit,
                    Debit = s.Debit,
                    Date = s.Date
                }).OrderBy(x => x.Date.Date).ThenBy(x => x.Id);
            }
            else if (EmployeeBalance.DateTo != null && EmployeeBalance.DateForm == null)
            {
                returnList = dbSet.Where(x => x.EmployeeId == EmployeeBalance.EmployeeId && x.Date.Date <= EmployeeBalance.DateTo.Value.Date && !x.IsDeleted).Select(s => new TransEmployeeVM
                {
                    Id = s.ID,
                    SerialNumber = (int)s.SerialNumber,
                    ShowDate = s.Date.ToString("dd/MM/yyyy"),
                    EmployeeId = s.EmployeeId,
                    EmployeeTypeID = s.EmployeeType,
                    EmployeeType = (Enums.Enums.TransEmployeeTypeEnum)s.EmployeeType,
                    DisplayName = GetDisplayAttribute((Enums.Enums.TransEmployeeTypeEnum)s.EmployeeType),
                    EmployeeName = _context.Employee.FirstOrDefault(c => c.ID == s.EmployeeId).Name,
                    Balance = s.Balance,
                    Credit = s.Credit,
                    Debit = s.Debit,
                    Date = s.Date
                }).OrderBy(x => x.Date.Date).ThenBy(x => x.Id);
            }
            else
            {
                returnList = dbSet.Where(x => x.EmployeeId == EmployeeBalance.EmployeeId && x.Date.Date >= EmployeeBalance.DateForm.Value.Date && x.Date.Date <= EmployeeBalance.DateTo.Value.Date && !x.IsDeleted).Select(s => new TransEmployeeVM
                {
                    Id = s.ID,
                    SerialNumber = (int)s.SerialNumber,
                    ShowDate = s.Date.ToString("dd/MM/yyyy"),
                    EmployeeId = s.EmployeeId,
                    EmployeeTypeID = s.EmployeeType,
                    EmployeeType = (Enums.Enums.TransEmployeeTypeEnum)s.EmployeeType,
                    DisplayName = GetDisplayAttribute((Enums.Enums.TransEmployeeTypeEnum)s.EmployeeType),
                    EmployeeName = _context.Employee.FirstOrDefault(c => c.ID == s.EmployeeId).Name,
                    Balance = s.Balance,
                    Credit = s.Credit,
                    Debit = s.Debit,
                    Date = s.Date
                }).OrderBy(x => x.Date.Date).ThenBy(x => x.Id);
            }

            return returnList;
        }
        public EmpolyeeBalanceReportVM GetCurrentBalance(int EmployeeId, DateTime? DateFrom)
        {
            EmpolyeeBalanceReportVM trans = new EmpolyeeBalanceReportVM();
            trans.EmployeeName = _context.Employee.FirstOrDefault(x => x.ID == EmployeeId).Name;
            var model = dbSet.Where(x => x.EmployeeId == EmployeeId && !x.IsDeleted && x.Date.Date <= DateFrom.Value.Date).OrderByDescending(x => x.ID).FirstOrDefault();
            trans.Balance = DateFrom.HasValue && DateFrom != DateTime.MinValue ? model != null ? model.Balance : 0 : 0;
            return trans;
        }
        public decimal GetBalance(int Employeeid)
        {
            var model = dbSet.Where(x => x.EmployeeId == Employeeid && x.IsDeleted == false).Select(s => new TransEmployeeVM
            {
                Id = s.ID,
                SerialNumber = (int)s.SerialNumber,
                Date = s.Date,
                EmployeeId = s.EmployeeId,
                Balance = s.Balance,
                EmployeeType = (Enums.Enums.TransEmployeeTypeEnum)s.EmployeeType,
                OpenBalFlag = s.OpenBalFlag,
                Credit = s.Credit,
                Debit = s.Debit,
            }).OrderBy(x => x.Date.Date).ThenByDescending(x => x.Id).ToList() ?? new List<TransEmployeeVM>();
            return model.Count > 0 ? model.FirstOrDefault().Balance : 0;
        }
        public decimal GetLatestBalance(int EmployeeId, DateTime Date)
        {
            var model = dbSet.Where(x => x.EmployeeId == EmployeeId && x.Date.Date <= Date.Date && x.IsDeleted == false).Select(s => new TransEmployeeVM
            {
                Id = s.ID,
                Date = s.Date,
                Balance = s.Balance,
            }).OrderByDescending(x => x.Date.Date).ThenByDescending(x => x.Id).ToList() ?? new List<TransEmployeeVM>();
            return model.Count > 0 ? model.FirstOrDefault().Balance : 0;
        }
        public bool Add(TransEmployeeVM Trans, decimal Balance)
        {
            TransEmployeeVM TransModel = dbSet.Where(x => x.EmployeeId == Trans.EmployeeId && x.SerialNumber == Trans.SerialNumber && x.EmployeeType == (int)Trans.EmployeeType && !x.IsDeleted).Select(s => new TransEmployeeVM
            {
                Id = s.ID,
                SerialNumber = (int)s.SerialNumber,
                Date = s.Date,
                EmployeeId = s.EmployeeId,
                EmployeeType = (Enums.Enums.TransEmployeeTypeEnum)s.EmployeeType,
                EmployeeTypeID = s.EmployeeType,
                DisplayName = GetDisplayAttribute((Enums.Enums.TransClientTypeEnum)s.EmployeeType),
                EmployeeName = _context.Employee.FirstOrDefault(c => c.ID == s.EmployeeId).Name,
                Balance = s.Balance,
                Credit = s.Credit,
                Debit = s.Debit,
            }).FirstOrDefault();
            if (TransModel == null)
            {
                var newModel = new TransEmployee
                {
                    SerialNumber = Trans.SerialNumber,
                    Date = Trans.Date.ToLocalTime(),
                    EmployeeId = Trans.EmployeeId,
                    Balance = Trans.Balance,
                    EmployeeType = (int)Trans.EmployeeType,
                    OpenBalFlag = Trans.OpenBalFlag,
                    Credit = Trans.Credit,
                    Debit = Trans.Debit,
                    IsDeleted = false,
                };
                Insert(newModel);
                return false;
            }
            else
            {
                var EmployeeBal = dbSet.Where(x => x.EmployeeId == TransModel.EmployeeId && x.Date.Date <= TransModel.Date.Date && x.IsDeleted == false && x.ID < TransModel.Id).Select(s => new TransEmployeeVM
                {
                    Date = s.Date,
                    Id = s.ID,
                    Balance = s.Balance,
                }).OrderByDescending(x => x.Date.Date).ThenByDescending(x => x.Id).ToList();
                decimal BeforeBalance = EmployeeBal != null && EmployeeBal.Count > 0 ? EmployeeBal.FirstOrDefault().Balance : 0;
                TransModel.Balance = TransModel.Credit > 0 ? BeforeBalance == 0 ? Balance : BeforeBalance - Balance : BeforeBalance == 0 ? Balance : BeforeBalance + Balance;
                if (!Trans.OpenBalFlag)
                {
                    if (TransModel.Credit > 0)
                        TransModel.Credit = Balance;
                    else
                        TransModel.Debit = Balance;
                }
                TransModel.Date = Trans.Date.ToLocalTime();
                Edit(TransModel);
                return false;
            }
        }
        public TransEmployee Edit(TransEmployeeVM s)
        {
            TransEmployee edited = dbSet.FirstOrDefault(p => p.EmployeeId == s.EmployeeId && p.SerialNumber == s.SerialNumber && p.EmployeeType == (int)s.EmployeeType && p.IsDeleted == false);
            edited.Date = s.Date.ToLocalTime();
            edited.Balance = s.Balance;
            edited.Credit = s.Credit;
            edited.Debit = s.Debit;
            //edited.IsDeleted = false;
            Update(edited);
            return edited;
        }
        public TransEmployee EditAfterBalance(TransEmployeeVM s)
        {
            TransEmployee edited = dbSet.FirstOrDefault(p => p.EmployeeId == s.EmployeeId && p.SerialNumber == s.SerialNumber && p.EmployeeType == (int)s.EmployeeType && p.IsDeleted == false);
            edited.Balance = s.Balance;
            Update(edited);
            return edited;
        }
        public void Delete(int id)
        {
            TransEmployee edited = dbSet.FirstOrDefault(p => p.ID == id && p.IsDeleted == false);
            if (edited != null)
            {
                edited.IsDeleted = true;
                Delete(edited);
            }
        }

        public int RecalculateEmployeeBalance(int Employeeid, int BillSerialNumber)
        {
            var DeletedBalance = dbSet.Where(x => x.EmployeeId == Employeeid && x.SerialNumber == BillSerialNumber && x.IsDeleted == false).Select(s => new TransEmployeeVM
            {
                Id = s.ID,
                // SerialNumber = s.SerialNumber,
                Date = s.Date,
                //EmployeeId = s.EmployeeId,
                Balance = s.Balance,
                //EmployeeType = (Enums.Enums.TransEmployeeTypeEnum)s.EmployeeType,
                //OpenBalFlag = s.OpenBalFlag,
                Credit = s.Credit,
                Debit = s.Debit,
            }).FirstOrDefault();
            if (DeletedBalance != null)
            {
                var BeforeBalanceTemp = dbSet.Where(x => x.EmployeeId == Employeeid && x.ID < DeletedBalance.Id && x.Date.Date < DeletedBalance.Date.Date && x.IsDeleted == false).Select(s => new TransEmployeeVM
                {
                    Id = s.ID,
                    Balance = s.Balance,
                    Date = s.Date,
                }).OrderByDescending(x => x.Date.Date).OrderByDescending(x => x.Id).FirstOrDefault();
                decimal BeforeBalance = 0;
                if (BeforeBalanceTemp != null)
                {
                    BeforeBalance = BeforeBalanceTemp.Balance;

                }//&& x.ID > DeletedBalance.Id
                var SameDateBalanceList = dbSet.Where(x => x.EmployeeId == Employeeid && x.Date.Date == DeletedBalance.Date.Date && x.IsDeleted == false && x.OpenBalFlag == false).Select(s => new TransEmployeeVM
                {
                    Id = s.ID,
                    SerialNumber = (int)s.SerialNumber,
                    Date = s.Date,
                    EmployeeId = s.EmployeeId,
                    Balance = s.Balance,
                    EmployeeType = (Enums.Enums.TransEmployeeTypeEnum)s.EmployeeType,
                    OpenBalFlag = s.OpenBalFlag,
                    Credit = s.Credit,
                    Debit = s.Debit,
                }).OrderBy(x => x.Date.Date).OrderBy(x => x.Id).ToList();
                if (SameDateBalanceList != null && SameDateBalanceList.Count > 0)
                {
                    foreach (var BalanceModel in SameDateBalanceList)
                    {
                        if (BalanceModel.Id != DeletedBalance.Id)
                        {
                            BalanceModel.Balance = BalanceModel.Credit > 0 ? BeforeBalance - BalanceModel.Credit : BeforeBalance + BalanceModel.Debit;
                            BeforeBalance = BalanceModel.Balance;
                            EditAfterBalance(BalanceModel);
                        }
                    }
                }
                //missed current balance
                BeforeBalance = DeletedBalance.Balance;
                var AfterBalanceList = dbSet.Where(x => x.EmployeeId == Employeeid && x.Date.Date > DeletedBalance.Date.Date && x.IsDeleted == false && x.OpenBalFlag == false).Select(s => new TransEmployeeVM
                {
                    Id = s.ID,
                    SerialNumber = (int)s.SerialNumber,
                    Date = s.Date,
                    EmployeeId = s.EmployeeId,
                    Balance = s.Balance,
                    EmployeeType = (Enums.Enums.TransEmployeeTypeEnum)s.EmployeeType,
                    OpenBalFlag = s.OpenBalFlag,
                    Credit = s.Credit,
                    Debit = s.Debit,
                }).OrderBy(x => x.Date.Date).OrderBy(x => x.Id).ToList();
                if (AfterBalanceList != null && AfterBalanceList.Count > 0)
                {
                    foreach (var BalanceModel in AfterBalanceList)
                    {
                        if (BalanceModel.Id != DeletedBalance.Id)
                        {
                            BalanceModel.Balance = BalanceModel.Credit > 0 ? BeforeBalance - BalanceModel.Credit : BeforeBalance + BalanceModel.Debit;
                            BeforeBalance = BalanceModel.Balance;
                            EditAfterBalance(BalanceModel);
                        }
                    }
                }
                return DeletedBalance.Id;
            }
            else
            {
                return 0;
            }
        }
        
        public void DeletePhysical(int EmployeeId, int Id, int Type)
        {
            TransEmployee DeleteModel = dbSet.FirstOrDefault(p => p.EmployeeId == EmployeeId && p.SerialNumber == Id && p.EmployeeType == Type && p.IsDeleted == false);
            if (DeleteModel != null)
                Remove(DeleteModel);
        }
        public void UpdateEmployeeBalance(int Employeeid)
        {
            List<TransEmployeeVM> EmployeeBalanceList = dbSet.Where(x => x.EmployeeId == Employeeid && x.IsDeleted == false).Select(s => new TransEmployeeVM
            {
                Id = s.ID,
                SerialNumber = (int)s.SerialNumber,
                Date = s.Date.Date,
                EmployeeId = s.EmployeeId,
                Balance = s.Balance,
                EmployeeType = (Enums.Enums.TransEmployeeTypeEnum)s.EmployeeType,
                OpenBalFlag = s.OpenBalFlag,
                Credit = s.Credit,
                Debit = s.Debit,
            }).OrderBy(x => x.Date.Date).ThenBy(x => x.Id).ToList();
            decimal BeforeBalance = 0;
            var tempBeforBalance = EmployeeBalanceList.Where(x => x.OpenBalFlag);
            if (tempBeforBalance.Count() != 0)
            {
                BeforeBalance = tempBeforBalance.FirstOrDefault().Balance;
            }
            foreach (var trans in EmployeeBalanceList)
            {
                TransEmployeeVM transEmployee = new TransEmployeeVM();

                if (trans.OpenBalFlag)
                {
                    transEmployee = dbSet.Where(x => x.ID == trans.Id && x.IsDeleted == false).Select(s => new TransEmployeeVM
                    {
                        Id = s.ID,
                        SerialNumber = (int)s.SerialNumber,
                        Date = s.Date.Date,
                        EmployeeId = s.EmployeeId,
                        EmployeeType = (Enums.Enums.TransEmployeeTypeEnum)s.EmployeeType,
                        Debit = 0,
                        Credit = 0,
                        Balance = trans.Balance,
                        OpenBalFlag = true,
                    }).FirstOrDefault();
                    Edit(transEmployee);
                }
                else
                {
                    transEmployee = dbSet.Where(x => x.ID == trans.Id && x.IsDeleted == false).Select(s => new TransEmployeeVM
                    {
                        Id = s.ID,
                        SerialNumber = (int)s.SerialNumber,
                        Date = s.Date,
                        EmployeeId = s.EmployeeId,
                        EmployeeType = (Enums.Enums.TransEmployeeTypeEnum)s.EmployeeType,
                        Debit = s.Debit,
                        Credit = s.Credit,
                        Balance = trans.Credit > 0 ? BeforeBalance - trans.Credit : BeforeBalance + trans.Debit,
                        OpenBalFlag = false,
                    }).FirstOrDefault();
                    EditAfterBalance(transEmployee);
                }
                BeforeBalance = transEmployee.Balance;
            }
        }
    }
}