﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Trans;
using MasProject.Domain.ViewModel.TransVM;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using static MasProject.Domain.Enums.Enums;
namespace MasProject.Domain.Services.TransServices
{
    public class TransClientService : Repository<TransClient>
    {
        private DBContext _context;
        public TransClientService(DBContext context) : base(context)
        {
            _context = context;
        }

        private static string GetDisplayAttribute(object value)
        {
            Type type = value.GetType();
            var field = type.GetField(value.ToString());
            return field == null ? null : field.GetCustomAttribute<DisplayAttribute>().GetName();
        }
        public ClientBalanceReportVM GetBill(int SerialNumber, int Type)
        {
            ClientBalanceReportVM trans = new ClientBalanceReportVM();
            switch (Type)
            {
                case (int)TransClientTypeEnum.رصيد_اول_المدة:
                    trans.PageName = "ClientOpeningBalanceEdit";
                    trans.BillId = SerialNumber;
                    break;
                case (int)TransClientTypeEnum.دفعات_عملاء:
                    trans.PageName = "ClientPaymentEdit";
                    trans.BillId = _context.ClientPayment.FirstOrDefault(x => x.SerialNumber == SerialNumber).ID;
                    break;
                case (int)TransClientTypeEnum.تسوية_إشعار_خصم:
                    trans.PageName = "DiscountNoticeEdit";
                    trans.BillId = _context.DiscountNotice.FirstOrDefault(x => x.SerialNumber == SerialNumber).ID;
                    break;
                case (int)TransClientTypeEnum.مردودات_مبيعات:
                    trans.PageName = "SalesReturnsEdit";
                    trans.BillId = _context.SalesReturns.FirstOrDefault(x => x.SerialNumber == SerialNumber).ID;
                    break;
                case (int)TransClientTypeEnum.مبيعات:
                    trans.PageName = "SalesBillEdit";
                    trans.BillId = _context.SalesBills.FirstOrDefault(x => x.SerialNumber == SerialNumber).ID;
                    break;
                case (int)TransClientTypeEnum.صيانة:
                    trans.PageName = "SalesMaintanceEdit";
                    trans.BillId = _context.MantienceBills.FirstOrDefault(x => x.SerialNumber == SerialNumber).ID;
                    break;
                case (int)TransClientTypeEnum.تنفيذ:
                    trans.PageName = "SalesExecutiveEdit";
                    trans.BillId = _context.ExecutiveBills.FirstOrDefault(x => x.SerialNumber == SerialNumber).ID;
                    break;
            }
            return trans;
        }
        public IEnumerable<TransClientVM> GetClientBalance(ClientBalanceReportVM clientBalance)
        {
            IEnumerable<TransClientVM> returnList;
            if (clientBalance.DateTo == DateTime.MinValue && clientBalance.DateForm == DateTime.MinValue)
            {
                returnList = dbSet.Where(x => x.ClientId == clientBalance.ClientId && !x.IsDeleted).Select(s => new TransClientVM
                {
                    Id = s.ID,
                    SerialNumber = (int)s.SerialNumber,
                    ShowDate = s.Date.ToString("dd/MM/yyyy"),
                    ClientId = s.ClientId,
                    ClientType = (Enums.Enums.TransClientTypeEnum)s.ClientType,
                    ClientTypeID = s.ClientType,
                    DisplayName = GetDisplayAttribute((Enums.Enums.TransClientTypeEnum)s.ClientType),
                    ClientName = _context.Client.FirstOrDefault(c => c.ID == s.ClientId).Name,
                    Balance = s.Balance,
                    Credit = s.Credit,
                    Debit = s.Debit,
                    Date = s.Date
                }).OrderBy(x => x.Date.Date).ThenBy(x => x.Id);
            }
            else if (clientBalance.DateTo == DateTime.MinValue && clientBalance.DateForm != DateTime.MinValue)
            {
                returnList = dbSet.Where(x => x.ClientId == clientBalance.ClientId && x.Date.Date == clientBalance.DateForm.Value.Date && !x.IsDeleted).Select(s => new TransClientVM
                {
                    Id = s.ID,
                    SerialNumber = (int)s.SerialNumber,
                    ShowDate = s.Date.ToString("dd/MM/yyyy"),
                    ClientId = s.ClientId,
                    ClientTypeID = s.ClientType,
                    ClientType = (Enums.Enums.TransClientTypeEnum)s.ClientType,
                    DisplayName = GetDisplayAttribute((Enums.Enums.TransClientTypeEnum)s.ClientType),
                    ClientName = _context.Client.FirstOrDefault(c => c.ID == s.ClientId).Name,
                    Balance = s.Balance,
                    Credit = s.Credit,
                    Debit = s.Debit,
                    Date = s.Date
                }).OrderBy(x => x.Date.Date).ThenBy(x => x.Id);
            }
            else if (clientBalance.DateTo != null && clientBalance.DateForm == null)
            {
                returnList = dbSet.Where(x => x.ClientId == clientBalance.ClientId && x.Date.Date <= clientBalance.DateTo.Value.Date && !x.IsDeleted).Select(s => new TransClientVM
                {
                    Id = s.ID,
                    SerialNumber = (int)s.SerialNumber,
                    ShowDate = s.Date.ToString("dd/MM/yyyy"),
                    ClientId = s.ClientId,
                    ClientTypeID = s.ClientType,
                    ClientType = (Enums.Enums.TransClientTypeEnum)s.ClientType,
                    DisplayName = GetDisplayAttribute((Enums.Enums.TransClientTypeEnum)s.ClientType),
                    ClientName = _context.Client.FirstOrDefault(c => c.ID == s.ClientId).Name,
                    Balance = s.Balance,
                    Credit = s.Credit,
                    Debit = s.Debit,
                    Date = s.Date
                }).OrderBy(x => x.Date.Date).ThenBy(x => x.Id);
            }
            else
            {
                returnList = dbSet.Where(x => x.ClientId == clientBalance.ClientId && x.Date.Date >= clientBalance.DateForm.Value.Date && x.Date.Date <= clientBalance.DateTo.Value.Date && !x.IsDeleted).Select(s => new TransClientVM
                {
                    Id = s.ID,
                    SerialNumber = (int)s.SerialNumber,
                    ShowDate = s.Date.ToString("dd/MM/yyyy"),
                    ClientId = s.ClientId,
                    ClientTypeID = s.ClientType,
                    ClientType = (Enums.Enums.TransClientTypeEnum)s.ClientType,
                    DisplayName = GetDisplayAttribute((Enums.Enums.TransClientTypeEnum)s.ClientType),
                    ClientName = _context.Client.FirstOrDefault(c => c.ID == s.ClientId).Name,
                    Balance = s.Balance,
                    Credit = s.Credit,
                    Debit = s.Debit,
                    Date = s.Date,
                }).OrderBy(x => x.Date.Date).ThenBy(x => x.Id);
            }
            return returnList;
        }
        public ClientBalanceReportVM GetCurrentBalance(int ClientId, DateTime? DateFrom)
        {
            ClientBalanceReportVM trans = new ClientBalanceReportVM();
            trans.ClientName = _context.Client.FirstOrDefault(x => x.ID == ClientId).Name;
            var model = dbSet.Where(x => x.ClientId == ClientId && !x.IsDeleted && x.Date.Date <= DateFrom.Value.Date).OrderByDescending(x => x.ID).FirstOrDefault();
            trans.Balance = DateFrom.HasValue && DateFrom != DateTime.MinValue ? model != null ? model.Balance : 0 : 0;
            return trans;
        }
        public decimal GetBalance(int Clientid)
        {
            var model = dbSet.Where(x => x.ClientId == Clientid && x.IsDeleted == false).Select(s => new TransClientVM
            {
                Id = s.ID,
                SerialNumber = (int)s.SerialNumber,
                Date = s.Date,
                ClientId = s.ClientId,
                Balance = s.Balance,
                ClientType = (Enums.Enums.TransClientTypeEnum)s.ClientType,
                OpenBalFlag = s.OpenBalFlag,
                Credit = s.Credit,
                Debit = s.Debit,
            }).OrderBy(x => x.Date.Date).ThenByDescending(x => x.Id).ToList() ?? new List<TransClientVM>();
            return model.Count > 0 ? model.FirstOrDefault().Balance : 0;
        }
        public decimal GetLatestBalance(int Clientid, DateTime Date)
        {
            var model = dbSet.Where(x => x.ClientId == Clientid && x.Date.Date <= Date.Date && x.IsDeleted == false).Select(s => new TransClientVM
            {
                Id = s.ID,
                Date = s.Date,
                Balance = s.Balance,
            }).OrderByDescending(x => x.Date.Date).ThenByDescending(x => x.Id).ToList() ?? new List<TransClientVM>();
            return model.Count > 0 ? model.FirstOrDefault().Balance : 0;
        }
        public bool Add(TransClientVM Trans, decimal Balance)
        {
            TransClientVM TransModel = dbSet.Where(x => x.ClientId == Trans.ClientId && x.SerialNumber == Trans.SerialNumber && x.ClientType == (int)Trans.ClientType && !x.IsDeleted).Select(s => new TransClientVM
            {
                Id = s.ID,
                SerialNumber = (int)s.SerialNumber,
                Date = s.Date,
                ClientId = s.ClientId,
                ClientType = (Enums.Enums.TransClientTypeEnum)s.ClientType,
                ClientTypeID = s.ClientType,
                DisplayName = GetDisplayAttribute((Enums.Enums.TransClientTypeEnum)s.ClientType),
                ClientName = _context.Client.FirstOrDefault(c => c.ID == s.ClientId).Name,
                Balance = s.Balance,
                Credit = s.Credit,
                Debit = s.Debit,
            }).FirstOrDefault();
            if (TransModel == null)
            {
                var newModel = new TransClient
                {
                    SerialNumber = Trans.SerialNumber,
                    Date = Trans.Date.ToLocalTime(),
                    ClientId = Trans.ClientId,
                    Balance = Trans.Balance,
                    ClientType = (int)Trans.ClientType,
                    OpenBalFlag = Trans.OpenBalFlag,
                    Credit = Trans.Credit,
                    Debit = Trans.Debit,
                    IsDeleted = false,
                };
                Insert(newModel);
                return false;

            }
            else
            {
                var clientBal = dbSet.Where(x => x.ClientId == TransModel.ClientId && x.Date.Date <= TransModel.Date.Date && x.IsDeleted == false && x.ID < TransModel.Id).Select(s => new TransClientVM
                {
                    Date = s.Date,
                    Id = s.ID,
                    Balance = s.Balance,
                }).OrderByDescending(x => x.Date.Date).ThenByDescending(x => x.Id).ToList();
                decimal BeforeBalance = clientBal != null && clientBal.Count > 0 ? clientBal.FirstOrDefault().Balance : 0;
                TransModel.Balance = TransModel.Credit > 0 ? BeforeBalance == 0 ? Balance : BeforeBalance - Balance : BeforeBalance == 0 ? Balance : BeforeBalance + Balance;
                if (!Trans.OpenBalFlag)
                {
                    if (TransModel.Credit > 0)
                        TransModel.Credit = Balance;
                    else
                        TransModel.Debit = Balance;
                }
                TransModel.Date = Trans.Date.ToLocalTime();
                Edit(TransModel);
                return false;
            }
        }
        public TransClient Edit(TransClientVM s)
        {
            TransClient edited = dbSet.FirstOrDefault(p => p.ClientId == s.ClientId && p.SerialNumber == s.SerialNumber && p.ClientType == (int)s.ClientType && p.IsDeleted == false);
            edited.Date = s.Date.ToLocalTime();
            edited.Balance = s.Balance;
            edited.Credit = s.Credit;
            edited.Debit = s.Debit;
            //edited.IsDeleted = false;//not allowed to change 
            Update(edited);
            return edited;
        }
        public TransClient EditAfterBalance(TransClientVM s)
        {
            TransClient edited = dbSet.FirstOrDefault(p => p.ClientId == s.ClientId && p.SerialNumber == s.SerialNumber && p.ClientType == (int)s.ClientType && p.IsDeleted == false);
            edited.Balance = s.Balance;
            Update(edited);
            return edited;
        }
        public void Delete(int id)
        {
            TransClient edited = dbSet.FirstOrDefault(p => p.ID == id && p.IsDeleted == false);
            if (edited != null)
            {
                edited.IsDeleted = true;
                Delete(edited);
            }
        }


        public int RecalculateClientBalance(int Clientid, int BillSerialNumber)//
        {
            var DeletedBalance = dbSet.Where(x => x.ClientId == Clientid && x.SerialNumber == BillSerialNumber && x.IsDeleted == false).Select(s => new TransClientVM
            {
                Id = s.ID,
                //SerialNumber = s.SerialNumber,
                Date = s.Date,
                //ClientId = s.ClientId,
                Balance = s.Balance,
                //ClientType = (Enums.Enums.TransClientTypeEnum)s.ClientType,
                //OpenBalFlag = s.OpenBalFlag,
                Credit = s.Credit,
                Debit = s.Debit,
            }).FirstOrDefault();
            if (DeletedBalance != null)
            {
                var BeforeBalanceTemp = dbSet.Where(x => x.ClientId == Clientid && x.ID < DeletedBalance.Id && x.Date.Date < DeletedBalance.Date.Date && x.IsDeleted == false).Select(s => new TransClientVM
                {
                    Id = s.ID,
                    Balance = s.Balance,
                    Date = s.Date,
                }).OrderByDescending(x => x.Date.Date).OrderByDescending(x => x.Id).FirstOrDefault();
                decimal BeforeBalance = 0;
                if (BeforeBalanceTemp != null)
                {
                    BeforeBalance = BeforeBalanceTemp.Balance;
                }//&& x.ID > DeletedBalance.Id
                var SameDateBalanceList = dbSet.Where(x => x.ClientId == Clientid && x.Date.Date == DeletedBalance.Date.Date && x.IsDeleted == false && x.OpenBalFlag == false).Select(s => new TransClientVM
                {
                    Id = s.ID,
                    SerialNumber = (int)s.SerialNumber,
                    Date = s.Date,
                    ClientId = s.ClientId,
                    Balance = s.Balance,
                    ClientType = (Enums.Enums.TransClientTypeEnum)s.ClientType,
                    OpenBalFlag = s.OpenBalFlag,
                    Credit = s.Credit,
                    Debit = s.Debit,
                }).OrderBy(x => x.Date.Date).OrderBy(x => x.Id).ToList();
                if (SameDateBalanceList != null && SameDateBalanceList.Count > 0)
                {
                    foreach (var BalanceModel in SameDateBalanceList)
                    {
                        if (BalanceModel.Id != DeletedBalance.Id)
                        {
                            BalanceModel.Balance = BalanceModel.Credit > 0 ? BeforeBalance - BalanceModel.Credit : BeforeBalance + BalanceModel.Debit;
                            BeforeBalance = BalanceModel.Balance;
                            EditAfterBalance(BalanceModel);
                        }
                    }
                }
                //missed current balance
                BeforeBalance = DeletedBalance.Balance;
                var AfterBalanceList = dbSet.Where(x => x.ClientId == Clientid && x.Date.Date > DeletedBalance.Date.Date && x.IsDeleted == false && x.OpenBalFlag == false).Select(s => new TransClientVM
                {
                    Id = s.ID,
                    SerialNumber = (int)s.SerialNumber,
                    Date = s.Date,
                    ClientId = s.ClientId,
                    Balance = s.Balance,
                    ClientType = (Enums.Enums.TransClientTypeEnum)s.ClientType,
                    OpenBalFlag = s.OpenBalFlag,
                    Credit = s.Credit,
                    Debit = s.Debit,
                }).OrderBy(x => x.Date.Date).OrderBy(x => x.Id).ToList();
                if (AfterBalanceList != null && AfterBalanceList.Count > 0)
                {
                    foreach (var BalanceModel in AfterBalanceList)
                    {
                        if (BalanceModel.Id != DeletedBalance.Id)
                        {
                            BalanceModel.Balance = BalanceModel.Credit > 0 ? BeforeBalance - BalanceModel.Credit : BeforeBalance + BalanceModel.Debit;
                            BeforeBalance = BalanceModel.Balance;
                            EditAfterBalance(BalanceModel);
                        }
                    }
                }
                return DeletedBalance.Id;
            }
            else
            {
                return 0;
            }
        }

      

        public void DeletePhysical(int ClientId, int Id, int Type)
        {
            TransClient DeleteModel = dbSet.FirstOrDefault(p => p.ClientId == ClientId && p.SerialNumber == Id && p.ClientType == Type && p.IsDeleted == false);
            if (DeleteModel != null)
                Remove(DeleteModel);
        }

        public void UpdateClientBalance(int Clientid)
        {
            List<TransClientVM> ClientBalanceList = dbSet.Where(x => x.ClientId == Clientid && x.IsDeleted == false).Select(s => new TransClientVM
            {
                Id = s.ID,
                SerialNumber = (int)s.SerialNumber,
                Date = s.Date.Date,
                ClientId = s.ClientId,
                Balance = s.Balance,
                ClientType = (Enums.Enums.TransClientTypeEnum)s.ClientType,
                OpenBalFlag = s.OpenBalFlag,
                Credit = s.Credit,
                Debit = s.Debit,
            }).OrderBy(x => x.Date.Date).ThenBy(x => x.Id).ToList();
            decimal BeforeBalance = 0;
            var tempBeforBalance = ClientBalanceList.Where(x => x.OpenBalFlag);
            if (tempBeforBalance.Count() != 0)
            {
                BeforeBalance = tempBeforBalance.FirstOrDefault().Balance;
            }
            foreach (var trans in ClientBalanceList)
            {
                TransClientVM transClient = new TransClientVM();

                if (trans.OpenBalFlag)
                {
                    transClient = dbSet.Where(x => x.ID == trans.Id && x.IsDeleted == false).Select(s => new TransClientVM
                    {
                        Id = s.ID,
                        SerialNumber = (int)s.SerialNumber,
                        Date = s.Date.Date,
                        ClientId = s.ClientId,
                        ClientType = (Enums.Enums.TransClientTypeEnum)s.ClientType,
                        Debit = 0,
                        Credit = 0,
                        Balance = trans.Balance,
                        OpenBalFlag = true,
                    }).FirstOrDefault();
                    Edit(transClient);
                }
                else
                {
                    transClient = dbSet.Where(x => x.ID == trans.Id && x.IsDeleted == false).Select(s => new TransClientVM
                    {
                        Id = s.ID,
                        SerialNumber = (int)s.SerialNumber,
                        Date = s.Date,
                        ClientId = s.ClientId,
                        ClientType = (Enums.Enums.TransClientTypeEnum)s.ClientType,
                        Debit = s.Debit,
                        Credit = s.Credit,
                        Balance = trans.Credit > 0 ? BeforeBalance - trans.Credit : BeforeBalance + trans.Debit,
                        OpenBalFlag = false,
                    }).FirstOrDefault();
                    EditAfterBalance(transClient);
                }
                BeforeBalance = transClient.Balance;
            }
        }

        //for update serial 
        public TransClientVM GetForEdit(int Clientid, int BillSerialNumber,  int ClientType) 
        {
            return   dbSet.Where(x => x.ClientId == Clientid && x.SerialNumber == BillSerialNumber && x.IsDeleted == false && x.ClientType==ClientType).Select(s => new TransClientVM
            {
                Id = s.ID,
                SerialNumber = (int)s.SerialNumber,
                Date = s.Date,
                ClientId = s.ClientId,
                Balance = s.Balance,
                ClientType = (Enums.Enums.TransClientTypeEnum)s.ClientType,
                OpenBalFlag = s.OpenBalFlag,
                Credit = s.Credit,
                Debit = s.Debit,
            }).FirstOrDefault();
        }
        public void EditForEdit(TransClientVM s)
        {
            TransClient edited = dbSet.FirstOrDefault(p => p.ID == s.Id);
            edited.SerialNumber = s.SerialNumber;
            Update(edited);
        }
    }
}
