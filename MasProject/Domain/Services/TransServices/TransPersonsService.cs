﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Trans;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.Text;

namespace MasProject.Domain.Services.TransServices
{
  public  class TransPersonsService : Repository<TransClient>
    {
        private DBContext _context;
        public TransPersonsService(DBContext context) : base(context)
        {
            _context = context;
        }
        private static string GetDisplayAttribute(object value)
        {
            Type type = value.GetType();
            var field = type.GetField(value.ToString());
            return field == null ? null : field.GetCustomAttribute<DisplayAttribute>().GetName();
        }



    }
}
