﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.TransTables;
using MasProject.Domain.ViewModel.TransVM;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using static MasProject.Domain.Enums.Enums;

namespace MasProject.Domain.Services.TransServices
{
    public class TransSupplierService : Repository<TransSupplier>
    {
        private DBContext _context;
        public TransSupplierService(DBContext context) : base(context)
        {
            _context = context;
        }
        private static string GetDisplayAttribute(object value)
        {
            Type type = value.GetType();
            var field = type.GetField(value.ToString());
            return field == null ? null : field.GetCustomAttribute<DisplayAttribute>().GetName();
        }
        public SupplierBalanceReportVM GetBill(int SerialNumber, int Type)
        {
            SupplierBalanceReportVM trans = new SupplierBalanceReportVM();
            switch (Type)
            {
                case (int)TransSupplierTypeEnum.رصيد_اول_المدة:
                    trans.PageName = "SupplierOpeningBalanceEdit";
                    trans.BillId = SerialNumber;
                    break;
                case (int)TransSupplierTypeEnum.دفعات_موردين:
                    trans.PageName = "SupplierPaymentEdit";
                    trans.BillId = _context.SupplierPayment.FirstOrDefault(x => x.SerialNumber == SerialNumber).ID;
                    break;
                case (int)TransSupplierTypeEnum.إشعار_إضافة_موردين:
                    trans.PageName = "AdditionNoticeEdit";
                    trans.BillId = _context.AdditionNotice.FirstOrDefault(x => x.SerialNumber == SerialNumber).ID;
                    break;
                case (int)TransSupplierTypeEnum.مشتريات:
                    trans.PageName = "PurchasesBillEdit";
                    trans.BillId = _context.PurchaseBill.FirstOrDefault(x => x.SerialNumber == SerialNumber).ID;
                    break;
                case (int)TransSupplierTypeEnum.مردودات_مشتريات:
                    trans.PageName = "PurchasesReturnsEdit";
                    trans.BillId = _context.PurchasesReturns_Bill.FirstOrDefault(x => x.SerialNumber == SerialNumber).ID;
                    break;
            }
            return trans;
        }
        public IEnumerable<TransSupplierVM> GetSupplierBalance(SupplierBalanceReportVM SupplierBalance)
        {
            IEnumerable<TransSupplierVM> returnList;
            if (SupplierBalance.DateTo == DateTime.MinValue && SupplierBalance.DateForm == DateTime.MinValue)
            {
                returnList = dbSet.Where(x => x.SupplierId == SupplierBalance.SupplierId && !x.IsDeleted).Select(s => new TransSupplierVM
                {
                    Id = s.ID,
                    SerialNumber = (int)s.SerialNumber,
                    ShowDate = s.Date.ToString("dd/MM/yyyy"),
                    SupplierId = s.SupplierId,
                    SupplierType = (Enums.Enums.TransSupplierTypeEnum)s.SupplierType,
                    SupplierTypeID = s.SupplierType,
                    DisplayName = GetDisplayAttribute((Enums.Enums.TransSupplierTypeEnum)s.SupplierType),
                    SupplierName = _context.Supplier.FirstOrDefault(c => c.ID == s.SupplierId).Name,
                    Balance = s.Balance,
                    Credit = s.Credit,
                    Debit = s.Debit,
                    Date = s.Date
                }).OrderBy(x => x.Date.Date).ThenBy(x => x.Id);
            }
            else if (SupplierBalance.DateTo == DateTime.MinValue && SupplierBalance.DateForm != DateTime.MinValue)
            {
                returnList = dbSet.Where(x => x.SupplierId == SupplierBalance.SupplierId && x.Date.Date == SupplierBalance.DateForm.Value.Date & !x.IsDeleted).Select(s => new TransSupplierVM
                {
                    Id = s.ID,
                    SerialNumber = (int)s.SerialNumber,
                    ShowDate = s.Date.ToString("dd/MM/yyyy"),
                    SupplierId = s.SupplierId,
                    SupplierTypeID = s.SupplierType,
                    SupplierType = (Enums.Enums.TransSupplierTypeEnum)s.SupplierType,
                    DisplayName = GetDisplayAttribute((Enums.Enums.TransSupplierTypeEnum)s.SupplierType),
                    SupplierName = _context.Supplier.FirstOrDefault(c => c.ID == s.SupplierId).Name,
                    Balance = s.Balance,
                    Credit = s.Credit,
                    Debit = s.Debit,
                    Date = s.Date
                }).OrderBy(x => x.Date.Date).ThenBy(x => x.Id);
            }
            else if (SupplierBalance.DateTo != null && SupplierBalance.DateForm == null)
            {
                returnList = dbSet.Where(x => x.SupplierId == SupplierBalance.SupplierId && x.Date.Date <= SupplierBalance.DateTo.Value.Date && !x.IsDeleted).Select(s => new TransSupplierVM
                {
                    Id = s.ID,
                    SerialNumber = (int)s.SerialNumber,
                    ShowDate = s.Date.ToString("dd/MM/yyyy"),
                    SupplierId = s.SupplierId,
                    SupplierTypeID = s.SupplierType,
                    SupplierType = (Enums.Enums.TransSupplierTypeEnum)s.SupplierType,
                    DisplayName = GetDisplayAttribute((Enums.Enums.TransSupplierTypeEnum)s.SupplierType),
                    SupplierName = _context.Supplier.FirstOrDefault(c => c.ID == s.SupplierId).Name,
                    Balance = s.Balance,
                    Credit = s.Credit,
                    Debit = s.Debit,
                    Date = s.Date
                }).OrderBy(x => x.Date.Date).ThenBy(x => x.Id);
            }
            else
            {
                returnList = dbSet.Where(x => x.SupplierId == SupplierBalance.SupplierId && x.Date.Date >= SupplierBalance.DateForm.Value.Date && x.Date.Date <= SupplierBalance.DateTo.Value.Date && !x.IsDeleted).Select(s => new TransSupplierVM
                {
                    Id = s.ID,
                    SerialNumber = (int)s.SerialNumber,
                    ShowDate = s.Date.ToString("dd/MM/yyyy"),
                    SupplierId = s.SupplierId,
                    SupplierTypeID = s.SupplierType,
                    SupplierType = (Enums.Enums.TransSupplierTypeEnum)s.SupplierType,
                    DisplayName = GetDisplayAttribute((Enums.Enums.TransSupplierTypeEnum)s.SupplierType),
                    SupplierName = _context.Supplier.FirstOrDefault(c => c.ID == s.SupplierId).Name,
                    Balance = s.Balance,
                    Credit = s.Credit,
                    Debit = s.Debit,
                    Date = s.Date
                }).OrderBy(x => x.Date.Date).ThenBy(x => x.Id);
            }
            return returnList;
        }
        public SupplierBalanceReportVM GetCurrentBalance(int SupplierId, DateTime? DateFrom)
        {
            SupplierBalanceReportVM trans = new SupplierBalanceReportVM();
            trans.SupplierName = _context.Supplier.FirstOrDefault(x => x.ID == SupplierId).Name;
            var model = dbSet.Where(x => x.SupplierId == SupplierId && !x.IsDeleted && x.Date.Date <= DateFrom.Value.ToLocalTime().Date).OrderByDescending(x => x.ID).FirstOrDefault();
            trans.Balance = DateFrom.HasValue && DateFrom != DateTime.MinValue ? model != null ? model.Balance : 0 : 0;
            return trans;
        }

        public decimal GetBalance(int Supplierid)
        {
            var model = dbSet.Where(x => x.SupplierId == Supplierid && x.IsDeleted == false).Select(s => new TransSupplierVM
            {
                Id = s.ID,
                SerialNumber = (int)s.SerialNumber,
                Date = s.Date,
                SupplierId = s.SupplierId,
                Balance = s.Balance,
                SupplierType = (Enums.Enums.TransSupplierTypeEnum)s.SupplierType,
                OpenBalFlag = s.OpenBalFlag,
                Credit = s.Credit,
                Debit = s.Debit,
            }).OrderBy(x => x.Date.Date).ThenByDescending(x => x.Id).ToList() ?? new List<TransSupplierVM>();
            return model.Count > 0 ? model.FirstOrDefault().Balance : 0;
        }
        public decimal GetLatestBalance(int SupplierId, DateTime Date)
        {
            var model = dbSet.Where(x => x.SupplierId == SupplierId && x.Date.Date <= Date.Date && x.IsDeleted == false).Select(s => new TransSupplierVM
            {
                Id = s.ID,
                Date = s.Date,
                Balance = s.Balance,
            }).OrderByDescending(x => x.Date.Date).ThenByDescending(x => x.Id).ToList() ?? new List<TransSupplierVM>();
            return model.Count > 0 ? model.FirstOrDefault().Balance : 0;
        }
        public bool Add(TransSupplierVM Trans, decimal Balance)
        {
            TransSupplierVM TransModel = dbSet.Where(x => x.SupplierId == Trans.SupplierId && x.SerialNumber == Trans.SerialNumber && x.SupplierType == (int)Trans.SupplierType && !x.IsDeleted).Select(s => new TransSupplierVM
            {
                Id = s.ID,
                SerialNumber = (int)s.SerialNumber,
                Date = s.Date,
                SupplierId = s.SupplierId,
                SupplierType = (Enums.Enums.TransSupplierTypeEnum)s.SupplierType,
                SupplierTypeID = s.SupplierType,
                DisplayName = GetDisplayAttribute((Enums.Enums.TransSupplierTypeEnum)s.SupplierType),
                SupplierName = _context.Supplier.FirstOrDefault(c => c.ID == s.SupplierId).Name,
                Balance = s.Balance,
                Credit = s.Credit,
                Debit = s.Debit,
            }).FirstOrDefault();
            if (TransModel == null)
            {
                var newModel = new TransSupplier
                {
                    SerialNumber = Trans.SerialNumber,
                    Date = Trans.Date.ToLocalTime(),
                    SupplierId = Trans.SupplierId,
                    Balance = Trans.Balance,
                    SupplierType = (int)Trans.SupplierType,
                    OpenBalFlag = Trans.OpenBalFlag,
                    Credit = Trans.Credit,
                    Debit = Trans.Debit,
                    IsDeleted = false,
                };
                Insert(newModel);
                return false;
            }
            else
            {
 
                var SupplierBal = dbSet.Where(x => x.SupplierId == TransModel.SupplierId && x.Date.Date <= TransModel.Date.Date && x.IsDeleted == false && x.ID < TransModel.Id).Select(s => new TransSupplierVM
                {
                    Date = s.Date,
                    Id = s.ID,
                    Balance = s.Balance,
                }).OrderByDescending(x => x.Date.Date).ThenByDescending(x => x.Id).ToList();
                decimal BeforeBalance = SupplierBal != null && SupplierBal.Count > 0 ? SupplierBal.FirstOrDefault().Balance : 0;
                TransModel.Balance = TransModel.Credit > 0 ? BeforeBalance == 0 ? Balance : BeforeBalance - Balance : BeforeBalance == 0 ? Balance : BeforeBalance + Balance;
                if (!Trans.OpenBalFlag)
                {
                    if (TransModel.Credit > 0)
                        TransModel.Credit = Balance;
                    else
                        TransModel.Debit = Balance;
                }
                TransModel.Date = Trans.Date.ToLocalTime();
                Edit(TransModel);
                return false;
            }
        }
        public TransSupplier Edit(TransSupplierVM s)
        {
            TransSupplier edited = dbSet.FirstOrDefault(p => p.SupplierId == s.SupplierId && p.SerialNumber == s.SerialNumber && p.SupplierType == (int)s.SupplierType && p.IsDeleted == false);
            edited.Date = s.Date.ToLocalTime();
            edited.Balance = s.Balance;
            edited.Credit = s.Credit;
            edited.Debit = s.Debit;
            //edited.IsDeleted = false;
            Update(edited);
            return edited;
        }
        public TransSupplier EditAfterBalance(TransSupplierVM s)
        {
            TransSupplier edited = dbSet.FirstOrDefault(p => p.SupplierId == s.SupplierId && p.SerialNumber == s.SerialNumber && p.SupplierType == (int)s.SupplierType && p.IsDeleted == false);
            edited.Balance = s.Balance;
            Update(edited);
            return edited;
        }
        public void Delete(int id)
        {
            TransSupplier edited = dbSet.FirstOrDefault(p => p.ID == id && p.IsDeleted == false);
            if (edited != null)
            {
                edited.IsDeleted = true;
                Delete(edited);
            }
        }

        public int RecalculateSupplierBalance(int Supplierid, int BillSerialNumber)//
        {
            var DeletedBalance = dbSet.Where(x => x.SupplierId == Supplierid && x.SerialNumber == BillSerialNumber && x.IsDeleted == false).Select(s => new TransSupplierVM
            {
                Id = s.ID,
                //SerialNumber = s.SerialNumber,
                Date = s.Date,
                //SupplierId = s.SupplierId,
                Balance = s.Balance,
                //SupplierType = (Enums.Enums.TransSupplierTypeEnum)s.SupplierType,
                //OpenBalFlag = s.OpenBalFlag,
                Credit = s.Credit,
                Debit = s.Debit,
            }).FirstOrDefault();
            if (DeletedBalance != null)
            {
                var BeforeBalanceTemp = dbSet.Where(x => x.SupplierId == Supplierid && x.ID < DeletedBalance.Id && x.Date.Date < DeletedBalance.Date.Date && x.IsDeleted == false).Select(s => new TransSupplierVM
                {
                    Id = s.ID,
                    Balance = s.Balance,
                    Date = s.Date,
                }).OrderByDescending(x => x.Date.Date).OrderByDescending(x => x.Id).FirstOrDefault();
                decimal BeforeBalance = 0;
                if (BeforeBalanceTemp != null)
                {
                    BeforeBalance = BeforeBalanceTemp.Balance;
                }//&& x.ID > DeletedBalance.Id
                var SameDateBalanceList = dbSet.Where(x => x.SupplierId == Supplierid && x.Date.Date == DeletedBalance.Date.Date && x.IsDeleted == false && x.OpenBalFlag == false).Select(s => new TransSupplierVM
                {
                    Id = s.ID,
                    SerialNumber = (int)s.SerialNumber,
                    Date = s.Date,
                    SupplierId = s.SupplierId,
                    Balance = s.Balance,
                    SupplierType = (Enums.Enums.TransSupplierTypeEnum)s.SupplierType,
                    OpenBalFlag = s.OpenBalFlag,
                    Credit = s.Credit,
                    Debit = s.Debit,
                }).OrderBy(x => x.Date.Date).OrderBy(x => x.Id).ToList();
                if (SameDateBalanceList != null && SameDateBalanceList.Count > 0)
                {
                    foreach (var BalanceModel in SameDateBalanceList)
                    {
                        if (BalanceModel.Id != DeletedBalance.Id)
                        {
                            BalanceModel.Balance = BalanceModel.Credit > 0 ? BeforeBalance - BalanceModel.Credit : BeforeBalance + BalanceModel.Debit;
                            BeforeBalance = BalanceModel.Balance;
                            EditAfterBalance(BalanceModel);
                        }
                    }
                }
                //missed current balance
                BeforeBalance = DeletedBalance.Balance;
                var AfterBalanceList = dbSet.Where(x => x.SupplierId == Supplierid && x.Date.Date > DeletedBalance.Date.Date && x.IsDeleted == false && x.OpenBalFlag == false).Select(s => new TransSupplierVM
                {
                    Id = s.ID,
                    SerialNumber = (int)s.SerialNumber,
                    Date = s.Date,
                    SupplierId = s.SupplierId,
                    Balance = s.Balance,
                    SupplierType = (Enums.Enums.TransSupplierTypeEnum)s.SupplierType,
                    OpenBalFlag = s.OpenBalFlag,
                    Credit = s.Credit,
                    Debit = s.Debit,
                }).OrderBy(x => x.Date.Date).OrderBy(x => x.Id).ToList();
                if (AfterBalanceList != null && AfterBalanceList.Count > 0)
                {
                    foreach (var BalanceModel in AfterBalanceList)
                    {
                        if (BalanceModel.Id != DeletedBalance.Id)
                        {
                            BalanceModel.Balance = BalanceModel.Credit > 0 ? BeforeBalance - BalanceModel.Credit : BeforeBalance + BalanceModel.Debit;
                            BeforeBalance = BalanceModel.Balance;
                            EditAfterBalance(BalanceModel);
                        }
                    }
                }
                return DeletedBalance.Id;
            }
            else
            {
                return 0;
            }
        }
     
        public void DeletePhysical(int SupplierId, int Id, int Type)
        {
            TransSupplier DeleteModel = dbSet.FirstOrDefault(p => p.SupplierId == SupplierId && p.SerialNumber == Id && p.SupplierType == Type && p.IsDeleted == false);
            if (DeleteModel != null)
                Remove(DeleteModel);
        }
        public void UpdateSupplierBalance(int Supplierid)
        {
            List<TransSupplierVM> SupplierBalanceList = dbSet.Where(x => x.SupplierId == Supplierid && x.IsDeleted == false).Select(s => new TransSupplierVM
            {
                Id = s.ID,
                SerialNumber = (int)s.SerialNumber,
                Date = s.Date,
                SupplierId = s.SupplierId,
                Balance = s.Balance,
                SupplierType = (Enums.Enums.TransSupplierTypeEnum)s.SupplierType,
                OpenBalFlag = s.OpenBalFlag,
                Credit = s.Credit,
                Debit = s.Debit,
            }).OrderBy(x => x.Date.Date).ThenBy(x => x.Id).ToList();
            decimal BeforeBalance = 0;
            var tempBeforBalance = SupplierBalanceList.Where(x => x.OpenBalFlag);
            if (tempBeforBalance.Count() != 0)
            {
                BeforeBalance = tempBeforBalance.FirstOrDefault().Balance;
            }
            foreach (var trans in SupplierBalanceList)
            {
                TransSupplierVM transSupplier = new TransSupplierVM();

                if (trans.OpenBalFlag)
                {
                    transSupplier = dbSet.Where(x => x.ID == trans.Id && x.IsDeleted == false).Select(s => new TransSupplierVM
                    {
                        Id = s.ID,
                        SerialNumber = (int)s.SerialNumber,
                        Date = s.Date.Date,
                        SupplierId = s.SupplierId,
                        SupplierType = (Enums.Enums.TransSupplierTypeEnum)s.SupplierType,
                        Debit = 0,
                        Credit = 0,
                        Balance = trans.Balance,
                        OpenBalFlag = true,
                    }).FirstOrDefault();
                    Edit(transSupplier);
                }
                else
                {
                    transSupplier = dbSet.Where(x => x.ID == trans.Id && x.IsDeleted == false).Select(s => new TransSupplierVM
                    {
                        Id = s.ID,
                        SerialNumber = (int)s.SerialNumber,
                        Date = s.Date.Date,
                        SupplierId = s.SupplierId,
                        SupplierType = (Enums.Enums.TransSupplierTypeEnum)s.SupplierType,
                        Debit = s.Debit,
                        Credit = s.Credit,
                        Balance = trans.Credit > 0 ? BeforeBalance - trans.Credit : BeforeBalance + trans.Debit,
                        OpenBalFlag = false,
                    }).FirstOrDefault();
                    EditAfterBalance(transSupplier);
                }
                BeforeBalance = transSupplier.Balance;
            }
        }
    }
}