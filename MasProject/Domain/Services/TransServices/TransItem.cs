﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Trans;
using MasProject.Domain.ViewModel.TransVM;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using static MasProject.Domain.Enums.Enums;

namespace MasProject.Domain.Services.TransServices
{
    public class TransItemService : Repository<TransItem>
    {
        private DBContext _context;
        public TransItemService(DBContext context) : base(context)
        {
            _context = context;
        }

        private static string GetDisplayAttribute(object value)
        {
            Type type = value.GetType();
            var field = type.GetField(value.ToString());
            return field == null ? null : field.GetCustomAttribute<DisplayAttribute>().GetName();
        }
        public ItemBalanceReportVM GetBill(int SerialNumber, int Type)
        {
            ItemBalanceReportVM trans = new ItemBalanceReportVM();
            switch (Type)
            {
                case (int)TransItemTypeEnum.رصيد_اول_المدة:
                    trans.PageName = "ItemOBEdit";
                    trans.BillId = _context.ItemsOpeningBalance.FirstOrDefault(x => x.DocumentNumber == SerialNumber).ID;
                    break;
                case (int)TransItemTypeEnum.سند_إضافة:
                    trans.PageName = "AdditionVoucherEdit";
                    trans.BillId = _context.AdditionVoucher.FirstOrDefault(x => x.SerialNumber == SerialNumber).ID;
                    break;
                case (int)TransItemTypeEnum.سند_صرف:
                    trans.PageName = "PaymentVoucherEdit";
                    trans.BillId = _context.PaymentVoucher.FirstOrDefault(x => x.SerialNumber == SerialNumber).ID;
                    break;
            }
            return trans;
        }
        public IEnumerable<TransItemVM> GetItemBalance(ItemBalanceReportVM ItemBalance)
        {
            IEnumerable<TransItemVM> returnList;
            if (ItemBalance.DateTo == DateTime.MinValue && ItemBalance.DateForm == DateTime.MinValue)
            {
                returnList = dbSet.Where(x => x.ItemId == ItemBalance.ItemId && x.StoreId == ItemBalance.StoreId && !x.IsDeleted).Select(s => new TransItemVM
                {
                    Id = s.ID,
                    SerialNumber = (int)s.SerialNumber,
                    ShowDate = s.Date.ToString("dd/MM/yyyy"),
                    ItemId = s.ItemId,
                    ItemType = (Enums.Enums.TransItemTypeEnum)s.ItemType,
                    ItemTypeID = s.ItemType,
                    DisplayName = GetDisplayAttribute((Enums.Enums.TransItemTypeEnum)s.ItemType),
                    ForeignSerialNumber =  
                    ((int)(s.ItemType == (int)TransItemTypeEnum.رصيد_اول_المدة ? 0 :
                   (s.ItemType == (int)TransItemTypeEnum.سند_إضافة ? _context.AdditionVoucher.FirstOrDefault(x => x.SerialNumber == s.SerialNumber).BillSerialNumber :
                    _context.PaymentVoucher.FirstOrDefault(x => x.SerialNumber == s.SerialNumber).BillSerialNumber))),
                    ItemName = _context.Items.FirstOrDefault(c => c.ID == s.ItemId).Name,
                    StoreName = _context.Stores.FirstOrDefault(x => x.ID == s.StoreId).Name,
                    Balance = s.Balance,
                    Subtraction = s.Credit,
                    Addition = s.Debit,
                    Date = s.Date,
                    PersonId= (
                    //سند اضافة
                    s.ItemType==2 ?(int) _context.AdditionVoucher.FirstOrDefault(x=>x.SerialNumber==s.SerialNumber && x.IsDeleted==false).Client_Supplier_Id:
                    //سند صرف
                    (s.ItemType==3 ? (int)_context.PaymentVoucher.FirstOrDefault(x => x.SerialNumber == s.SerialNumber && x.IsDeleted == false).Client_Supplier_Id:0)),
                    PersonType = (
                    //سند اضافة
                    s.ItemType == 2 ? _context.AdditionVoucher.FirstOrDefault(x => x.SerialNumber == s.SerialNumber && x.IsDeleted == false).UserType :
                    //سند صرف
                    (s.ItemType == 3 ? _context.PaymentVoucher.FirstOrDefault(x => x.SerialNumber == s.SerialNumber && x.IsDeleted == false).UserType : 0)),
                }).OrderBy(x => x.Date.Date).ThenBy(x => x.Id);
            }
            else if (ItemBalance.DateTo == DateTime.MinValue && ItemBalance.DateForm != DateTime.MinValue)
            {
                returnList = dbSet.Where(x => x.ItemId == ItemBalance.ItemId && x.Date.Date == ItemBalance.DateForm.Value.Date && !x.IsDeleted).Select(s => new TransItemVM
                {
                    Id = s.ID,
                    SerialNumber = (int)s.SerialNumber,
                    ShowDate = s.Date.ToString("dd/MM/yyyy"),
                    ItemId = s.ItemId,
                    ItemTypeID = s.ItemType,
                    ItemType = (Enums.Enums.TransItemTypeEnum)s.ItemType,
                    DisplayName = GetDisplayAttribute((Enums.Enums.TransItemTypeEnum)s.ItemType),
                    ForeignSerialNumber = (int)(s.ItemType == (int)TransItemTypeEnum.رصيد_اول_المدة ? 0 :
                   (s.ItemType == (int)TransItemTypeEnum.سند_إضافة ? _context.AdditionVoucher.FirstOrDefault(x => x.SerialNumber == s.SerialNumber).ID :
                    _context.PaymentVoucher.FirstOrDefault(x => x.SerialNumber == s.SerialNumber).ID)),
                    ItemName = _context.Items.FirstOrDefault(c => c.ID == s.ItemId).Name,
                    StoreName = _context.Stores.FirstOrDefault(x => x.ID == s.StoreId).Name,
                    Balance = s.Balance,
                    Subtraction = s.Credit,
                    Addition = s.Debit,
                    Date = s.Date
                }).OrderBy(x => x.Date.Date).ThenBy(x => x.Id);
            }
            else if (ItemBalance.DateTo != null && ItemBalance.DateForm == null)
            {
                returnList = dbSet.Where(x => x.ItemId == ItemBalance.ItemId && x.Date.Date <= ItemBalance.DateTo.Value.Date && !x.IsDeleted).Select(s => new TransItemVM
                {
                    Id = s.ID,
                    SerialNumber = (int)s.SerialNumber,
                    ShowDate = s.Date.ToString("dd/MM/yyyy"),
                    ItemId = s.ItemId,
                    ItemTypeID = s.ItemType,
                    ItemType = (Enums.Enums.TransItemTypeEnum)s.ItemType,
                    DisplayName = GetDisplayAttribute((Enums.Enums.TransItemTypeEnum)s.ItemType),
                    ForeignSerialNumber = (int)(s.ItemType == (int)TransItemTypeEnum.رصيد_اول_المدة ? 0 :
                   (s.ItemType == (int)TransItemTypeEnum.سند_إضافة ? _context.AdditionVoucher.FirstOrDefault(x => x.SerialNumber == s.SerialNumber).ID :
                    _context.PaymentVoucher.FirstOrDefault(x => x.SerialNumber == s.SerialNumber).ID)),
                    ItemName = _context.Items.FirstOrDefault(c => c.ID == s.ItemId).Name,
                    StoreName = _context.Stores.FirstOrDefault(x => x.ID == s.StoreId).Name,
                    Balance = s.Balance,
                    Subtraction = s.Credit,
                    Addition = s.Debit,
                    Date = s.Date
                }).OrderBy(x => x.Date.Date).ThenBy(x => x.Id);
            }
            else
            {
                returnList = dbSet.Where(x => x.ItemId == ItemBalance.ItemId && x.Date.Date >= ItemBalance.DateForm.Value.Date && x.Date.Date <= ItemBalance.DateTo.Value.Date && !x.IsDeleted).Select(s => new TransItemVM
                {
                    Id = s.ID,
                    SerialNumber = (int)s.SerialNumber,
                    ShowDate = s.Date.ToString("dd/MM/yyyy"),
                    ItemId = s.ItemId,
                    ItemTypeID = s.ItemType,
                    ItemType = (Enums.Enums.TransItemTypeEnum)s.ItemType,
                    DisplayName = GetDisplayAttribute((Enums.Enums.TransItemTypeEnum)s.ItemType),
                    ForeignSerialNumber = (int)(s.ItemType == (int)TransItemTypeEnum.رصيد_اول_المدة ? 0 :
                   (s.ItemType == (int)TransItemTypeEnum.سند_إضافة ? _context.AdditionVoucher.FirstOrDefault(x => x.SerialNumber == s.SerialNumber).ID :
                    _context.PaymentVoucher.FirstOrDefault(x => x.SerialNumber == s.SerialNumber).ID)),
                    ItemName = _context.Items.FirstOrDefault(c => c.ID == s.ItemId).Name,
                    StoreName = _context.Stores.FirstOrDefault(x => x.ID == s.StoreId).Name,
                    Balance = s.Balance,
                    Subtraction = s.Credit,
                    Addition = s.Debit,
                    Date = s.Date,
                }).OrderBy(x => x.Date.Date).ThenBy(x => x.Id);
            }
            return returnList;
        }
        public ItemBalanceReportVM GetCurrentBalance(int ItemId, int Storeid, DateTime? DateFrom)
        {
            ItemBalanceReportVM trans = new ItemBalanceReportVM();
            trans.ItemName = _context.Items.FirstOrDefault(x => x.ID == ItemId).Name;
            trans.StoreName = _context.Stores.FirstOrDefault(x => x.ID == Storeid).Name;
            var model = dbSet.Where(x => x.ItemId == ItemId && x.StoreId == Storeid && !x.IsDeleted && x.Date.Date <= DateFrom.Value.Date).OrderByDescending(x => x.ID).FirstOrDefault();
            trans.Balance = DateFrom.HasValue && DateFrom != DateTime.MinValue ? model != null ? model.Balance : 0 : 0;
            return trans;
        }


        public decimal GetBalance(int Itemid, int Storeid)
        {
            var model = dbSet.Where(x => x.ItemId == Itemid && x.StoreId == Storeid && x.IsDeleted == false).Select(s => new TransItemVM
            {
                Id = s.ID,
                SerialNumber = (int)s.SerialNumber,
                Date = s.Date,
                ItemId = s.ItemId,
                Balance = s.Balance,
                ItemType = (Enums.Enums.TransItemTypeEnum)s.ItemType,
                OpenBalFlag = s.OpenBalFlag,
                Subtraction = s.Credit,
                Addition = s.Debit,
            }).OrderBy(x => x.Date.Date).ThenByDescending(x => x.Id).ToList() ?? new List<TransItemVM>();
            return model.Count > 0 ? model.FirstOrDefault().Balance : 0;
        }
        public decimal GetLatestBalance(int Itemid, int Storeid, DateTime Date)
        {
            var model = dbSet.Where(x => x.ItemId == Itemid && x.StoreId == Storeid && x.Date.Date <= Date.Date && x.IsDeleted == false).Select(s => new TransItemVM
            {
                Id = s.ID,
                Date = s.Date,
                Balance = s.Balance,
            }).OrderByDescending(x => x.Date.Date).ThenByDescending(x => x.Id).ToList() ?? new List<TransItemVM>();
            return model.Count > 0 ? model.FirstOrDefault().Balance : 0;
        }
        public bool Add(TransItemVM Trans, decimal Balance)
        {
            TransItemVM TransModel = dbSet.Where(x => x.ItemId == Trans.ItemId && x.StoreId == Trans.StoreId && x.SerialNumber == Trans.SerialNumber && x.ItemType == (int)Trans.ItemType && !x.IsDeleted).Select(s => new TransItemVM
            {
                Id = s.ID,
                SerialNumber = (int)s.SerialNumber,
                Date = s.Date,
                ItemId = s.ItemId,
                StoreId = s.StoreId,
                ItemType = (Enums.Enums.TransItemTypeEnum)s.ItemType,
                ItemTypeID = s.ItemType,
                DisplayName = GetDisplayAttribute((Enums.Enums.TransItemTypeEnum)s.ItemType),
                ItemName = _context.Items.FirstOrDefault(c => c.ID == s.ItemId).Name,
                Balance = s.Balance,
                Subtraction = s.Credit,
                Addition = s.Debit,
            }).FirstOrDefault();
            if (TransModel == null)
            {
                var newModel = new TransItem
                {
                    SerialNumber = Trans.SerialNumber,
                    Date = Trans.Date.ToLocalTime(),
                    ItemId = Trans.ItemId,
                    StoreId = Trans.StoreId,
                    Balance = Trans.Balance,
                    ItemType = (int)Trans.ItemType,
                    OpenBalFlag = Trans.OpenBalFlag,
                    Credit = Trans.Subtraction,
                    Debit = Trans.Addition,
                    IsDeleted = false,
                };
                Insert(newModel);
                return false;

            }
            else
            {
                var ItemBal = dbSet.Where(x => x.ItemId == TransModel.ItemId && x.StoreId == Trans.StoreId && x.Date.Date <= TransModel.Date.Date && x.IsDeleted == false && x.ID < TransModel.Id).Select(s => new TransItemVM
                {
                    Date = s.Date,
                    Id = s.ID,
                    Balance = s.Balance,
                }).OrderByDescending(x => x.Date.Date).ThenByDescending(x => x.Id).ToList();
                decimal BeforeBalance = ItemBal != null && ItemBal.Count > 0 ? ItemBal.FirstOrDefault().Balance : 0;
                TransModel.Balance = TransModel.Subtraction > 0 ? BeforeBalance == 0 ? Balance : BeforeBalance - Balance : BeforeBalance == 0 ? Balance : BeforeBalance + Balance;
                if (!Trans.OpenBalFlag)
                {
                    if (TransModel.Subtraction > 0)
                        TransModel.Subtraction = Balance;
                    else
                        TransModel.Addition = Balance;
                }
                TransModel.Date = Trans.Date.ToLocalTime();
                Edit(TransModel);
                return false;
            }
        }
        public TransItem Edit(TransItemVM s)
        {
            TransItem edited = dbSet.FirstOrDefault(p => p.ItemId == s.ItemId && p.StoreId == s.StoreId && p.SerialNumber == s.SerialNumber && p.ItemType == (int)s.ItemType && p.IsDeleted == false);
            edited.Date = s.Date.ToLocalTime();
            edited.Balance = s.Balance;
            edited.Credit = s.Subtraction;
            edited.Debit = s.Addition;
            //edited.IsDeleted = false;//not allowed to change 
            Update(edited);
            return edited;
        }
        public TransItem EditAfterBalance(TransItemVM s)
        {
            TransItem edited = dbSet.FirstOrDefault(p => p.ItemId == s.ItemId && p.StoreId == s.StoreId && p.SerialNumber == s.SerialNumber && p.ItemType == (int)s.ItemType && p.IsDeleted == false);
            edited.Balance = s.Balance;
            Update(edited);
            return edited;
        }
        public void Delete(int id)
        {
            TransItem edited = dbSet.FirstOrDefault(p => p.ID == id && p.IsDeleted == false);
            if (edited != null)
            {
                edited.IsDeleted = true;
                Delete(edited);
            }
        }


        public int RecalculateItemBalance(int Itemid, int StoreId, int DocumentNumber)//
        {
            var DeletedBalance = dbSet.Where(x => x.ItemId == Itemid && x.StoreId == StoreId && x.SerialNumber == DocumentNumber && x.IsDeleted == false).Select(s => new TransItemVM
            {
                Id = s.ID,
                //SerialNumber = s.SerialNumber,
                Date = s.Date,
                //ItemId = s.ItemId,
                Balance = s.Balance,
                //ItemType = (Enums.Enums.TransItemTypeEnum)s.ItemType,
                //OpenBalFlag = s.OpenBalFlag,
                Subtraction = s.Credit,
                Addition = s.Debit,
            }).FirstOrDefault();
            if (DeletedBalance != null)
            {
                var BeforeBalanceTemp = dbSet.Where(x => x.ItemId == Itemid && x.StoreId == StoreId && x.ID < DeletedBalance.Id && x.Date.Date < DeletedBalance.Date.Date && x.IsDeleted == false).Select(s => new TransItemVM
                {
                    Id = s.ID,
                    Balance = s.Balance,
                    Date = s.Date,
                }).OrderByDescending(x => x.Date.Date).OrderByDescending(x => x.Id).FirstOrDefault();
                decimal BeforeBalance = 0;
                if (BeforeBalanceTemp != null)
                {
                    BeforeBalance = BeforeBalanceTemp.Balance;
                }//&& x.ID > DeletedBalance.Id
                var SameDateBalanceList = dbSet.Where(x => x.ItemId == Itemid && x.StoreId == StoreId && x.Date.Date == DeletedBalance.Date.Date && x.IsDeleted == false && x.OpenBalFlag == false).Select(s => new TransItemVM
                {
                    Id = s.ID,
                    SerialNumber = (int)s.SerialNumber,
                    Date = s.Date,
                    ItemId = s.ItemId,
                    Balance = s.Balance,
                    ItemType = (Enums.Enums.TransItemTypeEnum)s.ItemType,
                    OpenBalFlag = s.OpenBalFlag,
                    Subtraction = s.Credit,
                    Addition = s.Debit,
                    StoreId=s.StoreId,
                }).OrderBy(x => x.Date.Date).OrderBy(x => x.Id).ToList();
                if (SameDateBalanceList != null && SameDateBalanceList.Count > 0)
                {
                    foreach (var BalanceModel in SameDateBalanceList)
                    {
                        if (BalanceModel.Id != DeletedBalance.Id)
                        {
                            BalanceModel.Balance = BalanceModel.Subtraction > 0 ? BeforeBalance - BalanceModel.Subtraction : BeforeBalance + BalanceModel.Addition;
                            BeforeBalance = BalanceModel.Balance;
                            EditAfterBalance(BalanceModel);
                        }
                    }
                }
                //missed current balance
                BeforeBalance = DeletedBalance.Balance;
                var AfterBalanceList = dbSet.Where(x => x.ItemId == Itemid && x.StoreId == StoreId && x.Date.Date > DeletedBalance.Date.Date && x.IsDeleted == false && x.OpenBalFlag == false).Select(s => new TransItemVM
                {
                    Id = s.ID,
                    SerialNumber = (int)s.SerialNumber,
                    Date = s.Date,
                    ItemId = s.ItemId,
                    Balance = s.Balance,
                    ItemType = (Enums.Enums.TransItemTypeEnum)s.ItemType,
                    OpenBalFlag = s.OpenBalFlag,
                    Subtraction = s.Credit,
                    Addition = s.Debit,
                    StoreId = s.StoreId,
                }).OrderBy(x => x.Date.Date).OrderBy(x => x.Id).ToList();
                if (AfterBalanceList != null && AfterBalanceList.Count > 0)
                {
                    foreach (var BalanceModel in AfterBalanceList)
                    {
                        if (BalanceModel.Id != DeletedBalance.Id)
                        {
                            BalanceModel.Balance = BalanceModel.Subtraction > 0 ? BeforeBalance - BalanceModel.Subtraction : BeforeBalance + BalanceModel.Addition;
                            BeforeBalance = BalanceModel.Balance;
                            EditAfterBalance(BalanceModel);
                        }
                    }
                }
                return DeletedBalance.Id;
            }
            else
            {
                return 0;
            }
        }
        public void DeletePhysical(int ItemId, int StoreId, int Id, int Type)
        {
            TransItem DeleteModel = dbSet.FirstOrDefault(p => p.ItemId == ItemId && p.StoreId == StoreId && p.SerialNumber == Id && p.ItemType == Type && p.IsDeleted == false);
            if (DeleteModel != null)
                Remove(DeleteModel);
        }

        public void UpdateItemBalance(int Itemid, int StoreId)
        {
            List<TransItemVM> ItemBalanceList = dbSet.Where(x => x.ItemId == Itemid && x.StoreId == StoreId && x.IsDeleted == false).Select(s => new TransItemVM
            {
                Id = s.ID,
                SerialNumber = (int)s.SerialNumber,
                Date = s.Date.Date,
                ItemId = s.ItemId,
                Balance = s.Balance,
                ItemType = (Enums.Enums.TransItemTypeEnum)s.ItemType,
                OpenBalFlag = s.OpenBalFlag,
                Subtraction = s.Credit,
                Addition = s.Debit,
                StoreId=s.StoreId,
            }).OrderBy(x => x.Date.Date).ThenBy(x => x.Id).ToList();
            decimal BeforeBalance = 0;
            var tempBeforBalance = ItemBalanceList.Where(x => x.OpenBalFlag);
            if (tempBeforBalance.Count() != 0)
            {
                BeforeBalance = tempBeforBalance.FirstOrDefault().Balance;
            }
            foreach (var trans in ItemBalanceList)
            {
                TransItemVM transItem = new TransItemVM();

                if (trans.OpenBalFlag)
                {
                    transItem = dbSet.Where(x => x.ID == trans.Id && x.IsDeleted == false).Select(s => new TransItemVM
                    {
                        Id = s.ID,
                        SerialNumber = (int)s.SerialNumber,
                        Date = s.Date.Date,
                        ItemId = s.ItemId,
                        ItemType = (Enums.Enums.TransItemTypeEnum)s.ItemType,
                        Addition = 0,
                        Subtraction = 0,
                        Balance = trans.Balance,
                        OpenBalFlag = true,
                        StoreId=s.StoreId,
                    }).FirstOrDefault();
                    Edit(transItem);
                }
                else
                {
                    transItem = dbSet.Where(x => x.ID == trans.Id && x.IsDeleted == false).Select(s => new TransItemVM
                    {
                        Id = s.ID,
                        SerialNumber = (int)s.SerialNumber,
                        Date = s.Date,
                        ItemId = s.ItemId,
                        ItemType = (Enums.Enums.TransItemTypeEnum)s.ItemType,
                        Addition = s.Debit,
                        Subtraction = s.Credit,
                        Balance = trans.Subtraction > 0 ? BeforeBalance - trans.Subtraction : BeforeBalance + trans.Addition,
                        OpenBalFlag = false,
                        StoreId = s.StoreId,
                    }).FirstOrDefault();
                    EditAfterBalance(transItem);
                }
                BeforeBalance = transItem.Balance;
            }
        }

        //public List<TransItemVM> GetInsertedItems()
        //{
        //    return dbSet.Where(x => x.OpenBalFlag == false && x.StoreId != 0 && x.ItemId != 0).Select(s => new TransItemVM {
        //    ItemId=s.ItemId,
        //    StoreId=s.StoreId,
        //    Balance=s.Balance,
        //    Date=s.Date,
        //    SerialNumber=s.SerialNumber,
        //    }).ToList();
        //}

        public bool CheckExistForInsert(int Itemid, int StoreId,int Serial)
        {
            return dbSet.Any(x => x.IsDeleted == false && x.ItemId == Itemid && x.StoreId == StoreId && x.SerialNumber == Serial);
        }
    }
}

