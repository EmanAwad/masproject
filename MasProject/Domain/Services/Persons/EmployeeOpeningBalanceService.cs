﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Persons;
using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel;
using static MasProject.Domain.Enums.Enums;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Runtime.InteropServices.ComTypes;

namespace MasProject.Domain.Services.Persons
{
    public class EmployeeOpeningBalanceService : Repository<EmployeeOpeningBalance>
    {
        public EmployeeOpeningBalanceService(DBContext context) : base(context)
        {
        }
        public IEnumerable<EmployeeOpeningBalanceVM> GetAll()
        {
            return dbSet.Where(x => x.IsDeleted == false).Select(s => new EmployeeOpeningBalanceVM
            {
                ID = s.ID,
                Amount = s.Amount,
                EntryDate = s.EntryDate,
                EmployeeID =  s.EmployeeID,
                EmployeeName= s.EmployeeObj.Name
            }).OrderByDescending(s => s.ID);
        }

        public EmployeeOpeningBalanceVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new EmployeeOpeningBalanceVM
            {
                ID = s.ID,
                Amount = s.Amount,
                EntryDate = s.EntryDate.Date,
                EmployeeID =  s.EmployeeID,
                EmployeeName = s.EmployeeObj.Name,
                Note = s.Note
            }).FirstOrDefault();

        }
        public EmployeeOpeningBalance Add(EmployeeOpeningBalanceVM balance)
        {
            EmployeeOpeningBalance model = new EmployeeOpeningBalance
            {
                ID = balance.ID,
                Amount = balance.Amount,
                EntryDate = balance.EntryDate.ToLocalTime(),
                EmployeeID =  balance.EmployeeID,
                Note = balance.Note
            };
            Insert(model);
            return model;
        }
        public EmployeeOpeningBalance Edit(EmployeeOpeningBalanceVM balance)
        {
            EmployeeOpeningBalance model = new EmployeeOpeningBalance
            {
                ID = balance.ID,
                EntryDate = balance.EntryDate.ToLocalTime(),
                Amount = balance.Amount,
                EmployeeID =  balance.EmployeeID,
                Note = balance.Note,
                IsDeleted = false
            };

            Update(model);
            return model;
        }
        public void Delete(int id)
        {
            EmployeeOpeningBalance edited = dbSet.FirstOrDefault(p => p.ID == id);
            Delete(edited);
        }
    }
}