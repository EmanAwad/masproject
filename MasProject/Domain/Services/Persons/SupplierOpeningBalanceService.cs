﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Persons;
using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel;
using System.Collections.Generic;
using System.Linq;

namespace MasProject.Domain.Services.Persons
{
    public class SupplierOpeningBalanceService : Repository<SupplierOpeningBalance>
    {
        public SupplierOpeningBalanceService(DBContext context) : base(context)
        {
        }
        public IEnumerable<SupplierOpeningBalanceVM> GetAll()
        {
            return dbSet.Where(x => x.IsDeleted == false).Select(s => new SupplierOpeningBalanceVM
            {
                ID = s.ID,
               // Amount = s.Amount,
              //  Ratio = s.Ratio,
                EntryDate = s.EntryDate,
                //SupplierID = s.SupplierID,
                //SupplierName = s.SupplierObj.Name,
                //CurrencyID = s.CurrencyID,
                //CurrencyName = s.CurrencyObj.Name,
               DocumentNumber = s.DocumentNumber,
            }).OrderByDescending(s => s.ID);
        }

        public SupplierOpeningBalanceVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new SupplierOpeningBalanceVM
            {
                ID = s.ID,
                //Amount = s.Amount,
                //Ratio = s.Ratio,
                EntryDate = s.EntryDate.Date,
                //SupplierID = s.SupplierID,
                //SupplierName = s.SupplierObj.Name,
                //CurrencyID = s.CurrencyID,
                //CurrencyName = s.CurrencyObj.Name,
                Note = s.Note,
                DocumentNumber = s.DocumentNumber,
            }).FirstOrDefault();

        }
        public SupplierOpeningBalance Add(SupplierOpeningBalanceVM balance)
        {
            SupplierOpeningBalance model = new SupplierOpeningBalance
            {
                ID = balance.ID,
                //Amount = balance.Amount,
                //Ratio = balance.Ratio,
                EntryDate = balance.EntryDate.ToLocalTime(),
                //SupplierID = balance.SupplierID,
              //  CurrencyID = balance.CurrencyID,
                Note = balance.Note,
                DocumentNumber = balance.DocumentNumber,
            };
            Insert(model);
            return model;
        }
        public SupplierOpeningBalance Edit(SupplierOpeningBalanceVM balance)
        {
            SupplierOpeningBalance model = new SupplierOpeningBalance
            {
                ID = balance.ID,
                EntryDate = balance.EntryDate.ToLocalTime(),
                //Amount = balance.Amount.Value,
                //Ratio = balance.Ratio.Value,
                //SupplierID = balance.SupplierID,
                //CurrencyID = balance.CurrencyID,
                Note = balance.Note,
                IsDeleted = false,
                DocumentNumber = balance.DocumentNumber,
            };

            Update(model);
            return model;
        }
        public void Delete(int id)
        {
            SupplierOpeningBalance edited = dbSet.FirstOrDefault(p => p.ID == id);
            Delete(edited);
        }
        public int GenerateSerial()
        {
            int serial = dbSet.FirstOrDefault(x => x.IsDeleted == false) != null ? dbSet.Max(c => c.DocumentNumber) + 1 : 1;
            return serial;
        }
    }
}