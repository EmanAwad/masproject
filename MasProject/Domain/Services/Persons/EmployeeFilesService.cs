﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Persons;
using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Domain.Services.Persons
{
   public class EmployeeFilesService : Repository<EmployeeFiles>
    {
        public EmployeeFilesService(DBContext context) : base(context)
        {

        }

    }
}
