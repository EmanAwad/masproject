﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Persons;
using MasProject.Domain.ViewModel.Persons;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MasProject.Domain.Services.Persons
{
   public class SocialClientService : Repository<Social_Client>
    {
        public SocialClientService(DBContext context) : base(context)
        {
        }
        public List<SocialMediaVM> GetByClientID(int ClientID)
        {
            return dbSet.Where(x => x.IsDeleted == false && x.ClientID==ClientID).Select(s => new SocialMediaVM
            {
                ID = s.SocialID.Value,
                Name=s.SocialObj.Name,
                Value=s.Value,
            }).ToList();
        }
        public SocialClientVM GetByClientAndSocialID(int ClientID , int SocialID)
        {
            return dbSet.Where(x => x.IsDeleted == false && x.ClientID == ClientID && x.SocialID == SocialID).Select(s => new SocialClientVM
            {
                ID=s.ID,
                ClientID=s.ClientID.Value,
                SocialID = s.SocialID.Value,
                Name = s.SocialObj.Name,
                Value = s.Value,
            }).FirstOrDefault();
        }
        public Social_Client Edit(SocialClientVM social)
        {

            Social_Client model = new Social_Client
            {
                ID = social.ID,
                Name = social.Name,
                SocialID = social.SocialID,
                ClientID=social.ClientID,
                Value=social.Value,
                IsDeleted = false
            };

            Update(model);
            return model;
        }
        public Social_Client Add(SocialClientVM Addmodel)
        {
            Social_Client model = new Social_Client { 
               ClientID=Addmodel.ClientID,
               SocialID=Addmodel.SocialID,
               Value=Addmodel.Value
            };
            Insert(model);
            return model;
        }

        public void Delete(int id)
        {
            Social_Client edited = dbSet.FirstOrDefault(p => p.ClientID == id);
            Delete(edited);
        }
    }
}
