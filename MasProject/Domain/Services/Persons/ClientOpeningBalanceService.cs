﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Persons;
using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel;
using System.Collections.Generic;
using System.Linq;

namespace MasProject.Domain.Services.Persons
{
    public class ClientOpeningBalanceService : Repository<ClientOpeningBalance>
    {
        public ClientOpeningBalanceService(DBContext context) : base(context)
        {
        }
        public IEnumerable<ClientOpeningBalanceVM> GetAll()
        {
            return dbSet.Where(x => x.IsDeleted == false).Select(s => new ClientOpeningBalanceVM
            {
                ID = s.ID,
                DocumentNumber = s.DocumentNumber,
                EntryDate = s.EntryDate,
                BranchID =  s.BranchId,
                BranchName = s.BranchObj.Name,
                EmployeeID = s.EmployeeID,
                EmployeeName = s.EmployeeObj.UserName
            }).OrderByDescending(s => s.ID);
        }

        public ClientOpeningBalanceVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new ClientOpeningBalanceVM
            {
                ID = s.ID,
                DocumentNumber = s.DocumentNumber,
                EntryDate = s.EntryDate.Date,
                BranchID = s.BranchId,
                BranchName = s.BranchObj.Name,
                EmployeeID = s.EmployeeID,
                EmployeeName = s.EmployeeObj.Name,
                Note = s.Note,
                
            }).FirstOrDefault();

        }
        public ClientOpeningBalance Add(ClientOpeningBalanceVM balance)
        {
            ClientOpeningBalance model = new ClientOpeningBalance
            {
                ID = balance.ID,
                DocumentNumber = balance.DocumentNumber,
                EntryDate = balance.EntryDate.ToLocalTime(),
                BranchId = balance.BranchID,
                EmployeeID = balance.EmployeeID,
                Note = balance.Note
            };
            Insert(model);
            return model;
        }
        public ClientOpeningBalance Edit(ClientOpeningBalanceVM balance)
        {
            ClientOpeningBalance editedmodel = dbSet.Where(c => c.ID == balance.ID).FirstOrDefault();
            editedmodel.DocumentNumber = balance.DocumentNumber;
            editedmodel.BranchId = balance.BranchID;
            editedmodel. EmployeeID = balance.EmployeeID;
            editedmodel.Note = balance.Note;
            editedmodel.EntryDate = balance.EntryDate.ToLocalTime();
            Update(editedmodel);
            return editedmodel;
        }
        public void Delete(int id)
        {
            ClientOpeningBalance edited = dbSet.FirstOrDefault(p => p.DocumentNumber == id);
            Delete(edited);
        }
        public int GenerateSerial()
        {
            int serial = dbSet.FirstOrDefault(x => x.IsDeleted == false) != null ? dbSet.Max(c => c.DocumentNumber) + 1 : 1;
            return serial;
        }
    }

}