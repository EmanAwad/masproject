﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Persons;
using MasProject.Domain.ViewModel.Persons;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MasProject.Domain.Services.Persons
{
    public class PurchasesArchivesService : Repository<PurchasesArchives>
    {
        public PurchasesArchivesService(DBContext context) : base(context)
        {
        }
        public IEnumerable<PurchasesArchivesVM> GetAll()
        {
            try
            {
                var PurchasesArchivesList = dbSet.Where(x => x.IsDeleted == false).Select(s => new PurchasesArchivesVM
                {
                    ID = s.ID,
                    Year = s.Year,
                    Month = s.Month,
                    NetPurchases = s.NetPurchases,
                    Note = s.Note,
                    Note1 = s.Note1,
                    SupplierID = s.SupplierID,
                    SupplierName = s.SupplierObj.Name,
                }).OrderByDescending(s => s.ID);
                return PurchasesArchivesList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<PurchasesArchivesVM> GetBySupplierID(int SupplierID)
        {
            return dbSet.Where(x => x.SupplierID == SupplierID && x.IsDeleted == false).Select(s => new PurchasesArchivesVM
            {
                ID = s.ID,
                Year = s.Year,
                Month = s.Month,
                NetPurchases = s.NetPurchases,
                Note = s.Note,
                Note1 = s.Note1,
                SupplierID = s.SupplierID,
                SupplierName = s.SupplierObj.Name,
            }).ToList();
        }
        public PurchasesArchivesVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new PurchasesArchivesVM
            {
                ID = s.ID,
                Year = s.Year,
                Month = s.Month,
                NetPurchases = s.NetPurchases,
                Note = s.Note,
                Note1 = s.Note1,
                SupplierID = s.SupplierID,
                SupplierName = s.SupplierObj.Name,
            }).FirstOrDefault();

        }
        public PurchasesArchives Add(PurchasesArchivesVM sale)
        {
            PurchasesArchives model = new PurchasesArchives
            {
                ID = sale.ID,
                Year = sale.Year,
                Month = sale.Month,
                NetPurchases = sale.NetPurchases ,
                Note = sale.Note,
                Note1 = sale.Note1,
                SupplierID = sale.SupplierID,
            };
            Insert(model);
            return model;
        }
        public PurchasesArchives Edit(PurchasesArchivesVM sale)
        {
            PurchasesArchives model = new PurchasesArchives
            {
                ID = sale.ID,
                Year = sale.Year,
                Month = sale.Month,
                NetPurchases = sale.NetPurchases ,
                Note = sale.Note,
                Note1 = sale.Note1,
                SupplierID = sale.SupplierID,
                IsDeleted = false,
            };

            Update(model);
            return model;
        }
        public void Delete(int id)
        {
            PurchasesArchives edited = dbSet.FirstOrDefault(p => p.SupplierID == id);
            if (edited !=null)
            {
                Delete(edited);
            }
           
        }
        public void DeletePhysical(int id)
        {
            List<PurchasesArchives> DeleteList = dbSet.Where(p => p.SupplierID == id).ToList();
            foreach (var item in DeleteList)
            {
                Remove(item);
            }
        }
    }
}