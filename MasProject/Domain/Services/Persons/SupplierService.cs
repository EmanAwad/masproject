﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Persons;
using MasProject.Domain.ViewModel.Persons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MasProject.Domain.ViewModel;
namespace MasProject.Domain.Services.Persons
{
   public class SupplierService : Repository<Supplier>
    {
        public SupplierService(DBContext context) : base(context)
        {
        }
        public IEnumerable<LookupKeyValueVM> GetSuppliers()
        {
            return dbSet.Where(x => x.IsDeleted == false && x.IsHide == false).Select(s => new LookupKeyValueVM
            {
                ID = s.ID,
                Name = s.Name,
                Type=2
            }).OrderBy(s => s.ID);
        }
        public IEnumerable<SupplierVM> GetAll()
        {
            return dbSet.Where(x => x.IsDeleted == false && x.IsHide == false).Select(s => new SupplierVM
            {
                ID = s.ID,
                Name = s.Name,
                Note = s.Note,
                Phone = s.Phone,
                Mobile=s.Mobile,
                Address = s.Address,
                Mail = s.Mail,
                Code = s.Code,
                CountryID = s.CountryID,
                CityID = s.CityID,
                AreaID = s.AreaID,
                Taxfile= s.Taxfile,
                CurrencyID=s.CurrencyID,
                EmployeeName = s.EmployeeName,
                WebSiteURL = s.WebSiteURL,
                IsDeleted = s.IsDeleted,
                CreatedDate = s.CreatedDate,
                arrangement = s.arrangement,
            }).OrderBy(s => s.arrangement);
        }

        public SupplierVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new SupplierVM
            {
                ID = s.ID,
                Name = s.Name,
                Note = s.Note,
                Phone = s.Phone,
                Mobile = s.Mobile,
                Address = s.Address,
                Mail = s.Mail,
                Code = s.Code,
                CountryID = s.CountryID,
                CityID = s.CityID,
                AreaID = s.AreaID,
                Taxfile=s.Taxfile,
                CurrencyID = s.CurrencyID,
                EmployeeName = s.EmployeeName,
                WebSiteURL = s.WebSiteURL,
                IsDeleted =s.IsDeleted,
                CreatedDate = s.CreatedDate,
                arrangement = s.arrangement,
                IsHide = s.IsHide
            }).FirstOrDefault();
        }
        public int GetCurrencyBySupplierID(int Id)
        {
            return dbSet.Where(x => x.ID == Id && !x.IsDeleted).FirstOrDefault().CurrencyID ?? 0;
        }
        public Supplier Add(SupplierVM sup)
        {
            Supplier model = new Supplier
            {
                ID = sup.ID,
                Name = sup.Name,
                Note = sup.Note,
                Phone = sup.Phone,
                Mobile = sup.Mobile,
                Address = sup.Address,
                Mail = sup.Mail,
                Code = sup.Code,
                CountryID = sup.CountryID,
                CityID = sup.CityID,
                AreaID = sup.AreaID,
                Taxfile = sup.Taxfile,
                CurrencyID = sup.CurrencyID,
                EmployeeName = sup.EmployeeName,
                WebSiteURL = sup.WebSiteURL,
                IsDeleted = false,
                CreatedDate=DateTime.Now,
                arrangement = sup.arrangement,
                IsHide = sup.IsHide
            };
            Insert(model);
            return model;
        }
        public Supplier Edit(SupplierVM sup)
        {
            Supplier edited = dbSet.FirstOrDefault(p => p.ID == sup.ID);

            edited.Name = sup.Name;
            edited.Note = sup.Note;
            edited.Phone = sup.Phone;
            edited.Mobile = sup.Mobile;
            edited.Address = sup.Address;
            edited.Mail = sup.Mail;
            edited.Code = sup.Code;
            edited.CountryID = sup.CountryID;
            edited.CityID = sup.CityID;
            edited.AreaID = sup.AreaID;
            edited.Taxfile = sup.Taxfile;
            edited.CurrencyID = sup.CurrencyID;
            edited.EmployeeName = sup.EmployeeName;
            edited.WebSiteURL = sup.WebSiteURL;
            edited.ModifiedDate = DateTime.Now;
            edited.arrangement = sup.arrangement;
            edited.IsHide = sup.IsHide;

            Update(edited);
            return edited;
        }
        public void Delete(int id)
        {
            Supplier edited = dbSet.FirstOrDefault(p => p.ID == id);
            Delete(edited);
        }
        public string GetTaxFile(int id)
        {
            string ReturnStr = "";
            var temp = dbSet.FirstOrDefault(x => x.ID == id && x.IsDeleted == false);
            if (temp != null)
            {
                ReturnStr = temp.Taxfile == null ? "" : temp.Taxfile;
            }
            return ReturnStr;
        }
        public bool IsExistCode(string SupplierCode, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.Code == SupplierCode && g.IsDeleted == false);
            }
            else
            {
                return dbSet.Any(g => g.Code == SupplierCode  && g.IsDeleted == false && g.ID != id);
            }
        }
        public bool IsExistName(string ClientName, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.Name == ClientName & g.IsDeleted == false);
            }
            else
            {
                return dbSet.Any(g => g.Name == ClientName & g.ID != id & g.IsDeleted == false);
            }
        }
        public bool IsExistMail(string ClientMail, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.Mail == ClientMail & g.IsDeleted == false);
            }
            else
            {
                return dbSet.Any(g => g.Mail == ClientMail & g.ID != id & g.IsDeleted == false);
            }
        }
        public bool IsExistPhone(string ClientPhone, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.Phone == ClientPhone & g.IsDeleted == false);
            }
            else
            {
                return dbSet.Any(g => g.Phone == ClientPhone & g.ID != id & g.IsDeleted == false);
            }
        }
        public bool IsExistMobile(string ClientMobile, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.Mobile == ClientMobile & g.IsDeleted == false);
            }
            else
            {
                return dbSet.Any(g => g.Mobile == ClientMobile & g.ID != id & g.IsDeleted == false);
            }
        }
        public void Hide(int id)
        {

            Supplier Hidden = dbSet.FirstOrDefault(p => p.ID == id & p.IsHide == true);
            Hidden.IsHide = false;
            Update(Hidden);
        }

        public bool IsExistArrangement(int arrangement, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.arrangement == arrangement & g.IsDeleted == false);
            }
            else
            {
                return dbSet.Any(g => g.arrangement == arrangement & g.IsDeleted == false & g.ID != id);
            }
        }
    }
}
