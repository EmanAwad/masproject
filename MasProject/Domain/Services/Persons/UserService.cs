﻿using AutoMapper;
using MasProject.Data.DataAccess;
using MasProject.Data.Models.Persons;
using MasProject.Domain.ViewModel.Persons;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MasProject.Domain.Services.Persons
{
    public class UserService : Repository<Employee>
    {
        public UserService(DBContext context) : base(context)
        {

        }
        public IEnumerable<EmployeeVM> GetAllUsers()
        {
            return dbSet.Where(x => x.IsDeleted == false && x.IsUser == true && x.IsHide == false).Select(s => new EmployeeVM
            {
                ID = s.ID,
                Name = s.Name,
                UserName=s.UserName
            }).OrderBy(s => s.ID);
        }
    }
}
