﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Persons;
using MasProject.Domain.ViewModel.Persons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasProject.Domain.Services.Persons
{
    public class SalesArchivesService : Repository<SalesArchives>
    {
        public SalesArchivesService(DBContext context) : base(context)
        {
        }
        public IEnumerable<SalesArchivesVM> GetAll()
        {
            try
            {
                var SalesArchivesList = dbSet.Where(x => x.IsDeleted == false).Select(s => new SalesArchivesVM
                {
                    ID = s.ID,
                    Year = s.Year,
                    Month = s.Month,
                    NetSales = s.NetSales,
                    Note = s.Note,
                    Note1 = s.Note1,
                    ClientID = s.ClientID,
                    ClientName = s.ClientObj.Name,
                }).OrderByDescending(s => s.ID);
                return SalesArchivesList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<SalesArchivesVM> GetByClientID(int ClientID)
        {
            return dbSet.Where(x => x.ClientID == ClientID && x.IsDeleted == false).Select(s => new SalesArchivesVM
            {
                ID = s.ID,
                Year = s.Year,
                Month = s.Month,
                NetSales = s.NetSales,
                Note = s.Note,
                Note1 = s.Note1,
                ClientID = s.ClientID,
                ClientName = s.ClientObj.Name,
            }).ToList();
        }
        public SalesArchivesVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new SalesArchivesVM
            {
                ID = s.ID,
                Year = s.Year,
                Month = s.Month,
                NetSales = s.NetSales,
                Note = s.Note,
                Note1 = s.Note1,
                ClientID = s.ClientID,
                ClientName = s.ClientObj.Name,
            }).FirstOrDefault();

        }
        public SalesArchives Add(SalesArchivesVM sale)
        {
            SalesArchives model = new SalesArchives
            {
                ID = sale.ID,
                Year = sale.Year,
                Month = sale.Month,
                NetSales = sale.NetSales ,
                Note = sale.Note,
                Note1 = sale.Note1,
                ClientID = sale.ClientID,
            };
            Insert(model);
            return model;
        }
        public SalesArchives Edit(SalesArchivesVM sale)
        {
            SalesArchives model = new SalesArchives
            {
                ID = sale.ID,
                Year = sale.Year,
                Month = sale.Month,
                NetSales = sale.NetSales ,
                Note = sale.Note,
                Note1 = sale.Note1,
                ClientID = sale.ClientID,
                IsDeleted = false,
            };

            Update(model);
            return model;
        }
        public void Delete(int id)
        {
            SalesArchives edited = dbSet.FirstOrDefault(p => p.ClientID == id);
            if (edited != null)
            {
                Delete(edited);
            }
           
        }
        public void DeletePhysical(int id)
        {
            List<SalesArchives> DeleteList = dbSet.Where(p => p.ClientID == id).ToList();
            foreach (var item in DeleteList)
            {
                Remove(item);
            }
        }
    }
}