﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Persons;
using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.Services.TransServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasProject.Domain.Services.Persons
{
    public class SupplierOpeningBalanceDetailsService : Repository<SupplierOpeningBalanceDetails>
    {
        private DBContext _context;
        public SupplierOpeningBalanceDetailsService(DBContext context) : base(context)
        {
            _context = context;
        }

        public IEnumerable<SupplierOpeningBalaneDetailsVM> GetAll()
        {
            return dbSet.Where(x => x.IsDeleted == false).Select(s => new SupplierOpeningBalaneDetailsVM
            {
                ID = s.ID,
                Amount = s.Amount,
               
               SupplierDocumentNumber = s.SupplierDocumentNumber,
                Note = s.Note,
                Ratio = s.Ratio,
                CurrencyID = s.CurrencyID,
                SupplierID = s.SupplierID,
              
            }).OrderByDescending(s => s.ID);
        }

        public List<SupplierOpeningBalaneDetailsVM> GetByDocumentNumber(int DocumentNumber)
        {
            return dbSet.Where(x => x.SupplierDocumentNumber == DocumentNumber && x.IsDeleted == false).Select(s => new SupplierOpeningBalaneDetailsVM
            {
                ID = s.ID,
                Amount = s.Amount,
               SupplierDocumentNumber = s.SupplierDocumentNumber,
                Note = s.Note,
                Ratio = s.Ratio,
                CurrencyID = s.CurrencyID,
                SupplierID = s.SupplierID
            }).ToList();

        }


        public SupplierOpeningBalanceDetails Add(SupplierOpeningBalaneDetailsVM balance)
        {
            SupplierOpeningBalanceDetails model = new SupplierOpeningBalanceDetails
            {
                Amount = balance.Amount,
                SupplierDocumentNumber = balance.SupplierDocumentNumber,
                Note = balance.Note,
                Ratio = balance.Ratio,
                CurrencyID = balance.CurrencyID,
                SupplierID = balance.SupplierID
            };
            Insert(model);
            return model;
        }

        public void Delete(int DocumentNumber, int SerialNumber)
        {
            List<SupplierOpeningBalanceDetails> DeleteList = dbSet.Where(p => p.SupplierDocumentNumber == DocumentNumber).ToList();
            foreach (var item in DeleteList)
            {
                Delete(item);
                TransSupplierService transSupplierService = new TransSupplierService(_context);
                int Id = transSupplierService.RecalculateSupplierBalance(item.SupplierID.Value, SerialNumber);
                transSupplierService.Delete(Id);
            }
        }


        public void DeletePhysical(int DocumentNumber)
        {
            List<SupplierOpeningBalanceDetails> DeleteList = dbSet.Where(p => p.SupplierDocumentNumber == DocumentNumber).ToList();
            foreach (var item in DeleteList)
            {
                item.IsDeleted = true;
                Remove(item);
            }
        }
    }
}
