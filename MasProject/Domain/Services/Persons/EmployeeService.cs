﻿using AutoMapper;
using MasProject.Data.DataAccess;
using MasProject.Data.Models.Hierarchy;
using MasProject.Data.Models.Persons;
using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MasProject.Domain.Services.Persons
{
    public class EmployeeService : Repository<Employee>
    {
        public EmployeeService(DBContext context) : base(context)
        {
        }
        public IEnumerable<LookupKeyValueVM> GetEmployees()
        {
            return dbSet.Where(x => x.IsDeleted == false && x.IsHide == false).Select(s => new LookupKeyValueVM
            {
                ID = s.ID,
                Name = s.Name,
                Type=3
            }).OrderBy(s => s.ID);
        }
        public IEnumerable<EmployeeVM> GetAll()
        {
            try
            {
                var EmployeeList= dbSet.Where(x => x.IsDeleted == false && x.IsHide == false).Select(s => new EmployeeVM
                {
                    ID = s.ID,
                    Name = s.Name,
                    DateOfBirth = s.DateOfBirth,
                    JObTitle = s.JObTitle,
                    Image = s.Image,
                    Annual = s.Annual,
                    AnnualIncrease = s.AnnualIncrease,
                    StartSalary = s.StartSalary,
                    JoiningDate = s.JoiningDate,
                    Note = s.Note,
                    Phone = s.Phone,
                    Address = s.Address,
                    Mail = s.Mail,
                    IsUser = s.IsUser,
                    UserName = s.UserName,
                    Passwordouble = s.Passwordouble,
                    Code = s.Code,
                    CountryID = s.CountryID,
                    CityID = s.CityID,
                    AreaID = s.AreaID,
                    DepartmentName = s.DepartmentObj.Name,
                    BranchName = s.BranchObj.Name,
                    NationalID = s.NationalID,
                    Mobile = s.Mobile,
                    arrangement = s.arrangement,
                }).OrderBy(s => s.arrangement);
                return EmployeeList;
            }
            catch(Exception ex)
            {
                return null;
            }
        }
        public IEnumerable<EmployeeVM> GetAllWithHidden()
        {
            try
            {
                var EmployeeList = dbSet.Where(x => x.IsDeleted == false).Select(s => new EmployeeVM
                {
                    ID = s.ID,
                    Name = s.Name,
                    DateOfBirth = s.DateOfBirth,
                    JObTitle = s.JObTitle,
                    Image = s.Image,
                    Annual = s.Annual,
                    AnnualIncrease = s.AnnualIncrease,
                    StartSalary = s.StartSalary,
                    JoiningDate = s.JoiningDate,
                    Note = s.Note,
                    Phone = s.Phone,
                    Address = s.Address,
                    Mail = s.Mail,
                    IsUser = s.IsUser,
                    UserName = s.UserName,
                    Passwordouble = s.Passwordouble,
                    Code = s.Code,
                    CountryID = s.CountryID,
                    CityID = s.CityID,
                    AreaID = s.AreaID,
                    DepartmentName = s.DepartmentObj.Name,
                    BranchName = s.BranchObj.Name,
                    NationalID = s.NationalID,
                    Mobile = s.Mobile,
                    arrangement = s.arrangement,
                }).OrderBy(s => s.arrangement);
                return EmployeeList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public EmployeeVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new EmployeeVM
            {
                ID = s.ID,
                Name = s.Name,
                DateOfBirth = s.DateOfBirth.Date,
                JObTitle = s.JObTitle,
                Image = s.Image,
                Annual = s.Annual,
                AnnualIncrease = s.AnnualIncrease,
                StartSalary = s.StartSalary,
                JoiningDate = s.JoiningDate.Date,
                Note = s.Note,
                Phone = s.Phone,
                Address = s.Address,
                Mail = s.Mail,
                IsUser = s.IsUser,
                UserName = s.UserName,
                Passwordouble = s.Passwordouble,
                Code = s.Code,
                CountryID = s.CountryID,
                CityID = s.CityID,
                AreaID = s.AreaID,
                DepartmentID = (int)(s.DepartmentID),
                BranchID=(int)s.BranchId,
                NationalID = s.NationalID,
                Mobile = s.Mobile,
                arrangement = s.arrangement,
                IsHide = s.IsHide,
                LeavingDate=s.LeavingDate,
                PhoneWorkDescription=s.PhoneWorkDescription,
                PhoneWork=s.PhoneWork,
            }).FirstOrDefault();
        }
        public Employee Add(EmployeeVM emp)
        {
            Employee model = new Employee
            {
                ID = emp.ID,
                Name = emp.Name,
                DateOfBirth = emp.DateOfBirth.ToLocalTime(),
                JObTitle = emp.JObTitle,
                Image = emp.Image,
                Annual = emp.Annual,
                AnnualIncrease = emp.AnnualIncrease,
                StartSalary = emp.StartSalary,
                JoiningDate = emp.JoiningDate.ToLocalTime(),
                Note = emp.Note,
                Phone = emp.Phone,
                Address = emp.Address,
                Mail = emp.Mail,
                IsUser = emp.IsUser,
                UserName = emp.UserName,
                Passwordouble = emp.Passwordouble,
                Code = emp.Code,
                CountryID = emp.CountryID,
                CityID = emp.CityID,
                AreaID = emp.AreaID,
                DepartmentID = emp.DepartmentID,
                BranchId = emp.BranchID,
                NationalID = emp.NationalID,
                arrangement = emp.arrangement,
                Mobile = emp.Mobile,
                IsHide = emp.IsHide,
                LeavingDate = emp.LeavingDate,
                PhoneWorkDescription = emp.PhoneWorkDescription,
                PhoneWork = emp.PhoneWork,
            };
            Insert(model);
            return model;
        }
        public Employee Edit(EmployeeVM emp)
        {
            if(emp.IsUser==false)
            {
                emp.UserName = "";
                emp.Passwordouble = "";
            }
            Employee edited = dbSet.FirstOrDefault(p => p.ID == emp.ID);

            edited.Name = emp.Name;
            edited.DateOfBirth = emp.DateOfBirth.ToLocalTime();
            edited.JObTitle = emp.JObTitle;
            edited.Image = emp.Image;
            edited.Annual = emp.Annual;
            edited.AnnualIncrease = emp.AnnualIncrease;
            edited.StartSalary = emp.StartSalary;
            edited.JoiningDate = emp.JoiningDate.ToLocalTime();
            edited.Note = emp.Note;
            edited.Phone = emp.Phone;
            edited.Address = emp.Address;
            edited.Mail = emp.Mail;
            edited.IsUser = emp.IsUser;
            edited.UserName = emp.UserName;
            edited.Passwordouble = emp.Passwordouble;
            edited.Code = emp.Code;
            edited.CountryID = emp.CountryID;
            edited.CityID = emp.CityID;
            edited.AreaID = emp.AreaID;
            edited.DepartmentID = emp.DepartmentID;
            edited.BranchId = emp.BranchID;
            edited.NationalID = emp.NationalID;
            edited.Mobile = emp.Mobile;
            edited.arrangement = emp.arrangement;
            edited.IsHide = emp.IsHide;
            edited.LeavingDate = emp.LeavingDate;
            edited.PhoneWorkDescription = emp.PhoneWorkDescription;
            edited.PhoneWork = emp.PhoneWork;
            Update(edited);
            return edited;
        }
        public void Delete(int id)
        {
            Employee edited = dbSet.FirstOrDefault(p => p.ID == id);
            Delete(edited);
           
        }
        public bool IsExistCode(string EmpCode, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.Code == EmpCode & g.IsDeleted == false);
            }
            else
            {
                return dbSet.Any(g => g.Code == EmpCode & g.ID != id & g.IsDeleted == false);
            }
        }
        public bool IsExistName(string EmpName, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.Name == EmpName);
            }
            else
            {
                return dbSet.Any(g => g.Name == EmpName & g.ID != id);
            }
        }
        public bool IsExistMail(string EmpMail, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.Mail == EmpMail & g.IsDeleted == false);
            }
            else
            {
                return dbSet.Any(g => g.Mail == EmpMail & g.ID != id & g.IsDeleted == false);
            }
        }
        public bool IsExistPhone(string EmpPhone, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.Phone == EmpPhone & g.IsDeleted == false);
            }
            else
            {
                return dbSet.Any(g => g.Phone == EmpPhone & g.ID != id & g.IsDeleted == false);
            }
        }
        public bool IsExistMobile(string EmpMobile, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.Mobile == EmpMobile & g.IsDeleted == false);
            }
            else
            {
                return dbSet.Any(g => g.Mobile == EmpMobile & g.ID != id & g.IsDeleted == false);
            }
        }

        public void Hide(int id)
        {

            Employee Hidden = dbSet.FirstOrDefault(p => p.ID == id & p.IsHide == true);
            Hidden.IsHide = false;
            Update(Hidden);
        }
        public bool IsExistArrangement(int arrangement, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.arrangement == arrangement & g.IsDeleted == false);
            }
            else
            {
                return dbSet.Any(g => g.arrangement == arrangement & g.IsDeleted == false & g.ID != id);
            }
        }
    }
}
