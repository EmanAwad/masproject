﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Persons;
using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace MasProject.Domain.Services.Persons
{
    public class ClientsService : Repository<Clients>
    {
        public ClientsService(DBContext context) : base(context)
        {
        }
        public IEnumerable<LookupKeyValueVM> GetClients()
        {
            return dbSet.Where(x => x.IsDeleted == false && x.IsHide == false).Select(s => new LookupKeyValueVM
            {
                ID = s.ID,
                Name = s.Name,
                Type=1,
                arrangement = s.arrangement
            }).OrderBy(s => s.arrangement);
        }
        public IEnumerable<ClientsVM> GetAll()
        {
            return dbSet.Where(x => x.IsDeleted == false && x.IsHide == false).Select(s => new ClientsVM
            {
                ID = s.ID,
                Name = s.Name,
                Address = s.Address,
                Mail = s.Mail,
                Phone = s.Phone,
                Code = s.Code,
                Taxfile = s.Taxfile,
                WebSiteURL = s.WebSiteURL,
                Dealingtype = s.Dealingtype ,
                DealingtypeName=s.DealTypeObj.Name,
                FirstDealingDate = s.FirstDealingDate,
                LastDealingDate = s.LastDealingDate,
                MonthlyAverage = s.MonthlyAverage,
                IsBlocked = s.IsBlocked,
                Note = s.Note,
                EmployeeName = s.EmployeeName,
                BranchName = s.BranchObj.Name,
                Mobile=s.Mobile,
                Mobile2=s.Mobile2,
                arrangement = s.arrangement,
                //GeneratedCode= (s.Code== null || s.Code.Length != 8)? s.Code : s.Code.Substring(0,2)+ s.Code.Substring(4, s.Code.Length -2)  
            }).OrderBy(s => s.arrangement);//.OrderBy(s => s.GeneratedCode);
        }



        public ClientsVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new ClientsVM
            {
                ID = s.ID,
                Name = s.Name,
                Address = s.Address,
                Phone = s.Phone,
                Mail = s.Mail,
                Code = s.Code,
                CountryID = s.CountryID,
                CityID = s.CityID,
                Taxfile = s.Taxfile,
                WebSiteURL = s.WebSiteURL,
                Dealingtype = s.Dealingtype ,
                FirstDealingDate = s.FirstDealingDate.Date,
                LastDealingDate = s.LastDealingDate.Date,
                MonthlyAverage = s.MonthlyAverage,
                IsBlocked = s.IsBlocked,
                Note = s.Note,
                BranchID = s.BranchObj.ID,
                EmployeeName = s.EmployeeName,
                Mobile = s.Mobile,
                Mobile2 = s.Mobile2,
                arrangement = s.arrangement,
                IsHide = s.IsHide
            }).FirstOrDefault();

        }
        public string GetTaxFile(int id)
        {
            string ReturnStr = "";
            var temp = dbSet.FirstOrDefault(x => x.ID == id && x.IsDeleted == false);
            if (temp != null)
            {
                ReturnStr = temp.Taxfile==null?"":temp.Taxfile;
            }
            return ReturnStr;
        }
        public int GetDealType(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).FirstOrDefault()!= null ? dbSet.Where(x => x.ID == id && x.IsDeleted == false).FirstOrDefault().Dealingtype.Value : 0;
        }
        public Clients Add(ClientsVM client)
        {
            try
            {
                Clients model = new Clients
                {
                    // ID = client.ID,
                    Name = client.Name,
                    Address = client.Address,
                    Phone = client.Phone,
                    Mail = client.Mail,
                    Code = client.Code,
                 //   CountryID = client.CountryID,
                    CityID = client.CityID,
                    Taxfile = client.Taxfile,
                    WebSiteURL = client.WebSiteURL,
                    Dealingtype = client.Dealingtype,
                    FirstDealingDate = client.FirstDealingDate.ToLocalTime(),
                    LastDealingDate = client.LastDealingDate.ToLocalTime(),
                    MonthlyAverage = client.MonthlyAverage ,
                    IsBlocked = client.IsBlocked,
                    Note = client.Note,
                    EmployeeName = client.EmployeeName,
                    Mobile = client.Mobile,
                    BranchId = client.BranchID,
                    Mobile2 = client.Mobile2,
                    arrangement = client.arrangement,
                    IsHide = client.IsHide
                };
                Insert(model);
                return model;
            }


            catch (Exception ex)
            {
                return null;
            }

        }
        public Clients Edit(ClientsVM client)
        {
            Clients edited = dbSet.FirstOrDefault(p => p.ID == client.ID);

            //ID = client.ID,
            edited.Name = client.Name;
            edited.Address = client.Address;
            edited.Phone = client.Phone;
            edited.Mail = client.Mail;
            edited.Code = client.Code;
            edited.CountryID = client.CountryID;
            edited.CityID = client.CityID;
            edited.Taxfile = client.Taxfile;
            edited.WebSiteURL = client.WebSiteURL;
            edited.Dealingtype = client.Dealingtype;
            edited.FirstDealingDate = client.FirstDealingDate.ToLocalTime();
            edited.LastDealingDate = client.LastDealingDate.ToLocalTime();
            edited.MonthlyAverage = client.MonthlyAverage;
            edited.IsBlocked = client.IsBlocked;
            edited.Note = client.Note;
            edited.EmployeeName = client.EmployeeName;
            edited.Mobile = client.Mobile;
            edited.BranchId = client.BranchID;
            edited.Mobile2 = client.Mobile2;
            edited.arrangement = client.arrangement;
            edited.IsHide = client.IsHide;
            Update(edited);
            return edited;
        }
        public void Delete(int id)
        {
            Clients edited = dbSet.FirstOrDefault(p => p.ID == id);
            Delete(edited);
        }
        public bool IsExistArrangment(int? arrangment, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.arrangement == arrangment);
            }
            else
            {
                return dbSet.Any(g => g.arrangement == arrangment & g.ID != id);
            }
        }
        public bool IsExistCode(string ClientCode, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.Code == ClientCode);
            }
            else
            {
                return dbSet.Any(g => g.Code == ClientCode & g.ID != id);
            }
        }
        public bool IsExistName(string ClientName, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.Name == ClientName & g.IsDeleted == false);
            }
            else
            {
                return dbSet.Any(g => g.Name == ClientName & g.ID != id & g.IsDeleted == false);
            }
        }
        public bool IsExistMail(string ClientMail, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.Mail == ClientMail & g.IsDeleted == false);
            }
            else
            {
                return dbSet.Any(g => g.Mail == ClientMail & g.ID != id & g.IsDeleted == false);
            }
        }
        public bool IsExistPhone(string ClientPhone, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.Phone == ClientPhone & g.IsDeleted == false);
            }
            else
            {
                return dbSet.Any(g => g.Phone == ClientPhone & g.ID != id & g.IsDeleted == false);
            }
        }
        public bool IsExistMobile(string ClientMobile, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.Mobile == ClientMobile & g.IsDeleted == false);
            }
            else
            {
                return dbSet.Any(g => g.Mobile == ClientMobile & g.ID != id & g.IsDeleted == false);
            }
        }
        public bool IsExistMobile2(string ClientMobile, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.Mobile2 == ClientMobile & g.IsDeleted == false);
            }
            else
            {
                return dbSet.Any(g => g.Mobile2 == ClientMobile & g.ID != id & g.IsDeleted == false);
            }
        }
        public void Hide(int id)
        {

            Clients Hidden = dbSet.FirstOrDefault(p => p.ID == id & p.IsHide == true);
            Hidden.IsHide = false;
            Update(Hidden);
        }

       
    }
}