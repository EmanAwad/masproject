﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Persons;
using MasProject.Domain.ViewModel.Persons;
using MasProject.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MasProject.Domain.Services.Persons
{
    public class SocialMediaService : Repository<SocialMedia>
    {
        public SocialMediaService(DBContext context) : base(context)
        {
        }
        public IEnumerable<SocialMediaVM> GetAll()
        {
            return dbSet.Where(x => x.IsDeleted == false).Select(s => new SocialMediaVM
            {
                ID = s.ID,
                Name = s.Name
            }).OrderBy(s => s.ID);
        }

    }
}
