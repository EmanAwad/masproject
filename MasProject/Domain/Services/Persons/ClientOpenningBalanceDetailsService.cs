﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Persons;
using MasProject.Domain.Services.TransServices;
using MasProject.Domain.ViewModel.Persons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasProject.Domain.Services.Persons
{
   public class ClientOpenningBalanceDetailsService : Repository<ClientOpeningBalanceDetails>
    {
        private DBContext _context;
        public ClientOpenningBalanceDetailsService(DBContext context) : base(context)
        {
            _context = context;
        }
        public IEnumerable<ClientOpeningBalanceDetailsVM> GetAll()
        {
            return dbSet.Where(x => x.IsDeleted == false).Select(s => new ClientOpeningBalanceDetailsVM
            {
                ID = s.ID,
                Amount = s.Amount,
                ClientDocumentNumber = s.ClientDocumentNumber,
                Note = s.Note,
                ClientID = s.ClientID
            }).OrderByDescending(s => s.ID);
        }

        public List<ClientOpeningBalanceDetailsVM> GetByDocumentNumber(int DocumentNumber)
        {
            return dbSet.Where(x => x.ClientDocumentNumber == DocumentNumber && x.IsDeleted == false).Select(s => new ClientOpeningBalanceDetailsVM
            {
                ID = s.ID,
                Amount = s.Amount,
                ClientDocumentNumber = s.ClientDocumentNumber,
                Note = s.Note,
                ClientID = s.ClientID,
                ClientName = s.ClientObj.Name
            }).ToList();

        }
        public ClientOpeningBalanceDetails Add(ClientOpeningBalanceDetailsVM balance)
        {
            ClientOpeningBalanceDetails model = new ClientOpeningBalanceDetails
            {
                Amount = balance.Amount,
                ClientDocumentNumber = balance.ClientDocumentNumber,
                Note = balance.Note,
                ClientID = balance.ClientID
            };
            Insert(model);
            return model;
        }
        public void Delete(int DocumentNumber,int SerialNumber)
        {
            List<ClientOpeningBalanceDetails> DeleteList = dbSet.Where(p => p.ClientDocumentNumber == DocumentNumber).ToList();
            foreach (var item in DeleteList)
            {
                Delete(item);
                TransClientService transClientService = new TransClientService(_context);
                int Id = transClientService.RecalculateClientBalance(item.ClientID.Value, SerialNumber);
                transClientService.Delete(Id);
            }
        }
        public void DeletePhysical(int DocumentNumber)
        {
            List<ClientOpeningBalanceDetails> DeleteList = dbSet.Where(p => p.ClientDocumentNumber == DocumentNumber).ToList();
            foreach (var item in DeleteList)
            {
                item.IsDeleted = true;
                Remove(item);
            }
        }
    }
}