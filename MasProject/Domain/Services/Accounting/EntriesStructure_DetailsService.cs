﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Accounting;
using MasProject.Domain.ViewModel.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasProject.Domain.Services.Accounting
{
  public  class EntriesStructure_DetailsService : Repository<EntriesStructuring_Details>
    {
        DBContext _Context;
        public EntriesStructure_DetailsService(DBContext context) : base(context)
        {
            _Context = context;
        }

        public List<EntriesStructuring_DetailsVM> Get(int id)
        {
            return dbSet.Where(x => x.EntriesStructuringId == id && x.IsDeleted == false).Select(s => new EntriesStructuring_DetailsVM
            {
                ID = s.ID,
                AccountNo = s.AccountNo,
                AccountName = s.AccountName,
                Debit = s.Debit,
                Credit = s.Credit,
                CostCenterId = s.CostCenterId,
                CostCenterName = s.Name,
                //   Total = s.Total,
                TotalDR = s.TotalDR,
                TotalCR = s.TotalCR,
                DRCR_Difference = s.DRCR_Difference
            }).ToList();
        }

        public EntriesStructuring_Details Add(EntriesStructuring_DetailsVM s)
        {
            var newModel = new EntriesStructuring_Details
            {
                //ID = s.ID,
                AccountNo = (int)s.AccountNo,
                AccountName = s.AccountName,
                Debit = s.Debit,
                Credit = s.Credit,
                CostCenterId = s.CostCenterId,
                Name = s.CostCenterName,
                TotalDR = s.TotalDR,
                TotalCR = s.TotalCR,
                DRCR_Difference = (int)s.DRCR_Difference,
                EntriesStructuringId = s.EntriesStructuringId,
                //   CreatedBy = s.CreatedBy, 
                CreatedDate = DateTime.Today,
                IsDeleted = false
            };
            Insert(newModel);
            return newModel;
        }


        public EntriesStructuring_Details Edit(EntriesStructuring_DetailsVM model)
        {
            EntriesStructuring_Details oldModel = dbSet.FirstOrDefault(x => x.ID == model.ID);
            oldModel.ModifiedDate = DateTime.Today;
            oldModel.AccountNo = (int)model.AccountNo;
            oldModel.AccountName = model.AccountName;
            oldModel.Debit = (int)model.Debit;
            oldModel.Credit = (int)model.Credit;
            oldModel.CostCenterId = model.CostCenterId;
            oldModel.DRCR_Difference = (int)model.DRCR_Difference;
            oldModel.TotalDR = model.TotalDR;
            oldModel.TotalCR = model.TotalCR;
            oldModel.Name = model.CostCenterName;
            //var newModel = new EntriesDetails
            //{
            //    ID = model.ID,
            //    ModifiedDate = DateTime.Today,
            //    AccountNo = (int)model.AccountNo,
            //    AccountName = model.AccountName,
            //    Debit = (int)model.Debit,
            //    Credit = (int)model.Credit,
            //    CostCenterId = model.CostCenterId,
            //    //  ModifiedBy = model.ModifiedBy, 
            //    //     Total = (int)model.Total,
            //    DRCR_Difference = (int)model.DRCR_Difference,
            //     TotalDR = model.TotalDR,
            //    TotalCR = model.TotalCR,
            //};
            Update(oldModel);
            return oldModel;
        }


        public void Delete(int id)
        {
            List<EntriesStructuring_Details> edited = dbSet.Where(p => p.EntriesStructuringId == id).ToList();
            foreach (var item in edited)
            {
                //item.IsDeleted = true;
                Delete(item);
            }
        }
    }
}
