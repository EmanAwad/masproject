﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasProject.Data.Models.Accounting;
using MasProject.Data.DataAccess;
using MasProject.Domain.ViewModel.Accounting;
namespace MasProject.Domain.Services.Accounting
{
    public class AccountingGuideService : Repository<AccountingGuide>
    {
        DBContext _Context;
        public AccountingGuideService(DBContext context) : base(context)
        {
            _Context = context;
        }
        public AccountingGuideVM GetAccountCodeById(int id)
        {
            AccountingGuideVM Item = dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new AccountingGuideVM
            {
               AccountCode = s.AccountCode
            }).FirstOrDefault();
            return Item;
        }
        public AccountingGuideVM GetAccountNameByCode(int id)
        {
            AccountingGuideVM Item = dbSet.Where(x => x.AccountCode == id && x.IsDeleted == false).Select(s => new AccountingGuideVM
            {
                Name = s.Name
            }).FirstOrDefault();
            return Item;
        }
        public IEnumerable<AccountingGuideVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new AccountingGuideVM
            {
                ID = s.ID,
                AccountCode=s.AccountCode,
                Name = s.Name,
                ParentId = s.ParentId.HasValue ? s.ParentId.Value : 0,
                ParentCode =s.ParentObj != null ? s.ParentObj.Code : 0,
                FlagType = s.FlagType,
                CostCenter = s.CostCenter
            }).OrderByDescending(s => s.ID);
        }
        public IEnumerable<AccountingGuideVM> GetCostCenters()
        {
            return dbSet.Where(g => g.IsDeleted == false && g.CostCenter == true).Select(s => new AccountingGuideVM
            {
                ID = s.ID,
                AccountCode = s.AccountCode,
                Name = s.Name,
                ParentId = s.ParentId.HasValue ? s.ParentId.Value : 0,
                ParentCode = s.ParentObj != null ? s.ParentObj.Code : 0,
                FlagType = s.FlagType,
               
            }).OrderByDescending(s => s.ID);
        }
        public IEnumerable<AccountingGuideVM> GetAllAccounts()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new AccountingGuideVM
            {
                ID = s.ID,
                AccountCode = s.AccountCode,
                ViewAccountCode = s.AccountCode.ToString(),
                Code = s.Code,
                Name = s.Name,
                ParentId = s.ParentId.HasValue ? s.ParentId.Value : 0,
                ParentCode = s.ParentObj != null ? s.ParentObj.Code : 0,
                FlagType = s.FlagType,
                CostCenter = s.CostCenter
            }).OrderBy(s => s.ViewAccountCode);
        }
        public AccountingGuideVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new AccountingGuideVM
            {
                ID = s.ID,
                AccountCode = s.AccountCode,
                Name = s.Name,
                ParentId = s.ParentId.HasValue ? s.ParentId.Value : 0,
                ParentCode = s.ParentObj != null ? s.ParentObj.Code : 0,
                Code=s.Code,
                FlagType = s.FlagType,
                CostCenter = s.CostCenter
            }).FirstOrDefault();
        }
        public AccountingGuide Add(AccountingGuideVM s)
        {
            var newModel = new AccountingGuide
            {
                Code = s.ParentId == null ? s.AccountCode : s.Code,
                AccountCode = s.AccountCode,
                Name = s.Name,
                ParentId = s.ParentId,
                FlagType = s.FlagType,
                CreatedDate = DateTime.Today,
                IsDeleted = false,
                CostCenter = s.CostCenter
            };
            Insert(newModel);
            return newModel;
        }
        public AccountingGuide Edit(AccountingGuideVM s)
        {
            AccountingGuide newModel = dbSet.Where(x => x.ID == s.ID && x.IsDeleted == false).FirstOrDefault();
            newModel.AccountCode = s.AccountCode;
            newModel.Name = s.Name;
            newModel.ParentId = s.ParentId;
            newModel.FlagType = s.FlagType;
            newModel.CostCenter = s.CostCenter;
            Update(newModel);
            return newModel;
        }
        public void Delete(int id)
        {
            AccountingGuide edited = dbSet.FirstOrDefault(p => p.ID == id);
            Delete(edited);
        }
        public int GenerateParentCode()
        {
            int code = dbSet.FirstOrDefault(x => x.IsDeleted == false && x.ParentId == null) != null ? dbSet.Max(c => c.Code) + 1 : 1;
            return code;
        }
        public AccountingGuideVM GenerateChildCode(int ParentId)
        {
            AccountingGuideVM Guide = new AccountingGuideVM();
            var model = dbSet.Where(x => x.IsDeleted == false && x.ParentId == ParentId).ToList();
            int ParentCode = dbSet.FirstOrDefault(x => x.IsDeleted == false && x.ID == ParentId) != null ? dbSet.FirstOrDefault(x => x.IsDeleted == false && x.ID == ParentId).AccountCode : 1;
            Guide.Code = model != null && model.Count > 0 ? model.Max(c => c.Code) + 1 : 1;
            string serial = ParentCode.ToString()+ Guide.Code.ToString();
            Guide.AccountCode = int.Parse(serial);
            return Guide;
        }
    }
}