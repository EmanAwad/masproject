﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Accounting;
using MasProject.Data.Models.FundTransaction.ClientPayments;
using MasProject.Domain.ViewModel.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasProject.Domain.Services.Accounting
{
   public class EntriesDetailsService : Repository<EntriesDetails>
    {
        DBContext _Context;
        public EntriesDetailsService(DBContext context) : base(context)
        {
            _Context = context;
        }

        //public IEnumerable<EntriesDetailsVM> GetAll()
        //{
        //    return dbSet.Where(g => g.IsDeleted == false).Select(s => new EntriesDetailsVM
        //    {
        //        ID = s.ID,
        //       AccountNo = s.AccountNo , 
        //       AccountName = s.AccountName , 
        //       Debit = s.Debit , 
        //       Credit = s.Credit , 
        //       CostCenterId = s.CostCenterId , 
        //       //BranchObj = s.BranchObj,
        //   //    Total = s.Total , 
        //   TotalDR = s.TotalDR,
        //   TotalCR = s.TotalCR,
        //       DRCR_Difference = s.DRCR_Difference
        //    }).OrderByDescending(s => s.ID);
        //}


        public List<EntriesDetailsVM> Get(int id)
        {
            return dbSet.Where(x => x.EntriesId == id && x.IsDeleted == false).Select(s => new EntriesDetailsVM
            {
                ID = s.ID,
                AccountNo = s.AccountNo,
                AccountName = s.AccountName,
                Debit = s.Debit,
                Credit = s.Credit,
                CostCenterId = s.CostCenterId,
                CostCenterName = s.Name,
                //   Total = s.Total,
                TotalDR = s.TotalDR,
                TotalCR = s.TotalCR,
                DRCR_Difference = s.DRCR_Difference,
                DetailDesc_Note = s.DetailDesc_Note
            }).ToList();
        }

        public EntriesDetails Add(EntriesDetailsVM s)
        {
            var newModel = new EntriesDetails
            {
                //ID = s.ID,
                AccountNo = (int)s.AccountNo,
                AccountName = s.AccountName,
                Debit = s.Debit,
                Credit = s.Credit,
                CostCenterId = s.CostCenterId,
                Name = s.CostCenterName,
                TotalDR = s.TotalDR,
                TotalCR = s.TotalCR,
                DRCR_Difference = (int)s.DRCR_Difference,
                EntriesId = s.EntriesId,
                //   CreatedBy = s.CreatedBy, 
                CreatedDate = DateTime.Today,
                IsDeleted = false,
                DetailDesc_Note = s.DetailDesc_Note
            };
            Insert(newModel);
            return newModel;
        }

        public EntriesDetails Edit(EntriesDetailsVM model)
        {
            EntriesDetails oldModel = dbSet.FirstOrDefault(x => x.ID == model.ID);
            oldModel.ModifiedDate = DateTime.Today;
            oldModel.AccountNo = (int)model.AccountNo;
            oldModel.AccountName = model.AccountName;
            oldModel.Debit = (int)model.Debit;
            oldModel.Credit = (int)model.Credit;
            oldModel.CostCenterId = model.CostCenterId;
            oldModel.DRCR_Difference = (int)model.DRCR_Difference;
            oldModel.TotalDR = model.TotalDR;
            oldModel.TotalCR = model.TotalCR;
            oldModel.Name = model.CostCenterName;
            oldModel.DetailDesc_Note = model.DetailDesc_Note;
            //var newModel = new EntriesDetails
            //{
            //    ID = model.ID,
            //    ModifiedDate = DateTime.Today,
            //    AccountNo = (int)model.AccountNo,
            //    AccountName = model.AccountName,
            //    Debit = (int)model.Debit,
            //    Credit = (int)model.Credit,
            //    CostCenterId = model.CostCenterId,
            //    //  ModifiedBy = model.ModifiedBy, 
            //    //     Total = (int)model.Total,
            //    DRCR_Difference = (int)model.DRCR_Difference,
            //     TotalDR = model.TotalDR,
            //    TotalCR = model.TotalCR,
            //};
            Update(oldModel);
            return oldModel;
        }

        public void Delete(int id)
        {
            List<EntriesDetails> edited = dbSet.Where(p => p.EntriesId == id).ToList();
            foreach (var item in edited)
            {
                //item.IsDeleted = true;
                Delete(item);
            }
        }
    }
}
