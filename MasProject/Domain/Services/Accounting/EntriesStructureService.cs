﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Accounting;
using MasProject.Domain.ViewModel.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasProject.Domain.Services.Accounting
{
  public  class EntriesStructureService : Repository<EntriesStructuring>
    {
        DBContext _Context;
        public EntriesStructureService(DBContext context) : base(context)
        {
            _Context = context;
        }

        public IEnumerable<EntriesStructuringVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new EntriesStructuringVM
            {
                ID = s.ID,
                EntriesStructureNo = s.EntriesStructureNo,
                EntryDate = (DateTime)s.EntryDate,
                EntryType = s.EntryType
            }).OrderByDescending(s => s.ID);
        }

        public EntriesStructuringVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new EntriesStructuringVM
            {
                ID = s.ID,
                EntriesStructureNo = s.EntriesStructureNo,
                EntryDate = s.EntryDate.Value.Date,
                EntryType = s.EntryType


            }).FirstOrDefault();
        }

        public EntriesStructuring Add(EntriesStructuringVM s)
        {
            var newModel = new EntriesStructuring
            {
                //  ID = s.ID,
                EntriesStructureNo = s.EntriesStructureNo,
                EntryDate = s.EntryDate,
                EntryType = s.EntryType,

                //   CreatedBy = s.CreatedBy, 
                CreatedDate = DateTime.Today,
                IsDeleted = false

            };
            Insert(newModel);
            return newModel;
        }

        public EntriesStructuring Edit(EntriesStructuringVM model)
        {
            EntriesStructuring olddata = dbSet.FirstOrDefault(x => x.ID == model.ID);
            olddata.EntryDate = model.EntryDate;
            olddata.ModifiedDate = DateTime.Today;
            olddata.EntryType = model.EntryType;
            olddata.EntriesStructureNo = model.EntriesStructureNo;
            //var newModel = new Entries
            //{
            //   // ID = model.ID,
            //    ModifiedDate = DateTime.Today,
            //    EntriesStructureNo = model.EntriesStructureNo,
            //    EntryDate = model.EntryDate,
            //    EntryType = model.EntryType,
            //};
            Update(olddata);
            return olddata;
        }


        public void Delete(int id)
        {
            EntriesStructuring edited = dbSet.FirstOrDefault(p => p.ID == id);
            if (edited != null)
            {
                //edited.IsDeleted = true;
                Delete(edited);
            }
        }

        public int GenerateEntryNo()
        {
            int EntryNo = dbSet.FirstOrDefault(x => x.IsDeleted == false) != null ? dbSet.Max(c => c.EntriesStructureNo) + 1 : 1;
            return EntryNo;
        }



    }
}
