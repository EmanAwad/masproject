﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Accounting;
using MasProject.Domain.ViewModel.Accounting;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;

namespace MasProject.Domain.Services.Accounting
{
   public class EntriesService : Repository<Entries>
    {
        DBContext _Context;
        public EntriesService(DBContext context) : base(context)
        {
            _Context = context;
        }
        private static string GetDisplayAttribute(object value)
        {
            Type type = value.GetType();
            var field = type.GetField(value.ToString());
            return field == null ? null : field.GetCustomAttribute<DisplayAttribute>().GetName();
        }
        public IEnumerable<EntriesVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new EntriesVM
            {
                ID = s.ID,
               EntriesNo = s.EntriesNo, 
               EntryDate = (DateTime)s.EntryDate , 
               EntryType = s.EntryType,
               EntryDesc_Note = s.EntryDesc_Note
            }).OrderByDescending(s => s.ID);
        }

        public EntriesVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new EntriesVM
            {
                ID = s.ID,
                EntriesNo = s.EntriesNo,
                EntryDate = s.EntryDate.Value.Date,
                EntryType = s.EntryType,
                EntryDesc_Note = s.EntryDesc_Note


            }).FirstOrDefault();
        }


        public Entries Add(EntriesVM s)
        {
            var newModel = new Entries
            {
              //  ID = s.ID,
                EntriesNo = s.EntriesNo,
                EntryDate = s.EntryDate.ToLocalTime(),
                EntryType = s.EntryType,
             
                //   CreatedBy = s.CreatedBy, 
                CreatedDate = DateTime.Today,
                IsDeleted = false,
                EntryDesc_Note = s.EntryDesc_Note

            };
            Insert(newModel);
            return newModel;
        }

        public Entries Edit(EntriesVM model)
        {
            Entries olddata = dbSet.FirstOrDefault(x => x.ID == model.ID);
            olddata.EntryDate = model.EntryDate.ToLocalTime();
            olddata.ModifiedDate = DateTime.Today;
            olddata.EntryType = model.EntryType;
            olddata.EntriesNo = model.EntriesNo;
            olddata.EntryDesc_Note = model.EntryDesc_Note;
            //var newModel = new Entries
            //{
            //   // ID = model.ID,
            //    ModifiedDate = DateTime.Today,
            //    EntriesNo = model.EntriesNo,
            //    EntryDate = model.EntryDate,
            //    EntryType = model.EntryType,
            //};
            Update(olddata);
            return olddata;
        }

        public void Delete(int id)
        {
            Entries edited = dbSet.FirstOrDefault(p => p.ID == id);
            if (edited != null)
            {
                //edited.IsDeleted = true;
                Delete(edited);
            }
        }

        public int GenerateEntryNo()
        {
            int EntryNo = dbSet.FirstOrDefault(x => x.IsDeleted == false) != null ? dbSet.Max(c => c.EntriesNo) + 1 : 1;
            return EntryNo;
        }
    }
}
