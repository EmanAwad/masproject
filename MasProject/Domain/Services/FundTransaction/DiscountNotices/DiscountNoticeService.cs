﻿using System;
using System.Collections.Generic;
using System.Text;
using MasProject.Domain.ViewModel.FundTransaction.DiscountNotices;
using MasProject.Data.Models.FundTransaction.DiscountNotices;
using MasProject.Data.DataAccess;
using System.Linq;

namespace MasProject.Domain.Services.FundTransaction.DiscountNotices
{
    public class DiscountNoticeService : Repository<DiscountNotice>
    {
        public DiscountNoticeService(DBContext context) : base(context)
        {
        }
        public IEnumerable<DiscountNoticeVM> GetAllBySupplier(int SuppID)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.SupplierId == SuppID).Select(s => new DiscountNoticeVM
            {
                ID = s.ID,
                Note = s.Note,
                SerialNumber = s.SerialNumber,
                Date = s.Date,
                ClientId = s.ClientId,
                ClientName = s.Client.Name,
                CurrencyId = s.CurrencyId,
                CurrencyName = s.Currency.Name,
                Amount = s.Amount,
                UserTypeFlag = s.UserTypeFlag,
                SupplierId = s.SupplierId,
                SupplierName = s.Supplier.Name
            }).OrderByDescending(s => s.ID);
        }
        public IEnumerable<DiscountNoticeVM> GetAllByClient(int ClientID)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.ClientId == ClientID).Select(s => new DiscountNoticeVM
            {
                ID = s.ID,
                Note = s.Note,
                SerialNumber = s.SerialNumber,
                Date = s.Date,
                ClientId = s.ClientId,
                ClientName = s.Client.Name,
                CurrencyId = s.CurrencyId,
                CurrencyName = s.Currency.Name,
                Amount = s.Amount,
                UserTypeFlag = s.UserTypeFlag,
                SupplierId = s.SupplierId,
                SupplierName = s.Supplier.Name
            }).OrderByDescending(s => s.ID);
        }
        public IEnumerable<DiscountNoticeVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new DiscountNoticeVM
            {
                ID = s.ID,
                Note = s.Note,
                SerialNumber = s.SerialNumber,
                Date = s.Date,
                ClientId = s.ClientId,
                ClientName = s.Client.Name,
                CurrencyId = s.CurrencyId,
                CurrencyName = s.Currency.Name,
                Amount = s.Amount,
                UserTypeFlag =  s.UserTypeFlag,
                SupplierId = s.SupplierId,
                SupplierName = s.Supplier.Name
            }).OrderByDescending(s => s.ID);
        }
        public DiscountNoticeVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new DiscountNoticeVM
            {
                ID = s.ID,
                Note = s.Note,
                SerialNumber = s.SerialNumber,
                Date = s.Date.Date,
                ClientId = s.ClientId,
                CurrencyId = s.CurrencyId,
                Amount = s.Amount,
                UserTypeFlag = s.UserTypeFlag,
                SupplierId = s.SupplierId,
                ClientName = s.Client.Name
            }).FirstOrDefault();
        }
        public DiscountNotice Add(DiscountNoticeVM s)
        {
            var newModel = new DiscountNotice
            {
                ID = s.ID,
                Note = s.Note,
                SerialNumber = s.SerialNumber,
                Date = s.Date.ToLocalTime(),
                ClientId = s.ClientId,
                CurrencyId = s.CurrencyId,
                Amount = s.Amount,
                UserTypeFlag = s.UserTypeFlag,
                SupplierId = s.SupplierId,
            };
            Insert(newModel);
            return newModel;
        }
        public DiscountNotice Edit(DiscountNoticeVM s)
        {
            DiscountNotice edited = dbSet.FirstOrDefault(p => p.ID == s.ID);
            edited.Note = s.Note;
            edited.SerialNumber = s.SerialNumber;
            edited.Date = s.Date.ToLocalTime();
            edited.ClientId = s.ClientId;
            edited.CurrencyId = s.CurrencyId;
            edited.Amount = s.Amount;
            edited.UserTypeFlag = s.UserTypeFlag;
            edited.SupplierId = s.SupplierId;
            Update(edited);
            return edited;
        }
        public void Delete(int id)
        {
            DiscountNotice edited = dbSet.FirstOrDefault(p => p.ID == id);
            edited.IsDeleted = true;
            Update(edited);
        }

        public int GenerateSerial()
        {
            int serial = dbSet.FirstOrDefault(x => x.IsDeleted == false) != null ? dbSet.Max(c => c.SerialNumber).Value + 1 : 1;
            return serial;
        }
    }
}