﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.FundTransaction.SupplierPayments;
using MasProject.Domain.ViewModel.FundTransaction.SupplierPayments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasProject.Domain.Services.FundTransaction.SupplierPayments
{
    public class SupplierPaymentService : Repository<SupplierPayment>
    {
        public SupplierPaymentService(DBContext context) : base(context)
        {
        }

        public IEnumerable<SupplierPaymentVM> GetAllBySupplier(int SuppID)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.SupplierId == SuppID).Select(s => new SupplierPaymentVM
            {
                ID = s.ID,
                Note = s.Note,
                SerialNumber = s.SerialNumber,
                Date = s.Date,
                SupplierId = s.SupplierId,
                SupplierName = s.Supplier.Name,
                TreasuryId = s.TreasuryId,
                TreasuryName = s.Treasury.Name,
                CurrencyId = s.CurrencyId,
                CurrencyName = s.Currency.Name,
                Amount = s.Amount,
                Ratio = s.Ratio,
                Total = s.Total,
                DueDate = s.IsCheck == true ? s.DueDate : null,
                IsCheck = s.IsCheck,
                IsCollected = s.IsCollected,
                CollectionDate = s.IsCollected == true ? s.CollectionDate : null,
                Bank = s.Bank
            }).OrderByDescending(s => s.ID);
        }
        public IEnumerable<SupplierPaymentVM> GetAllByTreasury(int TresID)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.TreasuryId == TresID).Select(s => new SupplierPaymentVM
            {
                ID = s.ID,
                Note = s.Note,
                SerialNumber = s.SerialNumber,
                Date = s.Date,
                SupplierId = s.SupplierId,
                SupplierName = s.Supplier.Name,
                TreasuryId = s.TreasuryId,
                TreasuryName = s.Treasury.Name,
                CurrencyId = s.CurrencyId,
                CurrencyName = s.Currency.Name,
                Amount = s.Amount,
                Ratio = s.Ratio,
                Total = s.Total,
                DueDate = s.IsCheck == true ? s.DueDate : null,
                IsCheck = s.IsCheck,
                IsCollected = s.IsCollected,
                CollectionDate = s.IsCollected == true ? s.CollectionDate : null,
                Bank = s.Bank
            }).OrderByDescending(s => s.ID);
        }
        public IEnumerable<SupplierPaymentVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new SupplierPaymentVM
            {
                ID = s.ID,
                Note = s.Note,
                SerialNumber = s.SerialNumber,
                Date = s.Date,
                SupplierId = s.SupplierId,
                SupplierName = s.Supplier.Name,
                TreasuryId = s.TreasuryId,
                TreasuryName = s.Treasury.Name,
                CurrencyId = s.CurrencyId,
                CurrencyName = s.Currency.Name,
                Amount = s.Amount,
                Ratio = s.Ratio,
                Total = s.Total,
                DueDate = s.IsCheck == true ? s.DueDate : null,
                IsCheck = s.IsCheck,
                IsCollected = s.IsCollected,
                CollectionDate = s.IsCollected == true ? s.CollectionDate : null,
                Bank = s.Bank
            }).OrderByDescending(s => s.ID);
        }


        public SupplierPaymentVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new SupplierPaymentVM
            {
                ID = s.ID,
                Note = s.Note,
                SerialNumber = s.SerialNumber,
                Date = s.Date.Date,
                SupplierId = s.SupplierId,
                TreasuryId = s.TreasuryId,
                CurrencyId = s.CurrencyId,
                Amount = s.Amount,
                Ratio = s.Ratio,
                Total = s.Total,
                IsCheck = s.IsCheck,
                DueDate = s.IsCheck == true ? s.DueDate : null,
                IsCollected = s.IsCollected,
                CollectionDate = s.IsCollected == true ? s.CollectionDate : null,
                Bank = s.Bank
            }).FirstOrDefault();
        }


        public SupplierPayment Add(SupplierPaymentVM s)
        {
            var newModel = new SupplierPayment
            {
                ID = s.ID,
                Note = s.Note,
                SerialNumber = s.SerialNumber,
                Date = s.Date.ToLocalTime(),
                SupplierId = s.SupplierId,
                TreasuryId = s.TreasuryId,
                CurrencyId = s.CurrencyId,
                Amount = s.Amount,
                Ratio = s.Ratio,
                Total = s.Total,
                IsCheck = s.IsCheck,
                DueDate = s.IsCheck == true ? s.DueDate : null,
                IsCollected = s.IsCollected,
                CollectionDate = s.IsCollected == true ? s.CollectionDate : null,
                Bank = s.Bank
            };
            Insert(newModel);
            return newModel;
        }

        public SupplierPayment Edit(SupplierPaymentVM s)
        {
            SupplierPayment edited = dbSet.FirstOrDefault(p => p.ID == s.ID);
            edited.Note = s.Note;
            edited.SerialNumber = s.SerialNumber;
            edited.Date = s.Date.ToLocalTime();
            edited.SupplierId = s.SupplierId;
            edited.TreasuryId = s.TreasuryId;
            edited.CurrencyId = s.CurrencyId;
            edited.Amount = s.Amount;
            edited.Ratio = s.Ratio;
            edited.Total = s.Total;
            edited.IsCheck = s.IsCheck;
            edited.DueDate = s.IsCheck == true ? s.DueDate : null;
            edited.IsCollected = s.IsCollected;
            edited.CollectionDate = s.IsCollected == true ? s.CollectionDate : null;
            edited.Bank = s.Bank;
            Update(edited);
            return edited;
        }


        public void Delete(int id)
        {
            SupplierPayment edited = dbSet.FirstOrDefault(p => p.ID == id);
            edited.IsDeleted = true;
            Update(edited);
        }
        public int GenerateSerial()
        {
            int serial = dbSet.FirstOrDefault(x => x.IsDeleted == false) != null ? dbSet.Max(c => c.SerialNumber).Value + 1 : 1;
            return serial;
        }
    }
}
