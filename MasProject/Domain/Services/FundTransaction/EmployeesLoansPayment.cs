﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.FundTransaction;
using MasProject.Domain.ViewModel.FundTransaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasProject.Domain.Services.FundTransaction
{
   public class EmployeesLoansPaymentService : Repository<EmployeesLoansPayment>
    {
        public EmployeesLoansPaymentService(DBContext context) : base(context)
        {
        }
        public IEnumerable<EmployeesLoansPaymentVM> GetAllByEmployee(int EmpID)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.EmployeeId == EmpID).Select(s => new EmployeesLoansPaymentVM
            {
                ID = s.ID,
                Note = s.Note,
                SerialNumber = s.SerialNumber,
                Date = s.Date,
                TreasuryId = s.TreasuryId,
                TreasuryName = s.Treasury.Name,
                EmployeeId = s.EmployeeId,
                EmployeeName = s.Employee.Name,
                DocumentNo = s.DocumentNo,
                Amount = s.Amount
            }).OrderByDescending(s => s.ID);
        }
        public IEnumerable<EmployeesLoansPaymentVM> GetAllByTreasury(int TresID)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.TreasuryId == TresID).Select(s => new EmployeesLoansPaymentVM
            {
                ID = s.ID,
                Note = s.Note,
                SerialNumber = s.SerialNumber,
                Date = s.Date,
                TreasuryId = s.TreasuryId,
                TreasuryName = s.Treasury.Name,
                EmployeeId = s.EmployeeId,
                EmployeeName = s.Employee.Name,
                DocumentNo = s.DocumentNo,
                Amount = s.Amount
            }).OrderByDescending(s => s.ID);
        }
        public IEnumerable<EmployeesLoansPaymentVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new EmployeesLoansPaymentVM
            {
                ID = s.ID,
                Note = s.Note,
                SerialNumber = s.SerialNumber,
                Date = s.Date,
                TreasuryId = s.TreasuryId,
                TreasuryName = s.Treasury.Name,
                EmployeeId = s.EmployeeId,
                EmployeeName = s.Employee.Name,
                DocumentNo = s.DocumentNo,
                Amount = s.Amount
            }).OrderByDescending(s => s.ID);
        }

        public EmployeesLoansPaymentVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new EmployeesLoansPaymentVM
            {
                ID = s.ID,
                Note = s.Note,
                SerialNumber = s.SerialNumber,
                Date = s.Date.Date,
                TreasuryId = s.TreasuryId,
                TreasuryName = s.Treasury.Name,
                EmployeeId = s.EmployeeId,
                EmployeeName = s.Employee.Name,
                DocumentNo = s.DocumentNo,
                Amount = s.Amount
            }).FirstOrDefault();
        }

        public EmployeesLoansPayment Add(EmployeesLoansPaymentVM s)
        {
            var newModel = new EmployeesLoansPayment
            {
                ID = s.ID,
                Note = s.Note,
                SerialNumber = s.SerialNumber,
                Date = s.Date.ToLocalTime(),
                TreasuryId = s.TreasuryId,
                EmployeeId = s.EmployeeId,
                DocumentNo = s.DocumentNo,
                Amount = s.Amount
            };
            Insert(newModel);
            return newModel;
        }

        public EmployeesLoansPayment Edit(EmployeesLoansPaymentVM s)
        {
            EmployeesLoansPayment edited = dbSet.FirstOrDefault(p => p.ID == s.ID);
            edited.ID = s.ID;
            edited.Note = s.Note;
            edited.SerialNumber = s.SerialNumber;
            edited.Date = s.Date.ToLocalTime();
            edited.TreasuryId = s.TreasuryId;
            edited.EmployeeId = s.EmployeeId;
            edited.DocumentNo = s.DocumentNo;
            edited.Amount = s.Amount;
            Update(edited);
            return edited;
        }

        public void Delete(int id)
        {
            EmployeesLoansPayment edited = dbSet.FirstOrDefault(p => p.ID == id);
            edited.IsDeleted = true;
            Update(edited);
        }


        public int GenerateSerial()
        {
            int serial = dbSet.FirstOrDefault(x => x.IsDeleted == false) != null ? dbSet.Max(c => c.SerialNumber).Value + 1 : 1;
            return serial;
        }

    }
}
