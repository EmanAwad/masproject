﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.FundTransaction.ClientPayments;
using MasProject.Domain.ViewModel.FundTransaction.ClientPayments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasProject.Domain.Services.FundTransaction.ClientsPayment
{
    public class ClientPaymentService : Repository<ClientPayment>
    {
        DBContext _Context;
        public ClientPaymentService(DBContext context) : base(context)
        {
            _Context = context;
        }
        public IEnumerable<ClientPaymentVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new ClientPaymentVM
            {
                ID = s.ID,
                Note = s.Note,
                SerialNumber = s.SerialNumber,
                BillSerialNumber = s.BillSerialNumber,
                Date = s.Date.Date,
                ClientId = s.ClientId,
                ClientName = s.Client.Name,
                TreasuryId = s.TreasuryId,
                TreasuryName = s.Treasury.Name,
                CurrencyId = s.CurrencyId,
                CurrencyName = s.Currency.Name,
                Amount = s.Amount,
                Ratio = s.Ratio,
                Total = s.Total,
                DueDate = s.IsCheck == true ? s.DueDate : null,
                IsCheck = s.IsCheck,
                IsCollected = s.IsCollected,
                CollectionDate = s.IsCollected == true ? s.CollectionDate : null,
                Bank = s.Bank
            }).OrderByDescending(s => s.ID);
        }
        public IEnumerable<ClientPaymentVM> GetAllForEdit()
        {
            return dbSet.Where(g => g.IsDeleted == false && g.Name == "old").Select(s => new ClientPaymentVM
            {
                ID = s.ID,
                Date = s.Date,
                CreatedDate = s.CreatedDate,
                ClientId = s.ClientId,
            }).OrderBy(s => s.Date).ThenBy(s => s.ID);//
        }
        public ClientPaymentVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new ClientPaymentVM
            {
                ID = s.ID,
                Note = s.Note,
                SerialNumber = s.SerialNumber,
                BillSerialNumber = s.BillSerialNumber,
                Date = s.Date.Date,
                ClientId = s.ClientId,
                TreasuryId = s.TreasuryId,
                CurrencyId = s.CurrencyId,
                Amount = s.Amount,
                Ratio = s.Ratio,
                Total = s.Total,
                IsCheck = s.IsCheck,
                DueDate = s.IsCheck == true ? s.DueDate : null,
                IsCollected = s.IsCollected,
                CollectionDate = s.IsCollected == true ? s.CollectionDate : null,
                Bank = s.Bank,
                ClientName = s.Client.Name
            }).FirstOrDefault();
        }
        public IEnumerable<ClientPaymentVM> GetAllByClient(int ClientId)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.ClientId == ClientId).Select(s => new ClientPaymentVM
            {
                ID = s.ID,
                Note = s.Note,
                SerialNumber = s.SerialNumber,
                BillSerialNumber = s.BillSerialNumber,
                Date = s.Date.Date,
                ClientId = s.ClientId,
                ClientName = s.Client.Name,
                TreasuryId = s.TreasuryId,
                TreasuryName = s.Treasury.Name,
                CurrencyId = s.CurrencyId,
                CurrencyName = s.Currency.Name,
                Amount = s.Amount,
                Ratio = s.Ratio,
                Total = s.Total,
                DueDate = s.IsCheck == true ? s.DueDate : null,
                IsCheck = s.IsCheck,
                IsCollected = s.IsCollected,
                CollectionDate = s.IsCollected == true ? s.CollectionDate : null,
                Bank = s.Bank
            }).OrderByDescending(s => s.ID);
        }
        public IEnumerable<ClientPaymentVM> GetAllByTreasury(int TreasuryId)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.TreasuryId == TreasuryId).Select(s => new ClientPaymentVM
            {
                ID = s.ID,
                Note = s.Note,
                SerialNumber = s.SerialNumber,
                BillSerialNumber = s.BillSerialNumber,
                Date = s.Date.Date,
                ClientId = s.ClientId,
                ClientName = s.Client.Name,
                TreasuryId = s.TreasuryId,
                TreasuryName = s.Treasury.Name,
                CurrencyId = s.CurrencyId,
                CurrencyName = s.Currency.Name,
                Amount = s.Amount,
                Ratio = s.Ratio,
                Total = s.Total,
                DueDate = s.IsCheck == true ? s.DueDate : null,
                IsCheck = s.IsCheck,
                IsCollected = s.IsCollected,
                CollectionDate = s.IsCollected == true ? s.CollectionDate : null,
                Bank = s.Bank
            }).OrderByDescending(s => s.ID);
        }

        public IEnumerable<ClientPaymentVM> GetAllByDate(DateTime date)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.Date == date).Select(s => new ClientPaymentVM
            {


                ID = s.ID,
                Note = s.Note,
                SerialNumber = s.SerialNumber,
                BillSerialNumber = s.BillSerialNumber,
                Date = s.Date.Date,
                ClientId = s.ClientId,
                ClientName = s.Client.Name,
                TreasuryId = s.TreasuryId,
                TreasuryName = s.Treasury.Name,
                CurrencyId = s.CurrencyId,
                CurrencyName = s.Currency.Name,
                Amount = s.Amount,
                Ratio = s.Ratio,
                Total = s.Total,
                DueDate = s.IsCheck == true ? s.DueDate : null,
                IsCheck = s.IsCheck,
                IsCollected = s.IsCollected,
                CollectionDate = s.IsCollected == true ? s.CollectionDate : null,
                Bank = s.Bank
            }).OrderByDescending(s => s.ID);
        }
        public ClientPayment Add(ClientPaymentVM s)
        {
            var newModel = new ClientPayment
            {
                ID = s.ID,
                Note = s.Note,
                SerialNumber = s.SerialNumber,
                BillSerialNumber = s.BillSerialNumber,
                Date = s.Date.ToLocalTime(),
                ClientId = s.ClientId,
                TreasuryId = s.TreasuryId,
                CurrencyId = s.CurrencyId,
                Amount = s.Amount,
                Ratio = s.Ratio,
                Total = s.Total,
                IsCheck = s.IsCheck,
                DueDate = s.IsCheck == true ? s.DueDate : null,
                IsCollected = s.IsCollected,
                CollectionDate = s.IsCollected == true ? s.CollectionDate : null,
                Bank = s.Bank
            };
            Insert(newModel);
            return newModel;
        }
        public ClientPayment Edit(ClientPaymentVM s)
        {
            ClientPayment edited = dbSet.FirstOrDefault(p => p.ID == s.ID);
            edited.Note = s.Note;
            edited.SerialNumber = s.SerialNumber;
            edited.BillSerialNumber = s.BillSerialNumber;
            edited.Date = s.Date.ToLocalTime();
            edited.ClientId = s.ClientId;
            edited.TreasuryId = s.TreasuryId;
            edited.CurrencyId = s.CurrencyId;
            edited.Amount = s.Amount;
            edited.Ratio = s.Ratio;
            edited.Total = s.Total;
            edited.IsCheck = s.IsCheck;
            edited.DueDate = s.IsCheck == true ? s.DueDate : null;
            edited.IsCollected = s.IsCollected;
            edited.CollectionDate = s.IsCollected == true ? s.CollectionDate : null;
            edited.Bank = s.Bank;
            edited.Name = "";
            Update(edited);
            return edited;
        }
        public void Delete(int id)
        {
            ClientPayment edited = dbSet.FirstOrDefault(p => p.ID == id);
            Delete(edited);
        }
        //public int GenerateSerial()
        //{
        //    int serial = dbSet.FirstOrDefault(x => x.IsDeleted == false) != null ? dbSet.Max(c => c.SerialNumber).Value + 1 : 1;
        //    return serial;
        //}
        public int GenerateSerial(int? TreasuryId)
        {
            int? BranchId = null;
            int TreasuryArrang = 0;
            if (TreasuryId != 0 && TreasuryId != null)
            {
                var tempObj = _Context.Treasuries.FirstOrDefault(x => x.IsDeleted == false & x.ID == TreasuryId);
                BranchId = tempObj.BranchId;
                TreasuryArrang = (int)tempObj.arrangement;
            }
            string Serial = "";
            if (BranchId != null && BranchId > 0)
                Serial = _Context.Branch.FirstOrDefault(x => x.IsDeleted == false && x.ID == BranchId).arrangement.ToString();
            else
                Serial = _Context.Branch.FirstOrDefault(x => x.IsDeleted == false && x.ID == 3).arrangement.ToString();
            Serial += DateTime.Now.ToString("yy");
            Serial += TreasuryArrang;
            var model = dbSet.Where(x => x.TreasuryId == TreasuryId && x.IsDeleted == false && x.Name == "").OrderByDescending(x => x.SerialNumber).FirstOrDefault();
            int SerialNumber = model != null ? (model.SerialNumber.Value + 1) : 0;
            Serial += SerialNumber > 0 ? SerialNumber.ToString().Substring(6, SerialNumber.ToString().Length - 6) : "001";
            return int.Parse(Serial);
        }
    }
}