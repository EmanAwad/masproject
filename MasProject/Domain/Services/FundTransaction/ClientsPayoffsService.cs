﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.FundTransaction;
using MasProject.Domain.ViewModel.FundTransaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasProject.Domain.Services.FundTransaction
{
   public class ClientsPayoffsService : Repository<ClientsPayoffs>
    {
        public ClientsPayoffsService(DBContext context) : base(context)
        {
        }

        public IEnumerable<ClientsPayoffsVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new ClientsPayoffsVM
            {
                ID = s.ID,
                Note = s.Note,
                SerialNumber = s.SerialNumber,
                BillSerialNumber = s.BillSerialNumber,
                Date = s.Date,
                ClientId = s.ClientId,
                ClientName = s.Client.Name,
                TreasuryId = s.TreasuryId,
                TreasuryName = s.Treasury.Name,
                CurrencyId = s.CurrencyId,
                CurrencyName = s.Currency.Name,
                Amount = s.Amount,
                Ratio = s.Ratio,
                Total = s.Total,
                DueDate = s.IsCheck == true ? s.DueDate : null,
                IsCheck = s.IsCheck,
                IsCollected = s.IsCollected,
                CollectionDate = s.IsCollected == true ? s.CollectionDate : null, 
                Bank = s.Bank
            }).OrderByDescending(s => s.ID);
        }
        public IEnumerable<ClientsPayoffsVM> GetAllByClient(int ClientId)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.ClientId == ClientId).Select(s => new ClientsPayoffsVM
            {
                ID = s.ID,
                Note = s.Note,
                SerialNumber = s.SerialNumber,
                BillSerialNumber = s.BillSerialNumber,
                Date = s.Date,
                ClientId = s.ClientId,
                ClientName = s.Client.Name,
                TreasuryId = s.TreasuryId,
                TreasuryName = s.Treasury.Name,
                CurrencyId = s.CurrencyId,
                CurrencyName = s.Currency.Name,
                Amount = s.Amount,
                Ratio = s.Ratio,
                Total = s.Total,
                DueDate = s.IsCheck == true ? s.DueDate : null,
                IsCheck = s.IsCheck,
                IsCollected = s.IsCollected,
                CollectionDate = s.IsCollected == true ? s.CollectionDate : null,
                Bank = s.Bank
            }).OrderByDescending(s => s.ID);
        }
        public IEnumerable<ClientsPayoffsVM> GetAllByTreasury(int TreasuryId)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.TreasuryId == TreasuryId).Select(s => new ClientsPayoffsVM
            {
                ID = s.ID,
                Note = s.Note,
                SerialNumber = s.SerialNumber,
                BillSerialNumber = s.BillSerialNumber,
                Date = s.Date,
                ClientId = s.ClientId,
                ClientName = s.Client.Name,
                TreasuryId = s.TreasuryId,
                TreasuryName = s.Treasury.Name,
                CurrencyId = s.CurrencyId,
                CurrencyName = s.Currency.Name,
                Amount = s.Amount,
                Ratio = s.Ratio,
                Total = s.Total,
                DueDate = s.IsCheck == true ? s.DueDate : null,
                IsCheck = s.IsCheck,
                IsCollected = s.IsCollected,
                CollectionDate = s.IsCollected == true ? s.CollectionDate : null,
                Bank = s.Bank
            }).OrderByDescending(s => s.ID);
        }

        public IEnumerable<ClientsPayoffsVM> GetAllByDate(DateTime date)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.Date == date).Select(s => new ClientsPayoffsVM
            {


                ID = s.ID,
                Note = s.Note,
                SerialNumber = s.SerialNumber,
                BillSerialNumber = s.BillSerialNumber,
                Date = s.Date,
                ClientId = s.ClientId,
                ClientName = s.Client.Name,
                TreasuryId = s.TreasuryId,
                TreasuryName = s.Treasury.Name,
                CurrencyId = s.CurrencyId,
                CurrencyName = s.Currency.Name,
                Amount = s.Amount,
                Ratio = s.Ratio,
                Total = s.Total,
                DueDate = s.IsCheck == true ? s.DueDate : null,
                IsCheck = s.IsCheck,
                IsCollected = s.IsCollected,
                CollectionDate = s.IsCollected == true ? s.CollectionDate : null,
                Bank = s.Bank
            }).OrderByDescending(s => s.ID);
        }

        public ClientsPayoffsVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new ClientsPayoffsVM
            {
                ID = s.ID,
                Note = s.Note,
                SerialNumber = s.SerialNumber,
                BillSerialNumber = s.BillSerialNumber,
                Date = s.Date.Date,
                ClientId = s.ClientId,
                TreasuryId = s.TreasuryId,
                CurrencyId = s.CurrencyId,
                Amount = s.Amount,
                Ratio = s.Ratio,
                Total = s.Total,
                IsCheck = s.IsCheck,
                DueDate = s.IsCheck == true ? s.DueDate : null,
                IsCollected = s.IsCollected,
                CollectionDate = s.IsCollected == true ? s.CollectionDate : null,
                Bank = s.Bank,
                ClientName = s.Client.Name
            }).FirstOrDefault();
        }



        public ClientsPayoffs Add(ClientsPayoffsVM model)
        {
            var newModel = new ClientsPayoffs
            {
                ID = model.ID,
                Note = model.Note,
                SerialNumber = model.SerialNumber,
                BillSerialNumber = model.BillSerialNumber,
                Date = model.Date.ToLocalTime(),
                ClientId = model.ClientId,
                TreasuryId = model.TreasuryId,
                CurrencyId = model.CurrencyId,
                Amount = model.Amount,
                Ratio = model.Ratio,
                Total = model.Total,
                IsCheck = model.IsCheck,
                DueDate = model.IsCheck == true ? model.DueDate : null,
                IsCollected = model.IsCollected,
                CollectionDate = model.IsCollected == true ? model.CollectionDate : null,
                Bank = model.Bank
            };
            Insert(newModel);
            return newModel;
        }


        public ClientsPayoffs Edit(ClientsPayoffsVM model)
        {
            ClientsPayoffs edited = dbSet.FirstOrDefault(p => p.ID == model.ID);
            edited.Note = model.Note;
            edited.SerialNumber = model.SerialNumber;
            edited.BillSerialNumber = model.BillSerialNumber;
            edited.Date = model.Date.ToLocalTime();
            edited.ClientId = model.ClientId;
            edited.TreasuryId = model.TreasuryId;
            edited.CurrencyId = model.CurrencyId;
            edited.Amount = model.Amount;
            edited.Ratio = model.Ratio;
            edited.Total = model.Total;
            edited.IsCheck = model.IsCheck;
            edited.DueDate = model.IsCheck == true ? model.DueDate : null;
            edited.IsCollected = model.IsCollected;
            edited.CollectionDate = model.IsCollected == true ? model.CollectionDate : null;
            edited.Bank = model.Bank;
            Update(edited);
            return edited;
        }



        public void Delete(int id)
        {
            ClientsPayoffs edited = dbSet.FirstOrDefault(p => p.ID == id);
            Delete(edited);
        }

        public int GenerateSerial()
        {
            int serial = dbSet.FirstOrDefault(x => x.IsDeleted == false) != null ? dbSet.Max(c => c.SerialNumber).Value + 1 : 1;
            return serial;
        }
    }
}
