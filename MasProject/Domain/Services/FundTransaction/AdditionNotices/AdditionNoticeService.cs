﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.FundTransaction.AdditionNotice;
using MasProject.Domain.ViewModel.FundTransaction.AdditionNotice;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasProject.Domain.Services.FundTransaction.AdditionNotices
{
   public class AdditionNoticeService : Repository<AdditionNotice>
    {
        public AdditionNoticeService(DBContext context) : base(context)
        {
        }
        public IEnumerable<AdditionNoticeVM> GetAllByClient(int ClientId)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.ClientId == ClientId).Select(s => new AdditionNoticeVM
            {
                ID = s.ID,
                Note = s.Note,
                SerialNumber = s.SerialNumber,
                Date = s.Date,
                SupplierId = s.SupplierId,
                SupplierName = s.Supplier.Name,
                CurrencyId = s.CurrencyId,
                CurrencyName = s.Currency.Name,
                Amount = s.Amount,
                UserTypeFlag = s.UserTypeFlag,
                ClientId = s.ClientId,
                ClientName = s.Client.Name,
            }).OrderByDescending(s => s.ID);
        }
        public IEnumerable<AdditionNoticeVM> GetAllBySupplier(int SuppID)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.SupplierId == SuppID).Select(s => new AdditionNoticeVM
            {
                ID = s.ID,
                Note = s.Note,
                SerialNumber = s.SerialNumber,
                Date = s.Date,
                SupplierId = s.SupplierId,
                SupplierName = s.Supplier.Name,
                CurrencyId = s.CurrencyId,
                CurrencyName = s.Currency.Name,
                Amount = s.Amount,
                UserTypeFlag = s.UserTypeFlag,
                ClientId = s.ClientId,
                ClientName = s.Client.Name,
            }).OrderByDescending(s => s.ID);
        }
        public IEnumerable<AdditionNoticeVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new AdditionNoticeVM
            {
                ID = s.ID,
                Note = s.Note,
                SerialNumber = s.SerialNumber,
                Date = s.Date,
                SupplierId = s.SupplierId,
                SupplierName = s.Supplier.Name,
                CurrencyId = s.CurrencyId,
                CurrencyName = s.Currency.Name,
                Amount = s.Amount,
                UserTypeFlag = s.UserTypeFlag,
                ClientId = s.ClientId,
                ClientName = s.Client.Name,
            }).OrderByDescending(s => s.ID);
        }

        public AdditionNoticeVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new AdditionNoticeVM
            {
                ID = s.ID,
                Note = s.Note,
                SerialNumber = s.SerialNumber,
                Date = s.Date.Date,
                SupplierId = s.SupplierId,
                CurrencyId = s.CurrencyId,
                Amount = s.Amount,
                UserTypeFlag = s.UserTypeFlag,
                ClientId = s.ClientId,
                ClientName = s.Client.Name
            }).FirstOrDefault();
        }

        public AdditionNotice Add(AdditionNoticeVM s)
        {
            var newModel = new AdditionNotice
            {
                ID = s.ID,
                Note = s.Note,
                SerialNumber = s.SerialNumber,
                Date = s.Date.ToLocalTime(),
                SupplierId = s.SupplierId,
                CurrencyId = s.CurrencyId,
                Amount = s.Amount,
                UserTypeFlag = s.UserTypeFlag,
                ClientId = s.ClientId,
            };
            Insert(newModel);
            return newModel;
        }

        public AdditionNotice Edit(AdditionNoticeVM s)
        {
            AdditionNotice edited = dbSet.FirstOrDefault(p => p.ID == s.ID);
            edited.Note = s.Note;
            edited.SerialNumber = s.SerialNumber;
            edited.Date = s.Date.ToLocalTime();
            edited.SupplierId = s.SupplierId;
            edited.CurrencyId = s.CurrencyId;
            edited.Amount = s.Amount;
            edited.UserTypeFlag = s.UserTypeFlag;
            edited.ClientId = s.ClientId;
            Update(edited);
            return edited;
        }

        public void Delete(int id)
        {
            AdditionNotice edited = dbSet.FirstOrDefault(p => p.ID == id);
            edited.IsDeleted = true;
            Update(edited);
        }

        public int GenerateSerial()
        {
            int serial = dbSet.FirstOrDefault(x => x.IsDeleted == false) != null ? dbSet.Max(c => c.SerialNumber).Value + 1 : 1;
            return serial;
        }

    }
}
