﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.FundTransaction;
using MasProject.Domain.ViewModel.FundTransaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasProject.Domain.Services.FundTransaction
{
   public class EmployeesLoansEntryService : Repository<EmployeesLoansEntry>
    {
        public EmployeesLoansEntryService(DBContext context) : base(context)
        {
        }
        public IEnumerable<EmployeesLoansEntryVM> GetAllByEmployee(int EmpID)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.EmployeeId == EmpID).Select(s => new EmployeesLoansEntryVM
            {
                ID = s.ID,
                Note = s.Note,
                SerialNumber = s.SerialNumber,
                Date = s.Date,
                TreasuryId = s.TreasuryId,
                TreasuryName = s.Treasury.Name,
                EmployeeId = s.EmployeeId,
                EmployeeName = s.Employee.Name,
                DocumentNo = s.DocumentNo,
                Amount = s.Amount
            }).OrderByDescending(s => s.ID);
        }
        public IEnumerable<EmployeesLoansEntryVM> GetAllByTreasury(int TresID)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.TreasuryId == TresID).Select(s => new EmployeesLoansEntryVM
            {
                ID = s.ID,
                Note = s.Note,
                SerialNumber = s.SerialNumber,
                Date = s.Date,
                TreasuryId = s.TreasuryId,
                TreasuryName = s.Treasury.Name,
                EmployeeId = s.EmployeeId,
                EmployeeName = s.Employee.Name,
                DocumentNo = s.DocumentNo,
                Amount = s.Amount
            }).OrderByDescending(s => s.ID);
        }
        public IEnumerable<EmployeesLoansEntryVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new EmployeesLoansEntryVM
            {
                ID = s.ID,
                Note = s.Note,
                SerialNumber = s.SerialNumber,
                Date = s.Date,
                TreasuryId = s.TreasuryId,
                TreasuryName = s.Treasury.Name,
                EmployeeId = s.EmployeeId,
                EmployeeName = s.Employee.Name,
                DocumentNo = s.DocumentNo,
                Amount = s.Amount
            }).OrderByDescending(s => s.ID);
        }

        public EmployeesLoansEntryVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new EmployeesLoansEntryVM
            {
                ID = s.ID,
                Note = s.Note,
                SerialNumber = s.SerialNumber,
                Date = s.Date.Date,
                TreasuryId = s.TreasuryId,
                TreasuryName = s.Treasury.Name,
                EmployeeId = s.EmployeeId,
                EmployeeName = s.Employee.Name,
                DocumentNo = s.DocumentNo,
                Amount = s.Amount
            }).FirstOrDefault();
        }

        public EmployeesLoansEntry Add(EmployeesLoansEntryVM s)
        {
            var newModel = new EmployeesLoansEntry
            {
                ID = s.ID,
                Note = s.Note,
                SerialNumber = s.SerialNumber,
                Date = s.Date.ToLocalTime(),
                TreasuryId = s.TreasuryId,
                EmployeeId = s.EmployeeId,
                DocumentNo = s.DocumentNo,
                Amount = s.Amount
            };
            Insert(newModel);
            return newModel;
        }

        public EmployeesLoansEntry Edit(EmployeesLoansEntryVM s)
        {
            EmployeesLoansEntry edited = dbSet.FirstOrDefault(p => p.ID == s.ID);
            edited.ID = s.ID;
            edited.Note = s.Note;
            edited.SerialNumber = s.SerialNumber;
            edited.Date = s.Date.ToLocalTime();
            edited.TreasuryId = s.TreasuryId;
            edited.EmployeeId = s.EmployeeId;
            edited.DocumentNo = s.DocumentNo;
            edited.Amount = s.Amount;
            Update(edited);
            return edited;
        }

        public void Delete(int id)
        {
            EmployeesLoansEntry edited = dbSet.FirstOrDefault(p => p.ID == id);
            edited.IsDeleted = true;
            Update(edited);
        }

        public int GenerateSerial()
        {
            int serial = dbSet.FirstOrDefault(x => x.IsDeleted == false) != null ? dbSet.Max(c => c.SerialNumber).Value + 1 : 1;
            return serial;
        }

    }
}
