﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Address;
using MasProject.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasProject.Domain.Services
{
    public class CityService : Repository<Lookup_City>
    {
        public CityService(DBContext context) : base(context)
        {
        }
        public IEnumerable<LookupKeyValueVM> GetAll()
        {
            return dbSet.Where(x => x.IsDeleted == false).Select(s => new LookupKeyValueVM
            {
                ID = s.ID,
                Name = s.Name
            }).OrderBy(s => s.ID);
        }
    }
}