﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.StoreTransaction.Sales;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using MasProject.Domain.ViewModel.TransVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasProject.Domain.Services.StoreTransaction.Sales
{
    public class SalesItemsService : Repository<SalesItems>
    {
        public SalesItemsService(DBContext context) : base(context)
        {
        }
        //public IEnumerable<SalesItemsVM> GetAll()
        //{//.Where(g => g.IsDeleted == false)
        //    return dbSet.Select(s => new SalesItemsVM
        //    {
        //        ID = s.ID,
        //        IsDeleted = s.IsDeleted,
        //     SerialNumber=(int)s.SerialNumber,
        //     ItemId=(int)s.ItemId,
        //     Price=s.Price,
        //     Quantity=s.Quantity,
        //     SavedPrice=s.SavedPrice,
        //     Total=s.Total,
        //    }).OrderBy(s => s.ID);
        //}
        public List<StoreReportVM> GetByItemId(int ItemId)
        { 
            return dbSet.Where(x => x.ItemId==ItemId && x.IsDeleted == false).Select(s => new StoreReportVM
            {
                SerialNumber = (int)s.SerialNumber,
                ItemId = (int)s.ItemId,
                Price = (decimal)s.Price,
                Quantity = (int)s.Quantity,
                Total = (decimal)s.Total,
                ItemName = s.Item.Name,
                StoreIdentifier=s.Name,
                StoreId= (int)s.StoreId,
            }).ToList();
        }
        public List<SalesItemsVM> GetBySerial(int Serial,int StoreID,int? StoreIdentifier)
        {//&& x.IsDeleted == false
            string StoreIdentifierStr = "";
            if (StoreIdentifier != 0)
            {
                StoreIdentifierStr = StoreIdentifier.ToString();
            }
            return dbSet.Where(x => x.SerialNumber == Serial && x.StoreId==StoreID && x.Name== StoreIdentifierStr && x.IsDeleted == false).Select(s => new SalesItemsVM
            {
                //Identifer = index + 1,
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                ItemId = (int)s.ItemId,
                Box_ParCode = s.Item.National_ParCode,
                Price = s.Price,
                Quantity = s.Quantity,
                SavedPrice = s.SavedPrice,
                Total = s.Total,
                ItemName = s.Item.Name,
                // }).OrderBy(s => s.Identifer).ToList();
            }).ToList();
        }
        public decimal GetLatestPrice(int ItemId)
        {
            var model = dbSet.Where(x => x.ItemId == ItemId && x.IsDeleted == false).OrderByDescending(x => x.ID).ToList() ?? new List<SalesItems>();
            return model.Count > 0 ? model.FirstOrDefault().Price.Value : 0;
        }
        public SalesItems Add(SalesItemsVM s)
        {
            var newModel = new SalesItems
            {
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                ItemId = (int)s.ItemId,
                Price = s.Price,
                Quantity = s.Quantity,
                SavedPrice = s.SavedPrice,
                Total = s.Total,
                StoreId = s.StoreId,
                Name = s.Identifer.ToString(),
            };
            Insert(newModel);
            return newModel;
        }
        public SalesItems Edit(SalesItemsVM s)
        {
            SalesItems Edited = dbSet.FirstOrDefault(x => x.ID == s.ID);
            Edited.SerialNumber = (int)s.SerialNumber;
            Edited.ItemId = (int)s.ItemId;
            Edited.Price = s.Price;
            Edited.Quantity = s.Quantity;
            Edited.SavedPrice = s.SavedPrice;
            Edited.Total = s.Total;
            Edited.StoreId = s.StoreId;
            Edited.Name = s.Identifer.ToString();
            Update(Edited);
            return Edited;
        }
        public void Delete(int id)
        {
          List<  SalesItems > DeleteList = dbSet.Where(p => p.SerialNumber == id).ToList();
            foreach (var item in DeleteList)
            {
                Delete(item);
            }
        }
        public void DeletePhysical(int id)
        {
            List<SalesItems> DeleteList = dbSet.Where(p => p.SerialNumber == id).ToList();
            if (DeleteList != null)
            {
                foreach (var item in DeleteList)
                {
                    item.IsDeleted = true;
                    Remove(item);
                }
            }
           
        }
    }
}
