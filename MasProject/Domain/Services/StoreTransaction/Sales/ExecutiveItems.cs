﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.StoreTransaction.Sales;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasProject.Domain.Services.StoreTransaction.Sales
{
    public class ExecutiveItemsService : Repository<ExecutiveItems>
    {
        public ExecutiveItemsService(DBContext context) : base(context)
        {
        }
        public List<ExecutiveItemsVM> GetBySerial(int Serial, int StoreID, int? StoreIdentifier)
        {
            string StoreIdentifierStr = "";
            if (StoreIdentifier != 0)
            {
                StoreIdentifierStr = StoreIdentifier.ToString();
            }
            return dbSet.Where(x => x.SerialNumber == Serial && x.StoreId == StoreID && x.Name == StoreIdentifierStr && x.IsDeleted == false).Select(s => new ExecutiveItemsVM
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                ItemId = (int)s.ItemId,
                Box_ParCode=s.Item.National_ParCode,
                Price = s.Price,
                Quantity = s.Quantity,
                SavedPrice = s.SavedPrice,
                Total = s.Total,
                ItemName = s.Item.Name
            }).ToList();
        }
        public ExecutiveItems Add(ExecutiveItemsVM s)
        {
            var newModel = new ExecutiveItems
            {
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                ItemId = (int)s.ItemId,
                Price = s.Price,
                Quantity = s.Quantity,
                SavedPrice = s.SavedPrice,
                Total = s.Total,
                StoreId = s.StoreId,
                Name = s.Identifer.ToString(),
            };
            Insert(newModel);
            return newModel;
        }
        public ExecutiveItems Edit(ExecutiveItemsVM s)
        {
            var newModel = new ExecutiveItems
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                ItemId = (int)s.ItemId,
                Price = s.Price,
                Quantity = s.Quantity,
                SavedPrice = s.SavedPrice,
                Total = s.Total,
                StoreId = s.StoreId,
            };
            Update(newModel);
            return newModel;
        }
        public void Delete(int id)
        {
            List<ExecutiveItems> DeleteList = dbSet.Where(p => p.SerialNumber == id).ToList();
            foreach (var item in DeleteList)
            {
                Delete(item);
            }
        }
        public void DeletePhysical(int id)
        {
            List<ExecutiveItems> DeleteList = dbSet.Where(p => p.SerialNumber == id).ToList();
            foreach (var item in DeleteList)
            {
                item.IsDeleted = true;
                Remove(item);
            }
        }
    }
}