﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.StoreTransaction.Sales;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasProject.Domain.Services.StoreTransaction.Sales
{
    public class SalesSellExpensesService : Repository<SalesSellExpenses>
    {
        public SalesSellExpensesService(DBContext context) : base(context)
        {
        }

        public List<SalesSellExpensesVM> GetBySerial(int Serial)
        {//&& x.IsDeleted == false
            return dbSet.Where(x => x.SerialNumber == Serial && x.IsDeleted == false).Select(s => new SalesSellExpensesVM
            {
                //Identifer = index + 1,
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                Amount = s.Amount,
                CreatedBy = s.CreatedBy,
                CreatedDate = s.CreatedDate,
                ModifiedBy = s.ModifiedBy,
                ModifiedDate = s.ModifiedDate,
                Name = s.Name,
                ExpensesId = s.ExpensesId,

            }).ToList();
        }
        public SalesSellExpenses Add(SalesSellExpensesVM s)
        {
            var newModel = new SalesSellExpenses
            {
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                Amount = s.Amount,
                CreatedBy = s.CreatedBy,
                CreatedDate = s.CreatedDate,
                ModifiedBy = s.ModifiedBy,
                ModifiedDate = s.ModifiedDate,
                Name = s.Name,
                ExpensesId=s.ExpensesId,
            };
            Insert(newModel);
            return newModel;
        }
        public SalesSellExpenses Edit(SalesSellExpensesVM s)
        {
            SalesSellExpenses edited = dbSet.FirstOrDefault(x => x.ID == s.ID);
            edited.SerialNumber = (int)s.SerialNumber;
            edited.Amount = s.Amount;
            edited.Name = s.Name;
            edited.ExpensesId = s.ExpensesId;
            Update(edited);
            return edited;
        }
        public void Delete(int id)
        {
            List<SalesSellExpenses> DeleteList = dbSet.Where(p => p.SerialNumber == id).ToList();
            foreach (var item in DeleteList)
            {
                Delete(item);
            }
        }
        public void DeletePhysical(int id)
        {
            List<SalesSellExpenses> DeleteList = dbSet.Where(p => p.SerialNumber == id).ToList();
            foreach (var item in DeleteList)
            {
                item.IsDeleted = true;
                Remove(item);
            }
        }
    }
}
