﻿using System;
using System.Collections.Generic;
using MasProject.Data.DataAccess;
using MasProject.Data.Models.StoreTransaction.Sales;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using System.Linq;
namespace MasProject.Domain.Services.StoreTransaction.Sales
{
    public class ExecutiveOtherIncomeService : Repository<ExecutiveOtherIncome>
    {
        public ExecutiveOtherIncomeService(DBContext context) : base(context)
        {
        }

        public List<ExecutiveOtherIncomeVM> GetBySerial(int Serial)
        {//&& x.IsDeleted == false
            return dbSet.Where(x => x.SerialNumber == Serial && x.IsDeleted == false).Select(s => new ExecutiveOtherIncomeVM
            {
                //Identifer = index + 1,
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                Amount = s.Amount,
                CreatedBy = s.CreatedBy,
                CreatedDate = s.CreatedDate,
                ModifiedBy = s.ModifiedBy,
                ModifiedDate = s.ModifiedDate,
                Name = s.Name,
                IncomeId = s.IncomeId,

            }).ToList();
        }
        public ExecutiveOtherIncome Add(ExecutiveOtherIncomeVM s)
        {
            var newModel = new ExecutiveOtherIncome
            {
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                Amount = s.Amount,
                CreatedBy = s.CreatedBy,
                CreatedDate = s.CreatedDate,
                ModifiedBy = s.ModifiedBy,
                ModifiedDate = s.ModifiedDate,
                Name = s.Name,
                IncomeId = s.IncomeId,

            };
            Insert(newModel);
            return newModel;
        }
        public ExecutiveOtherIncome Edit(ExecutiveOtherIncomeVM s)
        {
            var newModel = new ExecutiveOtherIncome
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                Amount = s.Amount,
                CreatedBy = s.CreatedBy,
                CreatedDate = s.CreatedDate,
                ModifiedBy = s.ModifiedBy,
                ModifiedDate = s.ModifiedDate,
                Name = s.Name,
                IncomeId = s.IncomeId,

            };
            Update(newModel);
            return newModel;
        }
        public void DeletePhysical(int id)
        {
            List<ExecutiveOtherIncome> DeleteList = dbSet.Where(p => p.SerialNumber == id).ToList();
            foreach (var item in DeleteList)
            {
                item.IsDeleted = true;
                Remove(item);
            }
        }
    }
}
