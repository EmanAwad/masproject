﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.StoreTransaction.Sales;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasProject.Domain.Services.StoreTransaction.Sales
{
  public  class MaintanceBillStoresService : Repository<MaintanceBillStores>
    {
        public MaintanceBillStoresService(DBContext context) : base(context)
        {
        }
        public List<MaintanceBillStoresVM> GetBySerial(int Serial)
        {//&& x.IsDeleted == false
            return dbSet.Where(x => x.SerialNumber == Serial && x.IsDeleted == false && x.StoreId != null).Select(s => new MaintanceBillStoresVM
            {
                //Identifer = index + 1,
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                StoreId = s.StoreId,
                Identifer = s.Name == null ? 0 : int.Parse(s.Name),
            }).ToList();
        }

        public MaintanceBillStores Add(MaintanceBillStoresVM s)
        {
            var newModel = new MaintanceBillStores
            {
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                StoreId = s.StoreId,
                CreatedDate = DateTime.Now,
                Name = s.Identifer.ToString(),
            };
            Insert(newModel);
            return newModel;
        }
        public MaintanceBillStores Edit(MaintanceBillStoresVM s)
        {
            var newModel = new MaintanceBillStores
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                StoreId = s.StoreId,
                ModifiedDate = DateTime.Now,
                Name = s.Identifer.ToString(),
            };
            Update(newModel);
            return newModel;
        }
        public void Delete(int id)
        {
            List<MaintanceBillStores> DeleteList = dbSet.Where(p => p.SerialNumber == id).ToList();
            foreach (var item in DeleteList)
            {
                Delete(item);
            }
        }
        public void DeletePhysical(int id)
        {
            List<MaintanceBillStores> DeleteList = dbSet.Where(p => p.SerialNumber == id).ToList();
            foreach (var item in DeleteList)
            {
                item.IsDeleted = true;
                Remove(item);
            }
        }
    }
}
