﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.StoreTransaction.Sales;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasProject.Domain.Services.StoreTransaction.Sales
{
    public class SalesReturnsItemsService : Repository<SalesReturnsItems>
    {
        public SalesReturnsItemsService(DBContext context) : base(context)
        {
        }
        //public IEnumerable<SalesReturnsItemsVM> GetAll()
        //{//.Where(g => g.IsDeleted == false)
        //    return dbSet.Select(s => new SalesReturnsItemsVM
        //    {
        //        ID = s.ID,
        //        IsDeleted = s.IsDeleted,
        //     SerialNumber=(int)s.SerialNumber,
        //     ItemId=(int)s.ItemId,
        //     Price=s.Price,
        //     Quantity=s.Quantity,
        //     SavedPrice=s.SavedPrice,
        //     Total=s.Total,
        //    }).OrderBy(s => s.ID);
        //}
        public List<SalesReturnsItemsVM> GetBySerial(int Serial)
        {//&& x.IsDeleted == false, int StoreID&& x.StoreId == StoreID 
            return dbSet.Where(x => x.SerialNumber == Serial && x.IsDeleted == false).Select(s => new SalesReturnsItemsVM
            {
                //Identifer = index + 1,
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                ItemId = (int)s.ItemId,
                Box_ParCode = s.Item.National_ParCode,
                Price = s.Price,
                Quantity = s.Quantity,
                SavedPrice = s.SavedPrice,
                Total = s.Total,
                ItemName = s.Item.Name,
                
            }).ToList();
        }
        public SalesReturnsItems Add(SalesReturnsItemsVM s)
        {
            var newModel = new SalesReturnsItems
            {
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                ItemId = (int)s.ItemId,
                Price = s.Price,
                Quantity = s.Quantity,
                SavedPrice = s.SavedPrice,
                Total = s.Total,
                StoreId = s.StoreId,
                

            };
            Insert(newModel);
            return newModel;
        }
        public SalesReturnsItems Edit(SalesReturnsItemsVM s)
        {
            var newModel = new SalesReturnsItems
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                ItemId = (int)s.ItemId,
                Price = s.Price,
                Quantity = s.Quantity,
                SavedPrice = s.SavedPrice,
                Total = s.Total,
                StoreId = s.StoreId,

            };
            Update(newModel);
            return newModel;
        }
        public void Delete(int id)
        {
          List<SalesReturnsItems> DeleteList = dbSet.Where(p => p.SerialNumber == id).ToList();
            foreach (var item in DeleteList)
            {
                Delete(item);
            }
        }
        public void DeletePhysical(int id)
        {
            List<SalesReturnsItems> DeleteList = dbSet.Where(p => p.SerialNumber == id).ToList();
            foreach (var item in DeleteList)
            {
                item.IsDeleted = true;
                Remove(item);
            }
        }
    }
}
