﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.StoreTransaction.Sales;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasProject.Domain.Services.StoreTransaction.Sales
{
  public   class SalesBillStoresService : Repository<SalesBillStores>
    {
        public SalesBillStoresService(DBContext context) : base(context)
        {
        }
        public List<SalesBillStoresVM> GetBySerial(int Serial)
        {//&& x.IsDeleted == false
            return dbSet.Where(x => x.SerialNumber == Serial && x.IsDeleted == false && x.StoreId!=null).Select(s => new SalesBillStoresVM
            {
                //Identifer = index + 1,
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                StoreId = s.StoreId,
                Identifer = s.Name == null ? 0 : int.Parse(s.Name),
            }).ToList();
        }
        public List<SalesBillStoresVM> GetByStoreId(int StoreId)
        {//&& x.IsDeleted == false
            return dbSet.Where(x => x.StoreId == StoreId && x.IsDeleted == false).Select(s => new SalesBillStoresVM
            {
                //Identifer = index + 1,
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                StoreId = s.StoreId,
                Identifer = s.Name == null ? 0 : int.Parse(s.Name),
            }).ToList();
        }
        public SalesBillStores Add(SalesBillStoresVM s)
        {
            var newModel = new SalesBillStores
            {
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                StoreId = s.StoreId,
                CreatedDate = DateTime.Now,
                Name = s.Identifer.ToString(),
            };
            Insert(newModel);
            return newModel;
        }
        public SalesBillStores Edit(SalesBillStoresVM s)
        {
            SalesBillStores edited = dbSet.FirstOrDefault(x => x.ID == s.ID);
            edited.SerialNumber = (int)s.SerialNumber;
            edited.StoreId = s.StoreId;
            edited.ModifiedDate = DateTime.Now;
            edited.Name = s.Identifer.ToString();
            Update(edited);
            return edited;
        }
        public void Delete(int id)
        {
            List<SalesBillStores> DeleteList = dbSet.Where(p => p.SerialNumber == id).ToList();
            foreach (var item in DeleteList)
            {
                Delete(item);
            }
        }
        public void DeletePhysical(int id)
        {
            List<SalesBillStores> DeleteList = dbSet.Where(p => p.SerialNumber == id).ToList();
            foreach (var item in DeleteList)
            {
                item.IsDeleted = true;
                Remove(item);
            }
        }
    }
}
