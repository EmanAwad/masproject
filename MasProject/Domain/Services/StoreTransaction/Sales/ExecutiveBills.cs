﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.StoreTransaction.Sales;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasProject.Domain.Services.StoreTransaction.Sales
{
    public class ExecutiveBillsService : Repository<ExecutiveBills>
    {
        DBContext _Context;

        public ExecutiveBillsService(DBContext context) : base(context)
        {
            _Context = context;
        }
        public IEnumerable<ExecutiveBillsVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new ExecutiveBillsVM
            {
                ID = s.ID,
                //IsDeleted = s.IsDeleted,
                //Note = s.Note,
                //ShowNotesFlag=s.ShowNotesFlag,
                //ApproveId=s.ApproveId,
                TotalAfterDiscount = s.TotalAfterDiscount,
                TotalPrice = s.TotalPrice,
                //UserId=s.UserId,
                //StoreId=(int)s.StoreId,
                SerialNumber = s.SerialNumber,
                //RevewId=s.RevewId,
                //ModifiedDate=s.ModifiedDate,
                //ModifiedBy=s.ModifiedBy,
                //ClientId= (int)s.ClientId,
                //CreatedBy=s.CreatedBy,
                //CreatedDate=s.CreatedDate,
                Date = s.Date,
                //DealTypeId= (int)s.DealTypeId,
                //DiscountPrecentage=s.DiscountPrecentage,
                //DiscountValue=s.DiscountValue,
                //FlagType=s.FlagType,
                Client = s.Client,
                TotalAfterTax = s.TotalAfterTax,
                ClientId = (int)s.ClientId,
            }).OrderByDescending(s => s.Date.Date).ThenBy(x => x.SerialNumber);
        }
        public IEnumerable<ExecutiveBillsVM> GetAllByClient(int ClientId)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.ClientId == ClientId).Select(s => new ExecutiveBillsVM
            {
                ID = s.ID,
                // IsDeleted = s.IsDeleted,
                //Note = s.Note,
                //ShowNotesFlag=s.ShowNotesFlag,
                //ApproveId=s.ApproveId,
                TotalAfterDiscount = s.TotalAfterDiscount,
                TotalPrice = s.TotalPrice,
                //UserId=s.UserId,
                //StoreId=(int)s.StoreId,
                SerialNumber = s.SerialNumber,
                //RevewId=s.RevewId,
                //ModifiedDate=s.ModifiedDate,
                //ModifiedBy=s.ModifiedBy,
                ClientId = (int)s.ClientId,
                //CreatedBy=s.CreatedBy,
                //CreatedDate=s.CreatedDate,
                Date = s.Date,
                //DealTypeId= (int)s.DealTypeId,
                //DiscountPrecentage=s.DiscountPrecentage,
                //DiscountValue=s.DiscountValue,
                //FlagType=s.FlagType,
                Client = s.Client,
                //   AddtionTax = s.AddtionTax,
                //AddtionTaxAmount = s.AddtionTaxAmount,
                //SourceDeduction = s.SourceDeduction,
                //SourceDeductionAmount = s.SourceDeductionAmount,
                TotalAfterTax = s.TotalAfterTax,

            }).OrderByDescending(s => s.Date.Date).ThenBy(x => x.SerialNumber);
        }
        public IEnumerable<ExecutiveBillsVM> GetAllByBranch(int BranchId)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.BranchId == BranchId).Select(s => new ExecutiveBillsVM
            {
                ID = s.ID,
                // IsDeleted = s.IsDeleted,
                //Note = s.Note,
                //ShowNotesFlag=s.ShowNotesFlag,
                //ApproveId=s.ApproveId,
                TotalAfterDiscount = s.TotalAfterDiscount,
                TotalPrice = s.TotalPrice,
                //UserId=s.UserId,
                //StoreId=(int)s.StoreId,
                SerialNumber = s.SerialNumber,
                //RevewId=s.RevewId,
                //ModifiedDate=s.ModifiedDate,
                //ModifiedBy=s.ModifiedBy,
                ClientId = (int)s.ClientId,
                //CreatedBy=s.CreatedBy,
                //CreatedDate=s.CreatedDate,
                Date = s.Date,
                //DealTypeId= (int)s.DealTypeId,
                //DiscountPrecentage=s.DiscountPrecentage,
                //DiscountValue=s.DiscountValue,
                //FlagType=s.FlagType,
                Client = s.Client,
                //   AddtionTax = s.AddtionTax,
                //AddtionTaxAmount = s.AddtionTaxAmount,
                //SourceDeduction = s.SourceDeduction,
                //SourceDeductionAmount = s.SourceDeductionAmount,
                TotalAfterTax = s.TotalAfterTax,

            }).OrderByDescending(s => s.Date.Date).ThenBy(x => x.SerialNumber);
        }
        public decimal? GetBillIdByClient(int ClientId, int ThisSerial, int ItemID)
        {
            decimal? ReturnPrice = 0;
            var BillsList = dbSet.Where(x => x.IsDeleted == false && x.ClientId == ClientId && x.SerialNumber != ThisSerial).OrderByDescending(c => c.ID);

            foreach (var item in BillsList)
            {
                var serial = dbSet.Where(c => c.ID == item.ID).FirstOrDefault().SerialNumber;
                var ItemForSerialExist = _Context.ExecutiveItems.Where(x => x.IsDeleted == false && x.ItemId == ItemID && x.SerialNumber == serial).FirstOrDefault();
                if (ItemForSerialExist != null)
                {
                    ReturnPrice = ItemForSerialExist.Price;
                    break;
                }
            }
            return ReturnPrice;
        }
        public ExecutiveBillsVM Get(int id)
        {//
            return dbSet.Where(x => x.ID == id&& x.IsDeleted == false).Select(s => new ExecutiveBillsVM
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                Note = s.Note,
                ShowNotesFlag = s.ShowNotesFlag,
                ApproveId = s.ApproveId,
                TotalAfterDiscount = s.TotalAfterDiscount,
                TotalPrice = s.TotalPrice,
                UserId = s.UserId,
                //StoreId = (int)s.StoreId,
                SerialNumber = s.SerialNumber,
                RevewId = s.RevewId,
                ModifiedDate = s.ModifiedDate,
                ModifiedBy = s.ModifiedBy,
                ClientId = (int)s.ClientId,
                CreatedBy = s.CreatedBy,
                CreatedDate = s.CreatedDate,
                Date = s.Date.Date,
                DealTypeId = (int)s.DealTypeId,
                DiscountPrecentage = s.DiscountPrecentage,
                DiscountValue = s.DiscountValue,
                FlagType = s.FlagType,
                AddtionTax = s.AddtionTax,
                AddtionTaxAmount = s.AddtionTaxAmount,
                SourceDeduction = s.SourceDeduction,
                SourceDeductionAmount = s.SourceDeductionAmount,
                TotalAfterTax = s.TotalAfterTax,
                IsConverted=s.IsConverted,
                MultiStore = s.MultiStore,
                BranchId = s.BranchId,
                ClientName = s.Client.Name
             
            }).FirstOrDefault();
        }
        public int GetId(int Serial)
        {
            return dbSet.Where(x => x.SerialNumber == Serial && x.IsDeleted == false).FirstOrDefault().ID;
        }
        public ExecutiveBills Add(ExecutiveBillsVM s)
        {
            var newModel = new ExecutiveBills
            {
               // ID = s.ID,
                //IsDeleted = s.IsDeleted,
                //Note = s.Note,
                //ShowNotesFlag = s.ShowNotesFlag,
               // ApproveId = s.ApproveId,
               // TotalAfterDiscount = s.TotalAfterDiscount,
               // TotalPrice = s.TotalPrice,
               // UserId = s.UserId,
              //  StoreId = (int)s.StoreId,
                SerialNumber = s.SerialNumber,
                RevewId = s.RevewId,
                ClientId = (int)s.ClientId,
                CreatedBy = s.CreatedBy,
                CreatedDate = DateTime.Now,
                Date = s.Date.ToLocalTime(),
                DealTypeId = (int)s.DealTypeId,
               // DiscountPrecentage = s.DiscountPrecentage,
               // DiscountValue = s.DiscountValue,
                FlagType = s.FlagType,
               // AddtionTax = s.AddtionTax,
               // AddtionTaxAmount = s.AddtionTaxAmount,
               //// SourceDeduction = s.SourceDeduction,
               // SourceDeductionAmount = s.SourceDeductionAmount,
                TotalAfterTax = s.TotalAfterTax,
                //  IsConverted=false,
                MultiStore = s.MultiStore,
                BranchId = s.BranchId,
               
            };
            Insert(newModel);
            return newModel;
        }
        public ExecutiveBills Edit(ExecutiveBillsVM s)
        {
            ExecutiveBills edited = dbSet.FirstOrDefault(p => p.ID == s.ID);
            edited.Note = s.Note;
            edited.ShowNotesFlag = s.ShowNotesFlag;
            edited.ApproveId = s.ApproveId;
            edited.TotalAfterDiscount = s.TotalAfterDiscount;
            edited.TotalPrice = s.TotalPrice;
            edited.UserId = s.UserId;
          //  edited.StoreId = (int)s.StoreId;
            edited.SerialNumber = s.SerialNumber;
            edited.RevewId = s.RevewId;
            edited.ModifiedDate = DateTime.Now;
            edited.ModifiedBy = s.ModifiedBy;
            edited.ClientId = (int)s.ClientId;
            edited.Date = s.Date.ToLocalTime();
            edited.DealTypeId = (int)s.DealTypeId;
            edited.DiscountPrecentage = s.DiscountPrecentage;
            edited.DiscountValue = s.DiscountValue;
            edited.FlagType = s.FlagType;
            edited.AddtionTax = s.AddtionTax;
            edited.AddtionTaxAmount = s.AddtionTaxAmount;
            edited.SourceDeduction = s.SourceDeduction;
            edited.SourceDeductionAmount = s.SourceDeductionAmount;
            edited.TotalAfterTax = s.TotalAfterTax;
            edited.IsConverted = s.IsConverted;
            edited.BranchId = s.BranchId;
           
            Update(edited);
            return edited;
        }
        public void Delete(int id)
        {
            ExecutiveBills edited = dbSet.FirstOrDefault(p => p.ID == id && p.IsDeleted == false);
            Delete(edited);
        }
       
        public DateTime? GetLastDateByClient(int ClientId)
        {
            DateTime? ReturnDate=null;
            var exist = dbSet.Where(x => x.ClientId == ClientId && x.IsDeleted == false);
            if (exist.Count() != 0)
            {
                ReturnDate= exist.Max(x => x.Date);
            }
            return ReturnDate;
        }
    }
}
