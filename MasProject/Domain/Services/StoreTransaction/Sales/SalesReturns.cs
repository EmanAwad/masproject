﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.StoreTransaction.Sales;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasProject.Domain.Services.StoreTransaction.Sales
{
    public class SalesReturnsService : Repository<SalesReturns>
    {
        DBContext _Context;

        public SalesReturnsService(DBContext context) : base(context)
        {
            _Context = context;

        }
        public IEnumerable<SalesReturnsVM> GetAllByClient(int ClientId)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.ClientId == ClientId).Select(s => new SalesReturnsVM
            {
                ID = s.ID,
                // IsDeleted = s.IsDeleted,
                //Note = s.Note,
                //ShowNotesFlag=s.ShowNotesFlag,
                //ApproveId=s.ApproveId,
                TotalAfterDiscount = s.TotalAfterDiscount,
                TotalPrice = s.TotalPrice,
                //UserId=s.UserId,
                //StoreId=(int)s.StoreId,
                SerialNumber = s.SerialNumber,
                //RevewId=s.RevewId,
                //ModifiedDate=s.ModifiedDate,
                //ModifiedBy=s.ModifiedBy,
                ClientId = (int)s.ClientId,
                //CreatedBy=s.CreatedBy,
                //CreatedDate=s.CreatedDate,
                Date = s.Date,
                //DealTypeId= (int)s.DealTypeId,
                //DiscountPrecentage=s.DiscountPrecentage,
                //DiscountValue=s.DiscountValue,
                //FlagType=s.FlagType,
                Client = s.Client,
                //   AddtionTax = s.AddtionTax,
                //AddtionTaxAmount = s.AddtionTaxAmount,
                //SourceDeduction = s.SourceDeduction,
                //SourceDeductionAmount = s.SourceDeductionAmount,
                TotalAfterTax = s.TotalAfterTax,

            }).OrderByDescending(s => s.Date.Date).ThenBy(x => x.SerialNumber);
        }
        public IEnumerable<SalesReturnsVM> GetAllByBranch(int BranchId)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.BranchId == BranchId).Select(s => new SalesReturnsVM
            {
                ID = s.ID,
                // IsDeleted = s.IsDeleted,
                //Note = s.Note,
                //ShowNotesFlag=s.ShowNotesFlag,
                //ApproveId=s.ApproveId,
                TotalAfterDiscount = s.TotalAfterDiscount,
                TotalPrice = s.TotalPrice,
                //UserId=s.UserId,
                //StoreId=(int)s.StoreId,
                SerialNumber = s.SerialNumber,
                //RevewId=s.RevewId,
                //ModifiedDate=s.ModifiedDate,
                //ModifiedBy=s.ModifiedBy,
                ClientId = (int)s.ClientId,
                //CreatedBy=s.CreatedBy,
                //CreatedDate=s.CreatedDate,
                Date = s.Date,
                //DealTypeId= (int)s.DealTypeId,
                //DiscountPrecentage=s.DiscountPrecentage,
                //DiscountValue=s.DiscountValue,
                //FlagType=s.FlagType,
                Client = s.Client,
                //   AddtionTax = s.AddtionTax,
                //AddtionTaxAmount = s.AddtionTaxAmount,
                //SourceDeduction = s.SourceDeduction,
                //SourceDeductionAmount = s.SourceDeductionAmount,
                TotalAfterTax = s.TotalAfterTax,

            }).OrderByDescending(s => s.Date.Date).ThenBy(x => x.SerialNumber);
        }
        public IEnumerable<SalesReturnsVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new SalesReturnsVM
            {
                ID = s.ID,
                //IsDeleted = s.IsDeleted,
                //IsConverted=s.IsConverted,
                //Note = s.Note,
                //ShowNotesFlag=s.ShowNotesFlag,
                //ApproveId=s.ApproveId,
                TotalAfterDiscount=s.TotalAfterDiscount,
                TotalPrice=s.TotalPrice,
                //UserId=s.UserId,
                //StoreId=s.StoreId,
                SerialNumber=s.SerialNumber,
                //RevewId=s.RevewId,
                //ModifiedDate=s.ModifiedDate,
                //ModifiedBy=s.ModifiedBy,
                //ClientId= s.ClientId.Value,
                //CreatedBy=s.CreatedBy,
                //CreatedDate=s.CreatedDate,
                Date=s.Date,
                //DealTypeId= s.DealTypeId,
                //DiscountPrecentage=s.DiscountPrecentage,
                //DiscountValue=s.DiscountValue,
                //FlagType=s.FlagType,
                Client =  s.Client ,
                TotalAfterTax = s.TotalAfterTax,
                ClientId= (int)s.ClientId,
            }).OrderByDescending(s => s.Date.Date).ThenBy(x => x.SerialNumber);
        }
        public SalesReturnsVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id&& x.IsDeleted == false).Select(s => new SalesReturnsVM
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                Note = s.Note,
                ShowNotesFlag = s.ShowNotesFlag,
                ApproveId = s.ApproveId,
                TotalAfterDiscount = s.TotalAfterDiscount,
                TotalPrice = s.TotalPrice,
                UserId = s.UserId,
               // StoreId = s.StoreId,
                SerialNumber = s.SerialNumber,
                RevewId = s.RevewId,
                ModifiedDate = s.ModifiedDate,
                ModifiedBy = s.ModifiedBy,
                ClientId = s.ClientId.Value,
                CreatedBy = s.CreatedBy,
                CreatedDate = s.CreatedDate,
                Date = s.Date.Date,
                //DealTypeId = s.DealTypeId,
                DiscountPrecentage = s.DiscountPrecentage,
                DiscountValue = s.DiscountValue,
                FlagType = s.FlagType,
                AddtionTax = s.AddtionTax,
                AddtionTaxAmount = s.AddtionTaxAmount,
                SourceDeduction = s.SourceDeduction,
                SourceDeductionAmount = s.SourceDeductionAmount,
                TotalAfterTax = s.TotalAfterTax,
                IsConverted = s.IsConverted,
                ClientName = s.Client.Name,
                StoreId = s.StoreId
               
            }).FirstOrDefault();
        }
        public int GetId(int Serial)
        {
            return dbSet.Where(x => x.SerialNumber == Serial && x.IsDeleted == false).FirstOrDefault().ID;
        }
        public SalesReturns Add(SalesReturnsVM s)
        {
            var newModel = new SalesReturns
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                Note = s.Note,
                ShowNotesFlag = s.ShowNotesFlag,
                ApproveId = s.ApproveId,
                TotalAfterDiscount = s.TotalAfterDiscount,
                TotalPrice = s.TotalPrice,
                UserId = s.UserId,
               // StoreId = s.StoreId,
                SerialNumber = s.SerialNumber,
                RevewId = s.RevewId,
                ClientId = s.ClientId,
                CreatedBy = s.CreatedBy,
                CreatedDate = DateTime.Now,
                Date = s.Date.ToLocalTime(),
              //  DealTypeId = s.DealTypeId,
                DiscountPrecentage = s.DiscountPrecentage,
                DiscountValue = s.DiscountValue,
                FlagType = s.FlagType,
                AddtionTax = s.AddtionTax,
                AddtionTaxAmount = s.AddtionTaxAmount,
                SourceDeduction = s.SourceDeduction,
                SourceDeductionAmount = s.SourceDeductionAmount,
                TotalAfterTax = s.TotalAfterTax,
                StoreId = s.StoreId
                
            };
            Insert(newModel);
            return newModel;
        }
        public SalesReturns Edit(SalesReturnsVM s)
        {
            SalesReturns edited = dbSet.FirstOrDefault(p => p.ID == s.ID);
            edited.Note = s.Note;
            edited.IsConverted = s.IsConverted;
            edited.ShowNotesFlag = s.ShowNotesFlag;
            edited.ApproveId = s.ApproveId;
            edited.TotalAfterDiscount = s.TotalAfterDiscount;
            edited.TotalPrice = s.TotalPrice;
            edited.UserId = s.UserId;
           // edited.StoreId = s.StoreId;
            edited.SerialNumber = s.SerialNumber;
            edited.RevewId = s.RevewId;
            edited.ModifiedDate = DateTime.Now;
            edited.ModifiedBy = s.ModifiedBy;
            edited.ClientId = s.ClientId;
            edited.Date = s.Date.ToLocalTime();
            //edited.DealTypeId = s.DealTypeId;
            edited.DiscountPrecentage = s.DiscountPrecentage;
            edited.DiscountValue = s.DiscountValue;
            edited.FlagType = s.FlagType;
            edited.AddtionTax = s.AddtionTax;
            edited.AddtionTaxAmount = s.AddtionTaxAmount;
            edited.SourceDeduction = s.SourceDeduction;
            edited.SourceDeductionAmount = s.SourceDeductionAmount;
            edited.TotalAfterTax = s.TotalAfterTax;
            edited.StoreId = s.StoreId;
           
            Update(edited);
            return edited;
        }
        public void Delete(int id)
        {
            SalesReturns edited = dbSet.FirstOrDefault(p => p.ID == id);
            Delete(edited);
        }
     

        public DateTime? GetLastDateByClient(int ClientId)
        {
            DateTime? ReturnDate = null;
            var exist = dbSet.Where(x => x.ClientId == ClientId && x.IsDeleted == false);
            if (exist.Count() != 0)
            {
                ReturnDate = exist.Max(x => x.Date);
            }
            return ReturnDate;
        }
    }
}
