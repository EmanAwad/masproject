﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.StoreTransaction.Sales;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasProject.Domain.Services.StoreTransaction.Sales
{
 public   class SalesReturnsBillStoresService : Repository<SalesReturnsBillStores>
    {
        public SalesReturnsBillStoresService(DBContext context) : base(context)
        {
        }
        public List<SalesReturnsBillStoresVM> GetBySerial(int Serial)
        {//&& x.IsDeleted == false
            return dbSet.Where(x => x.SerialNumber == Serial && x.IsDeleted == false && x.StoreId != null).Select(s => new SalesReturnsBillStoresVM
            {
                //Identifer = index + 1,
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                StoreId = s.StoreId,
            }).ToList();
        }

        public SalesReturnsBillStores Add(SalesReturnsBillStoresVM s)
        {
            var newModel = new SalesReturnsBillStores
            {
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                StoreId = s.StoreId,
                CreatedDate = DateTime.Now,
            };
            Insert(newModel);
            return newModel;
        }
        public SalesReturnsBillStores Edit(SalesReturnsBillStoresVM s)
        {
            var newModel = new SalesReturnsBillStores
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                StoreId = s.StoreId,
                ModifiedDate = DateTime.Now,
            };
            Update(newModel);
            return newModel;
        }
        public void Delete(int id)
        {
            List<SalesReturnsBillStores> DeleteList = dbSet.Where(p => p.SerialNumber == id).ToList();
            foreach (var item in DeleteList)
            {
                Delete(item);
            }
        }
        public void DeletePhysical(int id)
        {
            List<SalesReturnsBillStores> DeleteList = dbSet.Where(p => p.SerialNumber == id).ToList();
            foreach (var item in DeleteList)
            {
                item.IsDeleted = true;
                Remove(item);
            }
        }
    }
}
