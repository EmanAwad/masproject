﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.StoreTransaction.Sales;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasProject.Domain.Services.StoreTransaction.Sales
{
    public class SalesBillsService : Repository<SalesBills>
    {
        DBContext _Context;

        public SalesBillsService(DBContext context) : base(context)
        {
            _Context = context;
        }
       
        public IQueryable<SalesBillsVM> GetAll()
        {
            //IQueryable<SalesBillsVM> queryIQueryable = null;
            //queryIQueryable = ((IQueryable<SalesBillsVM>)(from m in _Context.SalesBills
            //                                              where m.IsDeleted == false
            //                                              select m 
            // )) ;
            //return queryIQueryable;
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new SalesBillsVM
            {
                ID = s.ID,
                //IsDeleted = s.IsDeleted,
                //Note = s.Note,
                //ShowNotesFlag = s.ShowNotesFlag,
                //ApproveId = s.ApproveId,
                TotalAfterDiscount = s.TotalAfterDiscount,
                TotalPrice = s.TotalPrice,
                UserId = s.UserId,
                StoreId = (int)s.StoreId,
                SerialNumber = (int)s.SerialNumber,
                //RevewId = s.RevewId,
                //ModifiedDate = s.ModifiedDate,
                //ModifiedBy = s.ModifiedBy,
                //ClientId = (int)s.ClientId,
                //CreatedBy = s.CreatedBy,
                //CreatedDate = s.CreatedDate,
                Date = s.Date,
                //DealTypeId = (int)s.DealTypeId,
                //DiscountPrecentage = s.DiscountPrecentage,
                //DiscountValue = s.DiscountValue,
                //FlagType = s.FlagType,
                Client = s.Client,
                //AddtionTax = s.AddtionTax,
                //AddtionTaxAmount = s.AddtionTaxAmount,
                //SourceDeduction = s.SourceDeduction,
                //SourceDeductionAmount = s.SourceDeductionAmount,
                TotalAfterTax = s.TotalAfterTax,

            }).OrderByDescending(s => s.Date.Date).ThenBy(x => x.SerialNumber);
        }
        public IEnumerable<SalesBillsVM> GetAllByClient(int ClientId)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.ClientId == ClientId).Select(s => new SalesBillsVM
            {
                ID = s.ID,
                // IsDeleted = s.IsDeleted,
                //Note = s.Note,
                //ShowNotesFlag=s.ShowNotesFlag,
                //ApproveId=s.ApproveId,
                TotalAfterDiscount = s.TotalAfterDiscount,
                TotalPrice = s.TotalPrice,
                //UserId=s.UserId,
                //StoreId=(int)s.StoreId,
                SerialNumber = s.SerialNumber,
                //RevewId=s.RevewId,
                //ModifiedDate=s.ModifiedDate,
                //ModifiedBy=s.ModifiedBy,
                ClientId = (int)s.ClientId,
                //CreatedBy=s.CreatedBy,
                //CreatedDate=s.CreatedDate,
                Date = s.Date,
                //DealTypeId= (int)s.DealTypeId,
                //DiscountPrecentage=s.DiscountPrecentage,
                //DiscountValue=s.DiscountValue,
                //FlagType=s.FlagType,
                Client = s.Client,
                //   AddtionTax = s.AddtionTax,
                //AddtionTaxAmount = s.AddtionTaxAmount,
                //SourceDeduction = s.SourceDeduction,
                //SourceDeductionAmount = s.SourceDeductionAmount,
                TotalAfterTax = s.TotalAfterTax,
                BranchId=s.BranchId,
                CreatedDate=s.CreatedDate,
            }).OrderByDescending(s => s.Date.Date).ThenBy(x => x.SerialNumber);
        }
        public IEnumerable<SalesBillsVM> GetAllByBranch(int BranchId)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.BranchId == BranchId).Select(s => new SalesBillsVM
            {
                ID = s.ID,
                // IsDeleted = s.IsDeleted,
                //Note = s.Note,
                //ShowNotesFlag=s.ShowNotesFlag,
                //ApproveId=s.ApproveId,
                TotalAfterDiscount = s.TotalAfterDiscount,
                TotalPrice = s.TotalPrice,
                //UserId=s.UserId,
                //StoreId=(int)s.StoreId,
                SerialNumber = s.SerialNumber,
                //RevewId=s.RevewId,
                //ModifiedDate=s.ModifiedDate,
                //ModifiedBy=s.ModifiedBy,
                ClientId = (int)s.ClientId,
                //CreatedBy=s.CreatedBy,
                //CreatedDate=s.CreatedDate,
                Date = s.Date,
                //DealTypeId= (int)s.DealTypeId,
                //DiscountPrecentage=s.DiscountPrecentage,
                //DiscountValue=s.DiscountValue,
                //FlagType=s.FlagType,
                Client = s.Client,
                //   AddtionTax = s.AddtionTax,
                //AddtionTaxAmount = s.AddtionTaxAmount,
                //SourceDeduction = s.SourceDeduction,
                //SourceDeductionAmount = s.SourceDeductionAmount,
                TotalAfterTax = s.TotalAfterTax,

            }).OrderByDescending(s => s.Date.Date).ThenBy(x => x.SerialNumber);
        }

        public IEnumerable<SalesBillsVM> GetAllByDate(DateTime date)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.Date == date).Select(s => new SalesBillsVM
            {
                ID = s.ID,
                // IsDeleted = s.IsDeleted,
                //Note = s.Note,
                //ShowNotesFlag=s.ShowNotesFlag,
                //ApproveId=s.ApproveId,
                TotalAfterDiscount = s.TotalAfterDiscount,
                TotalPrice = s.TotalPrice,
                //UserId=s.UserId,
                //StoreId=(int)s.StoreId,
                SerialNumber = s.SerialNumber,
                //RevewId=s.RevewId,
                //ModifiedDate=s.ModifiedDate,
                //ModifiedBy=s.ModifiedBy,
                ClientId = (int)s.ClientId,
                //CreatedBy=s.CreatedBy,
                //CreatedDate=s.CreatedDate,
                Date = s.Date,
                //DealTypeId= (int)s.DealTypeId,
                //DiscountPrecentage=s.DiscountPrecentage,
                //DiscountValue=s.DiscountValue,
                //FlagType=s.FlagType,
                Client = s.Client,
                //   AddtionTax = s.AddtionTax,
                //AddtionTaxAmount = s.AddtionTaxAmount,
                //SourceDeduction = s.SourceDeduction,
                //SourceDeductionAmount = s.SourceDeductionAmount,
                TotalAfterTax = s.TotalAfterTax,

            }).OrderByDescending(s => s.Date.Date).ThenBy(x => x.SerialNumber);
        }
        public decimal? GetBillIdByClient(int ClientId,int ThisSerial, int ItemID)
        {
            decimal? ReturnPrice=0;
            var BillsList = dbSet.Where(x => x.IsDeleted == false && x.ClientId == ClientId && x.SerialNumber != ThisSerial).OrderByDescending(c => c.ID);
            foreach (var item in BillsList)
            {
                var serial = dbSet.Where(c => c.ID == item.ID).FirstOrDefault().SerialNumber;
                var ItemForSerialExist = _Context.SalesItems.Where(x => x.IsDeleted == false && x.ItemId == ItemID && x.SerialNumber == serial).FirstOrDefault();
                if (ItemForSerialExist != null)
                {
                    ReturnPrice = ItemForSerialExist.Price;
                    break;
                }
            }
            return ReturnPrice;
        }
        public SalesBillsVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new SalesBillsVM
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                Note = s.Note,
                ShowNotesFlag = s.ShowNotesFlag,
                ApproveId = s.ApproveId,
                TotalAfterDiscount = s.TotalAfterDiscount,
                TotalPrice = s.TotalPrice,
                UserId = s.UserId,
                //StoreId = (int)s.StoreId,
                SerialNumber = s.SerialNumber,
                RevewId = s.RevewId,
                ModifiedDate = s.ModifiedDate,
                ModifiedBy = s.ModifiedBy,
                ClientId = (int)s.ClientId,
                CreatedBy = s.CreatedBy,
                CreatedDate = s.CreatedDate,
                Date = s.Date.Date,
                DealTypeId = (int)s.DealTypeId,
                DiscountPrecentage = s.DiscountPrecentage,
                DiscountValue = s.DiscountValue,
                FlagType = s.FlagType,
                AddtionTax = s.AddtionTax,
                AddtionTaxAmount = s.AddtionTaxAmount,
                SourceDeduction = s.SourceDeduction,
                SourceDeductionAmount = s.SourceDeductionAmount,
                TotalAfterTax = s.TotalAfterTax,
                MultiStore=s.MultiStore,
                BranchId=s.BranchId,
                IsConverted = s.IsConverted,
                ClientName = s.Client.Name
              
            }).FirstOrDefault();
        }
        public IEnumerable<SalesBillsVM> GetAllForEdit()
        {
            return dbSet.Where(g => g.IsDeleted == false & g.Name=="old").Select(s => new SalesBillsVM
            {
                ID = s.ID,
                TotalAfterDiscount = s.TotalAfterDiscount,
                TotalPrice = s.TotalPrice,
                SerialNumber = s.SerialNumber,
                ClientId = (int)s.ClientId,
                Date = s.Date,
                Client = s.Client,
                TotalAfterTax = s.TotalAfterTax,
                BranchId=s.BranchId,
            }).OrderBy(s => s.BranchId).ThenBy(s => s.Date).ThenBy(s=>s.ID);//
        }

        public int GetId(int Serial)
        {
            var temp = dbSet.Where(x => x.SerialNumber == Serial && x.IsDeleted == false).FirstOrDefault();
            if (temp != null)
            {
                return temp.ID;
            }
            else { return 0; }
        }
        public SalesBillsVM GetIdBySerial(int Serial)
        {
            var temp = dbSet.Where(x => x.SerialNumber == Serial && x.IsDeleted == false).Select(x => new SalesBillsVM
            {
                ID = x.ID,
                BranchId = x.BranchId,
                ClientId=(int)x.ClientId,
                Date=x.Date,
                CreatedDate=x.CreatedDate,
            }).FirstOrDefault();
            if (temp != null)
            {
                return temp;
            }
            else { return null; }
        }
        public SalesBills Add(SalesBillsVM s)
        {
            var newModel = new SalesBills
            {
                SerialNumber = s.SerialNumber,
                ClientId = s.ClientId,
                CreatedBy = s.CreatedBy,
                CreatedDate = DateTime.Now,
                Date = s.Date.ToLocalTime(),
                DealTypeId = s.DealTypeId,
                FlagType = s.FlagType,
                MultiStore=s.MultiStore,
                BranchId = s.BranchId,
          
            };
            Insert(newModel);
            return newModel;
        }
        public SalesBills Edit(SalesBillsVM s)
        {
            SalesBills edited = dbSet.FirstOrDefault(p => p.ID == s.ID);
            edited.Note = s.Note;
            edited.ShowNotesFlag = s.ShowNotesFlag;
            edited.ApproveId = s.ApproveId;
            edited.TotalAfterDiscount = s.TotalAfterDiscount;
            edited.TotalPrice = s.TotalPrice;
            edited.UserId = s.UserId;
           // edited.StoreId = (int)s.StoreId;
            edited.SerialNumber = s.SerialNumber;
            edited.RevewId = s.RevewId;
            edited.ModifiedDate = DateTime.Now;
            edited.ModifiedBy = s.ModifiedBy;
            edited.ClientId = (int)s.ClientId;
            edited.Date = s.Date.ToLocalTime();
            edited.DealTypeId = (int)s.DealTypeId;
            edited.DiscountPrecentage = s.DiscountPrecentage;
            edited.DiscountValue = s.DiscountValue;
            edited.FlagType = s.FlagType;
            edited.AddtionTax = s.AddtionTax;
            edited.AddtionTaxAmount = s.AddtionTaxAmount;
            edited.SourceDeduction = s.SourceDeduction;
            edited.SourceDeductionAmount = s.SourceDeductionAmount;
            edited.TotalAfterTax = s.TotalAfterTax;
            edited.MultiStore = s.MultiStore;
            edited.BranchId = s.BranchId;
            edited.IsConverted = s.IsConverted;
            edited.Name = "";
           Update(edited);
            return edited;
        }
        public void Delete(int id)
        {
            SalesBills edited = dbSet.FirstOrDefault(p => p.ID == id);
            Delete(edited);
        }

      
        //public int GenerateSerialByBranch(int? BranchId)
        //{
        //    string Serial = "";
        //    if (BranchId != null && BranchId > 0)
        //        Serial = _Context.Branch.FirstOrDefault(x => x.IsDeleted == false && x.ID == BranchId).arrangement.ToString();
        //    else
        //        Serial = _Context.Branch.FirstOrDefault(x => x.IsDeleted == false && x.ID == 3).arrangement.ToString();
        //    Serial += DateTime.Now.ToString("yy");//
        //    var model = dbSet.Where(x => x.IsDeleted == false && x.BranchId== BranchId && x.Name == "").OrderByDescending(s => s.SerialNumber).FirstOrDefault();
        //    int SerialNumber = model != null ? (model.SerialNumber.Value + 1) : 0;
        //    Serial += SerialNumber > 0 ? SerialNumber.ToString().Substring(4, SerialNumber.ToString().Length - 4) : "00001";
        //    return int.Parse(Serial);
        //}
        public DateTime? GetLastDateByClient(int ClientId)
        {
            DateTime? ReturnDate = null;
            var exist = dbSet.Where(x => x.ClientId == ClientId && x.IsDeleted == false);
            if (exist.Count() != 0)
            {
                ReturnDate = exist.Max(x => x.Date);
            }
            return ReturnDate;
        }
    }
}
