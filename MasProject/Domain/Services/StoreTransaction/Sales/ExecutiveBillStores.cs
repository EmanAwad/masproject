﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.StoreTransaction.Sales;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasProject.Domain.Services.StoreTransaction.Sales
{
  public  class ExecutiveBillStoresService : Repository<ExecutiveBillStores>
    {
        public ExecutiveBillStoresService(DBContext context) : base(context)
        {
        }
        public List<ExecutiveBillStoresVM> GetBySerial(int Serial)
        {//&& x.IsDeleted == false
            return dbSet.Where(x => x.SerialNumber == Serial && x.IsDeleted == false && x.StoreId != null).Select(s => new ExecutiveBillStoresVM
            {
                //Identifer = index + 1,
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                StoreId = s.StoreId,
                Identifer = s.Name == null ? 0 : int.Parse(s.Name),
            }).ToList();
        }

        public ExecutiveBillStores Add(ExecutiveBillStoresVM s)
        {
            var newModel = new ExecutiveBillStores
            {
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                StoreId = s.StoreId,
                CreatedDate = DateTime.Now,
                Name = s.Identifer.ToString(),
            };
            Insert(newModel);
            return newModel;
        }
        public ExecutiveBillStores Edit(ExecutiveBillStoresVM s)
        {
            var newModel = new ExecutiveBillStores
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                StoreId = s.StoreId,
                ModifiedDate = DateTime.Now,
                Name = s.Identifer.ToString(),
            };
            Update(newModel);
            return newModel;
        }
        public void Delete(int id)
        {
            List<ExecutiveBillStores> DeleteList = dbSet.Where(p => p.SerialNumber == id && p.IsDeleted == false).ToList();
            foreach (var item in DeleteList)
            {
                Delete(item);
            }
        }
        public void DeletePhysical(int id)
        {
            List<ExecutiveBillStores> DeleteList = dbSet.Where(p => p.SerialNumber == id).ToList();
            foreach (var item in DeleteList)
            {
                item.IsDeleted = true;
                Remove(item);
            }
        }
    }
}
