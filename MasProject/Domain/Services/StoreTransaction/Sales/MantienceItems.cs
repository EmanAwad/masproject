﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.StoreTransaction.Sales;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using MasProject.Domain.ViewModel.TransVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasProject.Domain.Services.StoreTransaction.Sales
{
    public class MantienceItemsService : Repository<MantienceItems>
    {
        public MantienceItemsService(DBContext context) : base(context)
        {
        }
        //public IEnumerable<MantienceItemsVM> GetAll()
        //{//.Where(g => g.IsDeleted == false)
        //    return dbSet.Select(s => new MantienceItemsVM
        //    {
        //        ID = s.ID,
        //        IsDeleted = s.IsDeleted,
        //     SerialNumber=(int)s.SerialNumber,
        //     ItemId=(int)s.ItemId,
        //     Price=s.Price,
        //     Quantity=s.Quantity,
        //     SavedPrice=s.SavedPrice,
        //     Total=s.Total,
        //    }).OrderBy(s => s.ID);
        //}
        public List<MantienceItemsVM> GetBySerial(int Serial, int StoreID, int? StoreIdentifier)
        {//&& x.IsDeleted == false
            string StoreIdentifierStr = "";
            if (StoreIdentifier != 0)
            {
                StoreIdentifierStr = StoreIdentifier.ToString();
            }
            return dbSet.Where(x => x.SerialNumber == Serial && x.StoreId == StoreID && x.Name == StoreIdentifierStr && x.IsDeleted == false).Select(s => new MantienceItemsVM
            {
                //Identifer = index + 1,
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                ItemId = (int)s.ItemId,
                Box_ParCode = s.Item.National_ParCode,
                Price = s.Price,
                Quantity = s.Quantity,
                SavedPrice = s.SavedPrice,
                Total = s.Total,
                ItemName = s.Item.Name
            }).ToList();
        }
        //public List<StoreReportVM> GetByItemId(int ItemId)
        //{
        //    return dbSet.Where(x => x.ItemId == ItemId && x.IsDeleted == false).Select(s => new StoreReportVM
        //    {
        //        SerialNumber = (int)s.SerialNumber,
        //        ItemId = (int)s.ItemId,
        //        Price = (decimal)s.Price,
        //        Quantity = (int)s.Quantity,
        //        Total = (decimal)s.Total,
        //        ItemName = s.Item.Name,
        //        StoreIdentifier = s.Name,
        //    }).ToList();
        //}
        public MantienceItems Add(MantienceItemsVM s)
        {
            var newModel = new MantienceItems
            {
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                ItemId = (int)s.ItemId,
                Price = s.Price,
                Quantity = s.Quantity,
                SavedPrice = s.SavedPrice,
                Total = s.Total,
                StoreId = s.StoreId,
                Name = s.Identifer.ToString(),
            };
            Insert(newModel);
            return newModel;
        }
        //public MantienceItems Edit(MantienceItemsVM s)
        //{
        //    var newModel = new MantienceItems
        //    {
        //        ID = s.ID,
        //        IsDeleted = s.IsDeleted,
        //        SerialNumber = (int)s.SerialNumber,
        //        ItemId = (int)s.ItemId,
        //        Price = s.Price,
        //        Quantity = s.Quantity,
        //        SavedPrice = s.SavedPrice,
        //        Total = s.Total,
        //        StoreId = s.StoreId,

        //    };
        //    Update(newModel);
        //    return newModel;
        //}
        public void Delete(int id)
        {
            List<MantienceItems> DeleteList = dbSet.Where(p => p.SerialNumber == id).ToList();
            foreach (var item in DeleteList)
            {
                Delete(item);
            }
        }
        public void DeletePhysical(int id)
        {
            List<MantienceItems> DeleteList = dbSet.Where(p => p.SerialNumber == id).ToList();
            foreach (var item in DeleteList)
            {
                item.IsDeleted = true;
                Remove(item);
            }
        }
    }
}
