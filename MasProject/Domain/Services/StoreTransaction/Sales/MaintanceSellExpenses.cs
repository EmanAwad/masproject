﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.StoreTransaction.Sales;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasProject.Domain.Services.StoreTransaction.Sales
{
    public class MaintanceSellExpensesService : Repository<MaintanceSellExpenses>
    {
        public MaintanceSellExpensesService(DBContext context) : base(context)
        {
        }

        public List<MaintanceSellExpensesVM> GetBySerial(int Serial)
        {//&& x.IsDeleted == false
            return dbSet.Where(x => x.SerialNumber == Serial && x.IsDeleted == false).Select(s => new MaintanceSellExpensesVM
            {
                //Identifer = index + 1,
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                Amount = s.Amount,
                CreatedBy = s.CreatedBy,
                CreatedDate = s.CreatedDate,
                ModifiedBy = s.ModifiedBy,
                ModifiedDate = s.ModifiedDate,
                Name = s.Name,
                ExpensesId = s.ExpensesId,

            }).ToList();
        }
        public MaintanceSellExpenses Add(MaintanceSellExpensesVM s)
        {
            var newModel = new MaintanceSellExpenses
            {
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                Amount = s.Amount,
                CreatedBy = s.CreatedBy,
                CreatedDate = s.CreatedDate,
                ModifiedBy = s.ModifiedBy,
                ModifiedDate = s.ModifiedDate,
                Name = s.Name,
                ExpensesId = s.ExpensesId,

            };
            Insert(newModel);
            return newModel;
        }
        public MaintanceSellExpenses Edit(MaintanceSellExpensesVM s)
        {
            var newModel = new MaintanceSellExpenses
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                Amount = s.Amount,
                CreatedBy = s.CreatedBy,
                CreatedDate = s.CreatedDate,
                ModifiedBy = s.ModifiedBy,
                ModifiedDate = s.ModifiedDate,
                Name = s.Name,
                ExpensesId = s.ExpensesId,

            };
            Update(newModel);
            return newModel;
        }
        public void Delete(int id)
        {
            List<MaintanceSellExpenses> DeleteList = dbSet.Where(p => p.SerialNumber == id).ToList();
            foreach (var item in DeleteList)
            {
                Delete(item);
            }
        }
        public void DeletePhysical(int id)
        {
            List<MaintanceSellExpenses> DeleteList = dbSet.Where(p => p.SerialNumber == id).ToList();
            foreach (var item in DeleteList)
            {
                item.IsDeleted = true;
                Remove(item);
            }
        }
    }
}
