﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.StoreTransaction.Quotation;
using MasProject.Domain.ViewModel.StoreTransaction.Quotation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MasProject.Domain.Services.StoreTransaction.Quotation
{
    public class QuotationItemsService : Repository<QuotationItems>
    {
        public QuotationItemsService(DBContext context) : base(context)
        {
        }

        //public IEnumerable<QuotationItemsVM> GetAll()
        //{
        //    return dbSet.Select(s => new QuotationItemsVM
        //    {
        //        ID = s.ID,
        //        IsDeleted = s.IsDeleted,
        //        SerialNumber =  s.SerialNumber,
        //        ItemId =  s.ItemId,
        //        Price = s.Price,
        //        Quantity = s.Quantity,
        //        SavedPrice = s.SavedPrice,
        //        Total = s.Total,
        //    }).OrderBy(s => s.ID);
        //}

        public List< QuotationItemsVM> GetBySerial(int Serial)
        {
            return dbSet.Where(x => x.SerialNumber == Serial && x.IsDeleted == false).Select(s => new QuotationItemsVM
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = s.SerialNumber,
                ItemId = s.ItemId.Value,
                Box_ParCode = s.Item.National_ParCode,
                Price = s.Price,
                Quantity = s.Quantity,
                SavedPrice = s.SavedPrice,
                Total = s.Total,
                ItemName = s.Item.Name

            }).ToList();
        }

        public QuotationItems Add(QuotationItemsVM Q)
        {
            var newModel = new QuotationItems
            {
               // ID = Q.ID,
                IsDeleted = Q.IsDeleted,
                SerialNumber =  Q.SerialNumber,
                ItemId =  Q.ItemId,
                Price = Q.Price,
                Quantity = Q.Quantity,
                SavedPrice = Q.SavedPrice,
                Total = Q.Total,
            };
            Insert(newModel);
            return newModel;
        }

        public QuotationItems Edit(QuotationItemsVM Q)
        {
            var newModel = new QuotationItems
            {
                ID = Q.ID,
                IsDeleted = Q.IsDeleted,
                SerialNumber =  Q.SerialNumber,
                ItemId =  Q.ItemId,
                Price = Q.Price,
                Quantity = Q.Quantity,
                SavedPrice = Q.SavedPrice,
                Total = Q.Total,
            };
            Update(newModel);
            return newModel;
        }

        public void Delete(int id)
        {
            var List = dbSet.Where(p => p.SerialNumber == id && p.IsDeleted == false);
            if (List != null)
            {
                List<QuotationItems> DeleteList = List.ToList();
                foreach (var item in DeleteList)
                {
                    Delete(item);
                }
            }
        }
        public void DeletePhysical(int id)
        {
            var List = dbSet.Where(p => p.SerialNumber == id);
            if (List != null)
            {
                List<QuotationItems> DeleteList = List.ToList();
                foreach (var item in DeleteList)
                {
                    item.IsDeleted = true;
                    Remove(item);
                }
            }
        }
    }

}
