﻿
using MasProject.Data.DataAccess;
using MasProject.Data.Models.StoreTransaction.Quotation;
using MasProject.Domain.ViewModel.StoreTransaction.Quotation;
using Microsoft.EntityFrameworkCore.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MasProject.Domain.Services.Quotation
{
   public class QuotationService : Repository<QuotationClass>
    {
        DBContext _Context;
        public QuotationService(DBContext context) : base(context)
        {
            _Context = context;
        }
        public IEnumerable<QuotationVM> GetAllByClient(string ClientId)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.Client == ClientId).Select(s => new QuotationVM
            {
                ID = s.ID,
                // IsDeleted = s.IsDeleted,
                //Note = s.Note,
                //ShowNotesFlag=s.ShowNotesFlag,
                //ApproveId=s.ApproveId,
                TotalAfterDiscount = s.TotalAfterDiscount,
                TotalPrice = s.TotalPrice,
                //UserId=s.UserId,
                //StoreId=(int)s.StoreId,
                SerialNumber = s.SerialNumber,
                //RevewId=s.RevewId,
                //ModifiedDate=s.ModifiedDate,
                //ModifiedBy=s.ModifiedBy,
                Client = s.Client,
                //CreatedBy=s.CreatedBy,
                //CreatedDate=s.CreatedDate,
                Date = s.Date,
                //DealTypeId= (int)s.DealTypeId,
                //DiscountPrecentage=s.DiscountPrecentage,
                //DiscountValue=s.DiscountValue,
                //FlagType=s.FlagType,
             
                //   AddtionTax = s.AddtionTax,
                //AddtionTaxAmount = s.AddtionTaxAmount,
                //SourceDeduction = s.SourceDeduction,
                //SourceDeductionAmount = s.SourceDeductionAmount,
                TotalAfterTax = s.TotalAfterTax,

            }).OrderByDescending(s => s.Date.Date).ThenBy(x => x.SerialNumber);
        }
        public IEnumerable<QuotationVM> GetAllByBranch(int BranchId)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.BranchId == BranchId).Select(s => new QuotationVM
            {
                ID = s.ID,
                // IsDeleted = s.IsDeleted,
                //Note = s.Note,
                //ShowNotesFlag=s.ShowNotesFlag,
                //ApproveId=s.ApproveId,
                TotalAfterDiscount = s.TotalAfterDiscount,
                TotalPrice = s.TotalPrice,
                //UserId=s.UserId,
                //StoreId=(int)s.StoreId,
                SerialNumber = s.SerialNumber,
                //RevewId=s.RevewId,
                //ModifiedDate=s.ModifiedDate,
                //ModifiedBy=s.ModifiedBy,
               
                //CreatedBy=s.CreatedBy,
                //CreatedDate=s.CreatedDate,
                Date = s.Date,
                //DealTypeId= (int)s.DealTypeId,
                //DiscountPrecentage=s.DiscountPrecentage,
                //DiscountValue=s.DiscountValue,
                //FlagType=s.FlagType,
                Client = s.Client,
                //   AddtionTax = s.AddtionTax,
                //AddtionTaxAmount = s.AddtionTaxAmount,
                //SourceDeduction = s.SourceDeduction,
                //SourceDeductionAmount = s.SourceDeductionAmount,
                TotalAfterTax = s.TotalAfterTax,

            }).OrderByDescending(s => s.Date.Date).ThenBy(x => x.SerialNumber);
        }
        public IEnumerable<QuotationVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new QuotationVM
            {
                ID = s.ID,
                //IsDeleted = s.IsDeleted,
                //IsConverted=s.IsConverted,
                //Note = s.Note,
                //ShowNotesFlag=s.ShowNotesFlag,
                //ApproveId=s.ApproveId,
                TotalAfterDiscount = s.TotalAfterDiscount,
                TotalPrice = s.TotalPrice,
                //UserId=s.UserId,
                //StoreId=s.StoreId,
                SerialNumber = s.SerialNumber,
                //RevewId=s.RevewId,
                //ModifiedDate=s.ModifiedDate,
                //ModifiedBy=s.ModifiedBy,
                //ClientId= s.ClientId.Value,
                //CreatedBy=s.CreatedBy,
                //CreatedDate=s.CreatedDate,
                Date = s.Date,
                //DealTypeId= s.DealTypeId,
                //DiscountPrecentage=s.DiscountPrecentage,
                //DiscountValue=s.DiscountValue,
                //FlagType=s.FlagType,
                Client = s.Client,
                TotalAfterTax = s.TotalAfterTax,
                //ClientId = (int)s.ClientId,
            }).OrderByDescending(s => s.Date.Date).ThenBy(x => x.SerialNumber);
        }
        public QuotationVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new QuotationVM
            {
                ID = s.ID,
                Note = s.Note,
                ShowNotesFlag = s.ShowNotesFlag,
                ApproveId = s.ApproveId,
                TotalAfterDiscount = s.TotalAfterDiscount,
                TotalPrice = s.TotalPrice,
                UserId = s.UserId,
                BranchId = s.BranchId,
                SerialNumber = s.SerialNumber,
                RevewId = s.RevewId,
                Client = s.Client,
                Date = s.Date.Date,
                DealTypeId = s.DealTypeId,
                DiscountPrecentage = s.DiscountPrecentage,
                DiscountValue = s.DiscountValue,
                FlagType = s.FlagType,
                AddtionTax = s.AddtionTax,
                AddtionTaxAmount = s.AddtionTaxAmount,
                SourceDeduction = s.SourceDeduction,
                SourceDeductionAmount = s.SourceDeductionAmount,
                TotalAfterTax = s.TotalAfterTax,
                TaxFileNumber = s.TaxFileNumber,
                CreatedDate = s.CreatedDate.ToLocalTime(),
            }).FirstOrDefault();
        }
       
        public QuotationClass Add(QuotationVM s)
        {
            var newModel = new QuotationClass
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                Note = s.Note,
                ShowNotesFlag = s.ShowNotesFlag,
                ApproveId = s.ApproveId,
                TotalAfterDiscount = s.TotalAfterDiscount,
                TotalPrice = s.TotalPrice,
                UserId = s.UserId,
                BranchId = s.BranchId,
                SerialNumber = s.SerialNumber,
                RevewId = s.RevewId,
                Client = s.Client,
                CreatedBy = s.CreatedBy,
                CreatedDate = DateTime.Now,
                Date = s.Date.ToLocalTime(),
                DealTypeId = s.DealTypeId,
                DiscountPrecentage = s.DiscountPrecentage,
                DiscountValue = s.DiscountValue,
                FlagType = s.FlagType,
                AddtionTax = s.AddtionTax,
                AddtionTaxAmount = s.AddtionTaxAmount,
                SourceDeduction = s.SourceDeduction,
                SourceDeductionAmount = s.SourceDeductionAmount,
                TotalAfterTax = s.TotalAfterTax,
                TaxFileNumber = s.TaxFileNumber,
            };
            Insert(newModel);
            return newModel;
        }
        public QuotationClass Edit(QuotationVM s)
        {
            var newModel = new QuotationClass
            {
                ID = s.ID,
                IsConverted=s.IsConverted,
                IsDeleted = s.IsDeleted,
                Note = s.Note,
                ShowNotesFlag = s.ShowNotesFlag,
                ApproveId = s.ApproveId,
                TotalAfterDiscount = s.TotalAfterDiscount,
                TotalPrice = s.TotalPrice,
                UserId = s.UserId,
                BranchId = s.BranchId,
                SerialNumber = s.SerialNumber,
                RevewId = s.RevewId,
                ModifiedDate = DateTime.Now,
                ModifiedBy = s.ModifiedBy,
               CreatedDate = s.CreatedDate.ToLocalTime(),
             //   CreatedDate = s.CreatedDate.HasDefaultValueSql("(getdate())").Metadata.AfterSaveBehavior = PropertySaveBehavior.Throw,
              //  CreatedDate = _Context.Quotations.Where(c => c.CreatedDate == s.CreatedDate).FirstOrDefault(),
                Client = s.Client,
                Date = s.Date.ToLocalTime(),
                DealTypeId = s.DealTypeId,
                DiscountPrecentage = s.DiscountPrecentage,
                DiscountValue = s.DiscountValue,
                FlagType = s.FlagType,
                AddtionTax = s.AddtionTax,
                AddtionTaxAmount = s.AddtionTaxAmount,
                SourceDeduction = s.SourceDeduction,
                SourceDeductionAmount = s.SourceDeductionAmount,
                TotalAfterTax = s.TotalAfterTax,
                TaxFileNumber = s.TaxFileNumber,
            };
            Update(newModel);
            return newModel;
        }

        public void Delete(int id)
        {
            QuotationClass edited = dbSet.FirstOrDefault(p => p.ID == id && p.IsDeleted == false);
            Delete(edited);
        }
    }


}
