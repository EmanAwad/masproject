﻿using MasProject.Data.Models.StoreTransaction.Transfer;
using MasProject.Domain.ViewModel.StoreTransaction.Transfer;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using MasProject.Data.DataAccess;

namespace MasProject.Domain.Services.StoreTransaction.Transfer
{
   public class TransferRequestItemService : Repository<TransferRequestItem>
    {
        public TransferRequestItemService(DbContext context) : base((DBContext)context)
        {
        }
        public IEnumerable<TransferRequestItemVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new TransferRequestItemVM
            {
                ID = s.ID,
                TransferRequestId = s.TransferRequestId,
                RequiredQuantity = s.RequiredQuantity,
                ItemId = s.ItemId.Value,
                ItemName = s.Item.Name,
                BranchFromID = s.BranchFromID.Value,
                BranchFromName = s.BranchFrom.Name,
                IsDeleted = s.IsDeleted,
                Note = s.Note,
                ModifiedDate = s.ModifiedDate,
                ModifiedBy = s.ModifiedBy,
                CreatedBy = s.CreatedBy,
                CreatedDate = s.CreatedDate
            }).OrderBy(s => s.ID);
        }
        public List<TransferRequestItemVM> GetByTransID(int id)
        {
            return dbSet.Where(x => x.TransferRequestId == id && x.IsDeleted == false).Select(s => new TransferRequestItemVM
            {
                ID = s.ID,
                TransferRequestId=s.TransferRequestId,
                RequiredQuantity = s.RequiredQuantity,
                ItemId = s.ItemId,
                ItemName=s.Item.Name,
                ItemImg = s.Item.ItemImg,
                Code =s.Item.National_ParCode,
                BranchFromID = s.BranchFromID,
                IsDeleted = s.IsDeleted,
                Note = s.Note,
                ModifiedDate = s.ModifiedDate,
                ModifiedBy = s.ModifiedBy,
                CreatedBy = s.CreatedBy,
                CreatedDate = s.CreatedDate
            }).ToList();
        }
        public List<TransferOrderDetailsVM> GetByTransOrderID(int id)
        {
            return dbSet.Where(x => x.TransferRequestId == id && x.IsDeleted == false).Select(s => new TransferOrderDetailsVM
            {
                ID = s.ID,
                Quantity = s.RequiredQuantity,
                ItemId = s.ItemId,
                Code = s.Item.National_ParCode,
                BranchFromID = s.BranchFromID,
            }).ToList();
        }
        public TransferRequestItem Add(TransferRequestItemVM s)
        {
            var newModel = new TransferRequestItem
            {
                //ID = s.ID,
                TransferRequestId=s.TransferRequestId,
                RequiredQuantity = s.RequiredQuantity.Value,
                ItemId = s.ItemId,
                BranchFromID = s.BranchFromID,
                IsDeleted = s.IsDeleted,
                Note = s.Note,
                ModifiedDate = s.ModifiedDate,
                ModifiedBy = s.ModifiedBy,
                CreatedBy = s.CreatedBy,
                CreatedDate = DateTime.Now,
            };
            Insert(newModel);
            return newModel;
        }
        public TransferRequestItem Edit(TransferRequestItemVM s)
        {
            TransferRequestItem edited = dbSet.FirstOrDefault(p => p.ID == s.ID);
            edited.RequiredQuantity = s.RequiredQuantity.Value;
            edited.ItemId = s.ItemId;
            edited.BranchFromID = s.BranchFromID;
            edited.Note = s.Note;
            edited.IsDeleted = false;
            edited.ModifiedDate = DateTime.Now;
            edited.ModifiedBy = s.ModifiedBy;
            Update(edited);
            return edited;
        }
        public void Delete(int id)
        {
            List<TransferRequestItem> DeleteList = dbSet.Where(p => p.TransferRequestId == id).ToList();
            foreach (var item in DeleteList)
            {
                Delete(item);
            }
        }
        public void DeletePhysical(int id)
        {
            List<TransferRequestItem> DeleteList = dbSet.Where(p => p.TransferRequestId == id).ToList();
            foreach (var item in DeleteList)
            {
                item.IsDeleted = true;
                Remove(item);
            }
        }
       
    }
}