﻿using MasProject.Data.Models.StoreTransaction.Transfer;
using MasProject.Domain.ViewModel.StoreTransaction.Transfer;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using MasProject.Data.DataAccess;

namespace MasProject.Domain.Services.StoreTransaction.Transfer
{
    public class TransferRequestService : Repository<TransferRequest>
    {
        public TransferRequestService(DbContext context) : base((DBContext)context)
        {
        }

        public IEnumerable<TransferRequestVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new TransferRequestVM
            {
                ID = s.ID,
                Date=s.Date,
                SerialNumber=s.SerialNumber,
                BranchToID = s.BranchToID,
                BranchToName = s.BranchTo.Name,
                IsOrder=s.IsOrder
            }).OrderByDescending(s => s.ID);
        }
        public IEnumerable<TransferRequestVM> GetAllByBranch(int BranchId)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.BranchToID == BranchId).Select(s => new TransferRequestVM
            {
                ID = s.ID,
                Date = s.Date,
                SerialNumber = s.SerialNumber,
                BranchToID = s.BranchToID,
                BranchToName = s.BranchTo.Name,
                IsOrder = s.IsOrder

            }).OrderByDescending(s => s.ID);
        }



        public List<TransferRequestAndOrder> GetTransferRequestByBranch(int BranchId)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.BranchToID == BranchId).Select(s => new TransferRequestAndOrder
            {
                SerialNumber = s.SerialNumber,
                TransRequestId = s.ID,
                BranchToName = s.BranchTo.Name,
                IsOrder = s.IsOrder,
                Date = s.CreatedDate
            }).OrderByDescending(s => s.TransRequestId).ToList();
        }
        public List<TransferRequestAndOrder> GetTransferRequest()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new TransferRequestAndOrder
            {
                SerialNumber = s.SerialNumber,
                TransRequestId = s.ID,
                BranchToName = s.BranchTo.Name,
                IsOrder = s.IsOrder,
                Date = s.CreatedDate
            }).OrderByDescending(s => s.TransRequestId).ToList();
        }
        public TransferRequestVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new TransferRequestVM
            {
                ID = s.ID,
                SerialNumber=s.SerialNumber,
                Date=s.Date.Date,
                BranchToID = s.BranchToID,
                BranchToName = s.BranchTo.Name,
                IsDeleted = s.IsDeleted,
                Note = s.Note,
                ModifiedDate = s.ModifiedDate,
                ModifiedBy = s.ModifiedBy,
                CreatedBy = s.CreatedBy,
                CreatedDate = s.CreatedDate
            }).FirstOrDefault();
        }
        public TransferOrderVM GetTransferRequestAsOrder(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new TransferOrderVM
            {
                ID = s.ID,
                SerialNumber = s.SerialNumber,
                Date = s.Date,
                BranchToID = s.BranchToID
            }).FirstOrDefault();

        }
        public TransferRequest Add(TransferRequestVM s)
        {
            var newModel = new TransferRequest
            {
                ID = s.ID,
                SerialNumber=s.SerialNumber,
                Date=s.Date.ToLocalTime(),
                BranchToID = s.BranchToID,
                IsDeleted = s.IsDeleted,
                Note = s.Note,
                ModifiedDate = s.ModifiedDate,
                ModifiedBy = s.ModifiedBy,
                CreatedBy = s.CreatedBy,
                CreatedDate = DateTime.Now,
                IsOrder = false,
            };
            Insert(newModel);
            return newModel;
        }
        public TransferRequest Edit(TransferRequestVM s)
        {
            TransferRequest edited = dbSet.FirstOrDefault(p => p.ID == s.ID);
            edited.ID = s.ID;
            edited.SerialNumber = s.SerialNumber;
            edited.Date = s.Date.ToLocalTime();
            edited.BranchToID = s.BranchToID;
            edited.Note = s.Note;
            edited.IsOrder = s.IsOrder;
            edited.IsDeleted = false;
            edited.ModifiedDate = DateTime.Now;
            edited.ModifiedBy = s.ModifiedBy;
            Update(edited);
            return edited;
        }
        public void Delete(int id)
        {
            TransferRequest edited = dbSet.FirstOrDefault(p => p.ID == id);
            Delete(edited);
        }
        public int GenerateSerial()
        {
            int serial = dbSet.FirstOrDefault(x => x.IsDeleted == false) != null ? dbSet.Max(c => c.SerialNumber) + 1 : 1;
            return serial;
        }
    }
}
