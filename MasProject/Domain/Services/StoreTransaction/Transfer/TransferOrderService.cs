﻿using MasProject.Data.Models.StoreTransaction.Transfer;
using MasProject.Domain.ViewModel.StoreTransaction.Transfer;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using MasProject.Data.DataAccess;
namespace MasProject.Domain.Services.StoreTransaction.Transfer
{
    public class TransferOrderService : Repository<TransferOrder>
    {
        public TransferOrderService(DbContext context) : base((DBContext)context)
        {
        }
        public IEnumerable<TransferOrderVM> GetAll()
        {
            return  dbSet.Where(g => g.IsDeleted == false).Select(s => new TransferOrderVM
            {
                ID = s.ID,
                SerialNumber=s.SerialNumber,
                Date=s.Date,
                TransferRequestId=s.TransferRequestId,
                BranchToName=s.TransferRequest.BranchTo.Name,
            }).OrderByDescending(s => s.ID);
        }
        //public IEnumerable<TransferOrderVM> GetAllByBranch(int BranchId)
        //{
        //    return dbSet.Where(g => g.IsDeleted == false && g.BranchToID == BranchId).Select(s => new TransferOrderVM
        //    {
        //        ID = s.ID,
        //        SerialNumber = s.SerialNumber,
        //        Date = s.Date,
        //        TransferRequestId = s.TransferRequestId,
        //        BranchToName = s.TransferRequest.BranchTo.Name,
        //        BranchToID=s.BranchToID,
        //    }).OrderByDescending(s => s.ID);
        //}


        public List<TransferRequestAndOrder> GetTransferOrderByBranch(int BranchId)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.BranchToID == BranchId).Select(s => new TransferRequestAndOrder
            {
                SerialNumber = s.SerialNumber,
                TransOrderId = s.ID,
                BranchToName = s.BranchTo.Name,
                IsOrder = true,
                Date = s.CreatedDate
            }).OrderByDescending(s => s.TransOrderId).ToList();
        }
        public List<TransferRequestAndOrder> GetTransferOrder()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new TransferRequestAndOrder
            {
                SerialNumber = s.SerialNumber,
                TransOrderId=s.ID,
                BranchToName = s.BranchTo.Name,
                IsOrder = true,
                Date=s.CreatedDate
            }).OrderByDescending(s => s.TransOrderId).ToList();
        }
        public TransferOrderVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new TransferOrderVM
            {
                ID = s.ID,
                SerialNumber = s.SerialNumber,
                Date = s.Date.Date,
                TransferRequestId =s.TransferRequestId,
                BranchToID=s.BranchToID,
                IsDeleted = s.IsDeleted,
                Note = s.Note,
                ModifiedDate = s.ModifiedDate,
                ModifiedBy = s.ModifiedBy,
                CreatedBy = s.CreatedBy,
                CreatedDate = s.CreatedDate
            }).FirstOrDefault();
        }
        public TransferOrder Add(TransferOrderVM s)
        {
            var newModel = new TransferOrder
            {
                ID = s.ID,
                SerialNumber = s.SerialNumber,
                Date = s.Date.ToLocalTime(),
                BranchToID=s.BranchToID,
                TransferRequestId =s.TransferRequestId,
                IsDeleted = s.IsDeleted,
                Note = s.Note,
                ModifiedDate = s.ModifiedDate,
                ModifiedBy = s.ModifiedBy,
                CreatedBy = s.CreatedBy,
                CreatedDate = DateTime.Now,
            };
            Insert(newModel);
            return newModel;
        }
        public TransferOrder Edit(TransferOrderVM s)
        {
            TransferOrder edited = dbSet.FirstOrDefault(p => p.ID == s.ID);
            edited.ID = s.ID;
            edited.SerialNumber = s.SerialNumber;
            edited.Date = s.Date.ToLocalTime();
            edited.BranchToID = s.BranchToID;
            edited.TransferRequestId = s.TransferRequestId;
            edited.Note = s.Note;
            edited.IsDeleted = false;
            edited.ModifiedDate = DateTime.Now;
            edited.ModifiedBy = s.ModifiedBy;
            edited.IsConverted = s.IsConverted;
            Update(edited);
            return edited;
        }
        public TransferOrder ConvertTransferRequest(TransferRequestVM TransferRequestModel)
        {
            TransferOrder newModel = new TransferOrder
            {
                SerialNumber = TransferRequestModel.SerialNumber,
                Date= TransferRequestModel.Date,
                BranchToID = TransferRequestModel.BranchToID,
                TransferRequestId = TransferRequestModel.ID,
            };
            Insert(newModel);
            return newModel;
        }
        public void Delete(int id)
        {
            TransferOrder edited = dbSet.FirstOrDefault(p => p.ID == id);
            Delete(edited);
        }
        public int GenerateSerial()
        {
            int serial = dbSet.FirstOrDefault(x => x.IsDeleted == false) != null ? dbSet.Max(c => c.SerialNumber) + 1 : 1;
            return serial;
        }
    }
}
