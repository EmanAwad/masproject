﻿using MasProject.Data.Models.StoreTransaction.Transfer;
using MasProject.Domain.ViewModel.StoreTransaction.Transfer;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using MasProject.Data.DataAccess;

namespace MasProject.Domain.Services.StoreTransaction.Transfer
{
    public class TransferOrderDetailsService : Repository<TransferOrderDetails>
    {
        public TransferOrderDetailsService(DbContext context) : base((DBContext)context)
        {
        }
        public IEnumerable<TransferOrderDetailsVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new TransferOrderDetailsVM
            {
                ID = s.ID,
                TransferOrderId = s.TransferOrderId,
                Quantity = s.Quantity,
                ItemId = s.ItemId.Value,
                ItemName = s.Item.Name,
                BranchFromID = s.BranchFromID,
                BranchFromName = s.BranchFrom.Name,
                IsDeleted = s.IsDeleted,
                Note = s.Note,
                ModifiedDate = s.ModifiedDate,
                ModifiedBy = s.ModifiedBy,
                CreatedBy = s.CreatedBy,
                CreatedDate = s.CreatedDate,
               
            }).OrderBy(s => s.ID);
        }
        public List<TransferOrderDetailsVM> GetByTransferOrderID(int id)
        {
            return dbSet.Where(x => x.TransferOrderId == id && x.IsDeleted == false).Select(s => new TransferOrderDetailsVM
            {
                ID = s.ID,
                TransferOrderId = s.TransferOrderId,
                Quantity = s.Quantity,
                ItemId = s.ItemId.Value,
                ItemImg = s.Item.ItemImg,
                BranchFromID = s.BranchFromID,
                IsDeleted = s.IsDeleted,
                Note = s.Note,
                ModifiedDate = s.ModifiedDate,
                ModifiedBy = s.ModifiedBy,
                CreatedBy = s.CreatedBy,
                CreatedDate = s.CreatedDate
            }).ToList();
        }
        public TransferOrderDetails Add(TransferOrderDetailsVM s)
        {
            var newModel = new TransferOrderDetails
            {
              //  ID = s.ID,
                TransferOrderId = s.TransferOrderId,
                Quantity = s.Quantity.Value,
                ItemId = s.ItemId,
                BranchFromID = s.BranchFromID,
                IsDeleted = s.IsDeleted,
                Note = s.Note,
                ModifiedDate = s.ModifiedDate,
                ModifiedBy = s.ModifiedBy,
                CreatedBy = s.CreatedBy,
                CreatedDate = DateTime.Now,
            };
            Insert(newModel);
            return newModel;
        }
        public TransferOrderDetails Edit(TransferOrderDetailsVM s)
        {
            TransferOrderDetails edited = dbSet.FirstOrDefault(p => p.ID == s.ID);
            edited.Quantity = s.Quantity.Value;
            edited.ItemId = s.ItemId;
            edited.BranchFromID = s.BranchFromID;
            edited.Note = s.Note;
            edited.IsDeleted = false;
            edited.ModifiedDate = DateTime.Now;
            edited.ModifiedBy = s.ModifiedBy;
            Update(edited);
            return edited;
        }
        public void  ConvertTransferRequestItem (int TransferOrderId, List<TransferRequestItemVM> TransferRequestItemList)
        {
            foreach(var trans in TransferRequestItemList)
            {
                TransferOrderDetails newModel = new TransferOrderDetails
                {
                    ItemId= trans.ItemId,
                    BranchFromID= trans.BranchFromID,
                    Quantity=trans.RequiredQuantity,
                    TransferOrderId=TransferOrderId
                };
                Insert(newModel);
            }
        }
        public void Delete(int id)
        {
            List<TransferOrderDetails> DeleteList = dbSet.Where(p => p.TransferOrderId == id).ToList();
            foreach (var item in DeleteList)
            {
                Delete(item);
            }
        }
        public void DeletePhysical(int id)
        {
            List<TransferOrderDetails> DeleteList = dbSet.Where(p => p.TransferOrderId == id).ToList();
            foreach (var item in DeleteList)
            {
                item.IsDeleted = true;
                Remove(item);
            }
        }

    }
}