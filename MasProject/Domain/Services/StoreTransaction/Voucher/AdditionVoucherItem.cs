﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.StoreTransaction.Voucher;
using MasProject.Domain.ViewModel.StoreTransaction.Voucher;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasProject.Domain.Services.StoreTransaction.Voucher
{
    public class AdditionVoucherItemService : Repository<AdditionVoucherItem>
    {
        DBContext _Context;

        public AdditionVoucherItemService(DBContext context) : base(context)
        {
            _Context = context;

        }
        public List<AdditionVoucherItemVM> GetBySerial(int Serial, int StoreId)
        {
            return dbSet.Where(x => x.SerialNumber == Serial && x.IsDeleted == false).Select(s => new AdditionVoucherItemVM
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                ItemImg = s.Item.ItemImg,
                SerialNumber = (int)s.SerialNumber,
                ItemId = (int)s.ItemId,
                Box_ParCode = s.Item.National_ParCode,
                SheilfNo = s.SheilfNo ?? _Context.Item_Store.FirstOrDefault(x => x.ItemId == s.ItemId.Value && x.StoreId == StoreId).SheilfNo,
                Quantity = s.Quantity,
                BoxCode = s.BoxCode ?? _Context.Items.FirstOrDefault(x => x.ID == s.ItemId.Value).Box_ParCode,
                ItemName = s.Item.Name
            }).ToList();
        }
        public List<AdditionVoucherItemVM> GetBySerial(int Serial)
        {
            return dbSet.Where(x => x.SerialNumber == Serial && x.IsDeleted == false).Select(s => new AdditionVoucherItemVM
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                ItemImg = s.Item.ItemImg,
                SerialNumber = (int)s.SerialNumber,
                ItemId = (int)s.ItemId,
                  Quantity = s.Quantity,
                ItemName = s.Item.Name
            }).ToList();
        }
        public AdditionVoucherItem Add(AdditionVoucherItemVM s)
        {
            var newModel = new AdditionVoucherItem
            {
                //ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                ItemId = (int)s.ItemId,
                BoxCode = s.BoxCode,
                Quantity = s.Quantity,
                SheilfNo = s.SheilfNo,

            };
            Insert(newModel);
            return newModel;
        }
        public AdditionVoucherItem Edit(AdditionVoucherItemVM s)
        {
            var newModel = new AdditionVoucherItem
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                ItemId = (int)s.ItemId,
                BoxCode = s.BoxCode,
                Quantity = s.Quantity,
                SheilfNo = s.SheilfNo,
            };
            Update(newModel);
            return newModel;
        }
        public AdditionVoucherItem ConvertTempItemToAdditionItem(AdditionVoucherItemVM item)
        {
            AdditionVoucherItem newModel = new AdditionVoucherItem
            {
                IsDeleted = item.IsDeleted,
                SerialNumber = (int)item.SerialNumber,
                ItemId = (int)item.ItemId,
                Quantity = item.Quantity,
                SheilfNo = item.SheilfNo,
                BoxCode = item.BoxCode,
            };
            Insert(newModel);
            return newModel;
        }
        public void Delete(int id)
        {
            List<AdditionVoucherItem> DeleteList = dbSet.Where(p => p.SerialNumber == id).ToList();
            foreach (var item in DeleteList)
            {
                Delete(item);
            }
        }
        public void DeletePhysical(int id)
        {
            List<AdditionVoucherItem> DeleteList = dbSet.Where(p => p.SerialNumber == id).ToList();
            foreach (var item in DeleteList)
            {
                item.IsDeleted = true;
                Remove(item);
            }
        }
    }
}
