﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.StoreTransaction.Voucher;
using MasProject.Domain.ViewModel.StoreTransaction.Voucher;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasProject.Domain.Services.StoreTransaction.Voucher
{
    public class AdditionVoucherService : Repository<AdditionVoucher>
    {
        DBContext _Context;

        public AdditionVoucherService(DBContext context) : base(context)
        {
            _Context = context;

        }
        public IEnumerable<AdditionVoucherVM> GetAllByUserType(int UserType)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.Client_Supplier_Id == UserType).Select(s => new AdditionVoucherVM
            {
                ID = s.ID,
                SerialNumber = s.SerialNumber,
                Name = s.UserType == 1 ? _Context.Client.Where(x => x.ID == s.Client_Supplier_Id).FirstOrDefault().Name : s.UserType == 2 ? _Context.Supplier.Where(x => x.ID == s.Client_Supplier_Id).FirstOrDefault().Name : _Context.Employee.Where(x => x.ID == s.Client_Supplier_Id).FirstOrDefault().Name,
                BillSerialNumber = s.BillSerialNumber,
                BillType = s.BillType,
                Client_Supplier_Id = s.Client_Supplier_Id,
                CreatedBy = s.CreatedBy,
                CreatedDate = s.CreatedDate,
                Date = s.Date,
                Type = 1,

            }).OrderByDescending(s => s.ID);
        }
        public IEnumerable<AdditionVoucherVM> GetAllByStore(int StoreId)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.StoreId == StoreId).Select(s => new AdditionVoucherVM
            {
                ID = s.ID,
                SerialNumber = s.SerialNumber,
                Name = s.UserType == 1 ? _Context.Client.Where(x => x.ID == s.Client_Supplier_Id).FirstOrDefault().Name : s.UserType == 2 ? _Context.Supplier.Where(x => x.ID == s.Client_Supplier_Id).FirstOrDefault().Name : _Context.Employee.Where(x => x.ID == s.Client_Supplier_Id).FirstOrDefault().Name,
                BillSerialNumber = s.BillSerialNumber,
                BillType = s.BillType,
                Client_Supplier_Id = s.Client_Supplier_Id,
                CreatedBy = s.CreatedBy,
                CreatedDate = s.CreatedDate,
                Date = s.Date,
                Type = 1,

            }).OrderByDescending(s => s.ID);
        }
        public IEnumerable<AdditionVoucherVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new AdditionVoucherVM
            {
                ID = s.ID,
                SerialNumber=s.SerialNumber,
                Name=s.UserType==1 ? _Context.Client.Where(x=>x.ID==s.Client_Supplier_Id).FirstOrDefault().Name : s.UserType == 2 ? _Context.Supplier.Where(x => x.ID == s.Client_Supplier_Id).FirstOrDefault().Name : _Context.Employee.Where(x => x.ID == s.Client_Supplier_Id).FirstOrDefault().Name,
                BillSerialNumber =s.BillSerialNumber,
                BillType=s.BillType,
                Client_Supplier_Id =  s.Client_Supplier_Id,
                CreatedBy = s.CreatedBy,
                CreatedDate = s.CreatedDate,
                Date = s.Date,
                Type =1,
              
            }).OrderByDescending(s => s.ID);
        }
        public AdditionVoucherVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id&& x.IsDeleted == false).Select(s => new AdditionVoucherVM
            {
                ID = s.ID,
                ClientName = s.UserType == 1 ? _Context.Client.Where(x => x.ID == s.Client_Supplier_Id).FirstOrDefault().Name : s.UserType == 2 ? _Context.Supplier.Where(x => x.ID == s.Client_Supplier_Id).FirstOrDefault().Name : _Context.Employee.Where(x => x.ID == s.Client_Supplier_Id).FirstOrDefault().Name,
                IsDeleted = s.IsDeleted,
                Note = s.Note,
                ShowNotesFlag = s.ShowNotesFlag,
                ApproveId = s.ApproveId,
                UserId = s.UserId,
                StoreId =  s.StoreId,
                SerialNumber = s.SerialNumber,
                RevewId = s.RevewId,
                ModifiedDate = s.ModifiedDate,
                ModifiedBy = s.ModifiedBy,
                Client_Supplier_Id =  s.Client_Supplier_Id,
                CreatedBy = s.CreatedBy,
                CreatedDate = s.CreatedDate,
                Date = s.Date.Date,
                
            }).FirstOrDefault();
        }
        public AdditionVoucher Add(AdditionVoucherVM s)
        {
            var newModel = new AdditionVoucher
            {
                ID = s.ID,
                UserType=s.UserType,
                BillSerialNumber=s.BillSerialNumber,
                BillType=s.BillType,
                IsDeleted = s.IsDeleted,
                Note = s.Note,
                ShowNotesFlag = s.ShowNotesFlag,
                ApproveId = s.ApproveId,
                UserId = s.UserId,
                StoreId =  s.StoreId,
                SerialNumber = s.SerialNumber,
                RevewId = s.RevewId,
                Client_Supplier_Id =  s.Client_Supplier_Id,
                CreatedBy = s.CreatedBy,
                CreatedDate = DateTime.Now,
                Date = s.Date.ToLocalTime(),
            };
            Insert(newModel);
            return newModel;
        }
        public AdditionVoucher Edit(AdditionVoucherVM s)
        {
            AdditionVoucher editedmodel = dbSet.Where(x => x.ID == s.ID).FirstOrDefault();
            editedmodel.UserType = s.UserType;
            editedmodel.Note = s.Note;
            editedmodel.ShowNotesFlag = s.ShowNotesFlag;
            editedmodel.ApproveId = s.ApproveId;
            editedmodel.UserId = s.UserId;
             editedmodel.StoreId = s.StoreId;
            editedmodel.SerialNumber = s.SerialNumber;
            editedmodel.RevewId = s.RevewId;
            editedmodel.Client_Supplier_Id = s.Client_Supplier_Id;
            editedmodel.Date = s.Date.ToLocalTime();
           
            Update(editedmodel);
            return editedmodel;
        }
     
        public AdditionVoucher ConvertTempToAdditionVoucher(AdditionVoucherVM Bill)
        {
            AdditionVoucher addition = dbSet.FirstOrDefault(x => x.BillSerialNumber == Bill.BillSerialNumber && x.StoreId == Bill.StoreId && x.IsDeleted == false);
            if (addition != null)
                return null;
             addition = new AdditionVoucher
            {
                 SerialNumber = GenerateSerial(0),
                 BillSerialNumber = Bill.BillSerialNumber.Value,
                 BillType = Bill.BillType,
                 UserType = Bill.UserType,
                 IsDeleted = Bill.IsDeleted,
                 Note = Bill.Note,
                 ShowNotesFlag = Bill.ShowNotesFlag,
                 ApproveId = Bill.ApproveId,
                 UserId = Bill.UserId,
                 StoreId = Bill.StoreId,
                 RevewId = Bill.RevewId,
                 Client_Supplier_Id = Bill.Client_Supplier_Id,
                 CreatedBy = Bill.CreatedBy,
                 CreatedDate = DateTime.Now,
                 Date = Bill.Date,
             };
            Insert(addition);
            return addition;
        }
        public void Delete(int id)
        {
            AdditionVoucher edited = dbSet.FirstOrDefault(p => p.ID == id);
            Delete(edited);
        }
    }
}
