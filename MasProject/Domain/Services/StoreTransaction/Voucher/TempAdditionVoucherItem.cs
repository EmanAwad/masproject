﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.StoreTransaction.Voucher;
using MasProject.Domain.ViewModel.StoreTransaction.Voucher;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using MasProject.Domain.ViewModel.StoreTransaction.Purchase;
using MasProject.Domain.ViewModel.StoreTransaction.Conversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasProject.Domain.Services.StoreTransaction.Voucher
{
    public class TempAdditionVoucherItemService : Repository<TempAdditionVoucherItem>
    {
        DBContext _Context;

        public TempAdditionVoucherItemService(DBContext context) : base(context)
        {
            _Context = context;
        }
        public List<AdditionVoucherItemVM> GetBySerial(int Serial, int StoreId)
        {
            return dbSet.Where(x => x.SerialNumber == Serial && x.IsDeleted == false).Select(s => new AdditionVoucherItemVM
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                ItemId = (int)s.ItemId,
                Box_ParCode = s.Item.National_ParCode,
                SheilfNo = s.SheilfNo ?? _Context.Item_Store.FirstOrDefault(x => x.ItemId == s.ItemId.Value && x.StoreId == StoreId).SheilfNo,
                Quantity = s.Quantity,
                ItemName = s.Item.Name,
                BoxCode = s.BoxCode ?? _Context.Items.FirstOrDefault(x => x.ID == s.ItemId.Value).Box_ParCode,
            }).ToList();
        }
        public TempAdditionVoucherItem Add(TempAdditionVoucherItemVM s)
        {
            var newModel = new TempAdditionVoucherItem
            {
                //ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                ItemId = (int)s.ItemId,
                BoxCode = s.BoxCode,
                Quantity = s.Quantity,
                SheilfNo = s.SheilfNo,

            };
            Insert(newModel);
            return newModel;
        }
        public TempAdditionVoucherItem ConvertReturnItemToTempItem(SalesReturnsItemsVM item)
        {
            TempAdditionVoucherItem newModel = new TempAdditionVoucherItem
            {
                IsDeleted = item.IsDeleted,
                SerialNumber = (int)item.SerialNumber,
                ItemId = (int)item.ItemId,
                Quantity = item.Quantity,
            };
            Insert(newModel);
            return newModel;
        }
        public TempAdditionVoucherItem Edit(TempAdditionVoucherItemVM s)
        {
            var newModel = new TempAdditionVoucherItem
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                ItemId = (int)s.ItemId,
                BoxCode = s.BoxCode,
                Quantity = s.Quantity,
                SheilfNo = s.SheilfNo,
            };
            Update(newModel);
            return newModel;
        }
        public TempAdditionVoucherItem ConvertSalesReturnItemToTempItem(SalesReturnsItemsVM item)
        {
            TempAdditionVoucherItem newModel = new TempAdditionVoucherItem
            {
                IsDeleted = item.IsDeleted,
                SerialNumber = (int)item.SerialNumber,
                ItemId = (int)item.ItemId,
                Quantity = item.Quantity,
            };
            Insert(newModel);
            return newModel;
        }
        public TempAdditionVoucherItem ConvertPurchaseItemToTempItem(PurchaseItemVM item)
        {
            TempAdditionVoucherItem newModel = new TempAdditionVoucherItem
            {
                IsDeleted = item.IsDeleted,
                SerialNumber = (int)item.SerialNumber,
                ItemId = (int)item.ItemId,
                Quantity = item.Quantity,
            };
            Insert(newModel);
            return newModel;
        }
        public TempAdditionVoucherItem ConversionItemToTempItem(ItemConversion_ItemVM item)
        {
            TempAdditionVoucherItem newModel = new TempAdditionVoucherItem
            {
                IsDeleted = item.IsDeleted,
                SerialNumber = (int)item.ConversionRecordID,
                ItemId = (int)item.ItemIdTo,
                Quantity = item.QuantityTo,
            };
            Insert(newModel);
            return newModel;
        }

        public void Delete(int id)
        {
            List<TempAdditionVoucherItem> DeleteList = dbSet.Where(p => p.SerialNumber == id).ToList();
            foreach (var item in DeleteList)
            {
                Delete(item);
            }
        }
        public void DeletePhysical(int id)
        {
            List<TempAdditionVoucherItem> DeleteList = dbSet.Where(p => p.SerialNumber == id).ToList();
            foreach (var item in DeleteList)
            {
                item.IsDeleted = true;
                Remove(item);
            }
        }
    }
}
