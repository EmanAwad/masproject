﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.StoreTransaction.Voucher;
using MasProject.Domain.ViewModel.StoreTransaction.Voucher;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using MasProject.Domain.ViewModel.StoreTransaction.Purchase;
using MasProject.Domain.ViewModel.StoreTransaction.Conversion;
using MasProject.Domain.ViewModel.StoreTransaction.ExecutionWithDrawals;
using MasProject.Domain.ViewModel.StoreTransaction.Transfer;
using System;
using System.Collections.Generic;
using System.Linq;
namespace MasProject.Domain.Services.StoreTransaction.Voucher
{
    public class TempPaymentVoucherItemService : Repository<TempPaymentVoucherItem>
    {
        DBContext _Context;
        public TempPaymentVoucherItemService(DBContext context) : base(context)
        {
            _Context = context;
        }
        public List<PaymentVoucherItemVM> GetBySerial(int Serial , int StoreId)
        {
            return dbSet.Where(x => x.SerialNumber == Serial  && x.IsDeleted == false).Select(s => new PaymentVoucherItemVM
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                ItemId = (int)s.ItemId,
                Box_ParCode = s.Item.National_ParCode,
                SheilfNo = s.SheilfNo ?? _Context.Item_Store.FirstOrDefault(x => x.ItemId == s.ItemId.Value && x.StoreId == StoreId).SheilfNo,
                Quantity = s.Quantity,
                ItemName = s.Item.Name,
                BoxCode = s.BoxCode ?? _Context.Items.FirstOrDefault(x => x.ID == s.ItemId.Value).Box_ParCode,
            }).ToList();
        }
        public TempPaymentVoucherItem Add(TempPaymentVoucherItemVM s)
        {
            var newModel = new TempPaymentVoucherItem
            {
                // ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                ItemId = (int)s.ItemId,
                SheilfNo = s.SheilfNo,
                Quantity = s.Quantity,
                BoxCode = s.BoxCode,
            };
            Insert(newModel);
            return newModel;
        }
        public TempPaymentVoucherItem Edit(TempPaymentVoucherItemVM s)
        {
            var newModel = new TempPaymentVoucherItem
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                ItemId = (int)s.ItemId,
                SheilfNo = s.SheilfNo,
                Quantity = s.Quantity,
                BoxCode = s.BoxCode,
            };
            Update(newModel);
            return newModel;
        }
        public TempPaymentVoucherItem ConvertSalesItemToTempItem(SalesItemsVM item)
        {
            TempPaymentVoucherItem newModel = new TempPaymentVoucherItem
            {
                IsDeleted = item.IsDeleted,
                SerialNumber = (int)item.SerialNumber,
                ItemId = (int)item.ItemId,
                Quantity = item.Quantity,
            };
            Insert(newModel);
            return newModel;
        }
        public TempPaymentVoucherItem ConvertExecutiveItemToTempItem(ExecutiveItemsVM item)
        {
            TempPaymentVoucherItem newModel = new TempPaymentVoucherItem
            {
                IsDeleted = item.IsDeleted,
                SerialNumber = (int)item.SerialNumber,
                ItemId = (int)item.ItemId,
                Quantity = item.Quantity,
            };
            Insert(newModel);
            return newModel;
        }
        public TempPaymentVoucherItem ConvertMantienceItemToTempItem(MantienceItemsVM item)
        {
            TempPaymentVoucherItem newModel = new TempPaymentVoucherItem
            {
                IsDeleted = item.IsDeleted,
                SerialNumber = (int)item.SerialNumber,
                ItemId = (int)item.ItemId,
                Quantity = item.Quantity,
            };
            Insert(newModel);
            return newModel;
        }
        public TempPaymentVoucherItem ConvertPurchasesReturnItemToTempItem(PurchasesReturns_ItemVM item)
        {
            TempPaymentVoucherItem newModel = new TempPaymentVoucherItem
            {
                IsDeleted = item.IsDeleted,
                SerialNumber = (int)item.SerialNumber,
                ItemId = (int)item.ItemId,
                Quantity = item.Quantity,
            };
            Insert(newModel);
            return newModel;
        }
        public TempPaymentVoucherItem ConversionItemFromTempItem(ItemConversion_ItemVM item)
        {
            TempPaymentVoucherItem newModel = new TempPaymentVoucherItem
            {
                IsDeleted = item.IsDeleted,
                SerialNumber = (int)item.ConversionRecordID,
                ItemId = (int)item.ItemIdFrom,
                Quantity = item.QuantityFrom,
            };
            Insert(newModel);
            return newModel;
        }
        public TempPaymentVoucherItem ConvertExecutionWithdrawlItemToTempItem(ExecutionWithDrawals_ItemsVM item)
        {
            TempPaymentVoucherItem newModel = new TempPaymentVoucherItem
            {
                IsDeleted = item.IsDeleted,
                SerialNumber = (int)item.SerialNumber,
                ItemId = (int)item.ItemId,
                Quantity = item.Quantity,
            };
            Insert(newModel);
            return newModel;
        }
        public TempPaymentVoucherItem ConvertTransOrderItemToTempItem(TransferOrderDetailsVM item)
        {
            TempPaymentVoucherItem newModel = new TempPaymentVoucherItem
            {
                IsDeleted = item.IsDeleted,
                SerialNumber = (int)item.SerialNumber,
                ItemId = (int)item.ItemId,
                Quantity = (int)item.Quantity,
            };
            Insert(newModel);
            return newModel;
        }

        public void Delete(int id)
        {
            List<TempPaymentVoucherItem> DeleteList = dbSet.Where(p => p.SerialNumber == id).ToList();
            foreach (var item in DeleteList)
            {
                Delete(item);
            }
        }
        public void DeletePhysical(int id)
        {
            List<TempPaymentVoucherItem> DeleteList = dbSet.Where(p => p.SerialNumber == id).ToList();
            foreach (var item in DeleteList)
            {
                item.IsDeleted = true;
                Remove(item);
            }
        }
    }
}