﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.StoreTransaction.Voucher;
using MasProject.Domain.ViewModel.StoreTransaction.Voucher;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using System;
using System.Collections.Generic;
using System.Linq;
namespace MasProject.Domain.Services.StoreTransaction.Voucher
{
    public class PaymentVoucherService : Repository<PaymentVoucher>
    {
        DBContext _Context;

        public PaymentVoucherService(DBContext context) : base(context)
        {
            _Context = context;

        }
        public IEnumerable<PaymentVoucherVM> GetAllByUserType(int UserType)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.Client_Supplier_Id == UserType).Select(s => new PaymentVoucherVM
            {
                ID = s.ID,
                SerialNumber = s.SerialNumber,
                Name = s.UserType == 1 ? _Context.Client.Where(x => x.ID == s.Client_Supplier_Id).FirstOrDefault().Name : s.UserType == 2 ? _Context.Supplier.Where(x => x.ID == s.Client_Supplier_Id).FirstOrDefault().Name : _Context.Employee.Where(x => x.ID == s.Client_Supplier_Id).FirstOrDefault().Name,
                Client_Supplier_Id = s.Client_Supplier_Id,
                BillSerialNumber = s.BillSerialNumber,
                BillType = s.BillType,
                Date = s.Date,
                CreatedDate = s.CreatedDate,
                Type = 1,
            }).OrderByDescending(s => s.ID);
        }
        public IEnumerable<PaymentVoucherVM> GetAllByStore(int StoreId)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.StoreId == StoreId).Select(s => new PaymentVoucherVM
            {
                ID = s.ID,
                SerialNumber = s.SerialNumber,
                Name = s.UserType == 1 ? _Context.Client.Where(x => x.ID == s.Client_Supplier_Id).FirstOrDefault().Name : s.UserType == 2 ? _Context.Supplier.Where(x => x.ID == s.Client_Supplier_Id).FirstOrDefault().Name : _Context.Employee.Where(x => x.ID == s.Client_Supplier_Id).FirstOrDefault().Name,
                Client_Supplier_Id = s.Client_Supplier_Id,
                BillSerialNumber = s.BillSerialNumber,
                BillType = s.BillType,
                Date = s.Date,
                CreatedDate = s.CreatedDate,
                Type = 1,
            }).OrderByDescending(s => s.ID);
        }
        public IEnumerable<PaymentVoucherVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new PaymentVoucherVM
            {
                ID = s.ID,
                SerialNumber=s.SerialNumber,
                Name = s.UserType == 1 ? _Context.Client.Where(x => x.ID == s.Client_Supplier_Id).FirstOrDefault().Name : s.UserType == 2 ? _Context.Supplier.Where(x => x.ID == s.Client_Supplier_Id).FirstOrDefault().Name : _Context.Employee.Where(x => x.ID == s.Client_Supplier_Id).FirstOrDefault().Name,
                Client_Supplier_Id =  s.Client_Supplier_Id,
                BillSerialNumber=s.BillSerialNumber,
                BillType=s.BillType,
                Date= s.Date,
                CreatedDate=s.CreatedDate,
                Type = 1,
            }).OrderByDescending(s => s.ID);
        }
        public PaymentVoucherVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new PaymentVoucherVM
            {
                ID = s.ID,
                ClientName = s.UserType == 1 ? _Context.Client.Where(x => x.ID == s.Client_Supplier_Id).FirstOrDefault().Name : s.UserType == 2 ? _Context.Supplier.Where(x => x.ID == s.Client_Supplier_Id).FirstOrDefault().Name : _Context.Employee.Where(x => x.ID == s.Client_Supplier_Id).FirstOrDefault().Name,
                BillSerialNumber = s.BillSerialNumber,
                Note = s.Note,
                ShowNotesFlag = s.ShowNotesFlag,
                ApproveId = s.ApproveId,
                UserId = s.UserId,
                StoreId =  s.StoreId,
                SerialNumber = s.SerialNumber,
                RevewId = s.RevewId,
                ModifiedDate = s.ModifiedDate,
                ModifiedBy = s.ModifiedBy,
                Client_Supplier_Id =  s.Client_Supplier_Id,
                CreatedBy = s.CreatedBy,
                CreatedDate = s.CreatedDate,
                Date = s.Date.Date,
            }).FirstOrDefault();
        }
        public PaymentVoucher Add(PaymentVoucherVM s)
        {
            var newModel = new PaymentVoucher
            {
                ID = s.ID,
                UserType=s.UserType,
                IsDeleted = s.IsDeleted,
                Note = s.Note,
                ShowNotesFlag = s.ShowNotesFlag,
                ApproveId = s.ApproveId,
                UserId = s.UserId,
                StoreId =  s.StoreId,
                SerialNumber = s.SerialNumber,
                RevewId = s.RevewId,
                Client_Supplier_Id = s.Client_Supplier_Id,
                CreatedBy = s.CreatedBy,
                CreatedDate = DateTime.Now,
                Date = s.Date.ToLocalTime(),
                BillSerialNumber = s.BillSerialNumber,
                BillType = s.BillType,
            };
            Insert(newModel);
            return newModel;
        }
        public PaymentVoucher Edit(PaymentVoucherVM s)
        {
            var newModel = new PaymentVoucher
            {
                ID = s.ID,
                UserType=s.UserType,
                IsDeleted = s.IsDeleted,
                Note = s.Note,
                ShowNotesFlag = s.ShowNotesFlag,
                ApproveId = s.ApproveId,
                UserId = s.UserId,
                StoreId =  s.StoreId,
                SerialNumber = s.SerialNumber,
                RevewId = s.RevewId,
                ModifiedDate = DateTime.Now,
                ModifiedBy = s.ModifiedBy,
                Client_Supplier_Id =  s.Client_Supplier_Id,
                Date = s.Date.ToLocalTime(),
                BillSerialNumber = s.BillSerialNumber,
                //BillType = s.BillType,
            };
            Update(newModel);
            return newModel;
        }
      
        public PaymentVoucher ConvertTempToPaymentVoucher(PaymentVoucherVM Bill)
        {
            //temp 24-01-2021
            Bill.BillType = null;
            PaymentVoucher payment = dbSet.FirstOrDefault(x => x.BillSerialNumber == Bill.BillSerialNumber && x.StoreId == Bill.StoreId && x.IsDeleted == false);
            if (payment != null)
                return null;
            payment = new PaymentVoucher
            {
                SerialNumber =Bill.SerialNumber,// GenerateSerial(0),
                BillSerialNumber = Bill.BillSerialNumber.Value,
                BillType = Bill.BillType,
                UserType = Bill.UserType,
                IsDeleted = Bill.IsDeleted,
                Note = Bill.Note,
                ShowNotesFlag = Bill.ShowNotesFlag,
                ApproveId = Bill.ApproveId,
                UserId = Bill.UserId,
                StoreId = Bill.StoreId,
                RevewId = Bill.RevewId,
                Client_Supplier_Id = Bill.Client_Supplier_Id,
                CreatedBy = Bill.CreatedBy,
                CreatedDate = DateTime.Now,
                Date = Bill.Date,
            };
            Insert(payment);
            return payment;
        }
        public void Delete(int id)
        {
            PaymentVoucher edited = dbSet.FirstOrDefault(p => p.ID == id);
            Delete(edited);
        }
    }
}
