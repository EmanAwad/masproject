﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.StoreTransaction.Voucher;
using MasProject.Domain.ViewModel.StoreTransaction.Voucher;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasProject.Domain.Services.StoreTransaction.Voucher
{
    public class PaymentVoucherItemService : Repository<PaymentVoucherItem>
    {
        DBContext _Context;
        public PaymentVoucherItemService(DBContext context) : base(context)
        {
            _Context = context;

        }
        public List<PaymentVoucherItemVM> GetBySerial(int Serial , int StoreId)
        {
            return dbSet.Where(x => x.SerialNumber == Serial && x.IsDeleted == false).Select(s => new PaymentVoucherItemVM
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                ItemImg = s.Item.ItemImg,
                SerialNumber = (int)s.SerialNumber,
                ItemId = (int)s.ItemId,
                Box_ParCode=s.Item.National_ParCode,
                SheilfNo = s.SheilfNo ?? _Context.Item_Store.FirstOrDefault(x => x.ItemId == s.ItemId.Value && x.StoreId==StoreId).SheilfNo,
                Quantity = s.Quantity,
                ItemName = s.Item.Name,
                BoxCode = s.BoxCode ?? _Context.Items.FirstOrDefault(x => x.ID == s.ItemId.Value).Box_ParCode,
            }).ToList();
        }
        public List<PaymentVoucherItemVM> GetBySerial(int Serial)
        {
            return dbSet.Where(x => x.SerialNumber == Serial && x.IsDeleted == false).Select(s => new PaymentVoucherItemVM
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                ItemImg = s.Item.ItemImg,
                SerialNumber = (int)s.SerialNumber,
                ItemId = (int)s.ItemId,
                Box_ParCode = s.Item.National_ParCode,
                SheilfNo = s.SheilfNo ?? _Context.Item_Store.FirstOrDefault(x => x.ItemId == s.ItemId.Value ).SheilfNo,
                Quantity = s.Quantity,
                BoxCode = s.BoxCode ?? _Context.Items.FirstOrDefault(x => x.ID == s.ItemId.Value).Box_ParCode,
            }).ToList();
        }
        public PaymentVoucherItem Add(PaymentVoucherItemVM s)
        {
            var newModel = new PaymentVoucherItem
            {
               // ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                ItemId = (int)s.ItemId,
                SheilfNo = s.SheilfNo,
                Quantity = s.Quantity,
                BoxCode = s.BoxCode,
            };
            Insert(newModel);
            return newModel;
        }
        public PaymentVoucherItem Edit(PaymentVoucherItemVM s)
        {
            var newModel = new PaymentVoucherItem
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                ItemId = (int)s.ItemId,
                SheilfNo = s.SheilfNo,
                Quantity = s.Quantity,
                BoxCode = s.BoxCode,
            };
            Update(newModel);
            return newModel;
        }
        public PaymentVoucherItem ConvertTempItemToPaymentItem(PaymentVoucherItemVM item)
        {
            PaymentVoucherItem newModel = new PaymentVoucherItem
            {
                IsDeleted = item.IsDeleted,
                SerialNumber = (int)item.SerialNumber,
                ItemId = (int)item.ItemId,
                Quantity = item.Quantity,
                SheilfNo = item.SheilfNo,
                BoxCode = item.BoxCode,
            };
            Insert(newModel);
            return newModel;
        }
        public void Delete(int id)
        {
            List<PaymentVoucherItem> DeleteList = dbSet.Where(p => p.SerialNumber == id).ToList();
            foreach (var item in DeleteList)
            {
                Delete(item);
            }
        }
        public void DeletePhysical(int id)
        {
            List<PaymentVoucherItem> DeleteList = dbSet.Where(p => p.SerialNumber == id).ToList();
            foreach (var item in DeleteList)
            {
                item.IsDeleted = true;
                Remove(item);
            }
        }
    }
}