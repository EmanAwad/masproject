﻿using System;
using System.Collections.Generic;
using MasProject.Data.DataAccess;
using MasProject.Data.Models.StoreTransaction.Voucher;
using MasProject.Domain.ViewModel.StoreTransaction.Voucher;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using MasProject.Domain.ViewModel.StoreTransaction.Purchase;
using MasProject.Domain.ViewModel.StoreTransaction.Transfer;
using System.Linq;
using MasProject.Domain.ViewModel.StoreTransaction.Conversion;
using MasProject.Domain.ViewModel.StoreTransaction.ExecutionWithDrawals;
namespace MasProject.Domain.Services.StoreTransaction.Voucher
{
    public class TempPaymentVoucherService : Repository<TempPaymentVoucher>
    {
        DBContext _Context;

        public TempPaymentVoucherService(DBContext context) : base(context)
        {
            _Context = context;

        }
        public IEnumerable<PaymentVoucherVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new PaymentVoucherVM
            {
                ID = s.ID,
                StoreId=s.StoreId,
                SerialNumber =(int) s.SerialNumber,
                Name = s.UserType == 1 ? _Context.Client.Where(x => x.ID == s.Client_Supplier_Id).FirstOrDefault().Name : s.UserType == 2 ? _Context.Supplier.Where(x => x.ID == s.Client_Supplier_Id).FirstOrDefault().Name : _Context.Employee.Where(x => x.ID == s.Client_Supplier_Id).FirstOrDefault().Name,
                Client_Supplier_Id = s.Client_Supplier_Id,
                BillSerialNumber = s.BillSerialNumber,
                BillType = s.BillType,
                Date = s.Date,
                CreatedDate=s.CreatedDate,
                Type = 2,
            }).OrderByDescending(s => s.ID);
        }
        public PaymentVoucherVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new PaymentVoucherVM
            {
                ID = s.ID,
                ClientName = s.UserType == 1 ? _Context.Client.Where(x => x.ID == s.Client_Supplier_Id).FirstOrDefault().Name : s.UserType == 2 ? _Context.Supplier.Where(x => x.ID == s.Client_Supplier_Id).FirstOrDefault().Name : _Context.Employee.Where(x => x.ID == s.Client_Supplier_Id).FirstOrDefault().Name,
                BillSerialNumber = s.BillSerialNumber,
                BillType=s.BillType,
                Note = s.Note,
                ShowNotesFlag = s.ShowNotesFlag,
                ApproveId = s.ApproveId,
                UserId = s.UserId,
                StoreId = s.StoreId,
                SerialNumber = s.SerialNumber,
                RevewId = s.RevewId,
                ModifiedDate = s.ModifiedDate,
                ModifiedBy = s.ModifiedBy,
                Client_Supplier_Id = s.Client_Supplier_Id,
                CreatedBy = s.CreatedBy,
                CreatedDate = s.CreatedDate,
                Date = s.Date.Date,
            }).FirstOrDefault();
        }
        public TempPaymentVoucher Add(TempPaymentVoucherVM s)
        {
            var newModel = new TempPaymentVoucher
            {
                ID = s.ID,
                UserType = s.UserType,
                IsDeleted = s.IsDeleted,
                Note = s.Note,
                ShowNotesFlag = s.ShowNotesFlag,
                ApproveId = s.ApproveId,
                UserId = s.UserId,
                StoreId = s.StoreId,
                SerialNumber = s.SerialNumber,
                RevewId = s.RevewId,
                Client_Supplier_Id = s.Client_Supplier_Id,
                CreatedBy = s.CreatedBy,
                CreatedDate = DateTime.Now,
                Date = s.Date.ToLocalTime(),
                BillSerialNumber = s.BillSerialNumber,
                BillType = s.BillType,
            };
            Insert(newModel);
            return newModel;
        }
        public TempPaymentVoucher Edit(TempPaymentVoucherVM s)
        {
            var newModel = new TempPaymentVoucher
            {
                ID = s.ID,
                UserType = s.UserType,
                IsDeleted = s.IsDeleted,
                Note = s.Note,
                ShowNotesFlag = s.ShowNotesFlag,
                ApproveId = s.ApproveId,
                UserId = s.UserId,
                StoreId = s.StoreId,
                SerialNumber = s.SerialNumber,
                RevewId = s.RevewId,
                ModifiedDate = DateTime.Now,
                ModifiedBy = s.ModifiedBy,
                Client_Supplier_Id = s.Client_Supplier_Id,
                Date = s.Date.ToLocalTime(),
                BillSerialNumber = s.BillSerialNumber,
                BillType = s.BillType,
            };
            Update(newModel);
            return newModel;
        }
        public int GenerateSerial()
        {
            int serial = dbSet.FirstOrDefault(x => x.IsDeleted == false) != null ? dbSet.Max(c => c.SerialNumber).Value + 1 : 1;
            return serial;
        }
        public TempPaymentVoucher ConvertSalesToTemp(SalesBillsVM Bill)
        {
            TempPaymentVoucher payment = new TempPaymentVoucher
            {
                SerialNumber = GenerateSerial(),
                BillSerialNumber = Bill.SerialNumber,
                BillType = "فاتورة مبيعات",
                UserType = 1,
                IsDeleted = Bill.IsDeleted,
                Note = Bill.Note,
                ShowNotesFlag = Bill.ShowNotesFlag,
                ApproveId = Bill.ApproveId,
                UserId = Bill.UserId,
                StoreId = Bill.StoreId,
                RevewId = Bill.RevewId,
                Client_Supplier_Id = Bill.ClientId,
                CreatedBy = Bill.CreatedBy,
                CreatedDate = DateTime.Now,
                Date = Bill.Date,
            };
            Insert(payment);
            return payment;
        }
        public TempPaymentVoucher ConvertTransOrderToTemp(TransferOrderDetailsVM Trans)
        {
            TempPaymentVoucher payment = new TempPaymentVoucher
            {
                SerialNumber = GenerateSerial(),
                BillSerialNumber = Trans.SerialNumber,
                BillType = "أمر التحويل",
                UserType = 1,
                IsDeleted = Trans.IsDeleted,
                Note = Trans.Note,
                ShowNotesFlag = false,
                ApproveId = Trans.ApproveId,
                UserId = Trans.UserId,
                StoreId = Trans.StoreId,
                RevewId = Trans.RevewId,
                Client_Supplier_Id = 0,
                CreatedBy = Trans.CreatedBy,
                CreatedDate = DateTime.Now,
                Date = Trans.Date,
            };
            Insert(payment);
            return payment;
        }
        public TempPaymentVoucher ConvertExecutiveToTemp(ExecutiveBillsVM Bill)
        {
            TempPaymentVoucher payment = new TempPaymentVoucher
            {
                SerialNumber = GenerateSerial(),
                BillSerialNumber = Bill.SerialNumber,
                BillType = "فاتورة تنفيذ",
                UserType = 1,
                IsDeleted = Bill.IsDeleted,
                Note = Bill.Note,
                ShowNotesFlag = Bill.ShowNotesFlag,
                ApproveId = Bill.ApproveId,
                UserId = Bill.UserId,
                StoreId = Bill.StoreId,
                RevewId = Bill.RevewId,
                Client_Supplier_Id = Bill.ClientId,
                CreatedBy = Bill.CreatedBy,
                CreatedDate = DateTime.Now,
                Date = Bill.Date,
            };
            Insert(payment);
            return payment;
        }
        public TempPaymentVoucher ConvertMantienceToTemp(MantienceBillsVM Bill)
        {
            TempPaymentVoucher payment = new TempPaymentVoucher
            {
                SerialNumber = GenerateSerial(),
                BillSerialNumber = Bill.SerialNumber,
                BillType = "فاتورة صيانة",
                UserType = 1,
                IsDeleted = Bill.IsDeleted,
                Note = Bill.Note,
                ShowNotesFlag = Bill.ShowNotesFlag,
                ApproveId = Bill.ApproveId,
                UserId = Bill.UserId,
                StoreId = Bill.StoreId,
                RevewId = Bill.RevewId,
                Client_Supplier_Id = Bill.ClientId,
                CreatedBy = Bill.CreatedBy,
                CreatedDate = DateTime.Now,
                Date = Bill.Date,
            };
            Insert(payment);
            return payment;
        }
        public TempPaymentVoucher ConvertPurchasereturnToTemp(PurchasesReturns_BillVM Bill)
        {
            TempPaymentVoucher payment = new TempPaymentVoucher
            {
                SerialNumber = GenerateSerial(),
                BillSerialNumber = Bill.SerialNumber,
                BillType = "مردودات مشتريات",
                UserType = 2,
                IsDeleted = Bill.IsDeleted,
                Note = Bill.Note,
                ShowNotesFlag = Bill.ShowNotesFlag,
                ApproveId = Bill.ApproveId,
                UserId = Bill.UserId,
                StoreId = Bill.StoreId,
                RevewId = Bill.RevewId,
                Client_Supplier_Id = Bill.SupplierId,
                CreatedBy = Bill.CreatedBy,
                CreatedDate = DateTime.Now,
                Date = Bill.Date,
            };
            Insert(payment);
            return payment;
        }
        public TempPaymentVoucher ConversionItemFromTemp(ItemConversion_BillVM Bill)
        {
            TempPaymentVoucher payment = new TempPaymentVoucher
            {
                SerialNumber = GenerateSerial(),
                BillSerialNumber = Bill.ConversionRecordID,
                BillType = "التحويل من صنف لصنف",
                UserType = 3,
                IsDeleted = Bill.IsDeleted,
                Note = Bill.Note,
                ShowNotesFlag = false,
                ApproveId = 0,
                UserId = 0,
                StoreId = Bill.StoreId,
                RevewId = 0,
                Client_Supplier_Id = Bill.EmployeeId.Value,
                CreatedBy = Bill.CreatedBy,
                CreatedDate = DateTime.Now,
                Date = Bill.Date,
            };
            Insert(payment);
            return payment;
        }
        public TempPaymentVoucher ConvertExecutionWithdrawlToTemp(ExecutionithDrawals_BillVM Bill)
        {
            TempPaymentVoucher payment = new TempPaymentVoucher
            {
                SerialNumber = GenerateSerial(),
                BillSerialNumber = Bill.SerialNumber,
                BillType = "مسحوبات التنفيذ",
                UserType = 3,
                IsDeleted = Bill.IsDeleted,
                Note = Bill.Note,
                ShowNotesFlag = Bill.ShowNotesFlag,
                ApproveId = Bill.ApproveId,
                UserId = Bill.UserId,
                StoreId = Bill.StoreId,
                RevewId = Bill.RevewId,
                Client_Supplier_Id = Bill.UserId,
                CreatedBy = Bill.CreatedBy,
                CreatedDate = DateTime.Now,
                Date = Bill.Date,
            };
            Insert(payment);
            return payment;
        }
        public void Delete(int id)
        {
            TempPaymentVoucher edited = dbSet.FirstOrDefault(p => p.ID == id);
            Delete(edited);
        }
        public void DeletePhysical(int id)
        {
            TempPaymentVoucher edited = dbSet.FirstOrDefault(p => p.ID == id);
            Remove(edited);
        }
    }
}