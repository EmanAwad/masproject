﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.StoreTransaction.Voucher;
using MasProject.Domain.ViewModel.StoreTransaction.Voucher;
using MasProject.Domain.ViewModel.StoreTransaction.Sales;
using MasProject.Domain.ViewModel.StoreTransaction.Purchase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MasProject.Domain.ViewModel.StoreTransaction.Conversion;

namespace MasProject.Domain.Services.StoreTransaction.Voucher
{
    public class TempAdditionVoucherService : Repository<TempAdditionVoucher>
    {
        DBContext _Context;

        public TempAdditionVoucherService(DBContext context) : base(context)
        {
            _Context = context;

        }
        public IEnumerable<AdditionVoucherVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new AdditionVoucherVM
            {
                ID = s.ID,
                SerialNumber = s.SerialNumber,
                Name = s.UserType == 1 ? _Context.Client.Where(x => x.ID == s.Client_Supplier_Id).FirstOrDefault().Name : s.UserType == 2 ? _Context.Supplier.Where(x => x.ID == s.Client_Supplier_Id).FirstOrDefault().Name : _Context.Employee.Where(x => x.ID == s.Client_Supplier_Id).FirstOrDefault().Name,
                BillSerialNumber = s.BillSerialNumber,
                BillType = s.BillType,
                Client_Supplier_Id = s.Client_Supplier_Id,
                CreatedBy = s.CreatedBy,
                CreatedDate = s.CreatedDate,
                Date = s.Date,
                Type=2,
            }).OrderByDescending(s => s.ID);
        }
        public AdditionVoucherVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new AdditionVoucherVM
            {
                ID = s.ID,
                ClientName = s.UserType == 1 ? _Context.Client.Where(x => x.ID == s.Client_Supplier_Id).FirstOrDefault().Name : s.UserType == 2 ? _Context.Supplier.Where(x => x.ID == s.Client_Supplier_Id).FirstOrDefault().Name : _Context.Employee.Where(x => x.ID == s.Client_Supplier_Id).FirstOrDefault().Name,
                IsDeleted = s.IsDeleted,
                Note = s.Note,
                ShowNotesFlag = s.ShowNotesFlag,
                ApproveId = s.ApproveId,
                UserId = s.UserId,
                StoreId = s.StoreId,
                BillSerialNumber=s.BillSerialNumber,
                SerialNumber = s.SerialNumber,
                RevewId = s.RevewId,
                ModifiedDate = s.ModifiedDate,
                ModifiedBy = s.ModifiedBy,
                Client_Supplier_Id = s.Client_Supplier_Id,
                CreatedBy = s.CreatedBy,
                CreatedDate = s.CreatedDate,
                Date = s.Date.Date,
            }).FirstOrDefault();
        }
        public TempAdditionVoucher Add(TempAdditionVoucherVM s)
        {
            var newModel = new TempAdditionVoucher
            {
                ID = s.ID,
                UserType = s.UserType,
                BillSerialNumber = s.BillSerialNumber,
                BillType = s.BillType,
                IsDeleted = s.IsDeleted,
                Note = s.Note,
                ShowNotesFlag = s.ShowNotesFlag,
                ApproveId = s.ApproveId,
                UserId = s.UserId,
                StoreId = s.StoreId,
                SerialNumber = s.SerialNumber,
                RevewId = s.RevewId,
                Client_Supplier_Id = s.Client_Supplier_Id,
                CreatedBy = s.CreatedBy,
                CreatedDate = DateTime.Now,
                Date = s.Date.ToLocalTime(),
            };
            Insert(newModel);
            return newModel;
        }
        public TempAdditionVoucher Edit(TempAdditionVoucherVM s)
        {
            var newModel = new TempAdditionVoucher
            {
                ID = s.ID,
                UserType = s.UserType,
                IsDeleted = s.IsDeleted,
                Note = s.Note,
                ShowNotesFlag = s.ShowNotesFlag,
                ApproveId = s.ApproveId,
                UserId = s.UserId,
                StoreId = s.StoreId,
                SerialNumber = s.SerialNumber,
                RevewId = s.RevewId,
                ModifiedDate = DateTime.Now,
                ModifiedBy = s.ModifiedBy,
                Client_Supplier_Id = s.Client_Supplier_Id,
                Date = s.Date.ToLocalTime(),
            };
            Update(newModel);
            return newModel;
        }
        public int GenerateSerial()
        {
            int serial = dbSet.FirstOrDefault(x => x.IsDeleted == false) != null ? dbSet.Max(c => c.SerialNumber).Value + 1 : 1;
            return serial;
        }
        public TempAdditionVoucher ConvertSalesReturnsToTemp(SalesReturnsVM Bill)
        {
            TempAdditionVoucher addition = new TempAdditionVoucher
            {
                SerialNumber = GenerateSerial(),
                BillSerialNumber = Bill.SerialNumber,
                BillType = "مردودات مبيعات ",
                UserType = 1,
                IsDeleted = Bill.IsDeleted,
                Note = Bill.Note,
                ShowNotesFlag = Bill.ShowNotesFlag,
                ApproveId = Bill.ApproveId,
                UserId = Bill.UserId,
                StoreId = Bill.StoreId,
                RevewId = Bill.RevewId,
                Client_Supplier_Id = Bill.ClientId,
                CreatedBy = Bill.CreatedBy,
                CreatedDate = DateTime.Now,
                Date = Bill.Date,
            };
            Insert(addition);
            return addition;
        }
        public TempAdditionVoucher ConvertPurchaseToTemp(PurchaseBillVM Bill)
        {
            TempAdditionVoucher addition = new TempAdditionVoucher
            {
                SerialNumber = GenerateSerial(),
                BillSerialNumber = Bill.SerialNumber,
                BillType = "فاتورة مشريات",
                UserType = 2,
                IsDeleted = Bill.IsDeleted,
                Note = Bill.Note,
                ShowNotesFlag = Bill.ShowNotesFlag,
                ApproveId = Bill.ApproveId,
                UserId = Bill.UserId,
                StoreId = Bill.StoreId,
                RevewId = Bill.RevewId,
                Client_Supplier_Id = Bill.SupplierId,
                CreatedBy = Bill.CreatedBy,
                CreatedDate = DateTime.Now,
                Date = Bill.Date,
            };
            Insert(addition);
            return addition;
        }
        public TempAdditionVoucher ConversionItemToTemp(ItemConversion_BillVM Bill)
        {
            TempAdditionVoucher addition = new TempAdditionVoucher
            {
                SerialNumber = GenerateSerial(),
                BillSerialNumber = Bill.ConversionRecordID,
                BillType = "التحويل من صنف لصنف",
                UserType = 3,
                IsDeleted = Bill.IsDeleted,
                Note = Bill.Note,
                ShowNotesFlag = false,
                ApproveId = 0,
                UserId =0,
                StoreId = Bill.StoreId,
                RevewId = 0,
                Client_Supplier_Id = Bill.EmployeeId.Value,
                CreatedBy = Bill.CreatedBy,
                CreatedDate = DateTime.Now,
                Date = Bill.Date,
            };
            Insert(addition);
            return addition;
        }
        public void Delete(int id)
        {
            TempAdditionVoucher edited = dbSet.FirstOrDefault(p => p.ID == id);
            Delete(edited);
        }
        public void DeletePhysical(int id)
        {
            TempAdditionVoucher edited = dbSet.FirstOrDefault(p => p.ID == id);
            Remove(edited);
        }
    }
}
