﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.StoreTransaction.Purchase;
using MasProject.Data.Models.StoreTransaction.Sales;
using MasProject.Domain.ViewModel.StoreTransaction.Purchase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasProject.Domain.Services.StoreTransaction.Sales
{
    public class PurchaseOtherIncomeService : Repository<PurchaseOtherIncome>
    {
        public PurchaseOtherIncomeService(DBContext context) : base(context)
        {
        }

        public List<PurchaseOtherIncomeVM> GetBySerial(int Serial)
        {
            return dbSet.Where(x => x.SerialNumber == Serial && x.IsDeleted == false).Select(s => new PurchaseOtherIncomeVM
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                Amount = s.Amount,
                CreatedBy = s.CreatedBy,
                CreatedDate = s.CreatedDate,
                ModifiedBy = s.ModifiedBy,
                ModifiedDate = s.ModifiedDate,
                Name = s.Name,
                IncomeId = s.IncomeId,

            }).ToList();
        }
        public PurchaseOtherIncome Add(PurchaseOtherIncomeVM s)
        {
            var newModel = new PurchaseOtherIncome
            {
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                Amount = s.Amount,
                CreatedBy = s.CreatedBy,
                CreatedDate = s.CreatedDate,
                ModifiedBy = s.ModifiedBy,
                ModifiedDate = s.ModifiedDate,
                Name = s.Name,
                IncomeId = s.IncomeId,
            };
            Insert(newModel);
            return newModel;
        }
        public PurchaseOtherIncome Edit(PurchaseOtherIncomeVM s)
        {
            var newModel = new PurchaseOtherIncome
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                Amount = s.Amount,
                CreatedBy = s.CreatedBy,
                CreatedDate = s.CreatedDate,
                ModifiedBy = s.ModifiedBy,
                ModifiedDate = s.ModifiedDate,
                Name = s.Name,
                IncomeId = s.IncomeId,

            };
            Update(newModel);
            return newModel;
        }
        public void Delete(int id)
        {
            List<PurchaseOtherIncome> DeleteList = dbSet.Where(p => p.SerialNumber == id && p.IsDeleted == false).ToList();
            foreach (var item in DeleteList)
            {
                Delete(item);
            }
        }
        public void DeletePhysical(int id)
        {
            List<PurchaseOtherIncome> DeleteList = dbSet.Where(p => p.SerialNumber == id).ToList();
            foreach (var item in DeleteList)
            {
                item.IsDeleted = true;
                Remove(item);
            }
        }
    }
}
