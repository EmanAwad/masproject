﻿using MasProject.Data.Models.StoreTransaction.Purchase;
using MasProject.Domain.ViewModel.StoreTransaction.Purchase;
using System;
using System.Collections.Generic;
using System.Linq;
using MasProject.Data.DataAccess;
namespace MasProject.Domain.Services.StoreTransaction.Purchase
{
    public class PurchaseReturns_ItemsService : Repository<PurchasesReturns_Items>
    {
        public PurchaseReturns_ItemsService(DBContext context) : base(context)
        {
        }
        public List<PurchasesReturns_ItemVM> GetBySerial(int Serial)
        {//&& x.IsDeleted == false
            return dbSet.Where(x => x.SerialNumber == Serial && x.IsDeleted == false).Select(s => new PurchasesReturns_ItemVM
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                ItemId = (int)s.ItemId,
                Box_ParCode=s.Item.National_ParCode,
                Price = s.Price,
                Quantity = s.Quantity,
                SavedPrice = s.SavedPrice,
                Total = s.Total,
                ItemName = s.Item.Name
            }).ToList();
        }
        public PurchasesReturns_Items Add(PurchasesReturns_ItemVM s)
        {
            var newModel = new PurchasesReturns_Items
            {
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                ItemId = (int)s.ItemId,
                Price = s.Price,
                Quantity = s.Quantity,
                SavedPrice = s.SavedPrice,
                Total = s.Total,
            };
            Insert(newModel);
            return newModel;
        }
        public PurchasesReturns_Items Edit(PurchasesReturns_ItemVM s)
        {
            var newModel = new PurchasesReturns_Items
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                ItemId = (int)s.ItemId,
                Price = s.Price,
                Quantity = s.Quantity,
                SavedPrice = s.SavedPrice,
                Total = s.Total,
            };
            Update(newModel);
            return newModel;
        }
        public decimal GetLatestPrice(int ItemId)
        {
            var model= dbSet.Where(x => x.ItemId == ItemId && x.IsDeleted == false).OrderByDescending(x => x.ID).ToList() ?? new List<PurchasesReturns_Items>();
            return  model.Count > 0 ? model.FirstOrDefault().Price.Value : 0 ;
        }
        public void Delete(int id)
        {
            List<PurchasesReturns_Items> DeleteList = dbSet.Where(p => p.SerialNumber == id && p.IsDeleted == false).ToList();
            foreach (var item in DeleteList)
            {
                Delete(item);
            }
        }
        public void DeletePhysical(int id)
        {
            List<PurchasesReturns_Items> DeleteList = dbSet.Where(p => p.SerialNumber == id).ToList();
            foreach (var item in DeleteList)
            {
                item.IsDeleted = true;
                Remove(item);
            }
        }
    }
}
