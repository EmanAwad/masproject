using MasProject.Data.DataAccess;
using MasProject.Data.Models.StoreTransaction.Purchase;
using MasProject.Domain.ViewModel.StoreTransaction.Purchase;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using MasProject.Data.Models;

namespace MasProject.Domain.Services.StoreTransaction.Purchase
{
    public class PurchasesReturns_BillService : Repository<PurchasesReturns_Bill>
    {
        DBContext _Context;
        public PurchasesReturns_BillService(DBContext context) : base(context)
        {
            _Context = context;
        }
        public IEnumerable<PurchaseBillVM> GetAllBySupplier(int SupplierId)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.SupplierId == SupplierId).Select(s => new PurchaseBillVM
            {
                ID = s.ID,
                TotalAfterDiscount = s.TotalAfterDiscount,
                TotalPrice = s.TotalPrice,
                StoreId = (int)s.StoreId,
                SerialNumber = s.SerialNumber,
                SupplierId = (int)s.SupplierId,
                Date = s.Date,
                Supplier = s.Supplier,
                // DiscountPrecentage = s.DiscountPrecentage,
                // DiscountValue = s.DiscountValue,
                // FlagType = s.FlagType,
                // SupplierName = s.Supplier.Name,
                TotalAfterTax = s.TotalAfterTax,

            }).OrderByDescending(s => s.ID);
        }
        public IEnumerable<PurchaseBillVM> GetAllByBranch(int BranchId)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.BranchId == BranchId).Select(s => new PurchaseBillVM
            {
                ID = s.ID,
                TotalAfterDiscount = s.TotalAfterDiscount,
                TotalPrice = s.TotalPrice,
                StoreId = (int)s.StoreId,
                SerialNumber = s.SerialNumber,
                SupplierId = (int)s.SupplierId,
                Date = s.Date,
                Supplier = s.Supplier,
                // DiscountPrecentage = s.DiscountPrecentage,
                // DiscountValue = s.DiscountValue,
                // FlagType = s.FlagType,
                // SupplierName = s.Supplier.Name,
                TotalAfterTax = s.TotalAfterTax,

            }).OrderByDescending(s => s.ID);
        }
        public IEnumerable<PurchaseBillVM> GetAllByDate(DateTime date)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.Date == date).Select(s => new PurchaseBillVM
            {
                ID = s.ID,
                TotalAfterDiscount = s.TotalAfterDiscount,
                TotalPrice = s.TotalPrice,
                StoreId = (int)s.StoreId,
                SerialNumber = s.SerialNumber,
                SupplierId = (int)s.SupplierId,
                Date = s.Date,
                Supplier = s.Supplier,
                // DiscountPrecentage = s.DiscountPrecentage,
                // DiscountValue = s.DiscountValue,
                // FlagType = s.FlagType,
                // SupplierName = s.Supplier.Name,
                TotalAfterTax = s.TotalAfterTax,

            }).OrderByDescending(s => s.ID);
        }
        public IEnumerable<PurchasesReturns_BillVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted == false && !g.IsConverted).Select(s => new PurchasesReturns_BillVM
            {
                ID = s.ID,
                TotalAfterDiscount = s.TotalAfterDiscount,
                TotalPrice = s.TotalPrice,
                StoreId = (int)s.StoreId,
                SerialNumber = s.SerialNumber,
                SupplierId = (int)s.SupplierId,
                Date = s.Date,
                Supplier = s.Supplier,
                // DiscountPrecentage = s.DiscountPrecentage,
                // DiscountValue = s.DiscountValue,
                // FlagType = s.FlagType,
                // SupplierName = s.Supplier.Name,
                TotalAfterTax = s.TotalAfterTax,
            }).OrderByDescending(s => s.ID);
        }
        public PurchasesReturns_BillVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new PurchasesReturns_BillVM
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                Note = s.Note,
                ShowNotesFlag = s.ShowNotesFlag,
                ApproveId = s.ApproveId,
                TotalAfterDiscount = s.TotalAfterDiscount,
                TotalPrice = s.TotalPrice,
                UserId = s.UserId,
                StoreId = (int)s.StoreId,
                SerialNumber = s.SerialNumber,
                RevewId = s.RevewId,
                ModifiedDate = s.ModifiedDate,
                ModifiedBy = s.ModifiedBy,
                SupplierId = (int)s.SupplierId,
                CreatedBy = s.CreatedBy,
                CreatedDate = s.CreatedDate,
                Date = s.Date.Date,
                DiscountPrecentage = s.DiscountPrecentage,
                DiscountValue = s.DiscountValue,
                FlagType = s.FlagType,
                BranchId=s.BranchId,
                IsConverted = s.IsConverted,
                AddtionTax = s.AddtionTax,
                AddtionTaxAmount = s.AddtionTaxAmount,
                SourceDeduction = s.SourceDeduction,
                SourceDeductionAmount = s.SourceDeductionAmount,
                TotalAfterTax = s.TotalAfterTax,
            }).FirstOrDefault();


        }
        public int GetId(int Serial)
        {
            return dbSet.Where(x => x.SerialNumber == Serial && x.IsDeleted == false).FirstOrDefault().ID;
        }
        public PurchasesReturns_Bill Add(PurchasesReturns_BillVM s)
        {
            var newModel = new PurchasesReturns_Bill
            {
                //ID = s.ID,
                //IsDeleted = s.IsDeleted,
                //IsConverted = false,
                //Note = s.Note,
                //ShowNotesFlag = s.ShowNotesFlag,
                //ApproveId = s.ApproveId,
                //TotalAfterDiscount = s.TotalAfterDiscount,
                //TotalPrice = s.TotalPrice,
                UserId = s.UserId,
                StoreId =  s.StoreId,
                SerialNumber = s.SerialNumber,
              //  RevewId = s.RevewId,
                SupplierId = (int)s.SupplierId,
                CreatedBy = s.CreatedBy,
                CreatedDate = DateTime.Now,
                Date = s.Date.ToLocalTime(),
               // DiscountPrecentage = s.DiscountPrecentage,
              //  DiscountValue = s.DiscountValue,
                FlagType = s.FlagType,
                BranchId=s.BranchId,
            };
            Insert(newModel);
            return newModel;
        }
        public PurchasesReturns_Bill Edit(PurchasesReturns_BillVM s)
        {
            PurchasesReturns_Bill edited = dbSet.FirstOrDefault(p => p.ID == s.ID);
            edited.Note = s.Note;
            edited.IsConverted = s.IsConverted;
            edited.ShowNotesFlag = s.ShowNotesFlag;
            edited.ApproveId = s.ApproveId;
            edited.TotalAfterDiscount = s.TotalAfterDiscount;
            edited.TotalPrice = s.TotalPrice;
            edited.UserId = s.UserId;
            edited.StoreId = (int)s.StoreId;
            edited.SerialNumber = s.SerialNumber;
            edited.RevewId = s.RevewId;
            edited.ModifiedDate = DateTime.Now;
            edited.ModifiedBy = s.ModifiedBy;
            edited.SupplierId = (int)s.SupplierId;
            edited.Date = s.Date.ToLocalTime();
            edited.DiscountPrecentage = s.DiscountPrecentage;
            edited.DiscountValue = s.DiscountValue;
            edited.FlagType = s.FlagType;
            edited.BranchId = s.BranchId;
            edited.AddtionTax = s.AddtionTax;
            edited.AddtionTaxAmount = s.AddtionTaxAmount;
            edited.SourceDeduction = s.SourceDeduction;
            edited.SourceDeductionAmount = s.SourceDeductionAmount;
            edited.TotalAfterTax = s.TotalAfterTax;
            Update(edited);
            return edited;
        }
        public void Delete(int id)
        {
            PurchasesReturns_Bill edited = dbSet.FirstOrDefault(p => p.ID == id && p.IsDeleted == false);
            Delete(edited);
        }
       
    }
}
