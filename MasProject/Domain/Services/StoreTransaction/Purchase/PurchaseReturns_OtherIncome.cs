﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.StoreTransaction.Purchase;
using MasProject.Data.Models.StoreTransaction.Sales;
using MasProject.Domain.ViewModel.StoreTransaction.Purchase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasProject.Domain.Services.StoreTransaction.Sales
{
    public class PurchaseReturns_OtherIncomeService : Repository<PurchaseReturns_OtherIncome>
    {
        public PurchaseReturns_OtherIncomeService(DBContext context) : base(context)
        {
        }

        public List<PurchaseReturns_OtherIncomeVM> GetBySerial(int Serial)
        {
            return dbSet.Where(x => x.SerialNumber == Serial && x.IsDeleted == false).Select(s => new PurchaseReturns_OtherIncomeVM
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                Amount = s.Amount,
                CreatedBy = s.CreatedBy,
                CreatedDate = s.CreatedDate,
                ModifiedBy = s.ModifiedBy,
                ModifiedDate = s.ModifiedDate,
                Name = s.Name,
                IncomeId = s.IncomeId,

            }).ToList();
        }
        public PurchaseReturns_OtherIncome Add(PurchaseReturns_OtherIncomeVM s)
        {
            var newModel = new PurchaseReturns_OtherIncome
            {
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                Amount = s.Amount,
                CreatedBy = s.CreatedBy,
                CreatedDate = s.CreatedDate,
                ModifiedBy = s.ModifiedBy,
                ModifiedDate = s.ModifiedDate,
                Name = s.Name,
                IncomeId = s.IncomeId,
            };
            Insert(newModel);
            return newModel;
        }
        public PurchaseReturns_OtherIncome Edit(PurchaseReturns_OtherIncomeVM s)
        {
            var newModel = new PurchaseReturns_OtherIncome
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                Amount = s.Amount,
                CreatedBy = s.CreatedBy,
                CreatedDate = s.CreatedDate,
                ModifiedBy = s.ModifiedBy,
                ModifiedDate = s.ModifiedDate,
                Name = s.Name,
                IncomeId = s.IncomeId,

            };
            Update(newModel);
            return newModel;
        }
        public void Delete(int id)
        {
            List<PurchaseReturns_OtherIncome> DeleteList = dbSet.Where(p => p.SerialNumber == id && p.IsDeleted == false).ToList();
            foreach (var item in DeleteList)
            {
                Delete(item);
            }
        }
        public void DeletePhysical(int id)
        {
            List<PurchaseReturns_OtherIncome> DeleteList = dbSet.Where(p => p.SerialNumber == id).ToList();
            foreach (var item in DeleteList)
            {
                item.IsDeleted = true;
                Remove(item);
            }
        }
    }
}
