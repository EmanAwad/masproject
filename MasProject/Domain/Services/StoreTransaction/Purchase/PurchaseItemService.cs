﻿using MasProject.Data.Models.StoreTransaction.Purchase;
using MasProject.Domain.ViewModel.StoreTransaction.Purchase;
using System;
using System.Collections.Generic;
using System.Linq;
using MasProject.Data.DataAccess;
namespace MasProject.Domain.Services.StoreTransaction.Purchase
{
    public class PurchaseItemService : Repository<PurchaseItem>
    {
        public PurchaseItemService(DBContext context) : base(context)
        {
        }
        //public IEnumerable<PurchaseItemVM> GetAll()
        //{
        //    return dbSet.Where(g => g.IsDeleted == false).Select(s => new PurchaseItemVM
        //    {
        //        ID = s.ID,
        //        IsDeleted = s.IsDeleted,
        //        SerialNumber = (int)s.SerialNumber,
        //        ItemId = (int)s.ItemId,
        //        Price = s.Price,
        //        Quantity = s.Quantity,
        //        SavedPrice = s.SavedPrice,
        //        Total = s.Total,
        //    }).OrderBy(s => s.ID);
        //}
        public List <PurchaseItemVM> GetBySerial(int Serial)
        {//&& x.IsDeleted == false
            return dbSet.Where(x => x.SerialNumber == Serial && x.IsDeleted == false).Select(s => new PurchaseItemVM
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                ItemId = (int)s.ItemId,
                Box_ParCode=s.Item.National_ParCode,
                Price = s.Price,
                Quantity = s.Quantity,
                SavedPrice = s.SavedPrice,
                Total = s.Total,
                ItemName = s.Item.Name,
            }).ToList();
        }
        public decimal GetLatestPrice(int ItemId)
        {
            var model = dbSet.Where(x => x.ItemId == ItemId && x.IsDeleted == false).OrderByDescending(x => x.ID).ToList() ?? new List<PurchaseItem>();
            return model.Count > 0 ? model.FirstOrDefault().Price.Value : 0;
        }
        public PurchaseItem Add(PurchaseItemVM s)
        {
            var newModel = new PurchaseItem
            {
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                ItemId = (int)s.ItemId,
                Price = s.Price,
                Quantity = s.Quantity,
                SavedPrice = s.SavedPrice,
                Total = s.Total,
            };
            Insert(newModel);
            return newModel;
        }
        public PurchaseItem Edit(PurchaseItemVM s)
        {
            var newModel = new PurchaseItem
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                ItemId = (int)s.ItemId,
                Price = s.Price,
                Quantity = s.Quantity,
                SavedPrice = s.SavedPrice,
                Total = s.Total,
            };
            Update(newModel);
            return newModel;
        }
        public void Delete(int id)
        {
            List<PurchaseItem> DeleteList = dbSet.Where(p => p.SerialNumber == id && p.IsDeleted == false).ToList();
            foreach (var item in DeleteList)
            {
                Delete(item);
            }
        }
        public void DeletePhysical(int id)
        {
            List<PurchaseItem> DeleteList = dbSet.Where(p => p.SerialNumber == id).ToList();
            foreach (var item in DeleteList)
            {
                item.IsDeleted = true;
                Remove(item);
            }
        }
    }
}