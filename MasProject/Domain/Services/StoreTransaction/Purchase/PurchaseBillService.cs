﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.StoreTransaction.Purchase;
using MasProject.Domain.ViewModel.StoreTransaction.Purchase;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace MasProject.Domain.Services.StoreTransaction.Purchase
{
    public class PurchaseBillService : Repository<PurchaseBill>
    {
        DBContext _Context;

        public PurchaseBillService(DBContext context) : base(context)
        {
            _Context = context;

        }
        public IEnumerable<PurchaseBillVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted  == false ).Select(s => new PurchaseBillVM
            {
                ID = s.ID,
                TotalAfterDiscount = s.TotalAfterDiscount,
                //UserId = s.UserId,
                //StoreId = (int)s.StoreId,
                SerialNumber = s.SerialNumber,
                SupplierId = (int)s.SupplierId,
                Supplier=s.Supplier,
                Date = s.Date,
                //DealTypeId = (int)s.DealTypeId,
                //DiscountPrecentage = s.DiscountPrecentage,
                //DiscountValue = s.DiscountValue,
                //Supplier=_Context.Supplier.Where(x=>x.ID==s.SupplierId).FirstOrDefault()
                TotalAfterTax = s.TotalAfterTax,
            }).OrderByDescending(s => s.ID);
        }
        public IEnumerable<PurchaseBillVM> GetAllByDate(DateTime date)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.Date == date).Select(s => new PurchaseBillVM
            {
                ID = s.ID,
                TotalAfterDiscount = s.TotalAfterDiscount,
                //UserId = s.UserId,
                //StoreId = (int)s.StoreId,
                SerialNumber = s.SerialNumber,
                SupplierId = (int)s.SupplierId,
                Supplier = s.Supplier,
                Date = s.Date,
                //DealTypeId = (int)s.DealTypeId,
                //DiscountPrecentage = s.DiscountPrecentage,
                //DiscountValue = s.DiscountValue,
                //Supplier=_Context.Supplier.Where(x=>x.ID==s.SupplierId).FirstOrDefault()
                TotalAfterTax = s.TotalAfterTax,

            }).OrderByDescending(s => s.ID);
        }
        public IEnumerable<PurchaseBillVM> GetAllBySupplier(int SupplierId)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.SupplierId == SupplierId).Select(s => new PurchaseBillVM
            {
                ID = s.ID,
                TotalAfterDiscount = s.TotalAfterDiscount,
                //UserId = s.UserId,
                //StoreId = (int)s.StoreId,
                SerialNumber = s.SerialNumber,
                SupplierId = (int)s.SupplierId,
                Supplier = s.Supplier,
                Date = s.Date,
                //DealTypeId = (int)s.DealTypeId,
                //DiscountPrecentage = s.DiscountPrecentage,
                //DiscountValue = s.DiscountValue,
                //Supplier=_Context.Supplier.Where(x=>x.ID==s.SupplierId).FirstOrDefault()
                TotalAfterTax = s.TotalAfterTax,

            }).OrderByDescending(s => s.ID);
        }
        public IEnumerable<PurchaseBillVM> GetAllByBranch(int BranchId)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.BranchId == BranchId).Select(s => new PurchaseBillVM
            {
                ID = s.ID,
                TotalAfterDiscount = s.TotalAfterDiscount,
                //UserId = s.UserId,
                //StoreId = (int)s.StoreId,
                SerialNumber = s.SerialNumber,
                SupplierId = (int)s.SupplierId,
                Supplier = s.Supplier,
                Date = s.Date,
                //DealTypeId = (int)s.DealTypeId,
                //DiscountPrecentage = s.DiscountPrecentage,
                //DiscountValue = s.DiscountValue,
                //Supplier=_Context.Supplier.Where(x=>x.ID==s.SupplierId).FirstOrDefault()
                TotalAfterTax = s.TotalAfterTax,

            }).OrderByDescending(s => s.ID);
        }
        public decimal? GetBillIdBySupplier(int SupplierId, int ThisSerial, int ItemID)
        {
            decimal? ReturnPrice = 0;
            var BillsList = dbSet.Where(x => x.IsDeleted == false && x.SupplierId == SupplierId && x.SerialNumber != ThisSerial).OrderByDescending(c => c.ID);

            foreach (var item in BillsList)
            {
                var serial = dbSet.Where(c => c.ID == item.ID).FirstOrDefault().SerialNumber;
                var ItemForSerialExist = _Context.PurchaseItem.Where(x => x.IsDeleted == false && x.ItemId == ItemID && x.SerialNumber == serial).FirstOrDefault();
                if (ItemForSerialExist != null)
                {
                    ReturnPrice = ItemForSerialExist.Price;
                    break;
                }
            }
            return ReturnPrice;
        }
        public PurchaseBillVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new PurchaseBillVM
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                Note = s.Note,
                ShowNotesFlag = s.ShowNotesFlag,
                ApproveId = s.ApproveId,
                TotalAfterDiscount = s.TotalAfterDiscount,
                TotalPrice = s.TotalPrice,
                UserId = s.UserId,
                StoreId = (int)s.StoreId,
                SerialNumber = s.SerialNumber,
                RevewId = s.RevewId,
                ModifiedDate = s.ModifiedDate,
                ModifiedBy = s.ModifiedBy,
                SupplierId = (int)s.SupplierId,
                CreatedBy = s.CreatedBy,
                CreatedDate = s.CreatedDate,
                Date = s.Date.Date,
                //DealTypeId = (int)s.DealTypeId,
                DiscountPrecentage = s.DiscountPrecentage,
                DiscountValue = s.DiscountValue,
                FlagType = s.FlagType,
                BranchId=s.BranchId,
                IsConverted=s.IsConverted,
                AddtionTax = s.AddtionTax,
                AddtionTaxAmount = s.AddtionTaxAmount,
                SourceDeduction = s.SourceDeduction,
                SourceDeductionAmount = s.SourceDeductionAmount,
                TotalAfterTax = s.TotalAfterTax,
                //TaxFileNumber=s.TaxFileNumber,
            }).FirstOrDefault();
        }
        public int GetId(int Serial)
        {
            return dbSet.Where(x => x.SerialNumber == Serial && x.IsDeleted == false).FirstOrDefault().ID;
        }
        public PurchaseBill Add(PurchaseBillVM s)
        {
            var newModel = new PurchaseBill
            {
                //ID = s.ID,
                //IsDeleted = s.IsDeleted,
                //Note = s.Note,
                //ShowNotesFlag = s.ShowNotesFlag,
               // ApproveId = s.ApproveId,
               // TotalAfterDiscount = s.TotalAfterDiscount,
               // TotalPrice = s.TotalPrice,
                UserId = s.UserId,
                StoreId =  s.StoreId,
                SerialNumber = s.SerialNumber,
               // RevewId = s.RevewId,
                SupplierId = (int)s.SupplierId,
                CreatedBy = s.CreatedBy,
                CreatedDate = DateTime.Now,
                Date = s.Date.ToLocalTime(),
                //DealTypeId = (int)s.DealTypeId,
              //  DiscountPrecentage = s.DiscountPrecentage,
              //  DiscountValue = s.DiscountValue,
                FlagType = s.FlagType,
                BranchId = s.BranchId,
                
            };
            Insert(newModel);
            return newModel;
        }
        public PurchaseBill Edit(PurchaseBillVM s)
        {
            PurchaseBill edited = dbSet.FirstOrDefault(p => p.ID == s.ID);
            edited.Note = s.Note;
            edited.IsConverted = s.IsConverted;
            edited.ShowNotesFlag = s.ShowNotesFlag;
            edited.ApproveId = s.ApproveId;
            edited.TotalAfterDiscount = s.TotalAfterDiscount;
            edited.TotalPrice = s.TotalPrice;
            edited.UserId = s.UserId;
            edited.StoreId = s.StoreId;
            edited.SerialNumber = s.SerialNumber;
            edited.RevewId = s.RevewId;
            edited.ModifiedDate = DateTime.Now;
            edited.ModifiedBy = s.ModifiedBy;
            edited.SupplierId = (int)s.SupplierId;
            edited.Date = s.Date.ToLocalTime();
            //edited.DealTypeId = (int)s.DealTypeId;
            edited.DiscountPrecentage = s.DiscountPrecentage;
            edited.DiscountValue = s.DiscountValue;
            edited.FlagType = s.FlagType;
            edited.BranchId = s.BranchId;
            edited.AddtionTax = s.AddtionTax;
            edited.AddtionTaxAmount = s.AddtionTaxAmount;
            edited.SourceDeduction = s.SourceDeduction;
            edited.SourceDeductionAmount = s.SourceDeductionAmount;
            edited.TotalAfterTax = s.TotalAfterTax;
            //edited.TaxFileNumber = s.TaxFileNumber;
            Update(edited);
            return edited;
        }
        public void Delete(int id)
        {
            PurchaseBill edited = dbSet.FirstOrDefault(p => p.ID == id && p.IsDeleted == false);
            Delete(edited);
        }
       
    }
}
