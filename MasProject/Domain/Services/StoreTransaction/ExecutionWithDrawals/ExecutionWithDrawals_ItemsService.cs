﻿using MasProject.Data.Models.StoreTransaction.ExecutionWithDrawals;
using MasProject.Domain.ViewModel.StoreTransaction.ExecutionWithDrawals;
using System;
using System.Collections.Generic;
using System.Linq;
using MasProject.Data.DataAccess;

namespace MasProject.Domain.Services.StoreTransaction.ExecutionWithDrawals
{
   public class ExecutionWithDrawals_ItemsService : Repository<ExecutionWithDrawals_Items>
    {
        public ExecutionWithDrawals_ItemsService(DBContext context) : base(context)
        {
        }
        public List<ExecutionWithDrawals_ItemsVM> GetBySerial(int Serial, int StoreID)
        {//&& x.IsDeleted == false
            return dbSet.Where(x => x.SerialNumber == Serial && x.StoreId == StoreID && x.IsDeleted == false).Select(s => new ExecutionWithDrawals_ItemsVM
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                ItemId = (int)s.ItemId,
                Box_ParCode=s.Item.National_ParCode,
                Price = s.Price,
                Quantity = s.Quantity,
                SavedPrice = s.SavedPrice,
                Total = s.Total,
                ItemName = s.Item.Name
            }).ToList();
        }

        public ExecutionWithDrawals_Items Add(ExecutionWithDrawals_ItemsVM s)
        {
            var newModel = new ExecutionWithDrawals_Items
            {
                //ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                ItemId = (int)s.ItemId,
                Price = s.Price,
                Quantity = s.Quantity,
                SavedPrice = s.SavedPrice,
                Total = s.Total,
                StoreId = s.StoreId,
            };
            Insert(newModel);
            return newModel;
        }

        public ExecutionWithDrawals_Items Edit(ExecutionWithDrawals_ItemsVM s)
        {
            var newModel = new ExecutionWithDrawals_Items
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                ItemId = (int)s.ItemId,
                Price = s.Price,
                Quantity = s.Quantity,
                SavedPrice = s.SavedPrice,
                Total = s.Total,
            };
            Update(newModel);
            return newModel;
        }
        public void Delete(int id)
        {
            List<ExecutionWithDrawals_Items> DeleteList = dbSet.Where(p => p.SerialNumber == id && p.IsDeleted == false).ToList();
            foreach (var item in DeleteList)
            {
                Delete(item);
            }
        }
        public void DeletePhysical(int id)
        {
            List<ExecutionWithDrawals_Items> DeleteList = dbSet.Where(p => p.SerialNumber == id).ToList();
            foreach (var item in DeleteList)
            {
                item.IsDeleted = true;
                Remove(item);
            }
        }
    }
}
