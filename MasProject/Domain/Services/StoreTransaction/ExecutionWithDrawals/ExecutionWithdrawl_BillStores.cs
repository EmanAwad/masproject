﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.StoreTransaction.ExecutionWithDrawals;
using MasProject.Domain.ViewModel.StoreTransaction.ExecutionWithDrawals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasProject.Domain.Services.StoreTransaction.ExecutionWithDrawals
{
  public  class ExecutionWithdrawl_BillStoresService : Repository<ExecutionWithdrawl_BillStores>
    {
        public ExecutionWithdrawl_BillStoresService(DBContext context) : base(context)
        {
        }
        public List<ExecutionWithdrawl_BillStoresVM> GetBySerial(int Serial)
        {//&& x.IsDeleted == false
            return dbSet.Where(x => x.SerialNumber == Serial && x.IsDeleted == false && x.StoreId != null).Select(s => new ExecutionWithdrawl_BillStoresVM
            {
                //Identifer = index + 1,
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                StoreId = s.StoreId,
            }).ToList();
        }

        public ExecutionWithdrawl_BillStores Add(ExecutionWithdrawl_BillStoresVM s)
        {
            var newModel = new ExecutionWithdrawl_BillStores
            {
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                StoreId = s.StoreId,
                CreatedDate = DateTime.Now,
            };
            Insert(newModel);
            return newModel;
        }
        public ExecutionWithdrawl_BillStores Edit(ExecutionWithdrawl_BillStoresVM s)
        {
            var newModel = new ExecutionWithdrawl_BillStores
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                SerialNumber = (int)s.SerialNumber,
                StoreId = s.StoreId,
                ModifiedDate = DateTime.Now,
            };
            Update(newModel);
            return newModel;
        }
        public void Delete(int id)
        {
            List<ExecutionWithdrawl_BillStores> DeleteList = dbSet.Where(p => p.SerialNumber == id && p.IsDeleted == false).ToList();
            foreach (var item in DeleteList)
            {
                Delete(item);
            }
        }
        public void DeletePhysical(int id)
        {
            List<ExecutionWithdrawl_BillStores> DeleteList = dbSet.Where(p => p.SerialNumber == id).ToList();
            foreach (var item in DeleteList)
            {
                item.IsDeleted = true;
                Remove(item);
            }
        }
    }
}
