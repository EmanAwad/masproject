﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.StoreTransaction.ExecutionWithDrawals;
using MasProject.Domain.ViewModel.StoreTransaction.ExecutionWithDrawals;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using MasProject.Data.Models.StoreTransaction;

namespace MasProject.Domain.Services.StoreTransaction.ExecutionWithDrawals
{
    public class ExecutionWithDrawals_BillService : Repository<ExecutionWithDrawals_Bill>
    {
        DBContext _Context;

        public ExecutionWithDrawals_BillService(DBContext context) : base(context)
        {
            _Context = context;
        }
        public IEnumerable<ExecutionithDrawals_BillVM> GetAllByBranch(int BranchId)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.BranchId == BranchId).Select(s => new ExecutionithDrawals_BillVM
            {
                ID = s.ID,
                //IsDeleted = s.IsDeleted,
                //Note = s.Note,
                //ShowNotesFlag = s.ShowNotesFlag,
                //ApproveId = s.ApproveId,
                TotalAfterDiscount = s.TotalAfterDiscount,
                //TotalPrice = s.TotalPrice,
                //UserId = s.UserId,
                //StoreId = (int)s.StoreId,
                SerialNumber = s.SerialNumber,
                // RevewId = s.RevewId,
                //ModifiedDate = s.ModifiedDate,
                // ModifiedBy = s.ModifiedBy,
                BranchId = (int)s.BranchId,
                BranchName = s.BranchObj.Name,
                //  CreatedBy = s.CreatedBy,
                // CreatedDate = s.CreatedDate,
                Date = s.Date,
                //DealTypeId = (int)s.DealTypeId,
                //DiscountPrecentage = s.DiscountPrecentage,
                //DiscountValue = s.DiscountValue,
                FlagType = s.FlagType,

            }).OrderByDescending(s => s.Date.Date).ThenBy(x => x.SerialNumber);
        }
        public IEnumerable<ExecutionithDrawals_BillVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new ExecutionithDrawals_BillVM
            {
                ID = s.ID,
                //IsDeleted = s.IsDeleted,
                //Note = s.Note,
                //ShowNotesFlag = s.ShowNotesFlag,
                //ApproveId = s.ApproveId,
                TotalAfterDiscount = s.TotalAfterDiscount,
                //TotalPrice = s.TotalPrice,
                //UserId = s.UserId,
                //StoreId = (int)s.StoreId,
                SerialNumber = s.SerialNumber,
                // RevewId = s.RevewId,
                //ModifiedDate = s.ModifiedDate,
                // ModifiedBy = s.ModifiedBy,
                BranchId = (int)s.BranchId,
                BranchName = s.BranchObj.Name,
                //  CreatedBy = s.CreatedBy,
                // CreatedDate = s.CreatedDate,
                Date = s.Date,
                //DealTypeId = (int)s.DealTypeId,
                //DiscountPrecentage = s.DiscountPrecentage,
                //DiscountValue = s.DiscountValue,
                FlagType = s.FlagType,
            }).OrderByDescending(s => s.Date.Date).ThenBy(x => x.SerialNumber);
        }
        public ExecutionithDrawals_BillVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new ExecutionithDrawals_BillVM
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                Note = s.Note,
                ShowNotesFlag = s.ShowNotesFlag,
                ApproveId = s.ApproveId,
                TotalAfterDiscount = s.TotalAfterDiscount,
                TotalPrice = s.TotalPrice,
                UserId = s.UserId,
                StoreId = (int)s.StoreId,
                SerialNumber = s.SerialNumber,
                RevewId = s.RevewId,
                ModifiedDate = s.ModifiedDate,
                ModifiedBy = s.ModifiedBy,
                BranchId = (int)s.BranchId,
                CreatedBy = s.CreatedBy,
                CreatedDate = s.CreatedDate,
                Date = s.Date.Date,
                DealTypeId = (int)s.DealTypeId,
                DiscountPrecentage = s.DiscountPrecentage,
                DiscountValue = s.DiscountValue,
                FlagType = s.FlagType
            }).FirstOrDefault();
        }
        public int GetId(int Serial)
        {
            return dbSet.Where(x => x.SerialNumber == Serial && x.IsDeleted == false).FirstOrDefault().ID;
        }
        public ExecutionWithDrawals_Bill Add(ExecutionithDrawals_BillVM s)
        {
            var newModel = new ExecutionWithDrawals_Bill
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                IsConverted = false,
                Note = s.Note,
                ShowNotesFlag = s.ShowNotesFlag,
                ApproveId = s.ApproveId,
                TotalAfterDiscount = s.TotalAfterDiscount,
                TotalPrice = s.TotalPrice,
                UserId = s.UserId,
                // StoreId = (int)s.StoreId,
                SerialNumber = s.SerialNumber,
                RevewId = s.RevewId,
                BranchId = s.BranchId,
                CreatedBy = s.CreatedBy,
                CreatedDate = DateTime.Now,
                Date = s.Date.ToLocalTime(),
                DealTypeId = s.DealTypeId,
                DiscountPrecentage = s.DiscountPrecentage,
                DiscountValue = s.DiscountValue,
                FlagType = s.FlagType
            };
            Insert(newModel);
            return newModel;
        }

        public ExecutionWithDrawals_Bill Edit(ExecutionithDrawals_BillVM s)
        {
            ExecutionWithDrawals_Bill edited = dbSet.Where(x => x.ID == s.ID).FirstOrDefault();
            //edited.IsDeleted = s.IsDeleted;
            edited.IsConverted = s.IsConverted;
            edited.Note = s.Note;
            edited.ShowNotesFlag = s.ShowNotesFlag;
            edited.ApproveId = s.ApproveId;
            edited.TotalAfterDiscount = s.TotalAfterDiscount;
            edited.TotalPrice = s.TotalPrice;
            edited.UserId = s.UserId;
            //edited.StoreId = (int)s.StoreId;
            edited.SerialNumber = s.SerialNumber;
            edited.RevewId = s.RevewId;
            edited.ModifiedDate = DateTime.Now;
            edited.ModifiedBy = s.ModifiedBy;
            edited.BranchId = s.BranchId;
            edited.Date = s.Date.ToLocalTime();
            edited.DealTypeId = (int)s.DealTypeId;
            edited.DiscountPrecentage = s.DiscountPrecentage;
            edited.DiscountValue = s.DiscountValue;
            edited.FlagType = s.FlagType;
            Update(edited);
            return edited;
        }
        public void Delete(int id)
        {
            ExecutionWithDrawals_Bill edited = dbSet.FirstOrDefault(p => p.ID == id && p.IsDeleted == false);
            Delete(edited);
        }
    }
}
