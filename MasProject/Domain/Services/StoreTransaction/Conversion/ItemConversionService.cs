﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Items;
using MasProject.Domain.ViewModel.Items;
using MasProject.Domain.ViewModel.StoreTransaction.Conversion;
using MasProject.Domain.ViewModel.Persons;
using System;
using System.Collections.Generic;
using System.Linq;
using MasProject.Data.Models.StoreTransaction.Conversion;

namespace MasProject.Domain.Services.StoreTransaction
{
    public class ItemConversionService : Repository<ItemConversion>
    {
        private DBContext _context;
        public ItemConversionService(DBContext context) : base(context)
        {
            _context = context;
        }

        public IEnumerable<ItemConversionVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new ItemConversionVM
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                ConversionRecordID = s.ConversionRecordID,
                EmployeeId = s.EmployeeId,
                EmployeeName = s.Employee.Name,
                StoreId = s.StoreId,
                StoreName = s.Store.Name,
                ItemId = s.ItemId,
                ItemName = s.Item.Name,
                Date = s.Date,
                ConversionReason = s.ConversionReason,
                Quantity = s.Quantity,
            })

           .OrderBy(s => s.ID);
        }

        public ItemConversionVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new ItemConversionVM
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                ConversionRecordID = s.ConversionRecordID,
                EmployeeId = s.EmployeeId,
           
                StoreId = s.StoreId,
               
                ItemId = s.ItemId,
              
                Date = s.Date,
                ConversionReason = s.ConversionReason,
                Quantity = s.Quantity,

            }).FirstOrDefault();
        }

        public ItemConversion Add(ItemConversionVM model)
        {
            var newModel = new ItemConversion
            {
                ID = model.ID,
                IsDeleted = model.IsDeleted,
                ConversionRecordID = model.ConversionRecordID,
                EmployeeId = model.EmployeeId,
                StoreId = model.StoreId,
                ItemId = model.ItemId,
                Date = model.Date.ToLocalTime(),
                ConversionReason = model.ConversionReason,
                Quantity = model.Quantity,

            };
            Insert(newModel);
            return newModel;
        }


        public ItemConversion Edit(ItemConversionVM model)
        {
            var newModel = new ItemConversion
            {
                ID = model.ID,
                ModifiedDate = DateTime.Today,
                ConversionRecordID = model.ConversionRecordID,
                EmployeeId = model.EmployeeId,
                StoreId = model.StoreId,
                ItemId = model.ItemId,
                Date = model.Date.ToLocalTime(),
                ConversionReason = model.ConversionReason,
                Quantity = model.Quantity,
            };
            Update(newModel);
            return newModel;
        }

        public void Delete(int id)
        {
            ItemConversion edited = dbSet.FirstOrDefault(p => p.ID == id);
            Delete(edited);
        }
    }
}
