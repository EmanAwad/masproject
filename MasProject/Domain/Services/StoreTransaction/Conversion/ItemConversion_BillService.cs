﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Items;
using MasProject.Domain.ViewModel.Items;
using MasProject.Domain.ViewModel.StoreTransaction.Conversion;
using MasProject.Domain.ViewModel.Persons;
using System;
using System.Collections.Generic;
using System.Linq;
using MasProject.Data.Models.StoreTransaction.Conversion;

namespace MasProject.Domain.Services.StoreTransaction
{
    public class ItemConversion_BillService : Repository<ItemConversion_Bill>
    {
        private DBContext _context;
        public ItemConversion_BillService(DBContext context) : base(context)
        {
            _context = context;
        }
        public IEnumerable<ItemConversion_BillVM> GetAllByStore(int StoreId)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.StoreId == StoreId).Select(s => new ItemConversion_BillVM
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                ConversionRecordID = s.ConversionRecordID,
                EmployeeId = s.EmployeeId,
                EmployeeName = s.Employee.Name,
                StoreId = s.StoreId,
                StoreName = s.Store.Name,
                Date = s.Date,
                ConversionReason = s.ConversionReason,
                TotalPriceFrom = s.TotalPriceFrom,
                TotalPriceTo = s.TotalPriceTo
            })

           .OrderByDescending(s => s.ID);
        }
        //public IEnumerable<ItemConversion_BillVM> GetAllByItem()
        //{
        //    return dbSet.Where(g => g.IsDeleted == false && g.Item).Select(s => new ItemConversion_BillVM
        //    {
        //        ID = s.ID,
        //        IsDeleted = s.IsDeleted,
        //        ConversionRecordID = s.ConversionRecordID,
        //        EmployeeId = s.EmployeeId,
        //        EmployeeName = s.Employee.Name,
        //        StoreId = s.StoreId,
        //        StoreName = s.Store.Name,
        //        Date = s.Date,
        //        ConversionReason = s.ConversionReason,
        //        TotalPriceFrom = s.TotalPriceFrom,
        //        TotalPriceTo = s.TotalPriceTo
        //    })

        //   .OrderByDescending(s => s.ID);
        //}
        public IEnumerable<ItemConversion_BillVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new ItemConversion_BillVM
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                ConversionRecordID = s.ConversionRecordID,
                EmployeeId = s.EmployeeId,
                EmployeeName = s.Employee.Name,
                StoreId = s.StoreId,
                StoreName = s.Store.Name,
                Date = s.Date,
                ConversionReason = s.ConversionReason,
                TotalPriceFrom = s.TotalPriceFrom,
                TotalPriceTo = s.TotalPriceTo
            })

           .OrderByDescending(s => s.ID);
        }
        public ItemConversion_BillVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new ItemConversion_BillVM
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                ConversionRecordID = s.ConversionRecordID,
                EmployeeId = s.EmployeeId,
                StoreId = s.StoreId,
                Date = s.Date.Date,
                ConversionReason = s.ConversionReason,
                TotalPriceFrom = s.TotalPriceFrom,
                TotalPriceTo = s.TotalPriceTo,
                IsConverted=s.IsConverted,
                BranchId=s.BranchId,
            }).FirstOrDefault();
        }
        public ItemConversion_Bill Add(ItemConversion_BillVM model)
        {
            var newModel = new ItemConversion_Bill
            {
                IsDeleted = model.IsDeleted,
                ConversionRecordID = model.ConversionRecordID,
                EmployeeId = model.EmployeeId,
                StoreId = model.StoreId,
                Date = model.Date.ToLocalTime(),
                ConversionReason = model.ConversionReason,
                TotalPriceFrom = (decimal)model.TotalPriceFrom,
                TotalPriceTo = (decimal)model.TotalPriceTo,
                IsConverted = false,
                BranchId=model.BranchId,
                SerialNumber= model.ConversionRecordID,
            };
            Insert(newModel);
            return newModel;
        }
        public ItemConversion_Bill Edit(ItemConversion_BillVM model)
        {
            ItemConversion_Bill newModel = dbSet.Where(p => p.ID == model.ID).FirstOrDefault();
            newModel.ModifiedDate = DateTime.Today;
            newModel.ConversionRecordID = (int)model.ConversionRecordID;
            newModel.EmployeeId = model.EmployeeId;
            newModel.StoreId = model.StoreId;
            newModel.Date = model.Date.ToLocalTime();
            newModel.ConversionReason = model.ConversionReason;
            newModel.TotalPriceFrom = (decimal)model.TotalPriceFrom;
            newModel.TotalPriceTo = (decimal)model.TotalPriceTo;
            newModel.IsConverted = model.IsConverted;
            newModel.BranchId = model.BranchId;
            newModel.SerialNumber = model.ConversionRecordID;
            Update(newModel);
            return newModel;
        }
        public void Delete(int id)
        {
            ItemConversion_Bill edited = dbSet.Where(x=>x.IsDeleted==false).FirstOrDefault(p => p.ConversionRecordID == id);
            if (edited != null)
            {
                edited.IsDeleted = true;
                Update(edited);
            }
            //Delete(edited);
        }
        //public int GenerateID(int? BranchId)
        //{
        //    string Serial = "";
        //    if (BranchId != null && BranchId > 0)
        //        Serial = _context.Branch.FirstOrDefault(x => x.IsDeleted == false && x.ID == BranchId).arrangement.ToString();
        //    else
        //        Serial = _context.Branch.FirstOrDefault(x => x.IsDeleted == false && x.ID == 3).arrangement.ToString();
        //    Serial += DateTime.Now.ToString("yy");
        //    var model = dbSet.Where(x => x.IsDeleted == false).OrderByDescending(x => x.ID).FirstOrDefault();
        //    int SerialNumber = model != null ? (model.ConversionRecordID.Value + 1) : 0;
        //    Serial += SerialNumber > 0 ? SerialNumber.ToString().Substring(4, SerialNumber.ToString().Length - 4) : "0001";
        //    return int.Parse(Serial);
        //}
    }
}
