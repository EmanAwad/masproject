﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Items;
using MasProject.Domain.ViewModel.Items;
using MasProject.Domain.ViewModel.StoreTransaction.Conversion;
using MasProject.Domain.ViewModel.Persons;
using System;
using System.Collections.Generic;
using System.Linq;
using MasProject.Data.Models.StoreTransaction.Conversion;

namespace MasProject.Domain.Services.StoreTransaction.Conversion
{
    public class ItemConversion_ItemsService : Repository<ItemConversion_Items>
    {

        private DBContext _context;
        public ItemConversion_ItemsService(DBContext context) : base(context)
        {
            _context = context;
        }

        public IEnumerable<ItemConversion_ItemVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new ItemConversion_ItemVM
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                QuantityFrom = s.QuantityFrom,
                QuantityTo = s.QuantityTo,
                ConversionRecordID = s.ConversionRecordID,
                ItemIdFrom = s.ItemIdFrom,
                ItemIdTo = s.ItemIdTo,
                ItemNameTo = s.ItemTo.Name,
                ItemNameFrom = s.ItemFrom.Name,
                //         SavedPriceTo = s.SavedPriceTo,
                //        SavedPriceFrom = s.SavedPriceTo,
                PriceTo = s.PriceTo,
                PriceFrom = s.PriceFrom,
                TotalTo = s.TotalTo,
                TotalFrom = s.TotalFrom,
                ParCodeFrom = s.ParCodeFrom,
                ParCodeTo = s.ParCodeTo,
                IdentiferFrom = s.IdentiferFrom,
                IdentiferTo = s.IdentiferFromTo

            })

           .OrderByDescending(s => s.ID);
        }
        public List<ItemConversion_ItemVM> Get(int ConversionRecordId)
        {
            return dbSet.Where(x => x.ConversionRecordID == ConversionRecordId && x.IsDeleted == false).Select(s => new ItemConversion_ItemVM
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                QuantityFrom = s.QuantityFrom,
                QuantityTo = s.QuantityTo,
                ConversionRecordID = s.ConversionRecordID,
                ItemIdFrom = s.ItemIdFrom,
                ParCodeFrom = s.ItemFrom.National_ParCode,
                ParCodeTo = s.ItemTo.National_ParCode,
                ItemIdTo = s.ItemIdTo,
                ItemNameTo = s.ItemTo.Name,
                ItemNameFrom = s.ItemFrom.Name,
                //        SavedPriceTo = s.SavedPriceTo,
                //        SavedPriceFrom = s.SavedPriceTo,
                PriceTo = s.PriceTo,
                PriceFrom = s.PriceFrom,
                TotalTo = s.TotalTo,
                TotalFrom = s.TotalFrom
            }).ToList();
        }

        public void AddFrom(List<ItemConversion_ItemVM> modelList, int? ConvertID)
        {
            foreach (var model in modelList)
            {
                if(model.ItemIdFrom!=null && model.ItemIdFrom != 0)
                {
var newModel = new ItemConversion_Items
                {
                    IdentiferFrom = model.IdentiferFrom,
                    IsDeleted = model.IsDeleted,
                    QuantityFrom = (int)model.QuantityFrom,
                    ConversionRecordID = ConvertID,
                    ItemIdFrom = model.ItemIdFrom,
                    PriceFrom = (decimal)model.PriceFrom,
                    TotalFrom = (decimal)model.TotalFrom,
                    ParCodeFrom = model.ParCodeFrom,
                    CreatedDate = DateTime.Now,

                };
                Insert(newModel);
                }                
            }
            //return newModel;
        }

        public void AddTo(List<ItemConversion_ItemVM> modelList, int? ConvertID)
        {
            foreach (var model in modelList)
            {
                if (model.ItemIdTo != null && model.ItemIdTo != 0)
                {
                    var newModel = new ItemConversion_Items
                    {
                        IdentiferFromTo = model.IdentiferTo,
                        IsDeleted = model.IsDeleted,
                        QuantityTo = (int)model.QuantityTo,
                        ConversionRecordID = ConvertID,
                        ItemIdTo = model.ItemIdTo,
                        PriceTo = (decimal)model.PriceTo,
                        TotalTo = (decimal)model.TotalTo,
                        ParCodeTo = model.ParCodeTo,
                        CreatedDate = DateTime.Now,
                    };
                    Insert(newModel);
                }
            }
            //return newModel;
        }


        //public void EditFrom(List<ItemConversion_ItemVM> model,int ConversionRecordID)
        //{
        //    var oldEntry = dbSet.Where(x => x.ConversionRecordID == ConversionRecordID & x.ItemIdFrom != null).ToList();
        //    foreach (var item in oldEntry)
        //    {
        //        //oldEntry.QuantityFrom = (int)model.QuantityFrom,
        //        //ConversionRecordID = model.ConversionRecordID,
        //        //ItemIdFrom = model.ItemIdFrom,
        //        //PriceFrom = (decimal)model.PriceFrom,
        //        //TotalFrom = (decimal)model.TotalFrom
        //    }
        //    //var newModel = new ItemConversion_Items
        //    //{

        //    //    IsDeleted = model.IsDeleted,
        //    //    QuantityFrom = (int)model.QuantityFrom,
        //    //    ConversionRecordID = model.ConversionRecordID,
        //    //    ItemIdFrom = model.ItemIdFrom,
        //    //    PriceFrom = (decimal)model.PriceFrom,
        //    //    TotalFrom = (decimal)model.TotalFrom
        //    //};
        //    //Update(newModel);
        //    //return newModel;
        //}

        //public ItemConversion_Items EditTo(ItemConversion_ItemVM model)
        //{
        //    var newModel = new ItemConversion_Items
        //    {

        //        IsDeleted = model.IsDeleted,
        //        QuantityTo = (int)model.QuantityTo,
        //        ConversionRecordID = model.ConversionRecordID,
        //        ItemIdTo = model.ItemIdTo,
        //        PriceTo = (decimal)model.PriceTo,
        //        TotalTo = (decimal)model.TotalTo
        //    };
        //    Update(newModel);
        //    return newModel;
        //}

        public void Delete(int ConversionRecordID)
        {
            List<ItemConversion_Items> edited = dbSet.Where(p => p.ConversionRecordID == ConversionRecordID&& p.IsDeleted==false ).ToList();
            foreach (var item in edited)
            {
                item.IsDeleted = true;
                Update(item);
                //Delete(edited);
            }
        }
      
    }
}