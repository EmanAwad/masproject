﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Items;
using MasProject.Domain.ViewModel.Items;
using System;
using System.Collections.Generic;
using System.Linq;
namespace MasProject.Domain.Services.Items
{
    public class ItemOpeningBalanceService : Repository<ItemOpeningBalance>
    {
        public ItemOpeningBalanceService(DBContext context) : base(context)
        {
        }
        public IEnumerable<ItemOpeningBalanceVM> GetAll()
        {
            var xx = dbSet.Where(g => g.IsDeleted == false).Select(s => new ItemOpeningBalanceVM
            {
                ID = s.ID,
                DocumentNumber=s.DocumentNumber,
                Date = s.Date,
                Notes=s.Notes,
                StoreName = s.Store.Name,
                EmployeeName=s.Employee.UserName
            }).OrderByDescending(s => s.ID);
            return xx;
        }

        public ItemOpeningBalanceVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new ItemOpeningBalanceVM
            {
                ID = s.ID,
                Date = s.Date.Date,
                DocumentNumber=s.DocumentNumber,
                Notes=s.Notes,
                EmployeeId=s.EmployeeId,
                StoreId =  s.StoreId,
                
            }).FirstOrDefault();
        }

        public ItemOpeningBalance Add(ItemOpeningBalanceVM model)
        {
            var newModel = new ItemOpeningBalance
            {
                //ID = model.ID,
                Date = model.Date.ToLocalTime(),
                DocumentNumber = model.DocumentNumber,
                EmployeeId = model.EmployeeId,
                StoreId = model.StoreId,
                Notes=model.Notes
            };
            Insert(newModel);
            return newModel;
        }

        public ItemOpeningBalance Edit(ItemOpeningBalanceVM model)
                {
            var newModel = new ItemOpeningBalance
            {
                ID = model.ID,
                Date = model.Date.ToLocalTime(),
                DocumentNumber = model.DocumentNumber,
                EmployeeId = model.EmployeeId,
                StoreId = model.StoreId,
                Notes=model.Notes
            };
            Update(newModel);
            return newModel;
        }

        public void Delete(int id)
        {
            ItemOpeningBalance edited = dbSet.FirstOrDefault(p => p.DocumentNumber == id & p.IsDeleted == false);
            Delete(edited);
        }

        public int GenerateSerial()
        {
            int serial = dbSet.FirstOrDefault(x => x.IsDeleted == false) != null ? dbSet.Max(c => c.DocumentNumber) + 1 : 1;
            return serial;
        }

        public List<ItemOpeningBalanceVM> GetAllForInsert()
        {
           return dbSet.Where(g => g.IsDeleted == false).Select(s => new ItemOpeningBalanceVM
            {
                ID = s.ID,
                DocumentNumber = s.DocumentNumber,
                Date = s.Date,
              StoreId=s.StoreId,
            }).ToList() ;
        }
    }
}
