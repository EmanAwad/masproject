﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Items;
using MasProject.Domain.ViewModel.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasProject.Domain.Services.Items
{
    public class ItemOpeningBalanceDetailsService : Repository<ItemOpeningBalanceDetails>
    {
        public ItemOpeningBalanceDetailsService(DBContext context) : base(context)
        {
        }
        public IEnumerable<ItemOpeningBalanceDetailsVM> GetAll()
        {
            return dbSet.Where(x => x.IsDeleted == false).Select(s => new ItemOpeningBalanceDetailsVM
            {
                ID = s.ID,
                ItemDocumentNumber = s.ItemDocumentNumber,
                Notes = s.Notes,
                ItemId = s.ItemId
            }).OrderByDescending(s => s.ID);
        }

        public List<ItemOpeningBalanceDetailsVM> GetByDocumentNumber(int DocumentNumber)
        {
            return dbSet.Where(x => x.ItemDocumentNumber == DocumentNumber && x.IsDeleted == false).Select(s => new ItemOpeningBalanceDetailsVM
            {
                ID = s.ID,
                ItemDocumentNumber = s.ItemDocumentNumber,
                Notes = s.Notes,
                ItemId = s.ItemId,
                Quantity = s.Quantity,
                price = (float?)s.price,
                TotalPrice = s.TotalPrice,
                ItemCode = s.Item.National_ParCode,
                ItemName = s.Item.Name,
                
            }).ToList();

        }
        public ItemOpeningBalanceDetails Add(ItemOpeningBalanceDetailsVM balance)
        {
            ItemOpeningBalanceDetails model = new ItemOpeningBalanceDetails();
            var ItemModel = dbSet.FirstOrDefault(x => x.ItemDocumentNumber == balance.ItemDocumentNumber && x.ItemId == balance.ItemId && x.IsDeleted == false);
            if (ItemModel == null)
            {
                 model = new ItemOpeningBalanceDetails
                {
                     //ID = balance.ID,
                     ItemDocumentNumber = balance.ItemDocumentNumber,
                    Notes = balance.Notes,
                    ItemId = balance.ItemId,
                    Quantity = balance.Quantity,
                    price = balance.price,
                    TotalPrice = balance.price * balance.Quantity,

                };
                Insert(model);
            }
            else
            {
                 model = new ItemOpeningBalanceDetails
                 {
                     ID= ItemModel.ID,
                    ItemDocumentNumber = balance.ItemDocumentNumber,
                    Notes = balance.Notes,
                    ItemId = balance.ItemId,
                    Quantity = balance.Quantity,
                    price = balance.price,
                    TotalPrice = balance.price * balance.Quantity,

                };
                Edit(model);
            }
            return model;
        }
        public ItemOpeningBalanceDetails Edit(ItemOpeningBalanceDetails balance)
        {
            var newModel = dbSet.FirstOrDefault(x => x.ID == balance.ID && x.IsDeleted == false);
            newModel.ItemDocumentNumber = balance.ItemDocumentNumber;
            newModel.Notes = balance.Notes;
            newModel.ItemId = balance.ItemId;
            newModel.Quantity = balance.Quantity;
            newModel.price = balance.price;
            newModel.TotalPrice = balance.TotalPrice;
            Update(newModel);
            return newModel;
        }

        public void Delete(int DocumentNumber)
        {
            List<ItemOpeningBalanceDetails> DeleteList = dbSet.Where(p => p.ItemDocumentNumber == DocumentNumber).ToList();
            foreach (var item in DeleteList)
            {
                Delete(item);
               // item.IsDeleted = true;
               // Update(item);
            }
        }
        public void DeletePhysical(int DocumentNumber)
        {
               List<ItemOpeningBalanceDetails> DeleteList = dbSet.Where(p => p.ItemDocumentNumber == DocumentNumber).ToList();
                if (DeleteList != null)
                {
                    foreach (var item in DeleteList)
                    {
                        Remove(item);
                    }
                }
            }
            
        }
    }
