﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Items;
using MasProject.Domain.ViewModel.Items;
using MasProject.Domain.ViewModel.Persons;
using System;
using System.Collections.Generic;
using System.Linq;
using static MasProject.Domain.Enums.Enums;
using MasProject.Domain.ViewModel;
namespace MasProject.Domain.Services.Items
{
    public class ItemServices : Repository<Item>
    {
        private DBContext _context;
        public ItemServices(DBContext context) : base(context)
        {
            _context = context;
        }
        public IEnumerable<LookupItem> GetItems()
        {
            return dbSet.Where(x => x.IsDeleted == false).Select(s => new LookupItem
            {
                ID = s.ID,
                Name = s.Name,
                ItemImg = s.ItemImg ?? "\\Images\\Items\\DefaultItem.JPG",
                Code = s.National_ParCode,
                BoxCode=s.Box_ParCode,
                InterationalParCode=s.Interational_ParCode,
                SupplierParCode=s.Supplier_ParCode,
                ParCodeNote=s.ParCode_Note,
                GuaranteeParCode = s.Guarantee_ParCode,
                CostPrice = (float)s.CostPrice,
                ExecutionPrice= (float)s.ExecutionPrice,
                other_Price = (float)s.other_Price,
                SellingPrice = (float)s.SellingPrice,
                ShowPrice = (float)s.ShowPrice,
                SupplierPrice = (float)s.SupplierPrice,
                WholesalePrice = (float)s.WholesalePrice,
            }).OrderBy(s => s.ID);
        }
        public IEnumerable<ItemVM> GetItemsSpeical()
        {
            return dbSet.Where(x => x.IsDeleted == false).Select(s => new ItemVM
            {
                ID = s.ID,
                Name = s.Name,
                ItemImg = s.ItemImg ?? "\\Images\\Items\\DefaultItem.JPG",
                National_ParCode = s.National_ParCode,
                Box_ParCode = s.Box_ParCode,
                Interational_ParCode = s.Interational_ParCode,
                Supplier_ParCode = s.Supplier_ParCode,
                ParCode_Note = s.ParCode_Note,
                Guarantee_ParCode = s.Guarantee_ParCode,
                CostPrice = (decimal?)s.CostPrice,
                ExecutionPrice = (float)s.ExecutionPrice,
                other_Price =  s.other_Price,
                SellingPrice = (float)s.SellingPrice,
                ShowPrice = (float)s.ShowPrice,
                SupplierPrice = (float)s.SupplierPrice,
                WholesalePrice = (float)s.WholesalePrice,
            }).OrderBy(s => s.ID);
        }
        public IEnumerable<ItemVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted == false && g.IsShow == false).Select(s => new ItemVM
            {
                ID = s.ID,
                Name = s.Name,
                IsDeleted = s.IsDeleted,
                National_ParCode = s.National_ParCode,
                Interational_ParCode = s.Interational_ParCode,
                Box_ParCode = s.Box_ParCode,
                Supplier_ParCode = s.Supplier_ParCode,
                ParCode_Note = s.ParCode_Note,
                CostPrice = (decimal?)s.CostPrice,
                SupplierPrice = s.SupplierPrice,
                WholesalePrice = s.WholesalePrice,
                SellingPrice = s.SellingPrice,
                ExecutionPrice = s.ExecutionPrice,
                other_Price = s.other_Price,
                BoxNumber = s.BoxNumber,
                CBM = s.CBM,
                BoxWeight = s.BoxWeight,
                //StoredQuantity = s.StoredQuantity,
                //OrderLimit = s.OrderLimit,
                ItemImg = s.ItemImg ?? "\\Images\\Items\\DefaultItem.JPG",
                ItemDate = s.ItemDate,
                //CollectionName = s.ItemCollection.Name,
                //TypeName = s.ItemType.Name,
                //SupplierName = s.Supplier.Name,
                CollectionName = _context.ItemsCollection.Where(x => x.ID == s.CollectionId).FirstOrDefault().Name,
                TypeName = _context.ItemTypes.Where(x => x.ID == s.TypeId).FirstOrDefault().Name,
                SupplierName = _context.Supplier.Where(x => x.ID == s.SupplierId).FirstOrDefault().Name,
                //ShielfId = s.ShielfId,
                ShowPrice = s.ShowPrice,
                Guarantee_ParCode = s.Guarantee_ParCode,
                IsShow = s.IsShow,
               
            })
            .OrderBy(s => s.National_ParCode);
        }
        public IEnumerable<ItemVM> GetAllByCollection(int CollectionId)
        {
            return dbSet.Where(g => g.IsDeleted == false && g.IsShow == false && g.CollectionId == CollectionId).Select(s => new ItemVM
            {
                ID = s.ID,
                Name = s.Name,
            }).ToList();
        }
        public decimal GetItemPriceByDealId(int DealId , int ItemId)
        {
            float price = 0;
            int PriceType = _context.DealTypes.Where(x => x.ID == DealId & x.IsDeleted == false).FirstOrDefault().PriceType;
            switch (PriceType)
            {
                case (int)EnPriceTypes.التكلفة:
                    price = dbSet.FirstOrDefault(x => x.ID == ItemId).CostPrice ?? 0;
                    break;
                case (int)EnPriceTypes.الموزع:
                    price = dbSet.FirstOrDefault(x => x.ID == ItemId).SupplierPrice ?? 0;
                    break;
                case (int)EnPriceTypes.الجملة:
                    price = dbSet.FirstOrDefault(x => x.ID == ItemId).WholesalePrice ?? 0;
                    break;
                case (int)EnPriceTypes.البيع:
                    price = dbSet.FirstOrDefault(x => x.ID == ItemId).SellingPrice ?? 0;
                    break;
                case (int)EnPriceTypes.التنفيذ:
                    price = dbSet.FirstOrDefault(x => x.ID == ItemId).ExecutionPrice ?? 0;
                    break;
                case (int)EnPriceTypes.العرض:
                    price = dbSet.FirstOrDefault(x => x.ID == ItemId).ShowPrice ?? 0;
                    break;
                case (int)EnPriceTypes.التجزئة:
                    price = dbSet.FirstOrDefault(x => x.ID == ItemId).SellingPrice ?? 0;
                    break;
                case (int)EnPriceTypes.أخرى:
                    price = dbSet.FirstOrDefault(x => x.ID == ItemId).other_Price ?? 0;
                    break;
            }
            return (decimal)price;
        }
        public ItemVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new ItemVM
            {
                ID = s.ID,
                Name = s.Name,
                IsDeleted = s.IsDeleted,
                National_ParCode = s.National_ParCode,
                Interational_ParCode = s.Interational_ParCode,
                Box_ParCode = s.Box_ParCode,
                Supplier_ParCode = s.Supplier_ParCode,
                ParCode_Note = s.ParCode_Note,
                CostPrice = (decimal?)s.CostPrice,
                SupplierPrice = s.SupplierPrice,
                WholesalePrice = s.WholesalePrice,
                SellingPrice = s.SellingPrice,
                ExecutionPrice = s.ExecutionPrice,
                other_Price = s.other_Price,
                BoxNumber = s.BoxNumber,
                CBM = s.CBM,
                BoxWeight = s.BoxWeight,
                //StoredQuantity = s.StoredQuantity,
                //OrderLimit = s.OrderLimit,
                ItemImg = s.ItemImg,
                ItemDate = s.ItemDate.Date,
                CollectionId = s.CollectionId,
                TypeId = s.TypeId,
                SupplierId = s.SupplierId,
                //ShielfId = s.ShielfId,
                ShowPrice = s.ShowPrice,
                Guarantee_ParCode = s.Guarantee_ParCode,
                IsShow = s.IsShow,
            }).FirstOrDefault();
        }
        public ItemVM GetItemImage(int id)
        {
            ItemVM item = new ItemVM();
            item.ItemImg = dbSet.Where(x => x.ID == id && x.IsDeleted == false).FirstOrDefault().ItemImg ?? "\\Images\\Items\\DefaultItem.JPG";
            return item;
        }
        public string GetCodeById(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).FirstOrDefault().National_ParCode;

        }
        public ItemBillVM GetItemDataById(int id)
        {
            ItemBillVM Item = dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new ItemBillVM
            {
                National_ParCode = s.National_ParCode,
                ItemImg = s.ItemImg
            }).FirstOrDefault();
            return Item;
        }
        public ItemVM GetByAnyCode(string AnyCode)
        {
            ItemVM model;
            model = dbSet.Where(x => (x.Box_ParCode == AnyCode || x.Guarantee_ParCode == AnyCode || x.Interational_ParCode == AnyCode || x.National_ParCode == AnyCode || x.ParCode_Note == AnyCode || x.Supplier_ParCode == AnyCode) && x.IsDeleted == false).Select(s => new ItemVM
            {
                ID = s.ID,
                Name = s.Name,
                IsDeleted = s.IsDeleted,
                National_ParCode = s.National_ParCode,
                Interational_ParCode = s.Interational_ParCode,
                Box_ParCode = s.Box_ParCode,
                Supplier_ParCode = s.Supplier_ParCode,
                ParCode_Note = s.ParCode_Note,
                Guarantee_ParCode = s.Guarantee_ParCode,
            }).FirstOrDefault();
            return model;
        }
        public Item Add(ItemVM model)
        {
            var newModel = new Item
            {
                ID = model.ID,
                Name = model.Name,
                CreatedDate = DateTime.Today,
                IsDeleted = false,
                National_ParCode = model.National_ParCode,
                Interational_ParCode = model.Interational_ParCode,
                Box_ParCode = model.Box_ParCode,
                Supplier_ParCode = model.Supplier_ParCode,
                ParCode_Note = model.ParCode_Note,
                CostPrice = model.CostPrice==null?0: (float)model.CostPrice,
                SupplierPrice = model.SupplierPrice,
                WholesalePrice = model.WholesalePrice,
                SellingPrice = model.SellingPrice,
                ExecutionPrice = model.ExecutionPrice,
                other_Price = model.other_Price,
                BoxNumber = model.BoxNumber,
                CBM = model.CBM,
                BoxWeight = model.BoxWeight,
                //StoredQuantity = model.StoredQuantity,
                //OrderLimit = model.OrderLimit,
                ItemImg = model.ItemImg,
                ItemDate = model.ItemDate.ToLocalTime(),
                CollectionId = model.CollectionId,
                TypeId = model.TypeId,
                SupplierId = model.SupplierId,
                //ShielfId = model.ShielfId,
                ShowPrice = model.ShowPrice,
                Guarantee_ParCode = model.Guarantee_ParCode,
                IsShow = model.IsShow,
            };
            Insert(newModel);
            return newModel;
        }

        public Item Edit(ItemVM model)
        {
            var newModel = new Item
            {
                ID = model.ID,
                Name = model.Name,
                ModifiedDate = DateTime.Today,
                National_ParCode = model.National_ParCode,
                Interational_ParCode = model.Interational_ParCode,
                Box_ParCode = model.Box_ParCode,
                Supplier_ParCode = model.Supplier_ParCode,
                ParCode_Note = model.ParCode_Note,
                CostPrice = model.CostPrice == null ? 0 : (float)model.CostPrice,
                SupplierPrice = model.SupplierPrice,
                WholesalePrice = model.WholesalePrice,
                SellingPrice = model.SellingPrice,
                ExecutionPrice = model.ExecutionPrice,
                other_Price = model.other_Price,
                BoxNumber = model.BoxNumber,
                CBM = model.CBM,
                BoxWeight = model.BoxWeight,
                //StoredQuantity = model.StoredQuantity,
                //OrderLimit = model.OrderLimit,
                ItemImg = model.ItemImg,
                ItemDate = model.ItemDate.ToLocalTime(),
                CollectionId = model.CollectionId,
                TypeId = model.TypeId,
                SupplierId = model.SupplierId,
                //ShielfId = model.ShielfId,
                ShowPrice = model.ShowPrice,
                Guarantee_ParCode = model.Guarantee_ParCode,
                IsShow = model.IsShow,
            };
            Update(newModel);
            return newModel;
        }

        public List<Item> ItemSearch(){
            return _context.Items.ToList();
        }
        public void Delete(int id)
        {

            Item edited = dbSet.FirstOrDefault(p => p.ID == id & p.IsDeleted == false);
            Delete(edited);
        }

        public void Hide(int id)
        {

            Item Hidden = dbSet.FirstOrDefault(p => p.ID == id & p.IsShow == false);
            Hidden.IsShow = true;
            Update(Hidden);
        }

        public bool IsItemUsed(int Id)
        {
            var SalesItems = _context.SalesItems.Where(x => x.ItemId == Id && x.IsDeleted==false).ToList();
            if (SalesItems != null && SalesItems.Count > 0)
                return true;
            else {
                var MantienceItems = _context.MantienceItems.Where(x => x.ItemId == Id && x.IsDeleted == false).ToList();
                if (MantienceItems != null && MantienceItems.Count > 0)
                    return true;
                else
                {
                    var ExecutiveItems = _context.ExecutiveItems.Where(x => x.ItemId == Id && x.IsDeleted == false).ToList();
                    if (ExecutiveItems != null && ExecutiveItems.Count > 0)
                        return true;
                    else
                    {
                        var SalesReturnsItems = _context.SalesReturnsItems.Where(x => x.ItemId == Id && x.IsDeleted == false).ToList();
                        if (SalesReturnsItems != null && SalesReturnsItems.Count > 0)
                            return true;
                        else
                        {
                            var PurchaseItem = _context.PurchaseItem.Where(x => x.ItemId == Id && x.IsDeleted == false).ToList();
                            if (PurchaseItem != null && PurchaseItem.Count > 0)
                                return true;
                            else
                            {
                                var PurchasesReturnsItems = _context.PurchasesReturns_Items.Where(x => x.ItemId == Id && x.IsDeleted == false).ToList();
                                if (PurchasesReturnsItems != null && PurchasesReturnsItems.Count > 0)
                                    return true;
                                else
                                {
                                    var ExecutionWithDrawals_Items = _context.ExecutionWithDrawals_Items.Where(x => x.ItemId == Id && x.IsDeleted == false).ToList();
                                    if (ExecutionWithDrawals_Items != null && ExecutionWithDrawals_Items.Count > 0)
                                        return true;
                                    else
                                    {
                                        var TempAdditionVoucherItem = _context.TempAdditionVoucherItem.Where(x => x.ItemId == Id && x.IsDeleted == false).ToList();
                                        if (TempAdditionVoucherItem != null && TempAdditionVoucherItem.Count > 0)
                                            return true;
                                        else
                                        {
                                            var AdditionVoucherItem = _context.AdditionVoucherItem.Where(x => x.ItemId == Id && x.IsDeleted == false).ToList();
                                            if (AdditionVoucherItem != null && AdditionVoucherItem.Count > 0)
                                                return true;
                                            else
                                            {
                                                var TempPaymentVoucherItem = _context.TempPaymentVoucherItem.Where(x => x.ItemId == Id && x.IsDeleted == false).ToList();
                                                if (TempPaymentVoucherItem != null && TempPaymentVoucherItem.Count > 0)
                                                    return true;
                                                else
                                                {
                                                    var PaymentVoucherItem = _context.PaymentVoucherItem.Where(x => x.ItemId == Id && x.IsDeleted == false).ToList();
                                                    if (PaymentVoucherItem != null && PaymentVoucherItem.Count > 0)
                                                        return true;
                                                    else
                                                    {
                                                        var ItemConversionItems = _context.ItemConversion_Items.Where(x => x.ItemIdFrom == Id || x.ItemIdTo == Id && x.IsDeleted == false).ToList();
                                                        if (ItemConversionItems != null && ItemConversionItems.Count > 0)
                                                            return true;
                                                        else
                                                        {
                                                            var TransferRequestItem = _context.TransferRequestItem.Where(x => x.ItemId == Id  && x.IsDeleted == false).ToList();
                                                            if (TransferRequestItem != null && TransferRequestItem.Count > 0)
                                                                return true;
                                                            else
                                                            {
                                                                var TransferOrderDetails = _context.TransferOrderDetails.Where(x => x.ItemId == Id && x.IsDeleted == false).ToList();
                                                                if (TransferOrderDetails != null && TransferOrderDetails.Count > 0)
                                                                    return true;
                                                                else
                                                                    return false;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                  
                }
     
            }
        }
        public bool IsExistName(string name, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.Name == name & g.IsDeleted == false);
            }
            else
            {
                return dbSet.Any(g => g.Name == name & g.ID != id & g.IsDeleted == false);
            }
        }

        public bool IsExistNationalParCode(string NationalParCode, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.National_ParCode == NationalParCode & g.IsDeleted == false);
            }
            else
            {
                return dbSet.Any(g => g.National_ParCode == NationalParCode & g.ID != id & g.IsDeleted == false);
            }
        }

        public bool IsExistInternationalParCode(string InternationalParCode, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.Interational_ParCode == InternationalParCode & g.IsDeleted == false);
            }
            else
            {
                return dbSet.Any(g => g.Interational_ParCode == InternationalParCode & g.ID != id & g.IsDeleted == false);
            }
        }

        public bool IsExistBoxParCode(string BoxParCode, int id)
        {
            if (id == 0)
            {

                return dbSet.Any(g => g.Box_ParCode == BoxParCode & g.IsDeleted == false);
            }
            else
            {
                return dbSet.Any(g => g.Box_ParCode == BoxParCode & g.ID != id & g.IsDeleted == false);
            }
        }

        public bool IsExistSupplierParCode(string SupplierParCode, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.Supplier_ParCode == SupplierParCode & g.IsDeleted == false);
            }
            else
            {
                return dbSet.Any(g => g.Supplier_ParCode == SupplierParCode & g.ID != id & g.IsDeleted == false);
            }
        }

        public bool IsExistGuaranteeParCode(string GuaranteeParCode, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.Guarantee_ParCode == GuaranteeParCode & g.IsDeleted == false);
            }
            else
            {
                return dbSet.Any(g => g.Guarantee_ParCode == GuaranteeParCode & g.ID != id & g.IsDeleted == false);
            }
        }

    }
}
