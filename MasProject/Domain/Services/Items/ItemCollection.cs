﻿
using MasProject.Data.DataAccess;
using MasProject.Data.Models.Items;
using MasProject.Domain.ViewModel.Items;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MasProject.Domain.Services.Items
{
    public class ItemCollectionServices : Repository<ItemCollection>
    {
        public ItemCollectionServices(DBContext context) : base(context)
        {
        }
        public IEnumerable<ItemCollectionVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new ItemCollectionVM
            {
                ID = s.ID,
                Name = s.Name,
                IsDeleted = s.IsDeleted,
                arrangement = s.arrangement,
            }).OrderBy(s => s.arrangement);
        }

        public ItemCollectionVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new ItemCollectionVM
            {
                ID = s.ID,
                Name = s.Name,
                IsDeleted = s.IsDeleted,
                arrangement = s.arrangement,
            }).FirstOrDefault();
        }

        public ItemCollection Add(ItemCollectionVM model)
        {
            var newModel = new ItemCollection
            {
                ID = model.ID,
                Name = model.Name,
                CreatedDate = DateTime.Today,
                IsDeleted = false,
                arrangement = model.arrangement,
            };
            Insert(newModel);
            return newModel;
        }

        public ItemCollection Edit(ItemCollectionVM model)
        {
            var newModel = new ItemCollection
            {
                ID = model.ID,
                Name = model.Name,
                ModifiedDate = DateTime.Today,
                arrangement = model.arrangement,
            };
            Update(newModel);
            return newModel;
        }

        public void Delete(int id)
        {

            ItemCollection edited = dbSet.FirstOrDefault(p => p.ID == id & p.IsDeleted == false);
            Delete(edited);
        }

        public bool IsExistArrangement(int arrangement, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.arrangement == arrangement & g.IsDeleted == false);
            }
            else
            {
                return dbSet.Any(g => g.arrangement == arrangement & g.ID != id & g.IsDeleted == false);
            }
        }

       
    }
}
