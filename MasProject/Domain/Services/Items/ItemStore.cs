﻿
using MasProject.Data.DataAccess;
using MasProject.Data.Models.Items;
using MasProject.Data.Models.Hierarchy;
using MasProject.Domain.ViewModel.Items;
using MasProject.Domain.ViewModel.Hierarchy;
using MasProject.Domain.Services.Hierarchy;
using System;
using System.Collections.Generic;
using System.Linq;
using MasProject.Domain.ViewModel;

namespace MasProject.Domain.Services.Items
{
    public class ItemStoreServices : Repository<Item_Store>
    {
        public DBContext Context { get; private set; }
        public ItemStoreServices(DBContext context) : base(context)
        {
            Context = context;

        }
        public IEnumerable<Item_StoreVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new Item_StoreVM
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                ItemName = s.Item.Name,
                StoreName = s.Store.Name,
                Quantity = s.Quantity,
                SheilfNo = s.SheilfNo,
                OrderLimit = s.OrderLimit,
                Note = s.Note
            }).OrderByDescending(s => s.ID);
        }
        public List<Item_StoreVM> GetItemStore(int StoreId)
        {
            return dbSet.Where(g =>g.StoreId == StoreId && g.IsDeleted == false).Select(s => new Item_StoreVM
            {
                ItemId = s.ItemId,
                SheilfNo = s.SheilfNo
            }).ToList();
        }
        public List<Item_StoreVM> GetByItemID(int ItemID)
        {
            return dbSet.Where(x => x.IsDeleted == false && x.ItemId == ItemID).Select(s => new Item_StoreVM
            {
                ID = s.ID,
                ItemId = s.ItemId,
                StoreId = s.StoreId,
                SheilfNo = s.SheilfNo,
                Quantity = s.Quantity,
                OrderLimit = s.OrderLimit,
                Note = s.Note,
                Name = s.Item.Name,
                Value = s.Value,

            }).ToList();
        }

        public Item_StoreVM GetByItemAndStoreID(int ItemID, int? StoreID)
        {
            if (StoreID == 0 || StoreID == null)
            {
                return dbSet.Where(x => x.IsDeleted == false && x.ItemId == ItemID && x.StoreId == StoreID).Select(s => new Item_StoreVM
                {
                    ID = s.ID,
                    ItemId = s.ItemId,
                    StoreId = s.StoreId,
                    SheilfNo = s.SheilfNo,
                    Quantity = s.Quantity,
                    OrderLimit = s.OrderLimit,
                    Note = s.Note,
                    Name = s.Item.Name,
                    Value = s.Value,

                }).FirstOrDefault();
            }
            else
            {
                return dbSet.Where(x => x.IsDeleted == false && x.ItemId == ItemID && x.StoreId == StoreID).Select(s => new Item_StoreVM
                {
                    ID = s.ID,
                    ItemId = s.ItemId,
                    StoreId = s.StoreId,
                    SheilfNo = s.SheilfNo,
                    Quantity = s.Quantity,
                    OrderLimit = s.OrderLimit,
                    Note = s.Note,
                    Name = s.Item.Name,
                    Value = s.Value,

                }).FirstOrDefault();
            }

        }
        public LookupItem GetBranchAndMianStoreQuantity(int ItemId, int BranchId)
        {
            LookupItem Item = new LookupItem();
            StoreServices StoreServices = new StoreServices(Context);
            List<StoreVM> StoreList = StoreServices.GetStoreListByBranchID(BranchId);
            double Quantity = 0.0;
            foreach (var store in StoreList)
            {
                Quantity += GetItemQuantityByStoreID(ItemId, store.ID);
            }
            Item.BranchToQuantity = Quantity;
            Item.MainStoreQuantity = GetMainStoreItemQuantity(ItemId);
            return Item;
        }
        public double GetItemQuantityByBranch(int ItemId, int BranchId)
        {
            LookupItem Item = new LookupItem();
            StoreServices StoreServices = new StoreServices(Context);
            List<StoreVM> StoreList = StoreServices.GetStoreListByBranchID(BranchId);
            double Quantity = 0.0;
            foreach (var store in StoreList)
            {
                Quantity += GetItemQuantityByStoreID(ItemId, store.ID);
            }
            return Quantity;
        }
        public double GetMainStoreItemQuantity(int ItemId)
        {
            StoreServices StoreServices = new StoreServices(Context);
            List<StoreVM> StoreList = StoreServices.GetMainStore();
            double Quantity = 0.0;
            foreach (var store in StoreList)
            {
                Quantity += GetItemQuantityByStoreID(ItemId, store.ID);
            }
            return Quantity;
        }
        public int GetItemQuantityByStoreID(int ItemID, int StoreID)
        {
            var itemStore = dbSet.FirstOrDefault(x => x.IsDeleted == false && x.ItemId == ItemID && x.StoreId == StoreID);
            return (itemStore != null) ? (itemStore.Quantity.HasValue ? itemStore.Quantity.Value : 0) : 0;

        }
        public Item_Store Add(Item_StoreVM itmstore)
        {
            Item_Store model = new Item_Store
            {
                ID = itmstore.ID,
                Name = itmstore.Name,
                ItemId = itmstore.ItemId,
                StoreId = itmstore.StoreId,
                Quantity = itmstore.Quantity,
                SheilfNo = itmstore.SheilfNo,
                OrderLimit = itmstore.OrderLimit,
                Note = itmstore.Note,
                Value = itmstore.Value
            };
            Insert(model);
            return model;
        }

        public Item_Store Edit(Item_StoreVM itmstore)
        {

            Item_Store model = new Item_Store
            {
                ID = itmstore.ID,
                Name = itmstore.Name,
                ItemId = itmstore.ItemId,
                StoreId = itmstore.StoreId,
                Quantity = itmstore.Quantity,
                SheilfNo = itmstore.SheilfNo,
                OrderLimit = itmstore.OrderLimit,
                Note = itmstore.Note,
                Value = itmstore.Value,
                IsDeleted = false
            };

            Update(model);
            return model;
        }
        public void Delete(int id)
        {

            Item_Store edited = dbSet.FirstOrDefault(p => p.ID == id & p.IsDeleted == false);
            Delete(edited);
        }
    }

}
