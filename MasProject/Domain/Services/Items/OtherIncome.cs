﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Items;
using MasProject.Domain.ViewModel.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasProject.Domain.Services.Items
{
   public class OtherIncomeService : Repository<OtherIncome>
    {
        private DBContext _context;

        public OtherIncomeService(DBContext context) : base(context)
        {
            _context = context;

        }
        public IEnumerable<OtherIncomeVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new OtherIncomeVM
            {
                ID = s.ID,
                Name = s.Name,
                IsDeleted = s.IsDeleted,
            }).OrderByDescending(s => s.ID);
        }

        public OtherIncomeVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new OtherIncomeVM
            {
                ID = s.ID,
                Name = s.Name,
                IsDeleted = s.IsDeleted
            }).FirstOrDefault();
        }

        public OtherIncome Add(OtherIncomeVM model)
        {
            var newModel = new OtherIncome
            {
                ID = model.ID,
                Name = model.Name,
                CreatedDate = DateTime.Today,
                IsDeleted = false
            };
            Insert(newModel);
            return newModel;
        }

        public OtherIncome Edit(OtherIncomeVM model)
        {
            var newModel = new OtherIncome
            {
                ID = model.ID,
                Name = model.Name,
                ModifiedDate = DateTime.Today,
            };
            Update(newModel);
            return newModel;
        }

        public void Delete(int id)
        {

            OtherIncome edited = dbSet.FirstOrDefault(p => p.ID == id & p.IsDeleted == false);
            Delete(edited);
        }

        public bool IsExistName(string  OtherIncomeName, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.Name == OtherIncomeName & g.IsDeleted == false);
            }
            else
            {
                return dbSet.Any(g => g.Name == OtherIncomeName & g.ID != id & g.IsDeleted == false);
            }
        }
        public bool IsOtherIncomeUsed(int Id)
        {
            var ExecutiveOtherIncome = _context.ExecutiveOtherIncome.Where(x => x.IncomeId == Id && x.IsDeleted == false).ToList();
            if (ExecutiveOtherIncome != null && ExecutiveOtherIncome.Count > 0)
                return true;
            else
            {
                var PurchaseOtherIncome = _context.PurchaseOtherIncome.Where(x => x.IncomeId == Id && x.IsDeleted == false).ToList();
                if (PurchaseOtherIncome != null && PurchaseOtherIncome.Count > 0)
                    return true;
                else
                {
                    var PurchaseReturns_OtherIncome = _context.PurchaseReturns_OtherIncome.Where(x => x.IncomeId == Id && x.IsDeleted == false).ToList();
                    if (PurchaseReturns_OtherIncome != null && PurchaseReturns_OtherIncome.Count > 0)
                        return true;
                    else
                        return false;
                }
            }
        }

    }
}
