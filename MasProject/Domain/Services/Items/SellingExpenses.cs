﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Items;
using MasProject.Domain.ViewModel.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasProject.Domain.Services.Items
{
    public class SellingExpensesService : Repository<SellingExpenses>
    {
        private DBContext _context;
        public SellingExpensesService(DBContext context) : base(context)
        {
            _context = context;
        }
    public IEnumerable<SellingExpensesVM> GetAll()
    {
        return dbSet.Where(g => g.IsDeleted == false).Select(s => new SellingExpensesVM
        {
            ID = s.ID,
            Name = s.Name,
            IsDeleted = s.IsDeleted,
        }).OrderByDescending(s => s.ID);
    }

    public SellingExpensesVM Get(int id)
    {
        return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new SellingExpensesVM
        {
            ID = s.ID,
            Name = s.Name,
            IsDeleted = s.IsDeleted
        }).FirstOrDefault();
    }

    public SellingExpenses Add(SellingExpensesVM model)
    {
        var newModel = new SellingExpenses
        {
            ID = model.ID,
            Name = model.Name,
            CreatedDate = DateTime.Today,
            IsDeleted = false
        };
        Insert(newModel);
        return newModel;
    }

    public SellingExpenses Edit(SellingExpensesVM model)
    {
        var newModel = new SellingExpenses
        {
            ID = model.ID,
            Name = model.Name,
            ModifiedDate = DateTime.Today,
        };
        Update(newModel);
        return newModel;
    }

    public void Delete(int id)
    {

        SellingExpenses edited = dbSet.FirstOrDefault(p => p.ID == id & p.IsDeleted == false);
            Delete(edited);
        }

    public bool IsExistName(string SellingExpensesName, int id)
    {
        if (id == 0)
        {
            return dbSet.Any(g => g.Name == SellingExpensesName & g.IsDeleted == false);
        }
        else
        {
            return dbSet.Any(g => g.Name == SellingExpensesName & g.ID != id & g.IsDeleted == false);
        }
    }
    public bool IsSellingExpensesUsed(int Id)
    {
        var SalesSellExpenses = _context.SalesSellExpenses.Where(x => x.ExpensesId == Id && x.IsDeleted == false).ToList();
        if (SalesSellExpenses != null && SalesSellExpenses.Count > 0)
            return true;
        else
            {
                var MaintanceSellExpenses = _context.MaintanceSellExpenses.Where(x => x.ExpensesId == Id && x.IsDeleted == false).ToList();
                if (MaintanceSellExpenses != null && MaintanceSellExpenses.Count > 0)
                    return true;
                else
                {
                    var ExecutiveSellExpenses = _context.ExecutiveSellExpenses.Where(x => x.ExpensesId == Id && x.IsDeleted == false).ToList();
                    if (ExecutiveSellExpenses != null && ExecutiveSellExpenses.Count > 0)
                        return true;
                    else
                    {
                        var SalesReturnsExpenses = _context.SalesReturnsExpenses.Where(x => x.ExpensesId == Id && x.IsDeleted == false).ToList();
                        if (SalesReturnsExpenses != null && SalesReturnsExpenses.Count > 0)
                            return true;
                        else
                        return false;
                    }
                }
            }
        }
    }
}
