﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Items;
using MasProject.Domain.ViewModel.Items;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MasProject.Domain.Services.Items
{
    public class ItemTypeServices : Repository<ItemType>
    {
        public ItemTypeServices(DBContext context) : base(context)
        {
        }

        public IEnumerable<ItemTypeVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new ItemTypeVM
            {
                ID = s.ID,
                Name = s.Name,
                IsDeleted = s.IsDeleted,
                Item_GetType = s.Item_GetType,
                arrangement = s.arrangement,
            }).OrderBy(s => s.arrangement);


        }

        public ItemTypeVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new ItemTypeVM
            {
                ID = s.ID,
                Name = s.Name,
                IsDeleted = s.IsDeleted,
                Item_GetType = s.Item_GetType,
                arrangement = s.arrangement,
            }).FirstOrDefault();
        }

        public ItemType Add(ItemTypeVM model)
        {
            var newModel = new ItemType
            {
                ID = model.ID,
                Name = model.Name,
                CreatedDate = DateTime.Today,
                IsDeleted = false,
                Item_GetType = model.Item_GetType,
                arrangement = model.arrangement,
            };
            Insert(newModel);
            return newModel;
        }

        public ItemType Edit(ItemTypeVM model)
        {
            var newModel = new ItemType
            {
                ID = model.ID,
                Name = model.Name,
                ModifiedDate = DateTime.Today,
                Item_GetType = model.Item_GetType,
                arrangement = model.arrangement,
            };
            Update(newModel);
            return newModel;
        }

        public void Delete(int id)
        {

            ItemType edited = dbSet.FirstOrDefault(p => p.ID == id & p.IsDeleted == false);
            Delete(edited);
        }

        public bool IsExistArrangement(int arrangement, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.arrangement == arrangement & g.IsDeleted == false);
            }
            else
            {
                return dbSet.Any(g => g.arrangement == arrangement & g.ID != id & g.IsDeleted == false);
            }
        }

    }
}
