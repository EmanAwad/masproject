﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace MasProject.Domain.Services.Items
{
  public static  class DataUrlServices
    {
        public static string ToDataUrl(this MemoryStream data , string format)
        {
            var span = new Span<byte>(data.GetBuffer()).Slice(0, (int)data.Length);
            return $"data:{format};base64,{Convert.ToBase64String(span)}";
        }

        public static byte[] ToBytes(string url)
        {
            var CommaPas = url.IndexOf(',');
            if (CommaPas >= 0)
            {
                var base64 = url.Substring(CommaPas + 1);
                return Convert.FromBase64String(base64);
            }
            return null;
        }
    }
}
