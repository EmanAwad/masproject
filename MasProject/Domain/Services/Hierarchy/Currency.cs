﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Hierarchy;
using MasProject.Domain.ViewModel.Hierarchy;
using System;
using System.Collections.Generic;
using System.Linq;


namespace MasProject.Domain.Services.Hierarchy
{
    public class CurrencyServices : Repository<Currency>
    {
        public CurrencyServices(DBContext context) : base(context)
        {
        }
        public bool IsExistArrangement(int arrangement, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.arrangement == arrangement & g.IsDeleted == false);
            }
            else
            {
                return dbSet.Any(g => g.arrangement == arrangement & g.ID != id & g.IsDeleted == false);
            }
        }
        public IEnumerable<CurrencyVM> GetAll()
        {//
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new CurrencyVM
            {
                ID = s.ID,
                Name = s.Name,
                IsDeleted = s.IsDeleted,
                Ratio = s.Ratio,
                Note=s.Note,
                CreatedDate=s.CreatedDate,
                arrangement=s.arrangement,
            }).OrderBy(s => s.arrangement);
        }
        public CurrencyVM Get(int id)
        {//
            return dbSet.Where(x => x.ID == id&& x.IsDeleted == false).Select(s => new CurrencyVM
            {
                ID = s.ID,
                Name = s.Name,
                IsDeleted = s.IsDeleted,
                Ratio = s.Ratio,
                Note = s.Note,
                arrangement = s.arrangement,
            }).FirstOrDefault();
        }
        public double GetCurrencyRatioByID(int id)
        {
            
            return dbSet.Where(x => x.ID == id && !x.IsDeleted).FirstOrDefault().Ratio ?? 0 ;
        }
        public Currency Add(CurrencyVM model)
        {
            var newModel = new Currency
            {
                // ID = model.ID,
                Name = model.Name,
                CreatedDate = DateTime.Today,
                IsDeleted = false,
                Ratio = model.Ratio,
                Note = model.Note,
                arrangement = model.arrangement,
            };
            Insert(newModel);
            return newModel;
        }
        public Currency Edit(CurrencyVM model)
        {
            var newModel = new Currency
            {
                ID = model.ID,
                Name = model.Name,
                ModifiedDate = DateTime.Today,
                Ratio = model.Ratio,
                Note = model.Note,
                arrangement = model.arrangement,
            };
            Update(newModel);
            return newModel;
        }
        public void Delete(int id)
        {
            Currency edited = dbSet.FirstOrDefault(p => p.ID == id);
            Delete(edited);
        }
    }
}
