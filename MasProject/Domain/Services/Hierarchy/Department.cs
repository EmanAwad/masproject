﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Hierarchy;
using MasProject.Domain.ViewModel.Hierarchy;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MasProject.Domain.Services.Hierarchy
{
    public class DepartmentServices : Repository<Department>
    {
        public DepartmentServices(DBContext context) : base(context)
        {

        }
        public bool IsExistArrangement(int arrangement, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.arrangement == arrangement & g.IsDeleted == false);
            }
            else
            {
                return dbSet.Any(g => g.arrangement == arrangement & g.IsDeleted == false & g.ID != id);
            }
        }
        public IEnumerable<DepartmentVM> GetAll()
        {//
            try
            {
                return dbSet.Where(g => g.IsDeleted == false).Select(s => new DepartmentVM
                {
                    ID = s.ID,
                    Name = s.Name,
                    IsDeleted = s.IsDeleted,
                    BranchID = (int)s.BranchId,
                    Note = s.Note,
                    arrangement = s.arrangement,
                    CreatedDate = s.CreatedDate,
                }).OrderBy(s => s.arrangement);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return null;
            }
        }
        public DepartmentVM Get(int id)
        {// 
            return dbSet.Where(x => x.ID == id&& x.IsDeleted == false).Select(s => new DepartmentVM
            {
                ID = s.ID,
                Name = s.Name,
                IsDeleted = s.IsDeleted,
                BranchID = s.BranchId,
                Note = s.Note,
                arrangement = s.arrangement,

            }).FirstOrDefault();
        }
        public Department Add(DepartmentVM model)
        {
            var newModel = new Department
            {
                ID = model.ID,
                Name = model.Name,
                CreatedDate = DateTime.Today,
                IsDeleted = false,
                BranchId = model.BranchID,
                Note = model.Note,
                arrangement = model.arrangement,

            };
            Insert(newModel);
            return newModel;
        }
        public Department Edit(DepartmentVM model)
        {
            var newModel = new Department
            {
                ID = model.ID,
                Name = model.Name,
                ModifiedDate = DateTime.Today,
                BranchId = model.BranchID,
                Note = model.Note,
                arrangement = model.arrangement,

            };
            Update(newModel);
            return newModel;
        }
        public void Delete(int id)
        {
            Department edited = dbSet.FirstOrDefault(p => p.ID == id);
            Delete(edited);
        }
    }
}
