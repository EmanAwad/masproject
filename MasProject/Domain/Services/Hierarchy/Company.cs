﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Hierarchy;
using MasProject.Domain.ViewModel.Hierarchy;
using System;
using System.Collections.Generic;
using System.Linq;
namespace MasProject.Domain.Services.Hierarchy
{
    public class CompanyServices : Repository<Company>
    {
        public CompanyServices(DBContext context) : base(context)
        {
        }
        public bool IsExistArrangement(int arrangement, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.arrangement == arrangement);
            }
            else
            {
                return dbSet.Any(g => g.arrangement == arrangement & g.ID != id);
            }
        }
        public IEnumerable<CompanyVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new CompanyVM
            {
                ID = s.ID,
                Name = s.Name,
                IsDeleted = s.IsDeleted,
                Phone=s.Phone,
                Note=s.Note,
                Address=s.Address,
                CommerceNumber=s.CommerceNumber,
                Logo=s.Logo,
                TaxNumber=s.TaxNumber,
                Website=s.Website,
                Mobile = s.Mobile,
                arrangement = s.arrangement,
                CreatedDate=s.CreatedDate,
            }).OrderByDescending(s => s.CreatedDate);
        }
        public CompanyVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new CompanyVM
            {
                ID = s.ID,
                Name = s.Name,
                IsDeleted = s.IsDeleted,
                Phone = s.Phone,
                Note = s.Note,
                Address = s.Address,
                CommerceNumber = s.CommerceNumber,
                Logo = s.Logo,
                TaxNumber = s.TaxNumber,
                Website = s.Website,
                Mobile = s.Mobile,
                arrangement = s.arrangement,

            }).FirstOrDefault();
        }

        public Company Add(CompanyVM model)
        {
           
            var newModel = new Company
            {
                ID = model.ID,
                Name = model.Name,
                CreatedDate = DateTime.Today,
                IsDeleted = false,
                Phone = model.Phone,
                Note = model.Note,
                Address = model.Address,
                CommerceNumber = (int)model.CommerceNumber,
                Logo = model.Logo,
                TaxNumber = (int)model.TaxNumber,
                Website = model.Website,
                Mobile = model.Mobile,
                arrangement = (int)model.arrangement,
            };
          
            Insert(newModel);
            return newModel;
        }
        public Company Edit(CompanyVM model)
        {
            var newModel = new Company
            {
                ID = model.ID,
                Name = model.Name,
                ModifiedDate = DateTime.Today,
                Phone = model.Phone,
                Note = model.Note,
                Address = model.Address,
                CommerceNumber = (int)model.CommerceNumber,
                Logo = model.Logo,
                TaxNumber = (int)model.TaxNumber,
                Website = model.Website,
                Mobile = model.Mobile,
                arrangement = (int)model.arrangement,
            };
            Update(newModel);
            return newModel;
        }
        public void Delete(int id)
        {
            Company edited = dbSet.FirstOrDefault(p => p.ID == id);
            edited.IsDeleted = true;
            Update(edited);
        }
    }
}

