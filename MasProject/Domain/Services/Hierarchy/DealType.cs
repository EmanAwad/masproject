﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Hierarchy;
using MasProject.Domain.ViewModel.Hierarchy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MasProject.Domain.ViewModel;
using static MasProject.Domain.Enums.Enums;
namespace MasProject.Domain.Services.Hierarchy
{
    public class DealTypeService : Repository<DealType>
    {
        public DealTypeService(DBContext context) : base(context)
        {
        }
        public bool IsExistArrangement(int arrangement, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.arrangement == arrangement & g.IsDeleted == false);
            }
            else
            {
                return dbSet.Any(g => g.arrangement == arrangement & g.ID != id & g.IsDeleted == false);
            }
        }
        public IEnumerable<DealTypeVM> GetAll()
        {//
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new DealTypeVM
            {
                ID = s.ID,
                Name = s.Name,
                PriceType=s.PriceType,
                IsDeleted = s.IsDeleted,
                Note = s.Note,
                CreatedDate=s.CreatedDate,
                arrangement = s.arrangement,
            }).OrderBy(s => s.arrangement);
        }
        public List<LookupKeyValueVM> GetPriceTypes()
        {
            List<LookupKeyValueVM> PricetypeList = new List<LookupKeyValueVM>();
            LookupKeyValueVM pricetype = new LookupKeyValueVM();
            foreach (var type in Enum.GetValues(typeof(EnPriceTypes)))
            {
                pricetype = new LookupKeyValueVM {
                    ID = (int)type,
                    Name = type.ToString()
                };
                PricetypeList.Add(pricetype);

            }
            return PricetypeList;
        }
        public DealTypeVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id&& x.IsDeleted == false).Select(s => new DealTypeVM
            {
                ID = s.ID,
                Name = s.Name,
                PriceType = s.PriceType,
                IsDeleted = s.IsDeleted,
                Note = s.Note,
                arrangement = s.arrangement,
            }).FirstOrDefault();
        }
        public DealType Add(DealTypeVM model)
        {
            var newModel = new DealType
            {
                // ID = model.ID,
                Name = model.Name,
                PriceType = model.PriceType,
                CreatedDate = DateTime.Today,
                IsDeleted = false,
                Note = model.Note,
                arrangement = model.arrangement,
            };
            Insert(newModel);
            return newModel;
        }
        public DealType Edit(DealTypeVM model)
        {
            var newModel = new DealType
            {
                ID = model.ID,
                PriceType= model.PriceType,
                Name = model.Name,
                ModifiedDate = DateTime.Today,
                Note = model.Note,
                arrangement = model.arrangement,
            };
            Update(newModel);
            return newModel;
        }
        public void Delete(int id)
        {
            DealType edited = dbSet.FirstOrDefault(p => p.ID == id);
            Delete(edited);
        }
    }
}
