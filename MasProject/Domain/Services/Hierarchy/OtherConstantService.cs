﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Hierarchy;
using MasProject.Domain.ViewModel.Hierarchy;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MasProject.Domain.Services.Hierarchy
{
    public class OtherConstantService : Repository<OtherConstant>
    {
        public OtherConstantService(DBContext context) : base(context)
        {
        }

        public IEnumerable<OtherConstantVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new OtherConstantVM
            {
                ID = s.ID,
                Name = s.Name,
                IsDeleted = s.IsDeleted,
                Ratio = s.Ratio,
                arrangement = s.arrangement,
            }).OrderBy(s => s.arrangement);
        }
        public OtherConstantVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new OtherConstantVM
            {
                ID = s.ID,
                Name = s.Name,
                IsDeleted = s.IsDeleted,
                Ratio = s.Ratio,
                arrangement = s.arrangement,
            }).FirstOrDefault();
        }
        public double GetOtherConstantRatioByID(int id)
        {
            return dbSet.Where(x => x.ID == id && !x.IsDeleted).FirstOrDefault().Ratio ?? 0;
        }
        public OtherConstant Add(OtherConstantVM model)
        {
            var newModel = new OtherConstant
            {
                Name = model.Name,
                CreatedDate = DateTime.Today,
                IsDeleted = false,
                Ratio = model.Ratio,
                arrangement = model.arrangement,
            };
            Insert(newModel);
            return newModel;
        }
        public OtherConstant Edit(OtherConstantVM model)
        {
            var newModel = new OtherConstant
            {
                ID = model.ID,
                Name = model.Name,
                ModifiedDate = DateTime.Today,
                arrangement = model.arrangement,
                Ratio = model.Ratio
            };
            Update(newModel);
            return newModel;
        }
        public void Delete(int id)
        {
            OtherConstant edited = dbSet.FirstOrDefault(p => p.ID == id);
            Delete(edited);
        }

        public bool IsExistArrangement(int arrangement, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.arrangement == arrangement & g.IsDeleted == false);
            }
            else
            {
                return dbSet.Any(g => g.arrangement == arrangement & g.IsDeleted == false & g.ID != id);
            }
        }
    }
}