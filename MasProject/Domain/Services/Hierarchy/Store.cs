﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Hierarchy;
using MasProject.Domain.ViewModel;
using MasProject.Domain.ViewModel.Hierarchy;
using System;
using System.Collections.Generic;
using System.Linq;


namespace MasProject.Domain.Services.Hierarchy
{

    public class StoreServices : Repository<Store>
    {
        private DBContext _context;
        public StoreServices(DBContext context) : base(context)
        {
            _context = context;
        }
        public bool IsExistArrangement(int arrangement, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.arrangement == arrangement & g.IsDeleted == false);
            }
            else
            {
                return dbSet.Any(g => g.arrangement == arrangement & g.ID != id & g.IsDeleted == false);
            }
        }
        public IEnumerable<LookupKeyValueVM> GetStores()
        {
            return dbSet.Where(x => x.IsDeleted == false ).Select(s => new LookupKeyValueVM
            {
                ID = s.ID,
                Name = s.Name,
                Type = 4
            }).OrderBy(s => s.ID);
        }
        public IEnumerable<StoreVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new StoreVM
            {
                ID = s.ID,
                Name = s.Name,
                IsDeleted = s.IsDeleted,
                EmployeeID = (int)s.EmployeeID,
                EmployeeName = s.Employee.UserName,
                //   EmployeeName = _Context.Employee.Where(c => c.IsDeleted == false && c.ID == s.EmployeeID).FirstOrDefault(),
                Note = s.Note,
                Phone = s.Phone,
                BranchID = s.BranchId,
                Mobile = s.Mobile,
                Address = s.Address,
                arrangement = s.arrangement,
                CreatedDate = s.CreatedDate,
                MainStore = s.MainStore,

            }).OrderBy(s => s.arrangement);
        }
        public StoreVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new StoreVM
            {
                ID = s.ID,
                Name = s.Name,
                IsDeleted = s.IsDeleted,
                EmployeeID = (int)s.EmployeeID,
                EmployeeName = s.Employee.Name,
                Note = s.Note,
                Phone = s.Phone,
                BranchID = s.BranchId,
                Address = s.Address,
                Mobile = s.Mobile,
                arrangement = s.arrangement,
                MainStore = s.MainStore
            }).FirstOrDefault();
        }
        public int GetSoreByBranch(int BranchId)
        {
            return dbSet.FirstOrDefault(x => x.BranchId == BranchId && x.MainStore == true && x.IsDeleted == false).ID;
        }
        public List<StoreVM> GetMainStore()
        {
            return dbSet.Where(x => x.IsDeleted == false && x.MainStore).Select(s => new StoreVM
            {
                ID = s.ID,
                Name = s.Name
            }).ToList();
        }
        public List<StoreVM> GetStoreListByBranchID(int BranchID)
        {
            return dbSet.Where(x => x.BranchId == BranchID && x.IsDeleted == false && !x.MainStore).Select(s => new StoreVM
            {
                ID = s.ID,
                Name = s.Name
            }).ToList();
        }
        public Store Add(StoreVM model)
        {
            var newModel = new Store
            {
                ID = model.ID,
                Name = model.Name,
                CreatedDate = DateTime.Today,
                IsDeleted = false,
                EmployeeID = model.EmployeeID,
                Note = model.Note,
                Phone = model.Phone,
                BranchId = model.BranchID,
                Address = model.Address,
                Mobile = model.Mobile,
                arrangement = model.arrangement == null ? 0 : (int)model.arrangement,
                MainStore = model.MainStore
            };
            Insert(newModel);
            return newModel;
        }
        public Store Edit(StoreVM model)
        {
            var newModel = new Store
            {
                ID = model.ID,
                Name = model.Name,
                ModifiedDate = DateTime.Today,
                EmployeeID = model.EmployeeID,
                Note = model.Note,
                Phone = model.Phone,
                BranchId = model.BranchID,
                Address = model.Address,
                Mobile = model.Mobile,
                arrangement = model.arrangement,
                MainStore = model.MainStore
            };
            Update(newModel);
            return newModel;
        }
        public void Delete(int id)
        {
            Store edited = dbSet.FirstOrDefault(p => p.ID == id);
            Delete(edited);
        }
        public bool IsStoreUsed(int Id)
        {
            var Items = _context.Items.Where(x => x.StoreId == Id && x.IsDeleted == false).ToList();
            if (Items != null && Items.Count > 0)
                return true;
            else
            {
                var ItemStore = _context.Item_Store.Where(x => x.StoreId == Id && x.IsDeleted == false).ToList();
                if (ItemStore != null && ItemStore.Count > 0)
                    return true;
                else
                {
                    var ItemOB = _context.ItemsOpeningBalance.Where(x => x.StoreId == Id && x.IsDeleted == false).ToList();
                    if (ItemOB != null && ItemOB.Count > 0)
                        return true;
                    else
                    {
                        var SalesBill = _context.SalesBills.Where(x => x.StoreId == Id && x.IsDeleted == false).ToList();
                        if (SalesBill != null && SalesBill.Count > 0)
                            return true;
                        else
                        {
                            var SalesBillStore = _context.SalesBillStores.Where(x => x.StoreId == Id && x.IsDeleted == false).ToList();
                            if (SalesBillStore != null && SalesBillStore.Count > 0)
                                return true;
                            else
                            {
                                var SalesBillItems = _context.SalesItems.Where(x => x.StoreId == Id && x.IsDeleted == false).ToList();
                                if (SalesBillItems != null && SalesBillItems.Count > 0)
                                    return true;
                                else
                                {
                                    var ExecutiveBill = _context.ExecutiveBills.Where(x => x.StoreId == Id && x.IsDeleted == false).ToList();
                                    if (ExecutiveBill != null && ExecutiveBill.Count > 0)
                                        return true;
                                    else
                                    {
                                        var ExecutiveBillItems = _context.ExecutiveItems.Where(x => x.StoreId == Id && x.IsDeleted == false).ToList();
                                        if (ExecutiveBillItems != null && ExecutiveBillItems.Count > 0)
                                            return true;
                                        else
                                        {
                                            var ExecutiveBillStore = _context.ExecutiveBillStores.Where(x => x.StoreId == Id && x.IsDeleted == false).ToList();
                                            if (ExecutiveBillStore != null && ExecutiveBillStore.Count > 0)
                                                return true;
                                            else
                                            {
                                                var MaintainanceBill = _context.MantienceBills.Where(x => x.StoreId == Id && x.IsDeleted == false).ToList();
                                                if (MaintainanceBill != null && MaintainanceBill.Count > 0)
                                                    return true;
                                                else
                                                {
                                                    var MaintainanceBillStore = _context.MaintanceBillStores.Where(x => x.StoreId == Id && x.IsDeleted == false).ToList();
                                                    if (MaintainanceBillStore != null && MaintainanceBillStore.Count > 0)
                                                        return true;
                                                    else
                                                    {
                                                        var MaintainanceBillItems = _context.MantienceItems.Where(x => x.StoreId == Id && x.IsDeleted == false).ToList();
                                                        if (MaintainanceBillItems != null && MaintainanceBillItems.Count > 0)
                                                            return true;
                                                        else
                                                        {
                                                            var SalesReturns = _context.SalesReturns.Where(x => x.StoreId == Id && x.IsDeleted == false).ToList();
                                                            if (SalesReturns != null && SalesReturns.Count > 0)
                                                                return true;
                                                            else
                                                            {
                                                                var SalesReturnsStore = _context.SalesReturnsBillStores.Where(x => x.StoreId == Id && x.IsDeleted == false).ToList();
                                                                if (SalesReturnsStore != null && SalesReturnsStore.Count > 0)
                                                                    return true;
                                                                else
                                                                {
                                                                    var SalesReturnsItems = _context.SalesReturnsItems.Where(x => x.StoreId == Id && x.IsDeleted == false).ToList();
                                                                    if (SalesReturnsItems != null && SalesReturnsItems.Count > 0)
                                                                        return true;
                                                                    else
                                                                    {
                                                                        var PurchasesBill = _context.PurchaseBill.Where(x => x.StoreId == Id && x.IsDeleted == false).ToList();
                                                                        if (PurchasesBill != null && PurchasesBill.Count > 0)
                                                                            return true;


                                                                        else
                                                                        {
                                                                            var PurchasesReturns = _context.PurchasesReturns_Bill.Where(x => x.StoreId == Id && x.IsDeleted == false).ToList();
                                                                            if (PurchasesReturns != null && PurchasesReturns.Count > 0)
                                                                                return true;

                                                                            else
                                                                            {
                                                                                var ExecutionWithDrawals = _context.ExecutionWithDrawals_Bill.Where(x => x.StoreId == Id && x.IsDeleted == false).ToList();
                                                                                if (ExecutionWithDrawals != null && ExecutionWithDrawals.Count > 0)
                                                                                    return true;
                                                                                else
                                                                                {
                                                                                    var ExecutionWithDrawals_Items = _context.ExecutionWithDrawals_Items.Where(x => x.StoreId == Id && x.IsDeleted == false).ToList();
                                                                                    if (ExecutionWithDrawals_Items != null && ExecutionWithDrawals_Items.Count > 0)
                                                                                        return true;
                                                                                    else
                                                                                    {
                                                                                        var ExecutionWithDrawals_Store = _context.ExecutionWithdrawl_BillStores.Where(x => x.StoreId == Id && x.IsDeleted == false).ToList();
                                                                                        if (ExecutionWithDrawals_Store != null && ExecutionWithDrawals_Store.Count > 0)
                                                                                            return true;
                                                                                        else
                                                                                        {
                                                                                            var TempAdditionVoucher = _context.TempAdditionVoucher.Where(x => x.StoreId == Id && x.IsDeleted == false).ToList();
                                                                                            if (TempAdditionVoucher != null && TempAdditionVoucher.Count > 0)
                                                                                                return true;

                                                                                            else
                                                                                            {
                                                                                                var TempPaymentVoucher = _context.TempPaymentVoucher.Where(x => x.StoreId == Id && x.IsDeleted == false).ToList();
                                                                                                if (TempPaymentVoucher != null && TempPaymentVoucher.Count > 0)
                                                                                                    return true;
                                                                                                else
                                                                                                {
                                                                                                    var ItemConversion = _context.ItemConversion_Bill.Where(x => x.StoreId == Id && x.IsDeleted == false).ToList();
                                                                                                    if (ItemConversion != null && ItemConversion.Count > 0)
                                                                                                        return true;

                                                                                                    else
                                                                                                        return false;
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
