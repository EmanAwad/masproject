﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Hierarchy;
using MasProject.Domain.ViewModel.Hierarchy;
using NetTopologySuite.GeometriesGraph.Index;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MasProject.Domain.Services.Hierarchy
{
    public class TreasuryServices : Repository<Treasury>
    {
        public TreasuryServices(DBContext context) : base(context)
        {
        }

        public IEnumerable<TreasuryVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new TreasuryVM
            {
                ID = s.ID,
                Name = s.Name,
                IsDeleted = s.IsDeleted,
                BranchID = s.BranchId,
                UserId=s.UserId,
                UserName = s.Employee.UserName,
                Note=s.Note,
                CreatedDate = s.CreatedDate,
                Mobile = s.Mobile,
                arrangement = s.arrangement,
                Code = s.Code 
            }).OrderBy(s => s.arrangement);
        }
        public TreasuryVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new TreasuryVM
            {
                ID = s.ID,
                Name = s.Name,
                IsDeleted = s.IsDeleted,
                BranchID = s.BranchId,
               UserId = s.UserId,
                Note = s.Note,
                Mobile = s.Mobile,
                arrangement = s.arrangement,
                Code = s.Code
            }).FirstOrDefault();
        }
        public Treasury Add(TreasuryVM model)
        {
            var newModel = new Treasury
            {
                ID = model.ID,
                Name = model.Name,
                CreatedDate = DateTime.Today,
                IsDeleted = false,
                BranchId = model.BranchID,
                UserId = model.UserId,
                Note = model.Note,
                Mobile = model.Mobile,
                arrangement = model.arrangement,
                Code = model.Code
            };
            Insert(newModel);
            return newModel;
        }
        public Treasury Edit(TreasuryVM model)
        {
            var newModel = new Treasury
            {
                ID = model.ID,
                Name = model.Name,
                ModifiedDate = DateTime.Today,
                BranchId = model.BranchID,
                UserId = model.UserId,
                Note = model.Note,
                Mobile = model.Mobile,
                arrangement = model.arrangement,
                Code = model.Code

            };
            Update(newModel);
            return newModel;
        }
        public void Delete(int id)
        {
            Treasury edited = dbSet.FirstOrDefault(p => p.ID == id);
            Delete(edited);
        }


        public TreasuryVM GetTreasuryByBranch(int branchID)
        {
            return dbSet.Where(x => x.BranchId == branchID && x.IsDeleted == false).Select(s => new TreasuryVM
            {
                ID = s.ID
            }).FirstOrDefault();
        }


        public bool IsExistArrangement(int arrangement, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.arrangement == arrangement & g.IsDeleted == false);
            }
            else
            {
                return dbSet.Any(g => g.arrangement == arrangement & g.ID != id & g.IsDeleted == false);
            }
        }

        public bool IsExistTreasuryCode(string Code, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.Code == Code & g.IsDeleted == false);
            }
            else
            {
                return dbSet.Any(g => g.Code == Code & g.ID != id & g.IsDeleted == false);
            }
        }
    }
}