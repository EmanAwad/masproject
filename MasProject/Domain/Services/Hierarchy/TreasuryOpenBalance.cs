﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Hierarchy;
using MasProject.Domain.ViewModel.Hierarchy;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MasProject.Domain.Services.Hierarchy
{
    public class TreasuryOpenBalanceOpenBalanceServices : Repository<TreasuryOpenBalance>
    {
        public TreasuryOpenBalanceOpenBalanceServices(DBContext context) : base(context)
        {
        }

        public IEnumerable<TreasuryOpenBalanceVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new TreasuryOpenBalanceVM
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                CurrRatio=s.CurrRatio,
                Note=s.Note,
                Amount=s.Amount,
                TreasuryID = s.TreasuryId,
                CurrencyID =  s.CurrencyId  ,
                EntryDate = s.EntryDate,
                UserId = s.UserId,
                UserName = s.Employee.UserName,
                CurrencyName = s.Currency.Name,
                TreasuryName = s.Treasury.Name,
                    CreatedDate = s.CreatedDate,
            }).OrderByDescending(s => s.CreatedDate);
        }
        public TreasuryOpenBalanceVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new TreasuryOpenBalanceVM
            {
                ID = s.ID,
                IsDeleted = s.IsDeleted,
                CurrRatio = s.CurrRatio,
                Note = s.Note,
                Amount = s.Amount,
                TreasuryID =  s.TreasuryId,
                CurrencyID =  s.CurrencyId,
                EntryDate=s.EntryDate.Date,
                UserId = s.UserId
            }).FirstOrDefault();
        }
        public TreasuryOpenBalance Add(TreasuryOpenBalanceVM model)
        {
            var newModel = new TreasuryOpenBalance
            {
                ID = model.ID,
                CreatedDate = DateTime.Now,
                IsDeleted = false,
                EntryDate = model.EntryDate.ToLocalTime(),
                Amount=model.Amount,
                Note=model.Note,
                CurrRatio=model.CurrRatio,
                CurrencyId = model.CurrencyID,
                TreasuryId=model.TreasuryID,
                UserId = model.UserId
            };
            Insert(newModel);
            return newModel;
        }
        public TreasuryOpenBalance Edit(TreasuryOpenBalanceVM model)
        {
            var newModel = new TreasuryOpenBalance
            {
                ID = model.ID,
                ModifiedDate = DateTime.Today,
                Amount = model.Amount,
                Note = model.Note,
                CurrRatio = model.CurrRatio,
                CurrencyId = model.CurrencyID,
                TreasuryId = model.TreasuryID,
                EntryDate = model.EntryDate.ToLocalTime(),
                UserId = model.UserId
            };
            Update(newModel);
            return newModel;
        }
        public void Delete(int id)
        {
            TreasuryOpenBalance edited = dbSet.FirstOrDefault(p => p.ID == id);
            Delete(edited);
        }
    }
}