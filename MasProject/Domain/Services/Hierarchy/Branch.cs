﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Hierarchy;
using MasProject.Domain.ViewModel.Hierarchy;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MasProject.Domain.Services.Hierarchy
{
    public class BranchServices : Repository<Branch>
    {
        private DBContext _context;
        public BranchServices(DBContext context) : base(context)
        {
            _context = context;

        }

        public IEnumerable<BranchVM> GetAll()
        {
            try
            {
                return dbSet.Where(g => g.IsDeleted == false).Select(s => new BranchVM
                {
                    ID = s.ID,
                    Name = s.Name,
                    IsDeleted = s.IsDeleted,
                    BranchKey = s.BranchKey,
                    Address = s.Address,
                    Note = s.Note,
                    //  CompanyId=(int)s.CompanyId,
                    arrangement = s.arrangement,
                    CreatedDate = s.CreatedDate,
                    Phone = s.Phone,
                    Mobile = s.Mobile
                }).OrderBy(s => s.arrangement);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public BranchVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new BranchVM
            {
                ID = s.ID,
                Name = s.Name,
                IsDeleted = s.IsDeleted,
                BranchKey = s.BranchKey,
                Address = s.Address,
                Note = s.Note,
                //   CompanyId = (int)s.CompanyId,
                arrangement = s.arrangement,
                Phone = s.Phone,
                Mobile = s.Mobile

            }).FirstOrDefault();
        }

        public Branch Add(BranchVM model)
        {
            var newModel = new Branch
            {
                ID = model.ID,
                Name = model.Name,
                CreatedDate = DateTime.Today,
                IsDeleted = false,
                BranchKey = model.BranchKey,
                Address = model.Address,
                Note = model.Note,
                //  CompanyId = model.CompanyId,
                arrangement = model.arrangement,
                Phone = model.Phone,
                Mobile = model.Mobile

            };
            Insert(newModel);
            return newModel;
        }
        public Branch Edit(BranchVM model)
        {
            var newModel = new Branch
            {
                ID = model.ID,
                Name = model.Name,
                ModifiedDate = DateTime.Today,
                BranchKey = model.BranchKey,
                Address = model.Address,
                Note = model.Note,
                // CompanyId = model.CompanyId,
                arrangement = model.arrangement,
                Phone = model.Phone,
                Mobile = model.Mobile

            };
            Update(newModel);
            return newModel;
        }
        public void Delete(int id)
        {
            Branch edited = dbSet.FirstOrDefault(p => p.ID == id);
            Delete(edited);
        }
        public bool IsExistKey(string Key, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.BranchKey == Key & g.IsDeleted == false);
            }
            else
            {
                return dbSet.Any(g => g.BranchKey == Key & g.ID != id & g.IsDeleted == false);
            }
        }
        public bool IsExistArrangement(int arrangement, int id)
        {
            if (id == 0)
            {
                return dbSet.Any(g => g.arrangement == arrangement & g.IsDeleted == false);
            }
            else
            {
                return dbSet.Any(g => g.arrangement == arrangement & g.IsDeleted == false & g.ID != id);
            }
        }
        public bool IsBranchUsed(int Id)
        {
            var Quotations = _context.Quotations.Where(x => x.BranchId == Id && x.IsDeleted == false).ToList();
            if (Quotations != null && Quotations.Count > 0)
                return true;
            else
            {
                var SalesBill = _context.SalesBills.Where(x => x.BranchId == Id && x.IsDeleted == false).ToList();
                if (SalesBill != null && SalesBill.Count > 0)
                    return true;
                else
                {
                    var ExecutiveBill = _context.ExecutiveBills.Where(x => x.BranchId == Id && x.IsDeleted == false).ToList();
                    if (ExecutiveBill != null && ExecutiveBill.Count > 0)
                        return true;
                    else
                    {
                        var MaintainanceBill = _context.MantienceBills.Where(x => x.BranchId == Id && x.IsDeleted == false).ToList();
                        if (MaintainanceBill != null && MaintainanceBill.Count > 0)
                            return true;
                        else
                        {
                            var SalesReturns = _context.SalesReturns.Where(x => x.BranchId == Id && x.IsDeleted == false).ToList();
                            if (SalesReturns != null && SalesReturns.Count > 0)
                                return true;
                            else
                            {
                                var PurchasesBill = _context.PurchaseBill.Where(x => x.BranchId == Id && x.IsDeleted == false).ToList();
                                if (PurchasesBill != null && PurchasesBill.Count > 0)
                                    return true;
                                else
                                {
                                    var PurchasesReturns = _context.PurchasesReturns_Bill.Where(x => x.BranchId == Id && x.IsDeleted == false).ToList();
                                    if (PurchasesReturns != null && PurchasesReturns.Count > 0)
                                        return true;

                                    else
                                    {
                                        var ExecutionWithDrawals = _context.ExecutionWithDrawals_Bill.Where(x => x.BranchId == Id && x.IsDeleted == false).ToList();
                                        if (ExecutionWithDrawals != null && ExecutionWithDrawals.Count > 0)
                                            return true;
                                        else
                                        {
                                            var TransferOrder = _context.TransferOrder.Where(x => x.BranchToID == Id && x.IsDeleted == false).ToList();
                                            if (TransferOrder != null && TransferOrder.Count > 0)
                                                return true;

                                            else
                                            {
                                                var TransferRequest = _context.TransferRequest.Where(x => x.BranchToID == Id && x.IsDeleted == false).ToList();
                                                if (TransferRequest != null && TransferRequest.Count > 0)
                                                    return true;

                                                else
                                                    return false;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
                                            }
    }
}
