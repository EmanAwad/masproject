﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Hierarchy;
using MasProject.Domain.ViewModel.Hierarchy;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MasProject.Domain.Services.Hierarchy
{
    public class EntriesTypeService : Repository<EntriesType>
    {
        public EntriesTypeService(DBContext context) : base(context)
        {
        }

        public IEnumerable<EntriesTypeVM> GetAll()
        {
            return dbSet.Where(g => g.IsDeleted == false).Select(s => new EntriesTypeVM
            {
                ID = s.ID,
                Name = s.Name,
                IsDeleted = s.IsDeleted,
                Note = s.Note,
            }).OrderBy(s => s.ID);
        }
        public EntriesTypeVM Get(int id)
        {
            return dbSet.Where(x => x.ID == id && x.IsDeleted == false).Select(s => new EntriesTypeVM
            {
                ID = s.ID,
                Name = s.Name,
                IsDeleted = s.IsDeleted,
                Note = s.Note,
            }).FirstOrDefault();
        }
      
        public EntriesType Add(EntriesTypeVM model)
        {
            var newModel = new EntriesType
            {
                Name = model.Name,
                CreatedDate = DateTime.Today,
                IsDeleted = false,
               Note=model.Note,
            };
            Insert(newModel);
            return newModel;
        }
        public EntriesType Edit(EntriesTypeVM model)
        {
            var newModel = new EntriesType
            {
                ID = model.ID,
                Name = model.Name,
                ModifiedDate = DateTime.Today,
                Note = model.Note,
            };
            Update(newModel);
            return newModel;
        }
        public void Delete(int id)
        {
            EntriesType edited = dbSet.FirstOrDefault(p => p.ID == id);
            Delete(edited);
        }

        
    }
}