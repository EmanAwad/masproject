﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Rendering;
using Microsoft.AspNetCore.Http;
using System;
using System.Net;

namespace MasProject.Domain.Helpers
{
    public class AppRouteView : RouteView
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        [Inject]
        public NavigationManager NavigationManager { get; set; }

        //[Inject]
       // public IAccountService AccountService { get; set; }
       //public ApplicationUser User { get; set; }
        protected override void Render(RenderTreeBuilder builder)
        {
           var User = _httpContextAccessor.HttpContext.User.Identity;
            var authorize = Attribute.GetCustomAttribute(RouteData.PageType, typeof(AuthorizeAttribute)) != null;
            if (authorize && User.IsAuthenticated == false)// if (authorize && AccountService.User == null)
            {
                var returnUrl = WebUtility.UrlEncode(new Uri(NavigationManager.Uri).PathAndQuery);
                NavigationManager.NavigateTo($"/?returnUrl={returnUrl}");
            }
            else
            {
                base.Render(builder);
            }
        }
    }
}
