﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class RemoveColumnsFromEmployeeOpenBal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EmployeeOpeningBalance_Currency_CurrencyID",
                table: "EmployeeOpeningBalance");

            migrationBuilder.DropIndex(
                name: "IX_EmployeeOpeningBalance_CurrencyID",
                table: "EmployeeOpeningBalance");

            migrationBuilder.DropColumn(
                name: "CurrencyID",
                table: "EmployeeOpeningBalance");

            migrationBuilder.DropColumn(
                name: "Ratio",
                table: "EmployeeOpeningBalance");

          
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
         
            migrationBuilder.AddColumn<int>(
                name: "CurrencyID",
                table: "EmployeeOpeningBalance",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "Ratio",
                table: "EmployeeOpeningBalance",
                type: "float",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeOpeningBalance_CurrencyID",
                table: "EmployeeOpeningBalance",
                column: "CurrencyID");

            migrationBuilder.AddForeignKey(
                name: "FK_EmployeeOpeningBalance_Currency_CurrencyID",
                table: "EmployeeOpeningBalance",
                column: "CurrencyID",
                principalTable: "Currency",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
