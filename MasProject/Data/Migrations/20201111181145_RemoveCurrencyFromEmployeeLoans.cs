﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class RemoveCurrencyFromEmployeeLoans : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EmployeesLoansEntry_Currency_CurrencyId",
                table: "EmployeesLoansEntry");

            migrationBuilder.DropForeignKey(
                name: "FK_EmployeesLoansPayment_Currency_CurrencyId",
                table: "EmployeesLoansPayment");

            migrationBuilder.DropIndex(
                name: "IX_EmployeesLoansPayment_CurrencyId",
                table: "EmployeesLoansPayment");

            migrationBuilder.DropIndex(
                name: "IX_EmployeesLoansEntry_CurrencyId",
                table: "EmployeesLoansEntry");

            migrationBuilder.DropColumn(
                name: "CurrencyId",
                table: "EmployeesLoansPayment");

            migrationBuilder.DropColumn(
                name: "CurrencyId",
                table: "EmployeesLoansEntry");

           
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CurrencyId",
                table: "EmployeesLoansPayment",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CurrencyId",
                table: "EmployeesLoansEntry",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_EmployeesLoansPayment_CurrencyId",
                table: "EmployeesLoansPayment",
                column: "CurrencyId");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeesLoansEntry_CurrencyId",
                table: "EmployeesLoansEntry",
                column: "CurrencyId");

            migrationBuilder.AddForeignKey(
                name: "FK_EmployeesLoansEntry_Currency_CurrencyId",
                table: "EmployeesLoansEntry",
                column: "CurrencyId",
                principalTable: "Currency",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_EmployeesLoansPayment_Currency_CurrencyId",
                table: "EmployeesLoansPayment",
                column: "CurrencyId",
                principalTable: "Currency",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
