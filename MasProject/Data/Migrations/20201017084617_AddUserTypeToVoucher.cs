﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class AddUserTypeToVoucher : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
           
            migrationBuilder.AddColumn<int>(
                name: "UserType",
                table: "PaymentVoucher",
                nullable: false,
                defaultValue: 0);

           
            migrationBuilder.AddColumn<int>(
                name: "UserType",
                table: "AdditionVoucher",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserType",
                table: "PaymentVoucher");

          
            migrationBuilder.DropColumn(
                name: "UserType",
                table: "AdditionVoucher");
        }
    }
}
