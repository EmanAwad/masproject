﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class AddArrangmentColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "arrangement",
                table: "Supplier",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "arrangement",
                table: "OtherConstant",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "arrangement",
                table: "ItemTypes",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "arrangement",
                table: "Employee",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "arrangement",
                table: "DealTypes",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "arrangement",
                table: "Currency",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "arrangement",
                table: "Client",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "arrangement",
                table: "Supplier");

            migrationBuilder.DropColumn(
                name: "arrangement",
                table: "OtherConstant");

            migrationBuilder.DropColumn(
                name: "arrangement",
                table: "ItemTypes");

            migrationBuilder.DropColumn(
                name: "arrangement",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "arrangement",
                table: "DealTypes");

            migrationBuilder.DropColumn(
                name: "arrangement",
                table: "Currency");

            migrationBuilder.DropColumn(
                name: "arrangement",
                table: "Client");
        }
    }
}
