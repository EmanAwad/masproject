﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class PurchaseReturn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
           

            migrationBuilder.CreateTable(
                name: "PurchasesReturns_Bill",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Note = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    SerialNumber = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    StoreId = table.Column<int>(nullable: true),
                    DealTypeId = table.Column<int>(nullable: true),
                    FlagType = table.Column<bool>(nullable: false),
                    HandDocumentNumber = table.Column<int>(nullable: false),
                    TotalPrice = table.Column<decimal>(nullable: false),
                    DiscountPrecentage = table.Column<decimal>(nullable: false),
                    DiscountValue = table.Column<decimal>(nullable: false),
                    TotalAfterDiscount = table.Column<decimal>(nullable: false),
                    ShowNotesFlag = table.Column<bool>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    RevewId = table.Column<int>(nullable: false),
                    ApproveId = table.Column<int>(nullable: false),
                    SupplierId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PurchasesReturns_Bill", x => x.ID);
                    table.ForeignKey(
                        name: "FK_PurchasesReturns_Bill_DealTypes_DealTypeId",
                        column: x => x.DealTypeId,
                        principalTable: "DealTypes",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PurchasesReturns_Bill_Stores_StoreId",
                        column: x => x.StoreId,
                        principalTable: "Stores",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PurchasesReturns_Bill_Supplier_SupplierId",
                        column: x => x.SupplierId,
                        principalTable: "Supplier",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PurchasesReturns_Items",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ItemId = table.Column<int>(nullable: true),
                    Quantity = table.Column<int>(nullable: false),
                    Price = table.Column<decimal>(nullable: false),
                    Total = table.Column<decimal>(nullable: false),
                    SavedPrice = table.Column<decimal>(nullable: false),
                    SerialNumber = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PurchasesReturns_Items", x => x.ID);
                    table.ForeignKey(
                        name: "FK_PurchasesReturns_Items_Items_ItemId",
                        column: x => x.ItemId,
                        principalTable: "Items",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

          

            migrationBuilder.CreateIndex(
                name: "IX_PurchasesReturns_Bill_DealTypeId",
                table: "PurchasesReturns_Bill",
                column: "DealTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchasesReturns_Bill_StoreId",
                table: "PurchasesReturns_Bill",
                column: "StoreId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchasesReturns_Bill_SupplierId",
                table: "PurchasesReturns_Bill",
                column: "SupplierId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchasesReturns_Items_ItemId",
                table: "PurchasesReturns_Items",
                column: "ItemId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            

            migrationBuilder.DropTable(
                name: "PurchasesReturns_Bill");

            migrationBuilder.DropTable(
                name: "PurchasesReturns_Items");

       
        }
    }
}
