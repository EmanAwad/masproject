﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class AddTransferOrderTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.CreateTable(
            //    name: "TransferOrder",
            //    columns: table => new
            //    {
            //        ID = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        Name = table.Column<string>(nullable: true),
            //        IsDeleted = table.Column<bool>(nullable: false),
            //        CreatedBy = table.Column<string>(nullable: true),
            //        CreatedDate = table.Column<DateTime>(nullable: false),
            //        ModifiedBy = table.Column<string>(nullable: true),
            //        ModifiedDate = table.Column<DateTime>(nullable: false),
            //        Note = table.Column<string>(nullable: true),
            //        TransferRequestId = table.Column<int>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_TransferOrder", x => x.ID);
            //        table.ForeignKey(
            //            name: "FK_TransferOrder_TransferRequest_TransferRequestId",
            //            column: x => x.TransferRequestId,
            //            principalTable: "TransferRequest",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            migrationBuilder.CreateTable(
                name: "TransferOrderDetails",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Note = table.Column<string>(nullable: true),
                    Quantity = table.Column<double>(nullable: false),
                    ItemId = table.Column<int>(nullable: true),
                    StoreIDFrom = table.Column<int>(nullable: true),
                    TransferOrderId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransferOrderDetails", x => x.ID);
                    table.ForeignKey(
                        name: "FK_TransferOrderDetails_Items_ItemId",
                        column: x => x.ItemId,
                        principalTable: "Items",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TransferOrderDetails_Stores_StoreIDFrom",
                        column: x => x.StoreIDFrom,
                        principalTable: "Stores",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TransferOrderDetails_TransferOrder_TransferOrderId",
                        column: x => x.TransferOrderId,
                        principalTable: "TransferOrder",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            //migrationBuilder.CreateIndex(
            //    name: "IX_TransferOrder_TransferRequestId",
            //    table: "TransferOrder",
            //    column: "TransferRequestId");

            migrationBuilder.CreateIndex(
                name: "IX_TransferOrderDetails_ItemId",
                table: "TransferOrderDetails",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_TransferOrderDetails_StoreIDFrom",
                table: "TransferOrderDetails",
                column: "StoreIDFrom");

            migrationBuilder.CreateIndex(
                name: "IX_TransferOrderDetails_TransferOrderId",
                table: "TransferOrderDetails",
                column: "TransferOrderId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TransferOrderDetails");

            //migrationBuilder.DropTable(
            //    name: "TransferOrder");
        }
    }
}
