﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class BranchColBills : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_AdditionVoucher_DealTypes_DealTypeId",
            //    table: "AdditionVoucher");

            //migrationBuilder.DropForeignKey(
            //    name: "FK_PaymentVoucher_DealTypes_DealTypeId",
            //    table: "PaymentVoucher");

            //migrationBuilder.DropIndex(
            //    name: "IX_PaymentVoucher_DealTypeId",
            //    table: "PaymentVoucher");

            //migrationBuilder.DropIndex(
            //    name: "IX_AdditionVoucher_DealTypeId",
            //    table: "AdditionVoucher");

            //migrationBuilder.DropColumn(
            //    name: "Price",
            //    table: "PaymentVoucherItem");

            //migrationBuilder.DropColumn(
            //    name: "Total",
            //    table: "PaymentVoucherItem");

            //migrationBuilder.DropColumn(
            //    name: "DealTypeId",
            //    table: "PaymentVoucher");

            //migrationBuilder.DropColumn(
            //    name: "DiscountPrecentage",
            //    table: "PaymentVoucher");

            //migrationBuilder.DropColumn(
            //    name: "DiscountValue",
            //    table: "PaymentVoucher");

            //migrationBuilder.DropColumn(
            //    name: "FlagType",
            //    table: "PaymentVoucher");

            //migrationBuilder.DropColumn(
            //    name: "TotalAfterDiscount",
            //    table: "PaymentVoucher");

            //migrationBuilder.DropColumn(
            //    name: "TotalPrice",
            //    table: "PaymentVoucher");

            //migrationBuilder.DropColumn(
            //    name: "Price",
            //    table: "AdditionVoucherItem");

            //migrationBuilder.DropColumn(
            //    name: "SavedPrice",
            //    table: "AdditionVoucherItem");

            //migrationBuilder.DropColumn(
            //    name: "Total",
            //    table: "AdditionVoucherItem");

            //migrationBuilder.DropColumn(
            //    name: "DealTypeId",
            //    table: "AdditionVoucher");

            //migrationBuilder.DropColumn(
            //    name: "DiscountPrecentage",
            //    table: "AdditionVoucher");

            //migrationBuilder.DropColumn(
            //    name: "DiscountValue",
            //    table: "AdditionVoucher");

            //migrationBuilder.DropColumn(
            //    name: "FlagType",
            //    table: "AdditionVoucher");

            //migrationBuilder.DropColumn(
            //    name: "TotalAfterDiscount",
            //    table: "AdditionVoucher");

            //migrationBuilder.DropColumn(
            //    name: "TotalPrice",
            //    table: "AdditionVoucher");

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "SalesReturns",
                nullable: true);

            //migrationBuilder.AddColumn<int>(
            //    name: "StoreId",
            //    table: "SalesItems",
            //    nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "SalesBills",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "PurchasesReturns_Bill",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "PurchaseBill",
                nullable: true);

            //migrationBuilder.AddColumn<string>(
            //    name: "BoxCode",
            //    table: "PaymentVoucherItem",
            //    nullable: true);

            //migrationBuilder.AddColumn<string>(
            //    name: "SheilfNo",
            //    table: "PaymentVoucherItem",
            //    nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "MantienceBills",
                nullable: true);

            //migrationBuilder.AddColumn<bool>(
            //    name: "IsConverted",
            //    table: "MantienceBills",
            //    nullable: false,
            //    defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "ExecutiveBills",
                nullable: true);

            //migrationBuilder.AddColumn<bool>(
            //    name: "IsConverted",
            //    table: "ExecutiveBills",
            //    nullable: false,
            //    defaultValue: false);

            //migrationBuilder.AddColumn<string>(
            //    name: "BoxCode",
            //    table: "AdditionVoucherItem",
            //    nullable: true);

            //migrationBuilder.AddColumn<string>(
            //    name: "SheilfNo",
            //    table: "AdditionVoucherItem",
            //    nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_SalesReturns_BranchId",
                table: "SalesReturns",
                column: "BranchId");

            migrationBuilder.CreateIndex(
                name: "IX_SalesBills_BranchId",
                table: "SalesBills",
                column: "BranchId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchasesReturns_Bill_BranchId",
                table: "PurchasesReturns_Bill",
                column: "BranchId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseBill_BranchId",
                table: "PurchaseBill",
                column: "BranchId");

            migrationBuilder.CreateIndex(
                name: "IX_MantienceBills_BranchId",
                table: "MantienceBills",
                column: "BranchId");

            migrationBuilder.CreateIndex(
                name: "IX_ExecutiveBills_BranchId",
                table: "ExecutiveBills",
                column: "BranchId");

            migrationBuilder.AddForeignKey(
                name: "FK_ExecutiveBills_Branch_BranchId",
                table: "ExecutiveBills",
                column: "BranchId",
                principalTable: "Branch",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MantienceBills_Branch_BranchId",
                table: "MantienceBills",
                column: "BranchId",
                principalTable: "Branch",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PurchaseBill_Branch_BranchId",
                table: "PurchaseBill",
                column: "BranchId",
                principalTable: "Branch",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PurchasesReturns_Bill_Branch_BranchId",
                table: "PurchasesReturns_Bill",
                column: "BranchId",
                principalTable: "Branch",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SalesBills_Branch_BranchId",
                table: "SalesBills",
                column: "BranchId",
                principalTable: "Branch",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SalesReturns_Branch_BranchId",
                table: "SalesReturns",
                column: "BranchId",
                principalTable: "Branch",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ExecutiveBills_Branch_BranchId",
                table: "ExecutiveBills");

            migrationBuilder.DropForeignKey(
                name: "FK_MantienceBills_Branch_BranchId",
                table: "MantienceBills");

            migrationBuilder.DropForeignKey(
                name: "FK_PurchaseBill_Branch_BranchId",
                table: "PurchaseBill");

            migrationBuilder.DropForeignKey(
                name: "FK_PurchasesReturns_Bill_Branch_BranchId",
                table: "PurchasesReturns_Bill");

            migrationBuilder.DropForeignKey(
                name: "FK_SalesBills_Branch_BranchId",
                table: "SalesBills");

            migrationBuilder.DropForeignKey(
                name: "FK_SalesReturns_Branch_BranchId",
                table: "SalesReturns");

            migrationBuilder.DropIndex(
                name: "IX_SalesReturns_BranchId",
                table: "SalesReturns");

            migrationBuilder.DropIndex(
                name: "IX_SalesBills_BranchId",
                table: "SalesBills");

            migrationBuilder.DropIndex(
                name: "IX_PurchasesReturns_Bill_BranchId",
                table: "PurchasesReturns_Bill");

            migrationBuilder.DropIndex(
                name: "IX_PurchaseBill_BranchId",
                table: "PurchaseBill");

            migrationBuilder.DropIndex(
                name: "IX_MantienceBills_BranchId",
                table: "MantienceBills");

            migrationBuilder.DropIndex(
                name: "IX_ExecutiveBills_BranchId",
                table: "ExecutiveBills");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "SalesReturns");

            //migrationBuilder.DropColumn(
            //    name: "StoreId",
            //    table: "SalesItems");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "SalesBills");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "PurchasesReturns_Bill");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "PurchaseBill");

            //migrationBuilder.DropColumn(
            //    name: "BoxCode",
            //    table: "PaymentVoucherItem");

            //migrationBuilder.DropColumn(
            //    name: "SheilfNo",
            //    table: "PaymentVoucherItem");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "MantienceBills");

            //migrationBuilder.DropColumn(
            //    name: "IsConverted",
            //    table: "MantienceBills");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "ExecutiveBills");

            //migrationBuilder.DropColumn(
            //    name: "IsConverted",
            //    table: "ExecutiveBills");

            //migrationBuilder.DropColumn(
            //    name: "BoxCode",
            //    table: "AdditionVoucherItem");

            //migrationBuilder.DropColumn(
            //    name: "SheilfNo",
            //    table: "AdditionVoucherItem");

            //migrationBuilder.AddColumn<decimal>(
            //    name: "Price",
            //    table: "PaymentVoucherItem",
            //    type: "decimal(18,2)",
            //    nullable: true);

            //migrationBuilder.AddColumn<decimal>(
            //    name: "Total",
            //    table: "PaymentVoucherItem",
            //    type: "decimal(18,2)",
            //    nullable: true);

            //migrationBuilder.AddColumn<int>(
            //    name: "DealTypeId",
            //    table: "PaymentVoucher",
            //    type: "int",
            //    nullable: true);

            //migrationBuilder.AddColumn<decimal>(
            //    name: "DiscountPrecentage",
            //    table: "PaymentVoucher",
            //    type: "decimal(18,2)",
            //    nullable: true);

            //migrationBuilder.AddColumn<decimal>(
            //    name: "DiscountValue",
            //    table: "PaymentVoucher",
            //    type: "decimal(18,2)",
            //    nullable: true);

            //migrationBuilder.AddColumn<bool>(
            //    name: "FlagType",
            //    table: "PaymentVoucher",
            //    type: "bit",
            //    nullable: false,
            //    defaultValue: false);

            //migrationBuilder.AddColumn<decimal>(
            //    name: "TotalAfterDiscount",
            //    table: "PaymentVoucher",
            //    type: "decimal(18,2)",
            //    nullable: true);

            //migrationBuilder.AddColumn<decimal>(
            //    name: "TotalPrice",
            //    table: "PaymentVoucher",
            //    type: "decimal(18,2)",
            //    nullable: true);

            //migrationBuilder.AddColumn<decimal>(
            //    name: "Price",
            //    table: "AdditionVoucherItem",
            //    type: "decimal(18,2)",
            //    nullable: true);

            //migrationBuilder.AddColumn<decimal>(
            //    name: "SavedPrice",
            //    table: "AdditionVoucherItem",
            //    type: "decimal(18,2)",
            //    nullable: true);

            //migrationBuilder.AddColumn<decimal>(
            //    name: "Total",
            //    table: "AdditionVoucherItem",
            //    type: "decimal(18,2)",
            //    nullable: true);

            //migrationBuilder.AddColumn<int>(
            //    name: "DealTypeId",
            //    table: "AdditionVoucher",
            //    type: "int",
            //    nullable: true);

            //migrationBuilder.AddColumn<decimal>(
            //    name: "DiscountPrecentage",
            //    table: "AdditionVoucher",
            //    type: "decimal(18,2)",
            //    nullable: true);

            //migrationBuilder.AddColumn<decimal>(
            //    name: "DiscountValue",
            //    table: "AdditionVoucher",
            //    type: "decimal(18,2)",
            //    nullable: true);

            //migrationBuilder.AddColumn<bool>(
            //    name: "FlagType",
            //    table: "AdditionVoucher",
            //    type: "bit",
            //    nullable: false,
            //    defaultValue: false);

            //migrationBuilder.AddColumn<decimal>(
            //    name: "TotalAfterDiscount",
            //    table: "AdditionVoucher",
            //    type: "decimal(18,2)",
            //    nullable: true);

            //migrationBuilder.AddColumn<decimal>(
            //    name: "TotalPrice",
            //    table: "AdditionVoucher",
            //    type: "decimal(18,2)",
            //    nullable: true);

            //migrationBuilder.CreateIndex(
            //    name: "IX_PaymentVoucher_DealTypeId",
            //    table: "PaymentVoucher",
            //    column: "DealTypeId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_AdditionVoucher_DealTypeId",
            //    table: "AdditionVoucher",
            //    column: "DealTypeId");

            //migrationBuilder.AddForeignKey(
            //    name: "FK_AdditionVoucher_DealTypes_DealTypeId",
            //    table: "AdditionVoucher",
            //    column: "DealTypeId",
            //    principalTable: "DealTypes",
            //    principalColumn: "ID",
            //    onDelete: ReferentialAction.Restrict);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_PaymentVoucher_DealTypes_DealTypeId",
            //    table: "PaymentVoucher",
            //    column: "DealTypeId",
            //    principalTable: "DealTypes",
            //    principalColumn: "ID",
            //    onDelete: ReferentialAction.Restrict);
        }
    }
}
