﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class itemconversionEdites1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ItemConversion_Bill");

            migrationBuilder.DropTable(
                name: "ItemConversion_Items");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ItemConversion_Bill",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ConversionReason = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ConversionRecordID = table.Column<int>(type: "int", nullable: true),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    EmployeeId = table.Column<int>(type: "int", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    ModifiedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Note = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    StoreId = table.Column<int>(type: "int", nullable: true),
                    TotalPriceFrom = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    TotalPriceTo = table.Column<decimal>(type: "decimal(18,2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemConversion_Bill", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ItemConversion_Bill_Employee_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "Employee",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ItemConversion_Bill_Stores_StoreId",
                        column: x => x.StoreId,
                        principalTable: "Stores",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ItemConversion_Items",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ConversionRecordID = table.Column<int>(type: "int", nullable: true),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IdentiferFrom = table.Column<int>(type: "int", nullable: true),
                    IdentiferFromTo = table.Column<int>(type: "int", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    ItemIdFrom = table.Column<int>(type: "int", nullable: true),
                    ItemIdTo = table.Column<int>(type: "int", nullable: true),
                    ModifiedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Note = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ParCodeFrom = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ParCodeTo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PriceFrom = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    PriceTo = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    QuantityFrom = table.Column<int>(type: "int", nullable: true),
                    QuantityTo = table.Column<int>(type: "int", nullable: true),
                    TotalFrom = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    TotalTo = table.Column<decimal>(type: "decimal(18,2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemConversion_Items", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ItemConversion_Items_Items_ItemIdFrom",
                        column: x => x.ItemIdFrom,
                        principalTable: "Items",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ItemConversion_Items_Items_ItemIdTo",
                        column: x => x.ItemIdTo,
                        principalTable: "Items",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ItemConversion_Bill_EmployeeId",
                table: "ItemConversion_Bill",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_ItemConversion_Bill_StoreId",
                table: "ItemConversion_Bill",
                column: "StoreId");

            migrationBuilder.CreateIndex(
                name: "IX_ItemConversion_Items_ItemIdFrom",
                table: "ItemConversion_Items",
                column: "ItemIdFrom");

            migrationBuilder.CreateIndex(
                name: "IX_ItemConversion_Items_ItemIdTo",
                table: "ItemConversion_Items",
                column: "ItemIdTo");
        }
    }
}
