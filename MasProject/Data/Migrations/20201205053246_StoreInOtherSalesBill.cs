﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class StoreInOtherSalesBill : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropColumn(
            //    name: "SavedPrice",
            //    table: "PaymentVoucherItem");

            migrationBuilder.AddColumn<int>(
                name: "StoreId",
                table: "SalesReturnsItems",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "MultiStore",
                table: "SalesReturns",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "StoreId",
                table: "MantienceItems",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "MultiStore",
                table: "MantienceBills",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "StoreId",
                table: "ExecutiveItems",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "MultiStore",
                table: "ExecutiveBills",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "ExecutiveBillStores",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    DeletedBy = table.Column<string>(nullable: true),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    SerialNumber = table.Column<int>(nullable: true),
                    StoreId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExecutiveBillStores", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "MaintanceBillStores",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    DeletedBy = table.Column<string>(nullable: true),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    SerialNumber = table.Column<int>(nullable: true),
                    StoreId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MaintanceBillStores", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "SalesReturnsBillStores",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    DeletedBy = table.Column<string>(nullable: true),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    SerialNumber = table.Column<int>(nullable: true),
                    StoreId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SalesReturnsBillStores", x => x.ID);
                });

            //migrationBuilder.CreateTable(
            //    name: "TempAdditionVoucher",
            //    columns: table => new
            //    {
            //        ID = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        IsDeleted = table.Column<bool>(nullable: false),
            //        CreatedBy = table.Column<string>(nullable: true),
            //        CreatedDate = table.Column<DateTime>(nullable: false),
            //        ModifiedBy = table.Column<string>(nullable: true),
            //        ModifiedDate = table.Column<DateTime>(nullable: false),
            //        DeletedBy = table.Column<string>(nullable: true),
            //        DeletedDate = table.Column<DateTime>(nullable: false),
            //        Note = table.Column<string>(nullable: true),
            //        Name = table.Column<string>(nullable: true),
            //        SerialNumber = table.Column<int>(nullable: true),
            //        Date = table.Column<DateTime>(nullable: false),
            //        StoreId = table.Column<int>(nullable: true),
            //        ShowNotesFlag = table.Column<bool>(nullable: false),
            //        UserId = table.Column<int>(nullable: false),
            //        RevewId = table.Column<int>(nullable: false),
            //        ApproveId = table.Column<int>(nullable: false),
            //        Client_Supplier_Id = table.Column<int>(nullable: true),
            //        BillSerialNumber = table.Column<int>(nullable: true),
            //        BillType = table.Column<string>(nullable: true),
            //        UserType = table.Column<int>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_TempAdditionVoucher", x => x.ID);
            //        table.ForeignKey(
            //            name: "FK_TempAdditionVoucher_Stores_StoreId",
            //            column: x => x.StoreId,
            //            principalTable: "Stores",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "TempAdditionVoucherItem",
            //    columns: table => new
            //    {
            //        ID = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        IsDeleted = table.Column<bool>(nullable: false),
            //        CreatedBy = table.Column<string>(nullable: true),
            //        CreatedDate = table.Column<DateTime>(nullable: false),
            //        ModifiedBy = table.Column<string>(nullable: true),
            //        ModifiedDate = table.Column<DateTime>(nullable: false),
            //        DeletedBy = table.Column<string>(nullable: true),
            //        DeletedDate = table.Column<DateTime>(nullable: false),
            //        Name = table.Column<string>(nullable: true),
            //        ItemId = table.Column<int>(nullable: true),
            //        Quantity = table.Column<int>(nullable: true),
            //        SheilfNo = table.Column<string>(nullable: true),
            //        BoxCode = table.Column<string>(nullable: true),
            //        SerialNumber = table.Column<int>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_TempAdditionVoucherItem", x => x.ID);
            //        table.ForeignKey(
            //            name: "FK_TempAdditionVoucherItem_Items_ItemId",
            //            column: x => x.ItemId,
            //            principalTable: "Items",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "TempPaymentVoucher",
            //    columns: table => new
            //    {
            //        ID = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        IsDeleted = table.Column<bool>(nullable: false),
            //        CreatedBy = table.Column<string>(nullable: true),
            //        CreatedDate = table.Column<DateTime>(nullable: false),
            //        ModifiedBy = table.Column<string>(nullable: true),
            //        ModifiedDate = table.Column<DateTime>(nullable: false),
            //        DeletedBy = table.Column<string>(nullable: true),
            //        DeletedDate = table.Column<DateTime>(nullable: false),
            //        Note = table.Column<string>(nullable: true),
            //        Name = table.Column<string>(nullable: true),
            //        SerialNumber = table.Column<int>(nullable: true),
            //        Date = table.Column<DateTime>(nullable: false),
            //        StoreId = table.Column<int>(nullable: true),
            //        ShowNotesFlag = table.Column<bool>(nullable: false),
            //        UserId = table.Column<int>(nullable: false),
            //        RevewId = table.Column<int>(nullable: false),
            //        ApproveId = table.Column<int>(nullable: false),
            //        Client_Supplier_Id = table.Column<int>(nullable: true),
            //        BillSerialNumber = table.Column<int>(nullable: true),
            //        BillType = table.Column<string>(nullable: true),
            //        UserType = table.Column<int>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_TempPaymentVoucher", x => x.ID);
            //        table.ForeignKey(
            //            name: "FK_TempPaymentVoucher_Stores_StoreId",
            //            column: x => x.StoreId,
            //            principalTable: "Stores",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "TempPaymentVoucherItem",
            //    columns: table => new
            //    {
            //        ID = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        IsDeleted = table.Column<bool>(nullable: false),
            //        CreatedBy = table.Column<string>(nullable: true),
            //        CreatedDate = table.Column<DateTime>(nullable: false),
            //        ModifiedBy = table.Column<string>(nullable: true),
            //        ModifiedDate = table.Column<DateTime>(nullable: false),
            //        DeletedBy = table.Column<string>(nullable: true),
            //        DeletedDate = table.Column<DateTime>(nullable: false),
            //        Name = table.Column<string>(nullable: true),
            //        ItemId = table.Column<int>(nullable: true),
            //        Quantity = table.Column<int>(nullable: true),
            //        SheilfNo = table.Column<string>(nullable: true),
            //        BoxCode = table.Column<string>(nullable: true),
            //        SerialNumber = table.Column<int>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_TempPaymentVoucherItem", x => x.ID);
            //        table.ForeignKey(
            //            name: "FK_TempPaymentVoucherItem_Items_ItemId",
            //            column: x => x.ItemId,
            //            principalTable: "Items",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateIndex(
            //    name: "IX_TempAdditionVoucher_StoreId",
            //    table: "TempAdditionVoucher",
            //    column: "StoreId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_TempAdditionVoucherItem_ItemId",
            //    table: "TempAdditionVoucherItem",
            //    column: "ItemId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_TempPaymentVoucher_StoreId",
            //    table: "TempPaymentVoucher",
            //    column: "StoreId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_TempPaymentVoucherItem_ItemId",
            //    table: "TempPaymentVoucherItem",
            //    column: "ItemId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExecutiveBillStores");

            migrationBuilder.DropTable(
                name: "MaintanceBillStores");

            migrationBuilder.DropTable(
                name: "SalesReturnsBillStores");

            //migrationBuilder.DropTable(
            //    name: "TempAdditionVoucher");

            //migrationBuilder.DropTable(
            //    name: "TempAdditionVoucherItem");

            //migrationBuilder.DropTable(
            //    name: "TempPaymentVoucher");

            //migrationBuilder.DropTable(
            //    name: "TempPaymentVoucherItem");

            migrationBuilder.DropColumn(
                name: "StoreId",
                table: "SalesReturnsItems");

            migrationBuilder.DropColumn(
                name: "MultiStore",
                table: "SalesReturns");

            migrationBuilder.DropColumn(
                name: "StoreId",
                table: "MantienceItems");

            migrationBuilder.DropColumn(
                name: "MultiStore",
                table: "MantienceBills");

            migrationBuilder.DropColumn(
                name: "StoreId",
                table: "ExecutiveItems");

            migrationBuilder.DropColumn(
                name: "MultiStore",
                table: "ExecutiveBills");

            //migrationBuilder.AddColumn<decimal>(
            //    name: "SavedPrice",
            //    table: "PaymentVoucherItem",
            //    type: "decimal(18,2)",
            //    nullable: true);
        }
    }
}
