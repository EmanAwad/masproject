﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class Mig : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExecutionWithDrawals_Bill");

            migrationBuilder.DropTable(
                name: "ExecutionWithDrawals_Items");

            //migrationBuilder.CreateTable(
            //    name: "TransferRequest",
            //    columns: table => new
            //    {
            //        ID = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        Name = table.Column<string>(nullable: true),
            //        IsDeleted = table.Column<bool>(nullable: false),
            //        CreatedBy = table.Column<string>(nullable: true),
            //        CreatedDate = table.Column<DateTime>(nullable: false),
            //        ModifiedBy = table.Column<string>(nullable: true),
            //        ModifiedDate = table.Column<DateTime>(nullable: false),
            //        Note = table.Column<string>(nullable: true),
            //        StoreIDTo = table.Column<int>(nullable: true),
            //        IsOrder = table.Column<bool>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_TransferRequest", x => x.ID);
            //        table.ForeignKey(
            //            name: "FK_TransferRequest_Stores_StoreIDTo",
            //            column: x => x.StoreIDTo,
            //            principalTable: "Stores",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "TransferOrder",
            //    columns: table => new
            //    {
            //        ID = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        Name = table.Column<string>(nullable: true),
            //        IsDeleted = table.Column<bool>(nullable: false),
            //        CreatedBy = table.Column<string>(nullable: true),
            //        CreatedDate = table.Column<DateTime>(nullable: false),
            //        ModifiedBy = table.Column<string>(nullable: true),
            //        ModifiedDate = table.Column<DateTime>(nullable: false),
            //        Note = table.Column<string>(nullable: true),
            //        TransferRequestId = table.Column<int>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_TransferOrder", x => x.ID);
            //        table.ForeignKey(
            //            name: "FK_TransferOrder_TransferRequest_TransferRequestId",
            //            column: x => x.TransferRequestId,
            //            principalTable: "TransferRequest",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "TransferRequestItem",
            //    columns: table => new
            //    {
            //        ID = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        Name = table.Column<string>(nullable: true),
            //        IsDeleted = table.Column<bool>(nullable: false),
            //        CreatedBy = table.Column<string>(nullable: true),
            //        CreatedDate = table.Column<DateTime>(nullable: false),
            //        ModifiedBy = table.Column<string>(nullable: true),
            //        ModifiedDate = table.Column<DateTime>(nullable: false),
            //        Note = table.Column<string>(nullable: true),
            //        RequiredQuantity = table.Column<double>(nullable: false),
            //        ItemId = table.Column<int>(nullable: true),
            //        StoreIDFrom = table.Column<int>(nullable: true),
            //        TransferRequestId = table.Column<int>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_TransferRequestItem", x => x.ID);
            //        table.ForeignKey(
            //            name: "FK_TransferRequestItem_Items_ItemId",
            //            column: x => x.ItemId,
            //            principalTable: "Items",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //        table.ForeignKey(
            //            name: "FK_TransferRequestItem_Stores_StoreIDFrom",
            //            column: x => x.StoreIDFrom,
            //            principalTable: "Stores",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //        table.ForeignKey(
            //            name: "FK_TransferRequestItem_TransferRequest_TransferRequestId",
            //            column: x => x.TransferRequestId,
            //            principalTable: "TransferRequest",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "TransferOrderDetails",
            //    columns: table => new
            //    {
            //        ID = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        Name = table.Column<string>(nullable: true),
            //        IsDeleted = table.Column<bool>(nullable: false),
            //        CreatedBy = table.Column<string>(nullable: true),
            //        CreatedDate = table.Column<DateTime>(nullable: false),
            //        ModifiedBy = table.Column<string>(nullable: true),
            //        ModifiedDate = table.Column<DateTime>(nullable: false),
            //        Note = table.Column<string>(nullable: true),
            //        Quantity = table.Column<double>(nullable: false),
            //        ItemId = table.Column<int>(nullable: true),
            //        StoreIDFrom = table.Column<int>(nullable: true),
            //        TransferOrderId = table.Column<int>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_TransferOrderDetails", x => x.ID);
            //        table.ForeignKey(
            //            name: "FK_TransferOrderDetails_Items_ItemId",
            //            column: x => x.ItemId,
            //            principalTable: "Items",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //        table.ForeignKey(
            //            name: "FK_TransferOrderDetails_Stores_StoreIDFrom",
            //            column: x => x.StoreIDFrom,
            //            principalTable: "Stores",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //        table.ForeignKey(
            //            name: "FK_TransferOrderDetails_TransferOrder_TransferOrderId",
            //            column: x => x.TransferOrderId,
            //            principalTable: "TransferOrder",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateIndex(
            //    name: "IX_TransferOrder_TransferRequestId",
            //    table: "TransferOrder",
            //    column: "TransferRequestId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_TransferOrderDetails_ItemId",
            //    table: "TransferOrderDetails",
            //    column: "ItemId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_TransferOrderDetails_StoreIDFrom",
            //    table: "TransferOrderDetails",
            //    column: "StoreIDFrom");

            //migrationBuilder.CreateIndex(
            //    name: "IX_TransferOrderDetails_TransferOrderId",
            //    table: "TransferOrderDetails",
            //    column: "TransferOrderId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_TransferRequest_StoreIDTo",
            //    table: "TransferRequest",
            //    column: "StoreIDTo");

            //migrationBuilder.CreateIndex(
            //    name: "IX_TransferRequestItem_ItemId",
            //    table: "TransferRequestItem",
            //    column: "ItemId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_TransferRequestItem_StoreIDFrom",
            //    table: "TransferRequestItem",
            //    column: "StoreIDFrom");

            //migrationBuilder.CreateIndex(
            //    name: "IX_TransferRequestItem_TransferRequestId",
            //    table: "TransferRequestItem",
            //    column: "TransferRequestId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropTable(
            //    name: "TransferOrderDetails");

            //migrationBuilder.DropTable(
            //    name: "TransferRequestItem");

            //migrationBuilder.DropTable(
            //    name: "TransferOrder");

            //migrationBuilder.DropTable(
            //    name: "TransferRequest");

            migrationBuilder.CreateTable(
                name: "ExecutionWithDrawals_Bill",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ApproveId = table.Column<int>(type: "int", nullable: false),
                    BranchId = table.Column<int>(type: "int", nullable: true),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DealTypeId = table.Column<int>(type: "int", nullable: true),
                    DiscountPrecentage = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    DiscountValue = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    FlagType = table.Column<bool>(type: "bit", nullable: false),
                    HandDocumentNumber = table.Column<int>(type: "int", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    ModifiedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Note = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RevewId = table.Column<int>(type: "int", nullable: false),
                    SerialNumber = table.Column<int>(type: "int", nullable: false),
                    ShowNotesFlag = table.Column<bool>(type: "bit", nullable: false),
                    StoreId = table.Column<int>(type: "int", nullable: true),
                    TotalAfterDiscount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    TotalPrice = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExecutionWithDrawals_Bill", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ExecutionWithDrawals_Bill_Branch_BranchId",
                        column: x => x.BranchId,
                        principalTable: "Branch",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ExecutionWithDrawals_Bill_DealTypes_DealTypeId",
                        column: x => x.DealTypeId,
                        principalTable: "DealTypes",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ExecutionWithDrawals_Bill_Stores_StoreId",
                        column: x => x.StoreId,
                        principalTable: "Stores",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ExecutionWithDrawals_Items",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    ItemId = table.Column<int>(type: "int", nullable: true),
                    ModifiedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Price = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Quantity = table.Column<int>(type: "int", nullable: false),
                    SavedPrice = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    SerialNumber = table.Column<int>(type: "int", nullable: false),
                    Total = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExecutionWithDrawals_Items", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ExecutionWithDrawals_Items_Items_ItemId",
                        column: x => x.ItemId,
                        principalTable: "Items",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ExecutionWithDrawals_Bill_BranchId",
                table: "ExecutionWithDrawals_Bill",
                column: "BranchId");

            migrationBuilder.CreateIndex(
                name: "IX_ExecutionWithDrawals_Bill_DealTypeId",
                table: "ExecutionWithDrawals_Bill",
                column: "DealTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ExecutionWithDrawals_Bill_StoreId",
                table: "ExecutionWithDrawals_Bill",
                column: "StoreId");

            migrationBuilder.CreateIndex(
                name: "IX_ExecutionWithDrawals_Items_ItemId",
                table: "ExecutionWithDrawals_Items",
                column: "ItemId");
        }
    }
}
