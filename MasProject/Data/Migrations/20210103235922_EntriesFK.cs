﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class EntriesFK : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Entries_EntriesDetails_EntriesDetailsId",
                table: "Entries");

            migrationBuilder.DropIndex(
                name: "IX_Entries_EntriesDetailsId",
                table: "Entries");

            migrationBuilder.DropColumn(
                name: "AccountingEntryDetails",
                table: "Entries");

            migrationBuilder.DropColumn(
                name: "EntriesDetailsId",
                table: "Entries");

            migrationBuilder.AddColumn<int>(
                name: "EntriesId",
                table: "EntriesDetails",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_EntriesDetails_EntriesId",
                table: "EntriesDetails",
                column: "EntriesId");

            migrationBuilder.AddForeignKey(
                name: "FK_EntriesDetails_Entries_EntriesId",
                table: "EntriesDetails",
                column: "EntriesId",
                principalTable: "Entries",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EntriesDetails_Entries_EntriesId",
                table: "EntriesDetails");

            migrationBuilder.DropIndex(
                name: "IX_EntriesDetails_EntriesId",
                table: "EntriesDetails");

            migrationBuilder.DropColumn(
                name: "EntriesId",
                table: "EntriesDetails");

            migrationBuilder.AddColumn<int>(
                name: "AccountingEntryDetails",
                table: "Entries",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "EntriesDetailsId",
                table: "Entries",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Entries_EntriesDetailsId",
                table: "Entries",
                column: "EntriesDetailsId");

            migrationBuilder.AddForeignKey(
                name: "FK_Entries_EntriesDetails_EntriesDetailsId",
                table: "Entries",
                column: "EntriesDetailsId",
                principalTable: "EntriesDetails",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
