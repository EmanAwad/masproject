﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class EmployeeTransAndLoadPayment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.CreateTable(
            //    name: "AdditionNotice",
            //    columns: table => new
            //    {
            //        ID = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        IsDeleted = table.Column<bool>(nullable: false),
            //        CreatedBy = table.Column<string>(nullable: true),
            //        CreatedDate = table.Column<DateTime>(nullable: false),
            //        ModifiedBy = table.Column<string>(nullable: true),
            //        ModifiedDate = table.Column<DateTime>(nullable: false),
            //        Name = table.Column<string>(nullable: true),
            //        Note = table.Column<string>(nullable: true),
            //        SerialNumber = table.Column<int>(nullable: true),
            //        Date = table.Column<DateTime>(nullable: false),
            //        Amount = table.Column<double>(nullable: true),
            //        SupplierId = table.Column<int>(nullable: true),
            //        CurrencyId = table.Column<int>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_AdditionNotice", x => x.ID);
            //        table.ForeignKey(
            //            name: "FK_AdditionNotice_Currency_CurrencyId",
            //            column: x => x.CurrencyId,
            //            principalTable: "Currency",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //        table.ForeignKey(
            //            name: "FK_AdditionNotice_Supplier_SupplierId",
            //            column: x => x.SupplierId,
            //            principalTable: "Supplier",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "EmployeesLoansEntry",
            //    columns: table => new
            //    {
            //        ID = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        IsDeleted = table.Column<bool>(nullable: false),
            //        CreatedBy = table.Column<string>(nullable: true),
            //        CreatedDate = table.Column<DateTime>(nullable: false),
            //        ModifiedBy = table.Column<string>(nullable: true),
            //        ModifiedDate = table.Column<DateTime>(nullable: false),
            //        Name = table.Column<string>(nullable: true),
            //        Note = table.Column<string>(nullable: true),
            //        SerialNumber = table.Column<int>(nullable: true),
            //        Date = table.Column<DateTime>(nullable: false),
            //        TreasuryId = table.Column<int>(nullable: true),
            //        DocumentNo = table.Column<int>(nullable: false),
            //        EmployeeId = table.Column<int>(nullable: true),
            //        Amount = table.Column<double>(nullable: true),
            //        CurrencyId = table.Column<int>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_EmployeesLoansEntry", x => x.ID);
            //        table.ForeignKey(
            //            name: "FK_EmployeesLoansEntry_Currency_CurrencyId",
            //            column: x => x.CurrencyId,
            //            principalTable: "Currency",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //        table.ForeignKey(
            //            name: "FK_EmployeesLoansEntry_Employee_EmployeeId",
            //            column: x => x.EmployeeId,
            //            principalTable: "Employee",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //        table.ForeignKey(
            //            name: "FK_EmployeesLoansEntry_Treasuries_TreasuryId",
            //            column: x => x.TreasuryId,
            //            principalTable: "Treasuries",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            migrationBuilder.CreateTable(
                name: "EmployeesLoansPayment",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Note = table.Column<string>(nullable: true),
                    SerialNumber = table.Column<int>(nullable: true),
                    Date = table.Column<DateTime>(nullable: false),
                    TreasuryId = table.Column<int>(nullable: true),
                    DocumentNo = table.Column<int>(nullable: false),
                    EmployeeId = table.Column<int>(nullable: true),
                    Amount = table.Column<double>(nullable: true),
                    CurrencyId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeesLoansPayment", x => x.ID);
                    table.ForeignKey(
                        name: "FK_EmployeesLoansPayment_Currency_CurrencyId",
                        column: x => x.CurrencyId,
                        principalTable: "Currency",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EmployeesLoansPayment_Employee_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "Employee",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EmployeesLoansPayment_Treasuries_TreasuryId",
                        column: x => x.TreasuryId,
                        principalTable: "Treasuries",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            //migrationBuilder.CreateTable(
            //    name: "SupplierPayment",
            //    columns: table => new
            //    {
            //        ID = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        IsDeleted = table.Column<bool>(nullable: false),
            //        CreatedBy = table.Column<string>(nullable: true),
            //        CreatedDate = table.Column<DateTime>(nullable: false),
            //        ModifiedBy = table.Column<string>(nullable: true),
            //        ModifiedDate = table.Column<DateTime>(nullable: false),
            //        Name = table.Column<string>(nullable: true),
            //        Note = table.Column<string>(nullable: true),
            //        SerialNumber = table.Column<int>(nullable: true),
            //        Date = table.Column<DateTime>(nullable: false),
            //        Amount = table.Column<double>(nullable: true),
            //        Ratio = table.Column<double>(nullable: false),
            //        Total = table.Column<double>(nullable: true),
            //        IsCheck = table.Column<bool>(nullable: false),
            //        DueDate = table.Column<DateTime>(nullable: true),
            //        SupplierId = table.Column<int>(nullable: true),
            //        TreasuryId = table.Column<int>(nullable: true),
            //        CurrencyId = table.Column<int>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_SupplierPayment", x => x.ID);
            //        table.ForeignKey(
            //            name: "FK_SupplierPayment_Currency_CurrencyId",
            //            column: x => x.CurrencyId,
            //            principalTable: "Currency",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //        table.ForeignKey(
            //            name: "FK_SupplierPayment_Supplier_SupplierId",
            //            column: x => x.SupplierId,
            //            principalTable: "Supplier",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //        table.ForeignKey(
            //            name: "FK_SupplierPayment_Treasuries_TreasuryId",
            //            column: x => x.TreasuryId,
            //            principalTable: "Treasuries",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            migrationBuilder.CreateTable(
                name: "TransEmployee",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    SerialNumber = table.Column<int>(nullable: false),
                    Debit = table.Column<decimal>(nullable: false),
                    Credit = table.Column<decimal>(nullable: false),
                    Balance = table.Column<decimal>(nullable: false),
                    OpenBalFlag = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    EmployeeType = table.Column<int>(nullable: false),
                    EmployeeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransEmployee", x => x.ID);
                });

            //migrationBuilder.CreateIndex(
            //    name: "IX_AdditionNotice_CurrencyId",
            //    table: "AdditionNotice",
            //    column: "CurrencyId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_AdditionNotice_SupplierId",
            //    table: "AdditionNotice",
            //    column: "SupplierId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_EmployeesLoansEntry_CurrencyId",
            //    table: "EmployeesLoansEntry",
            //    column: "CurrencyId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_EmployeesLoansEntry_EmployeeId",
            //    table: "EmployeesLoansEntry",
            //    column: "EmployeeId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_EmployeesLoansEntry_TreasuryId",
            //    table: "EmployeesLoansEntry",
            //    column: "TreasuryId");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeesLoansPayment_CurrencyId",
                table: "EmployeesLoansPayment",
                column: "CurrencyId");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeesLoansPayment_EmployeeId",
                table: "EmployeesLoansPayment",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeesLoansPayment_TreasuryId",
                table: "EmployeesLoansPayment",
                column: "TreasuryId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_SupplierPayment_CurrencyId",
            //    table: "SupplierPayment",
            //    column: "CurrencyId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_SupplierPayment_SupplierId",
            //    table: "SupplierPayment",
            //    column: "SupplierId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_SupplierPayment_TreasuryId",
            //    table: "SupplierPayment",
            //    column: "TreasuryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropTable(
            //    name: "AdditionNotice");

            migrationBuilder.DropTable(
                name: "EmployeesLoansEntry");

            //migrationBuilder.DropTable(
            //    name: "EmployeesLoansPayment");

            //migrationBuilder.DropTable(
            //    name: "SupplierPayment");

            migrationBuilder.DropTable(
                name: "TransEmployee");
        }
    }
}
