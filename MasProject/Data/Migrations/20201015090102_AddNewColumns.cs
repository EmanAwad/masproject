﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class AddNewColumns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsConverted",
                table: "TransferOrder",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsConverted",
                table: "SalesReturns",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsConverted",
                table: "SalesBills",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsConverted",
                table: "PurchasesReturns_Bill",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsConverted",
                table: "PurchaseBill",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "BillSerialNumber",
                table: "PaymentVoucher",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BillType",
                table: "PaymentVoucher",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsConverted",
                table: "ExecutionWithDrawals_Bill",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "BillSerialNumber",
                table: "AdditionVoucher",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BillType",
                table: "AdditionVoucher",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsConverted",
                table: "TransferOrder");

            migrationBuilder.DropColumn(
                name: "IsConverted",
                table: "SalesReturns");

            migrationBuilder.DropColumn(
                name: "IsConverted",
                table: "SalesBills");

            migrationBuilder.DropColumn(
                name: "IsConverted",
                table: "Quotations");

            migrationBuilder.DropColumn(
                name: "IsConverted",
                table: "PurchasesReturns_Bill");

            migrationBuilder.DropColumn(
                name: "IsConverted",
                table: "PurchaseBill");

            migrationBuilder.DropColumn(
                name: "BillSerialNumber",
                table: "PaymentVoucher");

            migrationBuilder.DropColumn(
                name: "BillType",
                table: "PaymentVoucher");

            migrationBuilder.DropColumn(
                name: "IsConverted",
                table: "ExecutionWithDrawals_Bill");

            migrationBuilder.DropColumn(
                name: "BillSerialNumber",
                table: "AdditionVoucher");

            migrationBuilder.DropColumn(
                name: "BillType",
                table: "AdditionVoucher");
        }
    }
}
