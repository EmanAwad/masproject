﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class NotesEntries : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DetailDesc_Note",
                table: "EntriesDetails",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryDesc_Note",
                table: "Entries",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DetailDesc_Note",
                table: "EntriesDetails");

            migrationBuilder.DropColumn(
                name: "EntryDesc_Note",
                table: "Entries");
        }
    }
}
