﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class AddColsInClientPayment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Bank",
                table: "ClientPayment",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CollectionDate",
                table: "ClientPayment",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsCollected",
                table: "ClientPayment",
                nullable: false,
                defaultValue: false);

        }
        protected override void Down(MigrationBuilder migrationBuilder)
        {
          

            migrationBuilder.DropColumn(
                name: "Bank",
                table: "ClientPayment");

            migrationBuilder.DropColumn(
                name: "CollectionDate",
                table: "ClientPayment");

            migrationBuilder.DropColumn(
                name: "IsCollected",
                table: "ClientPayment");
        }
    }
}
