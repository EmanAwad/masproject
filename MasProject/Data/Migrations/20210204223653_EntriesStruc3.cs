﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class EntriesStruc3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EntriesNo",
                table: "EntriesStructuring");

            migrationBuilder.AddColumn<int>(
                name: "EntriesStructureNo",
                table: "EntriesStructuring",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EntriesStructureNo",
                table: "EntriesStructuring");

            migrationBuilder.AddColumn<int>(
                name: "EntriesNo",
                table: "EntriesStructuring",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
