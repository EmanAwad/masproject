﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class ItemsConversion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ItemConversion_Bill",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Note = table.Column<string>(nullable: true),
                    ConversionRecordID = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    EmployeeId = table.Column<int>(nullable: true),
                    ConversionReason = table.Column<string>(nullable: true),
                    StoreId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemConversion_Bill", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ItemConversion_Bill_Employee_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "Employee",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ItemConversion_Bill_Stores_StoreId",
                        column: x => x.StoreId,
                        principalTable: "Stores",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ItemConversion_Items",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Note = table.Column<string>(nullable: true),
                    ItemIdFrom = table.Column<int>(nullable: true),
                    ItemIdTo = table.Column<int>(nullable: true),
                    QuantityFrom = table.Column<int>(nullable: false),
                    QuantityTo = table.Column<int>(nullable: false),
                    SavedPriceTo = table.Column<decimal>(nullable: false),
                    SavedPriceFrom = table.Column<decimal>(nullable: false),
                    PriceTo = table.Column<decimal>(nullable: false),
                    PriceFrom = table.Column<decimal>(nullable: false),
                    TotalTo = table.Column<decimal>(nullable: false),
                    TotalFrom = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemConversion_Items", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ItemConversion_Items_Items_ItemIdFrom",
                        column: x => x.ItemIdFrom,
                        principalTable: "Items",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ItemConversion_Items_Items_ItemIdTo",
                        column: x => x.ItemIdTo,
                        principalTable: "Items",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ItemConversion_Bill_EmployeeId",
                table: "ItemConversion_Bill",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_ItemConversion_Bill_StoreId",
                table: "ItemConversion_Bill",
                column: "StoreId");

            migrationBuilder.CreateIndex(
                name: "IX_ItemConversion_Items_ItemIdFrom",
                table: "ItemConversion_Items",
                column: "ItemIdFrom");

            migrationBuilder.CreateIndex(
                name: "IX_ItemConversion_Items_ItemIdTo",
                table: "ItemConversion_Items",
                column: "ItemIdTo");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ItemConversion_Bill");

            migrationBuilder.DropTable(
                name: "ItemConversion_Items");
        }
    }
}
