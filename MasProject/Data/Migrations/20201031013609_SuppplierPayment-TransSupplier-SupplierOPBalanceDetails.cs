﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class SuppplierPaymentTransSupplierSupplierOPBalanceDetails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SupplierOpeningBalanceDetails",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Note = table.Column<string>(nullable: true),
                    SupplierDocumentNumber = table.Column<int>(nullable: true),
                    Amount = table.Column<double>(nullable: true),
                    SupplierID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SupplierOpeningBalanceDetails", x => x.ID);
                    table.ForeignKey(
                        name: "FK_SupplierOpeningBalanceDetails_Supplier_SupplierID",
                        column: x => x.SupplierID,
                        principalTable: "Supplier",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            //migrationBuilder.CreateTable(
            //    name: "SupplierPayment",
            //    columns: table => new
            //    {
            //        ID = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        IsDeleted = table.Column<bool>(nullable: false),
            //        CreatedBy = table.Column<string>(nullable: true),
            //        CreatedDate = table.Column<DateTime>(nullable: false),
            //        ModifiedBy = table.Column<string>(nullable: true),
            //        ModifiedDate = table.Column<DateTime>(nullable: false),
            //        Name = table.Column<string>(nullable: true),
            //        Note = table.Column<string>(nullable: true),
            //        SerialNumber = table.Column<int>(nullable: true),
            //        Date = table.Column<DateTime>(nullable: false),
            //        Amount = table.Column<double>(nullable: true),
            //        Ratio = table.Column<double>(nullable: false),
            //        Total = table.Column<double>(nullable: true),
            //        IsCheck = table.Column<bool>(nullable: false),
            //        DueDate = table.Column<DateTime>(nullable: true),
            //        SupplierId = table.Column<int>(nullable: true),
            //        TreasuryId = table.Column<int>(nullable: true),
            //        CurrencyId = table.Column<int>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_SupplierPayment", x => x.ID);
            //        table.ForeignKey(
            //            name: "FK_SupplierPayment_Currency_CurrencyId",
            //            column: x => x.CurrencyId,
            //            principalTable: "Currency",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //        table.ForeignKey(
            //            name: "FK_SupplierPayment_Supplier_SupplierId",
            //            column: x => x.SupplierId,
            //            principalTable: "Supplier",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //        table.ForeignKey(
            //            name: "FK_SupplierPayment_Treasuries_TreasuryId",
            //            column: x => x.TreasuryId,
            //            principalTable: "Treasuries",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            migrationBuilder.CreateTable(
                name: "TransSupplier",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    SerialNumber = table.Column<int>(nullable: false),
                    Debit = table.Column<decimal>(nullable: false),
                    Credit = table.Column<decimal>(nullable: false),
                    Balance = table.Column<decimal>(nullable: false),
                    OpenBalFlag = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    SupplierType = table.Column<int>(nullable: false),
                    SupplierId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransSupplier", x => x.ID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SupplierOpeningBalanceDetails_SupplierID",
                table: "SupplierOpeningBalanceDetails",
                column: "SupplierID");

            //migrationBuilder.CreateIndex(
            //    name: "IX_SupplierPayment_CurrencyId",
            //    table: "SupplierPayment",
            //    column: "CurrencyId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_SupplierPayment_SupplierId",
            //    table: "SupplierPayment",
            //    column: "SupplierId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_SupplierPayment_TreasuryId",
            //    table: "SupplierPayment",
            //    column: "TreasuryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SupplierOpeningBalanceDetails");

            //migrationBuilder.DropTable(
            //    name: "SupplierPayment");

            migrationBuilder.DropTable(
                name: "TransSupplier");
        }
    }
}
