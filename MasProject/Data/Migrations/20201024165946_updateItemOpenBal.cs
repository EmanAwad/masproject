﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class updateItemOpenBal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ItemsOpeningBalance_Items_ItemId",
                table: "ItemsOpeningBalance");

            migrationBuilder.DropForeignKey(
                name: "FK_ItemsOpeningBalance_Stores_StoreId",
                table: "ItemsOpeningBalance");

            migrationBuilder.DropIndex(
                name: "IX_ItemsOpeningBalance_ItemId",
                table: "ItemsOpeningBalance");

            migrationBuilder.DropColumn(
                name: "ItemId",
                table: "ItemsOpeningBalance");

            migrationBuilder.DropColumn(
                name: "Quantity",
                table: "ItemsOpeningBalance");

            migrationBuilder.DropColumn(
                name: "TotalPrice",
                table: "ItemsOpeningBalance");

            migrationBuilder.AlterColumn<int>(
                name: "StoreId",
                table: "ItemsOpeningBalance",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<string>(
                name: "DocumentNumber",
                table: "ItemsOpeningBalance",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "EmployeeId",
                table: "ItemsOpeningBalance",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Notes",
                table: "ItemsOpeningBalance",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ItemsOpeningBalanceDetails",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ItemDocumentNumber = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    ItemId = table.Column<int>(nullable: true),
                    price = table.Column<double>(nullable: false),
                    TotalPrice = table.Column<float>(nullable: false),
                    Quantity = table.Column<int>(nullable: false),
                    Notes = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemsOpeningBalanceDetails", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ItemsOpeningBalanceDetails_Items_ItemId",
                        column: x => x.ItemId,
                        principalTable: "Items",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ItemsOpeningBalance_EmployeeId",
                table: "ItemsOpeningBalance",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_ItemsOpeningBalanceDetails_ItemId",
                table: "ItemsOpeningBalanceDetails",
                column: "ItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_ItemsOpeningBalance_Employee_EmployeeId",
                table: "ItemsOpeningBalance",
                column: "EmployeeId",
                principalTable: "Employee",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ItemsOpeningBalance_Stores_StoreId",
                table: "ItemsOpeningBalance",
                column: "StoreId",
                principalTable: "Stores",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ItemsOpeningBalance_Employee_EmployeeId",
                table: "ItemsOpeningBalance");

            migrationBuilder.DropForeignKey(
                name: "FK_ItemsOpeningBalance_Stores_StoreId",
                table: "ItemsOpeningBalance");

            migrationBuilder.DropTable(
                name: "ItemsOpeningBalanceDetails");

            migrationBuilder.DropIndex(
                name: "IX_ItemsOpeningBalance_EmployeeId",
                table: "ItemsOpeningBalance");

            migrationBuilder.DropColumn(
                name: "DocumentNumber",
                table: "ItemsOpeningBalance");

            migrationBuilder.DropColumn(
                name: "EmployeeId",
                table: "ItemsOpeningBalance");

            migrationBuilder.DropColumn(
                name: "Notes",
                table: "ItemsOpeningBalance");

            migrationBuilder.AlterColumn<int>(
                name: "StoreId",
                table: "ItemsOpeningBalance",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ItemId",
                table: "ItemsOpeningBalance",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Quantity",
                table: "ItemsOpeningBalance",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<float>(
                name: "TotalPrice",
                table: "ItemsOpeningBalance",
                type: "real",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.CreateIndex(
                name: "IX_ItemsOpeningBalance_ItemId",
                table: "ItemsOpeningBalance",
                column: "ItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_ItemsOpeningBalance_Items_ItemId",
                table: "ItemsOpeningBalance",
                column: "ItemId",
                principalTable: "Items",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ItemsOpeningBalance_Stores_StoreId",
                table: "ItemsOpeningBalance",
                column: "StoreId",
                principalTable: "Stores",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
