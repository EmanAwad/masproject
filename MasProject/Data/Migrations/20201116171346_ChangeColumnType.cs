﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class ChangeColumnType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.AlterColumn<int>(
                name: "DocumentNumber",
                table: "ItemsOpeningBalance",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);


            migrationBuilder.AlterColumn<int>(
                name: "ClientDocumentNumber",
                table: "ClientOpeningBalanceDetails",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "DocumentNumber",
                table: "ClientOpeningBalance",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

           
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TransSupplier");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "TreasuryOpenBalances");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "TreasuryOpenBalances");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Treasuries");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Treasuries");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "TransferRequestItem");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "TransferRequestItem");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "TransferRequest");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "TransferRequest");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "TransferOrderDetails");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "TransferOrderDetails");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "TransferOrder");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "TransferOrder");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "TransEmployee");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "TransEmployee");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "TransClient");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "TransClient");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "SupplierPayment");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "SupplierPayment");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "SupplierOpeningBalance");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "SupplierOpeningBalance");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Supplier");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Supplier");

            migrationBuilder.DropColumn(
                name: "arrangement",
                table: "Supplier");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Stores");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Stores");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "SocialMEdia");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "SocialMEdia");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Social_Client");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Social_Client");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "SellingExpenses");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "SellingExpenses");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "SalesSellExpenses");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "SalesSellExpenses");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "SalesReturnsItems");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "SalesReturnsItems");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "SalesReturnsExpenses");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "SalesReturnsExpenses");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "SalesReturns");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "SalesReturns");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "SalesItems");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "SalesItems");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "SalesBills");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "SalesBills");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "SalesArchives");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "SalesArchives");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Quotations");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Quotations");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "QuotationItems");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "QuotationItems");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "PurchasesReturns_Items");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "PurchasesReturns_Items");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "PurchasesReturns_Bill");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "PurchasesReturns_Bill");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "PurchasesArchives");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "PurchasesArchives");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "PurchaseReturns_OtherIncome");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "PurchaseReturns_OtherIncome");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "PurchaseOtherIncome");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "PurchaseOtherIncome");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "PurchaseItem");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "PurchaseItem");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "PurchaseBill");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "PurchaseBill");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "PaymentVoucherItem");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "PaymentVoucherItem");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "PaymentVoucher");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "PaymentVoucher");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "OtherIncome");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "OtherIncome");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "OtherConstant");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "OtherConstant");

            migrationBuilder.DropColumn(
                name: "arrangement",
                table: "OtherConstant");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "MantienceItems");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "MantienceItems");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "MantienceBills");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "MantienceBills");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "MaintanceSellExpenses");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "MaintanceSellExpenses");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Lookup_Country");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Lookup_Country");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Lookup_City");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Lookup_City");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Lookup_Area");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Lookup_Area");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "ItemTypes");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "ItemTypes");

            migrationBuilder.DropColumn(
                name: "arrangement",
                table: "ItemTypes");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "ItemsOpeningBalance");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "ItemsOpeningBalance");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "ItemsCollection");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "ItemsCollection");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Items");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Items");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Item_Store");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Item_Store");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Item_PriceHistory");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Item_PriceHistory");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "ExecutiveSellExpenses");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "ExecutiveSellExpenses");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "ExecutiveItems");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "ExecutiveItems");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "ExecutiveBills");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "ExecutiveBills");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "ExecutionWithDrawals_Items");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "ExecutionWithDrawals_Items");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "ExecutionWithDrawals_Bill");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "ExecutionWithDrawals_Bill");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "EmployeesLoansPayment");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "EmployeesLoansPayment");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "EmployeesLoansEntry");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "EmployeesLoansEntry");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "EmployeeOpeningBalance");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "EmployeeOpeningBalance");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "arrangement",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "DiscountNotice");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "DiscountNotice");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Department");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Department");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "DealTypes");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "DealTypes");

            migrationBuilder.DropColumn(
                name: "arrangement",
                table: "DealTypes");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Currency");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Currency");

            migrationBuilder.DropColumn(
                name: "arrangement",
                table: "Currency");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "ClientPayment");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "ClientPayment");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "ClientOpeningBalanceDetails");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "ClientOpeningBalanceDetails");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "ClientOpeningBalance");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "ClientOpeningBalance");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Client");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Client");

            migrationBuilder.DropColumn(
                name: "arrangement",
                table: "Client");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Branch");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Branch");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "AdditionVoucherItem");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "AdditionVoucherItem");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "AdditionVoucher");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "AdditionVoucher");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "AdditionNotice");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "AdditionNotice");

            migrationBuilder.AlterColumn<string>(
                name: "DocumentNumber",
                table: "ItemsOpeningBalance",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "CurrencyId",
                table: "EmployeesLoansPayment",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CurrencyId",
                table: "EmployeesLoansEntry",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ClientDocumentNumber",
                table: "ClientOpeningBalanceDetails",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "DocumentNumber",
                table: "ClientOpeningBalance",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.CreateIndex(
                name: "IX_EmployeesLoansPayment_CurrencyId",
                table: "EmployeesLoansPayment",
                column: "CurrencyId");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeesLoansEntry_CurrencyId",
                table: "EmployeesLoansEntry",
                column: "CurrencyId");

            migrationBuilder.AddForeignKey(
                name: "FK_EmployeesLoansEntry_Currency_CurrencyId",
                table: "EmployeesLoansEntry",
                column: "CurrencyId",
                principalTable: "Currency",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_EmployeesLoansPayment_Currency_CurrencyId",
                table: "EmployeesLoansPayment",
                column: "CurrencyId",
                principalTable: "Currency",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
