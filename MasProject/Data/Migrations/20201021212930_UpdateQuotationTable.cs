﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class UpdateQuotationTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Quotations_Client_ClientId",
                table: "Quotations");

            migrationBuilder.DropIndex(
                name: "IX_Quotations_ClientId",
                table: "Quotations");

            migrationBuilder.DropColumn(
                name: "ClientId",
                table: "Quotations");

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "Quotations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Client",
                table: "Quotations",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Quotations_BranchId",
                table: "Quotations",
                column: "BranchId");

            migrationBuilder.AddForeignKey(
                name: "FK_Quotations_Branch_BranchId",
                table: "Quotations",
                column: "BranchId",
                principalTable: "Branch",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Quotations_Branch_BranchId",
                table: "Quotations");

            migrationBuilder.DropIndex(
                name: "IX_Quotations_BranchId",
                table: "Quotations");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "Quotations");

            migrationBuilder.DropColumn(
                name: "Client",
                table: "Quotations");

            migrationBuilder.AddColumn<int>(
                name: "ClientId",
                table: "Quotations",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Quotations_ClientId",
                table: "Quotations",
                column: "ClientId");

            migrationBuilder.AddForeignKey(
                name: "FK_Quotations_Client_ClientId",
                table: "Quotations",
                column: "ClientId",
                principalTable: "Client",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
