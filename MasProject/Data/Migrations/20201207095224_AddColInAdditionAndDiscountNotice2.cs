﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class AddColInAdditionAndDiscountNotice2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SupplierId",
                table: "DiscountNotice",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ClientId",
                table: "AdditionNotice",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_DiscountNotice_SupplierId",
                table: "DiscountNotice",
                column: "SupplierId");

            migrationBuilder.CreateIndex(
                name: "IX_AdditionNotice_ClientId",
                table: "AdditionNotice",
                column: "ClientId");

            migrationBuilder.AddForeignKey(
                name: "FK_AdditionNotice_Client_ClientId",
                table: "AdditionNotice",
                column: "ClientId",
                principalTable: "Client",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_DiscountNotice_Supplier_SupplierId",
                table: "DiscountNotice",
                column: "SupplierId",
                principalTable: "Supplier",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AdditionNotice_Client_ClientId",
                table: "AdditionNotice");

            migrationBuilder.DropForeignKey(
                name: "FK_DiscountNotice_Supplier_SupplierId",
                table: "DiscountNotice");

            migrationBuilder.DropIndex(
                name: "IX_DiscountNotice_SupplierId",
                table: "DiscountNotice");

            migrationBuilder.DropIndex(
                name: "IX_AdditionNotice_ClientId",
                table: "AdditionNotice");

            migrationBuilder.DropColumn(
                name: "SupplierId",
                table: "DiscountNotice");

            migrationBuilder.DropColumn(
                name: "ClientId",
                table: "AdditionNotice");
        }
    }
}
