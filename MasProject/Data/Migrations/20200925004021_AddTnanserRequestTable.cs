﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class AddTnanserRequestTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TransferRequest",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Note = table.Column<string>(nullable: true),
                    StoreIDTo = table.Column<int>(nullable: true),
                    IsOrder = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransferRequest", x => x.ID);
                    table.ForeignKey(
                        name: "FK_TransferRequest_Stores_StoreIDTo",
                        column: x => x.StoreIDTo,
                        principalTable: "Stores",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            //migrationBuilder.CreateTable(
            //    name: "TransferRequestItem",
            //    columns: table => new
            //    {
            //        ID = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        Name = table.Column<string>(nullable: true),
            //        IsDeleted = table.Column<bool>(nullable: false),
            //        CreatedBy = table.Column<string>(nullable: true),
            //        CreatedDate = table.Column<DateTime>(nullable: false),
            //        ModifiedBy = table.Column<string>(nullable: true),
            //        ModifiedDate = table.Column<DateTime>(nullable: false),
            //        Note = table.Column<string>(nullable: true),
            //        RequiredQuantity = table.Column<double>(nullable: false),
            //        ItemId = table.Column<int>(nullable: true),
            //        StoreIDFrom = table.Column<int>(nullable: true),
            //        TransferRequestId = table.Column<int>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_TransferRequestItem", x => x.ID);
            //        table.ForeignKey(
            //            name: "FK_TransferRequestItem_Items_ItemId",
            //            column: x => x.ItemId,
            //            principalTable: "Items",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //        table.ForeignKey(
            //            name: "FK_TransferRequestItem_Stores_StoreIDFrom",
            //            column: x => x.StoreIDFrom,
            //            principalTable: "Stores",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //        table.ForeignKey(
            //            name: "FK_TransferRequestItem_TransferRequest_TransferRequestId",
            //            column: x => x.TransferRequestId,
            //            principalTable: "TransferRequest",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            migrationBuilder.CreateIndex(
                name: "IX_TransferRequest_StoreIDTo",
                table: "TransferRequest",
                column: "StoreIDTo");

            //migrationBuilder.CreateIndex(
            //    name: "IX_TransferRequestItem_ItemId",
            //    table: "TransferRequestItem",
            //    column: "ItemId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_TransferRequestItem_StoreIDFrom",
            //    table: "TransferRequestItem",
            //    column: "StoreIDFrom");

            //migrationBuilder.CreateIndex(
            //    name: "IX_TransferRequestItem_TransferRequestId",
            //    table: "TransferRequestItem",
            //    column: "TransferRequestId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropTable(
            //    name: "TransferRequestItem");

            //migrationBuilder.DropTable(
            //    name: "TransferRequest");
        }
    }
}
