﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class adnewtbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ExecutiveOtherIncome",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    DeletedBy = table.Column<string>(nullable: true),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    IncomeId = table.Column<int>(nullable: true),
                    Amount = table.Column<decimal>(nullable: true),
                    Identifer = table.Column<int>(nullable: true),
                    ExecutiveBillID = table.Column<int>(nullable: true),
                    SerialNumber = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExecutiveOtherIncome", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ExecutiveOtherIncome_ExecutiveBills_ExecutiveBillID",
                        column: x => x.ExecutiveBillID,
                        principalTable: "ExecutiveBills",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ExecutiveOtherIncome_OtherIncome_IncomeId",
                        column: x => x.IncomeId,
                        principalTable: "OtherIncome",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ExecutiveOtherIncome_ExecutiveBillID",
                table: "ExecutiveOtherIncome",
                column: "ExecutiveBillID");

            migrationBuilder.CreateIndex(
                name: "IX_ExecutiveOtherIncome_IncomeId",
                table: "ExecutiveOtherIncome",
                column: "IncomeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExecutiveOtherIncome");
        }
    }
}
