﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class ExecutionWithDrawals : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<float>(
                name: "CostPrice",
                table: "Items",
                nullable: true,
                oldClrType: typeof(float),
                oldType: "real");

            migrationBuilder.CreateTable(
                name: "ExecutionWithDrawals_Bill",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Note = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    SerialNumber = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    StoreId = table.Column<int>(nullable: true),
                    DealTypeId = table.Column<int>(nullable: true),
                    FlagType = table.Column<bool>(nullable: false),
                    HandDocumentNumber = table.Column<int>(nullable: true),
                    TotalPrice = table.Column<decimal>(nullable: true),
                    DiscountPrecentage = table.Column<decimal>(nullable: true),
                    DiscountValue = table.Column<decimal>(nullable: true),
                    TotalAfterDiscount = table.Column<decimal>(nullable: true),
                    ShowNotesFlag = table.Column<bool>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    RevewId = table.Column<int>(nullable: false),
                    ApproveId = table.Column<int>(nullable: false),
                    BranchId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExecutionWithDrawals_Bill", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ExecutionWithDrawals_Bill_Branch_BranchId",
                        column: x => x.BranchId,
                        principalTable: "Branch",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ExecutionWithDrawals_Bill_DealTypes_DealTypeId",
                        column: x => x.DealTypeId,
                        principalTable: "DealTypes",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ExecutionWithDrawals_Bill_Stores_StoreId",
                        column: x => x.StoreId,
                        principalTable: "Stores",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ExecutionWithDrawals_Items",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ItemId = table.Column<int>(nullable: true),
                    Quantity = table.Column<int>(nullable: true),
                    Price = table.Column<decimal>(nullable: true),
                    Total = table.Column<decimal>(nullable: true),
                    SavedPrice = table.Column<decimal>(nullable: true),
                    SerialNumber = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExecutionWithDrawals_Items", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ExecutionWithDrawals_Items_Items_ItemId",
                        column: x => x.ItemId,
                        principalTable: "Items",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            //migrationBuilder.CreateTable(
            //    name: "Quotations",
            //    columns: table => new
            //    {
            //        ID = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        IsDeleted = table.Column<bool>(nullable: false),
            //        CreatedBy = table.Column<string>(nullable: true),
            //        CreatedDate = table.Column<DateTime>(nullable: false),
            //        ModifiedBy = table.Column<string>(nullable: true),
            //        ModifiedDate = table.Column<DateTime>(nullable: false),
            //        Note = table.Column<string>(nullable: true),
            //        Name = table.Column<string>(nullable: true),
            //        SerialNumber = table.Column<int>(nullable: false),
            //        Date = table.Column<DateTime>(nullable: false),
            //        StoreId = table.Column<int>(nullable: true),
            //        DealTypeId = table.Column<int>(nullable: true),
            //        FlagType = table.Column<bool>(nullable: false),
            //        HandDocumentNumber = table.Column<int>(nullable: false),
            //        TotalPrice = table.Column<decimal>(nullable: false),
            //        DiscountPrecentage = table.Column<decimal>(nullable: false),
            //        DiscountValue = table.Column<decimal>(nullable: false),
            //        TotalAfterDiscount = table.Column<decimal>(nullable: false),
            //        ShowNotesFlag = table.Column<bool>(nullable: false),
            //        UserId = table.Column<int>(nullable: false),
            //        RevewId = table.Column<int>(nullable: false),
            //        ApproveId = table.Column<int>(nullable: false),
            //        ClientId = table.Column<int>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Quotations", x => x.ID);
            //        table.ForeignKey(
            //            name: "FK_Quotations_Client_ClientId",
            //            column: x => x.ClientId,
            //            principalTable: "Client",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //        table.ForeignKey(
            //            name: "FK_Quotations_DealTypes_DealTypeId",
            //            column: x => x.DealTypeId,
            //            principalTable: "DealTypes",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //        table.ForeignKey(
            //            name: "FK_Quotations_Stores_StoreId",
            //            column: x => x.StoreId,
            //            principalTable: "Stores",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            migrationBuilder.CreateIndex(
                name: "IX_ExecutionWithDrawals_Bill_BranchId",
                table: "ExecutionWithDrawals_Bill",
                column: "BranchId");

            migrationBuilder.CreateIndex(
                name: "IX_ExecutionWithDrawals_Bill_DealTypeId",
                table: "ExecutionWithDrawals_Bill",
                column: "DealTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ExecutionWithDrawals_Bill_StoreId",
                table: "ExecutionWithDrawals_Bill",
                column: "StoreId");

            migrationBuilder.CreateIndex(
                name: "IX_ExecutionWithDrawals_Items_ItemId",
                table: "ExecutionWithDrawals_Items",
                column: "ItemId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Quotations_ClientId",
            //    table: "Quotations",
            //    column: "ClientId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Quotations_DealTypeId",
            //    table: "Quotations",
            //    column: "DealTypeId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Quotations_StoreId",
            //    table: "Quotations",
            //    column: "StoreId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExecutionWithDrawals_Bill");

            migrationBuilder.DropTable(
                name: "ExecutionWithDrawals_Items");

            //migrationBuilder.DropTable(
            //    name: "Quotations");

            migrationBuilder.AlterColumn<float>(
                name: "CostPrice",
                table: "Items",
                type: "real",
                nullable: false,
                oldClrType: typeof(float),
                oldNullable: true);
        }
    }
}
