﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class AddNewColumnsToClientOpenningBal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClientOpeningBalance_Currency_CurrencyID",
                table: "ClientOpeningBalance");

            migrationBuilder.DropIndex(
                name: "IX_ClientOpeningBalance_CurrencyID",
                table: "ClientOpeningBalance");

            migrationBuilder.DropColumn(
                name: "CurrencyID",
                table: "ClientOpeningBalance");

            migrationBuilder.DropColumn(
                name: "Ratio",
                table: "ClientOpeningBalance");

            migrationBuilder.AddColumn<int>(
                name: "BranchID",
                table: "ClientOpeningBalance",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DocumentNumber",
                table: "ClientOpeningBalance",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "EmployeeID",
                table: "ClientOpeningBalance",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "tbl_Notes",
                table: "ClientOpeningBalance",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ClientOpeningBalance_BranchID",
                table: "ClientOpeningBalance",
                column: "BranchID");

            migrationBuilder.CreateIndex(
                name: "IX_ClientOpeningBalance_EmployeeID",
                table: "ClientOpeningBalance",
                column: "EmployeeID");

            migrationBuilder.AddForeignKey(
                name: "FK_ClientOpeningBalance_Branch_BranchID",
                table: "ClientOpeningBalance",
                column: "BranchID",
                principalTable: "Branch",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ClientOpeningBalance_Employee_EmployeeID",
                table: "ClientOpeningBalance",
                column: "EmployeeID",
                principalTable: "Employee",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClientOpeningBalance_Branch_BranchID",
                table: "ClientOpeningBalance");

            migrationBuilder.DropForeignKey(
                name: "FK_ClientOpeningBalance_Employee_EmployeeID",
                table: "ClientOpeningBalance");

            migrationBuilder.DropIndex(
                name: "IX_ClientOpeningBalance_BranchID",
                table: "ClientOpeningBalance");

            migrationBuilder.DropIndex(
                name: "IX_ClientOpeningBalance_EmployeeID",
                table: "ClientOpeningBalance");

            migrationBuilder.DropColumn(
                name: "BranchID",
                table: "ClientOpeningBalance");

            migrationBuilder.DropColumn(
                name: "DocumentNumber",
                table: "ClientOpeningBalance");

            migrationBuilder.DropColumn(
                name: "EmployeeID",
                table: "ClientOpeningBalance");

            migrationBuilder.DropColumn(
                name: "tbl_Notes",
                table: "ClientOpeningBalance");

            migrationBuilder.AddColumn<int>(
                name: "CurrencyID",
                table: "ClientOpeningBalance",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "Ratio",
                table: "ClientOpeningBalance",
                type: "float",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ClientOpeningBalance_CurrencyID",
                table: "ClientOpeningBalance",
                column: "CurrencyID");

            migrationBuilder.AddForeignKey(
                name: "FK_ClientOpeningBalance_Currency_CurrencyID",
                table: "ClientOpeningBalance",
                column: "CurrencyID",
                principalTable: "Currency",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
