﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class SupplierCols : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "EmployeeName",
                table: "Supplier",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WebSiteURL",
                table: "Supplier",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EmployeeName",
                table: "Supplier");

            migrationBuilder.DropColumn(
                name: "WebSiteURL",
                table: "Supplier");
        }
    }
}
