﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class PurchaseReturns_OtherIncome : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PurchaseReturns_OtherIncome",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    IncomeId = table.Column<int>(nullable: true),
                    Amount = table.Column<decimal>(nullable: true),
                    Identifer = table.Column<int>(nullable: true),
                    PurchasesReturnsID = table.Column<int>(nullable: true),
                    SerialNumber = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PurchaseReturns_OtherIncome", x => x.ID);
                    table.ForeignKey(
                        name: "FK_PurchaseReturns_OtherIncome_OtherIncome_IncomeId",
                        column: x => x.IncomeId,
                        principalTable: "OtherIncome",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PurchaseReturns_OtherIncome_PurchasesReturns_Bill_PurchasesReturnsID",
                        column: x => x.PurchasesReturnsID,
                        principalTable: "PurchasesReturns_Bill",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseReturns_OtherIncome_IncomeId",
                table: "PurchaseReturns_OtherIncome",
                column: "IncomeId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseReturns_OtherIncome_PurchasesReturnsID",
                table: "PurchaseReturns_OtherIncome",
                column: "PurchasesReturnsID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PurchaseReturns_OtherIncome");
        }
    }
}
