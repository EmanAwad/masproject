﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class AddClientPaymentTbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
           
          
            migrationBuilder.CreateTable(
                name: "ClientPayment",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Note = table.Column<string>(nullable: true),
                    SerialNumber = table.Column<int>(nullable: true),
                    Date = table.Column<DateTime>(nullable: false),
                    Amount = table.Column<double>(nullable: true),
                    Ratio = table.Column<double>(nullable: false),
                    Total = table.Column<double>(nullable: true),
                    IsCheck = table.Column<bool>(nullable: false),
                    DueDate = table.Column<DateTime>(nullable: true),
                    ClientId = table.Column<int>(nullable: true),
                    TreasuryId = table.Column<int>(nullable: true),
                    CurrencyId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientPayment", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ClientPayment_Client_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Client",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClientPayment_Currency_CurrencyId",
                        column: x => x.CurrencyId,
                        principalTable: "Currency",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClientPayment_Treasuries_TreasuryId",
                        column: x => x.TreasuryId,
                        principalTable: "Treasuries",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            

            migrationBuilder.CreateIndex(
                name: "IX_ClientPayment_ClientId",
                table: "ClientPayment",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientPayment_CurrencyId",
                table: "ClientPayment",
                column: "CurrencyId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientPayment_TreasuryId",
                table: "ClientPayment",
                column: "TreasuryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
           
            migrationBuilder.DropTable(
                name: "ClientPayment");

        }
    }
}
