﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class AddColInAdditionAndDiscountNotice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UserTypeFlag",
                table: "DiscountNotice",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserTypeFlag",
                table: "AdditionNotice",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserTypeFlag",
                table: "DiscountNotice");

            migrationBuilder.DropColumn(
                name: "UserTypeFlag",
                table: "AdditionNotice");
        }
    }
}
