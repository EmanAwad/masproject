﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class UpdateTransferRequestTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransferRequest_Stores_StoreIDTo",
                table: "TransferRequest");

            migrationBuilder.DropForeignKey(
                name: "FK_TransferRequestItem_Stores_StoreIDFrom",
                table: "TransferRequestItem");

            migrationBuilder.DropIndex(
                name: "IX_TransferRequest_StoreIDTo",
                table: "TransferRequest");

            migrationBuilder.DropColumn(
                name: "StoreIDTo",
                table: "TransferRequest");

            migrationBuilder.AlterColumn<double>(
                name: "RequiredQuantity",
                table: "TransferRequestItem",
                nullable: true,
                oldClrType: typeof(double),
                oldType: "float");

            migrationBuilder.AddColumn<int>(
                name: "BranchFromID",
                table: "TransferRequestItem",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchToID",
                table: "TransferRequest",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Date",
                table: "TransferRequest",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "SerialNumber",
                table: "TransferRequest",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TransferRequest_BranchToID",
                table: "TransferRequest",
                column: "BranchToID");

            migrationBuilder.AddForeignKey(
                name: "FK_TransferRequest_Branch_BranchToID",
                table: "TransferRequest",
                column: "BranchToID",
                principalTable: "Branch",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TransferRequestItem_Branch_BranchFromID",
                table: "TransferRequestItem",
                column: "BranchFromID",
                principalTable: "Branch",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransferRequest_Branch_BranchToID",
                table: "TransferRequest");

            migrationBuilder.DropForeignKey(
                name: "FK_TransferRequestItem_Branch_StoreIDFrom",
                table: "TransferRequestItem");

            migrationBuilder.DropIndex(
                name: "IX_TransferRequest_BranchToID",
                table: "TransferRequest");

            migrationBuilder.DropColumn(
                name: "BranchFromID",
                table: "TransferRequestItem");

            migrationBuilder.DropColumn(
                name: "BranchToID",
                table: "TransferRequest");

            migrationBuilder.DropColumn(
                name: "Date",
                table: "TransferRequest");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "TransferRequest");

            migrationBuilder.AlterColumn<double>(
                name: "RequiredQuantity",
                table: "TransferRequestItem",
                type: "float",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StoreIDTo",
                table: "TransferRequest",
                type: "int",
                nullable: true);


            migrationBuilder.CreateIndex(
                name: "IX_TransferRequest_StoreIDTo",
                table: "TransferRequest",
                column: "StoreIDTo");

            migrationBuilder.AddForeignKey(
                name: "FK_TransferRequest_Stores_StoreIDTo",
                table: "TransferRequest",
                column: "StoreIDTo",
                principalTable: "Stores",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TransferRequestItem_Stores_StoreIDFrom",
                table: "TransferRequestItem",
                column: "StoreIDFrom",
                principalTable: "Stores",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
