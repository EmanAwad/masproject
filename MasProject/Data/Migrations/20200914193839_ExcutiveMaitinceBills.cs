﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class ExcutiveMaitinceBills : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
           
            migrationBuilder.CreateTable(
                name: "ExecutiveBills",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Note = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    SerialNumber = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    StoreId = table.Column<int>(nullable: true),
                    DealTypeId = table.Column<int>(nullable: true),
                    FlagType = table.Column<bool>(nullable: false),
                    HandDocumentNumber = table.Column<int>(nullable: false),
                    TotalPrice = table.Column<decimal>(nullable: false),
                    DiscountPrecentage = table.Column<decimal>(nullable: false),
                    DiscountValue = table.Column<decimal>(nullable: false),
                    TotalAfterDiscount = table.Column<decimal>(nullable: false),
                    ShowNotesFlag = table.Column<bool>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    RevewId = table.Column<int>(nullable: false),
                    ApproveId = table.Column<int>(nullable: false),
                    ClientId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExecutiveBills", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ExecutiveBills_Client_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Client",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ExecutiveBills_DealTypes_DealTypeId",
                        column: x => x.DealTypeId,
                        principalTable: "DealTypes",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ExecutiveBills_Stores_StoreId",
                        column: x => x.StoreId,
                        principalTable: "Stores",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ExecutiveItems",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ItemId = table.Column<int>(nullable: true),
                    Quantity = table.Column<int>(nullable: false),
                    Price = table.Column<decimal>(nullable: false),
                    Total = table.Column<decimal>(nullable: false),
                    SavedPrice = table.Column<decimal>(nullable: false),
                    SerialNumber = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExecutiveItems", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ExecutiveItems_Items_ItemId",
                        column: x => x.ItemId,
                        principalTable: "Items",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MantienceBills",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Note = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    SerialNumber = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    StoreId = table.Column<int>(nullable: true),
                    DealTypeId = table.Column<int>(nullable: true),
                    FlagType = table.Column<bool>(nullable: false),
                    HandDocumentNumber = table.Column<int>(nullable: false),
                    TotalPrice = table.Column<decimal>(nullable: false),
                    DiscountPrecentage = table.Column<decimal>(nullable: false),
                    DiscountValue = table.Column<decimal>(nullable: false),
                    TotalAfterDiscount = table.Column<decimal>(nullable: false),
                    ShowNotesFlag = table.Column<bool>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    RevewId = table.Column<int>(nullable: false),
                    ApproveId = table.Column<int>(nullable: false),
                    ClientId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MantienceBills", x => x.ID);
                    table.ForeignKey(
                        name: "FK_MantienceBills_Client_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Client",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MantienceBills_DealTypes_DealTypeId",
                        column: x => x.DealTypeId,
                        principalTable: "DealTypes",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MantienceBills_Stores_StoreId",
                        column: x => x.StoreId,
                        principalTable: "Stores",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MantienceItems",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ItemId = table.Column<int>(nullable: true),
                    Quantity = table.Column<int>(nullable: false),
                    Price = table.Column<decimal>(nullable: false),
                    Total = table.Column<decimal>(nullable: false),
                    SavedPrice = table.Column<decimal>(nullable: false),
                    SerialNumber = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MantienceItems", x => x.ID);
                    table.ForeignKey(
                        name: "FK_MantienceItems_Items_ItemId",
                        column: x => x.ItemId,
                        principalTable: "Items",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SalesReturns",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Note = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    SerialNumber = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    StoreId = table.Column<int>(nullable: true),
                    DealTypeId = table.Column<int>(nullable: true),
                    FlagType = table.Column<bool>(nullable: false),
                    HandDocumentNumber = table.Column<int>(nullable: false),
                    TotalPrice = table.Column<decimal>(nullable: false),
                    DiscountPrecentage = table.Column<decimal>(nullable: false),
                    DiscountValue = table.Column<decimal>(nullable: false),
                    TotalAfterDiscount = table.Column<decimal>(nullable: false),
                    ShowNotesFlag = table.Column<bool>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    RevewId = table.Column<int>(nullable: false),
                    ApproveId = table.Column<int>(nullable: false),
                    ClientId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SalesReturns", x => x.ID);
                    table.ForeignKey(
                        name: "FK_SalesReturns_Client_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Client",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SalesReturns_DealTypes_DealTypeId",
                        column: x => x.DealTypeId,
                        principalTable: "DealTypes",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SalesReturns_Stores_StoreId",
                        column: x => x.StoreId,
                        principalTable: "Stores",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SalesReturnsItems",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ItemId = table.Column<int>(nullable: true),
                    Quantity = table.Column<int>(nullable: false),
                    Price = table.Column<decimal>(nullable: false),
                    Total = table.Column<decimal>(nullable: false),
                    SavedPrice = table.Column<decimal>(nullable: false),
                    SerialNumber = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SalesReturnsItems", x => x.ID);
                    table.ForeignKey(
                        name: "FK_SalesReturnsItems_Items_ItemId",
                        column: x => x.ItemId,
                        principalTable: "Items",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

          

            migrationBuilder.CreateTable(
                name: "ExecutiveSellExpenses",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ExpensesId = table.Column<int>(nullable: true),
                    Amount = table.Column<decimal>(nullable: false),
                    SerialNumber = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExecutiveSellExpenses", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ExecutiveSellExpenses_SellingExpenses_ExpensesId",
                        column: x => x.ExpensesId,
                        principalTable: "SellingExpenses",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MaintanceSellExpenses",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ExpensesId = table.Column<int>(nullable: true),
                    Amount = table.Column<decimal>(nullable: false),
                    SerialNumber = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MaintanceSellExpenses", x => x.ID);
                    table.ForeignKey(
                        name: "FK_MaintanceSellExpenses_SellingExpenses_ExpensesId",
                        column: x => x.ExpensesId,
                        principalTable: "SellingExpenses",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SalesSellExpenses",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ExpensesId = table.Column<int>(nullable: true),
                    Amount = table.Column<decimal>(nullable: false),
                    SerialNumber = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SalesSellExpenses", x => x.ID);
                    table.ForeignKey(
                        name: "FK_SalesSellExpenses_SellingExpenses_ExpensesId",
                        column: x => x.ExpensesId,
                        principalTable: "SellingExpenses",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ExecutiveBills_ClientId",
                table: "ExecutiveBills",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ExecutiveBills_DealTypeId",
                table: "ExecutiveBills",
                column: "DealTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ExecutiveBills_StoreId",
                table: "ExecutiveBills",
                column: "StoreId");

            migrationBuilder.CreateIndex(
                name: "IX_ExecutiveItems_ItemId",
                table: "ExecutiveItems",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_ExecutiveSellExpenses_ExpensesId",
                table: "ExecutiveSellExpenses",
                column: "ExpensesId");

            migrationBuilder.CreateIndex(
                name: "IX_MaintanceSellExpenses_ExpensesId",
                table: "MaintanceSellExpenses",
                column: "ExpensesId");

            migrationBuilder.CreateIndex(
                name: "IX_MantienceBills_ClientId",
                table: "MantienceBills",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_MantienceBills_DealTypeId",
                table: "MantienceBills",
                column: "DealTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_MantienceBills_StoreId",
                table: "MantienceBills",
                column: "StoreId");

            migrationBuilder.CreateIndex(
                name: "IX_MantienceItems_ItemId",
                table: "MantienceItems",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_SalesReturns_ClientId",
                table: "SalesReturns",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_SalesReturns_DealTypeId",
                table: "SalesReturns",
                column: "DealTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_SalesReturns_StoreId",
                table: "SalesReturns",
                column: "StoreId");

            migrationBuilder.CreateIndex(
                name: "IX_SalesReturnsItems_ItemId",
                table: "SalesReturnsItems",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_SalesSellExpenses_ExpensesId",
                table: "SalesSellExpenses",
                column: "ExpensesId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExecutiveBills");

            migrationBuilder.DropTable(
                name: "ExecutiveItems");

            migrationBuilder.DropTable(
                name: "ExecutiveSellExpenses");

            migrationBuilder.DropTable(
                name: "MaintanceSellExpenses");

            migrationBuilder.DropTable(
                name: "MantienceBills");

            migrationBuilder.DropTable(
                name: "MantienceItems");

            migrationBuilder.DropTable(
                name: "SalesReturns");

            migrationBuilder.DropTable(
                name: "SalesReturnsItems");

            migrationBuilder.DropTable(
                name: "SalesSellExpenses");

           
        }
    }
}
