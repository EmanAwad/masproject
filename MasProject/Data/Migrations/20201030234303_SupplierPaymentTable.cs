﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace MasProject.Data.Migrations
{
    public partial class SupplierPaymentTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
              name: "SupplierPayment",
              columns: table => new
              {
                  ID = table.Column<int>(nullable: false)
                      .Annotation("SqlServer:Identity", "1, 1"),
                  IsDeleted = table.Column<bool>(nullable: false),
                  CreatedBy = table.Column<string>(nullable: true),
                  CreatedDate = table.Column<DateTime>(nullable: false),
                  ModifiedBy = table.Column<string>(nullable: true),
                  ModifiedDate = table.Column<DateTime>(nullable: false),
                  Name = table.Column<string>(nullable: true),
                  Note = table.Column<string>(nullable: true),
                  SerialNumber = table.Column<int>(nullable: true),
                  Date = table.Column<DateTime>(nullable: false),
                  Amount = table.Column<double>(nullable: true),
                  Ratio = table.Column<double>(nullable: false),
                  Total = table.Column<double>(nullable: true),
                  IsCheck = table.Column<bool>(nullable: false),
                  DueDate = table.Column<DateTime>(nullable: true),
                  SupplierId = table.Column<int>(nullable: true),
                  TreasuryId = table.Column<int>(nullable: true),
                  CurrencyId = table.Column<int>(nullable: true)
              },
              constraints: table =>
              {
                  table.PrimaryKey("PK_SupplierPayment", x => x.ID);
                  table.ForeignKey(
                      name: "FK_SupplierPayment_Supplier_SupplierId",
                      column: x => x.SupplierId,
                      principalTable: "Supplier",
                      principalColumn: "ID",
                      onDelete: ReferentialAction.Restrict);
                  table.ForeignKey(
                      name: "FK_SupplierPayment_Currency_CurrencyId",
                      column: x => x.CurrencyId,
                      principalTable: "Currency",
                      principalColumn: "ID",
                      onDelete: ReferentialAction.Restrict);
                  table.ForeignKey(
                      name: "FK_SupplierPayment_Treasuries_TreasuryId",
                      column: x => x.TreasuryId,
                      principalTable: "Treasuries",
                      principalColumn: "ID",
                      onDelete: ReferentialAction.Restrict);
              });



            migrationBuilder.CreateIndex(
                name: "IX_SupplierPayment_SupplierId",
                table: "SupplierPayment",
                column: "SupplierId");

            migrationBuilder.CreateIndex(
                name: "IX_SupplierPayment_CurrencyId",
                table: "SupplierPayment",
                column: "CurrencyId");

            migrationBuilder.CreateIndex(
                name: "IX_SupplierPayment_TreasuryId",
                table: "SupplierPayment",
                column: "TreasuryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
              name: "SupplierPayment");
        }
    }
}
