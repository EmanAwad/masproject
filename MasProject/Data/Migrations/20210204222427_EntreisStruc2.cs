﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class EntreisStruc2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EntriesStructuring_Details_Entries_EntriesId",
                table: "EntriesStructuring_Details");

            migrationBuilder.DropIndex(
                name: "IX_EntriesStructuring_Details_EntriesId",
                table: "EntriesStructuring_Details");

            migrationBuilder.DropColumn(
                name: "EntriesId",
                table: "EntriesStructuring_Details");

            migrationBuilder.AddColumn<int>(
                name: "EntriesStructuringId",
                table: "EntriesStructuring_Details",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_EntriesStructuring_Details_EntriesStructuringId",
                table: "EntriesStructuring_Details",
                column: "EntriesStructuringId");

            migrationBuilder.AddForeignKey(
                name: "FK_EntriesStructuring_Details_EntriesStructuring_EntriesStructuringId",
                table: "EntriesStructuring_Details",
                column: "EntriesStructuringId",
                principalTable: "EntriesStructuring",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EntriesStructuring_Details_EntriesStructuring_EntriesStructuringId",
                table: "EntriesStructuring_Details");

            migrationBuilder.DropIndex(
                name: "IX_EntriesStructuring_Details_EntriesStructuringId",
                table: "EntriesStructuring_Details");

            migrationBuilder.DropColumn(
                name: "EntriesStructuringId",
                table: "EntriesStructuring_Details");

            migrationBuilder.AddColumn<int>(
                name: "EntriesId",
                table: "EntriesStructuring_Details",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_EntriesStructuring_Details_EntriesId",
                table: "EntriesStructuring_Details",
                column: "EntriesId");

            migrationBuilder.AddForeignKey(
                name: "FK_EntriesStructuring_Details_Entries_EntriesId",
                table: "EntriesStructuring_Details",
                column: "EntriesId",
                principalTable: "Entries",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
