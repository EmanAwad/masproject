﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class ItemConversion2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ConversionRecordID",
                table: "ItemConversion_Items",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "IdentiferFrom",
                table: "ItemConversion_Items",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "IdentiferFromTo",
                table: "ItemConversion_Items",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ParCodeFrom",
                table: "ItemConversion_Items",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ParCodeTo",
                table: "ItemConversion_Items",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalPriceFrom",
                table: "ItemConversion_Bill",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalPriceTo",
                table: "ItemConversion_Bill",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.CreateIndex(
                name: "IX_ItemConversion_Items_ConversionRecordID",
                table: "ItemConversion_Items",
                column: "ConversionRecordID");

            migrationBuilder.AddForeignKey(
                name: "FK_ItemConversion_Items_ItemConversion_Bill_ConversionRecordID",
                table: "ItemConversion_Items",
                column: "ConversionRecordID",
                principalTable: "ItemConversion_Bill",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ItemConversion_Items_ItemConversion_Bill_ConversionRecordID",
                table: "ItemConversion_Items");

            migrationBuilder.DropIndex(
                name: "IX_ItemConversion_Items_ConversionRecordID",
                table: "ItemConversion_Items");

            migrationBuilder.DropColumn(
                name: "ConversionRecordID",
                table: "ItemConversion_Items");

            migrationBuilder.DropColumn(
                name: "IdentiferFrom",
                table: "ItemConversion_Items");

            migrationBuilder.DropColumn(
                name: "IdentiferFromTo",
                table: "ItemConversion_Items");

            migrationBuilder.DropColumn(
                name: "ParCodeFrom",
                table: "ItemConversion_Items");

            migrationBuilder.DropColumn(
                name: "ParCodeTo",
                table: "ItemConversion_Items");

            migrationBuilder.DropColumn(
                name: "TotalPriceFrom",
                table: "ItemConversion_Bill");

            migrationBuilder.DropColumn(
                name: "TotalPriceTo",
                table: "ItemConversion_Bill");
        }
    }
}
