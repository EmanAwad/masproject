﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class PurchaseIncome4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
          
            migrationBuilder.CreateTable(
                name: "PurchaseOtherIncome",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    IncomeId = table.Column<int>(nullable: true),
                    Amount = table.Column<decimal>(nullable: false),
                    Identifer = table.Column<int>(nullable: false),
                    SalesBillID = table.Column<int>(nullable: true),
                    SerialNumber = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PurchaseOtherIncome", x => x.ID);
                    table.ForeignKey(
                        name: "FK_PurchaseOtherIncome_OtherIncome_IncomeId",
                        column: x => x.IncomeId,
                        principalTable: "OtherIncome",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PurchaseOtherIncome_PurchaseBill_SalesBillID",
                        column: x => x.SalesBillID,
                        principalTable: "PurchaseBill",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

          
            migrationBuilder.CreateIndex(
                name: "IX_PurchaseOtherIncome_IncomeId",
                table: "PurchaseOtherIncome",
                column: "IncomeId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseOtherIncome_SalesBillID",
                table: "PurchaseOtherIncome",
                column: "SalesBillID");

          
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PurchaseOtherIncome");

        }
    }
}
