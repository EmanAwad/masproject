﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class ArrangemntCols : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_PurchaseBill_Client_ClientId",
            //    table: "PurchaseBill");

            //migrationBuilder.DropTable(
            //    name: "Quotations");

            //migrationBuilder.DropColumn(
            //    name: "ClientId",
            //    table: "PurchaseBill");

            migrationBuilder.AddColumn<int>(
                name: "arrangement",
                table: "Stores",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "arrangement",
                table: "Department",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "arrangement",
                table: "Companies",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "arrangement",
                table: "Branch",
                nullable: false,
                defaultValue: 0);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_SalesBills_Client_ClientId",
            //    table: "SalesBills",
            //    column: "ClientId",
            //    principalTable: "Client",
            //    principalColumn: "ID",
            //    onDelete: ReferentialAction.Restrict);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_SalesBills_DealTypes_DealTypeId",
            //    table: "SalesBills",
            //    column: "DealTypeId",
            //    principalTable: "DealTypes",
            //    principalColumn: "ID",
            //    onDelete: ReferentialAction.Restrict);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_SalesBills_Stores_StoreId",
            //    table: "SalesBills",
            //    column: "StoreId",
            //    principalTable: "Stores",
            //    principalColumn: "ID",
            //    onDelete: ReferentialAction.Restrict);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_SalesSellExpenses_SellingExpenses_ExpensesId",
            //    table: "SalesSellExpenses",
            //    column: "ExpensesId",
            //    principalTable: "SellingExpenses",
            //    principalColumn: "ID",
            //    onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_SalesBills_Client_ClientId",
            //    table: "SalesBills");

            //migrationBuilder.DropForeignKey(
            //    name: "FK_SalesBills_DealTypes_DealTypeId",
            //    table: "SalesBills");

            //migrationBuilder.DropForeignKey(
            //    name: "FK_SalesBills_Stores_StoreId",
            //    table: "SalesBills");

            //migrationBuilder.DropForeignKey(
            //    name: "FK_SalesSellExpenses_SellingExpenses_ExpensesId",
            //    table: "SalesSellExpenses");

            migrationBuilder.DropColumn(
                name: "arrangement",
                table: "Stores");

            migrationBuilder.DropColumn(
                name: "arrangement",
                table: "Department");

            migrationBuilder.DropColumn(
                name: "arrangement",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "arrangement",
                table: "Branch");

            //migrationBuilder.AddColumn<int>(
            //    name: "ClientId",
            //    table: "PurchaseBill",
            //    nullable: true);

            //migrationBuilder.CreateTable(
            //    name: "Quotations",
            //    columns: table => new
            //    {
            //        ID = table.Column<int>(type: "int", nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        ApproveId = table.Column<int>(type: "int", nullable: false),
            //        ClientId = table.Column<int>(type: "int", nullable: true),
            //        CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //        CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
            //        Date = table.Column<DateTime>(type: "datetime2", nullable: false),
            //        DealTypeId = table.Column<int>(type: "int", nullable: true),
            //        DiscountPrecentage = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
            //        DiscountValue = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
            //        FlagType = table.Column<bool>(type: "bit", nullable: false),
            //        HandDocumentNumber = table.Column<int>(type: "int", nullable: false),
            //        IsDeleted = table.Column<bool>(type: "bit", nullable: false),
            //        ModifiedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //        ModifiedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
            //        Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //        Note = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //        RevewId = table.Column<int>(type: "int", nullable: false),
            //        SerialNumber = table.Column<int>(type: "int", nullable: false),
            //        ShowNotesFlag = table.Column<bool>(type: "bit", nullable: false),
            //        StoreId = table.Column<int>(type: "int", nullable: true),
            //        TotalAfterDiscount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
            //        TotalPrice = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
            //        UserId = table.Column<int>(type: "int", nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Quotations", x => x.ID);
            //        table.ForeignKey(
            //            name: "FK_Quotations_Client_ClientId",
            //            column: x => x.ClientId,
            //            principalTable: "Client",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //        table.ForeignKey(
            //            name: "FK_Quotations_DealTypes_DealTypeId",
            //            column: x => x.DealTypeId,
            //            principalTable: "DealTypes",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //        table.ForeignKey(
            //            name: "FK_Quotations_Stores_StoreId",
            //            column: x => x.StoreId,
            //            principalTable: "Stores",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateIndex(
            //    name: "IX_Quotations_ClientId",
            //    table: "Quotations",
            //    column: "ClientId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Quotations_DealTypeId",
            //    table: "Quotations",
            //    column: "DealTypeId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Quotations_StoreId",
            //    table: "Quotations",
            //    column: "StoreId");

            //migrationBuilder.AddForeignKey(
            //    name: "FK_PurchaseBill_Client_ClientId",
            //    table: "PurchaseBill",
            //    column: "ClientId",
            //    principalTable: "Client",
            //    principalColumn: "ID",
            //    onDelete: ReferentialAction.Restrict);
        }
    }
}
