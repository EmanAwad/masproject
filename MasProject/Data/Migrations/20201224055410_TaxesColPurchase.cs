﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class TaxesColPurchase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "AddtionTax",
                table: "PurchasesReturns_Bill",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "AddtionTaxAmount",
                table: "PurchasesReturns_Bill",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "SourceDeduction",
                table: "PurchasesReturns_Bill",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "SourceDeductionAmount",
                table: "PurchasesReturns_Bill",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalAfterTax",
                table: "PurchasesReturns_Bill",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "AddtionTax",
                table: "PurchaseBill",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "AddtionTaxAmount",
                table: "PurchaseBill",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "SourceDeduction",
                table: "PurchaseBill",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "SourceDeductionAmount",
                table: "PurchaseBill",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalAfterTax",
                table: "PurchaseBill",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AddtionTax",
                table: "PurchasesReturns_Bill");

            migrationBuilder.DropColumn(
                name: "AddtionTaxAmount",
                table: "PurchasesReturns_Bill");

            migrationBuilder.DropColumn(
                name: "SourceDeduction",
                table: "PurchasesReturns_Bill");

            migrationBuilder.DropColumn(
                name: "SourceDeductionAmount",
                table: "PurchasesReturns_Bill");

            migrationBuilder.DropColumn(
                name: "TotalAfterTax",
                table: "PurchasesReturns_Bill");

            migrationBuilder.DropColumn(
                name: "AddtionTax",
                table: "PurchaseBill");

            migrationBuilder.DropColumn(
                name: "AddtionTaxAmount",
                table: "PurchaseBill");

            migrationBuilder.DropColumn(
                name: "SourceDeduction",
                table: "PurchaseBill");

            migrationBuilder.DropColumn(
                name: "SourceDeductionAmount",
                table: "PurchaseBill");

            migrationBuilder.DropColumn(
                name: "TotalAfterTax",
                table: "PurchaseBill");
        }
    }
}
