﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class AddColsInSupplierPayment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {


            migrationBuilder.AddColumn<string>(
                name: "Bank",
                table: "SupplierPayment",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CollectionDate",
                table: "SupplierPayment",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsCollected",
                table: "SupplierPayment",
                nullable: false,
                defaultValue: false);

        }
        protected override void Down(MigrationBuilder migrationBuilder)
        {
          
            migrationBuilder.DropColumn(
                name: "Bank",
                table: "SupplierPayment");

            migrationBuilder.DropColumn(
                name: "CollectionDate",
                table: "SupplierPayment");

            migrationBuilder.DropColumn(
                name: "IsCollected",
                table: "SupplierPayment");

        }
    }
}
