﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class adjustIEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_Client_Branch_BranchID",
            //    table: "Client");

            //migrationBuilder.DropForeignKey(
            //    name: "FK_ClientOpeningBalance_Branch_BranchID",
            //    table: "ClientOpeningBalance");

            //migrationBuilder.DropForeignKey(
            //    name: "FK_Employee_Branch_BranchID",
            //    table: "Employee");

            //migrationBuilder.DropIndex(
            //    name: "IX_Employee_BranchID",
            //    table: "Employee");

            //migrationBuilder.DropIndex(
            //    name: "IX_ClientOpeningBalance_BranchID",
            //    table: "ClientOpeningBalance");

            //migrationBuilder.DropIndex(
            //    name: "IX_Client_BranchID",
            //    table: "Client");

            //migrationBuilder.RenameColumn(
            //    name: "BranchID",
            //    table: "Employee",
            //    newName: "BranchId");

            //migrationBuilder.RenameColumn(
            //    name: "BranchID",
            //    table: "ClientOpeningBalance",
            //    newName: "BranchId");

            //migrationBuilder.RenameColumn(
            //    name: "BranchID",
            //    table: "Client",
            //    newName: "BranchId");

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "TreasuryOpenBalances",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "TreasuryOpenBalances",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "Treasuries",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "SerialNumber",
                table: "TransSupplier",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "TransSupplier",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "SerialNumber",
                table: "TransItem",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "TransItem",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "TransferRequestItem",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "TransferRequestItem",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "TransferRequest",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "TransferOrderDetails",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "TransferOrderDetails",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "TransferOrder",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "SerialNumber",
                table: "TransEmployee",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "TransEmployee",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "SerialNumber",
                table: "TransClient",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "TransClient",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "TempPaymentVoucherItem",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "TempPaymentVoucher",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "TempAdditionVoucherItem",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "TempAdditionVoucher",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "SupplierPayment",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "SupplierOpeningBalance",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "SupplierOpeningBalance",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "Supplier",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "Supplier",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "Stores",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "SocialMEdia",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "SocialMEdia",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "Social_Client",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "Social_Client",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "SellingExpenses",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "SellingExpenses",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "SalesSellExpenses",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "SalesReturnsItems",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "SalesReturnsExpenses",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "SalesReturnsBillStores",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "SalesItems",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "SalesBillStores",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "SalesArchives",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "SalesArchives",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "QuotationItems",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "PurchasesReturns_Items",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "PurchasesArchives",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "PurchasesArchives",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "PurchaseReturns_OtherIncome",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "PurchaseOtherIncome",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "PurchaseItem",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "PaymentVoucherItem",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "PaymentVoucher",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "OtherIncome",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "OtherIncome",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "OtherConstant",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "OtherConstant",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "MantienceItems",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "MaintanceSellExpenses",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "MaintanceBillStores",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "Lookup_Country",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "Lookup_Country",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "Lookup_City",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "Lookup_City",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "Lookup_Area",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "Lookup_Area",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "ItemTypes",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "ItemTypes",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "ItemsOpeningBalanceDetails",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "ItemsOpeningBalanceDetails",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "ItemsOpeningBalance",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "ItemsOpeningBalance",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "ItemsCollection",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "ItemsCollection",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "Items",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "Items",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "ItemConversion_Items",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "ItemConversion_Items",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "ItemConversion_Bill",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "ItemConversion_Bill",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "Item_Store",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "Item_Store",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "Item_PriceHistory",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "Item_PriceHistory",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "ExecutiveSellExpenses",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "ExecutiveOtherIncome",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "ExecutiveItems",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "ExecutiveBillStores",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "ExecutionWithdrawl_BillStores",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "ExecutionWithDrawals_Items",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "EntriesType",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "EntriesType",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "EntriesStructuring_Details",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "EntriesStructuring_Details",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "EntriesStructuring",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "EntriesStructuring",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "EntriesDetails",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "EntriesDetails",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "Entries",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "Entries",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "EmployeesLoansPayment",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "EmployeesLoansEntry",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "EmployeeOpeningBalance",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "EmployeeOpeningBalance",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "EmployeeFiles",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "EmployeeFiles",
                nullable: true);

            //migrationBuilder.AddColumn<int>(
            //    name: "BranchID",
            //    table: "Employee",
            //    nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "Employee",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "DiscountNotice",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "Department",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "DealTypes",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "DealTypes",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "Currency",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "Currency",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "Companies",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "Companies",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "ClientPayoffs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "ClientPayment",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "ClientOpeningBalanceDetails",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "ClientOpeningBalanceDetails",
                nullable: true);

            //migrationBuilder.AddColumn<int>(
            //    name: "BranchID",
            //    table: "ClientOpeningBalance",
            //    nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "ClientOpeningBalance",
                nullable: true);

            //migrationBuilder.AddColumn<int>(
            //    name: "BranchID",
            //    table: "Client",
            //    nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "Client",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "Branch",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "Branch",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "AdditionVoucherItem",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "AdditionVoucher",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "AdditionNotice",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "AccountingGuide",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SerialNumber",
                table: "AccountingGuide",
                nullable: true);

            //migrationBuilder.CreateIndex(
            //    name: "IX_Employee_BranchID",
            //    table: "Employee",
            //    column: "BranchID");

            //migrationBuilder.CreateIndex(
            //    name: "IX_ClientOpeningBalance_BranchID",
            //    table: "ClientOpeningBalance",
            //    column: "BranchID");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Client_BranchID",
            //    table: "Client",
            //    column: "BranchID");

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Client_Branch_BranchID",
            //    table: "Client",
            //    column: "BranchID",
            //    principalTable: "Branch",
            //    principalColumn: "ID",
            //    onDelete: ReferentialAction.Restrict);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_ClientOpeningBalance_Branch_BranchID",
            //    table: "ClientOpeningBalance",
            //    column: "BranchID",
            //    principalTable: "Branch",
            //    principalColumn: "ID",
            //    onDelete: ReferentialAction.Restrict);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Employee_Branch_BranchID",
            //    table: "Employee",
            //    column: "BranchID",
            //    principalTable: "Branch",
            //    principalColumn: "ID",
            //    onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_Client_Branch_BranchID",
            //    table: "Client");

            //migrationBuilder.DropForeignKey(
            //    name: "FK_ClientOpeningBalance_Branch_BranchID",
            //    table: "ClientOpeningBalance");

            //migrationBuilder.DropForeignKey(
            //    name: "FK_Employee_Branch_BranchID",
            //    table: "Employee");

            //migrationBuilder.DropIndex(
            //    name: "IX_Employee_BranchID",
            //    table: "Employee");

            //migrationBuilder.DropIndex(
            //    name: "IX_ClientOpeningBalance_BranchID",
            //    table: "ClientOpeningBalance");

            //migrationBuilder.DropIndex(
            //    name: "IX_Client_BranchID",
            //    table: "Client");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "TreasuryOpenBalances");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "TreasuryOpenBalances");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "Treasuries");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "TransSupplier");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "TransItem");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "TransferRequestItem");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "TransferRequestItem");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "TransferRequest");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "TransferOrderDetails");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "TransferOrderDetails");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "TransferOrder");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "TransEmployee");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "TransClient");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "TempPaymentVoucherItem");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "TempPaymentVoucher");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "TempAdditionVoucherItem");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "TempAdditionVoucher");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "SupplierPayment");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "SupplierOpeningBalance");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "SupplierOpeningBalance");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "Supplier");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "Supplier");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "Stores");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "SocialMEdia");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "SocialMEdia");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "Social_Client");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "Social_Client");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "SellingExpenses");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "SellingExpenses");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "SalesSellExpenses");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "SalesReturnsItems");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "SalesReturnsExpenses");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "SalesReturnsBillStores");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "SalesItems");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "SalesBillStores");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "SalesArchives");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "SalesArchives");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "QuotationItems");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "PurchasesReturns_Items");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "PurchasesArchives");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "PurchasesArchives");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "PurchaseReturns_OtherIncome");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "PurchaseOtherIncome");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "PurchaseItem");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "PaymentVoucherItem");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "PaymentVoucher");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "OtherIncome");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "OtherIncome");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "OtherConstant");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "OtherConstant");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "MantienceItems");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "MaintanceSellExpenses");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "MaintanceBillStores");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "Lookup_Country");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "Lookup_Country");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "Lookup_City");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "Lookup_City");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "Lookup_Area");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "Lookup_Area");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "ItemTypes");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "ItemTypes");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "ItemsOpeningBalanceDetails");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "ItemsOpeningBalanceDetails");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "ItemsOpeningBalance");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "ItemsOpeningBalance");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "ItemsCollection");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "ItemsCollection");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "Items");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "Items");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "ItemConversion_Items");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "ItemConversion_Items");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "ItemConversion_Bill");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "ItemConversion_Bill");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "Item_Store");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "Item_Store");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "Item_PriceHistory");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "Item_PriceHistory");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "ExecutiveSellExpenses");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "ExecutiveOtherIncome");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "ExecutiveItems");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "ExecutiveBillStores");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "ExecutionWithdrawl_BillStores");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "ExecutionWithDrawals_Items");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "EntriesType");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "EntriesType");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "EntriesStructuring_Details");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "EntriesStructuring_Details");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "EntriesStructuring");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "EntriesStructuring");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "EntriesDetails");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "EntriesDetails");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "Entries");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "Entries");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "EmployeesLoansPayment");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "EmployeesLoansEntry");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "EmployeeOpeningBalance");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "EmployeeOpeningBalance");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "EmployeeFiles");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "EmployeeFiles");

            ////migrationBuilder.DropColumn(
            ////    name: "BranchID",
            //    table: "Employee");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "DiscountNotice");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "Department");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "DealTypes");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "DealTypes");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "Currency");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "Currency");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "ClientPayoffs");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "ClientPayment");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "ClientOpeningBalanceDetails");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "ClientOpeningBalanceDetails");

            //migrationBuilder.DropColumn(
            //    name: "BranchID",
            //    table: "ClientOpeningBalance");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "ClientOpeningBalance");

            //migrationBuilder.DropColumn(
            //    name: "BranchID",
            //    table: "Client");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "Client");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "Branch");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "Branch");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "AdditionVoucherItem");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "AdditionVoucher");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "AdditionNotice");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "AccountingGuide");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "AccountingGuide");

            //migrationBuilder.RenameColumn(
            //    name: "BranchId",
            //    table: "Employee",
            //    newName: "BranchID");

            //migrationBuilder.RenameColumn(
            //    name: "BranchId",
            //    table: "ClientOpeningBalance",
            //    newName: "BranchID");

            //migrationBuilder.RenameColumn(
            //    name: "BranchId",
            //    table: "Client",
            //    newName: "BranchID");

            migrationBuilder.AlterColumn<int>(
                name: "SerialNumber",
                table: "TransSupplier",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "SerialNumber",
                table: "TransItem",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "SerialNumber",
                table: "TransEmployee",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "SerialNumber",
                table: "TransClient",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            //migrationBuilder.CreateIndex(
            //    name: "IX_Employee_BranchID",
            //    table: "Employee",
            //    column: "BranchID");

            //migrationBuilder.CreateIndex(
            //    name: "IX_ClientOpeningBalance_BranchID",
            //    table: "ClientOpeningBalance",
            //    column: "BranchID");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Client_BranchID",
            //    table: "Client",
            //    column: "BranchID");

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Client_Branch_BranchID",
            //    table: "Client",
            //    column: "BranchID",
            //    principalTable: "Branch",
            //    principalColumn: "ID",
            //    onDelete: ReferentialAction.Restrict);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_ClientOpeningBalance_Branch_BranchID",
            //    table: "ClientOpeningBalance",
            //    column: "BranchID",
            //    principalTable: "Branch",
            //    principalColumn: "ID",
            //    onDelete: ReferentialAction.Restrict);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Employee_Branch_BranchID",
            //    table: "Employee",
            //    column: "BranchID",
            //    principalTable: "Branch",
            //    principalColumn: "ID",
            //    onDelete: ReferentialAction.Restrict);
        }
    }
}
