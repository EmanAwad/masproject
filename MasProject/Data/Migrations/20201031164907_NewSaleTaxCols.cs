﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class NewSaleTaxCols : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "AddtionTax",
                table: "SalesReturns",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "AddtionTaxAmount",
                table: "SalesReturns",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "SourceDeduction",
                table: "SalesReturns",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "SourceDeductionAmount",
                table: "SalesReturns",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalAfterTax",
                table: "SalesReturns",
                nullable: false,
                defaultValue: 0m);

            //migrationBuilder.AddColumn<decimal>(
            //    name: "AddtionTax",
            //    table: "SalesBills",
            //    nullable: false,
            //    defaultValue: 0m);

            //migrationBuilder.AddColumn<decimal>(
            //    name: "AddtionTaxAmount",
            //    table: "SalesBills",
            //    nullable: false,
            //    defaultValue: 0m);

            //migrationBuilder.AddColumn<decimal>(
            //    name: "SourceDeduction",
            //    table: "SalesBills",
            //    nullable: false,
            //    defaultValue: 0m);

            //migrationBuilder.AddColumn<decimal>(
            //    name: "SourceDeductionAmount",
            //    table: "SalesBills",
            //    nullable: false,
            //    defaultValue: 0m);

            //migrationBuilder.AddColumn<decimal>(
            //    name: "TotalAfterTax",
            //    table: "SalesBills",
            //    nullable: false,
            //    defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "AddtionTax",
                table: "MantienceBills",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "AddtionTaxAmount",
                table: "MantienceBills",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "SourceDeduction",
                table: "MantienceBills",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "SourceDeductionAmount",
                table: "MantienceBills",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalAfterTax",
                table: "MantienceBills",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "AddtionTax",
                table: "ExecutiveBills",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "AddtionTaxAmount",
                table: "ExecutiveBills",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "SourceDeduction",
                table: "ExecutiveBills",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "SourceDeductionAmount",
                table: "ExecutiveBills",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalAfterTax",
                table: "ExecutiveBills",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AddtionTax",
                table: "SalesReturns");

            migrationBuilder.DropColumn(
                name: "AddtionTaxAmount",
                table: "SalesReturns");

            migrationBuilder.DropColumn(
                name: "SourceDeduction",
                table: "SalesReturns");

            migrationBuilder.DropColumn(
                name: "SourceDeductionAmount",
                table: "SalesReturns");

            migrationBuilder.DropColumn(
                name: "TotalAfterTax",
                table: "SalesReturns");

            migrationBuilder.DropColumn(
                name: "AddtionTax",
                table: "SalesBills");

            migrationBuilder.DropColumn(
                name: "AddtionTaxAmount",
                table: "SalesBills");

            migrationBuilder.DropColumn(
                name: "SourceDeduction",
                table: "SalesBills");

            migrationBuilder.DropColumn(
                name: "SourceDeductionAmount",
                table: "SalesBills");

            migrationBuilder.DropColumn(
                name: "TotalAfterTax",
                table: "SalesBills");

            migrationBuilder.DropColumn(
                name: "AddtionTax",
                table: "MantienceBills");

            migrationBuilder.DropColumn(
                name: "AddtionTaxAmount",
                table: "MantienceBills");

            migrationBuilder.DropColumn(
                name: "SourceDeduction",
                table: "MantienceBills");

            migrationBuilder.DropColumn(
                name: "SourceDeductionAmount",
                table: "MantienceBills");

            migrationBuilder.DropColumn(
                name: "TotalAfterTax",
                table: "MantienceBills");

            migrationBuilder.DropColumn(
                name: "AddtionTax",
                table: "ExecutiveBills");

            migrationBuilder.DropColumn(
                name: "AddtionTaxAmount",
                table: "ExecutiveBills");

            migrationBuilder.DropColumn(
                name: "SourceDeduction",
                table: "ExecutiveBills");

            migrationBuilder.DropColumn(
                name: "SourceDeductionAmount",
                table: "ExecutiveBills");

            migrationBuilder.DropColumn(
                name: "TotalAfterTax",
                table: "ExecutiveBills");
        }
    }
}
