﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class AddDeletedColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "TreasuryOpenBalances",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "TreasuryOpenBalances",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "Treasuries",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "Treasuries",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "TransSupplier",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "TransSupplier",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "TransferRequestItem",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "TransferRequestItem",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "TransferRequest",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "TransferRequest",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "TransferOrderDetails",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "TransferOrderDetails",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "TransferOrder",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "TransferOrder",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "TransEmployee",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "TransEmployee",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "TransClient",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "TransClient",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "SupplierPayment",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "SupplierPayment",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "SupplierOpeningBalance",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "SupplierOpeningBalance",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "Supplier",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "Supplier",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "Stores",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "Stores",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "SocialMEdia",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "SocialMEdia",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "Social_Client",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "Social_Client",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "SellingExpenses",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "SellingExpenses",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "SalesSellExpenses",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "SalesSellExpenses",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "SalesReturnsItems",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "SalesReturnsItems",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "SalesReturnsExpenses",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "SalesReturnsExpenses",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "SalesReturns",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "SalesReturns",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "SalesItems",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "SalesItems",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "SalesBills",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "SalesBills",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "SalesArchives",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "SalesArchives",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "Quotations",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "Quotations",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "QuotationItems",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "QuotationItems",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "PurchasesReturns_Items",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "PurchasesReturns_Items",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "PurchasesReturns_Bill",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "PurchasesReturns_Bill",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "PurchasesArchives",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "PurchasesArchives",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "PurchaseReturns_OtherIncome",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "PurchaseReturns_OtherIncome",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "PurchaseOtherIncome",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "PurchaseOtherIncome",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "PurchaseItem",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "PurchaseItem",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "PurchaseBill",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "PurchaseBill",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "PaymentVoucherItem",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "PaymentVoucherItem",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "PaymentVoucher",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "PaymentVoucher",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "OtherIncome",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "OtherIncome",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "OtherConstant",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "OtherConstant",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "MantienceItems",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "MantienceItems",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "MantienceBills",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "MantienceBills",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "MaintanceSellExpenses",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "MaintanceSellExpenses",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "Lookup_Country",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "Lookup_Country",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "Lookup_City",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "Lookup_City",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "Lookup_Area",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "Lookup_Area",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "ItemTypes",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "ItemTypes",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "ItemsOpeningBalance",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "ItemsOpeningBalance",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "ItemsCollection",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "ItemsCollection",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "Items",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "Items",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "Item_Store",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "Item_Store",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "Item_PriceHistory",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "Item_PriceHistory",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "ExecutiveSellExpenses",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "ExecutiveSellExpenses",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "ExecutiveItems",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "ExecutiveItems",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "ExecutiveBills",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "ExecutiveBills",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "ExecutionWithDrawals_Items",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "ExecutionWithDrawals_Items",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "ExecutionWithDrawals_Bill",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "ExecutionWithDrawals_Bill",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "EmployeesLoansPayment",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "EmployeesLoansPayment",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "EmployeesLoansEntry",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "EmployeesLoansEntry",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "EmployeeOpeningBalance",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "EmployeeOpeningBalance",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "Employee",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "Employee",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "DiscountNotice",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "DiscountNotice",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "Department",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "Department",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "DealTypes",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "DealTypes",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "Currency",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "Currency",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "Companies",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "Companies",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "ClientPayment",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "ClientPayment",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "ClientOpeningBalanceDetails",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "ClientOpeningBalanceDetails",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "ClientOpeningBalance",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "ClientOpeningBalance",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "Client",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "Client",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "Branch",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "Branch",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "AdditionVoucherItem",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "AdditionVoucherItem",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "AdditionVoucher",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "AdditionVoucher",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "AdditionNotice",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "AdditionNotice",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "TreasuryOpenBalances");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "TreasuryOpenBalances");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Treasuries");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Treasuries");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "TransSupplier");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "TransSupplier");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "TransferRequestItem");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "TransferRequestItem");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "TransferRequest");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "TransferRequest");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "TransferOrderDetails");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "TransferOrderDetails");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "TransferOrder");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "TransferOrder");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "TransEmployee");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "TransEmployee");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "TransClient");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "TransClient");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "SupplierPayment");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "SupplierPayment");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "SupplierOpeningBalance");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "SupplierOpeningBalance");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Supplier");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Supplier");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Stores");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Stores");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "SocialMEdia");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "SocialMEdia");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Social_Client");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Social_Client");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "SellingExpenses");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "SellingExpenses");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "SalesSellExpenses");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "SalesSellExpenses");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "SalesReturnsItems");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "SalesReturnsItems");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "SalesReturnsExpenses");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "SalesReturnsExpenses");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "SalesReturns");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "SalesReturns");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "SalesItems");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "SalesItems");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "SalesBills");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "SalesBills");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "SalesArchives");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "SalesArchives");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Quotations");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Quotations");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "QuotationItems");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "QuotationItems");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "PurchasesReturns_Items");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "PurchasesReturns_Items");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "PurchasesReturns_Bill");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "PurchasesReturns_Bill");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "PurchasesArchives");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "PurchasesArchives");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "PurchaseReturns_OtherIncome");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "PurchaseReturns_OtherIncome");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "PurchaseOtherIncome");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "PurchaseOtherIncome");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "PurchaseItem");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "PurchaseItem");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "PurchaseBill");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "PurchaseBill");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "PaymentVoucherItem");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "PaymentVoucherItem");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "PaymentVoucher");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "PaymentVoucher");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "OtherIncome");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "OtherIncome");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "OtherConstant");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "OtherConstant");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "MantienceItems");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "MantienceItems");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "MantienceBills");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "MantienceBills");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "MaintanceSellExpenses");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "MaintanceSellExpenses");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Lookup_Country");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Lookup_Country");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Lookup_City");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Lookup_City");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Lookup_Area");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Lookup_Area");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "ItemTypes");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "ItemTypes");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "ItemsOpeningBalance");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "ItemsOpeningBalance");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "ItemsCollection");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "ItemsCollection");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Items");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Items");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Item_Store");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Item_Store");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Item_PriceHistory");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Item_PriceHistory");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "ExecutiveSellExpenses");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "ExecutiveSellExpenses");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "ExecutiveItems");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "ExecutiveItems");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "ExecutiveBills");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "ExecutiveBills");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "ExecutionWithDrawals_Items");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "ExecutionWithDrawals_Items");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "ExecutionWithDrawals_Bill");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "ExecutionWithDrawals_Bill");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "EmployeesLoansPayment");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "EmployeesLoansPayment");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "EmployeesLoansEntry");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "EmployeesLoansEntry");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "EmployeeOpeningBalance");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "EmployeeOpeningBalance");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "DiscountNotice");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "DiscountNotice");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Department");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Department");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "DealTypes");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "DealTypes");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Currency");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Currency");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "ClientPayment");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "ClientPayment");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "ClientOpeningBalanceDetails");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "ClientOpeningBalanceDetails");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "ClientOpeningBalance");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "ClientOpeningBalance");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Client");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Client");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Branch");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Branch");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "AdditionVoucherItem");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "AdditionVoucherItem");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "AdditionVoucher");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "AdditionVoucher");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "AdditionNotice");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "AdditionNotice");
        }
    }
}
