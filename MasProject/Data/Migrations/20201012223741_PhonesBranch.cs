﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class PhonesBranch : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Mobile",
                table: "Branch",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Phone",
                table: "Branch",
                nullable: true);

            //migrationBuilder.CreateTable(
            //    name: "ExecutionWithDrawals_Bill",
            //    columns: table => new
            //    {
            //        ID = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        IsDeleted = table.Column<bool>(nullable: false),
            //        CreatedBy = table.Column<string>(nullable: true),
            //        CreatedDate = table.Column<DateTime>(nullable: false),
            //        ModifiedBy = table.Column<string>(nullable: true),
            //        ModifiedDate = table.Column<DateTime>(nullable: false),
            //        Note = table.Column<string>(nullable: true),
            //        Name = table.Column<string>(nullable: true),
            //        SerialNumber = table.Column<int>(nullable: true),
            //        Date = table.Column<DateTime>(nullable: false),
            //        StoreId = table.Column<int>(nullable: true),
            //        DealTypeId = table.Column<int>(nullable: true),
            //        FlagType = table.Column<bool>(nullable: false),
            //        TotalPrice = table.Column<decimal>(nullable: true),
            //        DiscountPrecentage = table.Column<decimal>(nullable: true),
            //        DiscountValue = table.Column<decimal>(nullable: true),
            //        TotalAfterDiscount = table.Column<decimal>(nullable: true),
            //        ShowNotesFlag = table.Column<bool>(nullable: false),
            //        UserId = table.Column<int>(nullable: false),
            //        RevewId = table.Column<int>(nullable: false),
            //        ApproveId = table.Column<int>(nullable: false),
            //        BranchId = table.Column<int>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_ExecutionWithDrawals_Bill", x => x.ID);
            //        table.ForeignKey(
            //            name: "FK_ExecutionWithDrawals_Bill_Branch_BranchId",
            //            column: x => x.BranchId,
            //            principalTable: "Branch",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //        table.ForeignKey(
            //            name: "FK_ExecutionWithDrawals_Bill_DealTypes_DealTypeId",
            //            column: x => x.DealTypeId,
            //            principalTable: "DealTypes",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //        table.ForeignKey(
            //            name: "FK_ExecutionWithDrawals_Bill_Stores_StoreId",
            //            column: x => x.StoreId,
            //            principalTable: "Stores",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "ExecutionWithDrawals_Items",
            //    columns: table => new
            //    {
            //        ID = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        IsDeleted = table.Column<bool>(nullable: false),
            //        CreatedBy = table.Column<string>(nullable: true),
            //        CreatedDate = table.Column<DateTime>(nullable: false),
            //        ModifiedBy = table.Column<string>(nullable: true),
            //        ModifiedDate = table.Column<DateTime>(nullable: false),
            //        Name = table.Column<string>(nullable: true),
            //        ItemId = table.Column<int>(nullable: true),
            //        Quantity = table.Column<int>(nullable: true),
            //        Price = table.Column<decimal>(nullable: true),
            //        Total = table.Column<decimal>(nullable: true),
            //        SavedPrice = table.Column<decimal>(nullable: true),
            //        SerialNumber = table.Column<int>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_ExecutionWithDrawals_Items", x => x.ID);
            //        table.ForeignKey(
            //            name: "FK_ExecutionWithDrawals_Items_Items_ItemId",
            //            column: x => x.ItemId,
            //            principalTable: "Items",
            //            principalColumn: "ID",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateIndex(
            //    name: "IX_ExecutionWithDrawals_Bill_BranchId",
            //    table: "ExecutionWithDrawals_Bill",
            //    column: "BranchId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_ExecutionWithDrawals_Bill_DealTypeId",
            //    table: "ExecutionWithDrawals_Bill",
            //    column: "DealTypeId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_ExecutionWithDrawals_Bill_StoreId",
            //    table: "ExecutionWithDrawals_Bill",
            //    column: "StoreId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_ExecutionWithDrawals_Items_ItemId",
            //    table: "ExecutionWithDrawals_Items",
            //    column: "ItemId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropTable(
            //    name: "ExecutionWithDrawals_Bill");

            //migrationBuilder.DropTable(
            //    name: "ExecutionWithDrawals_Items");

            migrationBuilder.DropColumn(
                name: "Mobile",
                table: "Branch");

            migrationBuilder.DropColumn(
                name: "Phone",
                table: "Branch");
        }
    }
}
