﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class ClientsPayoffsAddNewCols : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Bank",
                table: "ClientPayoffs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CollectionDate",
                table: "ClientPayoffs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsCollected",
                table: "ClientPayoffs",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Bank",
                table: "ClientPayoffs");

            migrationBuilder.DropColumn(
                name: "CollectionDate",
                table: "ClientPayoffs");

            migrationBuilder.DropColumn(
                name: "IsCollected",
                table: "ClientPayoffs");
        }
    }
}
