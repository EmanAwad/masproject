﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class UpdateVoucher : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AdditionVoucher_DealTypes_DealTypeId",
                table: "AdditionVoucher");

            migrationBuilder.DropForeignKey(
                name: "FK_PaymentVoucher_DealTypes_DealTypeId",
                table: "PaymentVoucher");

            migrationBuilder.DropIndex(
                name: "IX_PaymentVoucher_DealTypeId",
                table: "PaymentVoucher");

            migrationBuilder.DropIndex(
                name: "IX_AdditionVoucher_DealTypeId",
                table: "AdditionVoucher");

            migrationBuilder.DropColumn(
                name: "Price",
                table: "PaymentVoucherItem");

            migrationBuilder.DropColumn(
                name: "Total",
                table: "PaymentVoucherItem");

            migrationBuilder.DropColumn(
                name: "DealTypeId",
                table: "PaymentVoucher");

            migrationBuilder.DropColumn(
                name: "DiscountPrecentage",
                table: "PaymentVoucher");

            migrationBuilder.DropColumn(
                name: "DiscountValue",
                table: "PaymentVoucher");

            migrationBuilder.DropColumn(
                name: "FlagType",
                table: "PaymentVoucher");

            migrationBuilder.DropColumn(
                name: "TotalAfterDiscount",
                table: "PaymentVoucher");

            migrationBuilder.DropColumn(
                name: "TotalPrice",
                table: "PaymentVoucher");

            migrationBuilder.DropColumn(
                name: "Price",
                table: "AdditionVoucherItem");

            migrationBuilder.DropColumn(
                name: "SavedPrice",
                table: "AdditionVoucherItem");

            migrationBuilder.DropColumn(
                name: "Total",
                table: "AdditionVoucherItem");

            migrationBuilder.DropColumn(
                name: "DealTypeId",
                table: "AdditionVoucher");

            migrationBuilder.DropColumn(
                name: "DiscountPrecentage",
                table: "AdditionVoucher");

            migrationBuilder.DropColumn(
                name: "DiscountValue",
                table: "AdditionVoucher");

            migrationBuilder.DropColumn(
                name: "FlagType",
                table: "AdditionVoucher");

            migrationBuilder.DropColumn(
                name: "TotalAfterDiscount",
                table: "AdditionVoucher");

            migrationBuilder.DropColumn(
                name: "TotalPrice",
                table: "AdditionVoucher");

            migrationBuilder.AddColumn<string>(
                name: "BoxCode",
                table: "PaymentVoucherItem",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SheilfNo",
                table: "PaymentVoucherItem",
                nullable: true);


            migrationBuilder.AddColumn<string>(
                name: "BoxCode",
                table: "AdditionVoucherItem",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SheilfNo",
                table: "AdditionVoucherItem",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StoreId",
                table: "SalesItems");

            migrationBuilder.DropColumn(
                name: "BoxCode",
                table: "PaymentVoucherItem");

            migrationBuilder.DropColumn(
                name: "SheilfNo",
                table: "PaymentVoucherItem");

            migrationBuilder.DropColumn(
                name: "IsConverted",
                table: "MantienceBills");

            migrationBuilder.DropColumn(
                name: "IsConverted",
                table: "ExecutiveBills");

            migrationBuilder.DropColumn(
                name: "BoxCode",
                table: "AdditionVoucherItem");

            migrationBuilder.DropColumn(
                name: "SheilfNo",
                table: "AdditionVoucherItem");

            migrationBuilder.AddColumn<decimal>(
                name: "Price",
                table: "PaymentVoucherItem",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Total",
                table: "PaymentVoucherItem",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DealTypeId",
                table: "PaymentVoucher",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "DiscountPrecentage",
                table: "PaymentVoucher",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "DiscountValue",
                table: "PaymentVoucher",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "FlagType",
                table: "PaymentVoucher",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalAfterDiscount",
                table: "PaymentVoucher",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalPrice",
                table: "PaymentVoucher",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Price",
                table: "AdditionVoucherItem",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "SavedPrice",
                table: "AdditionVoucherItem",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Total",
                table: "AdditionVoucherItem",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DealTypeId",
                table: "AdditionVoucher",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "DiscountPrecentage",
                table: "AdditionVoucher",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "DiscountValue",
                table: "AdditionVoucher",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "FlagType",
                table: "AdditionVoucher",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalAfterDiscount",
                table: "AdditionVoucher",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalPrice",
                table: "AdditionVoucher",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PaymentVoucher_DealTypeId",
                table: "PaymentVoucher",
                column: "DealTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_AdditionVoucher_DealTypeId",
                table: "AdditionVoucher",
                column: "DealTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_AdditionVoucher_DealTypes_DealTypeId",
                table: "AdditionVoucher",
                column: "DealTypeId",
                principalTable: "DealTypes",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PaymentVoucher_DealTypes_DealTypeId",
                table: "PaymentVoucher",
                column: "DealTypeId",
                principalTable: "DealTypes",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
