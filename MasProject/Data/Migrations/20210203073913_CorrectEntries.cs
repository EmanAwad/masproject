﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class CorrectEntries : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EntriesDetails_Branch_BranchId",
                table: "EntriesDetails");

            migrationBuilder.DropIndex(
                name: "IX_EntriesDetails_BranchId",
                table: "EntriesDetails");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "EntriesDetails");

            migrationBuilder.AddColumn<int>(
                name: "CostCenterId",
                table: "EntriesDetails",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CostCenterId",
                table: "EntriesDetails");

            //migrationBuilder.AddColumn<int>(
            //    name: "BranchId",
            //    table: "EntriesDetails",
            //    type: "int",
            //    nullable: true);

            //migrationBuilder.CreateIndex(
            //    name: "IX_EntriesDetails_BranchId",
            //    table: "EntriesDetails",
            //    column: "BranchId");

            //migrationBuilder.AddForeignKey(
            //    name: "FK_EntriesDetails_Branch_BranchId",
            //    table: "EntriesDetails",
            //    column: "BranchId",
            //    principalTable: "Branch",
            //    principalColumn: "ID",
            //    onDelete: ReferentialAction.Restrict);
        }
    }
}
