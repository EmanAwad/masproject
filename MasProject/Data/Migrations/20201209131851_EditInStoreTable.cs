﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class EditInStoreTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
          

            migrationBuilder.AddColumn<int>(
                name: "EmployeeID",
                table: "Stores",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Stores_EmployeeID",
                table: "Stores",
                column: "EmployeeID");

            migrationBuilder.AddForeignKey(
                name: "FK_Stores_Employee_EmployeeID",
                table: "Stores",
                column: "EmployeeID",
                principalTable: "Employee",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Stores_Employee_EmployeeID",
                table: "Stores");

            migrationBuilder.DropIndex(
                name: "IX_Stores_EmployeeID",
                table: "Stores");

            migrationBuilder.DropColumn(
                name: "EmployeeID",
                table: "Stores");

       
        }
    }
}
