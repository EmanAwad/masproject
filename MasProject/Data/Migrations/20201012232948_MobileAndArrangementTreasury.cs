﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class MobileAndArrangementTreasury : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Mobile",
                table: "Treasuries",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "arrangement",
                table: "Treasuries",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Mobile",
                table: "Treasuries");

            migrationBuilder.DropColumn(
                name: "arrangement",
                table: "Treasuries");
        }
    }
}
