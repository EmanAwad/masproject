﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class UpdateTransferOrderTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransferOrderDetails_Stores_StoreIDFrom",
                table: "TransferOrderDetails");

           
            migrationBuilder.DropIndex(
                name: "IX_TransferOrderDetails_StoreIDFrom",
                table: "TransferOrderDetails");

            migrationBuilder.DropColumn(
                name: "StoreIDFrom",
                table: "TransferOrderDetails");

            migrationBuilder.AlterColumn<double>(
                name: "Quantity",
                table: "TransferOrderDetails",
                nullable: true,
                oldClrType: typeof(double),
                oldType: "float");

            migrationBuilder.AddColumn<int>(
                name: "BranchFromID",
                table: "TransferOrderDetails",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchToID",
                table: "TransferOrder",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Date",
                table: "TransferOrder",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "SerialNumber",
                table: "TransferOrder",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TransferOrderDetails_BranchFromID",
                table: "TransferOrderDetails",
                column: "BranchFromID");

            migrationBuilder.CreateIndex(
                name: "IX_TransferOrder_BranchToID",
                table: "TransferOrder",
                column: "BranchToID");

            migrationBuilder.AddForeignKey(
                name: "FK_TransferOrder_Branch_BranchToID",
                table: "TransferOrder",
                column: "BranchToID",
                principalTable: "Branch",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TransferOrderDetails_Branch_BranchFromID",
                table: "TransferOrderDetails",
                column: "BranchFromID",
                principalTable: "Branch",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransferOrder_Branch_BranchToID",
                table: "TransferOrder");

            migrationBuilder.DropForeignKey(
                name: "FK_TransferOrderDetails_Branch_BranchFromID",
                table: "TransferOrderDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_TransferRequestItem_Branch_BranchFromID",
                table: "TransferRequestItem");

            migrationBuilder.DropIndex(
                name: "IX_TransferRequestItem_BranchFromID",
                table: "TransferRequestItem");

            migrationBuilder.DropIndex(
                name: "IX_TransferOrderDetails_BranchFromID",
                table: "TransferOrderDetails");

            migrationBuilder.DropIndex(
                name: "IX_TransferOrder_BranchToID",
                table: "TransferOrder");

            migrationBuilder.DropColumn(
                name: "BranchFromID",
                table: "TransferOrderDetails");

            migrationBuilder.DropColumn(
                name: "BranchToID",
                table: "TransferOrder");

            migrationBuilder.DropColumn(
                name: "Date",
                table: "TransferOrder");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "TransferOrder");

            migrationBuilder.AddColumn<int>(
                name: "StoreIDFrom",
                table: "TransferRequestItem",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "Quantity",
                table: "TransferOrderDetails",
                type: "float",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StoreIDFrom",
                table: "TransferOrderDetails",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TransferRequestItem_StoreIDFrom",
                table: "TransferRequestItem",
                column: "StoreIDFrom");

            migrationBuilder.CreateIndex(
                name: "IX_TransferOrderDetails_StoreIDFrom",
                table: "TransferOrderDetails",
                column: "StoreIDFrom");

            migrationBuilder.AddForeignKey(
                name: "FK_TransferOrderDetails_Stores_StoreIDFrom",
                table: "TransferOrderDetails",
                column: "StoreIDFrom",
                principalTable: "Stores",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TransferRequestItem_Branch_StoreIDFrom",
                table: "TransferRequestItem",
                column: "StoreIDFrom",
                principalTable: "Branch",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
