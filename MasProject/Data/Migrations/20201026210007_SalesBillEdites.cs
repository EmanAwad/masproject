﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class SalesBillEdites : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_ClientOpeningBalance_Client_ClientID",
            //    table: "ClientOpeningBalance");

            //migrationBuilder.DropForeignKey(
            //    name: "FK_ItemsOpeningBalance_Items_ItemId",
            //    table: "ItemsOpeningBalance");

            //migrationBuilder.DropForeignKey(
            //    name: "FK_ItemsOpeningBalance_Stores_StoreId",
            //    table: "ItemsOpeningBalance");

            //migrationBuilder.DropForeignKey(
            //    name: "FK_Quotations_Client_ClientId",
            //    table: "Quotations");

            //migrationBuilder.DropIndex(
            //    name: "IX_Quotations_ClientId",
            //    table: "Quotations");

            //migrationBuilder.DropIndex(
            //    name: "IX_ItemsOpeningBalance_ItemId",
            //    table: "ItemsOpeningBalance");

            //migrationBuilder.DropIndex(
            //    name: "IX_ClientOpeningBalance_ClientID",
            //    table: "ClientOpeningBalance");

            //migrationBuilder.DropColumn(
            //    name: "ClientId",
            //    table: "Quotations");

            //migrationBuilder.DropColumn(
            //    name: "ItemId",
            //    table: "ItemsOpeningBalance");

            //migrationBuilder.DropColumn(
            //    name: "Quantity",
            //    table: "ItemsOpeningBalance");

            //migrationBuilder.DropColumn(
            //    name: "TotalPrice",
            //    table: "ItemsOpeningBalance");

            //migrationBuilder.DropColumn(
            //    name: "Amount",
            //    table: "ClientOpeningBalance");

            //migrationBuilder.DropColumn(
            //    name: "ClientID",
            //    table: "ClientOpeningBalance");

            //migrationBuilder.DropColumn(
            //    name: "tbl_Notes",
            //    table: "ClientOpeningBalance");

            migrationBuilder.AddColumn<decimal>(
                name: "AddtionTax",
                table: "SalesBills",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "AddtionTaxAmount",
                table: "SalesBills",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "SourceDeduction",
                table: "SalesBills",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "SourceDeductionAmount",
                table: "SalesBills",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalAfterTax",
                table: "SalesBills",
                nullable: false,
                defaultValue: 0m);

            //migrationBuilder.AddColumn<int>(
            //    name: "BranchId",
            //    table: "Quotations",
            //    nullable: true);

            //migrationBuilder.AddColumn<string>(
            //    name: "Client",
            //    table: "Quotations",
            //    nullable: true);

            //migrationBuilder.AlterColumn<int>(
            //    name: "StoreId",
            //    table: "ItemsOpeningBalance",
            //    nullable: true,
            //    oldClrType: typeof(int),
            //    oldType: "int");

            //migrationBuilder.AddColumn<string>(
            //    name: "DocumentNumber",
            //    table: "ItemsOpeningBalance",
            //    nullable: true);

            //migrationBuilder.AddColumn<int>(
            //    name: "EmployeeId",
            //    table: "ItemsOpeningBalance",
            //    nullable: true);

            //migrationBuilder.AddColumn<string>(
            //    name: "Notes",
            //    table: "ItemsOpeningBalance",
            //    nullable: true);

            //migrationBuilder.CreateTable(
            //    name: "ClientOpeningBalanceDetails",
            //    columns: table => new
            //    {
            //        ID = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        Name = table.Column<string>(nullable: true),
            //        IsDeleted = table.Column<bool>(nullable: false),
            //        CreatedBy = table.Column<string>(nullable: true),
            //        CreatedDate = table.Column<DateTime>(nullable: false),
            //        ModifiedBy = table.Column<string>(nullable: true),
            //        ModifiedDate = table.Column<DateTime>(nullable: false),
            //        Note = table.Column<string>(nullable: true),
            //        ClientDocumentNumber = table.Column<string>(nullable: true),
            //        Amount = table.Column<double>(nullable: true),
            //        ClientID = table.Column<int>(nullable: true)
            //    },
                //constraints: table =>
                //{
                //    table.PrimaryKey("PK_ClientOpeningBalanceDetails", x => x.ID);
                //    table.ForeignKey(
                //        name: "FK_ClientOpeningBalanceDetails_Client_ClientID",
                //        column: x => x.ClientID,
                //        principalTable: "Client",
                //        principalColumn: "ID",
                //        onDelete: ReferentialAction.Restrict);
                //});

            //migrationBuilder.CreateTable(
            //    name: "OtherConstant",
            //    columns: table => new
            //    {
            //        ID = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        Name = table.Column<string>(nullable: true),
            //        IsDeleted = table.Column<bool>(nullable: false),
            //        CreatedBy = table.Column<string>(nullable: true),
            //        CreatedDate = table.Column<DateTime>(nullable: false),
            //        ModifiedBy = table.Column<string>(nullable: true),
            //        ModifiedDate = table.Column<DateTime>(nullable: false),
            //        Note = table.Column<string>(nullable: true),
            //        Ratio = table.Column<double>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_OtherConstant", x => x.ID);
            //    });

            //migrationBuilder.CreateIndex(
            //    name: "IX_Quotations_BranchId",
            //    table: "Quotations",
            //    column: "BranchId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_ItemsOpeningBalance_EmployeeId",
            //    table: "ItemsOpeningBalance",
            //    column: "EmployeeId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_ClientOpeningBalanceDetails_ClientID",
            //    table: "ClientOpeningBalanceDetails",
            //    column: "ClientID");

            //migrationBuilder.AddForeignKey(
            //    name: "FK_ItemsOpeningBalance_Employee_EmployeeId",
            //    table: "ItemsOpeningBalance",
            //    column: "EmployeeId",
            //    principalTable: "Employee",
            //    principalColumn: "ID",
            //    onDelete: ReferentialAction.Restrict);

        //    migrationBuilder.AddForeignKey(
        //        name: "FK_ItemsOpeningBalance_Stores_StoreId",
        //        table: "ItemsOpeningBalance",
        //        column: "StoreId",
        //        principalTable: "Stores",
        //        principalColumn: "ID",
        //        onDelete: ReferentialAction.Restrict);

        //    migrationBuilder.AddForeignKey(
        //        name: "FK_Quotations_Branch_BranchId",
        //        table: "Quotations",
        //        column: "BranchId",
        //        principalTable: "Branch",
        //        principalColumn: "ID",
        //        onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_ItemsOpeningBalance_Employee_EmployeeId",
            //    table: "ItemsOpeningBalance");

            //migrationBuilder.DropForeignKey(
            //    name: "FK_ItemsOpeningBalance_Stores_StoreId",
            //    table: "ItemsOpeningBalance");

            //migrationBuilder.DropForeignKey(
            //    name: "FK_Quotations_Branch_BranchId",
            //    table: "Quotations");

            //migrationBuilder.DropTable(
            //    name: "ClientOpeningBalanceDetails");

            //migrationBuilder.DropTable(
            //    name: "OtherConstant");

            //migrationBuilder.DropIndex(
            //    name: "IX_Quotations_BranchId",
            //    table: "Quotations");

            //migrationBuilder.DropIndex(
            //    name: "IX_ItemsOpeningBalance_EmployeeId",
            //    table: "ItemsOpeningBalance");

            migrationBuilder.DropColumn(
                name: "AddtionTax",
                table: "SalesBills");

            migrationBuilder.DropColumn(
                name: "AddtionTaxAmount",
                table: "SalesBills");

            migrationBuilder.DropColumn(
                name: "SourceDeduction",
                table: "SalesBills");

            migrationBuilder.DropColumn(
                name: "SourceDeductionAmount",
                table: "SalesBills");

            migrationBuilder.DropColumn(
                name: "TotalAfterTax",
                table: "SalesBills");

            //migrationBuilder.DropColumn(
            //    name: "BranchId",
            //    table: "Quotations");

            //migrationBuilder.DropColumn(
            //    name: "Client",
            //    table: "Quotations");

            //migrationBuilder.DropColumn(
            //    name: "DocumentNumber",
            //    table: "ItemsOpeningBalance");

            //migrationBuilder.DropColumn(
            //    name: "EmployeeId",
            //    table: "ItemsOpeningBalance");

            //migrationBuilder.DropColumn(
            //    name: "Notes",
            //    table: "ItemsOpeningBalance");

            //migrationBuilder.AddColumn<int>(
            //    name: "ClientId",
            //    table: "Quotations",
            //    type: "int",
            //    nullable: true);

            //migrationBuilder.AlterColumn<int>(
            //    name: "StoreId",
            //    table: "ItemsOpeningBalance",
            //    type: "int",
            //    nullable: false,
            //    oldClrType: typeof(int),
            //    oldNullable: true);

            //migrationBuilder.AddColumn<int>(
            //    name: "ItemId",
            //    table: "ItemsOpeningBalance",
            //    type: "int",
            //    nullable: false,
            //    defaultValue: 0);

            //migrationBuilder.AddColumn<int>(
            //    name: "Quantity",
            //    table: "ItemsOpeningBalance",
            //    type: "int",
            //    nullable: false,
            //    defaultValue: 0);

            //migrationBuilder.AddColumn<float>(
            //    name: "TotalPrice",
            //    table: "ItemsOpeningBalance",
            //    type: "real",
            //    nullable: false,
            //    defaultValue: 0f);

            //migrationBuilder.AddColumn<double>(
            //    name: "Amount",
            //    table: "ClientOpeningBalance",
            //    type: "float",
            //    nullable: true);

            //migrationBuilder.AddColumn<int>(
            //    name: "ClientID",
            //    table: "ClientOpeningBalance",
            //    type: "int",
            //    nullable: true);

            //migrationBuilder.AddColumn<string>(
            //    name: "tbl_Notes",
            //    table: "ClientOpeningBalance",
            //    type: "nvarchar(max)",
            //    nullable: true);

            //migrationBuilder.CreateIndex(
            //    name: "IX_Quotations_ClientId",
            //    table: "Quotations",
            //    column: "ClientId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_ItemsOpeningBalance_ItemId",
            //    table: "ItemsOpeningBalance",
            //    column: "ItemId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_ClientOpeningBalance_ClientID",
            //    table: "ClientOpeningBalance",
            //    column: "ClientID");

            //migrationBuilder.AddForeignKey(
            //    name: "FK_ClientOpeningBalance_Client_ClientID",
            //    table: "ClientOpeningBalance",
            //    column: "ClientID",
            //    principalTable: "Client",
            //    principalColumn: "ID",
            //    onDelete: ReferentialAction.Restrict);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_ItemsOpeningBalance_Items_ItemId",
            //    table: "ItemsOpeningBalance",
            //    column: "ItemId",
            //    principalTable: "Items",
            //    principalColumn: "ID",
            //    onDelete: ReferentialAction.Cascade);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_ItemsOpeningBalance_Stores_StoreId",
            //    table: "ItemsOpeningBalance",
            //    column: "StoreId",
            //    principalTable: "Stores",
            //    principalColumn: "ID",
            //    onDelete: ReferentialAction.Cascade);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Quotations_Client_ClientId",
            //    table: "Quotations",
            //    column: "ClientId",
            //    principalTable: "Client",
            //    principalColumn: "ID",
            //    onDelete: ReferentialAction.Restrict);
        }
    }
}
