﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class TaxQuotation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_ClientOpeningBalance_Currency_CurrencyID",
            //    table: "ClientOpeningBalance");

            //migrationBuilder.DropForeignKey(
            //    name: "FK_EmployeeOpeningBalance_Currency_CurrencyID",
            //    table: "EmployeeOpeningBalance");

            //migrationBuilder.DropIndex(
            //    name: "IX_EmployeeOpeningBalance_CurrencyID",
            //    table: "EmployeeOpeningBalance");

            //migrationBuilder.DropIndex(
            //    name: "IX_ClientOpeningBalance_CurrencyID",
            //    table: "ClientOpeningBalance");

            //migrationBuilder.DropColumn(
            //    name: "CurrencyID",
            //    table: "EmployeeOpeningBalance");

            //migrationBuilder.DropColumn(
            //    name: "Ratio",
            //    table: "EmployeeOpeningBalance");

            //migrationBuilder.DropColumn(
            //    name: "CurrencyID",
            //    table: "ClientOpeningBalance");

            //migrationBuilder.DropColumn(
            //    name: "Ratio",
            //    table: "ClientOpeningBalance");

            migrationBuilder.AddColumn<decimal>(
                name: "AddtionTax",
                table: "Quotations",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "AddtionTaxAmount",
                table: "Quotations",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "SourceDeduction",
                table: "Quotations",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "SourceDeductionAmount",
                table: "Quotations",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalAfterTax",
                table: "Quotations",
                nullable: false,
                defaultValue: 0m);

            //migrationBuilder.AddColumn<int>(
            //    name: "PriceType",
            //    table: "DealTypes",
            //    nullable: false,
            //    defaultValue: 0);

            //migrationBuilder.AddColumn<int>(
            //    name: "BranchID",
            //    table: "ClientOpeningBalance",
            //    nullable: true);

            //migrationBuilder.AddColumn<string>(
            //    name: "DocumentNumber",
            //    table: "ClientOpeningBalance",
            //    nullable: true);

            //migrationBuilder.AddColumn<int>(
            //    name: "EmployeeID",
            //    table: "ClientOpeningBalance",
            //    nullable: true);

            //migrationBuilder.AddColumn<string>(
            //    name: "tbl_Notes",
            //    table: "ClientOpeningBalance",
            //    nullable: true);

            //migrationBuilder.CreateIndex(
            //    name: "IX_ClientOpeningBalance_BranchID",
            //    table: "ClientOpeningBalance",
            //    column: "BranchID");

            //migrationBuilder.CreateIndex(
            //    name: "IX_ClientOpeningBalance_EmployeeID",
            //    table: "ClientOpeningBalance",
            //    column: "EmployeeID");

            //migrationBuilder.AddForeignKey(
            //    name: "FK_ClientOpeningBalance_Branch_BranchID",
            //    table: "ClientOpeningBalance",
            //    column: "BranchID",
            //    principalTable: "Branch",
            //    principalColumn: "ID",
            //    onDelete: ReferentialAction.Restrict);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_ClientOpeningBalance_Employee_EmployeeID",
            //    table: "ClientOpeningBalance",
            //    column: "EmployeeID",
            //    principalTable: "Employee",
            //    principalColumn: "ID",
            //    onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_ClientOpeningBalance_Branch_BranchID",
            //    table: "ClientOpeningBalance");

            //migrationBuilder.DropForeignKey(
            //    name: "FK_ClientOpeningBalance_Employee_EmployeeID",
            //    table: "ClientOpeningBalance");

            //migrationBuilder.DropIndex(
            //    name: "IX_ClientOpeningBalance_BranchID",
            //    table: "ClientOpeningBalance");

            //migrationBuilder.DropIndex(
            //    name: "IX_ClientOpeningBalance_EmployeeID",
            //    table: "ClientOpeningBalance");

            migrationBuilder.DropColumn(
                name: "AddtionTax",
                table: "Quotations");

            migrationBuilder.DropColumn(
                name: "AddtionTaxAmount",
                table: "Quotations");

            migrationBuilder.DropColumn(
                name: "SourceDeduction",
                table: "Quotations");

            migrationBuilder.DropColumn(
                name: "SourceDeductionAmount",
                table: "Quotations");

            migrationBuilder.DropColumn(
                name: "TotalAfterTax",
                table: "Quotations");

            //migrationBuilder.DropColumn(
            //    name: "PriceType",
            //    table: "DealTypes");

            //migrationBuilder.DropColumn(
            //    name: "BranchID",
            //    table: "ClientOpeningBalance");

            //migrationBuilder.DropColumn(
            //    name: "DocumentNumber",
            //    table: "ClientOpeningBalance");

            //migrationBuilder.DropColumn(
            //    name: "EmployeeID",
            //    table: "ClientOpeningBalance");

            //migrationBuilder.DropColumn(
            //    name: "tbl_Notes",
            //    table: "ClientOpeningBalance");

            //migrationBuilder.AddColumn<int>(
            //    name: "CurrencyID",
            //    table: "EmployeeOpeningBalance",
            //    type: "int",
            //    nullable: true);

            //migrationBuilder.AddColumn<double>(
            //    name: "Ratio",
            //    table: "EmployeeOpeningBalance",
            //    type: "float",
            //    nullable: true);

            //migrationBuilder.AddColumn<int>(
            //    name: "CurrencyID",
            //    table: "ClientOpeningBalance",
            //    type: "int",
            //    nullable: true);

            //migrationBuilder.AddColumn<double>(
            //    name: "Ratio",
            //    table: "ClientOpeningBalance",
            //    type: "float",
            //    nullable: true);

            //migrationBuilder.CreateIndex(
            //    name: "IX_EmployeeOpeningBalance_CurrencyID",
            //    table: "EmployeeOpeningBalance",
            //    column: "CurrencyID");

            //migrationBuilder.CreateIndex(
            //    name: "IX_ClientOpeningBalance_CurrencyID",
            //    table: "ClientOpeningBalance",
            //    column: "CurrencyID");

            //migrationBuilder.AddForeignKey(
            //    name: "FK_ClientOpeningBalance_Currency_CurrencyID",
            //    table: "ClientOpeningBalance",
            //    column: "CurrencyID",
            //    principalTable: "Currency",
            //    principalColumn: "ID",
            //    onDelete: ReferentialAction.Restrict);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_EmployeeOpeningBalance_Currency_CurrencyID",
            //    table: "EmployeeOpeningBalance",
            //    column: "CurrencyID",
            //    principalTable: "Currency",
            //    principalColumn: "ID",
            //    onDelete: ReferentialAction.Restrict);
        }
    }
}
