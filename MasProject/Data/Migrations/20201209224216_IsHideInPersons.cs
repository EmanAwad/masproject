﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class IsHideInPersons : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsHide",
                table: "Supplier",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsHide",
                table: "Employee",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsHide",
                table: "Client",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsHide",
                table: "Supplier");

            migrationBuilder.DropColumn(
                name: "IsHide",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "IsHide",
                table: "Client");
        }
    }
}
