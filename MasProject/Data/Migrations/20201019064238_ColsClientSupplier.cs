﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class ColsClientSupplier : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.AddColumn<bool>(
            //    name: "IsConverted",
            //    table: "TransferOrder",
            //    nullable: false,
            //    defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "CurrencyID",
                table: "Supplier",
                nullable: true);

            //migrationBuilder.AddColumn<bool>(
            //    name: "IsConverted",
            //    table: "SalesReturns",
            //    nullable: false,
            //    defaultValue: false);

            //migrationBuilder.AddColumn<bool>(
            //    name: "IsConverted",
            //    table: "SalesBills",
            //    nullable: false,
            //    defaultValue: false);

            //migrationBuilder.AddColumn<bool>(
            //    name: "IsConverted",
            //    table: "Quotations",
            //    nullable: false,
            //    defaultValue: false);

            //migrationBuilder.AddColumn<bool>(
            //    name: "IsConverted",
            //    table: "PurchasesReturns_Bill",
            //    nullable: false,
            //    defaultValue: false);

            //migrationBuilder.AddColumn<bool>(
            //    name: "IsConverted",
            //    table: "PurchaseBill",
            //    nullable: false,
            //    defaultValue: false);

            //migrationBuilder.AddColumn<int>(
            //    name: "BillSerialNumber",
            //    table: "PaymentVoucher",
            //    nullable: true);

            //migrationBuilder.AddColumn<string>(
            //    name: "BillType",
            //    table: "PaymentVoucher",
            //    nullable: true);

            //migrationBuilder.AddColumn<int>(
            //    name: "UserType",
            //    table: "PaymentVoucher",
            //    nullable: false,
            //    defaultValue: 0);

            //migrationBuilder.AddColumn<bool>(
            //    name: "IsConverted",
            //    table: "ExecutionWithDrawals_Bill",
            //    nullable: false,
            //    defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Mobile2",
                table: "Client",
                nullable: true);

            //migrationBuilder.AddColumn<int>(
            //    name: "BillSerialNumber",
            //    table: "AdditionVoucher",
            //    nullable: true);

            //migrationBuilder.AddColumn<string>(
            //    name: "BillType",
            //    table: "AdditionVoucher",
            //    nullable: true);

            //migrationBuilder.AddColumn<int>(
            //    name: "UserType",
            //    table: "AdditionVoucher",
            //    nullable: false,
            //    defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Supplier_CurrencyID",
                table: "Supplier",
                column: "CurrencyID");

            migrationBuilder.AddForeignKey(
                name: "FK_Supplier_Currency_CurrencyID",
                table: "Supplier",
                column: "CurrencyID",
                principalTable: "Currency",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Supplier_Currency_CurrencyID",
                table: "Supplier");

            migrationBuilder.DropIndex(
                name: "IX_Supplier_CurrencyID",
                table: "Supplier");

            //migrationBuilder.DropColumn(
            //    name: "IsConverted",
            //    table: "TransferOrder");

            migrationBuilder.DropColumn(
                name: "CurrencyID",
                table: "Supplier");

            //migrationBuilder.DropColumn(
            //    name: "IsConverted",
            //    table: "SalesReturns");

            //migrationBuilder.DropColumn(
            //    name: "IsConverted",
            //    table: "SalesBills");

            //migrationBuilder.DropColumn(
            //    name: "IsConverted",
            //    table: "Quotations");

            //migrationBuilder.DropColumn(
            //    name: "IsConverted",
            //    table: "PurchasesReturns_Bill");

            //migrationBuilder.DropColumn(
            //    name: "IsConverted",
            //    table: "PurchaseBill");

            //migrationBuilder.DropColumn(
            //    name: "BillSerialNumber",
            //    table: "PaymentVoucher");

            //migrationBuilder.DropColumn(
            //    name: "BillType",
            //    table: "PaymentVoucher");

            //migrationBuilder.DropColumn(
            //    name: "UserType",
            //    table: "PaymentVoucher");

            //migrationBuilder.DropColumn(
            //    name: "IsConverted",
            //    table: "ExecutionWithDrawals_Bill");

            migrationBuilder.DropColumn(
                name: "Mobile2",
                table: "Client");

            //migrationBuilder.DropColumn(
            //    name: "BillSerialNumber",
            //    table: "AdditionVoucher");

            //migrationBuilder.DropColumn(
            //    name: "BillType",
            //    table: "AdditionVoucher");

            //migrationBuilder.DropColumn(
            //    name: "UserType",
            //    table: "AdditionVoucher");
        }
    }
}
