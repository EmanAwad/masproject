﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MasProject.Data.Migrations
{
    public partial class EntriesAndEntriesDetails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Entries_Branch_BranchId",
                table: "Entries");

            migrationBuilder.DropIndex(
                name: "IX_Entries_BranchId",
                table: "Entries");

            migrationBuilder.DropColumn(
                name: "AccountName",
                table: "Entries");

            migrationBuilder.DropColumn(
                name: "AccountNo",
                table: "Entries");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "Entries");

            migrationBuilder.DropColumn(
                name: "Credit",
                table: "Entries");

            migrationBuilder.DropColumn(
                name: "DRCR_Difference",
                table: "Entries");

            migrationBuilder.DropColumn(
                name: "Debit",
                table: "Entries");

            migrationBuilder.DropColumn(
                name: "Total",
                table: "Entries");

            migrationBuilder.AddColumn<int>(
                name: "AccountingEntryDetails",
                table: "Entries",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "EntriesDetailsId",
                table: "Entries",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "EntriesNo",
                table: "Entries",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "EntryDate",
                table: "Entries",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "EntryType",
                table: "Entries",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "EntriesDetails",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    DeletedBy = table.Column<string>(nullable: true),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    AccountNo = table.Column<int>(nullable: false),
                    AccountName = table.Column<string>(nullable: true),
                    Debit = table.Column<decimal>(nullable: false),
                    Credit = table.Column<decimal>(nullable: false),
                    BranchId = table.Column<int>(nullable: true),
                    Total = table.Column<decimal>(nullable: false),
                    DRCR_Difference = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EntriesDetails", x => x.ID);
                    table.ForeignKey(
                        name: "FK_EntriesDetails_Branch_BranchId",
                        column: x => x.BranchId,
                        principalTable: "Branch",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Entries_EntriesDetailsId",
                table: "Entries",
                column: "EntriesDetailsId");

            migrationBuilder.CreateIndex(
                name: "IX_EntriesDetails_BranchId",
                table: "EntriesDetails",
                column: "BranchId");

            migrationBuilder.AddForeignKey(
                name: "FK_Entries_EntriesDetails_EntriesDetailsId",
                table: "Entries",
                column: "EntriesDetailsId",
                principalTable: "EntriesDetails",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Entries_EntriesDetails_EntriesDetailsId",
                table: "Entries");

            migrationBuilder.DropTable(
                name: "EntriesDetails");

            migrationBuilder.DropIndex(
                name: "IX_Entries_EntriesDetailsId",
                table: "Entries");

            migrationBuilder.DropColumn(
                name: "AccountingEntryDetails",
                table: "Entries");

            migrationBuilder.DropColumn(
                name: "EntriesDetailsId",
                table: "Entries");

            migrationBuilder.DropColumn(
                name: "EntriesNo",
                table: "Entries");

            migrationBuilder.DropColumn(
                name: "EntryDate",
                table: "Entries");

            migrationBuilder.DropColumn(
                name: "EntryType",
                table: "Entries");

            migrationBuilder.AddColumn<string>(
                name: "AccountName",
                table: "Entries",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AccountNo",
                table: "Entries",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "Entries",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Credit",
                table: "Entries",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "DRCR_Difference",
                table: "Entries",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "Debit",
                table: "Entries",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "Total",
                table: "Entries",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.CreateIndex(
                name: "IX_Entries_BranchId",
                table: "Entries",
                column: "BranchId");

            migrationBuilder.AddForeignKey(
                name: "FK_Entries_Branch_BranchId",
                table: "Entries",
                column: "BranchId",
                principalTable: "Branch",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
