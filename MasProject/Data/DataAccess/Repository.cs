﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace MasProject.Domain.Services
{
    public class Repository<T> where T : class, IEntity
    {
        protected DBContext dbContext;
        protected DbSet<T> dbSet;

        public Repository(DBContext context)
        {
            dbContext = context;
            dbSet = dbContext.Set<T>();
        }
        public virtual T Insert(T item)
        {
            dbContext.Entry(item).State = EntityState.Added;
            item.IsDeleted = false;
            item.CreatedDate = DateTime.Now;
            //item.CreatedBy = "1";
            return dbContext.Set<T>().Add(item).Entity;
        }
        public virtual T Update(T item)
        {
            //DetachTrackedEntities();
            item.ModifiedDate = DateTime.Now;
            //item.ModifiedBy = "1";
            dbContext.Entry(item).State = EntityState.Modified;
            return item;
        }
        public virtual T Delete(T item)
        {
            item.IsDeleted = true;
            item.DeletedDate = DateTime.Now;
            item.DeletedBy = "1";
            dbContext.Entry(item).State = EntityState.Modified;
            return item;
        }
        public virtual List<T> GetWith(Expression<Func<T, bool>> where)
        {
            var result = dbSet.Where(where).ToList();

            return result;
        }
        public bool IsExist(string name, int id)
        {
            if (id == 0)
            {
                // return dbSet.Any(g => g.Name.CompareTo(name)==0);
                return dbSet.Any(g => EF.Functions.Like(g.Name, name)& g.IsDeleted==false);
            }
            else
            {
                return dbSet.Any(g => EF.Functions.Like(g.Name, name) & g.ID != id & g.IsDeleted == false);
            }
        }
        public virtual List<T> GetWith<TProperty>(Expression<Func<T, bool>> where, params Expression<Func<T, TProperty>>[] includes)
        {
            var query = dbSet.AsQueryable();
            foreach (var include in includes)
                query = query.Include(include);

            var result = query.Where(where).ToList();

            return result;
        }
        protected virtual void Remove(T entity)
        {
            dbContext.Set<T>().Remove(entity);
        }
        public int GenerateSerial(int? BranchId)
        {
            string Serial = "";
            int PrfixNumber = 4;
            try
            {
                if (BranchId != null && BranchId > 0)
                    Serial = dbContext.Branch.FirstOrDefault(x => x.IsDeleted == false && x.ID == BranchId).arrangement.ToString();
                else
                    Serial = dbContext.Branch.FirstOrDefault(x => x.IsDeleted == false && x.ID == 3).arrangement.ToString();
                Serial += DateTime.Now.ToString("yy");//
                PrfixNumber = Serial.Count();

                var model = dbSet.Where(x => x.IsDeleted == false && x.BranchId == BranchId && x.Name == "").OrderByDescending(s => s.SerialNumber).FirstOrDefault();
                int SerialNumber = model != null ? (model.SerialNumber.Value + 1) : 0;
                Serial += SerialNumber > 0 ? SerialNumber.ToString().Substring(PrfixNumber, SerialNumber.ToString().Length - PrfixNumber) : "00001";
            }
            catch (Exception ex)
            {
                string message = ex.Message;
            }

            return int.Parse(Serial);
        }

       
    }
}

