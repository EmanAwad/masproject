﻿using MasProject.Data.Models;
using MasProject.Data.Models.Address;
using MasProject.Data.Models.Hierarchy;
using MasProject.Data.Models.Items;
using MasProject.Data.Models.Persons;
using MasProject.Data.Models.StoreTransaction.Quotation;
using MasProject.Data.Models.StoreTransaction.Sales;
using MasProject.Data.Models.StoreTransaction.Purchase;
using MasProject.Data.Models.StoreTransaction.Voucher;
using MasProject.Data.Models.StoreTransaction.Transfer;
using Microsoft.EntityFrameworkCore;
using MasProject.Data.Models.StoreTransaction;
using MasProject.Data.Models.StoreTransaction.ExecutionWithDrawals;
using MasProject.Data.Models.FundTransaction.ClientPayments;
using MasProject.Data.Models.FundTransaction.DiscountNotices;
using MasProject.Data.Models.Trans;
using MasProject.Data.Models.FundTransaction.SupplierPayments;
using MasProject.Data.Models.FundTransaction.AdditionNotice;
using MasProject.Data.Models.FundTransaction;
using MasProject.Data.Models.TransTables;
using System.Linq;
using MasProject.Data.Models.StoreTransaction.Conversion;
using MasProject.Data.Models.Accounting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using MasProject.Data.Models.Login;

namespace MasProject.Data.DataAccess
{
    public class DBContext : IdentityDbContext<ApplicationUser>// DbContext:
    {

        public DBContext(DbContextOptions<DBContext> options) : base(options)
        {

        }
        public virtual DbSet<Employee> Employee { get; set; }
        public virtual DbSet<Supplier> Supplier { get; set; }
        public virtual DbSet<Social_Client> Social_Client { get; set; }
        public virtual DbSet<Clients> Client { get; set; }
        public virtual DbSet<EmployeeOpeningBalance> EmployeeOpeningBalance { get; set; }
        public virtual DbSet<SupplierOpeningBalance> SupplierOpeningBalance { get; set; }
        public virtual DbSet<SupplierOpeningBalanceDetails> SupplierOpeningBalanceDetails { get; set; }
        public virtual DbSet<ClientOpeningBalance> ClientOpeningBalance { get; set; }
        public virtual DbSet<ClientOpeningBalanceDetails> ClientOpeningBalanceDetails { get; set; }

        public virtual DbSet<PurchasesArchives> PurchasesArchives { get; set; }
        public virtual DbSet<SalesArchives> SalesArchives { get; set; }
        public virtual DbSet<Lookup_Area> Lookup_Area { get; set; }
        public virtual DbSet<Lookup_City> Lookup_City { get; set; }
        public virtual DbSet<Lookup_Country> Lookup_Country { get; set; }
        public virtual DbSet<Branch> Branch { get; set; }
        public virtual DbSet<Currency> Currency { get; set; }
        public virtual DbSet<Department> Department { get; set; }

        public virtual DbSet<SocialMedia> SocialMEdia { get; set; }
        public virtual DbSet<Company> Companies { get; set; }
        //public virtual DbSet<Branch> Branches { get; set; }
        public virtual DbSet<Store> Stores { get; set; }
        public virtual DbSet<Treasury> Treasuries { get; set; }
        public virtual DbSet<TreasuryOpenBalance> TreasuryOpenBalances { get; set; }
        public virtual DbSet<ItemCollection> ItemsCollection { get; set; }
        public virtual DbSet<ItemType> ItemTypes { get; set; }
        public virtual DbSet<Item> Items { get; set; }
        public virtual DbSet<ItemOpeningBalance> ItemsOpeningBalance { get; set; }
        public virtual DbSet<ItemOpeningBalanceDetails> ItemsOpeningBalanceDetails { get; set; }
        public virtual DbSet<Item_PriceHistory> Item_PriceHistory { get; set; }
        public virtual DbSet<OtherIncome> OtherIncome { get; set; }
        public virtual DbSet<SellingExpenses> SellingExpenses { get; set; }
        public virtual DbSet<Item_Store> Item_Store { get; set; }
        //New 31-08-2020
        public virtual DbSet<SalesBills> SalesBills { get; set; }
        public virtual DbSet<DealType> DealTypes { get; set; }
        public virtual DbSet<SalesItems> SalesItems { get; set; }
        public virtual DbSet<PurchaseBill> PurchaseBill { get; set; }
        public virtual DbSet<PurchaseItem> PurchaseItem { get; set; }
        public virtual DbSet<QuotationClass> Quotations { get; set; }
        public virtual DbSet<QuotationItems> QuotationItems { get; set; }
        //New 14-09-2020
        public virtual DbSet<ExecutiveBills> ExecutiveBills { get; set; }
        public virtual DbSet<ExecutiveItems> ExecutiveItems { get; set; }
        public virtual DbSet<ExecutiveSellExpenses> ExecutiveSellExpenses { get; set; }
        public virtual DbSet<ExecutiveOtherIncome> ExecutiveOtherIncome { get; set; }
        public virtual DbSet<MaintanceSellExpenses> MaintanceSellExpenses { get; set; }
        public virtual DbSet<MantienceBills> MantienceBills { get; set; }
        public virtual DbSet<MantienceItems> MantienceItems { get; set; }
        public virtual DbSet<SalesReturns> SalesReturns { get; set; }
        public virtual DbSet<SalesReturnsItems> SalesReturnsItems { get; set; }
        public virtual DbSet<SalesSellExpenses> SalesSellExpenses { get; set; }
        public virtual DbSet<AdditionVoucher> AdditionVoucher { get; set; }
        public virtual DbSet<AdditionVoucherItem> AdditionVoucherItem { get; set; }
        public virtual DbSet<TempAdditionVoucher> TempAdditionVoucher { get; set; }
        public virtual DbSet<TempAdditionVoucherItem> TempAdditionVoucherItem { get; set; }
        public virtual DbSet<PaymentVoucher> PaymentVoucher { get; set; }
        public virtual DbSet<PaymentVoucherItem> PaymentVoucherItem { get; set; }
        public virtual DbSet<TempPaymentVoucher> TempPaymentVoucher { get; set; }
        public virtual DbSet<TempPaymentVoucherItem> TempPaymentVoucherItem { get; set; }
        public virtual DbSet<PurchasesReturns_Bill> PurchasesReturns_Bill { get; set; }
        public virtual DbSet<PurchasesReturns_Items> PurchasesReturns_Items { get; set; }
        //new 25-09-2020
        public virtual DbSet<PurchaseOtherIncome> PurchaseOtherIncome { get; set; }
        public virtual DbSet<PurchaseReturns_OtherIncome> PurchaseReturns_OtherIncome { get; set; }
        public virtual DbSet<TransferRequest> TransferRequest { get; set; }
        public virtual DbSet<TransferRequestItem> TransferRequestItem { get; set; }
        public virtual DbSet<TransferOrder> TransferOrder { get; set; }
        public virtual DbSet<TransferOrderDetails> TransferOrderDetails { get; set; }
        //new 10-10-2020
        public virtual DbSet<SalesReturnsExpenses> SalesReturnsExpenses { get; set; }
        public virtual DbSet<ExecutionWithDrawals_Bill> ExecutionWithDrawals_Bill { get; set; }
        public virtual DbSet<ExecutionWithDrawals_Items> ExecutionWithDrawals_Items { get; set; }
        public virtual DbSet<OtherConstant> OtherConstant { get; set; }
        public virtual DbSet<ClientPayment> ClientPayment { get; set; }
        public virtual DbSet<DiscountNotice> DiscountNotice { get; set; }
        //new 26-10-2020
        public virtual DbSet<TransClient> TransClient { get; set; }
        public virtual DbSet<TransSupplier> TransSupplier { get; set; }
        public virtual DbSet<SupplierPayment> SupplierPayment { get; set; }
        public virtual DbSet<AdditionNotice> AdditionNotice { get; set; }
        public virtual DbSet<EmployeesLoansEntry> EmployeesLoansEntry { get; set; }
        public virtual DbSet<EmployeesLoansPayment> EmployeesLoansPayment { get; set; }
        public virtual DbSet<TransEmployee> TransEmployee { get; set; }
        public  virtual DbSet<SalesBillStores> SalesBillStores { get; set; }

        public virtual DbSet<ItemConversion_Bill> ItemConversion_Bill { get; set; }
        public virtual DbSet<ItemConversion_Items> ItemConversion_Items { get; set; }

        public virtual DbSet<ClientsPayoffs> ClientPayoffs { get; set; }
        //new 04-12-2020
        public virtual DbSet<ExecutiveBillStores> ExecutiveBillStores { get; set; }
        public virtual DbSet<MaintanceBillStores> MaintanceBillStores { get; set; }
        public virtual DbSet<SalesReturnsBillStores> SalesReturnsBillStores { get; set; }
        //11-12-2020
        public virtual DbSet<ExecutionWithdrawl_BillStores> ExecutionWithdrawl_BillStores { get; set; }

        public virtual DbSet<EntriesDetails> EntriesDetails { get; set; }
        public virtual DbSet<Entries> Entries { get; set; }
        public virtual DbSet<AccountingGuide> AccountingGuide { get; set; }
        //new 22-01-2021
        public virtual DbSet<TransItem> TransItem { get; set; }

        public virtual DbSet<EntriesStructuring> EntriesStructuring { get; set; }
        public virtual DbSet<EntriesStructuring_Details> EntriesStructuring_Details { get; set; }
        public virtual DbSet<EntriesType> EntriesType { get; set; }
        //new 27-08-2021
        public virtual DbSet<EmployeeFiles> EmployeeFiles { get; set; }
    }
}