﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Data.Models
{
   public class ApplicationSettings
    {
        public string JWT_Secret { get; set; }
        public string Issuer { get; set; }

        public string Audience { get; set; }

        public string Client_URL { get; set; }
    }
}
