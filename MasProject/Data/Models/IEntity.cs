﻿using System;

namespace MasProject.Data.Models
{
    public interface IEntity
    {
        public int ID { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string DeletedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public string Name { get; set; }
        //edit 15-10-2021
        public int? BranchId { get; set; }
        public int? SerialNumber { get; set; }
    }
}
