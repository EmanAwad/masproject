﻿using MasProject.Data.Models.Hierarchy;
using System;
using MasProject.Data.Models.Items;
using MasProject.Data.Models.Persons;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MasProject.Data.Models.Accounting
{
   public class EntriesDetails : AccountingEntity
    {
       
        public int? AccountNo { get; set; }

        public string AccountName { get; set; }

        public decimal? Debit { get; set; }

        public decimal? Credit { get; set; }

        //مركز التكلفة
        public int? CostCenterId { get; set; }
       
       // public virtual Branch BranchObj { get; set; }

        public decimal? TotalDR { get; set; }

        public decimal? TotalCR { get; set; }

        public decimal? DRCR_Difference { get; set; }

        public int? EntriesId { get; set; }
        [ForeignKey("EntriesId")]
        public Entries Entries { get; set; }

        public string DetailDesc_Note { get; set; }

        ///should be exist for reports and other screens entries
        // public int? ClientId { get; set; }
        //public virtual Clients clientObj { get; set; }
        //public int? StoreId { get; set; }
        //public virtual Store StoreObj { get; set; }
        //public int? ItemId { get; set; }
        //public virtual Item ItemsObj { get; set; }
        //when eman finish it is screen
        //public int? AccountId { get; set; }
        //public virtual Account AccountObj { get; set; }
    }
}
