﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
namespace MasProject.Data.Models.Accounting
{
    public class AccountingGuide : AccountingEntity
    {
        public int Code { get; set; }
        public int AccountCode { get; set; }
        public bool FlagType { get; set; } // true Income false budget
        public int? ParentId { get; set; }
        [ForeignKey("ParentId")]
        public AccountingGuide ParentObj { get; set; }

        public bool CostCenter { get; set; }
    }
}
