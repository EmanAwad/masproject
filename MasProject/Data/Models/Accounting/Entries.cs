﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MasProject.Data.Models.Accounting
{
    public class Entries : AccountingEntity
    {
        public int EntriesNo { get; set; }

        public DateTime? EntryDate { get; set; }

        public string EntryType { get; set; }

        public string EntryDesc_Note { get; set; }
    }
}
