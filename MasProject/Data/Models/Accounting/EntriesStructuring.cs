﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Data.Models.Accounting
{
   public class EntriesStructuring : AccountingEntity
    {
        public int EntriesStructureNo { get; set; }

        public DateTime? EntryDate { get; set; }

        public string EntryType { get; set; }
    }
}
