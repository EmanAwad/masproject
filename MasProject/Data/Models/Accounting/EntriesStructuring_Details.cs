﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MasProject.Data.Models.Accounting
{
   public class EntriesStructuring_Details : AccountingEntity
    {
        public int? AccountNo { get; set; }

        public string AccountName { get; set; }

        public decimal? Debit { get; set; }

        public decimal? Credit { get; set; }

        //مركز التكلفة
        public int? CostCenterId { get; set; }

        // public virtual Branch BranchObj { get; set; }

        public decimal? TotalDR { get; set; }

        public decimal? TotalCR { get; set; }

        public decimal? DRCR_Difference { get; set; }

        public int? EntriesStructuringId { get; set; }
        [ForeignKey("EntriesStructuringId")]
        public EntriesStructuring EntriesStructuring { get; set; }

        ///should be exist for reports and other screens entries
        // public int? ClientId { get; set; }
        //public virtual Clients clientObj { get; set; }
        //public int? StoreId { get; set; }
        //public virtual Store StoreObj { get; set; }
        //public int? ItemId { get; set; }
        //public virtual Item ItemsObj { get; set; }
        //when eman finish it is screen
        //public int? AccountId { get; set; }
        //public virtual Account AccountObj { get; set; }
    }
}
