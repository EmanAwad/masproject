﻿using MasProject.Data.Models.Persons;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace MasProject.Data.Models.Items
{
    public class Item_PriceHistory : BaseEntity
    {


        public int? ItemId { get; set; }
        [ForeignKey("ItemId")]
        public Item Item { get; set; }

        public DateTime Date { get; set; }
        public float? Price { get; set; }


        public int? EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public Employee Employee { get; set; }
    }
}
