﻿using MasProject.Data.Models.Hierarchy;
using System;
using System.ComponentModel.DataAnnotations;
using MasProject.Data.Models.Persons;
using System.ComponentModel.DataAnnotations.Schema;

namespace MasProject.Data.Models.Items
{
    public class ItemOpeningBalance : IEntity
    {
        public DateTime Date { get; set; }
        public int DocumentNumber { get; set; }
        public int? StoreId { get; set; }
        [ForeignKey("StoreId")]
        public Store Store { get; set; }
        public int? EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public Employee Employee { get; set; }
        public bool IsPrinted { get; set; }
        public string Name { get; set; } = "";
        public int ID { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string DeletedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public string Notes { get; set; }
        public int? BranchId { get; set; }
        public int? SerialNumber { get; set; }
    }
}
