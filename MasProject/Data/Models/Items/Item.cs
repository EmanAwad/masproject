﻿using MasProject.Data.Models.Persons;
using MasProject.Data.Models.Hierarchy;
using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace MasProject.Data.Models.Items
{
    public class Item : BaseEntity
    {

        public int? CollectionId { get; set; }
        //[ForeignKey("CollectionId")]
        //public ItemCollection ItemCollection { get; set; }


        public int? TypeId { get; set; }
        //[ForeignKey("TypeId")]
        //public ItemType ItemType { get; set; }

        public int? StoreId { get; set; }
        //[ForeignKey("StoreId")]

        //public Store Store { get; set; }

        public string National_ParCode { get; set; }
        public string Interational_ParCode { get; set; }
        public string Box_ParCode { get; set; }
        public string Supplier_ParCode { get; set; }

        public string ParCode_Note { get; set; }


        public float? CostPrice { get; set; }



        public float? SupplierPrice { get; set; }
        public float? WholesalePrice { get; set; }
        public float? SellingPrice { get; set; }
        public float? ExecutionPrice { get; set; }
        public int? other_Price { get; set; }


        public int? SupplierId { get; set; }
        //[ForeignKey("SupplierId")]
        //public Supplier Supplier { get; set; }


        public int? BoxNumber { get; set; }
        public float? CBM { get; set; }
        public float? BoxWeight { get; set; }



        public string ItemImg { get; set; }
        public DateTime ItemDate { get; set; }


        public float? ShowPrice { get; set; }
        public string Guarantee_ParCode { get; set; }

        public bool IsShow { get; set; }
    }
}
