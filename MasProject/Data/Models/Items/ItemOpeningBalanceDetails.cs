﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace MasProject.Data.Models.Items
{
    public class ItemOpeningBalanceDetails : IEntity
    {
        public int ID { get; set; }
        public int ItemDocumentNumber { get; set; }
        public string Name { get; set; } = "";
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string DeletedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public int? ItemId { get; set; }
        [ForeignKey("ItemId")]
        public Item Item { get; set; }
        public double? price { get; set; }
        public float? TotalPrice { get; set; }
        public int? Quantity { get; set; }
        public string Notes { get; set; }
        public int? BranchId { get; set; }
        public int? SerialNumber { get; set; }

    }
}
