﻿using MasProject.Data.Models.Hierarchy;
using MasProject.Data.Models.Persons;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MasProject.Data.Models.FundTransaction
{
   public class EmployeesLoansEntry : IEntity
    {
        [Key, Column(Order = 0)]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string DeletedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public string Name { get; set; } = "";

        public string Note { get; set; }

        public int? BranchId { get; set; }
        public int? SerialNumber { get; set; }
        public DateTime Date { get; set; }

        public int? TreasuryId { get; set; }
        [ForeignKey("TreasuryId")]
        public Treasury Treasury { get; set; }

        public int DocumentNo { get; set; }

        public int? EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public Employee Employee { get; set; }

        public double? Amount { get; set; }



    }
}
