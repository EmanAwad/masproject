﻿using MasProject.Data.Models.Hierarchy;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MasProject.Data.Models
{
  public  class SaleBaseEntity :IEntity
    {
        [Key, Column(Order = 0)]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string DeletedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public string Note { get; set; }
        public string Name { get; set; } = "";
        //columns for This Modules
        public int? SerialNumber { get; set; }
        public DateTime Date { get; set; }
        public int? StoreId { get; set; }
        [ForeignKey("StoreId")]
        public Store Store { get; set; }
        public int? DealTypeId { get; set; }
        [ForeignKey("DealTypeId")]
        public DealType DealType { get; set; }
        public bool FlagType { get; set; }
        public decimal? TotalPrice { get; set; }
        public decimal? DiscountPrecentage { get; set; }
        public decimal? DiscountValue { get; set; }
        public decimal? TotalAfterDiscount{ get; set; }
        public bool ShowNotesFlag { get; set; }
        public int UserId { get; set; }
        public int RevewId { get; set; }
        public int ApproveId { get; set; }
        public int? BranchId { get; set; }
        [ForeignKey("BranchId")]
        public virtual Branch BranchObj { get; set; }
    }
}
