﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Data.Models.StoreTransaction.Voucher
{
    public class AdditionVoucher : VoucherBaseEntity
    {
        public int? Client_Supplier_Id { get; set; }
        public int? BillSerialNumber { get; set; }
        public string BillType { get; set; }
        //1 clients
        //2 supplier
        //3 employee
        public int UserType { get; set; }
    }
}
