﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Data.Models.StoreTransaction.Voucher
{
   public class TempAdditionVoucher : VoucherBaseEntity
    {
        public int? Client_Supplier_Id { get; set; }
        public int? BillSerialNumber { get; set; }
        public string BillType { get; set; }
        public int UserType { get; set; }

    }
}