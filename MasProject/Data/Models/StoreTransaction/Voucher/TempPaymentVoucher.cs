﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Data.Models.StoreTransaction.Voucher
{
   public class TempPaymentVoucher : VoucherBaseEntity
    {
        public int? Client_Supplier_Id { get; set; }
        public int? BillSerialNumber { get; set; }
        //"فاتورة مبيعات"  "أمر التحويل"  "فاتورة تنفيذ""فاتورة صيانة"
        //"مردودات مشتريات"  "التحويل من صنف لصنف" "مسحوبات التنفيذ"
        public string BillType { get; set; }
        public int UserType { get; set; }//1 client 2 supplier 3 employee

    }
}