﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Data.Models.StoreTransaction.Voucher
{
    public class TempAdditionVoucherItem : VoucherItemBaseEntity
    {
        public int? SerialNumber { get; set; }
    }
}