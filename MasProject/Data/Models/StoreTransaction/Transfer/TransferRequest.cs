﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using MasProject.Data.Models.Items;
using MasProject.Data.Models.Hierarchy;
namespace MasProject.Data.Models.StoreTransaction.Transfer
{
   public class TransferRequest :BaseEntity
    {
        public int SerialNumber { get; set; }
        public DateTime Date { get; set; }
        public int? BranchToID { get; set; }
        [ForeignKey("BranchToID")]
        public Branch BranchTo { get; set; }
        public bool IsOrder { get; set; }
    }
}
