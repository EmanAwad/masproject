﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using MasProject.Data.Models.Items;
using MasProject.Data.Models.Hierarchy;
namespace MasProject.Data.Models.StoreTransaction.Transfer
{
    public class TransferOrder : BaseEntity
    {
        public int SerialNumber { get; set; }
        public DateTime Date { get; set; }
        public int? BranchToID { get; set; }
        [ForeignKey("BranchToID")]
        public Branch BranchTo { get; set; }
        public int? TransferRequestId { get; set; }
        [ForeignKey("TransferRequestId")]
        public TransferRequest TransferRequest { get; set; }
        public int? UserID;
        public bool IsConverted { get; set; }

    }
}
