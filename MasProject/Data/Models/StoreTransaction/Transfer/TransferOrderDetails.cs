﻿using System.ComponentModel.DataAnnotations.Schema;
using MasProject.Data.Models.Items;
using MasProject.Data.Models.Hierarchy;

namespace MasProject.Data.Models.StoreTransaction.Transfer
{
   public class TransferOrderDetails : BaseEntity
    {
        public double? Quantity { get; set; }
        public int? ItemId { get; set; }

        [ForeignKey("ItemId")]
        public Item Item { get; set; }

        public int? BranchFromID { get; set; }
        [ForeignKey("BranchFromID")]
        public Branch BranchFrom { get; set; }

        public int? TransferOrderId { get; set; }
        [ForeignKey("TransferOrderId")]
        public TransferOrder TransferOrder { get; set; }

    }
}

