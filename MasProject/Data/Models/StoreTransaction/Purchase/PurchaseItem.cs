﻿
namespace MasProject.Data.Models.StoreTransaction.Purchase
{
   public class PurchaseItem : SalesItemsBaseEntity
    {
        public decimal? SavedPrice { get; set; }
        public int? SerialNumber { get; set; }
        //[ForeignKey("SerialNumber")]
        //public PurchaseBill PurchaseBill { get; set; }
    }
}

