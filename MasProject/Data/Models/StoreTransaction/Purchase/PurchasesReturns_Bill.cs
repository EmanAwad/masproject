﻿using MasProject.Data.Models.Persons;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MasProject.Data.Models
{
   public  class PurchasesReturns_Bill : SaleBaseEntity
    {
        public int? SupplierId { get; set; }
        [ForeignKey("SupplierId")]
        public Supplier Supplier { get; set; }
        public bool IsConverted { get; set; }
        //New 24-12-2020
        public decimal TotalAfterTax { get; set; }
        public decimal SourceDeduction { get; set; }
        public decimal SourceDeductionAmount { get; set; }
        public decimal AddtionTax { get; set; }
        public decimal AddtionTaxAmount { get; set; }
        //public int TaxFileNumber { get; set; }
    }
}
