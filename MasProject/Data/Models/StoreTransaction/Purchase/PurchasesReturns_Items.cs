﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Data.Models.StoreTransaction.Purchase
{
   public class PurchasesReturns_Items : SalesItemsBaseEntity
    {
        public decimal? SavedPrice { get; set; }
        public int? SerialNumber { get; set; }
    }
}
