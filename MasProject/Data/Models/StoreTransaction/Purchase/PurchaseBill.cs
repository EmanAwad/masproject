﻿using MasProject.Data.Models.Persons;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MasProject.Data.Models.StoreTransaction.Purchase
{
   public class PurchaseBill : SaleBaseEntity
    {
        public int? SupplierId { get; set; }
        [ForeignKey("SupplierId")]
        public Supplier Supplier { get; set; }
        public bool IsConverted { get; set; }
        //new 24-12-2020
        public decimal TotalAfterTax { get; set; }
        public decimal SourceDeduction { get; set; }
        public decimal SourceDeductionAmount { get; set; }
        public decimal AddtionTax { get; set; }
        public decimal AddtionTaxAmount { get; set; }
        //public string TaxFileNumber { get; set; }
    }
}
