﻿using MasProject.Data.Models.Persons;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using MasProject.Data.Models.Hierarchy;
namespace MasProject.Data.Models.StoreTransaction.Quotation
{
   public class QuotationClass : SaleBaseEntity
    {
        //public int? BranchId { get; set; }
        //[ForeignKey("BranchId")]
        //public Branch BranchObj { get; set; }
        public string Client { get; set; }
        public bool IsConverted { get; set; } 
        //new 21-10-2020
        public decimal TotalAfterTax { get; set; }
        public decimal SourceDeduction { get; set; }
        public decimal SourceDeductionAmount { get; set; }
        public decimal AddtionTax { get; set; }
        public decimal AddtionTaxAmount { get; set; }

        public int? TaxFileNumber { get; set; }

       
    }
}
