﻿

namespace MasProject.Data.Models.StoreTransaction.Quotation
{
   public class QuotationItems : SalesItemsBaseEntity
    {
        public decimal? SavedPrice { get; set; }
        public int?  SerialNumber { get; set; }
        //[ForeignKey("SerialNumber")]
        //public QuotationClass Quotation { get; set; }
    }
}
