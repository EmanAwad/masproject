﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Data.Models.StoreTransaction.ExecutionWithDrawals
{
   public class ExecutionWithDrawals_Items : SalesItemsBaseEntity
    {
        public decimal? SavedPrice { get; set; }
        public int? SerialNumber { get; set; }
        public int? StoreId { get; set; }
    }
}
