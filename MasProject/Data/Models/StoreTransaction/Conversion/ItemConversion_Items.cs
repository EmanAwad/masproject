﻿using MasProject.Data.Models.Items;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MasProject.Data.Models.StoreTransaction.Conversion
{
   public class ItemConversion_Items : BaseEntity
    {
        public int? ConversionRecordID { get; set; }
    //  [ForeignKey("ConversionRecordID")]
     // public ItemConversion_Bill ConversionBill { get; set; }
        public int? ItemIdFrom { get; set; }
        [ForeignKey("ItemIdFrom")]
        public Item ItemFrom { get; set; }

        public int? ItemIdTo { get; set; }
        [ForeignKey("ItemIdTo")]
        public Item ItemTo { get; set; }
        public int? QuantityFrom { get; set; }

        public int? QuantityTo { get; set; }

        
        public decimal? PriceTo { get; set; }
        public decimal? PriceFrom { get; set; }
        public decimal? TotalTo { get; set; }
        public decimal? TotalFrom { get; set; }

        public int? IdentiferFrom { get; set; }

        public int? IdentiferFromTo { get; set; }

         public string ParCodeFrom { get; set; }
        public string ParCodeTo { get; set; }

       
    }
}
