﻿using MasProject.Data.Models.Hierarchy;
using MasProject.Data.Models.Items;
using MasProject.Data.Models.Persons;
using MasProject.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MasProject.Data.Models.StoreTransaction.Conversion
{
   public class ItemConversion_Bill : BaseEntity
    {
      
        public int? ConversionRecordID { get; set; }
        public  DateTime Date { get; set; }

        public int? EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public Employee Employee { get; set; }



        public string ConversionReason { get; set; }

        public int? StoreId { get; set; }
        [ForeignKey("StoreId")]
        public Store Store { get; set; }

        public decimal? TotalPriceFrom { get; set; }

        public decimal? TotalPriceTo { get; set; }
        public bool IsConverted { get; set; }


    }
}
