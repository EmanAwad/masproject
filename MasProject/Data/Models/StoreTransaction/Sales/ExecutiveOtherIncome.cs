﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System.Text;
using MasProject.Data.Models.Items;

namespace MasProject.Data.Models.StoreTransaction.Sales
{
   public class ExecutiveOtherIncome : IEntity
    {
        [Key, Column(Order = 0)]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string DeletedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public string Name { get; set; }
        //Columns For This Modules
        public int? IncomeId { get; set; }
        [ForeignKey("IncomeId")]
        public OtherIncome Income { get; set; }
        public decimal? Amount { get; set; }
        public int? Identifer { get; set; }
        public virtual ExecutiveBills ExecutiveBill { get; set; }
        public int? BranchId { get; set; }
        public int? SerialNumber { get; set; }//bill foriegn key
    }
}
