﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Data.Models.StoreTransaction.Sales
{
 public   class SalesReturnsBillStores : IEntity
    {
        public int ID { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string DeletedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public string Name { get; set; }
        public int? BranchId { get; set; }
        public int? SerialNumber { get; set; }
        public int? StoreId { get; set; }
    }
}
