﻿using MasProject.Data.Models.Persons;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MasProject.Data.Models.StoreTransaction.Sales
{
 public   class SalesBills :SaleBaseEntity
    {
        public int? ClientId { get; set; }
        [ForeignKey("ClientId")]
        public Clients Client { get; set; }
       
        public bool IsConverted { get; set; }
        //new 21-10-2020
        public decimal TotalAfterTax { get; set; }
        public decimal SourceDeduction { get; set; }
        public decimal SourceDeductionAmount { get; set; }
        public decimal AddtionTax { get; set; }
        public decimal AddtionTaxAmount { get; set; }
        public bool MultiStore { get; set; }
    }
}
