﻿

namespace MasProject.Data.Models.StoreTransaction.Sales
{
 public   class MantienceItems : SalesItemsBaseEntity
    {
        public decimal? SavedPrice { get; set; }
        public int? SerialNumber { get; set; }
        public int? StoreId { get; set; }

    }
}
