﻿using MasProject.Data.Models.Trans;
using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Data.Models.TransTables
{
   public class TransSupplier : TransEntity
    {
        public int SupplierType { get; set; }
        public int SupplierId { get; set; }
    }
}
