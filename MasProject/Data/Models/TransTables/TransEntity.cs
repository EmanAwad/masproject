﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Data.Models.Trans
{
  public  class TransEntity : IEntity
    {
        public int ID { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime Date { get; set; }
        public int? SerialNumber { get; set; }
        public decimal Debit { get; set; }//مدين//وارد
        public decimal Credit { get; set; }//دائن//منصرف
        public decimal Balance { get; set; }
        public bool OpenBalFlag { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string DeletedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public string Name { get; set; } = "";
        public int? BranchId { get; set; }
    }
}
