﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Data.Models.Trans
{
   public  class TransEmployee : TransEntity
    {
        public int EmployeeType { get; set; }
        public int EmployeeId { get; set; }
    }
}
