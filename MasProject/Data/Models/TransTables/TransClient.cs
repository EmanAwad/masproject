﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Data.Models.Trans
{
    public class TransClient : TransEntity
    {
        public int ClientType { get; set; }
        public int ClientId { get; set; }
    }   
}
