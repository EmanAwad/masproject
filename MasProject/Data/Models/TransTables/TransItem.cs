﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Data.Models.Trans
{
    public class TransItem : TransEntity
    {
        public int ItemType { get; set; }
        public int ItemId { get; set; }
        public int StoreId { get; set; }
    }
}
