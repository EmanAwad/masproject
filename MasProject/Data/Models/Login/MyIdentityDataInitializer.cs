﻿using System;
using System.Collections.Generic;
using System.Text;
using MasProject.Data.DataAccess;
using MasProject.Data.Models.Login;
using Microsoft.AspNetCore.Identity;

namespace MasProject.Data.Models
{
  public  class MyIdentityDataInitializer
    {
        public static void SeedRoles(RoleManager<IdentityRole> roleManager)
        {
            if (!roleManager.RoleExistsAsync("Administrator").Result)
            {
                IdentityRole role = new IdentityRole();
                role.Name = "Administrator";
                IdentityResult roleResult = roleManager.
                CreateAsync(role).Result;
            }
            if (!roleManager.RoleExistsAsync("UserSystem").Result)
            {
                IdentityRole role = new IdentityRole();
                role.Name = "UserSystem";
                IdentityResult roleResult = roleManager.
                CreateAsync(role).Result;
            }
        }
        public static void SeedUsers(UserManager<ApplicationUser> userManager)
        {
            if (userManager.FindByNameAsync("AdminMAS@MasSystem.com").Result == null)
            {
                ApplicationUser user = new ApplicationUser();
                user.UserName = "Ehab Samir";
                user.Email = "AdminMAS@MasSystem.com";
                //user.UserPlace = false;
                IdentityResult result = userManager.CreateAsync(user, "Ehab@123").Result;//zXS#$QR?!4S&9rY
                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "Administrator").Wait();
                }
            }
            if (userManager.FindByNameAsync("SalahAdb@MasSystem.com").Result == null)
            {
                ApplicationUser user = new ApplicationUser();
                user.UserName = "Salah Adb ElAzim";
                user.Email = "SalahAdb@MasSystem.com";
                //user.UserPlace = false;
                IdentityResult result = userManager.CreateAsync(user, "Salah@123").Result;
                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "UserSystem").Wait();
                }
            }
        }
        public static void SeedData(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, DBContext context)
        {
            SeedRoles(roleManager);
            SeedUsers(userManager);
            //SetProfileId(context);
        }
    }
}
