﻿using MasProject.Data.DataAccess;
using MasProject.Data.Models.Login;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace MasProject.Data.Models
{
    public class SeedDatabase
    {
        public static void Intialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetRequiredService<DBContext>();
            var userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            context.Database.EnsureCreated();
        }
    }
}
