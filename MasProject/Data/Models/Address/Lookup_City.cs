﻿using System.ComponentModel.DataAnnotations.Schema;

namespace MasProject.Data.Models.Address
{
    public class Lookup_City : BaseEntity
    {
        public int? CountryID { get; set; }
        [ForeignKey("CountryID")]
        public Lookup_Country Country { get; set; }
    }
}
