﻿using System.ComponentModel.DataAnnotations.Schema;

namespace MasProject.Data.Models.Address
{
    public class Lookup_Area : BaseEntity
    {
        public int CityID { get; set; }
        [ForeignKey("CityID")]
        public Lookup_City City { get; set; }
    }
}
