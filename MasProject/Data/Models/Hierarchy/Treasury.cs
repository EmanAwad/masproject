﻿using System.ComponentModel.DataAnnotations.Schema;
using MasProject.Data.Models.Persons;
namespace MasProject.Data.Models.Hierarchy
{
    public class Treasury : BaseEntity
    {
        //public string ResponsableName { get; set; }

        public int? UserId { get; set; }
        [ForeignKey("UserId")]
        public Employee Employee { get; set; }

        //public int? BranchId { get; set; }
        [ForeignKey("BranchId")]
        public Branch Branch { get; set; }

        public string Mobile { get; set; }

        public int? arrangement { get; set; }

        public string Code { get; set; }
       
    }
}
