﻿using MasProject.Data.Models.Persons;
using System.ComponentModel.DataAnnotations.Schema;

namespace MasProject.Data.Models.Hierarchy
{
    public class Store : BaseEntity
    {
        //public int? BranchId { get; set; }
        [ForeignKey("BranchId")]
        public Branch Branch { get; set; }
        public string Address { get; set; }
        public int CountryID { get; set; }
        public int CityID { get; set; }
        public int AreaID { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public int? EmployeeID { get; set; }
        [ForeignKey("EmployeeID")]
        public Employee Employee { get; set; }
        public int? arrangement { get; set; }
        public  bool MainStore { get; set; }
     }
}
