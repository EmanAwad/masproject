﻿using System.ComponentModel.DataAnnotations.Schema;

namespace MasProject.Data.Models.Hierarchy
{
    public class Branch : BaseEntity
    {
        public string Address { get; set; }
        public int CountryID { get; set; }
        public int CityID { get; set; }
        public int AreaID { get; set; }
        public int? CompanyId { get; set; }
        [ForeignKey("CompanyId")]
        public Company Company { get; set; }
        public string BranchKey { get; set; }
        public int? arrangement { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
    }
}
