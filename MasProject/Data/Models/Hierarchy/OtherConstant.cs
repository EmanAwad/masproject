﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Data.Models.Hierarchy
{
    public class OtherConstant : BaseEntity
    {
        public double? Ratio { get; set; }
        public int? arrangement { get; set; }
    }
}
