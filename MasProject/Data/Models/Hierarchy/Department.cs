﻿using System.ComponentModel.DataAnnotations.Schema;


namespace MasProject.Data.Models.Hierarchy
{
    public class Department : BaseEntity
    {
        //public int? BranchId { get; set; }
        [ForeignKey("BranchId")]
        public Branch Branch { get; set; }
        public int? arrangement { get; set; }
    }
}
