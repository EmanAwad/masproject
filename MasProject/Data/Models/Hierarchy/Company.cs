﻿namespace MasProject.Data.Models.Hierarchy
{
    public class Company : BaseEntity
    {
        public string Address { get; set; }
        public int CountryID { get; set; }
        public int CityID { get; set; }
        public int AreaID { get; set; }
        public string Logo { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Website { get; set; }
        public int? TaxNumber { get; set; }
        public int? CommerceNumber { get; set; }
        public int? arrangement { get; set; }

    }
}
