﻿namespace MasProject.Data.Models.Hierarchy
{
    public class Currency : BaseEntity
    {
        public double? Ratio { get; set; }
        public int? arrangement { get; set; }
    }
}
