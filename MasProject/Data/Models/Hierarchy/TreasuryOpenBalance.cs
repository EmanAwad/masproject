﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MasProject.Data.Models.Persons;
namespace MasProject.Data.Models.Hierarchy
{
    public class TreasuryOpenBalance : IEntity
    {
        [Key, Column(Order = 0)]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public DateTime EntryDate { get; set; }
        public int? TreasuryId { get; set; }
        [ForeignKey("TreasuryId")]
        public Treasury Treasury { get; set; }
        public decimal? Amount { get; set; }
        public int? CurrencyId { get; set; }
        [ForeignKey("CurrencyId")]
        public Currency Currency { get; set; }
        public decimal? CurrRatio { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string DeletedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public string Note { get; set; }
        public string Name { get; set; } = "";
        public int? UserId { get; set; }
        [ForeignKey("UserId")]
        public Employee Employee { get; set; }

        public int? BranchId { get; set; }
        public int? SerialNumber { get; set; }
    }
}
