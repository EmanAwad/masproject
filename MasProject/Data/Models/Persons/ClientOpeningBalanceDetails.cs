﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
namespace MasProject.Data.Models.Persons
{
   public class ClientOpeningBalanceDetails : BaseEntity
    {
        public int ClientDocumentNumber { get; set; }
        public double? Amount { get; set; }
        public int? ClientID { get; set; }
        [ForeignKey("ClientID")]
        public Clients ClientObj { get; set; }
    }
}
