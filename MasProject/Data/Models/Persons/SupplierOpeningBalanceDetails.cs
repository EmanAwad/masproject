﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MasProject.Data.Models.Persons
{
    public class SupplierOpeningBalanceDetails : BaseEntity
    {
        public int SupplierDocumentNumber { get; set; }
        public double? Amount { get; set; }

        public double? Ratio { get; set; }
        public int? SupplierID { get; set; }
        [ForeignKey("SupplierID")]
        public Supplier SupplierObj { get; set; }

        public int? CurrencyID { get; set; }
        [ForeignKey("CurrencyID")]
        public Supplier CurrencyObj { get; set; }
    }
}
