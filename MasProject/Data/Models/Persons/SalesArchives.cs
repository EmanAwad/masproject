﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace MasProject.Data.Models.Persons
{
   public class SalesArchives : BaseEntity
    {
        public string Year { get; set; }
        public string Month { get; set; }
        public Decimal? NetSales { get; set; }
        public string Note1 { get; set; }
        public int? ClientID { get; set; }
        [ForeignKey("ClientID")]
        public Clients ClientObj { get; set; }
    }
}
