﻿using System;   
using System.ComponentModel.DataAnnotations.Schema;
using MasProject.Data.Models.Hierarchy;

namespace MasProject.Data.Models.Persons
{
    public class SupplierOpeningBalance : BaseEntity
    {
        public DateTime EntryDate { get; set; }
        //   public double? Amount { get; set; }
        //  public double? Ratio { get; set; }
        //public int? SupplierID { get; set; }
        //[ForeignKey("SupplierID")]
        //public Supplier SupplierObj { get; set; }
        //public int? CurrencyID { get; set; }
        //[ForeignKey("CurrencyID")]
        //public Currency CurrencyObj { get; set; }

        public int DocumentNumber { get; set; }
    }
}