﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MasProject.Data.Models.Persons
{
    public class Social_Client : BaseEntity
    {

        public string Value { get; set; }
        public int? SocialID { get; set; }
        [ForeignKey("SocialID")]
        public SocialMedia SocialObj { get; set; }
        public int? ClientID { get; set; }
        [ForeignKey("ClientID")]
        public Clients ClientObj { get; set; }
    }
}
