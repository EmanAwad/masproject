﻿using MasProject.Data.Models.Hierarchy;
using System.ComponentModel.DataAnnotations.Schema;

namespace MasProject.Data.Models.Persons
{
    public class Supplier : Person
    {
        public string Taxfile { get; set; }
        public int? CurrencyID { get; set; }
        [ForeignKey("CurrencyID")]
        public Currency CurrencyObj { get; set; }
        public string EmployeeName { get; set; }
        public string WebSiteURL { get; set; }

        public bool IsHide { get; set; }
    }
}
