﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using MasProject.Data.Models.Hierarchy;

namespace MasProject.Data.Models.Persons
{
    public class ClientOpeningBalance : BaseEntity
    {
        public DateTime EntryDate { get; set; }
        public int DocumentNumber { get; set; }
       // public int? BranchID { get; set; }
        //[ForeignKey("BranchID")]
        public Branch BranchObj { get; set; }
        public int? EmployeeID { get; set; }
        [ForeignKey("EmployeeID")]
        public Employee EmployeeObj { get; set; }

    }
}
