﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasProject.Data.Models.Persons
{
  public  class EmployeeFiles : BaseEntity
    {
        public int EmployeeId { get; set; }
        public string FilePath { get; set; }
    }
}
