﻿using MasProject.Data.Models.Hierarchy;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace MasProject.Data.Models.Persons
{
    public class Clients : Person
    {

        public string Taxfile { get; set; }
        public string WebSiteURL { get; set; }
        public int? Dealingtype { get; set; }
        public DateTime FirstDealingDate { get; set; }
        public DateTime LastDealingDate { get; set; }
        public double? MonthlyAverage { get; set; }
        public bool IsBlocked { get; set; }
        public string EmployeeName { get; set; }
        // public int? BranchID { get; set; }
       // [ForeignKey("BranchID")]
        public Branch BranchObj { get; set; }
        [ForeignKey("Dealingtype")]
        public DealType DealTypeObj { get; set; }
        public string Mobile2 { get; set; }

        public bool IsHide { get; set; }
    }
}
