﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace MasProject.Data.Models.Persons
{
    public class PurchasesArchives : BaseEntity
    {
        public string Year { get; set; }
        public string Month { get; set; }
        public Decimal? NetPurchases { get; set; }
        public string Note1 { get; set; }
        public int? SupplierID { get; set; }
        [ForeignKey("SupplierID")]
        public Supplier SupplierObj { get; set; }
    }
}
