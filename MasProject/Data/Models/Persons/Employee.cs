﻿using MasProject.Data.Models.Hierarchy;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace MasProject.Data.Models.Persons
{
    public class Employee : Person
    {
        public string Image { get; set; }
        public DateTime DateOfBirth { get; set; }
        public DateTime JoiningDate { get; set; }
        public string JObTitle { get; set; }

        public double? StartSalary { get; set; }
        public double? AnnualIncrease { get; set; }
        public double? Annual { get; set; }
        public bool IsUser { get; set; }
        public string UserName { get; set; }
        public string Passwordouble { get; set; }
        public int? DepartmentID { get; set; }
        [ForeignKey("DepartmentID")]
        public Department DepartmentObj { get; set; }

       // public int? BranchID { get; set; }
        //[ForeignKey("BranchID")]
        public Branch BranchObj { get; set; }

        public string NationalID { get; set; }

        public bool IsHide { get; set; }
        //New 27-05-2021
        public DateTime LeavingDate { get; set; }
        public int AttachmentFiles { get; set; }
        public string PhoneWork { get; set; }
        public string PhoneWorkDescription { get; set; }
    }
}
