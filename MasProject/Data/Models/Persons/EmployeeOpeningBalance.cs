﻿using MasProject.Data.Models.Hierarchy;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace MasProject.Data.Models.Persons
{
    public class EmployeeOpeningBalance : BaseEntity
    {

        public DateTime EntryDate { get; set; }
        public double? Amount { get; set; }
        public int? EmployeeID { get; set; }
        [ForeignKey("EmployeeID")]
        public Employee EmployeeObj { get; set; }
    }
}
