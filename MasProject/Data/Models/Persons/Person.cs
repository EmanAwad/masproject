﻿namespace MasProject.Data.Models.Persons
{
    public class Person : BaseEntity
    {
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Address { get; set; }
        public string Mail { get; set; }
        public int? CountryID { get; set; }
        public int? CityID { get; set; }
        public int? AreaID { get; set; }
        public string Code { get; set; }
        public int? arrangement { get; set; }
        //public decimal EgyValue { get; set; }
    }
}
